#npm install uglify-js -g
#npm install css-minify -g
#npm install --save @babel/polyfill
./node_modules/.bin/babel assets/js/library-loader.js --out-file temp.js
uglifyjs --compress --mangle -o assets/js/dave-library-loader.js -- temp.js
rm -f temp.js

./node_modules/.bin/babel assets/js/jquery.js assets/js/polyfill.js assets/js/datetimepicker.js assets/js/blobUtil.js assets/js/dave.js assets/js/new-chat.js --out-file temp.js
uglifyjs --compress --mangle -o assets/js/dave-chatbot.js -- temp.js
rm -f temp.js

uglifyjs --compress --mangle -o assets/js/dave-chatbot-slim.js --  assets/js/jquery.js assets/js/dave.js assets/js/new-chat.js

./node_modules/.bin/babel assets/js/jquery.js assets/js/polyfill.js assets/js/datetimepicker.js assets/js/blobUtil.js assets/js/babylon.js assets/js/babylonjs.loaders.min.js assets/js/avatar.js assets/js/dave.js assets/js/new-chat.js assets/js/help.js --out-file temp.js
uglifyjs --compress --mangle -o assets/js/dave-chatbot-avatar.js -- temp.js
rm temp.js


./node_modules/.bin/babel assets/js/jquery.js assets/js/polyfill.js assets/js/datetimepicker.js assets/js/blobUtil.js  assets/js/streaming_speech/vendor/socket.io/socket.io.js  assets/js/streaming_speech/vendor/socket.io/socket.stream.io.js assets/js/streaming_speech/vendor/recordRTC/RecordRTC.js assets/js/streaming_speech/vendor/vad/vad.js assets/js/streaming_speech/vendor/libflac/data-util.js assets/js/streaming_speech/vendor/libflac/download-util.js assets/js/streaming_speech/vendor/libflac/file-handler.js assets/js/streaming_speech/vendor/libflac/libflac.js assets/js/streaming_speech/streaming_speech.js assets/js/dave.js assets/js/new-chat.js --out-file temp.js
uglifyjs --compress --mangle -o assets/js/dave-chatbot-speech.js -- temp.js 
rm temp.js


./node_modules/.bin/babel assets/js/jquery.js assets/js/polyfill.js assets/js/datetimepicker.js assets/js/blobUtil.js  assets/js/streaming_speech/vendor/socket.io/socket.io.js  assets/js/streaming_speech/vendor/socket.io/socket.stream.io.js assets/js/streaming_speech/vendor/recordRTC/RecordRTC.js assets/js/streaming_speech/vendor/vad/vad.js assets/js/streaming_speech/vendor/libflac/data-util.js assets/js/streaming_speech/vendor/libflac/download-util.js assets/js/streaming_speech/vendor/libflac/file-handler.js assets/js/streaming_speech/vendor/libflac/libflac.js assets/js/streaming_speech/streaming_speech.js assets/js/babylon.js assets/js/babylonjs.loaders.min.js assets/js/avatar.js assets/js/dave.js assets/js/new-chat.js assets/js/help.js --out-file temp.js
uglifyjs --compress --mangle -o assets/js/dave-chatbot-avatar-speech.js -- temp.js
rm temp.js

./node_modules/.bin/babel assets/js/jquery.js assets/js/polyfill.js assets/js/datetimepicker.js assets/js/blobUtil.js  assets/js/streaming_speech/vendor/socket.io/socket.io.js  assets/js/streaming_speech/vendor/socket.io/socket.stream.io.js assets/js/streaming_speech/vendor/recordRTC/RecordRTC.js assets/js/streaming_speech/vendor/vad/vad.js assets/js/streaming_speech/vendor/libflac/data-util.js assets/js/streaming_speech/vendor/libflac/download-util.js assets/js/streaming_speech/vendor/libflac/file-handler.js assets/js/streaming_speech/vendor/libflac/libflac.js assets/js/streaming_speech/streaming_speech.js assets/js/avatar.js assets/js/dave.js assets/js/new-chat.js assets/js/help.js --out-file temp.js
uglifyjs --compress --mangle -o assets/js/dave-chatbot-avatar-speech-wbabylon.js -- temp.js
rm temp.js

./node_modules/.bin/babel assets/js/polyfill.js assets/js/datetimepicker.js assets/js/blobUtil.js  assets/js/streaming_speech/vendor/socket.io/socket.io.js  assets/js/streaming_speech/vendor/socket.io/socket.stream.io.js assets/js/streaming_speech/vendor/recordRTC/RecordRTC.js assets/js/streaming_speech/vendor/vad/vad.js assets/js/streaming_speech/vendor/libflac/data-util.js assets/js/streaming_speech/vendor/libflac/download-util.js assets/js/streaming_speech/vendor/libflac/file-handler.js assets/js/streaming_speech/vendor/libflac/libflac.js assets/js/streaming_speech/streaming_speech.js assets/js/babylon.js assets/js/babylonjs.loaders.min.js assets/js/avatar.js assets/js/dave.js assets/js/new-chat.js assets/js/help.js --out-file temp.js
uglifyjs --compress --mangle -o assets/js/dave-chatbot-avatar-speech-wjq.js -- temp.js
rm temp.js

./node_modules/.bin/babel assets/js/polyfill.js assets/js/jquery.js assets/js/dave.js assets/js/help.js --out-file temp.js
uglifyjs --compress --mangle -o assets/js/dave-help.js -- temp.js
rm temp.js


./node_modules/.bin/babel assets/js/jquery.js assets/js/polyfill.js assets/js/datetimepicker.js assets/js/blobUtil.js  assets/js/dave.js assets/js/new-chat.js assets/js/help.js --out-file assets/js/dave-chatbot-help.unmin.js
uglifyjs --compress --mangle -o assets/js/dave-chatbot-help.js -- assets/js/dave-chatbot-help.unmin.js
rm -f temp.js

./node_modules/.bin/babel assets/js/jqc.js assets/js/polyfill.js assets/js/datetimepicker.js assets/js/blobUtil.js assets/js/dave.js assets/js/new-chat.js assets/js/help.js --out-file assets/js/dave-chatbot-wjq-help.unmin.js
uglifyjs --compress --mangle -o assets/js/dave-chatbot-wjq-help.js -- assets/js/dave-chatbot-wjq-help.unmin.js
rm -f temp.js

./node_modules/.bin/babel assets/js/jqc.js assets/js/datetimepicker.js assets/js/blobUtil.js assets/js/dave.js assets/js/new-chat.js assets/js/help.js --out-file temp.js
uglifyjs --compress --mangle -o assets/js/dave-chatbot-wjqpf-help.js -- temp.js
rm -f temp.js

uglifyjs -o assets/js/dave-libraries.js -- assets/js/jquery.js assets/js/streaming_speech/vendor/socket.io/socket.io.js  assets/js/streaming_speech/vendor/socket.io/socket.stream.io.js assets/js/streaming_speech/vendor/recordRTC/RecordRTC.js assets/js/streaming_speech/vendor/vad/vad.js assets/js/streaming_speech/vendor/libflac/data-util.js assets/js/streaming_speech/vendor/libflac/download-util.js assets/js/streaming_speech/vendor/libflac/file-handler.js assets/js/streaming_speech/vendor/libflac/libflac.js assets/js/streaming_speech/streaming_speech.js assets/js/babylon.js assets/js/babylonjs.loaders.min.js assets/js/avatar.js assets/js/dave.js assets/js/help.js

if [[ -n $1 ]]; then
    mkdir -p assets/js/$1
    mkdir -p assets/css/$1
    cp assets/js/dave-libraries.js assets/js/$1
    cp assets/js/dave-chatbot*.js assets/js/$1
    cp assets/js/dave-help.js assets/js/$1
    css-minify -d assets/css -o assets/css/$1
    mv assets/css/$1/dave-style.min.css assets/css/$1/dave-slim.min.css 
    cat assets/css/$1/dave-slim.min.css assets/css/$1/datetimepicker.min.css > assets/css/$1/dave-style.min.css
    pushd assets/css/$1
    for k in `ls *.min.css`; do
        cp $k ${k/min\./};
    done
fi
