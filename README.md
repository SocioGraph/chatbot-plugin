Plugin for DaveAI Chatbot
Hi! Welcome to the Javascript Plugin for DaveAI chatbot. This document walks-you through how to add the plugin to your page, how to customize the look and feel of the plugin.

What is not covered is how to configure the chat responses of Dave, which can currently be done by contacting our team. Do drop in a mail to sales@iamdave.ai to understand how you can do this.

How to include it your web-page
Adding the DaveAI chatbot to your webpage is very simple. You need the following three attributes

Conversation ID ( Don’t have the Conversation ID ?contact at sales@iamdave.ai )
Enterprise ID (optional)
Signup API Key (optional)
Once you do this, you have to add the following to the header of your HTML page

<script type="text/javascript" src="//chatbot-plugin.iamdave.ai/assets/js/dave-chatbot.js"></script>
<link rel="stylesheet" type="text/css" href="//chatbot-plugin.iamdave.ai/assets/css/dave-style.css">
Within the body somewhere, you need to add a div inside which the Dave Chatbot will be inserted.

<div id="dave-settings" data-conversation-id="<conversation id>"
</div>
Note – You may be able to add other settings as data parameters, the full-list of which is shared below.

Note – Altering the CSS for the plugin is also possible list of classes are also discussed below.

How to initialize the chat
If you don’t want to chat to be opened automatically, and only when the user clicks on the icon, then you don’t need to add any code. But you can open up the chat under different scenarios, as well as configure the bot to give different responses when it opens up.

Example 1:
Initialize, open up the chat as soon as the page loads

<script>
window.onload(function() {
   open_dave();
});
</script>
Example 2:
Send a message to the chat when the user clicks on something

<button onclick="botchat_data({'customer_response': 'Do something'},’open’)">Done Something</button>
Optional Data parameters
Chatbot’s Name/Title - <div class=“dave-main-container” id=“dave- settings” data-dave-cbTitle= ’ TITLE OF YOUR CHATBOT’
Image for chatbot - data-dave-botIcon=*‘LINK OF BOT ICON TO BE USED IN CHATBOT’**
Image for User/Customer - data-dave-userIcon=*‘LINK OF USER ICON TO BE USED IN CHATBOT’**
Main Icon Chatbot On Clicks On It Chat Box Will Open - data-dave-cbIcon=*‘MAIN ICON OF CHATBOT’**
Chat box Height - data-cb-height-desktop=*‘HEIGHT OF CHATBOT’**
Height Of Message Section - data-cb-innerHeight-desktop=*‘HEIGHT OF MESSAGE SECTION’**
Width Of Chat bot - data-cb-width-desktop='WIDTH OF CHATBOT’
Height Of Chat Bot For Mobile - data-cb-height-mobile=*‘HEIGHT OF CHATBOT MOBILE’**
Height Of Message Section For Mobile - data-cb-innerHeight-mobile=*‘HEIGHT OF MESSAGE SECTION MOBILE’**
Width Of Chat Bot For Mobile -
data-cb-width-mobile=*‘WIDTH OF CHATBOX(SET IT TO 100%)**’>
CSS classes
DYNAMIC ELEMENT CHANGES

CHAT TITLE BAR FONT CAN BE ADJUSTED HERE

.dave-cb-tt{

font: normal normal bold 16px/19px Calibri;
}

CHAT BUBLE COLOUR CAN BE CHANGED HERE
.dave-botchat,.dave-botchat:before,.dave-userchat,.dave-userchat:before{
background:#EFF5FC 0% 0% no-repeat padding-box;
}

SUGGESTED OPTIONS COLOR CAN BE CHANGED HERE
.dave-cb-chatsugg-list {
background: #DCEBFF 0% 0% no-repeat padding-box;

FONTS OF SUGGESTED OPTIONS CAN BE CHANGED HERE
font: normal normal bold 10px/13px Calibri;
}

COLOR OF TEXT WITHIN CHAT BUBLE CAN BE CHANGED FROM HERE
p.dave-userchat-p,p.dave-chattext{
color:#414042;

FONT OF TEXT WITHIN CHAT BUBBLE CAN BE CHANGED FROM HERE
font: normal normal normal 14px/15px Calibri;

}

.dave-cb-form form input[type=“submit”]{

COLOR OF CONTACT FORM CAN BE CHANGED FROM HERE
background-color: #b2cdec;

BORDER COLOR OF CONTACT FORM CAN BE CHANGED FROM HERE
border-color: #b2cdec;

FONT OF CONTACT FORM CAN BE CHANGED FROM HERE
font: normal normal normal 14px/15px Calibri;
}

ul.dave-option-list li button{

CHAT BUBLE LIST OPTION BACKGORUND COLOR CAN BE CHANGED FROM HERE
background: #FFFFFF;

CHAT BUBLE LIST OPTION BORDER COLOR CAN BE CHANGED FROM HERE
border-color:#FFFFFF;

CHAT BUBLE LIST OPTION FONT CAN BE CHANGED FROM HERE
font: normal normal bold 14px/13px Calibri;

CHAT BUBLE LIST OPTION TEXT COLOR CAN BE CHANGED FROM HERE
color: #000000;

}

CHANGE POSTION OF CHATBOT ON DESKTOP USING RIGHT BOTTOM [ALSO YOU CAN USE LEFT AND TOP IF NEEDED]
.dave-chatbox-cont{

right: 10%;

bottom: 10%;

}

/CHANGE POSTION OF CHATBOT ON MOBILE USING RIGHT BOTTOM [ALSO YOU CAN USE LEFT AND TOP IF NEEDED]/
@media only screen and (max-width: 450px){

.dave-chatbox-cont{

right: 0;

bottom: 5%;

}

}