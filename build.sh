#npm install uglify-js -g
#npm install css-minify -g
#npm install babel
#npm install --save @babel/polyfill
if [[ -n $1 ]]; then
    mkdir -p assets/js/$1
    mkdir -p assets/js/$1/min
    mkdir -p assets/css/$1
    for l in `ls assets/js/*.js`; do
        if [[ `basename $l` != 'dave-'* ]]; then
            echo "Compressing file" $l 1>&2
            ./node_modules/.bin/babel $l --out-file temp.js
            uglifyjs --compress --mangle -o assets/js/$1/min/`basename $l` -- temp.js
            rm -f temp.js
            cp $l assets/js/$1/`basename $l`
        fi
    done
    for l in streaming_speech/vendor/socket.io/socket.io.js streaming_speech/vendor/socket.io/socket.stream.io.js streaming_speech/vendor/recordRTC/RecordRTC.js streaming_speech/vendor/vad/vad.js streaming_speech/vendor/libflac/data-util.js streaming_speech/vendor/libflac/download-util.js streaming_speech/vendor/libflac/file-handler.js streaming_speech/vendor/libflac/libflac.js streaming_speech/streaming_speech.js; do
        echo "Compressing file" $l 1>&2
        mkdir -p `dirname assets/js/$1/$l`
        mkdir -p `dirname assets/js/$1/min/$l`
        ./node_modules/.bin/babel assets/js/$l --out-file temp.js
        uglifyjs --compress --mangle -o assets/js/$1/min/$l -- temp.js
        rm -f temp.js
        cp assets/js/$l assets/js/$1/$l
    done
    css-minify -d assets/css -o assets/css/$1
    mv assets/css/$1/dave-style.min.css assets/css/$1/dave-slim.min.css 
    cat assets/css/$1/dave-slim.min.css assets/css/$1/datetimepicker.min.css > assets/css/$1/dave-style.min.css
    pushd assets/css/$1
    for k in `ls *.min.css`; do
        cp $k ${k/min\./};
    done
fi

