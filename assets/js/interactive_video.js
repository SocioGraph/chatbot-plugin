var DAVE_IV_SETTINGS = {
  CONVERSATION_ID: null,
  LANGUAGE: null,
  VOICE_ID: null,
  AVATAR_ID: null,
  VIDEO_ID: "9ac779b7-bcd4-38c6-af0d-263c97c7e61d",
};
//65129589-bda0-38a9-afe3-c2fd1e01dc7f
DAVE_IV_SETTINGS.initialize_video_player = async function initialize_video_player(video_id, callbackFunc, errorFunc, repeats) {
    repeats = repeats || 0;
    // get video id from DAVE_IV_SETTINGS or params
    video_id = video_id || DAVE_IV_SETTINGS.VIDEO_ID;

    if ( !DAVE_SETTINGS.signup_guard(DAVE_IV_SETTINGS.initialize_video_player, [video_id, callbackFunc, errorFunc, repeats || 0], "dave_user_id")) {
        return;
    }
    // get video, tag, branch, forum questions, answers, chat data for video_id
    let videoData = {};
    DAVE_IV_SETTINGS.get_video(video_id, {}, function(data) {
        videoData.video = data;
        DAVE_IV_SETTINGS.get_video_tags(video_id, {'_page_number': 1}, videoData, function(){
            DAVE_IV_SETTINGS.get_video_branches(video_id, {'_page_number': 1}, videoData, function(){
                DAVE_IV_SETTINGS.get_forum_questions(video_id, {'_page_number': 1}, videoData, function(){
                    DAVE_IV_SETTINGS.get_forum_answers(video_id, {'_page_number': 1}, videoData, function(){
                        DAVE_SETTINGS.chat({'conversation_id': data.conversation_id, 'query_type': 'auto'});
                        callbackFunc(videoData);
                    });
                });
            });
            
        });
       
    });
};



/* Sample response for video DATA
 *         {
            "tags": [
                "3D virtual Store",
                "Demostration",
                "iPhone Store"
            ],
            "plays": 0,
            "views": 0,
            "answers": 0,
            "created": "2021-07-09 02:00:35 PM +0000",
            "queries": 0,
            "updated": "2021-07-09 02:00:35 PM +0000",
            "video_id": "7e4f5397-ea1e-3814-8b75-2fc8802160a8",
            "video_url": "https://general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/promotekar/hgjgh.mp4",
            "video_name": "3D Virtual Store example",
            "exhibitor_id": "dave_interactive_video_demo",
            "public_queries": 0,
            "conversation_id": "exhibitor_interactive_video_demo",
            "private_queries": 0
        }
 */
DAVE_IV_SETTINGS.get_video = function get_video(video_id, params, callbackFunc, errorFunc, repeats) {
    repeats = repeats || 0;
    params = params || {};
    if ( !DAVE_SETTINGS.signup_guard(DAVE_IV_SETTINGS.get_video, [video_id, params, callbackFunc, errorFunc, repeats || 0], "dave_user_id" )) {
        return;
    }
    // get video id from DAVE_IV_SETTINGS or params
    video_id = video_id || DAVE_IV_SETTINGS.VIDEO_ID;
    return DAVE_SETTINGS.ajaxRequestWithData("/object/video/" + video_id, "GET", params, callbackFunc, errorFunc);
};


DAVE_IV_SETTINGS.get_objects = function(model, video_id, params, videoData, addToVideoPlayer, callbackFunc, errorFunc, repeats) {
    repeats = repeats || 0;
    if ( !DAVE_SETTINGS.signup_guard(DAVE_IV_SETTINGS.get_objects, [model, video_id, params, callbackFunc, errorFunc, repeats || 0], "dave_user_id") ) {
        return;
    }
    params = params || {};
    params.video_id = video_id || DAVE_IV_SETTINGS.VIDEO_ID;
    params['_page_number'] = params['_page_number'] || 1;
    return DAVE_SETTINGS.ajaxRequestWithData("/objects/"+ model, "GET", params, function(data) {
        if ( params['_page_number'] <= 1 ) {
            videoData[model] = [];
        }
        videoData[model].push(...data.data);
        if (addToVideoPlayer && typeof(addToVideoPlayer) == 'function') {
            addToVideoPlayer(data.data);
        }
        if ( data.is_last ) {
            if (callbackFunc && typeof(callbackFunc) == 'function') {
                callbackFunc(data)
            }
        } else {
            let p1 = {...params};
            p1['_page_number'] += 1;
            DAVE_IV_SETTINGS.get_objects(model, video_id, p1, videoData, callbackFunc, errorFunc);
        }
    }, errorFunc);
}


/* Sample data for video tags
 *  [
        {
            "clicks": 0,
            "position": [
                {
                    "x": 50,
                    "y": 50,
                    "end_time": 17,
                    "start_time": 17
                },
                {
                    "x": 51,
                    "y": 51,
                    "end_time": 18,
                    "start_time": 18
                },
                {
                    "x": 52,
                    "y": 52,
                    "end_time": 19,
                    "start_time": 19
                },
                {
                    "x": 53,
                    "y": 53,
                    "end_time": 25,
                    "start_time": 20
                }
            ],
            "video_id": "7e4f5397-ea1e-3814-8b75-2fc8802160a8",
            "exhibitor_id": "dave_interactive_video_demo",
            "video_tag_id": "8dba91b3-6600-36b7-909b-89f11808392a",
            "customer_state": "cs_iphone_12",
            "conversation_id": "exhibitor_interactive_video_demo",
            "customer_response": "Tell me more about iPhone 12"
        },
        {
            "clicks": 0,
            "position": [
                {
                    "x": 70,
                    "y": 70,
                    "end_time": 17,
                    "start_time": 17
                },
                {
                    "x": 71,
                    "y": 71,
                    "end_time": 18,
                    "start_time": 18
                },
                {
                    "x": 72,
                    "y": 72,
                    "end_time": 19,
                    "start_time": 19
                },
                {
                    "x": 73,
                    "y": 73,
                    "end_time": 25,
                    "start_time": 20
                }
            ],
            "video_id": "7e4f5397-ea1e-3814-8b75-2fc8802160a8",
            "exhibitor_id": "dave_interactive_video_demo",
            "video_tag_id": "9c6f5a35-2ff9-3bab-98d3-dea783e136d4",
            "customer_state": "cs_iphone_10",
            "conversation_id": "exhibitor_interactive_video_demo",
            "customer_response": "Tell me more about iPhone 10"
        }
    ]
 */
DAVE_IV_SETTINGS.get_video_tags = function get_video_tags(video_id, params, videoData, callbackFunc, errorFunc) {
    DAVE_IV_SETTINGS.get_objects('video_tag', video_id, params, videoData, function(tags) {
        // NOTE: Code to register callback to video player with the video tags.
    }, callbackFunc, errorFunc);
};


/*  Sample of video branch data
 * [
        {
            "clicks": 0,
            "options": [
                {
                    "customer_state": "cs_not_interested",
                    "customer_response": "I'm not interested"
                },
                {
                    "customer_state": "cs_interested",
                    "customer_response": "I'm interested"
                }
            ],
            "question": "Are you interested?",
            "template": "options.html",
            "video_id": "7e4f5397-ea1e-3814-8b75-2fc8802160a8",
            "timestamp": 55,
            "exhibitor_id": "dave_interactive_video_demo",
            "conversation_id": "exhibitor_interactive_video_demo",
            "video_branch_id": "09d90dac-fce8-3ddf-bfb7-197bb9213503"
        },
        {
            "clicks": 0,
            "options": [
                {
                    "customer_state": "cs_not_purchase",
                    "customer_response": "I don't want to buy it"
                },
                {
                    "customer_state": "cs_purchase",
                    "customer_response": "I want to buy it"
                }
            ],
            "question": "Do you want to buy it?",
            "template": "options.html",
            "video_id": "7e4f5397-ea1e-3814-8b75-2fc8802160a8",
            "timestamp": 72,
            "exhibitor_id": "dave_interactive_video_demo",
            "conversation_id": "exhibitor_interactive_video_demo",
            "video_branch_id": "d4af4492-d09f-31ca-8981-11d097c19fd4"
        }
    ]
 */
DAVE_IV_SETTINGS.get_video_branches = function get_video_branches(video_id, params, videoData, callbackFunc, errorFunc) {
    DAVE_IV_SETTINGS.get_objects('video_branch', video_id, params, videoData, function(branches) {
        // NOTE: Code to register callback to video player with the video branch.
    }, callbackFunc, errorFunc);
};

DAVE_IV_SETTINGS.get_forum_questions = function get_forum_questions(video_id, params, videoData, callbackFunc, errorFunc) {
    params = params || {};
    params.is_public = true;
    DAVE_IV_SETTINGS.get_objects('forum_question', video_id, params, videoData, function(branches) {
        // NOTE: Code to register callback to video player with the video branch.
    }, callbackFunc, errorFunc);
};

DAVE_IV_SETTINGS.get_forum_answers = function get_forum_answers(video_id, params, videoData, callbackFunc, errorFunc) {
    params = params || {};
    params.is_public = true;
    DAVE_IV_SETTINGS.get_objects('forum_answer', video_id, params, videoData, function(branches) {
        // NOTE: Code to register callback to video player with the video branch.
    }, callbackFunc, errorFunc);
};

DAVE_IV_SETTINGS.post_forum_answer = function post_forum_answer(question_id, answer, video_id, params, callbackFunc, errorFunc, repeats) {
    repeats = repeats || 0;
    if ( !DAVE_SETTINGS.signup_guard(DAVE_IV_SETTINGS.get_objects, [question_id, answer, video_id, params, callbackFunc, errorFunc, repeats || 0], "dave_user_id") ) {
        return;
    }
    params = params || {};
    params.video_id = video_id || DAVE_IV_SETTINGS.VIDEO_ID;
    params.question_id = question_id;
    params.user_id = DAVE_SETTINGS.getCookie('dave_user_id');
    params.answer = answer;
    return DAVE_SETTINGS.ajaxRequestWithData("/object/forum_answer", "POST", params, function(data) {
        if (callbackFunc && typeof(callbackFunc) == 'function') {
            callbackFunc(data)
        }
    }, errorFunc);
};

DAVE_IV_SETTINGS.post_forum_question = function post_forum_question(question, video_id, is_private, params, callbackFunc, errorFunc, repeats) {
    repeats = repeats || 0;
    if ( !DAVE_SETTINGS.signup_guard(DAVE_IV_SETTINGS.get_objects, [question, video_id, is_private, params, callbackFunc, errorFunc, repeats || 0], "dave_user_id") ) {
        return;
    }
    params = params || {};
    params.video_id = video_id || DAVE_IV_SETTINGS.VIDEO_ID;
    params.question = question;
    params.is_public = !is_private;
    params.user_id = DAVE_SETTINGS.getCookie('dave_user_id');
    return DAVE_SETTINGS.ajaxRequestWithData("/object/forum_question", "POST", params, function(data) {
        if (callbackFunc && typeof(callbackFunc) == 'function') {
            callbackFunc(data)
        }
    }, errorFunc);
};
