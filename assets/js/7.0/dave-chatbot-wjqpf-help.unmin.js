var djQ = $;
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2016
 * @version 1.3.4
 *
 * Date formatter utility library that allows formatting date/time variables or Date objects using PHP DateTime format.
 * @see http://php.net/manual/en/function.date.php
 *
 * For more JQuery plugins visit http://plugins.krajee.com
 * For more Yii related demos visit http://demos.krajee.com
 */
var DateFormatter;
!function () {
  "use strict";

  var t, _e, _r, n, a, u, i;

  u = 864e5, i = 3600, t = function t(_t, e) {
    return "string" == typeof _t && "string" == typeof e && _t.toLowerCase() === e.toLowerCase();
  }, _e = function e(t, r, n) {
    var a = n || "0",
        u = t.toString();
    return u.length < r ? _e(a + u, r) : u;
  }, _r = function r(t) {
    var e, n;

    for (t = t || {}, e = 1; e < arguments.length; e++) {
      if (n = arguments[e]) for (var a in n) {
        n.hasOwnProperty(a) && ("object" == _typeof(n[a]) ? _r(t[a], n[a]) : t[a] = n[a]);
      }
    }

    return t;
  }, n = function n(t, e) {
    for (var r = 0; r < e.length; r++) {
      if (e[r].toLowerCase() === t.toLowerCase()) return r;
    }

    return -1;
  }, a = {
    dateSettings: {
      days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      meridiem: ["AM", "PM"],
      ordinal: function ordinal(t) {
        var e = t % 10,
            r = {
          1: "st",
          2: "nd",
          3: "rd"
        };
        return 1 !== Math.floor(t % 100 / 10) && r[e] ? r[e] : "th";
      }
    },
    separators: /[ \-+\/\.T:@]/g,
    validParts: /[dDjlNSwzWFmMntLoYyaABgGhHisueTIOPZcrU]/g,
    intParts: /[djwNzmnyYhHgGis]/g,
    tzParts: /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
    tzClip: /[^-+\dA-Z]/g
  }, DateFormatter = function DateFormatter(t) {
    var e = this,
        n = _r(a, t);

    e.dateSettings = n.dateSettings, e.separators = n.separators, e.validParts = n.validParts, e.intParts = n.intParts, e.tzParts = n.tzParts, e.tzClip = n.tzClip;
  }, DateFormatter.prototype = {
    constructor: DateFormatter,
    getMonth: function getMonth(t) {
      var e,
          r = this;
      return e = n(t, r.dateSettings.monthsShort) + 1, 0 === e && (e = n(t, r.dateSettings.months) + 1), e;
    },
    parseDate: function parseDate(e, r) {
      var n,
          a,
          u,
          i,
          s,
          o,
          c,
          f,
          l,
          h,
          d = this,
          g = !1,
          m = !1,
          p = d.dateSettings,
          y = {
        date: null,
        year: null,
        month: null,
        day: null,
        hour: 0,
        min: 0,
        sec: 0
      };
      if (!e) return null;
      if (e instanceof Date) return e;
      if ("U" === r) return u = parseInt(e), u ? new Date(1e3 * u) : e;

      switch (_typeof(e)) {
        case "number":
          return new Date(e);

        case "string":
          break;

        default:
          return null;
      }

      if (n = r.match(d.validParts), !n || 0 === n.length) throw new Error("Invalid date format definition.");

      for (a = e.replace(d.separators, "\x00").split("\x00"), u = 0; u < a.length; u++) {
        switch (i = a[u], s = parseInt(i), n[u]) {
          case "y":
          case "Y":
            if (!s) return null;
            l = i.length, y.year = 2 === l ? parseInt((70 > s ? "20" : "19") + i) : s, g = !0;
            break;

          case "m":
          case "n":
          case "M":
          case "F":
            if (isNaN(s)) {
              if (o = d.getMonth(i), !(o > 0)) return null;
              y.month = o;
            } else {
              if (!(s >= 1 && 12 >= s)) return null;
              y.month = s;
            }

            g = !0;
            break;

          case "d":
          case "j":
            if (!(s >= 1 && 31 >= s)) return null;
            y.day = s, g = !0;
            break;

          case "g":
          case "h":
            if (c = n.indexOf("a") > -1 ? n.indexOf("a") : n.indexOf("A") > -1 ? n.indexOf("A") : -1, h = a[c], c > -1) f = t(h, p.meridiem[0]) ? 0 : t(h, p.meridiem[1]) ? 12 : -1, s >= 1 && 12 >= s && f > -1 ? y.hour = s + f - 1 : s >= 0 && 23 >= s && (y.hour = s);else {
              if (!(s >= 0 && 23 >= s)) return null;
              y.hour = s;
            }
            m = !0;
            break;

          case "G":
          case "H":
            if (!(s >= 0 && 23 >= s)) return null;
            y.hour = s, m = !0;
            break;

          case "i":
            if (!(s >= 0 && 59 >= s)) return null;
            y.min = s, m = !0;
            break;

          case "s":
            if (!(s >= 0 && 59 >= s)) return null;
            y.sec = s, m = !0;
        }
      }

      if (g === !0 && y.year && y.month && y.day) y.date = new Date(y.year, y.month - 1, y.day, y.hour, y.min, y.sec, 0);else {
        if (m !== !0) return null;
        y.date = new Date(0, 0, 0, y.hour, y.min, y.sec, 0);
      }
      return y.date;
    },
    guessDate: function guessDate(t, e) {
      if ("string" != typeof t) return t;
      var r,
          n,
          a,
          u,
          i,
          s,
          o = this,
          c = t.replace(o.separators, "\x00").split("\x00"),
          f = /^[djmn]/g,
          l = e.match(o.validParts),
          h = new Date(),
          d = 0;
      if (!f.test(l[0])) return t;

      for (a = 0; a < c.length; a++) {
        if (d = 2, i = c[a], s = parseInt(i.substr(0, 2)), isNaN(s)) return null;

        switch (a) {
          case 0:
            "m" === l[0] || "n" === l[0] ? h.setMonth(s - 1) : h.setDate(s);
            break;

          case 1:
            "m" === l[0] || "n" === l[0] ? h.setDate(s) : h.setMonth(s - 1);
            break;

          case 2:
            if (n = h.getFullYear(), r = i.length, d = 4 > r ? r : 4, n = parseInt(4 > r ? n.toString().substr(0, 4 - r) + i : i.substr(0, 4)), !n) return null;
            h.setFullYear(n);
            break;

          case 3:
            h.setHours(s);
            break;

          case 4:
            h.setMinutes(s);
            break;

          case 5:
            h.setSeconds(s);
        }

        u = i.substr(d), u.length > 0 && c.splice(a + 1, 0, u);
      }

      return h;
    },
    parseFormat: function parseFormat(t, r) {
      var n,
          a = this,
          s = a.dateSettings,
          o = /\\?(.?)/gi,
          _c = function c(t, e) {
        return n[t] ? n[t]() : e;
      };

      return n = {
        d: function d() {
          return _e(n.j(), 2);
        },
        D: function D() {
          return s.daysShort[n.w()];
        },
        j: function j() {
          return r.getDate();
        },
        l: function l() {
          return s.days[n.w()];
        },
        N: function N() {
          return n.w() || 7;
        },
        w: function w() {
          return r.getDay();
        },
        z: function z() {
          var t = new Date(n.Y(), n.n() - 1, n.j()),
              e = new Date(n.Y(), 0, 1);
          return Math.round((t - e) / u);
        },
        W: function W() {
          var t = new Date(n.Y(), n.n() - 1, n.j() - n.N() + 3),
              r = new Date(t.getFullYear(), 0, 4);
          return _e(1 + Math.round((t - r) / u / 7), 2);
        },
        F: function F() {
          return s.months[r.getMonth()];
        },
        m: function m() {
          return _e(n.n(), 2);
        },
        M: function M() {
          return s.monthsShort[r.getMonth()];
        },
        n: function n() {
          return r.getMonth() + 1;
        },
        t: function t() {
          return new Date(n.Y(), n.n(), 0).getDate();
        },
        L: function L() {
          var t = n.Y();
          return t % 4 === 0 && t % 100 !== 0 || t % 400 === 0 ? 1 : 0;
        },
        o: function o() {
          var t = n.n(),
              e = n.W(),
              r = n.Y();
          return r + (12 === t && 9 > e ? 1 : 1 === t && e > 9 ? -1 : 0);
        },
        Y: function Y() {
          return r.getFullYear();
        },
        y: function y() {
          return n.Y().toString().slice(-2);
        },
        a: function a() {
          return n.A().toLowerCase();
        },
        A: function A() {
          var t = n.G() < 12 ? 0 : 1;
          return s.meridiem[t];
        },
        B: function B() {
          var t = r.getUTCHours() * i,
              n = 60 * r.getUTCMinutes(),
              a = r.getUTCSeconds();
          return _e(Math.floor((t + n + a + i) / 86.4) % 1e3, 3);
        },
        g: function g() {
          return n.G() % 12 || 12;
        },
        G: function G() {
          return r.getHours();
        },
        h: function h() {
          return _e(n.g(), 2);
        },
        H: function H() {
          return _e(n.G(), 2);
        },
        i: function i() {
          return _e(r.getMinutes(), 2);
        },
        s: function s() {
          return _e(r.getSeconds(), 2);
        },
        u: function u() {
          return _e(1e3 * r.getMilliseconds(), 6);
        },
        e: function e() {
          var t = /\((.*)\)/.exec(String(r))[1];
          return t || "Coordinated Universal Time";
        },
        I: function I() {
          var t = new Date(n.Y(), 0),
              e = Date.UTC(n.Y(), 0),
              r = new Date(n.Y(), 6),
              a = Date.UTC(n.Y(), 6);
          return t - e !== r - a ? 1 : 0;
        },
        O: function O() {
          var t = r.getTimezoneOffset(),
              n = Math.abs(t);
          return (t > 0 ? "-" : "+") + _e(100 * Math.floor(n / 60) + n % 60, 4);
        },
        P: function P() {
          var t = n.O();
          return t.substr(0, 3) + ":" + t.substr(3, 2);
        },
        T: function T() {
          var t = (String(r).match(a.tzParts) || [""]).pop().replace(a.tzClip, "");
          return t || "UTC";
        },
        Z: function Z() {
          return 60 * -r.getTimezoneOffset();
        },
        c: function c() {
          return "Y-m-d\\TH:i:sP".replace(o, _c);
        },
        r: function r() {
          return "D, d M Y H:i:s O".replace(o, _c);
        },
        U: function U() {
          return r.getTime() / 1e3 || 0;
        }
      }, _c(t, t);
    },
    formatDate: function formatDate(t, e) {
      var r,
          n,
          a,
          u,
          i,
          s = this,
          o = "",
          c = "\\";
      if ("string" == typeof t && (t = s.parseDate(t, e), !t)) return null;

      if (t instanceof Date) {
        for (a = e.length, r = 0; a > r; r++) {
          i = e.charAt(r), "S" !== i && i !== c && (r > 0 && e.charAt(r - 1) === c ? o += i : (u = s.parseFormat(i, t), r !== a - 1 && s.intParts.test(i) && "S" === e.charAt(r + 1) && (n = parseInt(u) || 0, u += s.dateSettings.ordinal(n)), o += u));
        }

        return o;
      }

      return "";
    }
  };
}();
/**
 * @preserve jQuery DateTimePicker
 * @homepage http://xdsoft.net/jqplugins/datetimepicker/
 * @author Chupurnov Valeriy (<chupurnov@gmail.com>)
 */

/**
 * @param {jQuery} $
 */

var datetimepickerFactory = function datetimepickerFactory($) {
  'use strict';

  var default_options = {
    i18n: {
      ar: {
        // Arabic
        months: ["كانون الثاني", "شباط", "آذار", "نيسان", "مايو", "حزيران", "تموز", "آب", "أيلول", "تشرين الأول", "تشرين الثاني", "كانون الأول"],
        dayOfWeekShort: ["ن", "ث", "ع", "خ", "ج", "س", "ح"],
        dayOfWeek: ["الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت", "الأحد"]
      },
      ro: {
        // Romanian
        months: ["Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"],
        dayOfWeekShort: ["Du", "Lu", "Ma", "Mi", "Jo", "Vi", "Sâ"],
        dayOfWeek: ["Duminică", "Luni", "Marţi", "Miercuri", "Joi", "Vineri", "Sâmbătă"]
      },
      id: {
        // Indonesian
        months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        dayOfWeekShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
        dayOfWeek: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"]
      },
      is: {
        // Icelandic
        months: ["Janúar", "Febrúar", "Mars", "Apríl", "Maí", "Júní", "Júlí", "Ágúst", "September", "Október", "Nóvember", "Desember"],
        dayOfWeekShort: ["Sun", "Mán", "Þrið", "Mið", "Fim", "Fös", "Lau"],
        dayOfWeek: ["Sunnudagur", "Mánudagur", "Þriðjudagur", "Miðvikudagur", "Fimmtudagur", "Föstudagur", "Laugardagur"]
      },
      bg: {
        // Bulgarian
        months: ["Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември"],
        dayOfWeekShort: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        dayOfWeek: ["Неделя", "Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък", "Събота"]
      },
      fa: {
        // Persian/Farsi
        months: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],
        dayOfWeekShort: ['یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه', 'شنبه'],
        dayOfWeek: ["یک‌شنبه", "دوشنبه", "سه‌شنبه", "چهارشنبه", "پنج‌شنبه", "جمعه", "شنبه", "یک‌شنبه"]
      },
      ru: {
        // Russian
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        dayOfWeekShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        dayOfWeek: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"]
      },
      uk: {
        // Ukrainian
        months: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
        dayOfWeekShort: ["Ндл", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Сбт"],
        dayOfWeek: ["Неділя", "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота"]
      },
      en: {
        // English
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        dayOfWeekShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
      },
      el: {
        // Ελληνικά
        months: ["Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"],
        dayOfWeekShort: ["Κυρ", "Δευ", "Τρι", "Τετ", "Πεμ", "Παρ", "Σαβ"],
        dayOfWeek: ["Κυριακή", "Δευτέρα", "Τρίτη", "Τετάρτη", "Πέμπτη", "Παρασκευή", "Σάββατο"]
      },
      de: {
        // German
        months: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        dayOfWeekShort: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
        dayOfWeek: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"]
      },
      nl: {
        // Dutch
        months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
        dayOfWeekShort: ["zo", "ma", "di", "wo", "do", "vr", "za"],
        dayOfWeek: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"]
      },
      tr: {
        // Turkish
        months: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayOfWeekShort: ["Paz", "Pts", "Sal", "Çar", "Per", "Cum", "Cts"],
        dayOfWeek: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"]
      },
      fr: {
        //French
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        dayOfWeekShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
        dayOfWeek: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"]
      },
      es: {
        // Spanish
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        dayOfWeekShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
        dayOfWeek: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]
      },
      th: {
        // Thai
        months: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
        dayOfWeekShort: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
        dayOfWeek: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์", "อาทิตย์"]
      },
      pl: {
        // Polish
        months: ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"],
        dayOfWeekShort: ["nd", "pn", "wt", "śr", "cz", "pt", "sb"],
        dayOfWeek: ["niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"]
      },
      pt: {
        // Portuguese
        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        dayOfWeekShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
        dayOfWeek: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"]
      },
      ch: {
        // Simplified Chinese
        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"]
      },
      se: {
        // Swedish
        months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
        dayOfWeekShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"]
      },
      km: {
        // Khmer (ភាសាខ្មែរ)
        months: ["មករា​", "កុម្ភៈ", "មិនា​", "មេសា​", "ឧសភា​", "មិថុនា​", "កក្កដា​", "សីហា​", "កញ្ញា​", "តុលា​", "វិច្ឆិកា", "ធ្នូ​"],
        dayOfWeekShort: ["អាទិ​", "ច័ន្ទ​", "អង្គារ​", "ពុធ​", "ព្រហ​​", "សុក្រ​", "សៅរ៍"],
        dayOfWeek: ["អាទិត្យ​", "ច័ន្ទ​", "អង្គារ​", "ពុធ​", "ព្រហស្បតិ៍​", "សុក្រ​", "សៅរ៍"]
      },
      kr: {
        // Korean
        months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
        dayOfWeekShort: ["일", "월", "화", "수", "목", "금", "토"],
        dayOfWeek: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"]
      },
      it: {
        // Italian
        months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
        dayOfWeekShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
        dayOfWeek: ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"]
      },
      da: {
        // Dansk
        months: ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"],
        dayOfWeekShort: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
        dayOfWeek: ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"]
      },
      no: {
        // Norwegian
        months: ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"],
        dayOfWeekShort: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
        dayOfWeek: ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag']
      },
      ja: {
        // Japanese
        months: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
        dayOfWeekShort: ["日", "月", "火", "水", "木", "金", "土"],
        dayOfWeek: ["日曜", "月曜", "火曜", "水曜", "木曜", "金曜", "土曜"]
      },
      vi: {
        // Vietnamese
        months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        dayOfWeekShort: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        dayOfWeek: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"]
      },
      sl: {
        // Slovenščina
        months: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
        dayOfWeekShort: ["Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob"],
        dayOfWeek: ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Četrtek", "Petek", "Sobota"]
      },
      cs: {
        // Čeština
        months: ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"],
        dayOfWeekShort: ["Ne", "Po", "Út", "St", "Čt", "Pá", "So"]
      },
      hu: {
        // Hungarian
        months: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
        dayOfWeekShort: ["Va", "Hé", "Ke", "Sze", "Cs", "Pé", "Szo"],
        dayOfWeek: ["vasárnap", "hétfő", "kedd", "szerda", "csütörtök", "péntek", "szombat"]
      },
      az: {
        //Azerbaijanian (Azeri)
        months: ["Yanvar", "Fevral", "Mart", "Aprel", "May", "Iyun", "Iyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"],
        dayOfWeekShort: ["B", "Be", "Ça", "Ç", "Ca", "C", "Ş"],
        dayOfWeek: ["Bazar", "Bazar ertəsi", "Çərşənbə axşamı", "Çərşənbə", "Cümə axşamı", "Cümə", "Şənbə"]
      },
      bs: {
        //Bosanski
        months: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
        dayOfWeekShort: ["Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub"],
        dayOfWeek: ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota"]
      },
      ca: {
        //Català
        months: ["Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"],
        dayOfWeekShort: ["Dg", "Dl", "Dt", "Dc", "Dj", "Dv", "Ds"],
        dayOfWeek: ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"]
      },
      'en-GB': {
        //English (British)
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        dayOfWeekShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
      },
      et: {
        //"Eesti"
        months: ["Jaanuar", "Veebruar", "Märts", "Aprill", "Mai", "Juuni", "Juuli", "August", "September", "Oktoober", "November", "Detsember"],
        dayOfWeekShort: ["P", "E", "T", "K", "N", "R", "L"],
        dayOfWeek: ["Pühapäev", "Esmaspäev", "Teisipäev", "Kolmapäev", "Neljapäev", "Reede", "Laupäev"]
      },
      eu: {
        //Euskara
        months: ["Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua"],
        dayOfWeekShort: ["Ig.", "Al.", "Ar.", "Az.", "Og.", "Or.", "La."],
        dayOfWeek: ['Igandea', 'Astelehena', 'Asteartea', 'Asteazkena', 'Osteguna', 'Ostirala', 'Larunbata']
      },
      fi: {
        //Finnish (Suomi)
        months: ["Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "Kesäkuu", "Heinäkuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"],
        dayOfWeekShort: ["Su", "Ma", "Ti", "Ke", "To", "Pe", "La"],
        dayOfWeek: ["sunnuntai", "maanantai", "tiistai", "keskiviikko", "torstai", "perjantai", "lauantai"]
      },
      gl: {
        //Galego
        months: ["Xan", "Feb", "Maz", "Abr", "Mai", "Xun", "Xul", "Ago", "Set", "Out", "Nov", "Dec"],
        dayOfWeekShort: ["Dom", "Lun", "Mar", "Mer", "Xov", "Ven", "Sab"],
        dayOfWeek: ["Domingo", "Luns", "Martes", "Mércores", "Xoves", "Venres", "Sábado"]
      },
      hr: {
        //Hrvatski
        months: ["Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"],
        dayOfWeekShort: ["Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub"],
        dayOfWeek: ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota"]
      },
      ko: {
        //Korean (한국어)
        months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
        dayOfWeekShort: ["일", "월", "화", "수", "목", "금", "토"],
        dayOfWeek: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"]
      },
      lt: {
        //Lithuanian (lietuvių)
        months: ["Sausio", "Vasario", "Kovo", "Balandžio", "Gegužės", "Birželio", "Liepos", "Rugpjūčio", "Rugsėjo", "Spalio", "Lapkričio", "Gruodžio"],
        dayOfWeekShort: ["Sek", "Pir", "Ant", "Tre", "Ket", "Pen", "Šeš"],
        dayOfWeek: ["Sekmadienis", "Pirmadienis", "Antradienis", "Trečiadienis", "Ketvirtadienis", "Penktadienis", "Šeštadienis"]
      },
      lv: {
        //Latvian (Latviešu)
        months: ["Janvāris", "Februāris", "Marts", "Aprīlis ", "Maijs", "Jūnijs", "Jūlijs", "Augusts", "Septembris", "Oktobris", "Novembris", "Decembris"],
        dayOfWeekShort: ["Sv", "Pr", "Ot", "Tr", "Ct", "Pk", "St"],
        dayOfWeek: ["Svētdiena", "Pirmdiena", "Otrdiena", "Trešdiena", "Ceturtdiena", "Piektdiena", "Sestdiena"]
      },
      mk: {
        //Macedonian (Македонски)
        months: ["јануари", "февруари", "март", "април", "мај", "јуни", "јули", "август", "септември", "октомври", "ноември", "декември"],
        dayOfWeekShort: ["нед", "пон", "вто", "сре", "чет", "пет", "саб"],
        dayOfWeek: ["Недела", "Понеделник", "Вторник", "Среда", "Четврток", "Петок", "Сабота"]
      },
      mn: {
        //Mongolian (Монгол)
        months: ["1-р сар", "2-р сар", "3-р сар", "4-р сар", "5-р сар", "6-р сар", "7-р сар", "8-р сар", "9-р сар", "10-р сар", "11-р сар", "12-р сар"],
        dayOfWeekShort: ["Дав", "Мяг", "Лха", "Пүр", "Бсн", "Бям", "Ням"],
        dayOfWeek: ["Даваа", "Мягмар", "Лхагва", "Пүрэв", "Баасан", "Бямба", "Ням"]
      },
      'pt-BR': {
        //Português(Brasil)
        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        dayOfWeekShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
        dayOfWeek: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"]
      },
      sk: {
        //Slovenčina
        months: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
        dayOfWeekShort: ["Ne", "Po", "Ut", "St", "Št", "Pi", "So"],
        dayOfWeek: ["Nedeľa", "Pondelok", "Utorok", "Streda", "Štvrtok", "Piatok", "Sobota"]
      },
      sq: {
        //Albanian (Shqip)
        months: ["Janar", "Shkurt", "Mars", "Prill", "Maj", "Qershor", "Korrik", "Gusht", "Shtator", "Tetor", "Nëntor", "Dhjetor"],
        dayOfWeekShort: ["Die", "Hën", "Mar", "Mër", "Enj", "Pre", "Shtu"],
        dayOfWeek: ["E Diel", "E Hënë", "E Martē", "E Mërkurë", "E Enjte", "E Premte", "E Shtunë"]
      },
      'sr-YU': {
        //Serbian (Srpski)
        months: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
        dayOfWeekShort: ["Ned", "Pon", "Uto", "Sre", "čet", "Pet", "Sub"],
        dayOfWeek: ["Nedelja", "Ponedeljak", "Utorak", "Sreda", "Četvrtak", "Petak", "Subota"]
      },
      sr: {
        //Serbian Cyrillic (Српски)
        months: ["јануар", "фебруар", "март", "април", "мај", "јун", "јул", "август", "септембар", "октобар", "новембар", "децембар"],
        dayOfWeekShort: ["нед", "пон", "уто", "сре", "чет", "пет", "суб"],
        dayOfWeek: ["Недеља", "Понедељак", "Уторак", "Среда", "Четвртак", "Петак", "Субота"]
      },
      sv: {
        //Svenska
        months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
        dayOfWeekShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"],
        dayOfWeek: ["Söndag", "Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag"]
      },
      'zh-TW': {
        //Traditional Chinese (繁體中文)
        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"],
        dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
      },
      zh: {
        //Simplified Chinese (简体中文)
        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"],
        dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
      },
      ug: {
        // Uyghur(ئۇيغۇرچە)
        months: ["1-ئاي", "2-ئاي", "3-ئاي", "4-ئاي", "5-ئاي", "6-ئاي", "7-ئاي", "8-ئاي", "9-ئاي", "10-ئاي", "11-ئاي", "12-ئاي"],
        dayOfWeek: ["يەكشەنبە", "دۈشەنبە", "سەيشەنبە", "چارشەنبە", "پەيشەنبە", "جۈمە", "شەنبە"]
      },
      he: {
        //Hebrew (עברית)
        months: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'],
        dayOfWeekShort: ['א\'', 'ב\'', 'ג\'', 'ד\'', 'ה\'', 'ו\'', 'שבת'],
        dayOfWeek: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת", "ראשון"]
      },
      hy: {
        // Armenian
        months: ["Հունվար", "Փետրվար", "Մարտ", "Ապրիլ", "Մայիս", "Հունիս", "Հուլիս", "Օգոստոս", "Սեպտեմբեր", "Հոկտեմբեր", "Նոյեմբեր", "Դեկտեմբեր"],
        dayOfWeekShort: ["Կի", "Երկ", "Երք", "Չոր", "Հնգ", "Ուրբ", "Շբթ"],
        dayOfWeek: ["Կիրակի", "Երկուշաբթի", "Երեքշաբթի", "Չորեքշաբթի", "Հինգշաբթի", "Ուրբաթ", "Շաբաթ"]
      },
      kg: {
        // Kyrgyz
        months: ['Үчтүн айы', 'Бирдин айы', 'Жалган Куран', 'Чын Куран', 'Бугу', 'Кулжа', 'Теке', 'Баш Оона', 'Аяк Оона', 'Тогуздун айы', 'Жетинин айы', 'Бештин айы'],
        dayOfWeekShort: ["Жек", "Дүй", "Шей", "Шар", "Бей", "Жум", "Ише"],
        dayOfWeek: ["Жекшемб", "Дүйшөмб", "Шейшемб", "Шаршемб", "Бейшемби", "Жума", "Ишенб"]
      },
      rm: {
        // Romansh
        months: ["Schaner", "Favrer", "Mars", "Avrigl", "Matg", "Zercladur", "Fanadur", "Avust", "Settember", "October", "November", "December"],
        dayOfWeekShort: ["Du", "Gli", "Ma", "Me", "Gie", "Ve", "So"],
        dayOfWeek: ["Dumengia", "Glindesdi", "Mardi", "Mesemna", "Gievgia", "Venderdi", "Sonda"]
      },
      ka: {
        // Georgian
        months: ['იანვარი', 'თებერვალი', 'მარტი', 'აპრილი', 'მაისი', 'ივნისი', 'ივლისი', 'აგვისტო', 'სექტემბერი', 'ოქტომბერი', 'ნოემბერი', 'დეკემბერი'],
        dayOfWeekShort: ["კვ", "ორშ", "სამშ", "ოთხ", "ხუთ", "პარ", "შაბ"],
        dayOfWeek: ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი"]
      }
    },
    ownerDocument: document,
    contentWindow: window,
    value: '',
    rtl: false,
    format: 'Y/m/d H:i',
    formatTime: 'H:i',
    formatDate: 'Y/m/d',
    startDate: false,
    // new Date(), '1986/12/08', '-1970/01/05','-1970/01/05',
    step: 60,
    monthChangeSpinner: true,
    closeOnDateSelect: false,
    closeOnTimeSelect: true,
    closeOnWithoutClick: true,
    closeOnInputClick: true,
    openOnFocus: true,
    timepicker: true,
    datepicker: true,
    weeks: false,
    defaultTime: false,
    // use formatTime format (ex. '10:00' for formatTime:	'H:i')
    defaultDate: false,
    // use formatDate format (ex new Date() or '1986/12/08' or '-1970/01/05' or '-1970/01/05')
    minDate: false,
    maxDate: false,
    minTime: false,
    maxTime: false,
    minDateTime: false,
    maxDateTime: false,
    allowTimes: [],
    opened: false,
    initTime: true,
    inline: false,
    theme: '',
    touchMovedThreshold: 5,
    onSelectDate: function onSelectDate() {},
    onSelectTime: function onSelectTime() {},
    onChangeMonth: function onChangeMonth() {},
    onGetWeekOfYear: function onGetWeekOfYear() {},
    onChangeYear: function onChangeYear() {},
    onChangeDateTime: function onChangeDateTime() {},
    onShow: function onShow() {},
    onClose: function onClose() {},
    onGenerate: function onGenerate() {},
    withoutCopyright: true,
    inverseButton: false,
    hours12: false,
    next: 'xdsoft_next',
    prev: 'xdsoft_prev',
    dayOfWeekStart: 0,
    parentID: 'body',
    timeHeightInTimePicker: 25,
    timepickerScrollbar: true,
    todayButton: true,
    prevButton: true,
    nextButton: true,
    defaultSelect: true,
    scrollMonth: true,
    scrollTime: true,
    scrollInput: true,
    lazyInit: false,
    mask: false,
    validateOnBlur: true,
    allowBlank: true,
    yearStart: 1950,
    yearEnd: 2050,
    monthStart: 0,
    monthEnd: 11,
    style: '',
    id: '',
    fixed: false,
    roundTime: 'round',
    // ceil, floor
    className: '',
    weekends: [],
    highlightedDates: [],
    highlightedPeriods: [],
    allowDates: [],
    allowDateRe: null,
    disabledDates: [],
    disabledWeekDays: [],
    yearOffset: 0,
    beforeShowDay: null,
    enterLikeTab: true,
    showApplyButton: false,
    insideParent: false
  };
  var dateHelper = null,
      defaultDateHelper = null,
      globalLocaleDefault = 'en',
      globalLocale = 'en';
  var dateFormatterOptionsDefault = {
    meridiem: ['AM', 'PM']
  };

  var initDateFormatter = function initDateFormatter() {
    var locale = default_options.i18n[globalLocale],
        opts = {
      days: locale.dayOfWeek,
      daysShort: locale.dayOfWeekShort,
      months: locale.months,
      monthsShort: djQ.map(locale.months, function (n) {
        return n.substring(0, 3);
      })
    };

    if (typeof DateFormatter === 'function') {
      dateHelper = defaultDateHelper = new DateFormatter({
        dateSettings: djQ.extend({}, dateFormatterOptionsDefault, opts)
      });
    }
  };

  var dateFormatters = {
    moment: {
      default_options: {
        format: 'YYYY/MM/DD HH:mm',
        formatDate: 'YYYY/MM/DD',
        formatTime: 'HH:mm'
      },
      formatter: {
        parseDate: function parseDate(date, format) {
          if (isFormatStandard(format)) {
            return defaultDateHelper.parseDate(date, format);
          }

          var d = moment(date, format);
          return d.isValid() ? d.toDate() : false;
        },
        formatDate: function formatDate(date, format) {
          if (isFormatStandard(format)) {
            return defaultDateHelper.formatDate(date, format);
          }

          return moment(date).format(format);
        },
        formatMask: function formatMask(format) {
          return format.replace(/Y{4}/g, '9999').replace(/Y{2}/g, '99').replace(/M{2}/g, '19').replace(/D{2}/g, '39').replace(/H{2}/g, '29').replace(/m{2}/g, '59').replace(/s{2}/g, '59');
        }
      }
    }
  }; // for locale settings

  djQ.datetimepicker = {
    setLocale: function setLocale(locale) {
      var newLocale = default_options.i18n[locale] ? locale : globalLocaleDefault;

      if (globalLocale !== newLocale) {
        globalLocale = newLocale; // reinit date formatter

        initDateFormatter();
      }
    },
    setDateFormatter: function setDateFormatter(dateFormatter) {
      if (typeof dateFormatter === 'string' && dateFormatters.hasOwnProperty(dateFormatter)) {
        var df = dateFormatters[dateFormatter];
        djQ.extend(default_options, df.default_options);
        dateHelper = df.formatter;
      } else {
        dateHelper = dateFormatter;
      }
    }
  };
  var standardFormats = {
    RFC_2822: 'D, d M Y H:i:s O',
    ATOM: 'Y-m-d\TH:i:sP',
    ISO_8601: 'Y-m-d\TH:i:sO',
    RFC_822: 'D, d M y H:i:s O',
    RFC_850: 'l, d-M-y H:i:s T',
    RFC_1036: 'D, d M y H:i:s O',
    RFC_1123: 'D, d M Y H:i:s O',
    RSS: 'D, d M Y H:i:s O',
    W3C: 'Y-m-d\TH:i:sP'
  };

  var isFormatStandard = function isFormatStandard(format) {
    return Object.values(standardFormats).indexOf(format) === -1 ? false : true;
  };

  djQ.extend(djQ.datetimepicker, standardFormats); // first init date formatter

  initDateFormatter(); // fix for ie8

  if (!window.getComputedStyle) {
    window.getComputedStyle = function (el) {
      this.el = el;

      this.getPropertyValue = function (prop) {
        var re = /(-([a-z]))/g;

        if (prop === 'float') {
          prop = 'styleFloat';
        }

        if (re.test(prop)) {
          prop = prop.replace(re, function (a, b, c) {
            return c.toUpperCase();
          });
        }

        return el.currentStyle[prop] || null;
      };

      return this;
    };
  }

  if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
      var i, j;

      for (i = start || 0, j = this.length; i < j; i += 1) {
        if (this[i] === obj) {
          return i;
        }
      }

      return -1;
    };
  }

  Date.prototype.countDaysInMonth = function () {
    return new Date(this.getFullYear(), this.getMonth() + 1, 0).getDate();
  };

  djQ.fn.xdsoftScroller = function (options, percent) {
    return this.each(function () {
      var timeboxparent = djQ(this),
          pointerEventToXY = function pointerEventToXY(e) {
        var out = {
          x: 0,
          y: 0
        },
            touch;

        if (e.type === 'touchstart' || e.type === 'touchmove' || e.type === 'touchend' || e.type === 'touchcancel') {
          touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
          out.x = touch.clientX;
          out.y = touch.clientY;
        } else if (e.type === 'mousedown' || e.type === 'mouseup' || e.type === 'mousemove' || e.type === 'mouseover' || e.type === 'mouseout' || e.type === 'mouseenter' || e.type === 'mouseleave') {
          out.x = e.clientX;
          out.y = e.clientY;
        }

        return out;
      },
          getWheelDelta = function getWheelDelta(e) {
        var deltaY = 0;

        if ('detail' in e) {
          deltaY = e.detail;
        }

        if ('wheelDelta' in e) {
          deltaY = -e.wheelDelta / 120;
        }

        if ('wheelDeltaY' in e) {
          deltaY = -e.wheelDeltaY / 120;
        }

        if ('axis' in e && e.axis === e.HORIZONTAL_AXIS) {
          deltaY = 0;
        }

        deltaY *= 10;

        if ('deltaY' in e) {
          deltaY = e.deltaY;
        }

        if (deltaY && e.deltaMode) {
          if (e.deltaMode === 1) {
            deltaY *= 40;
          } else {
            deltaY *= 800;
          }
        }

        return deltaY;
      },
          timebox,
          timeboxTop = 0,
          parentHeight,
          height,
          scrollbar,
          scroller,
          maximumOffset = 100,
          start = false,
          startY = 0,
          startTop = 0,
          h1 = 0,
          touchStart = false,
          startTopScroll = 0,
          calcOffset = function calcOffset() {};

      if (percent === 'hide') {
        timeboxparent.find('.xdsoft_scrollbar').hide();
        return;
      }

      if (!djQ(this).hasClass('xdsoft_scroller_box')) {
        timebox = timeboxparent.children().eq(0);
        timeboxTop = Math.abs(parseInt(timebox.css('marginTop'), 10));
        parentHeight = timeboxparent[0].clientHeight;
        height = timebox[0].offsetHeight;
        scrollbar = djQ('<div class="xdsoft_scrollbar"></div>');
        scroller = djQ('<div class="xdsoft_scroller"></div>');
        scrollbar.append(scroller);
        timeboxparent.addClass('xdsoft_scroller_box').append(scrollbar);

        calcOffset = function calcOffset(event) {
          var offset = pointerEventToXY(event).y - startY + startTopScroll;

          if (offset < 0) {
            offset = 0;
          }

          if (offset + scroller[0].offsetHeight > h1) {
            offset = h1 - scroller[0].offsetHeight;
          }

          timeboxparent.trigger('scroll_element.xdsoft_scroller', [maximumOffset ? offset / maximumOffset : 0]);
        };

        scroller.on('touchstart.xdsoft_scroller mousedown.xdsoft_scroller', function (event) {
          if (!parentHeight) {
            timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
          }

          startY = pointerEventToXY(event).y;
          startTopScroll = parseInt(scroller.css('marginTop'), 10);
          h1 = scrollbar[0].offsetHeight;

          if (event.type === 'mousedown' || event.type === 'touchstart') {
            if (options.ownerDocument) {
              djQ(options.ownerDocument.body).addClass('xdsoft_noselect');
            }

            djQ([options.ownerDocument.body, options.contentWindow]).on('touchend mouseup.xdsoft_scroller', function arguments_callee() {
              djQ([options.ownerDocument.body, options.contentWindow]).off('touchend mouseup.xdsoft_scroller', arguments_callee).off('mousemove.xdsoft_scroller', calcOffset).removeClass('xdsoft_noselect');
            });
            djQ(options.ownerDocument.body).on('mousemove.xdsoft_scroller', calcOffset);
          } else {
            touchStart = true;
            event.stopPropagation();
            event.preventDefault();
          }
        }).on('touchmove', function (event) {
          if (touchStart) {
            event.preventDefault();
            calcOffset(event);
          }
        }).on('touchend touchcancel', function () {
          touchStart = false;
          startTopScroll = 0;
        });
        timeboxparent.on('scroll_element.xdsoft_scroller', function (event, percentage) {
          if (!parentHeight) {
            timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percentage, true]);
          }

          percentage = percentage > 1 ? 1 : percentage < 0 || isNaN(percentage) ? 0 : percentage;
          timeboxTop = parseFloat(Math.abs((timebox[0].offsetHeight - parentHeight) * percentage).toFixed(4));
          scroller.css('marginTop', maximumOffset * percentage);
          timebox.css('marginTop', -timeboxTop);
        }).on('resize_scroll.xdsoft_scroller', function (event, percentage, noTriggerScroll) {
          var percent, sh;
          parentHeight = timeboxparent[0].clientHeight;
          height = timebox[0].offsetHeight;
          percent = parentHeight / height;
          sh = percent * scrollbar[0].offsetHeight;

          if (percent > 1) {
            scroller.hide();
          } else {
            scroller.show();
            scroller.css('height', parseInt(sh > 10 ? sh : 10, 10));
            maximumOffset = scrollbar[0].offsetHeight - scroller[0].offsetHeight;

            if (noTriggerScroll !== true) {
              timeboxparent.trigger('scroll_element.xdsoft_scroller', [percentage || timeboxTop / (height - parentHeight)]);
            }
          }
        });
        timeboxparent.on('mousewheel', function (event) {
          var deltaY = getWheelDelta(event.originalEvent);
          var top = Math.max(0, timeboxTop - deltaY);
          timeboxparent.trigger('scroll_element.xdsoft_scroller', [top / (height - parentHeight)]);
          event.stopPropagation();
          return false;
        });
        timeboxparent.on('touchstart', function (event) {
          start = pointerEventToXY(event);
          startTop = timeboxTop;
        });
        timeboxparent.on('touchmove', function (event) {
          if (start) {
            event.preventDefault();
            var coord = pointerEventToXY(event);
            timeboxparent.trigger('scroll_element.xdsoft_scroller', [(startTop - (coord.y - start.y)) / (height - parentHeight)]);
          }
        });
        timeboxparent.on('touchend touchcancel', function () {
          start = false;
          startTop = 0;
        });
      }

      timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
    });
  };

  djQ.fn.datetimepicker = function (opt, opt2) {
    var result = this,
        KEY0 = 48,
        KEY9 = 57,
        _KEY0 = 96,
        _KEY9 = 105,
        CTRLKEY = 17,
        CMDKEY = 91,
        DEL = 46,
        ENTER = 13,
        ESC = 27,
        BACKSPACE = 8,
        ARROWLEFT = 37,
        ARROWUP = 38,
        ARROWRIGHT = 39,
        ARROWDOWN = 40,
        TAB = 9,
        F5 = 116,
        AKEY = 65,
        CKEY = 67,
        VKEY = 86,
        ZKEY = 90,
        YKEY = 89,
        ctrlDown = false,
        cmdDown = false,
        options = djQ.isPlainObject(opt) || !opt ? djQ.extend(true, {}, default_options, opt) : djQ.extend(true, {}, default_options),
        lazyInitTimer = 0,
        createDateTimePicker,
        destroyDateTimePicker,
        lazyInit = function lazyInit(input) {
      input.on('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', function initOnActionCallback() {
        if (input.is(':disabled') || input.data('xdsoft_datetimepicker')) {
          return;
        }

        clearTimeout(lazyInitTimer);
        lazyInitTimer = setTimeout(function () {
          if (!input.data('xdsoft_datetimepicker')) {
            createDateTimePicker(input);
          }

          input.off('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', initOnActionCallback).trigger('open.xdsoft');
        }, 100);
      });
    };

    createDateTimePicker = function createDateTimePicker(input) {
      var datetimepicker = djQ('<div class="xdsoft_datetimepicker xdsoft_noselect"></div>'),
          xdsoft_copyright = djQ('<div class="xdsoft_copyright"><a target="_blank" href="http://xdsoft.net/jqplugins/datetimepicker/">xdsoft.net</a></div>'),
          datepicker = djQ('<div class="xdsoft_datepicker active"></div>'),
          month_picker = djQ('<div class="xdsoft_monthpicker"><button type="button" class="xdsoft_prev"></button><button type="button" class="xdsoft_today_button"></button>' + '<div class="xdsoft_label xdsoft_month"><span></span><i></i></div>' + '<div class="xdsoft_label xdsoft_year"><span></span><i></i></div>' + '<button type="button" class="xdsoft_next"></button></div>'),
          calendar = djQ('<div class="xdsoft_calendar"></div>'),
          timepicker = djQ('<div class="xdsoft_timepicker active"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_time_box"></div><button type="button" class="xdsoft_next"></button></div>'),
          timeboxparent = timepicker.find('.xdsoft_time_box').eq(0),
          timebox = djQ('<div class="xdsoft_time_variant"></div>'),
          applyButton = djQ('<button type="button" class="xdsoft_save_selected blue-gradient-button">Save Selected</button>'),
          monthselect = djQ('<div class="xdsoft_select xdsoft_monthselect"><div></div></div>'),
          yearselect = djQ('<div class="xdsoft_select xdsoft_yearselect"><div></div></div>'),
          triggerAfterOpen = false,
          XDSoft_datetime,
          xchangeTimer,
          timerclick,
          current_time_index,
          setPos,
          timer = 0,
          _xdsoft_datetime,
          forEachAncestorOf;

      if (options.id) {
        datetimepicker.attr('id', options.id);
      }

      if (options.style) {
        datetimepicker.attr('style', options.style);
      }

      if (options.weeks) {
        datetimepicker.addClass('xdsoft_showweeks');
      }

      if (options.rtl) {
        datetimepicker.addClass('xdsoft_rtl');
      }

      datetimepicker.addClass('xdsoft_' + options.theme);
      datetimepicker.addClass(options.className);
      month_picker.find('.xdsoft_month span').after(monthselect);
      month_picker.find('.xdsoft_year span').after(yearselect);
      month_picker.find('.xdsoft_month,.xdsoft_year').on('touchstart mousedown.xdsoft', function (event) {
        var select = djQ(this).find('.xdsoft_select').eq(0),
            val = 0,
            top = 0,
            visible = select.is(':visible'),
            items,
            i;
        month_picker.find('.xdsoft_select').hide();

        if (_xdsoft_datetime.currentTime) {
          val = _xdsoft_datetime.currentTime[djQ(this).hasClass('xdsoft_month') ? 'getMonth' : 'getFullYear']();
        }

        select[visible ? 'hide' : 'show']();

        for (items = select.find('div.xdsoft_option'), i = 0; i < items.length; i += 1) {
          if (items.eq(i).data('value') === val) {
            break;
          } else {
            top += items[0].offsetHeight;
          }
        }

        select.xdsoftScroller(options, top / (select.children()[0].offsetHeight - select[0].clientHeight));
        event.stopPropagation();
        return false;
      });

      var handleTouchMoved = function handleTouchMoved(event) {
        var evt = event.originalEvent;
        var touchPosition = evt.touches ? evt.touches[0] : evt;
        this.touchStartPosition = this.touchStartPosition || touchPosition;
        var xMovement = Math.abs(this.touchStartPosition.clientX - touchPosition.clientX);
        var yMovement = Math.abs(this.touchStartPosition.clientY - touchPosition.clientY);
        var distance = Math.sqrt(xMovement * xMovement + yMovement * yMovement);

        if (distance > options.touchMovedThreshold) {
          this.touchMoved = true;
        }
      };

      month_picker.find('.xdsoft_select').xdsoftScroller(options).on('touchstart mousedown.xdsoft', function (event) {
        var evt = event.originalEvent;
        this.touchMoved = false;
        this.touchStartPosition = evt.touches ? evt.touches[0] : evt;
        event.stopPropagation();
        event.preventDefault();
      }).on('touchmove', '.xdsoft_option', handleTouchMoved).on('touchend mousedown.xdsoft', '.xdsoft_option', function () {
        if (!this.touchMoved) {
          if (_xdsoft_datetime.currentTime === undefined || _xdsoft_datetime.currentTime === null) {
            _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
          }

          var year = _xdsoft_datetime.currentTime.getFullYear();

          if (_xdsoft_datetime && _xdsoft_datetime.currentTime) {
            _xdsoft_datetime.currentTime[djQ(this).parent().parent().hasClass('xdsoft_monthselect') ? 'setMonth' : 'setFullYear'](djQ(this).data('value'));
          }

          djQ(this).parent().parent().hide();
          datetimepicker.trigger('xchange.xdsoft');

          if (options.onChangeMonth && djQ.isFunction(options.onChangeMonth)) {
            options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
          }

          if (year !== _xdsoft_datetime.currentTime.getFullYear() && djQ.isFunction(options.onChangeYear)) {
            options.onChangeYear.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
          }
        }
      });

      datetimepicker.getValue = function () {
        return _xdsoft_datetime.getCurrentTime();
      };

      datetimepicker.setOptions = function (_options) {
        var highlightedDates = {};
        options = djQ.extend(true, {}, options, _options);

        if (_options.allowTimes && djQ.isArray(_options.allowTimes) && _options.allowTimes.length) {
          options.allowTimes = djQ.extend(true, [], _options.allowTimes);
        }

        if (_options.weekends && djQ.isArray(_options.weekends) && _options.weekends.length) {
          options.weekends = djQ.extend(true, [], _options.weekends);
        }

        if (_options.allowDates && djQ.isArray(_options.allowDates) && _options.allowDates.length) {
          options.allowDates = djQ.extend(true, [], _options.allowDates);
        }

        if (_options.allowDateRe && Object.prototype.toString.call(_options.allowDateRe) === "[object String]") {
          options.allowDateRe = new RegExp(_options.allowDateRe);
        }

        if (_options.highlightedDates && djQ.isArray(_options.highlightedDates) && _options.highlightedDates.length) {
          djQ.each(_options.highlightedDates, function (index, value) {
            var splitData = djQ.map(value.split(','), djQ.trim),
                exDesc,
                hDate = new HighlightedDate(dateHelper.parseDate(splitData[0], options.formatDate), splitData[1], splitData[2]),
                // date, desc, style
            keyDate = dateHelper.formatDate(hDate.date, options.formatDate);

            if (highlightedDates[keyDate] !== undefined) {
              exDesc = highlightedDates[keyDate].desc;

              if (exDesc && exDesc.length && hDate.desc && hDate.desc.length) {
                highlightedDates[keyDate].desc = exDesc + "\n" + hDate.desc;
              }
            } else {
              highlightedDates[keyDate] = hDate;
            }
          });
          options.highlightedDates = djQ.extend(true, [], highlightedDates);
        }

        if (_options.highlightedPeriods && djQ.isArray(_options.highlightedPeriods) && _options.highlightedPeriods.length) {
          highlightedDates = djQ.extend(true, [], options.highlightedDates);
          djQ.each(_options.highlightedPeriods, function (index, value) {
            var dateTest, // start date
            dateEnd, desc, hDate, keyDate, exDesc, style;

            if (djQ.isArray(value)) {
              dateTest = value[0];
              dateEnd = value[1];
              desc = value[2];
              style = value[3];
            } else {
              var splitData = djQ.map(value.split(','), djQ.trim);
              dateTest = dateHelper.parseDate(splitData[0], options.formatDate);
              dateEnd = dateHelper.parseDate(splitData[1], options.formatDate);
              desc = splitData[2];
              style = splitData[3];
            }

            while (dateTest <= dateEnd) {
              hDate = new HighlightedDate(dateTest, desc, style);
              keyDate = dateHelper.formatDate(dateTest, options.formatDate);
              dateTest.setDate(dateTest.getDate() + 1);

              if (highlightedDates[keyDate] !== undefined) {
                exDesc = highlightedDates[keyDate].desc;

                if (exDesc && exDesc.length && hDate.desc && hDate.desc.length) {
                  highlightedDates[keyDate].desc = exDesc + "\n" + hDate.desc;
                }
              } else {
                highlightedDates[keyDate] = hDate;
              }
            }
          });
          options.highlightedDates = djQ.extend(true, [], highlightedDates);
        }

        if (_options.disabledDates && djQ.isArray(_options.disabledDates) && _options.disabledDates.length) {
          options.disabledDates = djQ.extend(true, [], _options.disabledDates);
        }

        if (_options.disabledWeekDays && djQ.isArray(_options.disabledWeekDays) && _options.disabledWeekDays.length) {
          options.disabledWeekDays = djQ.extend(true, [], _options.disabledWeekDays);
        }

        if ((options.open || options.opened) && !options.inline) {
          input.trigger('open.xdsoft');
        }

        if (options.inline) {
          triggerAfterOpen = true;
          datetimepicker.addClass('xdsoft_inline');
          input.after(datetimepicker).hide();
        }

        if (options.inverseButton) {
          options.next = 'xdsoft_prev';
          options.prev = 'xdsoft_next';
        }

        if (options.datepicker) {
          datepicker.addClass('active');
        } else {
          datepicker.removeClass('active');
        }

        if (options.timepicker) {
          timepicker.addClass('active');
        } else {
          timepicker.removeClass('active');
        }

        if (options.value) {
          _xdsoft_datetime.setCurrentTime(options.value);

          if (input && input.val) {
            input.val(_xdsoft_datetime.str);
          }
        }

        if (isNaN(options.dayOfWeekStart)) {
          options.dayOfWeekStart = 0;
        } else {
          options.dayOfWeekStart = parseInt(options.dayOfWeekStart, 10) % 7;
        }

        if (!options.timepickerScrollbar) {
          timeboxparent.xdsoftScroller(options, 'hide');
        }

        if (options.minDate && /^[\+\-](.*)$/.test(options.minDate)) {
          options.minDate = dateHelper.formatDate(_xdsoft_datetime.strToDateTime(options.minDate), options.formatDate);
        }

        if (options.maxDate && /^[\+\-](.*)$/.test(options.maxDate)) {
          options.maxDate = dateHelper.formatDate(_xdsoft_datetime.strToDateTime(options.maxDate), options.formatDate);
        }

        if (options.minDateTime && /^\+(.*)$/.test(options.minDateTime)) {
          options.minDateTime = _xdsoft_datetime.strToDateTime(options.minDateTime).dateFormat(options.formatDate);
        }

        if (options.maxDateTime && /^\+(.*)$/.test(options.maxDateTime)) {
          options.maxDateTime = _xdsoft_datetime.strToDateTime(options.maxDateTime).dateFormat(options.formatDate);
        }

        applyButton.toggle(options.showApplyButton);
        month_picker.find('.xdsoft_today_button').css('visibility', !options.todayButton ? 'hidden' : 'visible');
        month_picker.find('.' + options.prev).css('visibility', !options.prevButton ? 'hidden' : 'visible');
        month_picker.find('.' + options.next).css('visibility', !options.nextButton ? 'hidden' : 'visible');
        setMask(options);

        if (options.validateOnBlur) {
          input.off('blur.xdsoft').on('blur.xdsoft', function () {
            if (options.allowBlank && (!djQ.trim(djQ(this).val()).length || typeof options.mask === "string" && djQ.trim(djQ(this).val()) === options.mask.replace(/[0-9]/g, '_'))) {
              djQ(this).val(null);
              datetimepicker.data('xdsoft_datetime').empty();
            } else {
              var d = dateHelper.parseDate(djQ(this).val(), options.format);

              if (d) {
                // parseDate() may skip some invalid parts like date or time, so make it clear for user: show parsed date/time
                djQ(this).val(dateHelper.formatDate(d, options.format));
              } else {
                var splittedHours = +[djQ(this).val()[0], djQ(this).val()[1]].join(''),
                    splittedMinutes = +[djQ(this).val()[2], djQ(this).val()[3]].join(''); // parse the numbers as 0312 => 03:12

                if (!options.datepicker && options.timepicker && splittedHours >= 0 && splittedHours < 24 && splittedMinutes >= 0 && splittedMinutes < 60) {
                  djQ(this).val([splittedHours, splittedMinutes].map(function (item) {
                    return item > 9 ? item : '0' + item;
                  }).join(':'));
                } else {
                  djQ(this).val(dateHelper.formatDate(_xdsoft_datetime.now(), options.format));
                }
              }

              datetimepicker.data('xdsoft_datetime').setCurrentTime(djQ(this).val());
            }

            datetimepicker.trigger('changedatetime.xdsoft');
            datetimepicker.trigger('close.xdsoft');
          });
        }

        options.dayOfWeekStartPrev = options.dayOfWeekStart === 0 ? 6 : options.dayOfWeekStart - 1;
        datetimepicker.trigger('xchange.xdsoft').trigger('afterOpen.xdsoft');
      };

      datetimepicker.data('options', options).on('touchstart mousedown.xdsoft', function (event) {
        event.stopPropagation();
        event.preventDefault();
        yearselect.hide();
        monthselect.hide();
        return false;
      }); //scroll_element = timepicker.find('.xdsoft_time_box');

      timeboxparent.append(timebox);
      timeboxparent.xdsoftScroller(options);
      datetimepicker.on('afterOpen.xdsoft', function () {
        timeboxparent.xdsoftScroller(options);
      });
      datetimepicker.append(datepicker).append(timepicker);

      if (options.withoutCopyright !== true) {
        datetimepicker.append(xdsoft_copyright);
      }

      datepicker.append(month_picker).append(calendar).append(applyButton);

      if (options.insideParent) {
        djQ(input).parent().append(datetimepicker);
      } else {
        djQ(options.parentID).append(datetimepicker);
      }

      XDSoft_datetime = function XDSoft_datetime() {
        var _this = this;

        _this.now = function (norecursion) {
          var d = new Date(),
              date,
              time;

          if (!norecursion && options.defaultDate) {
            date = _this.strToDateTime(options.defaultDate);
            d.setFullYear(date.getFullYear());
            d.setMonth(date.getMonth());
            d.setDate(date.getDate());
          }

          d.setFullYear(d.getFullYear());

          if (!norecursion && options.defaultTime) {
            time = _this.strtotime(options.defaultTime);
            d.setHours(time.getHours());
            d.setMinutes(time.getMinutes());
            d.setSeconds(time.getSeconds());
            d.setMilliseconds(time.getMilliseconds());
          }

          return d;
        };

        _this.isValidDate = function (d) {
          if (Object.prototype.toString.call(d) !== "[object Date]") {
            return false;
          }

          return !isNaN(d.getTime());
        };

        _this.setCurrentTime = function (dTime, requireValidDate) {
          if (typeof dTime === 'string') {
            _this.currentTime = _this.strToDateTime(dTime);
          } else if (_this.isValidDate(dTime)) {
            _this.currentTime = dTime;
          } else if (!dTime && !requireValidDate && options.allowBlank && !options.inline) {
            _this.currentTime = null;
          } else {
            _this.currentTime = _this.now();
          }

          datetimepicker.trigger('xchange.xdsoft');
        };

        _this.empty = function () {
          _this.currentTime = null;
        };

        _this.getCurrentTime = function () {
          return _this.currentTime;
        };

        _this.nextMonth = function () {
          if (_this.currentTime === undefined || _this.currentTime === null) {
            _this.currentTime = _this.now();
          }

          var month = _this.currentTime.getMonth() + 1,
              year;

          if (month === 12) {
            _this.currentTime.setFullYear(_this.currentTime.getFullYear() + 1);

            month = 0;
          }

          year = _this.currentTime.getFullYear();

          _this.currentTime.setDate(Math.min(new Date(_this.currentTime.getFullYear(), month + 1, 0).getDate(), _this.currentTime.getDate()));

          _this.currentTime.setMonth(month);

          if (options.onChangeMonth && djQ.isFunction(options.onChangeMonth)) {
            options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
          }

          if (year !== _this.currentTime.getFullYear() && djQ.isFunction(options.onChangeYear)) {
            options.onChangeYear.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
          }

          datetimepicker.trigger('xchange.xdsoft');
          return month;
        };

        _this.prevMonth = function () {
          if (_this.currentTime === undefined || _this.currentTime === null) {
            _this.currentTime = _this.now();
          }

          var month = _this.currentTime.getMonth() - 1;

          if (month === -1) {
            _this.currentTime.setFullYear(_this.currentTime.getFullYear() - 1);

            month = 11;
          }

          _this.currentTime.setDate(Math.min(new Date(_this.currentTime.getFullYear(), month + 1, 0).getDate(), _this.currentTime.getDate()));

          _this.currentTime.setMonth(month);

          if (options.onChangeMonth && djQ.isFunction(options.onChangeMonth)) {
            options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
          }

          datetimepicker.trigger('xchange.xdsoft');
          return month;
        };

        _this.getWeekOfYear = function (datetime) {
          if (options.onGetWeekOfYear && djQ.isFunction(options.onGetWeekOfYear)) {
            var week = options.onGetWeekOfYear.call(datetimepicker, datetime);

            if (typeof week !== 'undefined') {
              return week;
            }
          }

          var onejan = new Date(datetime.getFullYear(), 0, 1); //First week of the year is th one with the first Thursday according to ISO8601

          if (onejan.getDay() !== 4) {
            onejan.setMonth(0, 1 + (4 - onejan.getDay() + 7) % 7);
          }

          return Math.ceil(((datetime - onejan) / 86400000 + onejan.getDay() + 1) / 7);
        };

        _this.strToDateTime = function (sDateTime) {
          var tmpDate = [],
              timeOffset,
              currentTime;

          if (sDateTime && sDateTime instanceof Date && _this.isValidDate(sDateTime)) {
            return sDateTime;
          }

          tmpDate = /^([+-]{1})(.*)$/.exec(sDateTime);

          if (tmpDate) {
            tmpDate[2] = dateHelper.parseDate(tmpDate[2], options.formatDate);
          }

          if (tmpDate && tmpDate[2]) {
            timeOffset = tmpDate[2].getTime() - tmpDate[2].getTimezoneOffset() * 60000;
            currentTime = new Date(_this.now(true).getTime() + parseInt(tmpDate[1] + '1', 10) * timeOffset);
          } else {
            currentTime = sDateTime ? dateHelper.parseDate(sDateTime, options.format) : _this.now();
          }

          if (!_this.isValidDate(currentTime)) {
            currentTime = _this.now(true);
          }

          return currentTime;
        };

        _this.strToDate = function (sDate) {
          if (sDate && sDate instanceof Date && _this.isValidDate(sDate)) {
            return sDate;
          }

          var currentTime = sDate ? dateHelper.parseDate(sDate, options.formatDate) : _this.now(true);

          if (!_this.isValidDate(currentTime)) {
            currentTime = _this.now(true);
          }

          return currentTime;
        };

        _this.strtotime = function (sTime) {
          if (sTime && sTime instanceof Date && _this.isValidDate(sTime)) {
            return sTime;
          }

          var currentTime = sTime ? dateHelper.parseDate(sTime, options.formatTime) : _this.now(true);

          if (!_this.isValidDate(currentTime)) {
            currentTime = _this.now(true);
          }

          return currentTime;
        };

        _this.str = function () {
          var format = options.format;

          if (options.yearOffset) {
            format = format.replace('Y', _this.currentTime.getFullYear() + options.yearOffset);
            format = format.replace('y', String(_this.currentTime.getFullYear() + options.yearOffset).substring(2, 4));
          }

          return dateHelper.formatDate(_this.currentTime, format);
        };

        _this.currentTime = this.now();
      };

      _xdsoft_datetime = new XDSoft_datetime();
      applyButton.on('touchend click', function (e) {
        //pathbrite
        e.preventDefault();
        datetimepicker.data('changed', true);

        _xdsoft_datetime.setCurrentTime(getCurrentValue());

        input.val(_xdsoft_datetime.str());
        datetimepicker.trigger('close.xdsoft');
      });
      month_picker.find('.xdsoft_today_button').on('touchend mousedown.xdsoft', function () {
        datetimepicker.data('changed', true);

        _xdsoft_datetime.setCurrentTime(0, true);

        datetimepicker.trigger('afterOpen.xdsoft');
      }).on('dblclick.xdsoft', function () {
        var currentDate = _xdsoft_datetime.getCurrentTime(),
            minDate,
            maxDate;

        currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
        minDate = _xdsoft_datetime.strToDate(options.minDate);
        minDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate());

        if (currentDate < minDate) {
          return;
        }

        maxDate = _xdsoft_datetime.strToDate(options.maxDate);
        maxDate = new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate());

        if (currentDate > maxDate) {
          return;
        }

        input.val(_xdsoft_datetime.str());
        input.trigger('change');
        datetimepicker.trigger('close.xdsoft');
      });
      month_picker.find('.xdsoft_prev,.xdsoft_next').on('touchend mousedown.xdsoft', function () {
        var $this = djQ(this),
            timer = 0,
            stop = false;

        (function arguments_callee1(v) {
          if ($this.hasClass(options.next)) {
            _xdsoft_datetime.nextMonth();
          } else if ($this.hasClass(options.prev)) {
            _xdsoft_datetime.prevMonth();
          }

          if (options.monthChangeSpinner) {
            if (!stop) {
              timer = setTimeout(arguments_callee1, v || 100);
            }
          }
        })(500);

        djQ([options.ownerDocument.body, options.contentWindow]).on('touchend mouseup.xdsoft', function arguments_callee2() {
          clearTimeout(timer);
          stop = true;
          djQ([options.ownerDocument.body, options.contentWindow]).off('touchend mouseup.xdsoft', arguments_callee2);
        });
      });
      timepicker.find('.xdsoft_prev,.xdsoft_next').on('touchend mousedown.xdsoft', function () {
        var $this = djQ(this),
            timer = 0,
            stop = false,
            period = 110;

        (function arguments_callee4(v) {
          var pheight = timeboxparent[0].clientHeight,
              height = timebox[0].offsetHeight,
              top = Math.abs(parseInt(timebox.css('marginTop'), 10));

          if ($this.hasClass(options.next) && height - pheight - options.timeHeightInTimePicker >= top) {
            timebox.css('marginTop', '-' + (top + options.timeHeightInTimePicker) + 'px');
          } else if ($this.hasClass(options.prev) && top - options.timeHeightInTimePicker >= 0) {
            timebox.css('marginTop', '-' + (top - options.timeHeightInTimePicker) + 'px');
          }
          /**
           * Fixed bug:
           * When using css3 transition, it will cause a bug that you cannot scroll the timepicker list.
           * The reason is that the transition-duration time, if you set it to 0, all things fine, otherwise, this
           * would cause a bug when you use jquery.css method.
           * Let's say: * { transition: all .5s ease; }
           * jquery timebox.css('marginTop') will return the original value which is before you clicking the next/prev button,
           * meanwhile the timebox[0].style.marginTop will return the right value which is after you clicking the
           * next/prev button.
           *
           * What we should do:
           * Replace timebox.css('marginTop') with timebox[0].style.marginTop.
           */


          timeboxparent.trigger('scroll_element.xdsoft_scroller', [Math.abs(parseInt(timebox[0].style.marginTop, 10) / (height - pheight))]);
          period = period > 10 ? 10 : period - 10;

          if (!stop) {
            timer = setTimeout(arguments_callee4, v || period);
          }
        })(500);

        djQ([options.ownerDocument.body, options.contentWindow]).on('touchend mouseup.xdsoft', function arguments_callee5() {
          clearTimeout(timer);
          stop = true;
          djQ([options.ownerDocument.body, options.contentWindow]).off('touchend mouseup.xdsoft', arguments_callee5);
        });
      });
      xchangeTimer = 0; // base handler - generating a calendar and timepicker

      datetimepicker.on('xchange.xdsoft', function (event) {
        clearTimeout(xchangeTimer);
        xchangeTimer = setTimeout(function () {
          if (_xdsoft_datetime.currentTime === undefined || _xdsoft_datetime.currentTime === null) {
            _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
          }

          var table = '',
              start = new Date(_xdsoft_datetime.currentTime.getFullYear(), _xdsoft_datetime.currentTime.getMonth(), 1, 12, 0, 0),
              i = 0,
              j,
              today = _xdsoft_datetime.now(),
              maxDate = false,
              minDate = false,
              minDateTime = false,
              maxDateTime = false,
              hDate,
              day,
              d,
              y,
              m,
              w,
              classes = [],
              customDateSettings,
              newRow = true,
              time = '',
              h,
              line_time,
              description;

          while (start.getDay() !== options.dayOfWeekStart) {
            start.setDate(start.getDate() - 1);
          }

          table += '<table><thead><tr>';

          if (options.weeks) {
            table += '<th></th>';
          }

          for (j = 0; j < 7; j += 1) {
            table += '<th>' + options.i18n[globalLocale].dayOfWeekShort[(j + options.dayOfWeekStart) % 7] + '</th>';
          }

          table += '</tr></thead>';
          table += '<tbody>';

          if (options.maxDate !== false) {
            maxDate = _xdsoft_datetime.strToDate(options.maxDate);
            maxDate = new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate(), 23, 59, 59, 999);
          }

          if (options.minDate !== false) {
            minDate = _xdsoft_datetime.strToDate(options.minDate);
            minDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate());
          }

          if (options.minDateTime !== false) {
            minDateTime = _xdsoft_datetime.strToDate(options.minDateTime);
            minDateTime = new Date(minDateTime.getFullYear(), minDateTime.getMonth(), minDateTime.getDate(), minDateTime.getHours(), minDateTime.getMinutes(), minDateTime.getSeconds());
          }

          if (options.maxDateTime !== false) {
            maxDateTime = _xdsoft_datetime.strToDate(options.maxDateTime);
            maxDateTime = new Date(maxDateTime.getFullYear(), maxDateTime.getMonth(), maxDateTime.getDate(), maxDateTime.getHours(), maxDateTime.getMinutes(), maxDateTime.getSeconds());
          }

          var maxDateTimeDay;

          if (maxDateTime !== false) {
            maxDateTimeDay = (maxDateTime.getFullYear() * 12 + maxDateTime.getMonth()) * 31 + maxDateTime.getDate();
          }

          while (i < _xdsoft_datetime.currentTime.countDaysInMonth() || start.getDay() !== options.dayOfWeekStart || _xdsoft_datetime.currentTime.getMonth() === start.getMonth()) {
            classes = [];
            i += 1;
            day = start.getDay();
            d = start.getDate();
            y = start.getFullYear();
            m = start.getMonth();
            w = _xdsoft_datetime.getWeekOfYear(start);
            description = '';
            classes.push('xdsoft_date');

            if (options.beforeShowDay && djQ.isFunction(options.beforeShowDay.call)) {
              customDateSettings = options.beforeShowDay.call(datetimepicker, start);
            } else {
              customDateSettings = null;
            }

            if (options.allowDateRe && Object.prototype.toString.call(options.allowDateRe) === "[object RegExp]") {
              if (!options.allowDateRe.test(dateHelper.formatDate(start, options.formatDate))) {
                classes.push('xdsoft_disabled');
              }
            }

            if (options.allowDates && options.allowDates.length > 0) {
              if (options.allowDates.indexOf(dateHelper.formatDate(start, options.formatDate)) === -1) {
                classes.push('xdsoft_disabled');
              }
            }

            var currentDay = (start.getFullYear() * 12 + start.getMonth()) * 31 + start.getDate();

            if (maxDate !== false && start > maxDate || minDateTime !== false && start < minDateTime || minDate !== false && start < minDate || maxDateTime !== false && currentDay > maxDateTimeDay || customDateSettings && customDateSettings[0] === false) {
              classes.push('xdsoft_disabled');
            }

            if (options.disabledDates.indexOf(dateHelper.formatDate(start, options.formatDate)) !== -1) {
              classes.push('xdsoft_disabled');
            }

            if (options.disabledWeekDays.indexOf(day) !== -1) {
              classes.push('xdsoft_disabled');
            }

            if (input.is('[disabled]')) {
              classes.push('xdsoft_disabled');
            }

            if (customDateSettings && customDateSettings[1] !== "") {
              classes.push(customDateSettings[1]);
            }

            if (_xdsoft_datetime.currentTime.getMonth() !== m) {
              classes.push('xdsoft_other_month');
            }

            if ((options.defaultSelect || datetimepicker.data('changed')) && dateHelper.formatDate(_xdsoft_datetime.currentTime, options.formatDate) === dateHelper.formatDate(start, options.formatDate)) {
              classes.push('xdsoft_current');
            }

            if (dateHelper.formatDate(today, options.formatDate) === dateHelper.formatDate(start, options.formatDate)) {
              classes.push('xdsoft_today');
            }

            if (start.getDay() === 0 || start.getDay() === 6 || options.weekends.indexOf(dateHelper.formatDate(start, options.formatDate)) !== -1) {
              classes.push('xdsoft_weekend');
            }

            if (options.highlightedDates[dateHelper.formatDate(start, options.formatDate)] !== undefined) {
              hDate = options.highlightedDates[dateHelper.formatDate(start, options.formatDate)];
              classes.push(hDate.style === undefined ? 'xdsoft_highlighted_default' : hDate.style);
              description = hDate.desc === undefined ? '' : hDate.desc;
            }

            if (options.beforeShowDay && djQ.isFunction(options.beforeShowDay)) {
              classes.push(options.beforeShowDay(start));
            }

            if (newRow) {
              table += '<tr>';
              newRow = false;

              if (options.weeks) {
                table += '<th>' + w + '</th>';
              }
            }

            table += '<td data-date="' + d + '" data-month="' + m + '" data-year="' + y + '"' + ' class="xdsoft_date xdsoft_day_of_week' + start.getDay() + ' ' + classes.join(' ') + '" title="' + description + '">' + '<div>' + d + '</div>' + '</td>';

            if (start.getDay() === options.dayOfWeekStartPrev) {
              table += '</tr>';
              newRow = true;
            }

            start.setDate(d + 1);
          }

          table += '</tbody></table>';
          calendar.html(table);
          month_picker.find('.xdsoft_label span').eq(0).text(options.i18n[globalLocale].months[_xdsoft_datetime.currentTime.getMonth()]);
          month_picker.find('.xdsoft_label span').eq(1).text(_xdsoft_datetime.currentTime.getFullYear() + options.yearOffset); // generate timebox

          time = '';
          h = '';
          m = '';
          var minTimeMinutesOfDay = 0;

          if (options.minTime !== false) {
            var t = _xdsoft_datetime.strtotime(options.minTime);

            minTimeMinutesOfDay = 60 * t.getHours() + t.getMinutes();
          }

          var maxTimeMinutesOfDay = 24 * 60;

          if (options.maxTime !== false) {
            var t = _xdsoft_datetime.strtotime(options.maxTime);

            maxTimeMinutesOfDay = 60 * t.getHours() + t.getMinutes();
          }

          if (options.minDateTime !== false) {
            var t = _xdsoft_datetime.strToDateTime(options.minDateTime);

            var currentDayIsMinDateTimeDay = dateHelper.formatDate(_xdsoft_datetime.currentTime, options.formatDate) === dateHelper.formatDate(t, options.formatDate);

            if (currentDayIsMinDateTimeDay) {
              var m = 60 * t.getHours() + t.getMinutes();
              if (m > minTimeMinutesOfDay) minTimeMinutesOfDay = m;
            }
          }

          if (options.maxDateTime !== false) {
            var t = _xdsoft_datetime.strToDateTime(options.maxDateTime);

            var currentDayIsMaxDateTimeDay = dateHelper.formatDate(_xdsoft_datetime.currentTime, options.formatDate) === dateHelper.formatDate(t, options.formatDate);

            if (currentDayIsMaxDateTimeDay) {
              var m = 60 * t.getHours() + t.getMinutes();
              if (m < maxTimeMinutesOfDay) maxTimeMinutesOfDay = m;
            }
          }

          line_time = function line_time(h, m) {
            var now = _xdsoft_datetime.now(),
                current_time,
                isALlowTimesInit = options.allowTimes && djQ.isArray(options.allowTimes) && options.allowTimes.length;

            now.setHours(h);
            h = parseInt(now.getHours(), 10);
            now.setMinutes(m);
            m = parseInt(now.getMinutes(), 10);
            classes = [];
            var currentMinutesOfDay = 60 * h + m;

            if (input.is('[disabled]') || currentMinutesOfDay >= maxTimeMinutesOfDay || currentMinutesOfDay < minTimeMinutesOfDay) {
              classes.push('xdsoft_disabled');
            }

            current_time = new Date(_xdsoft_datetime.currentTime);
            current_time.setHours(parseInt(_xdsoft_datetime.currentTime.getHours(), 10));

            if (!isALlowTimesInit) {
              current_time.setMinutes(Math[options.roundTime](_xdsoft_datetime.currentTime.getMinutes() / options.step) * options.step);
            }

            if ((options.initTime || options.defaultSelect || datetimepicker.data('changed')) && current_time.getHours() === parseInt(h, 10) && (!isALlowTimesInit && options.step > 59 || current_time.getMinutes() === parseInt(m, 10))) {
              if (options.defaultSelect || datetimepicker.data('changed')) {
                classes.push('xdsoft_current');
              } else if (options.initTime) {
                classes.push('xdsoft_init_time');
              }
            }

            if (parseInt(today.getHours(), 10) === parseInt(h, 10) && parseInt(today.getMinutes(), 10) === parseInt(m, 10)) {
              classes.push('xdsoft_today');
            }

            time += '<div class="xdsoft_time ' + classes.join(' ') + '" data-hour="' + h + '" data-minute="' + m + '">' + dateHelper.formatDate(now, options.formatTime) + '</div>';
          };

          if (!options.allowTimes || !djQ.isArray(options.allowTimes) || !options.allowTimes.length) {
            for (i = 0, j = 0; i < (options.hours12 ? 12 : 24); i += 1) {
              for (j = 0; j < 60; j += options.step) {
                var currentMinutesOfDay = i * 60 + j;
                if (currentMinutesOfDay < minTimeMinutesOfDay) continue;
                if (currentMinutesOfDay >= maxTimeMinutesOfDay) continue;
                h = (i < 10 ? '0' : '') + i;
                m = (j < 10 ? '0' : '') + j;
                line_time(h, m);
              }
            }
          } else {
            for (i = 0; i < options.allowTimes.length; i += 1) {
              h = _xdsoft_datetime.strtotime(options.allowTimes[i]).getHours();
              m = _xdsoft_datetime.strtotime(options.allowTimes[i]).getMinutes();
              line_time(h, m);
            }
          }

          timebox.html(time);
          opt = '';

          for (i = parseInt(options.yearStart, 10); i <= parseInt(options.yearEnd, 10); i += 1) {
            opt += '<div class="xdsoft_option ' + (_xdsoft_datetime.currentTime.getFullYear() === i ? 'xdsoft_current' : '') + '" data-value="' + i + '">' + (i + options.yearOffset) + '</div>';
          }

          yearselect.children().eq(0).html(opt);

          for (i = parseInt(options.monthStart, 10), opt = ''; i <= parseInt(options.monthEnd, 10); i += 1) {
            opt += '<div class="xdsoft_option ' + (_xdsoft_datetime.currentTime.getMonth() === i ? 'xdsoft_current' : '') + '" data-value="' + i + '">' + options.i18n[globalLocale].months[i] + '</div>';
          }

          monthselect.children().eq(0).html(opt);
          djQ(datetimepicker).trigger('generate.xdsoft');
        }, 10);
        event.stopPropagation();
      }).on('afterOpen.xdsoft', function () {
        if (options.timepicker) {
          var classType, pheight, height, top;

          if (timebox.find('.xdsoft_current').length) {
            classType = '.xdsoft_current';
          } else if (timebox.find('.xdsoft_init_time').length) {
            classType = '.xdsoft_init_time';
          }

          if (classType) {
            pheight = timeboxparent[0].clientHeight;
            height = timebox[0].offsetHeight;
            top = timebox.find(classType).index() * options.timeHeightInTimePicker + 1;

            if (height - pheight < top) {
              top = height - pheight;
            }

            timeboxparent.trigger('scroll_element.xdsoft_scroller', [parseInt(top, 10) / (height - pheight)]);
          } else {
            timeboxparent.trigger('scroll_element.xdsoft_scroller', [0]);
          }
        }
      });
      timerclick = 0;
      calendar.on('touchend click.xdsoft', 'td', function (xdevent) {
        xdevent.stopPropagation(); // Prevents closing of Pop-ups, Modals and Flyouts in Bootstrap

        timerclick += 1;
        var $this = djQ(this),
            currentTime = _xdsoft_datetime.currentTime;

        if (currentTime === undefined || currentTime === null) {
          _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
          currentTime = _xdsoft_datetime.currentTime;
        }

        if ($this.hasClass('xdsoft_disabled')) {
          return false;
        }

        currentTime.setDate(1);
        currentTime.setFullYear($this.data('year'));
        currentTime.setMonth($this.data('month'));
        currentTime.setDate($this.data('date'));
        datetimepicker.trigger('select.xdsoft', [currentTime]);
        input.val(_xdsoft_datetime.str());

        if (options.onSelectDate && djQ.isFunction(options.onSelectDate)) {
          options.onSelectDate.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), xdevent);
        }

        datetimepicker.data('changed', true);
        datetimepicker.trigger('xchange.xdsoft');
        datetimepicker.trigger('changedatetime.xdsoft');

        if ((timerclick > 1 || options.closeOnDateSelect === true || options.closeOnDateSelect === false && !options.timepicker) && !options.inline) {
          datetimepicker.trigger('close.xdsoft');
        }

        setTimeout(function () {
          timerclick = 0;
        }, 200);
      });
      timebox.on('touchstart', 'div', function (xdevent) {
        this.touchMoved = false;
      }).on('touchmove', 'div', handleTouchMoved).on('touchend click.xdsoft', 'div', function (xdevent) {
        if (!this.touchMoved) {
          xdevent.stopPropagation();
          var $this = djQ(this),
              currentTime = _xdsoft_datetime.currentTime;

          if (currentTime === undefined || currentTime === null) {
            _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
            currentTime = _xdsoft_datetime.currentTime;
          }

          if ($this.hasClass('xdsoft_disabled')) {
            return false;
          }

          currentTime.setHours($this.data('hour'));
          currentTime.setMinutes($this.data('minute'));
          datetimepicker.trigger('select.xdsoft', [currentTime]);
          datetimepicker.data('input').val(_xdsoft_datetime.str());

          if (options.onSelectTime && djQ.isFunction(options.onSelectTime)) {
            options.onSelectTime.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), xdevent);
          }

          datetimepicker.data('changed', true);
          datetimepicker.trigger('xchange.xdsoft');
          datetimepicker.trigger('changedatetime.xdsoft');

          if (options.inline !== true && options.closeOnTimeSelect === true) {
            datetimepicker.trigger('close.xdsoft');
          }
        }
      });
      datepicker.on('mousewheel.xdsoft', function (event) {
        if (!options.scrollMonth) {
          return true;
        }

        if (event.deltaY < 0) {
          _xdsoft_datetime.nextMonth();
        } else {
          _xdsoft_datetime.prevMonth();
        }

        return false;
      });
      input.on('mousewheel.xdsoft', function (event) {
        if (!options.scrollInput) {
          return true;
        }

        if (!options.datepicker && options.timepicker) {
          current_time_index = timebox.find('.xdsoft_current').length ? timebox.find('.xdsoft_current').eq(0).index() : 0;

          if (current_time_index + event.deltaY >= 0 && current_time_index + event.deltaY < timebox.children().length) {
            current_time_index += event.deltaY;
          }

          if (timebox.children().eq(current_time_index).length) {
            timebox.children().eq(current_time_index).trigger('mousedown');
          }

          return false;
        }

        if (options.datepicker && !options.timepicker) {
          datepicker.trigger(event, [event.deltaY, event.deltaX, event.deltaY]);

          if (input.val) {
            input.val(_xdsoft_datetime.str());
          }

          datetimepicker.trigger('changedatetime.xdsoft');
          return false;
        }
      });
      datetimepicker.on('changedatetime.xdsoft', function (event) {
        if (options.onChangeDateTime && djQ.isFunction(options.onChangeDateTime)) {
          var $input = datetimepicker.data('input');
          options.onChangeDateTime.call(datetimepicker, _xdsoft_datetime.currentTime, $input, event);
          delete options.value;
          $input.trigger('change');
        }
      }).on('generate.xdsoft', function () {
        if (options.onGenerate && djQ.isFunction(options.onGenerate)) {
          options.onGenerate.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
        }

        if (triggerAfterOpen) {
          datetimepicker.trigger('afterOpen.xdsoft');
          triggerAfterOpen = false;
        }
      }).on('click.xdsoft', function (xdevent) {
        xdevent.stopPropagation();
      });
      current_time_index = 0;
      /**
       * Runs the callback for each of the specified node's ancestors.
       *
       * Return FALSE from the callback to stop ascending.
       *
       * @param {DOMNode} node
       * @param {Function} callback
       * @returns {undefined}
       */

      forEachAncestorOf = function forEachAncestorOf(node, callback) {
        do {
          node = node.parentNode;

          if (!node || callback(node) === false) {
            break;
          }
        } while (node.nodeName !== 'HTML');
      };
      /**
       * Sets the position of the picker.
       *
       * @returns {undefined}
       */


      setPos = function setPos() {
        var dateInputOffset, dateInputElem, verticalPosition, left, position, datetimepickerElem, dateInputHasFixedAncestor, $dateInput, windowWidth, verticalAnchorEdge, datetimepickerCss, windowHeight, windowScrollTop;
        $dateInput = datetimepicker.data('input');
        dateInputOffset = $dateInput.offset();
        dateInputElem = $dateInput[0];
        verticalAnchorEdge = 'top';
        verticalPosition = dateInputOffset.top + dateInputElem.offsetHeight - 1;
        left = dateInputOffset.left;
        position = "absolute";
        windowWidth = djQ(options.contentWindow).width();
        windowHeight = djQ(options.contentWindow).height();
        windowScrollTop = djQ(options.contentWindow).scrollTop();

        if (options.ownerDocument.documentElement.clientWidth - dateInputOffset.left < datepicker.parent().outerWidth(true)) {
          var diff = datepicker.parent().outerWidth(true) - dateInputElem.offsetWidth;
          left = left - diff;
        }

        if ($dateInput.parent().css('direction') === 'rtl') {
          left -= datetimepicker.outerWidth() - $dateInput.outerWidth();
        }

        if (options.fixed) {
          verticalPosition -= windowScrollTop;
          left -= djQ(options.contentWindow).scrollLeft();
          position = "fixed";
        } else {
          dateInputHasFixedAncestor = false;
          forEachAncestorOf(dateInputElem, function (ancestorNode) {
            if (ancestorNode === null) {
              return false;
            }

            if (options.contentWindow.getComputedStyle(ancestorNode).getPropertyValue('position') === 'fixed') {
              dateInputHasFixedAncestor = true;
              return false;
            }
          });

          if (dateInputHasFixedAncestor && !options.insideParent) {
            position = 'fixed'; //If the picker won't fit entirely within the viewport then display it above the date input.

            if (verticalPosition + datetimepicker.outerHeight() > windowHeight + windowScrollTop) {
              verticalAnchorEdge = 'bottom';
              verticalPosition = windowHeight + windowScrollTop - dateInputOffset.top;
            } else {
              verticalPosition -= windowScrollTop;
            }
          } else {
            if (verticalPosition + datetimepicker[0].offsetHeight > windowHeight + windowScrollTop) {
              verticalPosition = dateInputOffset.top - datetimepicker[0].offsetHeight + 1;
            }
          }

          if (verticalPosition < 0) {
            verticalPosition = 0;
          }

          if (left + dateInputElem.offsetWidth > windowWidth) {
            left = windowWidth - dateInputElem.offsetWidth;
          }
        }

        datetimepickerElem = datetimepicker[0];
        forEachAncestorOf(datetimepickerElem, function (ancestorNode) {
          var ancestorNodePosition;
          ancestorNodePosition = options.contentWindow.getComputedStyle(ancestorNode).getPropertyValue('position');

          if (ancestorNodePosition === 'relative' && windowWidth >= ancestorNode.offsetWidth) {
            left = left - (windowWidth - ancestorNode.offsetWidth) / 2;
            return false;
          }
        });
        datetimepickerCss = {
          position: position,
          left: options.insideParent ? dateInputElem.offsetLeft : left,
          top: '',
          //Initialize to prevent previous values interfering with new ones.
          bottom: '' //Initialize to prevent previous values interfering with new ones.

        };

        if (options.insideParent) {
          datetimepickerCss[verticalAnchorEdge] = dateInputElem.offsetTop + dateInputElem.offsetHeight;
        } else {
          datetimepickerCss[verticalAnchorEdge] = verticalPosition;
        }

        datetimepicker.css(datetimepickerCss);
      };

      datetimepicker.on('open.xdsoft', function (event) {
        var onShow = true;

        if (options.onShow && djQ.isFunction(options.onShow)) {
          onShow = options.onShow.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), event);
        }

        if (onShow !== false) {
          datetimepicker.show();
          setPos();
          djQ(options.contentWindow).off('resize.xdsoft', setPos).on('resize.xdsoft', setPos);

          if (options.closeOnWithoutClick) {
            djQ([options.ownerDocument.body, options.contentWindow]).on('touchstart mousedown.xdsoft', function arguments_callee6() {
              datetimepicker.trigger('close.xdsoft');
              djQ([options.ownerDocument.body, options.contentWindow]).off('touchstart mousedown.xdsoft', arguments_callee6);
            });
          }
        }
      }).on('close.xdsoft', function (event) {
        var onClose = true;
        month_picker.find('.xdsoft_month,.xdsoft_year').find('.xdsoft_select').hide();

        if (options.onClose && djQ.isFunction(options.onClose)) {
          onClose = options.onClose.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), event);
        }

        if (onClose !== false && !options.opened && !options.inline) {
          datetimepicker.hide();
        }

        event.stopPropagation();
      }).on('toggle.xdsoft', function () {
        if (datetimepicker.is(':visible')) {
          datetimepicker.trigger('close.xdsoft');
        } else {
          datetimepicker.trigger('open.xdsoft');
        }
      }).data('input', input);
      timer = 0;
      datetimepicker.data('xdsoft_datetime', _xdsoft_datetime);
      datetimepicker.setOptions(options);

      function getCurrentValue() {
        var ct = false,
            time;

        if (options.startDate) {
          ct = _xdsoft_datetime.strToDate(options.startDate);
        } else {
          ct = options.value || (input && input.val && input.val() ? input.val() : '');

          if (ct) {
            ct = _xdsoft_datetime.strToDateTime(ct);

            if (options.yearOffset) {
              ct = new Date(ct.getFullYear() - options.yearOffset, ct.getMonth(), ct.getDate(), ct.getHours(), ct.getMinutes(), ct.getSeconds(), ct.getMilliseconds());
            }
          } else if (options.defaultDate) {
            ct = _xdsoft_datetime.strToDateTime(options.defaultDate);

            if (options.defaultTime) {
              time = _xdsoft_datetime.strtotime(options.defaultTime);
              ct.setHours(time.getHours());
              ct.setMinutes(time.getMinutes());
            }
          }
        }

        if (ct && _xdsoft_datetime.isValidDate(ct)) {
          datetimepicker.data('changed', true);
        } else {
          ct = '';
        }

        return ct || 0;
      }

      function setMask(options) {
        var isValidValue = function isValidValue(mask, value) {
          var reg = mask.replace(/([\[\]\/\{\}\(\)\-\.\+]{1})/g, '\\$1').replace(/_/g, '{digit+}').replace(/([0-9]{1})/g, '{digit$1}').replace(/\{digit([0-9]{1})\}/g, '[0-$1_]{1}').replace(/\{digit[\+]\}/g, '[0-9_]{1}');
          return new RegExp(reg).test(value);
        },
            getCaretPos = function getCaretPos(input) {
          try {
            if (options.ownerDocument.selection && options.ownerDocument.selection.createRange) {
              var range = options.ownerDocument.selection.createRange();
              return range.getBookmark().charCodeAt(2) - 2;
            }

            if (input.setSelectionRange) {
              return input.selectionStart;
            }
          } catch (e) {
            return 0;
          }
        },
            setCaretPos = function setCaretPos(node, pos) {
          node = typeof node === "string" || node instanceof String ? options.ownerDocument.getElementById(node) : node;

          if (!node) {
            return false;
          }

          if (node.createTextRange) {
            var textRange = node.createTextRange();
            textRange.collapse(true);
            textRange.moveEnd('character', pos);
            textRange.moveStart('character', pos);
            textRange.select();
            return true;
          }

          if (node.setSelectionRange) {
            node.setSelectionRange(pos, pos);
            return true;
          }

          return false;
        };

        if (options.mask) {
          input.off('keydown.xdsoft');
        }

        if (options.mask === true) {
          if (dateHelper.formatMask) {
            options.mask = dateHelper.formatMask(options.format);
          } else {
            options.mask = options.format.replace(/Y/g, '9999').replace(/F/g, '9999').replace(/m/g, '19').replace(/d/g, '39').replace(/H/g, '29').replace(/i/g, '59').replace(/s/g, '59');
          }
        }

        if (djQ.type(options.mask) === 'string') {
          if (!isValidValue(options.mask, input.val())) {
            input.val(options.mask.replace(/[0-9]/g, '_'));
            setCaretPos(input[0], 0);
          }

          input.on('paste.xdsoft', function (event) {
            // couple options here
            // 1. return false - tell them they can't paste
            // 2. insert over current characters - minimal validation
            // 3. full fledged parsing and validation
            // let's go option 2 for now
            // fires multiple times for some reason
            // https://stackoverflow.com/a/30496488/1366033
            var clipboardData = event.clipboardData || event.originalEvent.clipboardData || window.clipboardData,
                pastedData = clipboardData.getData('text'),
                val = this.value,
                pos = this.selectionStart;
            var valueBeforeCursor = val.substr(0, pos);
            var valueAfterPaste = val.substr(pos + pastedData.length);
            val = valueBeforeCursor + pastedData + valueAfterPaste;
            pos += pastedData.length;

            if (isValidValue(options.mask, val)) {
              this.value = val;
              setCaretPos(this, pos);
            } else if (djQ.trim(val) === '') {
              this.value = options.mask.replace(/[0-9]/g, '_');
            } else {
              input.trigger('error_input.xdsoft');
            }

            event.preventDefault();
            return false;
          });
          input.on('keydown.xdsoft', function (event) {
            var val = this.value,
                key = event.which,
                pos = this.selectionStart,
                selEnd = this.selectionEnd,
                hasSel = pos !== selEnd,
                digit; // only alow these characters

            if (key >= KEY0 && key <= KEY9 || key >= _KEY0 && key <= _KEY9 || key === BACKSPACE || key === DEL) {
              // get char to insert which is new character or placeholder ('_')
              digit = key === BACKSPACE || key === DEL ? '_' : String.fromCharCode(_KEY0 <= key && key <= _KEY9 ? key - KEY0 : key); // we're deleting something, we're not at the start, and have normal cursor, move back one
              // if we have a selection length, cursor actually sits behind deletable char, not in front

              if (key === BACKSPACE && pos && !hasSel) {
                pos -= 1;
              } // don't stop on a separator, continue whatever direction you were going
              //   value char - keep incrementing position while on separator char and we still have room
              //   del char   - keep decrementing position while on separator char and we still have room


              while (true) {
                var maskValueAtCurPos = options.mask.substr(pos, 1);
                var posShorterThanMaskLength = pos < options.mask.length;
                var posGreaterThanZero = pos > 0;
                var notNumberOrPlaceholder = /[^0-9_]/;
                var curPosOnSep = notNumberOrPlaceholder.test(maskValueAtCurPos);
                var continueMovingPosition = curPosOnSep && posShorterThanMaskLength && posGreaterThanZero; // if we hit a real char, stay where we are

                if (!continueMovingPosition) break; // hitting backspace in a selection, you can possibly go back any further - go forward

                pos += key === BACKSPACE && !hasSel ? -1 : 1;
              }

              if (event.metaKey) {
                // cmd has been pressed
                pos = 0;
                hasSel = true;
              }

              if (hasSel) {
                // pos might have moved so re-calc length
                var selLength = selEnd - pos; // if we have a selection length we will wipe out entire selection and replace with default template for that range

                var defaultBlank = options.mask.replace(/[0-9]/g, '_');
                var defaultBlankSelectionReplacement = defaultBlank.substr(pos, selLength);
                var selReplacementRemainder = defaultBlankSelectionReplacement.substr(1); // might be empty

                var valueBeforeSel = val.substr(0, pos);
                var insertChars = digit + selReplacementRemainder;
                var charsAfterSelection = val.substr(pos + selLength);
                val = valueBeforeSel + insertChars + charsAfterSelection;
              } else {
                var valueBeforeCursor = val.substr(0, pos);
                var insertChar = digit;
                var valueAfterNextChar = val.substr(pos + 1);
                val = valueBeforeCursor + insertChar + valueAfterNextChar;
              }

              if (djQ.trim(val) === '') {
                // if empty, set to default
                val = defaultBlank;
              } else {
                // if at the last character don't need to do anything
                if (pos === options.mask.length) {
                  event.preventDefault();
                  return false;
                }
              } // resume cursor location


              pos += key === BACKSPACE ? 0 : 1; // don't stop on a separator, continue whatever direction you were going

              while (/[^0-9_]/.test(options.mask.substr(pos, 1)) && pos < options.mask.length && pos > 0) {
                pos += key === BACKSPACE ? 0 : 1;
              }

              if (isValidValue(options.mask, val)) {
                this.value = val;
                setCaretPos(this, pos);
              } else if (djQ.trim(val) === '') {
                this.value = options.mask.replace(/[0-9]/g, '_');
              } else {
                input.trigger('error_input.xdsoft');
              }
            } else {
              if ([AKEY, CKEY, VKEY, ZKEY, YKEY].indexOf(key) !== -1 && ctrlDown || [ESC, ARROWUP, ARROWDOWN, ARROWLEFT, ARROWRIGHT, F5, CTRLKEY, TAB, ENTER].indexOf(key) !== -1) {
                return true;
              }
            }

            event.preventDefault();
            return false;
          });
        }
      }

      _xdsoft_datetime.setCurrentTime(getCurrentValue());

      input.data('xdsoft_datetimepicker', datetimepicker).on('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', function () {
        if (input.is(':disabled') || input.data('xdsoft_datetimepicker').is(':visible') && options.closeOnInputClick) {
          return;
        }

        if (!options.openOnFocus) {
          return;
        }

        clearTimeout(timer);
        timer = setTimeout(function () {
          if (input.is(':disabled')) {
            return;
          }

          triggerAfterOpen = true;

          _xdsoft_datetime.setCurrentTime(getCurrentValue(), true);

          if (options.mask) {
            setMask(options);
          }

          datetimepicker.trigger('open.xdsoft');
        }, 100);
      }).on('keydown.xdsoft', function (event) {
        var elementSelector,
            key = event.which;

        if ([ENTER].indexOf(key) !== -1 && options.enterLikeTab) {
          elementSelector = djQ("input:visible,textarea:visible,button:visible,a:visible");
          datetimepicker.trigger('close.xdsoft');
          elementSelector.eq(elementSelector.index(this) + 1).focus();
          return false;
        }

        if ([TAB].indexOf(key) !== -1) {
          datetimepicker.trigger('close.xdsoft');
          return true;
        }
      }).on('blur.xdsoft', function () {
        datetimepicker.trigger('close.xdsoft');
      });
    };

    destroyDateTimePicker = function destroyDateTimePicker(input) {
      var datetimepicker = input.data('xdsoft_datetimepicker');

      if (datetimepicker) {
        datetimepicker.data('xdsoft_datetime', null);
        datetimepicker.remove();
        input.data('xdsoft_datetimepicker', null).off('.xdsoft');
        djQ(options.contentWindow).off('resize.xdsoft');
        djQ([options.contentWindow, options.ownerDocument.body]).off('mousedown.xdsoft touchstart');

        if (input.unmousewheel) {
          input.unmousewheel();
        }
      }
    };

    djQ(options.ownerDocument).off('keydown.xdsoftctrl keyup.xdsoftctrl').off('keydown.xdsoftcmd keyup.xdsoftcmd').on('keydown.xdsoftctrl', function (e) {
      if (e.keyCode === CTRLKEY) {
        ctrlDown = true;
      }
    }).on('keyup.xdsoftctrl', function (e) {
      if (e.keyCode === CTRLKEY) {
        ctrlDown = false;
      }
    }).on('keydown.xdsoftcmd', function (e) {
      if (e.keyCode === CMDKEY) {
        cmdDown = true;
      }
    }).on('keyup.xdsoftcmd', function (e) {
      if (e.keyCode === CMDKEY) {
        cmdDown = false;
      }
    });
    this.each(function () {
      var datetimepicker = djQ(this).data('xdsoft_datetimepicker'),
          $input;

      if (datetimepicker) {
        if (djQ.type(opt) === 'string') {
          switch (opt) {
            case 'show':
              djQ(this).select().focus();
              datetimepicker.trigger('open.xdsoft');
              break;

            case 'hide':
              datetimepicker.trigger('close.xdsoft');
              break;

            case 'toggle':
              datetimepicker.trigger('toggle.xdsoft');
              break;

            case 'destroy':
              destroyDateTimePicker(djQ(this));
              break;

            case 'reset':
              this.value = this.defaultValue;

              if (!this.value || !datetimepicker.data('xdsoft_datetime').isValidDate(dateHelper.parseDate(this.value, options.format))) {
                datetimepicker.data('changed', false);
              }

              datetimepicker.data('xdsoft_datetime').setCurrentTime(this.value);
              break;

            case 'validate':
              $input = datetimepicker.data('input');
              $input.trigger('blur.xdsoft');
              break;

            default:
              if (datetimepicker[opt] && djQ.isFunction(datetimepicker[opt])) {
                result = datetimepicker[opt](opt2);
              }

          }
        } else {
          datetimepicker.setOptions(opt);
        }

        return 0;
      }

      if (djQ.type(opt) !== 'string') {
        if (!options.lazyInit || options.open || options.inline) {
          createDateTimePicker(djQ(this));
        } else {
          lazyInit(djQ(this));
        }
      }
    });
    return result;
  };

  djQ.fn.datetimepicker.defaults = default_options;

  function HighlightedDate(date, desc, style) {
    "use strict";

    this.date = date;
    this.desc = desc;
    this.style = style;
  }
};

;

(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery', 'jquery-mousewheel'], factory);
  } else if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object') {
    // Node/CommonJS style for Browserify
    module.exports = factory(require('jquery'));
    ;
  } else {
    // Browser globals
    factory(djQ);
  }
})(datetimepickerFactory);
/*!
 * jQuery Mousewheel 3.1.13
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 */


(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object') {
    // Node/CommonJS style for Browserify
    module.exports = factory;
  } else {
    // Browser globals
    factory(djQ);
  }
})(function ($) {
  var toFix = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'],
      toBind = 'onwheel' in document || document.documentMode >= 9 ? ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'],
      slice = Array.prototype.slice,
      nullLowestDeltaTimeout,
      lowestDelta;

  if (djQ.event.fixHooks) {
    for (var i = toFix.length; i;) {
      djQ.event.fixHooks[toFix[--i]] = djQ.event.mouseHooks;
    }
  }

  var special = djQ.event.special.mousewheel = {
    version: '3.1.12',
    setup: function setup() {
      if (this.addEventListener) {
        for (var i = toBind.length; i;) {
          this.addEventListener(toBind[--i], handler, false);
        }
      } else {
        this.onmousewheel = handler;
      } // Store the line height and page height for this particular element


      djQ.data(this, 'mousewheel-line-height', special.getLineHeight(this));
      djQ.data(this, 'mousewheel-page-height', special.getPageHeight(this));
    },
    teardown: function teardown() {
      if (this.removeEventListener) {
        for (var i = toBind.length; i;) {
          this.removeEventListener(toBind[--i], handler, false);
        }
      } else {
        this.onmousewheel = null;
      } // Clean up the data we added to the element


      djQ.removeData(this, 'mousewheel-line-height');
      djQ.removeData(this, 'mousewheel-page-height');
    },
    getLineHeight: function getLineHeight(elem) {
      var $elem = djQ(elem),
          $parent = $elem['offsetParent' in djQ.fn ? 'offsetParent' : 'parent']();

      if (!$parent.length) {
        $parent = djQ('body');
      }

      return parseInt($parent.css('fontSize'), 10) || parseInt($elem.css('fontSize'), 10) || 16;
    },
    getPageHeight: function getPageHeight(elem) {
      return djQ(elem).height();
    },
    settings: {
      adjustOldDeltas: true,
      // see shouldAdjustOldDeltas() below
      normalizeOffset: true // calls getBoundingClientRect for each event

    }
  };
  djQ.fn.extend({
    mousewheel: function mousewheel(fn) {
      return fn ? this.bind('mousewheel', fn) : this.trigger('mousewheel');
    },
    unmousewheel: function unmousewheel(fn) {
      return this.unbind('mousewheel', fn);
    }
  });

  function handler(event) {
    var orgEvent = event || window.event,
        args = slice.call(arguments, 1),
        delta = 0,
        deltaX = 0,
        deltaY = 0,
        absDelta = 0,
        offsetX = 0,
        offsetY = 0;
    event = djQ.event.fix(orgEvent);
    event.type = 'mousewheel'; // Old school scrollwheel delta

    if ('detail' in orgEvent) {
      deltaY = orgEvent.detail * -1;
    }

    if ('wheelDelta' in orgEvent) {
      deltaY = orgEvent.wheelDelta;
    }

    if ('wheelDeltaY' in orgEvent) {
      deltaY = orgEvent.wheelDeltaY;
    }

    if ('wheelDeltaX' in orgEvent) {
      deltaX = orgEvent.wheelDeltaX * -1;
    } // Firefox < 17 horizontal scrolling related to DOMMouseScroll event


    if ('axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
      deltaX = deltaY * -1;
      deltaY = 0;
    } // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy


    delta = deltaY === 0 ? deltaX : deltaY; // New school wheel delta (wheel event)

    if ('deltaY' in orgEvent) {
      deltaY = orgEvent.deltaY * -1;
      delta = deltaY;
    }

    if ('deltaX' in orgEvent) {
      deltaX = orgEvent.deltaX;

      if (deltaY === 0) {
        delta = deltaX * -1;
      }
    } // No change actually happened, no reason to go any further


    if (deltaY === 0 && deltaX === 0) {
      return;
    } // Need to convert lines and pages to pixels if we aren't already in pixels
    // There are three delta modes:
    //   * deltaMode 0 is by pixels, nothing to do
    //   * deltaMode 1 is by lines
    //   * deltaMode 2 is by pages


    if (orgEvent.deltaMode === 1) {
      var lineHeight = djQ.data(this, 'mousewheel-line-height');
      delta *= lineHeight;
      deltaY *= lineHeight;
      deltaX *= lineHeight;
    } else if (orgEvent.deltaMode === 2) {
      var pageHeight = djQ.data(this, 'mousewheel-page-height');
      delta *= pageHeight;
      deltaY *= pageHeight;
      deltaX *= pageHeight;
    } // Store lowest absolute delta to normalize the delta values


    absDelta = Math.max(Math.abs(deltaY), Math.abs(deltaX));

    if (!lowestDelta || absDelta < lowestDelta) {
      lowestDelta = absDelta; // Adjust older deltas if necessary

      if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
        lowestDelta /= 40;
      }
    } // Adjust older deltas if necessary


    if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
      // Divide all the things by 40!
      delta /= 40;
      deltaX /= 40;
      deltaY /= 40;
    } // Get a whole, normalized value for the deltas


    delta = Math[delta >= 1 ? 'floor' : 'ceil'](delta / lowestDelta);
    deltaX = Math[deltaX >= 1 ? 'floor' : 'ceil'](deltaX / lowestDelta);
    deltaY = Math[deltaY >= 1 ? 'floor' : 'ceil'](deltaY / lowestDelta); // Normalise offsetX and offsetY properties

    if (special.settings.normalizeOffset && this.getBoundingClientRect) {
      var boundingRect = this.getBoundingClientRect();
      offsetX = event.clientX - boundingRect.left;
      offsetY = event.clientY - boundingRect.top;
    } // Add information to the event object


    event.deltaX = deltaX;
    event.deltaY = deltaY;
    event.deltaFactor = lowestDelta;
    event.offsetX = offsetX;
    event.offsetY = offsetY; // Go ahead and set deltaMode to 0 since we converted to pixels
    // Although this is a little odd since we overwrite the deltaX/Y
    // properties with normalized deltas.

    event.deltaMode = 0; // Add event and delta to the front of the arguments

    args.unshift(event, delta, deltaX, deltaY); // Clearout lowestDelta after sometime to better
    // handle multiple device types that give different
    // a different lowestDelta
    // Ex: trackpad = 3 and mouse wheel = 120

    if (nullLowestDeltaTimeout) {
      clearTimeout(nullLowestDeltaTimeout);
    }

    nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);
    return (djQ.event.dispatch || djQ.event.handle).apply(this, args);
  }

  function nullLowestDelta() {
    lowestDelta = null;
  }

  function shouldAdjustOldDeltas(orgEvent, absDelta) {
    // If this is an older event and the delta is divisable by 120,
    // then we are assuming that the browser is treating this as an
    // older mouse wheel event and that we should divide the deltas
    // by 40 to try and get a more usable deltaFactor.
    // Side note, this actually impacts the reported scroll distance
    // in older browsers and can cause scrolling to be slower than native.
    // Turn this off by setting djQ.event.special.mousewheel.settings.adjustOldDeltas to false.
    return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
  }
});
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function (global, factory) {
  (typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object' && typeof module !== 'undefined' ? factory(exports) : typeof define === 'function' && define.amd ? define(['exports'], factory) : factory(global.blobUtil = {});
})(this, function (exports) {
  'use strict'; // TODO: including these in blob-util.ts causes typedoc to generate docs for them,
  // even with --excludePrivate ¯\_(ツ)_/¯

  /** @private */

  function loadImage(src, crossOrigin) {
    return new Promise(function (resolve, reject) {
      var img = new Image();

      if (crossOrigin) {
        img.crossOrigin = crossOrigin;
      }

      img.onload = function () {
        resolve(img);
      };

      img.onerror = reject;
      img.src = src;
    });
  }
  /** @private */


  function imgToCanvas(img) {
    var canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height; // copy the image contents to the canvas

    var context = canvas.getContext('2d');
    context.drawImage(img, 0, 0, img.width, img.height, 0, 0, img.width, img.height);
    return canvas;
  }
  /* global Promise, Image, Blob, FileReader, atob, btoa,
     BlobBuilder, MSBlobBuilder, MozBlobBuilder, WebKitBlobBuilder, webkitURL */

  /**
   * Shim for
   * [`new Blob()`](https://developer.mozilla.org/en-US/docs/Web/API/Blob.Blob)
   * to support
   * [older browsers that use the deprecated `BlobBuilder` API](http://caniuse.com/blob).
   *
   * Example:
   *
   * ```js
   * var myBlob = blobUtil.createBlob(['hello world'], {type: 'text/plain'});
   * ```
   *
   * @param parts - content of the Blob
   * @param properties - usually `{type: myContentType}`,
   *                           you can also pass a string for the content type
   * @returns Blob
   */


  function createBlob(parts, properties) {
    parts = parts || [];
    properties = properties || {};

    if (typeof properties === 'string') {
      properties = {
        type: properties
      }; // infer content type
    }

    try {
      return new Blob(parts, properties);
    } catch (e) {
      if (e.name !== 'TypeError') {
        throw e;
      }

      var Builder = typeof BlobBuilder !== 'undefined' ? BlobBuilder : typeof MSBlobBuilder !== 'undefined' ? MSBlobBuilder : typeof MozBlobBuilder !== 'undefined' ? MozBlobBuilder : WebKitBlobBuilder;
      var builder = new Builder();

      for (var i = 0; i < parts.length; i += 1) {
        builder.append(parts[i]);
      }

      return builder.getBlob(properties.type);
    }
  }
  /**
   * Shim for
   * [`URL.createObjectURL()`](https://developer.mozilla.org/en-US/docs/Web/API/URL.createObjectURL)
   * to support browsers that only have the prefixed
   * `webkitURL` (e.g. Android <4.4).
   *
   * Example:
   *
   * ```js
   * var myUrl = blobUtil.createObjectURL(blob);
   * ```
   *
   * @param blob
   * @returns url
   */


  function createObjectURL(blob) {
    return (typeof URL !== 'undefined' ? URL : webkitURL).createObjectURL(blob);
  }
  /**
   * Shim for
   * [`URL.revokeObjectURL()`](https://developer.mozilla.org/en-US/docs/Web/API/URL.revokeObjectURL)
   * to support browsers that only have the prefixed
   * `webkitURL` (e.g. Android <4.4).
   *
   * Example:
   *
   * ```js
   * blobUtil.revokeObjectURL(myUrl);
   * ```
   *
   * @param url
   */


  function revokeObjectURL(url) {
    return (typeof URL !== 'undefined' ? URL : webkitURL).revokeObjectURL(url);
  }
  /**
   * Convert a `Blob` to a binary string.
   *
   * Example:
   *
   * ```js
   * blobUtil.blobToBinaryString(blob).then(function (binaryString) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param blob
   * @returns Promise that resolves with the binary string
   */


  function blobToBinaryString(blob) {
    return new Promise(function (resolve, reject) {
      var reader = new FileReader();
      var hasBinaryString = typeof reader.readAsBinaryString === 'function';

      reader.onloadend = function () {
        var result = reader.result || '';

        if (hasBinaryString) {
          return resolve(result);
        }

        resolve(arrayBufferToBinaryString(result));
      };

      reader.onerror = reject;

      if (hasBinaryString) {
        reader.readAsBinaryString(blob);
      } else {
        reader.readAsArrayBuffer(blob);
      }
    });
  }
  /**
   * Convert a base64-encoded string to a `Blob`.
   *
   * Example:
   *
   * ```js
   * var blob = blobUtil.base64StringToBlob(base64String);
   * ```
   * @param base64 - base64-encoded string
   * @param type - the content type (optional)
   * @returns Blob
   */


  function base64StringToBlob(base64, type) {
    var parts = [binaryStringToArrayBuffer(atob(base64))];
    return type ? createBlob(parts, {
      type: type
    }) : createBlob(parts);
  }
  /**
   * Convert a binary string to a `Blob`.
   *
   * Example:
   *
   * ```js
   * var blob = blobUtil.binaryStringToBlob(binaryString);
   * ```
   *
   * @param binary - binary string
   * @param type - the content type (optional)
   * @returns Blob
   */


  function binaryStringToBlob(binary, type) {
    return base64StringToBlob(btoa(binary), type);
  }
  /**
   * Convert a `Blob` to a binary string.
   *
   * Example:
   *
   * ```js
   * blobUtil.blobToBase64String(blob).then(function (base64String) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param blob
   * @returns Promise that resolves with the binary string
   */


  function blobToBase64String(blob) {
    return blobToBinaryString(blob).then(btoa);
  }
  /**
   * Convert a data URL string
   * (e.g. `'data:image/png;base64,iVBORw0KG...'`)
   * to a `Blob`.
   *
   * Example:
   *
   * ```js
   * var blob = blobUtil.dataURLToBlob(dataURL);
   * ```
   *
   * @param dataURL - dataURL-encoded string
   * @returns Blob
   */


  function dataURLToBlob(dataURL) {
    var type = dataURL.match(/data:([^;]+)/)[1];
    var base64 = dataURL.replace(/^[^,]+,/, '');
    var buff = binaryStringToArrayBuffer(atob(base64));
    return createBlob([buff], {
      type: type
    });
  }
  /**
   * Convert a `Blob` to a data URL string
   * (e.g. `'data:image/png;base64,iVBORw0KG...'`).
   *
   * Example:
   *
   * ```js
   * var dataURL = blobUtil.blobToDataURL(blob);
   * ```
   *
   * @param blob
   * @returns Promise that resolves with the data URL string
   */


  function blobToDataURL(blob) {
    return blobToBase64String(blob).then(function (base64String) {
      return 'data:' + blob.type + ';base64,' + base64String;
    });
  }
  /**
   * Convert an image's `src` URL to a data URL by loading the image and painting
   * it to a `canvas`.
   *
   * Note: this will coerce the image to the desired content type, and it
   * will only paint the first frame of an animated GIF.
   *
   * Examples:
   *
   * ```js
   * blobUtil.imgSrcToDataURL('http://mysite.com/img.png').then(function (dataURL) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * ```js
   * blobUtil.imgSrcToDataURL('http://some-other-site.com/img.jpg', 'image/jpeg',
   *                          'Anonymous', 1.0).then(function (dataURL) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param src - image src
   * @param type - the content type (optional, defaults to 'image/png')
   * @param crossOrigin - for CORS-enabled images, set this to
   *                                         'Anonymous' to avoid "tainted canvas" errors
   * @param quality - a number between 0 and 1 indicating image quality
   *                                     if the requested type is 'image/jpeg' or 'image/webp'
   * @returns Promise that resolves with the data URL string
   */


  function imgSrcToDataURL(src, type, crossOrigin, quality) {
    type = type || 'image/png';
    return loadImage(src, crossOrigin).then(imgToCanvas).then(function (canvas) {
      return canvas.toDataURL(type, quality);
    });
  }
  /**
   * Convert a `canvas` to a `Blob`.
   *
   * Examples:
   *
   * ```js
   * blobUtil.canvasToBlob(canvas).then(function (blob) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * Most browsers support converting a canvas to both `'image/png'` and `'image/jpeg'`. You may
   * also want to try `'image/webp'`, which will work in some browsers like Chrome (and in other browsers, will just fall back to `'image/png'`):
   *
   * ```js
   * blobUtil.canvasToBlob(canvas, 'image/webp').then(function (blob) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param canvas - HTMLCanvasElement
   * @param type - the content type (optional, defaults to 'image/png')
   * @param quality - a number between 0 and 1 indicating image quality
   *                                     if the requested type is 'image/jpeg' or 'image/webp'
   * @returns Promise that resolves with the `Blob`
   */


  function canvasToBlob(canvas, type, quality) {
    if (typeof canvas.toBlob === 'function') {
      return new Promise(function (resolve) {
        canvas.toBlob(resolve, type, quality);
      });
    }

    return Promise.resolve(dataURLToBlob(canvas.toDataURL(type, quality)));
  }
  /**
   * Convert an image's `src` URL to a `Blob` by loading the image and painting
   * it to a `canvas`.
   *
   * Note: this will coerce the image to the desired content type, and it
   * will only paint the first frame of an animated GIF.
   *
   * Examples:
   *
   * ```js
   * blobUtil.imgSrcToBlob('http://mysite.com/img.png').then(function (blob) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * ```js
   * blobUtil.imgSrcToBlob('http://some-other-site.com/img.jpg', 'image/jpeg',
   *                          'Anonymous', 1.0).then(function (blob) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param src - image src
   * @param type - the content type (optional, defaults to 'image/png')
   * @param crossOrigin - for CORS-enabled images, set this to
   *                                         'Anonymous' to avoid "tainted canvas" errors
   * @param quality - a number between 0 and 1 indicating image quality
   *                                     if the requested type is 'image/jpeg' or 'image/webp'
   * @returns Promise that resolves with the `Blob`
   */


  function imgSrcToBlob(src, type, crossOrigin, quality) {
    type = type || 'image/png';
    return loadImage(src, crossOrigin).then(imgToCanvas).then(function (canvas) {
      return canvasToBlob(canvas, type, quality);
    });
  }
  /**
   * Convert an `ArrayBuffer` to a `Blob`.
   *
   * Example:
   *
   * ```js
   * var blob = blobUtil.arrayBufferToBlob(arrayBuff, 'audio/mpeg');
   * ```
   *
   * @param buffer
   * @param type - the content type (optional)
   * @returns Blob
   */


  function arrayBufferToBlob(buffer, type) {
    return createBlob([buffer], type);
  }
  /**
   * Convert a `Blob` to an `ArrayBuffer`.
   *
   * Example:
   *
   * ```js
   * blobUtil.blobToArrayBuffer(blob).then(function (arrayBuff) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param blob
   * @returns Promise that resolves with the `ArrayBuffer`
   */


  function blobToArrayBuffer(blob) {
    return new Promise(function (resolve, reject) {
      var reader = new FileReader();

      reader.onloadend = function () {
        var result = reader.result || new ArrayBuffer(0);
        resolve(result);
      };

      reader.onerror = reject;
      reader.readAsArrayBuffer(blob);
    });
  }
  /**
   * Convert an `ArrayBuffer` to a binary string.
   *
   * Example:
   *
   * ```js
   * var myString = blobUtil.arrayBufferToBinaryString(arrayBuff)
   * ```
   *
   * @param buffer - array buffer
   * @returns binary string
   */


  function arrayBufferToBinaryString(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var length = bytes.byteLength;
    var i = -1;

    while (++i < length) {
      binary += String.fromCharCode(bytes[i]);
    }

    return binary;
  }
  /**
   * Convert a binary string to an `ArrayBuffer`.
   *
   * ```js
   * var myBuffer = blobUtil.binaryStringToArrayBuffer(binaryString)
   * ```
   *
   * @param binary - binary string
   * @returns array buffer
   */


  function binaryStringToArrayBuffer(binary) {
    var length = binary.length;
    var buf = new ArrayBuffer(length);
    var arr = new Uint8Array(buf);
    var i = -1;

    while (++i < length) {
      arr[i] = binary.charCodeAt(i);
    }

    return buf;
  }

  exports.createBlob = createBlob;
  exports.createObjectURL = createObjectURL;
  exports.revokeObjectURL = revokeObjectURL;
  exports.blobToBinaryString = blobToBinaryString;
  exports.base64StringToBlob = base64StringToBlob;
  exports.binaryStringToBlob = binaryStringToBlob;
  exports.blobToBase64String = blobToBase64String;
  exports.dataURLToBlob = dataURLToBlob;
  exports.blobToDataURL = blobToDataURL;
  exports.imgSrcToDataURL = imgSrcToDataURL;
  exports.canvasToBlob = canvasToBlob;
  exports.imgSrcToBlob = imgSrcToBlob;
  exports.arrayBufferToBlob = arrayBufferToBlob;
  exports.blobToArrayBuffer = blobToArrayBuffer;
  exports.arrayBufferToBinaryString = arrayBufferToBinaryString;
  exports.binaryStringToArrayBuffer = binaryStringToArrayBuffer;
  Object.defineProperty(exports, '__esModule', {
    value: true
  });
});
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var DAVE_SETTINGS = {
  BASE_URL: "https://staging.iamdave.ai",
  //test.iamdave.ai
  SIGNUP_API_KEY: null,
  ENTERPRISE_ID: null,
  CONVERSATION_ID: null,
  SPEECH_SERVER: null,
  SPEECH_RECOGNIZER: 'kaldi',
  SPEECH_MODEL_NAME: 'indian_english',
  USER_MODEL: 'person',
  USER_ID_ATTR: 'user_id',
  USER_EMAIL_ATTR: 'email',
  USER_PHONE_NUMBER_ATTR: 'mobile_number',
  LOGIN_ATTRS: null,
  DEFAULT_SYSTEM_RESPONSE: null,
  DEFAULT_USER_DATA: {
    "person_type": "visitor"
  },
  SESSION_MODEL: 'person_session',
  SESSION_ID: 'session_id',
  SESSION_USER_ID: 'user_id',
  DEFAULT_SESSION_DATA: {
    'location_dict': '{agent_info.ip}',
    'browser': '{agent_info.browser}',
    'os': '{agent_info.os}',
    'device_type': '{agent_info.device}'
  },
  LANGUAGE_CONVERSATION_MAP: {},
  INTERACTION_MODEL: 'interaction',
  INTERACTION_USER_ID: 'person_id',
  INTERACTION_SESSION_ID: 'session_id',
  INTERACTION_STAGE_ATTR: 'stage',
  DEFAULT_INTERACTION_DATA: {},
  LANGUAGE: null,
  VOICE_ID: null,
  MAX_SPEECH_DURATION: 10000,
  SESSION_ORIGIN: null,
  INTERACTION_ORIGIN: null,
  AVATAR_ID: null,
  AVATAR_MODEL: "avatar",
  LANGUAGE_AVATAR_MAP: {},
  GLB_ATTR: "glb_url",
  EVENT_MODEL: null,
  EVENT_SESSION_ID: 'session_id',
  EVENT_USER_ID: 'user_id',
  EVENT_DESCRIPTION: 'event',
  EVENT_WAIT_TIME: 'wait_time',
  EVENT_ORIGIN: 'origin',
  DEFAULT_EVENT_DATA: null,
  EVENT_TIMER: null,
  GOOGLE_MAP_KEY: null,
  RESPONSE_MODEL: null,
  RESPONSE_GLB_ATTR: "glb_url",
  RESPONSE_ATTR: "response",
  RESPONSE_PLAY_CREDITS_ATTR: "available_play_credits",
  RESPONSE_TOTAL_PLAYS_ATTR: "total_plays"
};

if (document.currentScript && isFinite(document.currentScript.src.split('/').slice(-2, -1)[0])) {
  DAVE_SETTINGS.asset_path = document.currentScript && document.currentScript.src.split('/').slice(0, -4).join('/') + '/';
} else {
  DAVE_SETTINGS.asset_path = document.currentScript && document.currentScript.src.split('/').slice(0, -3).join('/') + '/' || "https://chatbot-plugin.iamdave.ai/";
}

if (typeof daveAvatar !== 'undefined') {
  var DAVE_SCENE = new daveAvatar();
} else {
  var DAVE_SCENE = {};
}

var DAVE_HELP = null;

DAVE_SETTINGS.signup = function signup(data, callbackFunc, errorFunc, force) {
  if (DAVE_SETTINGS.getCookie("dave_started_signup") || DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("started_signup") || DAVE_SETTINGS.getCookie("authentication")) {
    console.warn("Another dave signup going on, so exiting");
    return;
  }

  if (!force && DAVE_SETTINGS.custom_signup && typeof DAVE_SETTINGS.custom_signup == 'function') {
    DAVE_SETTINGS.custom_signup(data, callbackFunc, errorFunc);
    return;
  }

  DAVE_SETTINGS.clearCookies();
  DAVE_SETTINGS.setCookie("dave_started_signup", true);
  var signupurl = DAVE_SETTINGS.BASE_URL + "/customer-signup/" + DAVE_SETTINGS.USER_MODEL; //Password string generator

  var randomstring = Math.random().toString(36).slice(-8);
  data = data || {};

  if (!data.email) {
    data.email = "ananth+" + randomstring + "@iamdave.ai";
  }

  for (var k in DAVE_SETTINGS.DEFAULT_USER_DATA) {
    if (!data[k]) {
      data[k] = DAVE_SETTINGS.DEFAULT_USER_DATA[k];
    }
  }

  djQ.ajax({
    url: signupurl,
    method: "POST",
    dataType: "json",
    contentType: "json",
    withCredentials: true,
    headers: {
      "Content-Type": "application/json",
      "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
      "X-I2CE-SIGNUP-API-KEY": DAVE_SETTINGS.SIGNUP_API_KEY
    },
    data: JSON.stringify(data),
    success: function success(data) {
      HEADERS = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
        "X-I2CE-USER-ID": data[DAVE_SETTINGS.USER_ID_ATTR],
        "X-I2CE-API-KEY": data.api_key
      };
      DAVE_SETTINGS.setCookie("dave_authentication", JSON.stringify(HEADERS), 24);
      DAVE_SETTINGS.setCookie("dave_user_id", data[DAVE_SETTINGS.USER_ID_ATTR]);
      DAVE_SETTINGS.setCookie("dave_conversation_id", DAVE_SETTINGS.CONVERSATION_ID);
      DAVE_SETTINGS.clearCookie("dave_started_signup");
      DAVE_SETTINGS.clearCookie("started_signup");
      DAVE_SETTINGS.clearCookie("authentication");
      DAVE_SETTINGS.clearCookie("user_id");
      DAVE_SETTINGS.clearCookie("conversation_id");
      DAVE_SETTINGS.clearCookie("engagement_id");
      DAVE_SETTINGS.clearCookie("dave_history");
      DAVE_SETTINGS.initialize_session();

      if (callbackFunc && typeof callbackFunc == 'function') {
        callbackFunc(data);
      }

      DAVE_SETTINGS.predict();
    },
    error: function error(r, e) {
      console.error(e);
      DAVE_SETTINGS.clearCookie("started_signup");
      DAVE_SETTINGS.clearCookie("dave_started_signup");

      if (errorFunc && typeof callbackFunc == 'function') {
        errorFunc(r, e);
      }
    }
  });
}; // Login function


DAVE_SETTINGS.login = function login(user_name, password, callbackFunc, errorFunc, unauthorizedFunc, attrs) {
  var params = {};
  params["user_id"] = user_name;
  params["password"] = password;
  params["roles"] = DAVE_SETTINGS.USER_MODEL;
  params["attrs"] = attrs || DAVE_SETTINGS.LOGIN_ATTRS || [DAVE_SETTINGS.USER_ID_ATTR, DAVE_SETTINGS.USER_EMAIL_ATTR, DAVE_SETTINGS.USER_PHONE_NUMBER_ATTR];
  params["enterprise_id"] = DAVE_SETTINGS.ENTERPRISE_ID;
  DAVE_SETTINGS.ajaxRequestWithData("/dave/oauth", "POST", JSON.stringify(params), function (data) {
    var HEADERS = {
      "Content-Type": "application/json",
      "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
      "X-I2CE-USER-ID": data.user_id,
      "X-I2CE-API-KEY": data.api_key
    };
    DAVE_SETTINGS.setCookie("dave_authentication", JSON.stringify(HEADERS), 24);
    DAVE_SETTINGS.setCookie("dave_user_id", data.user_id);
    DAVE_SETTINGS.setCookie("dave_conversation_id", DAVE_SETTINGS.CONVERSATION_ID);
    DAVE_SETTINGS.clearCookie("dave_started_signup");
    DAVE_SETTINGS.clearCookie("dave_engagement_id");
    DAVE_SETTINGS.setCookie("dave_logged_in", true);
    DAVE_SETTINGS.clearCookie("dave_history");
    DAVE_SETTINGS.initialize_session();

    if (callbackFunc && typeof callbackFunc == 'function') {
      callbackFunc(data);
    }

    DAVE_SETTINGS.predict();
    DAVE_SETTINGS.history(null, null, null, true);
  }, errorFunc, unauthorizedFunc);
}; // You can get the column names of any model with the following API
// DAVE_SETTINGS.ajaxRequestWithData("/attributes/<model_name>/name", "GET", 
//
//


DAVE_SETTINGS.patch_user = function patch_user(params, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.patch_user, [params, callbackFunc, errorFunc, repeats || 0], "dave_user_id")) {
    return;
  } // e.g. patch_user({"pincode": <pincode>, "name": <name of user>, "company_name": <company_name>}, function(data) {console.log(data)});
  // In the response you will get the warehouse_id


  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  params = params || {};

  if (params.person_name && params.email && params.mobile_number) {
    params["signed_up"] = true;
  }

  DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.USER_MODEL + "/" + user_id, "PATCH", JSON.stringify(params), callbackFunc, errorFunc);
};

DAVE_SETTINGS.create_session = function create_session(params, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.create_session, [params, callbackFunc, errorFunc, repeats || 0], "dave_user_id")) {
    return;
  }

  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  params[DAVE_SETTINGS.SESSION_USER_ID] = user_id;

  for (var k in DAVE_SETTINGS.DEFAULT_SESSION_DATA) {
    params[k] = DAVE_SETTINGS.DEFAULT_SESSION_DATA[k];
  }

  DAVE_SETTINGS.get_url_params(params);

  if (DAVE_SETTINGS.SESSION_ORIGIN) {
    params[DAVE_SETTINGS.SESSION_ORIGIN] = window.location.href.split(/[?#]/)[0];
  }

  DAVE_SETTINGS.execute_custom_callback("before_create_session", [params]);
  DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.SESSION_MODEL, "POST", JSON.stringify(params), function (data) {
    DAVE_SETTINGS.setCookie("dave_session_id", data[DAVE_SETTINGS.SESSION_ID]);
    DAVE_SETTINGS.execute_custom_callback("after_create_session", [data, params]);

    if (callbackFunc && _typeof(callbackFunc)) {
      callbackFunc(data);
    }
  }, errorFunc);
  DAVE_SETTINGS.create_event("created", {
    'event_type': 'auto'
  });
}; //
//
//

/* API of the Data response for chat
{
    "name": "sr_greeting",
    "title": "Sr Greeting",
    "whiteboard": null,
    "customer_state": "cs_hi",
    "placeholder_aliases": {},
    "state_options": {     // These are the quick access buttons
        "cs_xpress_cash_loan_repayment": "Method of repayment Personal Loan",
        "cs_car_loan_minimum_salary": "Car Loan minimum salaried individual",
        "cs_xpress_home_loan_tenure": "Home Loan tenure",
        "cs_car_loan_eligibility": "Car Loan eligibility",
        "cs_term_deposit_schemes": "Term deposit schemes",
        "cs_xpress_msme_loan_security": "Security for MSME Loan",
        "cs_xpress_home_loan_interest_rate": "Home Loan interest rate",
        "cs_personal_loan_features": "Personal Loan features"
    },
    "to_state_function": {
        "function": "_function_find_customer_state"
    },
    "placeholder": "Hello, hope you're having a good day!",     // This will go into the speech bubble, NOTE: This could be HTML, so it should be HTML safe
    "whiteboard_title": "Sr Greeting",
    "wait": 10000,    //// Time in millisecs to wait for the followup
    "data": {
         "_follow_ups": [  // nudges
            "nu_follow_up_state_1",
            "nu_follow_up_state_2"
         ],
         "response_type": "image",   // Other options are "video", "options", "thumbnails", "url", "form", "template", "to_timestamp",
         "template": <name of template (default is none)>
         "title"; <title to give the image, video or url or template>
         "sub_title"; <sub_title to give to the template, image or video>
         "description": "this description can be used in the template",
         "image": <url to the image>, //optional
         "video": <url to the video>,
         "video_start_timestamp": HH:MM:SS
         "video_stop_timestamp": HH:MM:SS
         "reply_to_id": <id of the response to which we want to provide the data>,
         "thumbnails": [
          {
             "image": <url to image>,
             "url": <url to redirect to if clicked>,
             "title": <title of the image>,
             "sub_title": <sub title of the image>,
             "description": <description of the image>,
             "target": <open in same window or another window (self or _blank)"
          }, ....
         ],
         "slideshow": [
          {
             "image": <url to image>,
             "title": <title of the image>,
             "sub_title": <sub title of the image>,
             "description": <description of the image>,
             "url": <url to redirect to if clicked>,
             "target": <open in same window or another window (self or _blank)"
          }, ....
         ],
         "url": <url to redirect to in case of clicking on the image or video>,
         "target": <target of the url, default "_blank" other option is self>,
         "form": [
             {
                "name": <name of field to send in json>,
                "title": <title of field, defaults to name>,
                "placeholder": <placeholder of field>,
                "ui_element": <text, textarea, number, switch, datetime-local, mobile_number, email, select, multiselect, tags, multiselect-tags>
                "error": <error message if required, can be null or not available>,
                "min": <min value in case of ui element to be number or date>,
                "max": <min value in case of ui element to be number or date>,
                "step": <resolution in seconds in case of datetime>,
                "required": true/ or false,
             }, ....
         ],    // The data collected from the form will be sent as json string in customer_response
         "customer_response": <custom title for the submit button>
         "options": {
             <customer_state>: <customer_response>,
             <customer_state>: <customer_response>
         },
         "options": [
            <customer_response option>,
            <customer_response option>
         ],
         "options": [
          {"customer_state": <customer_response>}
          {"customer_state": <customer_response>}
          {"customer_state": <customer_response>}
         ],
         "customer_state": <customer state to send in case there are options to select from>,
    },
    "options": null,
    "engagement_id": "YXNsa2RmamFsc2tqZGZkZXBsb3ltZW50IGtpb3NrIGthbm5hZGE_",
    "response_id": <response_id>
}*/
//
// params will contain one or both of
// "customer_state", and/or "customer_response"
// If user clicks on the Quick Access buttons, then both will be sent
// If there are options, then the selected option will be sent along with the customer_state sent in data
// If there is any text input then only customer_response will be sent.


DAVE_SETTINGS.on_having_response = function on_having_response(data, params, callbackFunc, unknownFunc, skip_history) {
  DAVE_SETTINGS.setCookie("dave_latest_response_id", data.response_id || DAVE_SETTINGS.generate_random_string(8));
  DAVE_SETTINGS.setCookie("dave_system_response", data.name);
  console.debug("Response Id: " + data.response_id);

  if (data.console) {
    var _iterator = _createForOfIteratorHelper(data.console.split('\n')),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var k = _step.value;

        if (k.indexOf('DEBUG') >= 0) {
          console.debug(k);
        } else if (k.indexOf('WARN') >= 0) {
          console.warn(k);
        } else if (k.indexOf('ERROR') >= 0) {
          console.error(k);
        } else {
          console.log(k);
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  }

  if (DAVE_SCENE && DAVE_SCENE.play && DAVE_SCENE.loaded && DAVE_SCENE.playable && DAVE_SCENE.opened && data.response_channels && data.response_channels.voice && data.response_channels.frames && data.response_channels.shapes) {
    DAVE_SCENE.play(data.response_channels);
  }

  if (!skip_history) {
    var history = DAVE_SETTINGS.getCookie("dave_history") || {
      'history': []
    };
    var customer_state = params['customer_state'] || data['customer_state'] || "__init__";

    if (customer_state != '__init__' && !data.hide_in_customer_history) {
      history["history"].push({
        "direction": "user",
        "customer_response": params["customer_response"] || DAVE_SETTINGS.toTitleCase(params["customer_state"] || ''),
        "customer_state": customer_state,
        "user_id": DAVE_SETTINGS.getCookie("dave_user_id"),
        "user_model": DAVE_SETTINGS.USER_MODEL,
        "timestamp": data['start_timestamp'] || new Date(),
        "response_id": data["response_id"] || DAVE_SETTINGS.generate_random_string(8)
      });
    }

    if (data.show_in_history) {
      history["history"].push({
        "direction": "system",
        "name": data["name"],
        "placeholder": data['placeholder'],
        "data": data["data"] || {},
        "title": data["title"],
        "options": data["options"],
        "state_options": data["state_options"],
        "whiteboard": data["whiteboard"],
        "placeholder_aliases": data["placeholder_aliases"],
        "timestamp": data['timestamp'],
        "response_id": data["response_id"]
      });
    }

    DAVE_SETTINGS.setCookie("dave_history", history);
  }

  if (data.customer_state == 'unknown_customer_state' && unknownFunc && typeof unknownFunc == 'function') {
    unknownFunc(data);
    return;
  }

  if (callbackFunc && typeof callbackFunc == 'function') {
    callbackFunc(data);
  }
};

DAVE_SETTINGS.chat = function chat(params, callbackFunc, errorFunc, unknownFunc, discard_local, repeats) {
  repeats = repeats || 0;
  DAVE_SETTINGS.execute_custom_callback('on_chat_start', [params.customer_state || params.customer_response || "sent chat message", params]);

  if (DAVE_SETTINGS.DEFAULT_SYSTEM_RESPONSE && !params.customer_state && !params.customer_response && !discard_local) {
    DAVE_SETTINGS.DEFAULT_SYSTEM_RESPONSE.response_id = DAVE_SETTINGS.generate_random_string(8);
    DAVE_SETTINGS.on_having_response(DAVE_SETTINGS.DEFAULT_SYSTEM_RESPONSE, params, callbackFunc, unknownFunc, true);
    DAVE_SETTINGS.chat(params, null, null, null, true);
    return;
  }

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.chat, [params, callbackFunc, errorFunc, discard_local, repeats || 0], "dave_user_id")) {
    return;
  }

  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  var conversation_id = params['conversation_id'] || DAVE_SETTINGS.CONVERSATION_ID || DAVE_SETTINGS.getCookie('dave_conversation_id') || DAVE_SETTINGS.getCookie('conversation_id');
  delete params['conversation_id'];
  var engagement_id = params['engagement_id'] || DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id');

  if (engagement_id) {
    params["engagement_id"] = engagement_id;
  }

  var system_response = params['system_response'] || DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response');

  if (system_response && (params['customer_state'] || params['customer_response'])) {
    params["system_response"] = system_response;
  } else {
    delete params["system_response"];
  }

  if (DAVE_SETTINGS.VOICE_ID && !params['voice_id']) {
    params["voice_id"] = DAVE_SETTINGS.VOICE_ID;
  }

  if (DAVE_SETTINGS.LANGUAGE && !params['language']) {
    params["language"] = DAVE_SETTINGS.LANGUAGE;
  }

  if (DAVE_SETTINGS.AVATAR_ID || params['avatar_id']) {
    params["synthesize_now"] = true;
    params["csv_response"] = true;
  }

  params["origin"] = window.location.href;
  DAVE_SETTINGS.create_event("queried");

  if (params.customer_state || params.customer_response) {
    DAVE_SETTINGS.did_interact = true;
  }

  return DAVE_SETTINGS.ajaxRequestWithData("/conversation/" + conversation_id + "/" + user_id, "POST", JSON.stringify(params), function (data) {
    DAVE_SETTINGS.setCookie("dave_engagement_id", data.engagement_id);
    DAVE_SETTINGS.on_having_response(data, params, callbackFunc, unknownFunc);
    DAVE_SETTINGS.execute_custom_callback('on_response', [data.name || "received chat response", data.customer_state, data, params]);
  }, errorFunc);
};

DAVE_SETTINGS.set_conversation = function set_conversation(conversation_id, callbackFunc, errorFuc) {
  if (conversation_id == DAVE_SETTINGS.CONVERSATION_ID) {
    return;
  }

  DAVE_SETTINGS.CONVERSATION_ID = conversation_id;
  DAVE_SETTINGS.setCookie("dave_conversation_id", conversation_id);
  DAVE_SETTINGS.clearCookie('dave_keywords');
  DAVE_SETTINGS.clearCookie('dave_keyword_time');
  DAVE_SETTINGS.clearCookie('dave_engagement_id');
  DAVE_SETTINGS.chat({}, callbackFunc, errorFunc);
}; // get history function
// e.g. response data
//
// Check direction attribute to check if user or system
// Use placeholder if system and customer_response if user
//{
//    "history": [
//        {
//            "data": {},
//            "direction": "system",
//            "options": null,
//            "placeholder": "sr_init",
//            "placeholder_aliases": {},
//            "timestamp": "Sat, 09 Jan 2021 16:10:55 GMT",
//            "title": "Sr Agriculturists",
//            "whiteboard": null
//        },
//        {
//            "customer_response": "Response 1",
//            "customer_state": {
//                "unknown_customer_state": []
//            },
//            "direction": "user",
//            "timestamp": "Sat, 09 Jan 2021 16:11:57 GMT",
//            "user_id": "ananth+kabank@i2ce.in",
//            "user_model": "admin"
//        },
//        {
//            "data": {},
//            "direction": "system",
//            "options": null,
//            "placeholder": "I'm not sure what you mean. Can you rephrase that?",
//            "placeholder_aliases": {},
//            "timestamp": "Sat, 09 Jan 2021 16:11:57 GMT",
//            "title": "Sr Agriculturists",
//            "whiteboard": null
//        },
//        {
//            "customer_response": "Repsonse 2",
//            "customer_state": {
//                "unknown_customer_state": []
//            },
//            "direction": "user",
//            "timestamp": "Sat, 09 Jan 2021 16:13:40 GMT",
//            "user_id": "ananth+kabank@i2ce.in",
//            "user_model": "admin"
//        },
//        ....
//    ]
//}


DAVE_SETTINGS.history = function history(callbackFunc, errorFunc, reconcileFunction, discard_local, repeats) {
  repeats = repeats || 0;

  if (DAVE_SETTINGS.getCookie("dave_history") && !discard_local) {
    if (callbackFunc && typeof callbackFunc == "function") {
      callbackFunc(DAVE_SETTINGS.getCookie("dave_history"));
    }

    return DAVE_SETTINGS.history(null, null, reconcileFunction || callbackFunc, true);
  }

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.history, [callbackFunc, errorFunc, reconcileFunction, discard_local, repeats || 0], "dave_user_id")) {
    return;
  }

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.history, [callbackFunc, errorFunc, reconcileFunction, discard_local, repeats || 0], "dave_engagement_id")) {
    return;
  }

  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  var engagement_id = DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id');
  DAVE_SETTINGS.ajaxRequest("/conversation-history/" + DAVE_SETTINGS.CONVERSATION_ID + "/" + user_id + "/" + engagement_id, "GET", function (data) {
    var history = DAVE_SETTINGS.getCookie("dave_history") || {
      "history": []
    };
    nh = data['history'].filter(function (h) {
      return !history['history'].some(function (i) {
        if (h.response_id == i.response_id) {
          return true;
        } else {
          return false;
        }
      });
    });
    DAVE_SETTINGS.setCookie("dave_history", data);
    callbackFunc = callbackFunc || reconcileFunction;

    if (callbackFunc && typeof callbackFunc == "function") {
      callbackFunc({
        'history': nh
      });
    }
  }, errorFunc);
}; // DAVE_SETTINGS.feedback({"usefulness_rating": <rating>, "accuracy_rating": <rating>, "feedback": <feedback>}, null, function(){ console.log("Callback") }) to give accuracy rating for entire conversation
// DAVE_SETTINGS.feedback({"response_rating": <rating (true or false)>}, <response_id>, function(){ console.log("Callback") } ) to give rating for specific response
// DAVE_SETTINGS.feedback({"accuracy_rating": <rating (true or false)>}, <response_id>, function(){ console.log("Callback") } ) to give rating for recognition result


DAVE_SETTINGS.feedback = function send_feedback(rating, response_id, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.feedback, [rating, response_id, callbackFunc, errorFunc, repeats || 0], "dave_engagement_id")) {
    return;
  }

  var engagement_id = DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id');

  for (var k in rating) {
    if (typeof rating[k] == "boolean") {
      rating[k] = rating[k] ? 5 : 1;
    }
  }

  DAVE_SETTINGS.ajaxRequestWithData("/conversation-feedback/dave/" + engagement_id + (response_id ? "/" + response_id : ""), "PATCH", JSON.stringify(rating), function (data) {
    DAVE_SETTINGS.execute_custom_callback("after_submit_feedback", [rating, response_id]);

    if (callbackFunc && typeof callbackFunc == "function") {
      callbackFunc(data);
    }
  }, errorFunc);
}; // DAVE_SETTINGS.predict(<partial keywords>, function(response) { console.log(response)})
// response of type: [{sentence match: customer_state}, {sentence_match: customer_state}] 


DAVE_SETTINGS.predict = function predict(typed, callbackFunc, errorFunc, conversation_id, repeats) {
  typed = typed || "";
  repeats = repeats || 0;

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.predict, [typed, callbackFunc, errorFunc, conversation_id, repeats || 0], "dave_user_id")) {
    return;
  }

  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  conversation_id = conversation_id || DAVE_SETTINGS.CONVERSATION_ID || DAVE_SETTINGS.getCookie('dave_conversation_id') || DAVE_SETTINGS.getCookie('conversation_id');

  function do_set(data) {
    DAVE_SETTINGS.setCookie("dave_keywords", data);
    DAVE_SETTINGS.setCookie("dave_keyword_time", djQ.now());
  }

  typed = typed.split(" ").filter(function (el) {
    return el.length >= 3;
  });

  function do_keywords(data, noset) {
    if (!noset) {
      do_set(data);
    }

    response = [];
    resp = {};

    if (typed.length <= 0) {
      if (callbackFunc) {
        callbackFunc(response);
      }

      return response;
    }

    for (var k in data.keywords) {
      var d = data.keywords[k];

      if (!d[0]) {
        continue;
      }

      var d0 = d[0].split(" ");

      for (var v = 0; v < d0.length; v++) {
        var d1 = d0[v];

        for (var tv = 0; tv < typed.length; tv++) {
          var t = typed[tv];

          if (typeof d1 == 'string' && d1.toLowerCase() == t.toLowerCase()) {
            var r = {};
            r[d[1]] = d[2];

            if (!resp[d[2]]) {
              response.push(r);
              resp[d[2]] = d[1];
            }

            break;
          }
        }
      }
    }

    if (callbackFunc) {
      callbackFunc(response);
    }

    return response;
  }

  var keywords = DAVE_SETTINGS.getCookie("dave_keywords");
  var ret = do_keywords(keywords, true);

  if (DAVE_SETTINGS.getCookie("dave_keyword_time") > djQ.now() - 5 * 60 * 60 * 1000) {
    return ret;
  }

  DAVE_SETTINGS.ajaxRequest("/conversation-keywords/" + DAVE_SETTINGS.CONVERSATION_ID, "GET", do_set, errorFunc);
  return ret;
}; // Below this are all internal functions


DAVE_SETTINGS.iupdate_user = function iupdate_user(params, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.iupdate_user, [params, callbackFunc, errorFunc, repeats || 0])) {
    return;
  } // Used to update session duration and session number


  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  params = params || {};
  params['_async'] = true;
  DAVE_SETTINGS.ajaxRequestWithData("/iupdate/" + DAVE_SETTINGS.USER_MODEL + "/" + user_id, "PATCH", JSON.stringify(params), callbackFunc, errorFunc);
};

DAVE_SETTINGS.iupdate_session = function iupdate_session(params, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.iupdate_session, [params, callbackFunc, errorFunc, repeats || 0])) {
    return;
  } // Used to update session duration and session number


  var session_id = DAVE_SETTINGS.getCookie('dave_session_id') || DAVE_SETTINGS.getCookie('session_id');
  params = params || {};
  params['_async'] = true;
  DAVE_SETTINGS.ajaxRequestWithData("/iupdate/" + DAVE_SETTINGS.SESSION_MODEL + "/" + session_id, "PATCH", JSON.stringify(params), callbackFunc, errorFunc);
};

DAVE_SETTINGS.create_interaction = function create_interaction(params, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.create_interaction, [params, callbackFunc, errorFunc, repeats || 0])) {
    return;
  } // Used to update session duration and session number


  var session_id = DAVE_SETTINGS.getCookie('dave_session_id') || DAVE_SETTINGS.getCookie('session_id');
  var user_id = DAVE_SETTINGS.getCookie("dave_user_id") || DAVE_SETTINGS.getCookie("user_id");
  params = params || {};
  params['_async'] = true;
  params[DAVE_SETTINGS.INTERACTION_SESSION_ID] = session_id;
  params[DAVE_SETTINGS.INTERACTION_USER_ID] = user_id;
  params["conversation_id"] = DAVE_SETTINGS.CONVERSATION_ID;

  if (DAVE_SETTINGS.INTERACTION_ORIGIN) {
    params[DAVE_SETTINGS.INTERACTION_ORIGIN] = window.location.href;
  }

  for (var k in DAVE_SETTINGS.DEFAULT_INTERACTION_DATA) {
    params[k] = DAVE_SETTINGS.DEFAULT_INTERACTION_DATA[k];
  }

  DAVE_SETTINGS.execute_custom_callback("before_create_interaction", [params]);
  DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.INTERACTION_MODEL, "POST", JSON.stringify(params), function (data) {
    DAVE_SETTINGS.execute_custom_callback("after_create_interaction", [data, params]);

    if (callbackFunc && _typeof(callbackFunc)) {
      callbackFunc(data);
    }
  }, errorFunc);
};

DAVE_SETTINGS.event_callbacks = {};

DAVE_SETTINGS.create_event = function create_event(description, params, callbackFunc, errorFunc, timestamp, repeats) {
  timestamp = timestamp || Math.floor(new Date().getTime() / 1000);

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.create_event, [description, params, callbackFunc, errorFunc, timestamp, repeats || 0], 'dave_session_id')) {
    return;
  }

  params = params || {};

  if (['opened', 'resumed', 'closed', 'minimized', 'queried', 'created'].indexOf(description) >= 0) {
    params[DAVE_SETTINGS.INTERACTION_STAGE_ATTR] = description;
    DAVE_SETTINGS.create_interaction(params);
  }

  if (DAVE_SETTINGS.cancel_events && DAVE_SETTINGS.cancel_events[description]) {
    for (var k in DAVE_SETTINGS.cancel_events[description]) {
      console.log("Cancelling timer for event " + description);
      var t = DAVE_SETTINGS.cancel_events[description][k];

      if (t) {
        clearTimeout(DAVE_SETTINGS.event_timers[t]);
        delete DAVE_SETTINGS.event_timers[t];
      }
    }

    DAVE_SETTINGS.cancel_events[description] = [];
  }

  if (!DAVE_SETTINGS.EVENT_MODEL || params["_skip_post"]) {
    return;
  }

  var session_id = DAVE_SETTINGS.getCookie('dave_session_id') || DAVE_SETTINGS.getCookie('session_id');
  var user_id = DAVE_SETTINGS.getCookie("dave_user_id") || DAVE_SETTINGS.getCookie("user_id");
  params['_async'] = true;
  params[DAVE_SETTINGS.EVENT_DESCRIPTION] = description;
  params[DAVE_SETTINGS.EVENT_SESSION_ID] = session_id;
  params[DAVE_SETTINGS.EVENT_USER_ID] = user_id;
  params["conversation_id"] = DAVE_SETTINGS.CONVERSATION_ID;
  params[DAVE_SETTINGS.EVENT_WAIT_TIME] = timestamp - DAVE_SETTINGS.EVENT_TIMER;
  DAVE_SETTINGS.EVENT_TIMER = timestamp;

  if (DAVE_SETTINGS.EVENT_ORIGIN) {
    params[DAVE_SETTINGS.EVENT_ORIGIN] = window.location.href;
  }

  var ed = DAVE_SETTINGS.DEFAULT_EVENT_DATA || DAVE_SETTINGS.DEFAULT_INTERACTION_DATA;

  for (var _k in ed) {
    params[_k] = ed[_k];
  }

  DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.EVENT_MODEL, "POST", JSON.stringify(params), function (data) {
    if (DAVE_SETTINGS.event_callbacks[description]) {
      DAVE_SETTINGS.event_callbacks[description] = DAVE_SETTINGS.makeList(DAVE_SETTINGS.event_callbacks[description], []);

      var _iterator2 = _createForOfIteratorHelper(DAVE_SETTINGS.event_callbacks[description]),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var _k2 = _step2.value;

          _k2(description, params.event_type);
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
    }

    if (DAVE_SETTINGS.event_callbacks['__ALL__']) {
      DAVE_SETTINGS.event_callbacks['__ALL__'] = DAVE_SETTINGS.makeList(DAVE_SETTINGS.event_callbacks['__ALL__'], []);

      var _iterator3 = _createForOfIteratorHelper(DAVE_SETTINGS.event_callbacks['__ALL__']),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var _k3 = _step3.value;

          _k3(description, params.event_type);
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }
    }

    if (callbackFunc && typeof callbackFunc == "function") {
      callbackFunc(data);
    }
  }, errorFunc);
};

DAVE_SETTINGS.register_event = function register_event(identifier, description, event_type, params, beforeEventCallback, afterEventCallback) {
  event_type = event_type || 'click';
  djQ(identifier).on(event_type, function () {
    if (beforeEventCallback && typeof beforeEventCallback == 'function') {
      var d = beforeEventCallback(this, description, params, event_type);

      if (d) {
        description = d;
      }
    }

    params['event_type'] = event_type;
    DAVE_SETTINGS.create_event(description, params, afterEventCallback);
  });
};

DAVE_SETTINGS.play_again = function () {
  if (DAVE_SCENE.snd) {
    DAVE_SCENE.playAgain();
  }
};

DAVE_SETTINGS.execute_custom_callback = function execute_custom_callback(cb, args, base) {
  args = args || [];
  base = base || DAVE_SETTINGS;
  console.debug("Executing custom callback " + cb);
  base[cb] = DAVE_SETTINGS.makeList(base[cb], []);

  var _iterator4 = _createForOfIteratorHelper(base[cb]),
      _step4;

  try {
    for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
      var k = _step4.value;

      if (k && typeof k == 'function') {
        try {
          var r = k.apply({}, args);

          if (r === false) {
            return r;
          }
        } catch (err) {
          console.error(err);
        }
      }
    }
  } catch (err) {
    _iterator4.e(err);
  } finally {
    _iterator4.f();
  }

  return true;
};

DAVE_SETTINGS.register_custom_callback = function register_custom_callback(cb, f, base) {
  base = base || DAVE_SETTINGS;
  base[cb] = DAVE_SETTINGS.makeList(base[cb], []);
  base[cb].push(f);
};

DAVE_SETTINGS.on_maximize = function () {
  DAVE_SCENE.opened = true;

  if (DAVE_SCENE.opened_once) {
    DAVE_SETTINGS.create_event('resumed');
  } else {
    DAVE_SETTINGS.create_event('opened');
  }

  DAVE_SETTINGS.opened_time = new Date();
  DAVE_SCENE.unmute();
  DAVE_SCENE.opened_once = true;
  return DAVE_SETTINGS.execute_custom_callback('on_maximize_custom');
};

DAVE_SETTINGS.on_close = function () {
  DAVE_SCENE.opened = false;
  DAVE_SETTINGS.create_event('closed');

  if (DAVE_SETTINGS.opened_time instanceof Date) {
    DAVE_SETTINGS.iupdate_session({
      'session_duration': (new Date().getTime() - DAVE_SETTINGS.opened_time.getTime()) / 1000
    }, function () {
      DAVE_SETTINGS.opened_time = null;
    });
  }

  DAVE_SCENE.stop();
  return DAVE_SETTINGS.execute_custom_callback('on_close_custom');
};

DAVE_SETTINGS.register_event_callback = function (func, event_type) {
  event_type = event_type || '__ALL__';

  if (typeof func == "function") {
    if (!DAVE_SETTINGS.event_callbacks[event_type]) {
      DAVE_SETTINGS.event_callbacks[event_type] = [];
    }

    DAVE_SETTINGS.event_callbacks[event_type].push(func);
  }
};

DAVE_SETTINGS.on_minimize = function () {
  if (DAVE_SCENE.is_mobile) {
    DAVE_SETTINGS.on_close();
  }

  DAVE_SETTINGS.create_event('minimized');
  DAVE_SCENE.mute();
  return DAVE_SETTINGS.execute_custom_callback('on_minimize_custom');
};

DAVE_SETTINGS.getCookie = function (key, def) {
  var result = window.localStorage.getItem(key);
  return DAVE_SETTINGS.get_value(result, def);
};

DAVE_SETTINGS.get_value = function (value, def) {
  if (value === null || value === undefined) {
    return def === 'undefined' ? null : def;
  }

  try {
    return JSON.parse(value);
  } catch (err) {
    return value;
  }

  return value;
};

DAVE_SETTINGS.setCookie = function (key, value, hoursExpire) {
  if (hoursExpire === undefined) {
    hoursExpire = 24;
  }

  if (value === undefined) {
    return;
  }

  if (_typeof(value) == "object") {
    value = JSON.stringify(value);
  }

  if (hoursExpire < 0 || value === null) {
    window.localStorage.removeItem(key);
  } else {
    window.localStorage.setItem(key, value);
  }
};

DAVE_SETTINGS.clearCookie = function (key) {
  var r = DAVE_SETTINGS.getCookie(key, null);
  DAVE_SETTINGS.setCookie(key, null, -1);
  return r;
};

DAVE_SETTINGS.clearCookies = function () {
  // This is for older versions so that we can do migration, now it is not required
  //let cks = ["conversation_id", "authentication", "session_id", "keywords", "keyword_time", "user_id", "started_signup", "engagement_id", "history"];
  //for (let key in cks) {
  //    DAVE_SETTINGS.clearCookie(cks[key]);
  //}
  for (var key in window.localStorage) {
    if (key.startsWith('dave_')) {
      DAVE_SETTINGS.clearCookie(key);
    }
  }
};

DAVE_SETTINGS.Trim = function (strValue) {
  return strValue.replace(/^\s+|\s+$/g, '');
};

DAVE_SETTINGS.toTitleCase = function (str) {
  return str.replace(/_/g, ' ').replace(/(?:^|\s)\w/g, function (match) {
    return match.toUpperCase();
  });
};

DAVE_SETTINGS.makeList = function (inp, def) {
  if (inp == undefined || inp == null) {
    return def;
  }

  if (Array.isArray(inp)) {
    return inp;
  }

  return [inp];
};

DAVE_SETTINGS.generate_random_string = function (string_length) {
  var random_string = '';
  var random_ascii;

  for (var i = 0; i < string_length; i++) {
    random_ascii = Math.floor(Math.random() * 25 + 97);
    random_string += String.fromCharCode(random_ascii);
  }

  return random_string;
};

DAVE_SETTINGS.ajaxRequestSync = function (URL, METHOD, callbackFunc, errorFunc, async, unauthorized, HEADERS) {
  HEADERS = HEADERS || DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication");

  if (!unauthorized) {
    unauthorized = function unauthorized() {
      DAVE_SETTINGS.clearCookie('dave_user_id');
      DAVE_SETTINGS.clearCookie('dave_authentication');
      DAVE_SETTINGS.signup({}, function () {
        DAVE_SETTINGS.ajaxRequestSync(URL, METHOD, callbackFunc, errorFunc, async, function () {
          console.error("Could not signup do DaveAI chatbot! Please contact customer support!");

          if (typeof DAVE_SETTINGS.signup_failed == 'function') {
            DAVE_SETTINGS.signup_failed();
          }
        }, HEADERS);
      });
    };
  }

  djQ.ajax({
    url: DAVE_SETTINGS.BASE_URL + URL,
    method: (METHOD || "GET").toUpperCase(),
    dataType: "json",
    contentType: "json",
    async: async || false,
    headers: HEADERS,
    statusCode: {
      401: unauthorized,
      404: unauthorized
    }
  }).done(function (data) {
    result = data;

    if (callbackFunc) {
      callbackFunc(data);
    }
  }).fail(function (err) {
    if (errorFunc) {
      errorFunc(err);
    }
  });
};

DAVE_SETTINGS.ajaxRequest = function (URL, METHOD, callbackFunc, errorFunc, unauthorized, HEADERS) {
  return DAVE_SETTINGS.ajaxRequestSync(URL, METHOD, callbackFunc, errorFunc, true, unauthorized, HEADERS);
};

DAVE_SETTINGS.ajaxRequestWithData = function (URL, METHOD, DATA, callbackFunc, errorFunc, unauthorized, HEADERS, retries) {
  HEADERS = HEADERS || DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication");
  METHOD = (METHOD || "GET").toUpperCase();
  retries = retries || 0;
  var defaultData = "";

  if (DATA) {
    defaultData = DATA;
  }

  if (["POST", "PATCH", "DELETE"].indexOf(METHOD) >= 0 && typeof defaultData != 'string') {
    defaultData = JSON.stringify(defaultData);
  }

  if (!unauthorized) {
    unauthorized = function unauthorized() {
      if (retries <= 5) {
        setTimeout(function () {
          DAVE_SETTINGS.clearCookie('dave_user_id');
          DAVE_SETTINGS.clearCookie('dave_authentication');
          DAVE_SETTINGS.signup({}, DAVE_SETTINGS.ajaxRequestWithData(URL, METHOD, DATA, callbackFunc, errorFunc, function () {
            console.error("Could not signup do DaveAI chatbot! Please contact customer support!");

            if (typeof DAVE_SETTINGS.signup_failed == 'function') {
              DAVE_SETTINGS.signup_failed();
            }
          }, HEADERS, retries + 1));
        }, (retries + 1) * (retries + 1) * 5000);
      } else {
        console.error("Failed to authorize DaveAI plugin after 5 attempts");
      }
    };
  }

  return djQ.ajax({
    url: DAVE_SETTINGS.BASE_URL + URL,
    method: (METHOD || "GET").toUpperCase(),
    dataType: "json",
    contentType: "application/json",
    headers: HEADERS,
    data: defaultData,
    statusCode: {
      401: unauthorized,
      404: unauthorized
    }
  }).done(function (data) {
    if (callbackFunc) {
      callbackFunc(data);
    }
  }).fail(function (err) {
    if (errorFunc) {
      errorFunc(err);
    }
  });
};

DAVE_SETTINGS.initialize_session = function (force, repeats) {
  if (!force && DAVE_SETTINGS.custom_session && typeof DAVE_SETTINGS.custom_session == 'function') {
    DAVE_SETTINGS.custom_session();
    return;
  }

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.initialize_session, [force, repeats || 0])) {
    return;
  } // If Page is refreshed, then this should not happen. Which is the scenario if
  // DAVE_SETTINGS.setCookie("system_response", null);


  DAVE_SETTINGS.create_session({}, function (data) {
    DAVE_SETTINGS.setCookie("dave_last_login", data.updated);
    DAVE_SETTINGS.execute_custom_callback('on_initialize_session');
  });

  if (DAVE_SETTINGS.AVATAR_ID && DAVE_SCENE.loadPreScene) {
    DAVE_SETTINGS.ajaxRequest("/object/" + DAVE_SETTINGS.AVATAR_MODEL + "/" + DAVE_SETTINGS.AVATAR_ID, "GET", function (data) {
      if (data[DAVE_SETTINGS.GLB_ATTR || 'glb_url']) {
        DAVE_SCENE.loadPreScene("dave-canvas", data[DAVE_SETTINGS.GLB_ATTR || 'glb_url'], function () {
          DAVE_SCENE.loaded = true;
          DAVE_SETTINGS.execute_custom_callback('onload', [], DAVE_SCENE);
        });
        DAVE_SETTINGS.LANGUAGE = data.language;
        DAVE_SETTINGS.VOICE_ID = data.voice_id;
      }
    });
  }
};

DAVE_SETTINGS.get_url_params = function (qd) {
  qd = qd || {};
  if (location.search) location.search.substr(1).split("&").forEach(function (item) {
    var s = item.split("="),
        k = s[0],
        v = s[1] && decodeURIComponent(s[1]); //  null-coalescing / short-circuit
    //(k in qd) ? qd[k].push(v) : qd[k] = [v]

    (qd[k] = qd[k] || []).push(v); // null-coalescing / short-circuit
  });
  return qd;
};

DAVE_SETTINGS.pretty_print = function (data, field_name_map, field_type_map, skip_fields, empty, separator) {
  separator = separator || '<br>';
  skip_fields = skip_fields || [];
  empty = empty || "";
  field_name_map = field_name_map || {};
  field_type_map = field_type_map || {};

  if (data == null || data == undefined) {
    return empty;
  }

  if (typeof data == "string") {
    try {
      data = JSON.parse(data);
    } catch (err) {
      return data;
    }
  }

  if (typeof data == "number") {
    return data;
  }

  if (typeof field_name_map == "string") {
    field_name_map = data[field_name_map] || {};
  }

  if (typeof field_type_map == "string") {
    field_type_map = field_type_map || data[field_type_map] || {};
  }

  var output_string = [];

  if (Array.isArray(data)) {
    for (var k in data) {
      var d = data[k];
      d = DAVE_SETTINGS.pretty_print(d, field_name_map, field_type_map, skip_fields, empty, separator);

      if (d) {
        output_string.push(d);
      }
    }
  } else if (djQ.type(data) == "object") {
    if (data._name_map && !djQ.isEmptyObject(data._name_map)) {
      field_name_map = data._name_map;
    }

    if (data._type_map && !djQ.isEmptyObject(data._type_map)) {
      field_type_map = data._type_map;
    }

    for (var _k4 in data) {
      if (skip_fields.indexOf(_k4) >= 0 || _k4.startsWith('_')) {
        continue;
      }

      var v = data[_k4];

      if (Array.isArray(v)) {
        v = DAVE_SETTINGS.pretty_print(v, ", ", field_name_map, field_type_map, skip_fields, empty);
      }

      if (djQ.type(v) == "object") {
        if (!djQ.isEmptyObject(v)) {
          v = '{ ' + DAVE_SETTINGS.pretty_print(v, ", ", field_name_map, field_type_map, skip_fields, empty) + " }";
        } else {
          v = empty;
        }
      }

      if (field_type_map[_k4]) {
        if (field_type_map[_k4] == 'datetime' || field_type_map[_k4] == 'datetime-local') {
          v = DAVE_SETTINGS.print_timestamp(v, v, true);
        } else if (field_type_map[_k4] == "file") {
          v = '\<Uploaded File of type' + v.split('.').pop() + '\>';
        } // Put here other field type cases

      } else {
        var v1 = new Date(v);

        if (v1.toLocaleString() != 'Invalid Date') {
          v = DAVE_SETTINGS.print_timestamp(v1, v1.toLocaleString(), true);
        }
      }

      if (v) {
        output_string.push('<b>' + (field_name_map[_k4] || DAVE_SETTINGS.toTitleCase(_k4)) + "</b> : " + v);
      }
    }

    return output_string.join(separator);
  } else {
    output_string.push(data);
  }

  if (output_string.length == 0) {
    if (empty) {
      output_string.push(empty);
    } else {
      return empty;
    }
  }

  return output_string.join(separator);
};

DAVE_SETTINGS.unauthorized = function (f, args) {
  return function () {
    DAVE_SETTINGS.clearCookie('dave_user_id');
    DAVE_SETTINGS.clearCookie('dave_authentication');

    var fu = function fu() {
      console.error("Could not signup do DaveAI chatbot! Please contact customer support!");

      if (typeof DAVE_SETTINGS.signup_failed == 'function') {
        DAVE_SETTINGS.signup_failed();
      }
    };

    args.push(fu);
    DAVE_SETTINGS.signup({}, function () {
      f.apply({}, args);
    }, fu, true);
  };
};

DAVE_SETTINGS.upload_file = function upload_file(blob_data, file_name, callbackFunc, errorFunc, unauthorized) {
  var formData = new FormData();
  formData.append('file', blob_data, file_name);
  var headers = DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication");

  if (!unauthorized) {
    unauthorized = DAVE_SETTINGS.unauthorized(DAVE_SETTINGS.upload_file, [blob_data, file_name, callbackFunc, errorFunc]);
  }

  djQ.ajax({
    url: DAVE_SETTINGS.BASE_URL + "/upload_file?large_file=true",
    type: "POST",
    dataType: "json",
    contentType: false,
    processData: false,
    headers: {
      "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
      "X-I2CE-USER-ID": headers["X-I2CE-USER-ID"],
      "X-I2CE-API-KEY": headers["X-I2CE-API-KEY"]
    },
    data: formData,
    statusCode: {
      401: unauthorized
    }
  }).done(function (data) {
    if (callbackFunc) {
      callbackFunc(data);
    }
  }).fail(function (err) {
    if (errorFunc) {
      errorFunc(err);
    }
  });
};

DAVE_SETTINGS.pad_string = function pad_string(n, width, z) {
  width = width || 2;
  z = z || '0';
  return (String(z).repeat(width) + String(n)).slice(String(n).length);
};

DAVE_SETTINGS.print_timestamp = function (timestamp, invalid_date, full_form) {
  invalid_date = invalid_date || 'N/A';
  td = new Date();
  timestamp = timestamp || td;
  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  if (typeof timestamp == "string") {
    if (timestamp.indexOf("/") >= 0) {
      // This is India date format
      var _timestamp$split = timestamp.split(/\D+/),
          _timestamp$split2 = _slicedToArray(_timestamp$split, 6),
          d = _timestamp$split2[0],
          m = _timestamp$split2[1],
          y = _timestamp$split2[2],
          h = _timestamp$split2[3],
          M = _timestamp$split2[4],
          s = _timestamp$split2[5];

      if (!s) {
        s = '00';
      }

      if (!M) {
        M = td.getMinutes();
      }

      if (!h) {
        h = td.getHours();
      }

      timestamp = new Date(y, m - 1, d, h, M, s);
    }
  }

  if (djQ.type(timestamp) != 'date') {
    timestamp = new Date(timestamp);
  }

  if (timestamp.toString() == 'Invalid Date') {
    return invalid_date;
  }

  if (full_form || timestamp.getYear() != td.getYear()) {
    return months[timestamp.getMonth()] + " " + DAVE_SETTINGS.pad_string(timestamp.getDate()) + ', ' + timestamp.getFullYear() + ', ' + DAVE_SETTINGS.pad_string(timestamp.getHours()) + ':' + DAVE_SETTINGS.pad_string(timestamp.getMinutes());
  }

  if (timestamp.getMonth() != td.getMonth() || timestamp.getDate() != td.getDate()) {
    return months[timestamp.getMonth()] + " " + DAVE_SETTINGS.pad_string(timestamp.getDate()) + ', ' + DAVE_SETTINGS.pad_string(timestamp.getHours()) + ':' + DAVE_SETTINGS.pad_string(timestamp.getMinutes());
  }

  return DAVE_SETTINGS.pad_string(timestamp.getHours()) + ':' + DAVE_SETTINGS.pad_string(timestamp.getMinutes());
};

DAVE_SETTINGS.signup_guard = function (func, args, cookie_name) {
  cookie_name = cookie_name || "dave_authentication";

  if (!(DAVE_SETTINGS.getCookie(cookie_name) || DAVE_SETTINGS.getCookie('authentication'))) {
    console.warn(cookie_name + " headers are not set, but trying to run function " + func.name);
    var repeats = args.slice(-1);
    var args1 = args;

    if (repeats < 30) {
      if ((repeats == 10 || repeats == 20) && (DAVE_SETTINGS.getCookie('dave_started_signup') || DAVE_SETTINGS.getCookie('started_signup'))) {
        DAVE_SETTINGS.clearCookie('dave_started_signup');
        DAVE_SETTINGS.signup();
      }

      setTimeout(function () {
        args1.push(args1.pop() + 1);
        func.apply({}, args1);
      }, 1000 * repeats);
    } else {
      console.error("Stopping trying to run function" + func.name + " after " + repeats + " attempts");

      if (typeof DAVE_SETTINGS.signup_failed == 'function') {
        DAVE_SETTINGS.signup_failed();
      }
    }

    return false;
  }

  return true;
};

DAVE_SETTINGS.load_settings = function load_settings(identifier, namespace) {
  if (djQ(identifier).length) {
    var ds = djQ(identifier);

    for (var s in namespace) {
      if (s.toUpperCase() != s) {
        continue;
      }

      var v = ds.attr("data-" + s.toLowerCase().replace(/_/g, '-')) || ds.attr("data-" + s.toLowerCase()) || ds.attr("data-" + s);

      if (v) {
        console.debug("Entered the condition " + s);
        var g = DAVE_SETTINGS.get_value(v);

        if (_typeof(g === 'object') && g !== null && _typeof(namespace[s]) === 'object' && namespace[s] !== null) {
          for (var k in g) {
            namespace[s][k] = g[k];
          }
        } else {
          namespace[s] = g;
        }
      } else {
        console.debug("Didn't get condition " + s);
      }
    }
  }
};

DAVE_SETTINGS.load_dave = function load_dave() {
  DAVE_SETTINGS.EVENT_TIMER = Math.floor(new Date().getTime() / 1000);
  DAVE_SETTINGS.load_settings("#dave-settings", DAVE_SETTINGS);

  if (DAVE_SETTINGS.ENTERPRISE_ID && DAVE_SETTINGS.CONVERSATION_ID && DAVE_SETTINGS.SIGNUP_API_KEY) {
    console.debug("Got Enterprise ID, Conversation ID, SignUp API KEY");

    if (!(DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication")) || (DAVE_SETTINGS.getCookie("dave_conversation_id") || DAVE_SETTINGS.getCookie("conversation_id")) != DAVE_SETTINGS.CONVERSATION_ID) {
      DAVE_SETTINGS.clearCookies();
      DAVE_SETTINGS.signup();
    } else if ((DAVE_SETTINGS.getCookie("dave_conversation_id") || DAVE_SETTINGS.getCookie("conversation_id")) != DAVE_SETTINGS.CONVERSATION_ID) {
      DAVE_SETTINGS.clearCookies();
      DAVE_SETTINGS.signup();
    } else if ((DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication"))['X-I2CE-ENTERPRISE-ID'] != DAVE_SETTINGS.ENTERPRISE_ID) {
      DAVE_SETTINGS.clearCookies();
      DAVE_SETTINGS.signup();
    } else {
      DAVE_SETTINGS.initialize_session();
    }
  } else {
    console.warn("Did not get one of Enterprise ID, Conversation ID or SignUp API KEY");
  }

  djQ("#dave-settings").one("click", function () {
    DAVE_SCENE.audio_click();
  });

  if (DAVE_SETTINGS.SPEECH_SERVER) {
    var now_params = null;
    DAVE_SETTINGS.StreamingSpeech = {};
    DAVE_SETTINGS.streamer = new Streamer({
      wakeup_server: null,
      asr_server: DAVE_SETTINGS.SPEECH_SERVER,
      recognizer: DAVE_SETTINGS.SPEECH_RECOGNIZER,
      conversation_id: DAVE_SETTINGS.CONVERSATION_ID,
      enterprise_id: DAVE_SETTINGS.ENTERPRISE_ID,
      base_url: DAVE_SETTINGS.BASE_URL,
      auto_asr_detect_from_wakeup: false,
      vad: true,
      asr_only: true,
      audio_auto_stop: DAVE_SETTINGS.MAX_SPEECH_DURATION / 1000.0,
      asr_type: "chunks",
      //full-'flacking'||"chunks"-'asteam'||"direct"-'sending-flag',
      wakeup_type: "chunks",
      //chunks-'asteam'||"direct"-'sending-flag',
      video_stream_type: "chunks",
      //chunks-'asteam'||"direct"-'sending-flag'
      asr_additional_info: {},
      model_name: DAVE_SETTINGS.SPEECH_MODEL_NAME,
      event_callback: function event_callback(event, data) {
        if (event == 'wsconnect') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onSocketConnect == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onSocketConnect();
          }
        } else if (event == 'auto_stopped_recording') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording();
          }
        } else if (event == 'asr_intermediate_results') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable(data);
          }
        } else if (event == 'asr_results') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable(data);
          }
        } else if (event == 'conversation_response') {
          DAVE_SETTINGS.execute_custom_callback('on_response', [data.name || "received chat response", data.customer_state, data, now_params]);

          if (typeof DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable(data);
          }
        } else if (event == 'wsdisconnect') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect(data);
          }
        } else if (event == 'asr_error') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onError == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onError(data);
          }
        } else if (event == 'conv_error') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onError == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onError(data);
          }
        }
      }
    });

    DAVE_SETTINGS.StreamingSpeech.onStartVoiceRecording = function (params) {
      params = params || {};
      params.engagement_id = DAVE_SETTINGS.getCookie("dave_engagement_id");
      params.customer_id = DAVE_SETTINGS.getCookie("dave_user_id");
      params.api_key = DAVE_SETTINGS.getCookie("dave_authentication")['X-I2CE-API-KEY'];
      params.system_response = DAVE_SETTINGS.getCookie("dave_system_response");
      DAVE_SETTINGS.execute_custom_callback("on_start_recording", [params]);
      DAVE_SETTINGS.execute_custom_callback("on_chat_start", [DAVE_SETTINGS.streamer.asr_additional_info]);
      DAVE_SETTINGS.streamer.startVoiceRecording(params);
      DAVE_SETTINGS.execute_custom_callback("after_start_recording", [params]);
      now_params = DAVE_SETTINGS.streamer.asr_additional_info;
    };

    DAVE_SETTINGS.StreamingSpeech.onStopVoiceRecording = function () {
      DAVE_SETTINGS.execute_custom_callback("on_stop_recording");
      DAVE_SETTINGS.streamer.stopVoiceRecording();
      DAVE_SETTINGS.execute_custom_callback("after_stop_recording");
    };
  }
};

DAVE_SCENE.audio_click = function () {
  var aud = new Audio(DAVE_SETTINGS.asset_path + "assets/audio/empty.wav");
  aud.volume = 0.0;
  aud.play();
  DAVE_SCENE.playable = true;
}; // Pause temporaririly


DAVE_SCENE.pause = function () {
  if (DAVE_SCENE.snd) {
    DAVE_SCENE.snd.pause();
    DAVE_SCENE.stopMorphs();
    DAVE_SCENE.snd = null;
  }
}; // Pause permanently


DAVE_SCENE.stop = function () {
  if (DAVE_SCENE.snd) {
    DAVE_SCENE.snd.pause();
    DAVE_SCENE.pauseAnimsandMorphs();
    DAVE_SCENE.snd = null;
    DAVE_SCENE.queue = [];
  }
};

DAVE_SCENE.mute = function () {
  if (DAVE_SCENE.snd) {
    DAVE_SCENE.snd.volume = 0.0;
  }

  DAVE_SCENE.volume = 0.0;
};

DAVE_SCENE.unmute = function () {
  if (DAVE_SCENE.snd) {
    DAVE_SCENE.snd.volume = 1.0;
  }

  DAVE_SCENE.volume = 1.0;
};

djQ(document).one("click", function () {
  DAVE_SCENE.audio_click();
});
djQ(window).blur(function (e) {
  if (DAVE_SCENE.opened && DAVE_SETTINGS.opened_time instanceof Date) {
    DAVE_SETTINGS.iupdate_session({
      'session_duration': (new Date().getTime() - DAVE_SETTINGS.opened_time.getTime()) / 1000
    }, function () {
      DAVE_SETTINGS.opened_time = null;
    });
  }
});
djQ(window).focus(function (e) {
  if (DAVE_SCENE.opened && !DAVE_SETTINGS.opened_time) {
    DAVE_SETTINGS.opened_time = new Date();
  }
});
djQ(window).on("beforeunload", function () {
  if (DAVE_SCENE.opened && DAVE_SETTINGS.opened_time instanceof Date) {
    DAVE_SETTINGS.iupdate_session({
      'session_duration': (new Date().getTime() - DAVE_SETTINGS.opened_time.getTime()) / 1000
    });
  }

  alert("Checking when closing window");
});
djQ(function () {
  DAVE_SETTINGS.load_dave();
});
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var DAVE_ASSET_PATH = "https://chatbot-plugin.iamdave.ai/";

if (document.currentScript && isFinite(document.currentScript.src.split('/').slice(-2, -1)[0])) {
  DAVE_ASSET_PATH = document.currentScript && document.currentScript.src.split('/').slice(0, -4).join('/') + '/';
} else {
  DAVE_ASSET_PATH = document.currentScript && document.currentScript.src.split('/').slice(0, -3).join('/') + '/' || "https://chatbot-plugin.iamdave.ai/";
}

function gm_authFailure() {
  document.getElementById("map").style.display = "none";
  console.log("MAP API KEY IS NOT WORKING ");
  handleMapError();
}

;

djQ.fn.ForcePhoneNumbersOnly = function () {
  return this.each(function () {
    djQ(this).keydown(function (e) {
      var key = e.charCode || e.keyCode || 0; // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
      // home, end, period, and numpad decimal

      return key == 8 || key == 9 || key == 13 || key == 46 || key == 110 || key == 190 || key >= 35 && key <= 40 || key >= 48 && key <= 57 || key >= 96 && key <= 105 || key == 61;
    });
  });
};

window.dave_load_chatbot = function () {
  DAVE_SETTINGS.execute_custom_callback("before_load_chatbot"); //All Dynamic Settings to change chatbot lookandfeel

  var dave_chatTitle = djQ('#dave-settings').attr('data-cbTitle') || djQ('#dave-settings').attr('data-dave-cbTitle') || "DAVE VIRTUAL ASSISTANT";
  var dave_bot_bubbleTitle = djQ('#dave-settings').attr('data-bot-bubbleTitle') || djQ('#dave-settings').attr('data-cbdTitle') || djQ('#dave-settings').attr('data-dave-cbTitle') || "Dave Virtual Assistant";
  var dave_user_bubbleTitle = djQ('#dave-settings').attr('data-user-bubbleTitle') || "You";
  var dave_optionTitle = djQ('#dave-settings').attr('data-optionTitle') || '';
  var data_feedback_redo_time = djQ("#dave-settings").attr("data-feedback-redo-time") || 3600000;
  var feedback_successMSG = djQ("#dave-settings").attr("data-feedback-successMsg") || "Thank you for your valuable feedback";
  var maxLockHeight = djQ("#dave-settings").attr("data-cb-minLockHeight") || 150;
  var dave_desktop_size = {
    dave_cb_height: djQ('#dave-settings').attr('data-cb-height-desktop') || '520px'
  };
  var dave_mobile_size = {
    dave_cb_height: djQ('#dave-settings').attr('data-cb-height-mobile') || '500px'
  }; //ALL IMAGE DATA-SETTINGS 

  var dave_bot_icon = djQ('#dave-settings').attr('data-botIcon') || djQ('#dave-settings').attr('data-dave-botIcon') || DAVE_ASSET_PATH + 'assets/img/dave-icon.png';
  var dave_user_icon = djQ('#dave-settings').attr('data-userIcon') || djQ('#dave-settings').attr('data-dave-userIcon') || DAVE_ASSET_PATH + 'assets/img/userchat-icon.png';
  var dave_chatbot_icon = djQ('#dave-settings').attr('data-cbIcon') || djQ('#dave-settings').attr('data-dave-cbIcon') || DAVE_ASSET_PATH + 'assets/img/dave-icon.png';
  var dave_title_icon = djQ('#dave-settings').attr('data-title-icon') || djQ('#dave-settings').attr('data-dave-title-icon') || dave_chatbot_icon;
  var cross_img = djQ("#dave-settings").attr("data-cross-img") || djQ("#dave-settings").attr("data-dave-cross-img") || DAVE_ASSET_PATH + 'assets/img/cross.png';
  var min_img = djQ("#dave-settings").attr("data-min-img") || djQ("#dave-settings").attr("data-dave-min-img") || DAVE_ASSET_PATH + 'assets/img/min.svg';
  var max_img = djQ("#dave-settings").attr("data-max-img") || djQ("#dave-settings").attr("data-dave-max-img") || DAVE_ASSET_PATH + 'assets/img/max.svg';
  var like_img = djQ("#dave-settings").attr("data-like-img") || DAVE_ASSET_PATH + 'assets/img/up.svg';
  var liked_img = djQ("#dave-settings").attr("data-liked-img") || DAVE_ASSET_PATH + 'assets/img/up-hover.svg';
  var dislike_img = djQ("#dave-settings").attr("data-dislike-img") || DAVE_ASSET_PATH + 'assets/img/down.svg';
  var disliked_img = djQ("#dave-settings").attr("data-disliked-img") || DAVE_ASSET_PATH + 'assets/img/down-hover.svg';
  var cb_sugg_next = djQ("#dave-settings").attr("data-cbSuggNext-img") || DAVE_ASSET_PATH + 'assets/img/next.png';
  var cb_sugg_prev = djQ("#dave-settings").attr("data-cbSuggPrev-img") || DAVE_ASSET_PATH + 'assets/img/pre.png';
  var typing_gif = djQ("#dave-settings").attr("data-typing-img") || DAVE_ASSET_PATH + 'assets/img/typing.gif';
  var mic_img = djQ("#dave-settings").attr("data-mic-img") || DAVE_ASSET_PATH + 'assets/img/mic.svg';
  var mic_hover_img = djQ("#dave-settings").attr("data-micHover-img") || DAVE_ASSET_PATH + 'assets/img/michover.svg';
  var send_img = djQ("#dave-settings").attr("data-send-img") || DAVE_ASSET_PATH + 'assets/img/send.png';
  var homeBtnImg = djQ("#dave-settings").attr("data-homeBtn-img");
  var WIA_RatingTitle = djQ("#dave-settings").attr("data-feedback-accurate-title") || djQ("#dave-settings").attr("data-dave-feedback-accurate-title") || "Was I accurate?";
  var WIH_RatingTitle = djQ("#dave-settings").attr("data-feedback-useful-title") || djQ("#dave-settings").attr("data-dave-feedback-useful-title") || "Was I helpful?";
  var writeFeedback = !(djQ("#dave-settings").attr("data-hide-feedback-text") === "true"); //All Dynamic Settings ENDS
  //global letiable declaration as flag and window width

  var originalScroll = djQ('body').css('overflow');
  var dave_window_width = djQ(window).width();
  var dave_window_height = djQ(window).height();
  console.log("Window height and width when page loads => " + dave_window_width + " " + dave_window_height);
  var dave_chat_heightchange = false;
  var pred_attach = false;
  var text_cleared = true;
  var chat_historyActivated = false;
  var map_enabled = undefined;
  var userPositon = {
    "lat": undefined,
    "lng": undefined
  };
  var dave_userchatfunc_exe = undefined;
  var dave_predication_send = undefined;
  var dave_eng_id_status = undefined;
  var chat_loading = undefined;
  var dave_activityTimer = undefined;
  var nudge_activityInterval = undefined;
  var followupIndex = undefined;
  var followupList_array = new Array();
  var timerFlag = true;
  var close_count = 0;
  var chatbox_width = djQ(".dave-chatbox-cont").width() || "360px"; //GLOBAL VARIABLE VALUES SET AS PER SCREEN SIZING

  if (dave_window_width > 450) {
    //DESKTOP
    djQ(".dave-chatbox").css({
      "height": dave_desktop_size.dave_cb_height
    }); //chatbox_width = djQ('#dave-settings').attr('data-cb-width-desktop') || chatbox_width;
  } else {
    //MOBILE
    djQ(".dave-chatbox").css({
      "height": dave_mobile_size.dave_cb_height
    });
    DAVE_SCENE.is_mobile = true;
  } //AVATAR CODE BLOCK STARTS


  var data_avatar_initChk = djQ("#dave-settings").attr("data-avatar-id") || false;
  var dave_avatar_loadStatus = false;
  var chatboxOpenStatus = 'close';
  var nowRecording = false;

  function showAvatar(status) {
    if (status) {
      djQ(".dave-cb-avatar-contt").css("display", "block");
    } else {
      djQ(".dave-cb-avatar-contt").css("display", "none");
    }
  }

  DAVE_SCENE.set_avatar_position = function (x, y, h) {
    x = x || 0;
    y = y || DAVE_SCENE.is_mobile ? 0 : '100vh';
    h = h || 200;
    djQ(".dave-cb-avatar-contt").css({
      "left": x - parseFloat(h) / 2,
      "top": y
    });
    djQ("canvas#dave-canvas").height(h);
  };

  if (data_avatar_initChk) {
    DAVE_SETTINGS.register_custom_callback('onload', function () {
      dave_avatar_loadStatus = true;
      var avatar_xCord = djQ("#dave-settings").attr("data-avatar-position-x") || null;
      var avatar_yCord = djQ("#dave-settings").attr("data-avatar-position-y") || null;
      var avatarHeight = djQ("#dave-settings").attr("data-avatar-position-h") || null;
      /*let avatarWidth =  djQ("#dave-settings").attr("data-avatar-position-w") || null;*/

      DAVE_SCENE.set_avatar_position(avatar_xCord, avatar_yCord, avatarHeight);
      djQ(".dave-cb-tt").css("margin-left", "");

      if (dave_window_width > 450 && chatboxOpenStatus == "open") {
        var h = djQ(".dave-chatbox").height() + 50;
        dave_desktop_size.dave_cb_height = h;
      }

      if (chatboxOpenStatus == "open") {
        showAvatar(true);
      }

      set_maximized_height();
    }, DAVE_SCENE);
  } else {
    showAvatar(false);
  } //AVATAR CODE BLOCKS ENDS


  function set_inner_height() {
    if (dave_optionTitle) {
      djQ(".dave-cb-chatarea").height(-djQ(".dave-cb-tt-sec").first().offset().top - djQ(".dave-cb-tt-sec").first().height() + djQ(".dave-cb-stickBottom").first().offset().top - 27);
    } else {
      djQ(".dave-cb-chatarea").height(-djQ(".dave-cb-tt-sec").first().offset().top - djQ(".dave-cb-tt-sec").first().height() + djQ(".dave-cb-stickBottom").first().offset().top - 22);
    }
  }

  function set_maximized_height() {
    if (dave_window_width > 450) {
      djQ(".dave-chatbox").css({
        "height": dave_desktop_size.dave_cb_height
      });
    } else {
      djQ(".dave-chatbox").css({
        "height": dave_mobile_size.dave_cb_height
      });
    } //Just check if avatar is initialized then> is it mobile view or desk then> have a size of avtar detect that size and add to variable h


    var h = djQ(".dave-chatbox").height();
    var w = window.innerHeight;

    if (h > w) {
      //Just check if avatar is initialized then> is it mobile view or desk then> have a size of avtar detect that size and substract to variable w in height parameter passed
      djQ(".dave-chatbox").height(w);
    }

    set_inner_height();
  } // FUNCTION FOR PADDING LEADING ZEROS


  function pad(n, width, z) {
    width = width || 2;
    z = z || '0';
    return (String(z).repeat(width) + String(n)).slice(String(n).length);
  } //NUDGES CODE BLOCKS STARTS


  function dave_nudgeTrigger(index) {
    clearTimeout(dave_activityTimer);
    followupIndex++;

    if (followupIndex <= followupList_array.length) {
      if (DAVE_SCENE.opened == true) {
        n_openState = 'max';
      } else {
        n_openState = followupList_array[index][2];
      }

      if (DAVE_SCENE.opened) {
        botchat_data({
          'customer_state': followupList_array[index][0],
          "query_type": "auto"
        }, '', '', '', function (data) {
          dave_timerFunc();
        });
      } else {
        if (followupList_array[index][1]) {
          if (chatboxOpenStatus == 'close') {
            z = 'open';
          } else {
            z = '';
          }

          if (n_openState == 'max') {
            botchat_data({
              'customer_state': followupList_array[index][0],
              "query_type": "auto"
            }, '', z, 'max', function (data) {
              dave_timerFunc();
            });
          } else {
            botchat_data({
              'customer_state': followupList_array[index][0],
              "query_type": "auto"
            }, '', z, 'min', function (data) {
              dave_timerFunc();
            });
          }
        }
      }
    }
  }

  function dave_nudge(dave_activityInterval, followupList, forceStatus, openState) {
    openState = openState || false;
    var i = 0;
    followupIndex = 0;
    nudge_activityInterval = dave_activityInterval;

    if (dave_activityInterval && followupList) {
      followupList_array.length = 0;
      djQ.each(followupList, function (key, value) {
        followupList_array.push([value, forceStatus, openState]);
        i++;
      });

      if (followupList_array.length > 0) {
        timerFlag = true;
        dave_timerFunc();
        djQ("body").off("keydown click touchstart dblclick resize", window.dave_nudge_clear_func);
        djQ("body").on("keydown click touchstart dblclick resize", window.dave_nudge_clear_func);
      }
    }
  }

  function dave_timerFunc() {
    if (followupIndex <= followupList_array.length && timerFlag) {
      clearTimeout(dave_activityTimer);
      dave_activityTimer = setTimeout(function () {
        dave_nudgeTrigger(followupIndex);
      }, nudge_activityInterval);
    } else {
      djQ("body").off("keydown click touchstart dblclick resize", window.dave_nudge_clear_func);
    }
  } // THIS clears an nudges if they exist when you do any activities on the page


  window.dave_nudge_clear_func = function (e) {
    if (e.type == 'keydown' && (e.which < 30 || e.which > 90 && e.which < 96 || e.which > 105)) {
      return true;
    }

    console.debug("Reset nudge", e, e.which);
    clearTimeout(dave_activityTimer);
    dave_timerFunc();
  };

  window.stopTimmer = function () {
    timerFlag = false;
    clearTimeout(dave_activityTimer);
  };

  window.startTimmer = function () {
    timerFlag = true;

    if (typeof dave_timerFunc === 'function') {
      dave_timerFunc();
    }
  }; //NUDGES CODE BLOCKS END HERE
  //Like Dislike Function Block


  function like_dislike(currClickedElem, feedback_reaction, feedback_type) {
    if (feedback_reaction == 'like') {
      DAVE_SETTINGS.execute_custom_callback("on_click_like", [currClickedElem, feedback_type]);
      djQ(currClickedElem).html('');
      djQ(currClickedElem).html("<img src='".concat(liked_img, "'>"));
      djQ(currClickedElem).next().html('');
      djQ(currClickedElem).next().html("<img src='".concat(dislike_img, "'>"));
      l_d_react = true;
    } else {
      DAVE_SETTINGS.execute_custom_callback("on_click_dislike", [currClickedElem, feedback_type]);
      djQ(currClickedElem).html('');
      djQ(currClickedElem).html("<img src='".concat(disliked_img, "'>"));
      djQ(currClickedElem).prev().html();
      djQ(currClickedElem).prev().html("<img src='".concat(like_img, "'>"));

      if (feedback_type == "speech") {
        var disliked_voice_text = djQ(currClickedElem).closest('.dave-voiceBubble-feedback').prev('p').text();
        djQ("#dave-cb-textinput").val(disliked_voice_text);
      }

      l_d_react = false;
    }

    return l_d_react;
  } //printing Bubble Timing


  function dateTiming(part) {
    var dave_date = new Date();

    if (part) {
      return dave_date;
    } else {
      var printHrsMin = pad(dave_date.getHours()) + ":" + pad(dave_date.getMinutes());
      return printHrsMin;
    }
  } //auto scroll on every msg pop-in


  function scrollChatarea(speed) {
    speed = speed || 400;
    djQ(".dave-cb-chatarea").animate({
      scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight")
    }, speed);
  }

  DAVE_SETTINGS.scrollChatarea = scrollChatarea; //loader icon display function block

  function dave_chatRes_loader(loaderType) {
    if (loaderType == 'bot') {
      if (DAVE_SETTINGS.execute_custom_callback('before_bot_loader') === false) {
        return djQ(".dave-cb-chatarea");
      }

      return djQ(".dave-cb-chatarea").append("<div class='dave-botchat dave-bottyping-loader'><div class='dave-bc-avatar'><img src='".concat(dave_bot_icon, "'></div><p class='dave-chattext dave-botchat-typing-p'><img class='dave-botchat-typing-img' src='").concat(typing_gif, "'></p></div>"));
    } else if (loaderType == 'user') {
      if (DAVE_SETTINGS.execute_custom_callback('before_user_loader') === false) {
        return djQ(".dave-cb-chatarea");
      }

      return djQ(".dave-cb-chatarea").append("<div class='dave-userchat dave-usertyping-loader'><div class='dave-uc-avatar'><img src='".concat(dave_user_icon, "'></div><p class='dave-userchat-p dave-user-typing-p'><img class='dave-user-typing-img' src='").concat(typing_gif, "'></p></div>"));
    }
  } //remove loading gif 


  function dave_typingLoaderRemove(loaderType) {
    if (loaderType == 'user') {
      djQ('.dave-usertyping-loader').remove();
    } else if (loaderType == 'bot') {
      djQ('.dave-bottyping-loader').remove();
    }
  } //maximize chatbot


  function maximize_chatbox(minCustomSize) {
    originalScroll = djQ('body').css('overflow');

    if (DAVE_SETTINGS.on_maximize() === false) {
      return;
    }

    chatboxOpenStatus = 'max';

    if (DAVE_SCENE.is_mobile) {
      djQ('.dave-cb-chatarea').css('display', 'block');
      djQ('.dave-cb-stickBottom').css('display', 'block');
    }

    djQ(".dave-cb-tt-minmax img").remove();
    djQ(".dave-cb-tt-minmax").append("<img src='".concat(min_img, "'>"));
    set_maximized_height();

    if (!minCustomSize) {
      djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
        minimize_chatbox();
      });
    } else {
      djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
        minimize_chatbox(minCustomSize);
      });
      djQ("textarea#dave-feedback-write").css("height", "");
      djQ(".rating-star-contt").css("bottom", "");
      djQ("#dave-userfull-rate, #dave-accuracy-rate").css('height', '');
    }

    scrollChatarea(500);
    DAVE_SETTINGS.execute_custom_callback("after_maximize_custom");
  } //minimize chatbot function


  function minimize_chatbox(customSize, chatType) {
    if (DAVE_SETTINGS.on_minimize() === false) {
      return;
    }

    chatboxOpenStatus = 'min';

    if (!DAVE_SCENE.is_mobile) {
      if (!customSize) {
        var type_sec_height = djQ(".dave-cb-type-sec").height();
        var chatsugg_height = djQ(".dave-cb-chatsugg").height();
        var tt_height = djQ(".dave-cb-tt-sec").height();
        var last_message_height = djQ(".dave-botchat").last().height();
        var ext_spc = 50;

        if (maxLockHeight) {
          maxLockHeight = parseInt(maxLockHeight);

          if (last_message_height > maxLockHeight) {
            djQ(".dave-chatbox").height(type_sec_height + chatsugg_height + tt_height + maxLockHeight + ext_spc);
          } else {
            djQ(".dave-chatbox").height(type_sec_height + chatsugg_height + tt_height + last_message_height + ext_spc);
          }
        } else {
          djQ(".dave-chatbox").height(type_sec_height + chatsugg_height + tt_height + last_message_height + ext_spc);
        }
      } else {
        if (!chatType) {
          djQ(".dave-chatbox").height(customSize);
        } else {
          djQ(".dave-chatbox").height(customSize); // if (chatType == 'feedback') {
          //     var writeFeedbackBox = (djQ("textarea#dave-feedback-write").height()) / 2;
          //     djQ("textarea#dave-feedback-write").css("height", writeFeedbackBox);
          //     djQ(".rating-star-contt").css("bottom", "-30%");
          //     djQ("#dave-userfull-rate, #dave-accuracy-rate").height('10%');
          // }
        }
      }

      set_inner_height();
      scrollChatarea(500);
    } else {
      var headerSecHeight = Math.max(djQ('.dave-cb-tt-sec').height(), '200');
      djQ('.dave-chatbox-open').height(headerSecHeight);
    }

    djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
      maximize_chatbox();
    });
    djQ('body').css("overflow", originalScroll);
    djQ(".dave-cb-tt-minmax img").remove();
    djQ(".dave-cb-tt-minmax").append("<img src='".concat(max_img, "'>"));
    DAVE_SETTINGS.execute_custom_callback("after_minimize_custom");
  }

  DAVE_SETTINGS.maximize = maximize_chatbox;
  DAVE_SETTINGS.minimize = minimize_chatbox; //PREDICTION FOR USER TYPEING MESSAGE CODE BLOCK

  function dave_prediction(selectedPrediction) {
    if (selectedPrediction.value.length > 2) {
      DAVE_SETTINGS.predict(selectedPrediction.value, function (response) {
        DAVE_SETTINGS.execute_custom_callback("before_predicted_text", [selectedPrediction.value, response]);

        if (text_cleared) {
          console.warn("text was sent before we got prediction, so doing nothing!");
          return;
        }

        if (response.length > 0) {
          console.debug("We are getting response for prediction");

          if (!pred_attach) {
            djQ('.dave-pred-contt').append("<ol class='dave-pred-box'></ol>");

            if (response.length < 5) {
              djQ(".dave-pred-contt").css({
                "bottom": djQ(".dave-cb-type-sec").height() + 10 + "px",
                "height": "auto",
                "overflow": "auto",
                "max-height": "110px",
                "padding-bottom": "10px"
              });
            } else if (response.length >= 5) {
              djQ(".dave-pred-contt").css({
                "bottom": djQ(".dave-cb-type-sec").height() + 10 + "px",
                "height": "110px",
                "overflow-y": "scroll"
              });
            }

            pred_attach = true;
          }
        } else {
          console.warn("we are not getting any response for prediction");
          djQ(".dave-pred-contt").html('');
          djQ(".dave-pred-contt").css({
            "height": "0",
            "overflow": "hidden",
            "padding": "0"
          });
          pred_attach = false;
        }

        djQ('.dave-pred-box').html('');
        djQ.each(response, function (keys, values) {
          pred_custState = Object.keys(values);
          pred_title = Object.values(values);
          DAVE_SETTINGS.execute_custom_callback("before_add_predicted_text", [pred_title, pred_custState]);
          var prd = "\n                        <li class='dave-pred-txt' data-pred-custState='".concat(pred_custState, "'>").concat(pred_title, "</li>\n                    ");
          DAVE_SETTINGS.execute_custom_callback("on_add_predicted_text", [prd]);
          djQ(".dave-pred-box").append(prd);
        });

        if (pred_attach) {
          djQ(".dave-pred-txt").one("click", function () {
            var clicked_pred = djQ(this).text();
            clicked_pred_custState = djQ(this).attr('data-pred-custState');
            DAVE_SETTINGS.execute_custom_callback("before_click_predicted_option", [clicked_pred, clicked_pred_custState]);

            if (DAVE_SETTINGS.execute_custom_callback("before_click_predicted_option", [clicked_pred, clicked_pred_custState]) === false) {
              return;
            }

            djQ("#dave-cb-textinput").val(clicked_pred);
            djQ("#dave-cb-textinput").focus();
            djQ(".dave-pred-contt").html('');
            djQ(".dave-pred-contt").css({
              "height": "0",
              "overflow": "auto",
              "padding": "0"
            });
            pred_attach = false;
            dave_predication_send = true;
            set_inner_height();
            DAVE_SETTINGS.execute_custom_callback("after_click_predicted_option", [clicked_pred, clicked_pred_custState]);
          });
        }
      });
    } else {
      djQ(".dave-pred-contt").html('');
      djQ(".dave-pred-contt").css({
        "height": "0",
        "overflow": "hidden",
        "padding": "0"
      });
      pred_attach = false;
    }
  }

  djQ(document).on("click", ".dave-cb-chatarea, .dave-cb-chatsugg", function () {
    djQ(".dave-pred-contt").html('');
    djQ(".dave-pred-contt").css({
      "height": "0",
      "overflow": "auto",
      "padding": "0"
    });
    pred_attach = false;
  });

  function replyBubbleUserMsg(replyId) {
    if (typeof replyId == 'undefined') {
      return false;
    }

    ;
    var msg = djQ('.dave-cb-chatarea').find("#".concat(replyId)).find('.dave-userchat-p').text();
    djQ('.dave-cb-chatarea').find("#".concat(replyId)).nextAll().find("[data-scrollmsgid = '".concat(replyId, "']")).find('.replyBubbleMsg').html(msg); // /.find('.replyBubbleMsg').html(msg)
  } //ATTACH ONLY TEXT MESSAGE BUBBLE USER END


  function userTextMsgBubble(text, chatType, dateTime, bubbleId, msgId) {
    var utmb_date = dateTime || dateTiming();
    dave_typingLoaderRemove('user');
    djQ(".dave-cb-chatarea").append("\n                <div class='dave-userchat' id='".concat(bubbleId, "'>\n                    <div class='dave-uc-avatar'>\n                        <img src='").concat(dave_user_icon, "'>\n                    </div>\n                    <p class='dave-userBubble-header'>\n                        <span class='dave-userBubble-Title'> ").concat(dave_user_bubbleTitle, " &nbsp;&nbsp;</span>\n                        <span class='dave-userBubble-timestamp'>").concat(utmb_date, "</span>\n                    </p>\n                    <p class='dave-userchat-p'>").concat(text, "</p>\n                </div>\n            ")).ready(function () {
      if (chatType == "speech") {
        djQ('.dave-userchat').last().attr('data-voiceMsgId', msgId);
        dave_chatRes_loader('bot');
      }
    });
  } //Message Bubble Print Function Block


  function OCD_msgBubbles(userIcon, name, timestamp, bubbleId, responseTxt, reply_to) {
    djQ(".dave-cb-chatarea").append("\n                <div class='dave-botchat'>\n                    <div class='dave-bc-avatar'>\n                        <img src='".concat(userIcon, "'>\n                    </div>\n                    <p class='dave-botBubble-header'>\n                        <span class='dave-botBubble-Title'>").concat(name, "&nbsp;&nbsp;</span>\n                        <span class='dave-botBubble-timestamp'>").concat(timestamp, "</span>\n                    </p>\n                    ").concat(bubbleId && !reply_to ? "<div style='display: none;' class='scrollToMsg' data-scrollMsgId='".concat(bubbleId, "'></div>") : "".concat(bubbleId && reply_to ? "<div class='scrollToMsg' data-scrollMsgId='".concat(reply_to, "'><p class='replyBubbleTitle'>Query</p><p class='replyBubbleMsg'></p></div>") : ''), "\n                    <p class='dave-chattext'>").concat(responseTxt, "</p>\n                </div>\n            ")).ready(function () {
      djQ(".dave-cb-chatarea").animate({
        scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight")
      }, 500);

      if (reply_to) {
        replyBubbleUserMsg(reply_to);
      }
    });
  }

  function updateBubbleId(bubbleId) {
    var x = temp_bubbleId.pop();
    var z = perm_bubbleId.pop();
    djQ('.dave-cb-chatarea').find('#' + x).attr('id', z);
  }

  function msgBubbles(msgBy, chatType, dateTime, userMsg, bubbleId, replyPrintState, voiceMsgId, custState, callback) {
    DAVE_SETTINGS.execute_custom_callback('before_message_bubble', [msgBy, chatType, dateTime, userMsg, bubbleId, replyPrintState, voiceMsgId, custState]);
    userMsg = userMsg || undefined;
    voiceMsgId = voiceMsgId || undefined;
    custState = custState || undefined;

    if (msgBy == 'bot') {
      if (chatType == 'normal' || chatType == 'history' || chatType == 'speech' || chatType == 'error') {
        dave_typingLoaderRemove('bot');
        djQ(".dave-cb-chatarea").append("\n                <div class='dave-botchat'>\n                    <div class='dave-bc-avatar'>\n                        <img src='".concat(dave_bot_icon, "'>\n                    </div>\n                    <p class='dave-botBubble-header'>\n                        <span class='dave-botBubble-Title'>").concat(dave_bot_bubbleTitle, "&nbsp;&nbsp;</span>\n                        <span class='dave-botBubble-timestamp'>").concat(dateTime, "</span>\n                    </p>\n                    ").concat(bubbleId && !replyPrintState ? "<div style='display: none;' class='scrollToMsg' data-scrollMsgId='".concat(bubbleId, "'></div>") : "".concat(bubbleId && replyPrintState ? "<div class='scrollToMsg' data-scrollMsgId='".concat(bubbleId, "'><p class='replyBubbleTitle'>Query</p><p class='replyBubbleMsg'></p></div>") : ''), "\n\n                    <p class='dave-chattext'>").concat(userMsg, "</p>\n                </div>\n            ")).ready(function () {
          if (chatType == 'speech') {
            djQ('button#start-recording').prop('disabled', false);
            djQ(".dave-userchat[data-voiceMsgId='".concat(voiceMsgId, "']")).next('.dave-botchat').attr("data-voiceMsgId", voiceMsgId);
          }

          if (replyPrintState) {
            replyBubbleUserMsg(replyPrintState);
          }

          if (chatType == "normal") {
            updateBubbleId(bubbleId);
          }
        });
      } else {
        djQ(".dave-cb-chatarea").append("\n                <div class='dave-botchat'>\n                    <div class='dave-bc-avatar'>\n                        <img src='".concat(dave_bot_icon, "'>\n                    </div>\n                    <p class='dave-chattext'>We don't have a response to your request. Did you really say something ?</p>\n                </div>\n            "));
      }
    } else {
      if (chatType == 'normal' || chatType == 'history' || chatType == 'error' || chatType == 'speech') {
        if (!voiceMsgId) {
          userTextMsgBubble(userMsg, chatType, dateTime, bubbleId);
        } else {
          userTextMsgBubble(userMsg, chatType, dateTime, bubbleId, voiceMsgId);
        }

        if (chatType == 'history') {
          djQ('.dave-userchat').append("<p class='hidden' id='customer_response_history'>".concat(custState, "</p>"));
        } else if (chatType == 'speech') {
          djQ('button#start-recording').prop('disabled', true);
          djQ('.dave-userchat').append("\n                    <div class='dave-voiceBubble-feedback'>\n                        <input type='hidden' value='".concat(voiceMsgId, "' name='voice-responseStoredId'>\n                        <button class='voice-thumbsup' data-thumbtype='like'>\n                            <img src='").concat(like_img, "'>\n                        </button>\n                        <button class='voice-thumbsdown' data-thumbtype='dislike'>\n                            <img src='").concat(dislike_img, "'>\n                        </button>\n                    </div>\n                "));
        }
      }
    }

    DAVE_SETTINGS.execute_custom_callback('after_message_bubble', [msgBy, chatType, dateTime, userMsg, bubbleId, replyPrintState, voiceMsgId, custState]);

    if (callback) {
      callback();
    }
  }

  djQ(document).on('click', '.scrollToMsg', function () {
    var msgId = djQ(this).attr('data-scrollMsgId');
    var cbHeight = djQ('.dave-cb-chatarea').height();
    var msgBubblePos = djQ('#' + msgId).position().top;
    var chatScrollHeight = djQ('.dave-cb-chatarea').scrollTop();

    if (msgBubblePos < 0) {
      djQ('.dave-cb-chatarea').animate({
        scrollTop: chatScrollHeight - Math.round(cbHeight / 2.5)
      }, 200);
      djQ('#' + msgId).css("box-shadow", "-10px 0px 5px 5px #ecebeb");
      setTimeout(function () {
        djQ('#' + msgId).css("box-shadow", "");
      }, 1000);
    } else if (msgBubblePos > cbHeight) {
      djQ('.dave-cb-chatarea').animate({
        scrollTop: chatScrollHeight + msgBubblePos - Math.round(cbHeight / 2.5)
      }, 200);
      djQ('#' + msgId).css("box-shadow", "-10px 0px 5px 5px #ecebeb");
      setTimeout(function () {
        djQ('#' + msgId).css("box-shadow", "");
      }, 1000);
    } else {
      djQ('#' + msgId).css("box-shadow", "-10px 0px 5px 5px #ecebeb");
      setTimeout(function () {
        djQ('#' + msgId).css("box-shadow", "");
      }, 1000);
    }

    console.debug("== " + msgId + " == " + chatScrollHeight + " == " + cbHeight + " == " + Math.round(cbHeight / 2.5) + " == " + msgBubblePos);
  });

  window.other_chat_data = function (status, userIcon, name, timestamp, responseTxt, reply_to, data, bubbleId) {
    timestamp = DAVE_SETTINGS.print_timestamp(timestamp);

    if (status == 'typing') {
      djQ(".dave-cb-chatarea").append("<div class='dave-botchat .dave-otherpersonTyping-loader'><div class='dave-bc-avatar'><img src='".concat(userIcon, "'></div><p class='dave-chattext dave-botchat-typing-p'><img class='dave-botchat-typing-img' src='").concat(typing_gif, "'></p></div>"));
    } else {
      djQ(".dave-otherpersonTyping-loader").remove(); //window.response_print = function (data, bubbleId, data_length, new_date, chatType, msgId) {

      OCD_msgBubbles(userIcon, name, timestamp, bubbleId, responseTxt, reply_to);
      response_print(data, 1, timestamp, 'other_chat_data');
    }
  }; //On Error Display Msg Function Block


  function chatResponseError(chatType, eCode, params, bubbleId, behaviour, openState, lockHeight, prevTimeout) {
    prevTimeout = prevTimeout || 500;
    error_code = eCode['status'];
    dave_typingLoaderRemove('bot');
    msgBubbles('bot', 'error', dateTiming());

    if (error_code >= 400 & error_code < 500) {
      djQ(".dave-chattext").last().html("There seems to be some technical issue in the system. Please try a different query.");
      scrollChatarea();
    } else if (error_code >= 500) {
      djQ(".dave-chattext").last().html("There seems to be a delay in getting your response, hold on!");
      scrollChatarea();
      setTimeout(function () {
        if (chatType == "normal") {
          botchat_data(params, bubbleId, behaviour, lockHeight, openState);
        } else if (chatType == "history") {
          botchat_history();
        }
      }, prevTimeout * 2);
    } else {
      djQ(".dave-chattext").last().html("There seems to be problem in getting your response.  Can you please try again in some time?");
      scrollChatarea();
    }
  }

  window.handleMapError = function () {
    djQ(".dave-cb-form form").last().prev().remove();
    djQ(".dave-cb-form form").last().before("\n            <button class=\"getCurLoc\">Get My Location</button>\n        ");
  };

  djQ(document).on("click", ".getCurLoc", function () {
    getUserCurrentLocation(true);
    console.debug("TOOG");
  });

  function getUserCurrentLocation(directAccess) {
    if (directAccess) {
      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function (position) {
          userPositon['lat'] = position.coords.latitude;
          userPositon['lng'] = position.coords.longitude;
          console.debug(userPositon);
        }, function (error) {
          switch (error.code) {
            case error.PERMISSION_DENIED:
              console.warn("Permission denied by user.");
              break;

            case error.POSITION_UNAVAILABLE:
              console.warn("Location position unavailable.");
              break;

            case error.TIMEOUT:
              console.warn("Request timeout.");
              break;

            case error.UNKNOWN_ERROR:
              console.warn("Unknown error.");
              break;
          }
        }, {
          timeout: 1000,
          enableHighAccuracy: true
        });
      } else {
        console.log("Browser doesn't support geolocation!");
      }
    }
  }

  function loadDaveMap() {
    var googleMapKey = djQ(document).find("#dave-settings").attr("data-google-map-key");

    if (googleMapKey) {
      djQ.ajax({
        url: "https://maps.googleapis.com/maps/api/js?key=".concat(googleMapKey, "&libraries=geometry,places"),
        type: "GET",
        dataType: "script",
        cache: true,
        success: function success(data) {
          var map;
          var marker;
          var predefinedLocation = [{
            location: {
              "lat": 19.236370361630183,
              "lng": 73.13019900709297
            },
            title: "Title",
            address: "Adress"
          }, {
            location: {
              "lat": 19.193919679842637,
              "lng": 72.97556454923306
            },
            title: "Title",
            address: "Adress"
          }, {
            location: {
              "lat": 18.95095435428654,
              "lng": 72.83516893108772
            },
            title: "Title",
            address: "Adress"
          }]; // INITIALIZING THE GOOGLE MAP

          function initMap() {
            // SETTING UP THE MAP VARIABLE
            map = new google.maps.Map(djQ("#map")[0], {
              center: {
                lat: 20.5937,
                lng: 78.9629
              },
              zoom: 5
            }); // InfoWindow, MARKER, GEOCODER USED TO GENERATE PIN, AND LOCATION FULL DETAILS

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById("infowindow-content");
            var geocoder = new google.maps.Geocoder();
            var marker = new google.maps.Marker({
              map: map
            });
            infowindow.setContent(infowindowContent); // PREDEFINED LOCATION USED TO POINT SEVERAL LOCATION

            function pdl_InfoWindow(marker, map, title, address, url) {
              google.maps.event.addListener(marker, 'click', function () {
                var html = "\n                            <div id=\"infowindow-content\">\n                                <span id=\"place-name\" class=\"title\">".concat(title, "</span><br />\n                                <span id=\"place-address\">").concat(address, "</span>\n                            </div>\n                            ");
                pdl_infoWin = new google.maps.InfoWindow({
                  content: html,
                  maxWidth: 350
                });
                pdl_infoWin.open(map, marker);
              });
            } // LOOPING THROUGH PREDEFINED LOCATION DATA TO SET ALL LOCATIONS WITH MARKER ON MAP


            djQ.each(predefinedLocation, function (i, val) {
              geocoder.geocode({
                location: val['location']
              }).then(function (response, status) {
                if (response.results[0]) {
                  var predefinedMarker = new google.maps.Marker({
                    map: map,
                    icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                    position: val['location'],
                    animation: google.maps.Animation.DROP,
                    address: val['Address'] || response.results[0].formatted_address,
                    title: val['title'] || "Location" // url: url

                  });
                  pdl_InfoWindow(predefinedMarker, map, val['title'], response.results[0].formatted_address);
                } else {
                  window.alert("No results found");
                }
              }).catch(function (e) {
                return window.alert("Geocoder failed due to: " + e);
              });
            }); //INPUT BOX FOR USERS TO SEARCH PARTICULAR LOCATION

            djQ(document).on("focus", "#locationInput", function () {
              var input = document.getElementById('locationInput');
              var options = {
                fields: ["formatted_address", "geometry", "name"],
                strictBounds: false,
                types: ["establishment"]
              }; // SETTING UP THE Autocomplete VARIABLE TO TRIGGER AND GET AUTOSUGGESTIONS OF LOCATION

              var autocomplete = new google.maps.places.Autocomplete(input, options); //POINT NEW LOCATION USER SELECTED USING INPUT BOX

              autocomplete.addListener("place_changed", function () {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();

                if (!place.geometry || !place.geometry.location) {
                  window.alert("No details available for input: '" + place.name + "'");
                  return;
                }

                if (place.geometry.viewport) {
                  map.fitBounds(place.geometry.viewport);
                } else {
                  map.setCenter(place.geometry.location);
                  map.setZoom(17);
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                userPositon['lat'] = place.geometry.location.lat();
                userPositon['lng'] = place.geometry.location.lng();
                console.debug(userPositon);
                infowindowContent.children["place-name"].textContent = place.name;
                infowindowContent.children["place-address"].textContent = place.formatted_address;
                infowindow.open(map, marker);
              });
            }); //GET USERS CURRENT LOCATION

            if ("geolocation" in navigator) {
              navigator.geolocation.getCurrentPosition(function (position) {
                userPositon['lat'] = position.coords.latitude;
                userPositon['lng'] = position.coords.longitude;
                marker.setPosition(userPositon);
                marker.setVisible(true);
                geocoder.geocode({
                  location: userPositon
                }).then(function (response, status) {
                  if (response.results[0]) {
                    infowindowContent.children["place-name"].textContent = response.results[0].name;
                    infowindowContent.children["place-address"].textContent = response.results[0].formatted_address;
                    infowindow.open(map, marker);
                  } else {
                    window.alert("No results found");
                  }
                }).catch(function (e) {
                  return window.alert("Geocoder failed due to: " + e);
                });
                marker.setDraggable(true);
                map.setCenter(userPositon);
                map.setZoom(10);
                console.debug(userPositon);
                google.maps.event.addListener(marker, 'drag', function () {
                  userPositon['lat'] = marker.position.lat();
                  userPositon['lng'] = marker.position.lng();
                  console.debug(userPositon);
                });
              }, function (error) {
                switch (error.code) {
                  case error.PERMISSION_DENIED:
                    console.warn("Permission denied by user.");
                    break;

                  case error.POSITION_UNAVAILABLE:
                    console.warn("Location position unavailable.");
                    break;

                  case error.TIMEOUT:
                    console.warn("Request timeout.");
                    break;

                  case error.UNKNOWN_ERROR:
                    console.warn("Unknown error.");
                    break;
                }
              }, {
                timeout: 1000,
                enableHighAccuracy: true
              });
            } else {
              console.log("Browser doesn't support geolocation!");
            }
          }

          window.initializeMap = function () {
            initMap();
          };
        },
        error: function error(e) {
          console.error(e);
          console.warn("Something Went Wrong While Loading Google Map API");
          getUserCurrentLocation(true);
        }
      });
    } else {
      console.warn("NO KEY FOUND SO NO MAP LOAD");
    }
  }

  if (djQ(document).find("#dave-settings").attr("data-google-map-key")) {
    loadDaveMap();
  } //SERVER RESPONSE PRINTING FUNCTION BLOCK


  window.response_print = function (data, data_length, new_date, chatType, msgId) {
    if (close_count > 0) {
      return;
    }

    var permbubId = "RI" + data['response_id'];
    map_enabled = false;
    perm_bubbleId.push(permbubId);
    voice_MsgId = msgId || undefined;

    if (chatType == "speech") {
      dataLength_thresold = 1;
    } else {
      dataLength_thresold = 0;
      voice_MsgId = null;
    }

    if (data_length > dataLength_thresold) {
      dave_response_dataType = data['data']['response_type'];
      var rp_replyId = data['data']['reply_to'] || false;

      if (chatType == "normal") {
        dave_typingLoaderRemove('bot'); //print bot reply as message
      }

      if (chatType == "normal" || chatType == "speech") {
        msgBubbles('bot', chatType, dateTiming(), '', permbubId, rp_replyId, voice_MsgId, '', function () {
          if (chatType == 'speech') {
            djQ(".dave-userchat[data-voiceMsgId='".concat(voice_MsgId, "']")).find("input[name='voice-responseStoredId']").val(data['response_id']);
          }
        });
      }

      scrollChatarea(500);

      if (chatType == "normal" || chatType == "speech") {
        djQ(".dave-chattext").last().html(data["placeholder"] || ""); // if no state options are provided, then previous options are kept

        if (data['state_options'] && !djQ.isEmptyObject(data['state_options'])) {
          djQ('.dave-cb-chatsugg').html('');
          djQ(".dave-cb-chatsugg").css("overflow-x", "auto");
        }

        djQ(".dave-cb-chatsugg").append("<span class='chatsugg-next'><img src='".concat(cb_sugg_next, "'></span><span class='chatsugg-prev'><img src='").concat(cb_sugg_prev, "'></span>"));
      }

      response_elementPrint(dave_response_dataType, data, chatType);

      if (data['state_options'] && !djQ.isEmptyObject(data['state_options'])) {
        //print bot suggestion
        for (var _i = 0, _Object$entries = Object.entries(data['state_options']); _i < _Object$entries.length; _i++) {
          var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
              key = _Object$entries$_i[0],
              value = _Object$entries$_i[1];

          djQ('.dave-cb-chatsugg').append("<div class='dave-cb-chatsugg-list'>".concat(value, "<input type='hidden' value='").concat(key, "'></div>"));
        }
      } //Bot Reply Feedback Thumbsup and ThumbsDown


      if (chatType == "normal" || chatType == "speech") {
        if (data['show_feedback']) {
          djQ('p.dave-chattext').last().append("\n                    <div class='dave-botBubble-feedback'>\n                        <span class='hidden'>".concat(data['response_id'], "</span>\n                        <button class='dave-thumbsup' data-thumbType='like'><img src='").concat(like_img, "'></button>\n                        <button class='dave-thumbsdown' data-thumbType='dislike'><img src='").concat(dislike_img, "'></button>\n                    </div>\n                "));
        } //Nudges Starting


        if (typeof data['data']['_follow_ups'] !== 'undefined') {
          var forceOpen = data['data']['_force_open'] || null;
          var openState = data['data']['_open_state'] || 'min';
          dave_nudge(data['wait'], data['data']['_follow_ups'], forceOpen, openState);
        } //QUICKACCESS BUTTON NEXT & PREV ONLY VISIBLE ON SCROLL IS AVAILABLE


        var chatsugg_scrollWidth = djQ('.dave-cb-chatsugg')[0].scrollWidth;

        if (chatsugg_scrollWidth > chatbox_width - 20) {
          djQ('span.chatsugg-next').css('display', 'block');

          if (dave_optionTitle) {
            djQ('span.chatsugg-next').css('top', '12px');
            djQ('span.chatsugg-prev').css('top', '12px');
          } else {
            djQ('.dave-chatsuggTitle').hide();
          }
        }
      } //Chat box height Adjustment according to device


      set_inner_height();
      dave_userchatfunc_exe = false;
      chat_loading = true;
      userchat_data();
      djQ('button#start-recording').prop('disabled', false);
    } else {
      //if no data or reply recieved from bot
      dave_typingLoaderRemove('bot');
      msgBubbles('bot', 'error', dateTiming(), 'We don\'t have a response to your request. Did you really say something ?');
      scrollChatarea(500);
    }
  }; //Response Element Print


  function response_elementPrint(dave_response_dataType, data, chatType, count) {
    if (dave_response_dataType == 'thumbnails') {
      DAVE_SETTINGS.execute_custom_callback("before_thumbnails_response", [data['data'][dave_response_dataType], data['data'], data]);
      djQ('p.dave-chattext').last().append("\n                <div class=\"dave-chat-items-contt\">\n                    <ul class=\"dave-chat-items\"></ul>\n                </div>\n            ");
      djQ.each(data['data'][dave_response_dataType], function (key, value) {
        var s = djQ("\n                    <a href=\"".concat(encodeURI(value['url']) || '#', "\" data-customer-state='").concat(value['customer_state'], "' data-customer-response='").concat(value['customer_response'] || value['title'] || DAVE_SETTINGS.toTitleCase(value['customer_state']), "'>\n                        <li>\n                            <div class='dave-chat-items-imgcontt'><img src='").concat(value['image'], "'></div>\n                            <p class='dave-chat-items-bottom-p'>").concat(value['title'], "</p>\n                        </li>\n                    </a>\n                "));
        djQ('.dave-chat-items').last().append(s);

        if (value['customer_state'] || value['customer_response']) {
          djQ(s).on('click', function () {
            userTextMsgBubble(djQ(this).attr('data-customer-response'), 'normal', dateTiming(), djQ.now());
            botchat_data({
              'query_type': 'click',
              'customer_state': djQ(this).attr('data-customer-state'),
              'customer_response': djQ(this).attr('data-customer-response')
            });
            return false;
          });
        }
      });
      DAVE_SETTINGS.execute_custom_callback("after_thumbnails_response", [data['data'][dave_response_dataType], data['data'], data]);
    } else if (dave_response_dataType == 'image') {
      DAVE_SETTINGS.execute_custom_callback("before_image_response", [data['data'][dave_response_dataType], data['data'], data]);
      var s = djQ("\n                <div class='dave-chat-items-contt' data-url='".concat(encodeURI(data.data.url), "' data-customer-state='").concat(data.data.customer_state, "' data-customer-response='").concat(data.data.customer_response || data.data.title || DAVE_SETTINGS.toTitleCase(data.data.customer_state), "'>\n                    <div class='dave-chat-imageResponse'><img class='dave-imgResponse-image' src='").concat(data['data']['image'], "'></div>\n                </div>\n            "));
      djQ('p.dave-chattext').last().append(s);

      if (data.data.customer_state || data.data.customer_response) {
        djQ(s).on('click', function () {
          userTextMsgBubble(djQ(this).attr('data-customer-response'), 'normal', dateTiming(), djQ.now());
          botchat_data({
            'query_type': 'click',
            'customer_state': djQ(this).attr('data-customer-state'),
            'customer_response': djQ(this).attr('data-customer-response')
          });
          return false;
        });
      }

      DAVE_SETTINGS.execute_custom_callback("after_image_response", [data['data'][dave_response_dataType], data['data'], data]);
    } else if (dave_response_dataType == "form") {
      DAVE_SETTINGS.execute_custom_callback("before_form_response", [data['data'][dave_response_dataType], data['data'], data]);
      form_error = '';
      var dbld = chatType == 'history' && count < dave_return_size ? 'disabled' : '';
      djQ('p.dave-chattext').last().append("\n                <div class=\"dave-cb-form\">\n                    <form class='dave-form' type='POST'></form>\n                </div>\n            ");
      djQ.each(data['data'][dave_response_dataType], function (key, value) {
        var datetime_converter = {
          'datetime': 'text',
          'date': 'text',
          'time': 'text'
        };
        datetime_converter['minDT'] = value.min_date || value.min_time ? (value.min_date || '1900-01-01') + "T" + (value.min_time || '00:00') : "";
        datetime_converter['maxDT'] = value.max_date || value.max_time ? (value.max_date || '2050-12-31') + "T" + (value.max_time || '23:59') : "";
        datetime_converter['step'] = value['step'] || '';
        datetime_converter['default_date'] = new Date(value['default_date']);
        var formats = {
          'datetime': 'd/m/Y H:i',
          'date': 'd/m/Y',
          'time': 'H:i'
        };

        if (value['format']) {
          for (var k in value['formats']) {
            formats[k] = value['formats'][k];
          }
        }

        value['title'] = value['title'] || DAVE_SETTINGS.toTitleCase(value['name']);

        if (value['ui_element'] == "textarea") {
          djQ('.dave-cb-form form').last().append("<textarea name='".concat(value['name'], "' placeholder='").concat(value['placeholder']).concat(value['required'] ? '*' : '', "' value='").concat(value['value'] || '', "' title='").concat(value['title'], "' ").concat(value['required'] ? 'required' : '', " ui_element='").concat(value['ui_element'], "' ").concat(dbld, "></textarea>"));
        } else if (value['ui_element'] == "select") {
          var _s2 = "<option value=\"\">".concat(value['placeholder']).concat(value['required'] ? '*' : '', "</option>");

          djQ.each(value['options'] || [], function (k, v) {
            _s2 += "<option value=".concat(typeof k == 'string' ? k : typeof v != 'string' ? v[0] : v, ">").concat(typeof v != 'string' ? v[1] : v, "</option>");
          });
          djQ('.dave-cb-form form').last().append("<select name='".concat(value['name'], "' title='").concat(value['title'], "' ").concat(value['required'] ? 'required' : '', " ui_element='").concat(value['ui_element'], "' ").concat(dbld, ">").concat(_s2, "</select>"));
        } else if (value['ui_element'] == "file") {
          var file_typeAllowed = value['file_type'];
          var max_uploadSize = value['max_file_size'];
          djQ('.dave-cb-form form').last().append("\n                        <label class='file_upload_label'>".concat(value['title'], "</label>\n                        <input type='file' name='").concat(value['name'], "' class='file_upload' accept=\"").concat(file_typeAllowed, "\" data-max='").concat(max_uploadSize, "' ").concat(value['required'] ? 'required' : '', " ui_element='").concat(value['ui_element'], "' title='").concat(value['title'], "' ").concat(dbld, ">\n                    "));
        } else if (value['ui_element'] == "geolocation") {
          map_enabled = true; // *********************************

          if (chatType !== "history") {
            djQ(".dave-cb-form form").last().before("\n                        <input type=\"text\" name=\"".concat(value["name"], "\" id=\"locationInput\" class=\"mapLocationInput\" placeholder=\"").concat(value['placeholder'], "\"").concat(value['required'] ? '*' : '', " title=\"").concat(value['title'], "\" error='").concat(value['error'] || '', "' ui_element='").concat(value['ui_element'], "'} >\n                        "));
            djQ('.dave-cb-form form').last().append("\n                            <div class=\"mapBox\">\n                                <div id=\"map\"></div>\n                                <div id=\"infowindow-content\">\n                                    <span id=\"place-name\" class=\"title\"></span><br />\n                                    <span id=\"place-address\"></span>\n                                </div>\n                            </div>\n                        ");
            initializeMap();
          } else if (chatType == 'history' && count >= dave_return_size) {
            djQ('.dave-cb-form form').last().append("\n                            <input type=\"text\" name=\"".concat(value["name"], "\" placeholder=\"").concat(value['placeholder']).concat(value['required'] ? '*' : '', "\" title=\"").concat(value['title'], "\" required=\"value\" error='").concat(value['error'] || '', "' ui_element='").concat(value['ui_element'], "' ").concat(value['required'] ? 'required' : '', " disabled>\n                        "));
          } // *********************************

        } else if (value['ui_element'] == "tel") {
          var _s3 = djQ("\n                            <input type='text' class='tel' name='".concat(value['name'], "' placeholder='").concat(value['placeholder']).concat(value['required'] ? '*' : '', "' value='").concat(value['value'] || '', "' title='").concat(value['title'], "' ").concat(value['required'] ? 'required' : '', " autocomplete=\"off\" error='").concat(value['error'] || 'Please enter a valid mobile number', "' ui_element='").concat(value['ui_element'], "' validate=\"[+]{0,1}[0]{0,2}[1-9][0-9]{9}\" ").concat(dbld, ">\n                    "));

          djQ('.dave-cb-form form').last().append(_s3);
          djQ(_s3).ForcePhoneNumbersOnly();
        } else if (value['ui_element'] == "email") {
          var _s4 = djQ("\n                            <input type='text' class='tel' name='".concat(value['name'], "' placeholder='").concat(value['placeholder']).concat(value['required'] ? '*' : '', "' value='").concat(value['value'] || '', "' title='").concat(value['title'], "' ").concat(value['required'] ? 'required' : '', " autocomplete=\"off\" error='").concat(value['error'] || 'Please enter a valid email', "' ui_element='").concat(value['ui_element'], "' validate=\"\\w+([\\.-_]?\\w+)*@\\w+([\\.-_]?\\w+)*(\\.\\w{2,5})+\" ").concat(dbld, ">\n                    "));

          djQ('.dave-cb-form form').last().append(_s4);
        } else {
          djQ('.dave-cb-form form').last().append("<input type='".concat(datetime_converter[value['ui_element'] || 'text'] || value['ui_element'] || 'text', "' class='").concat(value['ui_element'], "'name='").concat(value['name'], "' placeholder='").concat(value['placeholder']).concat(value['required'] ? '*' : '', "' value='").concat(value['value'] || '', "' title='").concat(value['title'], "' ").concat(value['required'] ? 'required' : '', " min='").concat(datetime_converter['minDT'], "' max='").concat(datetime_converter['maxDT'], "' step='").concat(datetime_converter['step'], "' autocomplete=\"off\" error='").concat(value['error'] || '', "' ui_element='").concat(value['ui_element'], "' ").concat(dbld, ">"));
        }

        if (['datetime', 'date', 'time'].indexOf(value['ui_element']) >= 0) {
          var t = {
            minDate: value.ui_element != 'time' ? value['min_date'] : false,
            maxDate: value.ui_element != 'time' ? value['max_date'] : false,
            minTime: value.ui_element != 'date' ? value['min_time'] : false,
            maxTime: value.ui_element != 'date' ? value['max_time'] : false,
            defaultTime: value['default_time'],
            defaultDate: datetime_converter.default_date,
            step: value['step'] || parseInt(value['time_resolution'] || "0") * 60 || 3600,
            mask: true,
            value: value.value ? new Date(value['value']) : '',
            format: formats[value.ui_element],
            timepicker: value['ui_element'] == 'date' ? false : true,
            datepicker: value['ui_element'] == 'time' ? false : true,
            allowBlank: !value['required'],
            lazyInit: value.value ? false : true,
            initTime: true
          };
          djQ('.dave-form input[name="' + value['name'] + '"]').datetimepicker(t);
        }

        if (chatType == "normal") {
          form_error += value['error'] + "<br /><br />";
        }
      });

      if (chatType == "history") {
        /////////you have to make a count variable here
        if (count == dave_return_size) {
          djQ('.dave-cb-form form').last().append("<input type='submit' value='".concat(data.data.submit_value || "submit", "'>"));
        } else {
          djQ('.dave-cb-form form').last().append("<input type='submit' value='".concat(data.data.submit_value || "submit", "' disabled>"));
        }
      } else if (chatType == "normal") {
        var _s5 = djQ("<input type='submit' value='".concat(data.data.submit_value || "submit", "'>"));

        djQ('.dave-cb-form form').last().append(_s5);
      }

      djQ('.dave-cb-form').append("<p class='hidden'>".concat(data['data']['customer_state'], "</p>"));
      DAVE_SETTINGS.execute_custom_callback("after_form_response", [data['data'][dave_response_dataType], data['data'], data]);
    } else if (dave_response_dataType == "url") {
      DAVE_SETTINGS.execute_custom_callback("before_url_response", [data['data'][dave_response_dataType], data['data'], data]);

      var _s6 = djQ("<a href=\"".concat(encodeURI(data['data']['url']), "\" target='").concat(data['data']['target'] || '_blank', "'>").concat(data['data']['title'] || data['data']['url'], "</a>"));

      djQ('p.dave-chattext').last().append(_s6);

      _s6.attr('href', encodeURI(data['data']['url']));

      djQ(_s6).on('click', function () {
        DAVE_SETTINGS.execute_custom_callback('on_url_click');
      });

      if (data.data['sub_title']) {
        djQ('p.dave-chattext').last().append("".concat(data.data['sub_title']));
      }

      DAVE_SETTINGS.execute_custom_callback("after_url_response", [data['data'][dave_response_dataType], data['data'], data]);
    } else if (dave_response_dataType == "video") {
      DAVE_SETTINGS.execute_custom_callback("before_video_response", [data['data'][dave_response_dataType], data['data'], data]);
      djQ('p.dave-chattext').last().append("\n            <a href='".concat(data['data']['video'], "' target='").concat(data['data']['target'], "'>\n                <div class='dave-videoplayer'>\n                    <video controls width='100%' height='200px' autoplay muted><source src='").concat(data['data']['video'], "'>Your Browser Does Not Support Video Player Try With Chrome</video>\n                </div>\n            </a>\n            "));
      DAVE_SETTINGS.execute_custom_callback("after_video_response", [data['data'][dave_response_dataType], data['data'], data]);
    } else if (dave_response_dataType == "custom") {
      var data_func_name = data['data']['custom_function'];
      DAVE_SETTINGS.execute_custom_callback("before_custom_response", [data['data'][data_func_name], data['data'], data]);
      var data_func = window[data_func_name];

      if (data_func && typeof data_func == "function") {
        data_func(djQ('p.dave-chattext').last(), data["data"][data_func_name], chatType);
      }

      DAVE_SETTINGS.execute_custom_callback("after_custom_response", [data['data']['option'], data['data'], data]);
    }

    if (dave_response_dataType == "options" || data['data']['options']) {
      DAVE_SETTINGS.execute_custom_callback("before_options_response", [data['data']['option'], data['data'], data]);
      djQ('p.dave-chattext').last().append("\n                </br><ul class='dave-option-list'></ul>\n            ");
      console.debug(data['data']['options']);
      djQ.each(data['data']['options'], function (key, value) {
        if (typeof key != "string") {
          if (typeof value != "string") {
            key = value[0];
            value = value[1];
          } else {
            key = data["customer_state"] || null;
          }
        }

        djQ('.dave-option-list').last().append("<li><input type='hidden' value='".concat(key, "'><button>").concat(value, "</button></li>"));
      });
      DAVE_SETTINGS.execute_custom_callback("after_options_response", [data['data']['option'], data['data'], data]);
    }
  } //FUNCTION TO TAKE USER MESSAGE AND PRINT IN CHATBOT AND PASS AS MESSAGE TO SYSTEM


  function userchat_data() {
    if ((DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response')) && !chat_historyActivated) {
      userInputBubble('normal');
    }
    /*IF HISTORY IS LOADED THEN THIS BLOCKS GET EXECUTED FOR ONCE*/
    else if ((DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id')) && chat_historyActivated) {
        userInputBubble('history');
      }
  } //history - CHECKING AND PRINTING USER CHAT HISTORY


  function on_load_history(data) {
    DAVE_SETTINGS.execute_custom_callback("before_load_history", [data]);
    var count = 0;
    chat_loading = true;
    djQ.each(data['history'], function (his_key, his_value) {
      count++;
      djQ('.dave-cb-chatsugg').html('');

      if (his_value['direction'] == 'system') {
        dave_response_dataType = his_value['data']['response_type'];
        his_value['timestamp'] = DAVE_SETTINGS.print_timestamp(his_value['timestamp']); //print bot reply as message

        c_bubbleId = his_value['response_id'];
        c_bubbleId = "RI" + c_bubbleId;

        if (count > 1) {
          if (typeof his_value['reply_to'] == "undefined") {
            msgBubbles('bot', 'history', his_value['timestamp'], '', c_bubbleId);
          } else {
            msgBubbles('bot', 'history', his_value['timestamp'], '', c_bubbleId, his_value['response_id']);
          }
        } else {
          msgBubbles('bot', 'history', his_value['timestamp'], '');
        }

        djQ(".dave-chattext").last().html(his_value["placeholder"]);

        if (his_value['state_options'] && !djQ.isEmptyObject(his_value['state_options'])) {
          djQ(".dave-cb-chatsugg").append("<span class='chatsugg-next'><img src='".concat(cb_sugg_next, "'></span><span class='chatsugg-prev'><img src='").concat(cb_sugg_prev, "'></span>"));
        }

        response_elementPrint(dave_response_dataType, his_value, 'history', count);

        if (his_value['state_options'] && !djQ.isEmptyObject(his_value['state_options'])) {
          //print bot suggestion
          for (var _i2 = 0, _Object$entries2 = Object.entries(his_value['state_options']); _i2 < _Object$entries2.length; _i2++) {
            var _Object$entries2$_i = _slicedToArray(_Object$entries2[_i2], 2),
                key = _Object$entries2$_i[0],
                value = _Object$entries2$_i[1];

            djQ('.dave-cb-chatsugg').append("<div class='dave-cb-chatsugg-list'>".concat(value, "<input type='hidden' value='").concat(key, "'></div>"));
          }
        } //QUICKACCESS BUTTON NEXT & PREV ONLY VISIBLE ON SCROLL IS AVAILABLE


        var chatsugg_scrollWidth = djQ('.dave-cb-chatsugg')[0].scrollWidth;

        if (chatsugg_scrollWidth > chatbox_width - 20) {
          djQ('span.chatsugg-next').css('display', 'block');

          if (dave_optionTitle) {
            djQ('span.chatsugg-next').css('top', '10px');
            djQ('span.chatsugg-prev').css('top', '10px');
          } else {
            djQ('.dave-chatsuggTitle').hide();
          }
        }
      } else if (his_value['direction'] == 'other') {
        c_bubbleId = his_value['response_id'];
        c_bubbleId = "RI" + c_bubbleId;
        replyTo = "RI" + his_value['reply_to'];
        other_chat_data(null, his_value['user_icon'], his_value['user_name'], his_value['timestamp'], his_value['placeholder'], replyTo, his_value, c_bubbleId);
      } else {
        //user chat printing
        his_value['timestamp'] = DAVE_SETTINGS.print_timestamp(his_value['timestamp']);
        c_bubbleId = his_value['response_id'];
        c_bubbleId = "RI" + c_bubbleId;
        msgBubbles('user', 'history', his_value['timestamp'], DAVE_SETTINGS.pretty_print(his_value['customer_response'], '_name_map', '_type_map'), c_bubbleId, '', his_value['customer_state']);
      } //Chat box height Adjustment according to device


      set_inner_height();

      if (count == dave_return_size) {
        //
        djQ('button#start-recording').prop('disabled', false); //

        djQ(".dave-cb-chatarea").animate({
          scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight")
        }, 500);
        dave_typingLoaderRemove('bot');
      }
    });
    dave_userchatfunc_exe = false;
    chat_historyActivated = true;
    userchat_data();
    DAVE_SETTINGS.execute_custom_callback("after_load_history", [data]);
  }

  function botchat_history() {
    dave_chatRes_loader('bot');
    set_inner_height();
    DAVE_SETTINGS.history(function (data) {
      djQ('button#start-recording').prop('disabled', true);
      dave_return_size = Object.keys(data['history']).length;

      if (dave_return_size > 0) {
        on_load_history(data);
      } else {
        //If no history available
        chat_historyActivated = false;
        botchat_data({
          "query_type": "auto"
        });
      }
    }, function (e) {
      chatResponseError('history', e);
    }, function (data) {
      dave_return_size = Object.keys(data['history']).length;

      if (dave_return_size > 0) {
        on_load_history(data);
      }
    });
  } //USER INPUT MESSAGE SENDING TO SYSTEM ON REGULAR AND HISTORY CHAT


  var temp_bubbleId = new Array();
  var perm_bubbleId = new Array();

  function userInputBubble(chatType) {
    var dave_user_typeloader = 0;
    var clicked_pred_custState;
    djQ("#dave-cb-textinput").on("input", function () {
      text_cleared = false;
      djQ("#dave-cb-usersend").off('click').on('click', function () {
        c_bubbleId = djQ.now();
        temp_bubbleId.push(c_bubbleId);
        var dave_user_inputtext = djQ("#dave-cb-textinput").val();
        dave_user_inputtext = dave_user_inputtext.trim();

        if (dave_user_inputtext.length >= 1) {
          dave_typingLoaderRemove('user');
          userTextMsgBubble(dave_user_inputtext, 'normal', dateTiming(), c_bubbleId);
          scrollChatarea(500);
          djQ(".dave-pred-contt").html('');
          djQ(".dave-pred-contt").css({
            "height": "0",
            "overflow": "hidden",
            "padding": "0"
          });

          if (!dave_predication_send) {
            botchat_data({
              'customer_response': dave_user_inputtext,
              "query_type": "type"
            }, c_bubbleId);
          } else {
            botchat_data({
              'customer_response': dave_user_inputtext,
              'customer_state': clicked_pred_custState,
              "query_type": "click"
            }, c_bubbleId);
          } //Chat box height Adjustment according to device


          set_inner_height();
          djQ("#dave-cb-textinput").val('');
          text_cleared = true;
          dave_user_typeloader = 0;

          if (chatType == 'normal') {
            stopTimmer();
          } else if (chatType == 'history') {
            chat_historyActivated = false;
          }
        }
      });
      text_input = this.value.trim().replace(/\s/g, "");
      var dave_input_length = text_input.length;
      dave_predication_send = false;

      if (dave_input_length > 0 && dave_user_typeloader == 0) {
        dave_typingLoaderRemove('user');
        dave_user_typeloader = 1;

        if (dave_user_typeloader = 1) {
          //User typing gif
          dave_chatRes_loader('user');
          scrollChatarea(500);
        }
      } else if (dave_input_length == 0) {
        dave_user_typeloader = 0;
        dave_typingLoaderRemove('user');
        djQ('.dave-pred-contt').html('');
        pred_attach = false;
      } else if (dave_input_length >= 2 && !dave_userchatfunc_exe) {
        //User click on send icon
        //user all data print as message bubble
        //user all data then send to server
        dave_prediction(this);
      }
    });
  } //SERVER RESPONSE PRINTING FUNCTION BLOCK ENDS ENDS ENDS
  //THE CHATBOT FRONT-END ELEMENTS ASSEMBLING STARTS


  djQ('#dave-settings').append("\n        <div class='dave-cb-avatar-contt'>\n            <canvas id='dave-canvas'></canvas>\n        </div>\n        <div class='dave-chatbox-cont'>\n            <div class='dave-chatbox'>\n                <div class='dave-cb-tt-sec'>\n                    <div class='dave-tt-sec1'>\n                        <div class='dave-bottitleicon'>\n                            <img src='".concat(dave_title_icon, "'>\n                        </div>\n                        <div class='dave-cb-tt'> \n                            ").concat(dave_chatTitle, " \n                        </div>\n                    </div>\n                    <div class='close-min-max'>\n                        <div id='minchat' class='dave-cb-tt-minmax'>\n                            <img src=''>\n                        </div>\n                        <div class='dave-cb-tt-cross'>\n                            <img src=").concat(cross_img, " />\n                        </div>\n                    </div>\n                </div>\n                <div class='dave-cb-chatarea'></div>\n                <div class='dave-cb-stickBottom'>\n                    <p class=\"dave-chatsuggTitle\">").concat(dave_optionTitle, "</p>\n                    <div class='dave-cb-chatsugg'></div>\n                    <div class='dave-cb-type-sec'>\n                        <div class='dave-pred-contt'></div>\n                        <div class='dave-cb-input'>\n                        ").concat(homeBtnImg ? "<button class=\"dave-homeBtn\"><img src=\"".concat(homeBtnImg, "\" alt=\"Home\"></button>") : "", "\n                            <input type='text' name='chatbot-input-box' id='dave-cb-textinput' placeholder='Type here..' autocomplete=\"off\">\n                            <div class='dave-cb-send-icon'>\n                                <button id='dave-cb-usersend'>\n                                    <img src='").concat(send_img, "' />\n                                </button>\n                            </div>\n                        </div>\n                        <div class=\"dave-mic-contt\">\n                            <button class='dave-mic-button rec-start' id='start-recording' disabled><img src=\"").concat(mic_img, "\"></button>\n                            <button class='dave-mic-button rec-stop' id='stop-recording'><img src=\"").concat(mic_img, "\"></button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class='dave-cb-icon'>\n                <img src='").concat(dave_chatbot_icon, "'>\n            </div>\n        </div>\n        ")).ready(function () {
    djQ(window).resize(function () {
      if (djQ(window).width() != dave_window_width || djQ(window).height() != dave_window_height) {
        if (!djQ('input').is(':focus')) {
          set_maximized_height();
        }
      }

      set_inner_height();
    });
    /*HE CHATBOT FRONT-END ELEMENTS ASSEMBLING ENDS*/
    //CODE BLOCK TO AVOID OVERLAPPING CHATBOX ON OTHER PAGE ELEMENTS ON CLOSE

    chatbox_width = djQ(".dave-chatbox-cont").width();
    djQ(".dave-chatbox-cont").width(0); //SPEECH-REC CODE BLOCK STARTS

    if (DAVE_SETTINGS.SPEECH_SERVER) {
      var recButton_visibility = function recButton_visibility(startRecVis) {
        if (startRecVis) {
          nowRecording = true;
          djQ("button#start-recording").hide();
          djQ("button#stop-recording").show();
        } else {
          nowRecording = false;
          djQ("button#stop-recording").hide();
          djQ("button#start-recording").show();
        }
      };

      djQ(".dave-mic-contt button").show();
      var _voice_MsgId = undefined;
      var mic_status = undefined;
      navigator.permissions.query({
        name: 'microphone'
      }).then(function (r) {
        if (r.state === 'granted') {
          DAVE_SETTINGS.micAccess = true;
          recButton_visibility();
        } else {
          DAVE_SETTINGS.micAccess = false;
        }
      });
      djQ(document).on("click", "#start-recording", function (event) {
        djQ("#dave-cb-textinput").val('');
        window.stopTimmer();
        _voice_MsgId = DAVE_SETTINGS.generate_random_string(6);

        if (!DAVE_SETTINGS.micAccess) {
          navigator.mediaDevices.getUserMedia({
            audio: true
          }).then(function (auStream) {
            DAVE_SETTINGS.micAccess = true;
            console.log('Mic Access is provided');
          }).catch(function (e) {
            console.log('No mic access is provided');
          });
        }

        if (DAVE_SETTINGS.micAccess) {
          if (DAVE_SCENE.loaded) {
            DAVE_SCENE.stop();
          }

          recButton_visibility(true);
          DAVE_SETTINGS.StreamingSpeech.onStartVoiceRecording();
        } else {
          recButton_visibility();
          alert('Please allow permission to access microphone');
          djQ("button#start-recording").attr('title', 'You have dis-allowed the microphone. Please go to settings to allow microphone for this website');
        }

        event.stopPropagation();
      });
      djQ("#stop-recording").on("click", function () {
        DAVE_SETTINGS.StreamingSpeech.onStopVoiceRecording();
        recButton_visibility();
        djQ('button#start-recording').prop('disabled', true);
      });

      DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording = function () {
        recButton_visibility();
        djQ('button#start-recording').prop('disabled', true);
      };

      DAVE_SETTINGS.StreamingSpeech.onSocketConnect = function (o) {
        if (!nowRecording) {
          recButton_visibility();
        }

        djQ('button#start-recording').prop('disabled', false);
      };

      DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable = function (data) {
        stopTimmer();

        if (!data['final_text']) {
          var voice_rawData = JSON.stringify(data['rec_text']);
          voice_rawData = voice_rawData.slice(1, -1);
          djQ("#dave-cb-textinput").val(djQ("#dave-cb-textinput").val() + " " + voice_rawData);
        } else {
          dave_chatRes_loader('user');

          var _voice_rawData = JSON.stringify(data['final_text']);

          _voice_rawData = _voice_rawData.slice(1, -1);
          console.debug(_voice_rawData + ' =====> This is final speech text');
          djQ("#dave-cb-textinput").val('');
          msgBubbles('user', 'speech', dateTiming(), _voice_rawData, _voice_MsgId, null, _voice_MsgId);
          scrollChatarea(500);
        }
      };

      DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable = function (data) {
        if (!data['final_text']) {
          var voice_rawData = JSON.stringify(data['rec_text']);
          voice_rawData = voice_rawData.slice(1, -1);
          djQ("#dave-cb-textinput").val(djQ("#dave-cb-textinput").val() + " " + voice_rawData);
          console.debug(voice_rawData + " =====> this is in-time rec text");
        }
      };

      DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable = function (data) {
        data = data['conv_resp'];
        var v_returnSize = Object.keys(data).length;

        if (v_returnSize > 2) {
          djQ("input[value=\"".concat(_voice_MsgId, "\"]")).val(data['response_id']);
          _voice_MsgId = data['response_id'];
          response_print(data, v_returnSize, dateTiming(), "speech", _voice_MsgId);
        } else {
          c_bubbleId = djQ.now();
          userTextMsgBubble("---Silence--Noise---", 'normal', dateTiming(), c_bubbleId);
          msgBubbles('bot', 'normal', dateTiming(), 'Voice Recognition Failed! Please check your microphone settings and try again');
          scrollChatarea(500);
        }

        djQ('button#start-recording').prop('disabled', false);
      };

      DAVE_SETTINGS.StreamingSpeech.onError = function (data) {
        console.error("Speech onError is generated");
        msgBubbles('bot', 'normal', dateTiming(), 'Failed to get a response! Please try with a different query');
        scrollChatarea(500);

        if (typeof window.startTimmer == 'function') {
          window.startTimmer();
        }

        djQ('button#start-recording').prop('disabled', false);
      };

      DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect = function (o) {
        recButton_visibility();

        if (typeof window.startTimmer == 'function') {
          window.startTimmer();
        }

        djQ('button#start-recording').prop('disabled', false);
      };

      djQ(document).on("click", ".voice-thumbsup, .voice-thumbsdown", function () {
        var curr_rating = djQ(this).attr('data-thumbtype');
        var v_feedbackResId = djQ(this).siblings('input[type="hidden"]').val();
        like_dislike(this, curr_rating, "speech");

        if (v_feedbackResId) {
          DAVE_SETTINGS.feedback({
            "recognition_rating": l_d_react
          }, v_feedbackResId, function () {});
        }
      });
    } else {
      console.log("Speech Server not set.");
      djQ(".dave-mic-contt button").hide();
    }
    /*SPEECH CODE BLOCK ENDS*/
    //FUNTION TO PRINT ALL BOT RESPONSES IN CHAT-BOX PROVIDED BY DAVE_SETTINGS.chat


    window.botchat_data = function botchat_data(params, bubbleId, behaviour, nudgeopenState, lockHeight) {
      //bot chat typing gif
      if (close_count > 0) {
        return;
      }

      dave_chatRes_loader('bot');
      djQ('button#start-recording').prop('disabled', true); //pass user input to chat function

      params = params || {};
      behaviour = behaviour || false;
      nudgeopenState = nudgeopenState || false;
      lockHeight = lockHeight || maxLockHeight; //botchat_data({ 'customer_state': dave_clicked_sugg_value, 'customer_response': dave_clicked_sugg_message, "query_type": "click" }, c_bubbleId);

      DAVE_SETTINGS.chat(params, function (data) {
        var dave_return_size = Object.keys(data).length;
        response_print(data, dave_return_size, dateTiming('date'), "normal");
      }, function (e) {
        chatResponseError('normal', e, params, bubbleId, behaviour, nudgeopenState, lockHeight);
      });

      if (DAVE_SCENE.is_mobile == true) {
        nudgeopenState = 'max';
      }

      if (behaviour == 'open') {
        djQ('.dave-cb-icon').trigger('click');

        if (nudgeopenState == 'min') {
          if (chatboxOpenStatus == 'close' || chatboxOpenStatus == 'min') {
            minimize_chatbox(lockHeight);
          }
        } else if (nudgeopenState == 'max') {
          maximize_chatbox();
        }
      }
    }; //QUICK ACCESS BUTTON CODE BLOCK


    djQ(document).on("click", ".dave-cb-chatsugg-list", function () {
      stopTimmer();
      var dave_clicked_sugg_value = djQ(this).children().val();
      var dave_clicked_sugg_message = djQ(this).text();
      c_bubbleId = djQ.now();
      temp_bubbleId.push(c_bubbleId);
      userTextMsgBubble(dave_clicked_sugg_message, 'normal', dateTiming(), c_bubbleId);
      scrollChatarea(500);
      djQ("#dave-cb-textinput").val('');
      botchat_data({
        'customer_state': dave_clicked_sugg_value,
        'customer_response': dave_clicked_sugg_message,
        "query_type": "click"
      }, c_bubbleId);
    }); //OPITONS CODE BLOCK (IN MSG-BUBBLE)

    djQ(document).on("click", ".dave-option-list li button", function () {
      stopTimmer();
      var curr_clicked_option_key = djQ(this).prev().val();
      var curr_clicked_option_val = djQ(this).text();
      c_bubbleId = djQ.now();
      temp_bubbleId.push(c_bubbleId);
      userTextMsgBubble(curr_clicked_option_val, 'normal', dateTiming(), c_bubbleId);
      scrollChatarea(500);
      djQ("#dave-cb-textinput").val('');
      botchat_data({
        "customer_state": curr_clicked_option_key,
        "customer_response": curr_clicked_option_val,
        "query_type": "click"
      }, c_bubbleId);
    }); //If there is not cookie set for user and system then call botchat_data function

    if ((DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id')) && !(DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response'))) {
      botchat_history();
      dave_eng_id_status = true;
    } else if (!(DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id')) && !(DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response'))) {
      dave_eng_id_status = true;
      console.debug(DAVE_SETTINGS.getCookie('dave_engagement_id'));
      botchat_data({
        "query_type": "auto"
      });
    } else if (!dave_eng_id_status) {
      djQ('.dave-cb-stickBottom').css('display', 'block');
      botchat_history();
      dave_eng_id_status = true;
    } //Open Chat Box using Chatbox Icon


    djQ(".dave-cb-icon").on("click", function () {
      if (DAVE_SETTINGS.on_maximize() === false) {
        return;
      }

      djQ(".dave-cb-tt-cross").removeAttr('disabled');
      djQ(".dave-chatbox").addClass("dave-chatbox-open");
      djQ(".dave-cb-icon").addClass("dave-cb-icon-hide");
      djQ(".dave-chatbox-cont").width(chatbox_width);
      djQ(".dave-cb-tt-minmax img").remove();
      djQ(".dave-cb-tt-minmax").append("<img src='".concat(min_img, "'>"));
      chatboxOpenStatus = "open";
      set_maximized_height();
      djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
        minimize_chatbox();
      });
      djQ(".dave-cb-chatarea").animate({
        scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight")
      }, 500);
      dave_chat_heightchange = true;

      if (dave_window_width < 450) {
        djQ('body').css("overflow", "hidden");
      }

      if (DAVE_SCENE.loaded) {
        showAvatar(true);
      }
    }); //Send user data to chat box using Enter Button

    djQ('#dave-cb-textinput').off('keypress').on('keypress', function (e) {
      var daveInput_length = this.value.length;

      if (daveInput_length >= 1 && !dave_userchatfunc_exe) {
        if (e.which == 13) {
          djQ("#dave-cb-usersend").trigger('click');
          e.preventDefault();
        }
      }
    }); //Close chatbox using Cross "close" sign

    djQ(".dave-cb-tt-cross").click(function () {
      //feedback on close
      if (djQ(this).attr('disabled')) {
        return;
      }

      close_count++;
      now = new Date().getTime();
      previous_time = parseInt(window.localStorage.getItem('previous-feedback-time')) || data_feedback_redo_time * 1000;
      time_diff = Math.max(now - previous_time, 0);

      if (DAVE_SCENE.loaded || data_avatar_initChk) {
        showAvatar(false);
      }

      if (time_diff >= data_feedback_redo_time * 1000 && chat_loading && close_count < 2 && DAVE_SETTINGS.did_interact) {
        stopTimmer();
        maximize_chatbox();
        djQ(".dave-cb-tt-minmax ").unbind("click");
        var userfull_rate_value = 0;
        var accuracy_rate_value = 0;
        djQ('.dave-cb-chatarea').html('');
        djQ('.dave-cb-chatarea').css('padding', '0');
        djQ('.dave-cb-stickBottom').css('display', 'none');
        djQ('.dave-cb-chatarea').append("\n                            <div class=\"rating-star-bg\"></div>\n                            <div class='rating-star-contt'>\n                                ".concat(WIH_RatingTitle != "__NULL__" ? "\n                                            <p>".concat(WIH_RatingTitle || "Was I Helpful?", "</p>\n                                            <div class=\"dave-rate feedback-row-box\" id='dave-userfull-rate'>\n                                                <input type=\"radio\" id=\"dave-usefull-star5\" name=\"dave-usefull-rate\" value=\"5\" />\n                                                <label for=\"dave-usefull-star5\" title=\"dave-usefull-text\">5 stars</label>\n                                                <input type=\"radio\" id=\"dave-usefull-star4\" name=\"dave-usefull-rate\" value=\"4\" />\n                                                <label for=\"dave-usefull-star4\" title=\"dave-usefull-text\">4 stars</label>\n                                                <input type=\"radio\" id=\"dave-usefull-star3\" name=\"dave-usefull-rate\" value=\"3\" />\n                                                <label for=\"dave-usefull-star3\" title=\"dave-usefull-text\">3 stars</label>\n                                                <input type=\"radio\" id=\"dave-usefull-star2\" name=\"dave-usefull-rate\" value=\"2\" />\n                                                <label for=\"dave-usefull-star2\" title=\"dave-usefull-text\">2 stars</label>\n                                                <input type=\"radio\" id=\"dave-usefull-star1\" name=\"dave-usefull-rate\" value=\"1\" />\n                                                <label for=\"dave-usefull-star1\" title=\"dave-usefull-text\">1 star</label>\n                                            </div>\n                                        ") : "", "\n                                ").concat(WIA_RatingTitle != "__NULL__" ? "\n                                            <p>".concat(WIA_RatingTitle || "Was I Accurate?", "</p>\n                                            <div class=\"dave-rate feedback-row-box\" id='dave-accuracy-rate'>\n                                                <input type=\"radio\" id=\"dave-accuracy-star5\" name=\"dave-accuracy-rate\" value=\"5\" />\n                                                <label for=\"dave-accuracy-star5\" title=\"dave-accuracy-text\">5 stars</label>\n                                                <input type=\"radio\" id=\"dave-accuracy-star4\" name=\"dave-accuracy-rate\" value=\"4\" />\n                                                <label for=\"dave-accuracy-star4\" title=\"dave-accuracy-text\">4 stars</label>\n                                                <input type=\"radio\" id=\"dave-accuracy-star3\" name=\"dave-accuracy-rate\" value=\"3\" />\n                                                <label for=\"dave-accuracy-star3\" title=\"dave-accuracy-text\">3 stars</label>\n                                                <input type=\"radio\" id=\"dave-accuracy-star2\" name=\"dave-accuracy-rate\" value=\"2\" />\n                                                <label for=\"dave-accuracy-star2\" title=\"dave-accuracy-text\">2 stars</label>\n                                                <input type=\"radio\" id=\"dave-accuracy-star1\" name=\"dave-accuracy-rate\" value=\"1\" />\n                                                <label for=\"dave-accuracy-star1\" title=\"dave-accuracy-text\">1 star</label>\n                                            </div>\n                                        ") : "", "\n                                ").concat(writeFeedback ? "<div class='feedback-row-box'><textarea id='dave-feedback-write' placeholder='Write Your Feedback'></textarea></div>" : '', "\n                                <div class='feedback-row-box'><button id='dave-final-feedbackButton' tittle='Submit'>Submit</button></div>\n                            </div>\n                        "));

        if (WIH_RatingTitle != "__NULL__") {
          djQ('#dave-userfull-rate input').on('change', function () {
            if (djQ(this).is(':checked')) {
              userfull_rate_value = djQ(this).val();
            }
          });
        }

        if (WIA_RatingTitle != "__NULL__") {
          djQ('#dave-accuracy-rate input').on('change', function () {
            if (djQ(this).is(':checked')) {
              accuracy_rate_value = djQ(this).val();
            }
          });
        }

        djQ('#dave-final-feedbackButton').on("click", function () {
          djQ(".dave-error").remove();
          djQ(this).prop("disabled", true);
          var dave_feedback_write = djQ('#dave-feedback-write').val() || null;
          djQ('.rating-star-contt .dave-error .dave-valid').remove();

          if (WIA_RatingTitle == "__NULL__") {
            accuracy_rate_value = null;
          }

          if (WIH_RatingTitle == "__NULL__") {
            userfull_rate_value = null;
          }

          if ((WIA_RatingTitle == '__NULL__' || accuracy_rate_value > 0) && (WIH_RatingTitle == "__NULL__" || userfull_rate_value > 0)) {
            djQ(".dave-cb-tt-cross").attr('disabled', true);
            DAVE_SETTINGS.feedback({
              "usefulness_rating": userfull_rate_value,
              "accuracy_rating": accuracy_rate_value,
              "feedback": dave_feedback_write
            }, null, function (data) {
              dave_eng_id_status = false;
              djQ('.dave-error').remove();
              djQ('.dave-valid').remove();
              djQ('.rating-star-contt').append("<p class='dave-valid'>".concat(feedback_successMSG, "</p>"));
            });
            setTimeout(function () {
              if (DAVE_SETTINGS.on_close() === false) {
                return;
              }

              chatboxOpenStatus = 'close';
              stopTimmer();
              close_count = 0;
              djQ('.dave-cb-chatarea').html('');
              djQ(".dave-chatbox").removeClass("dave-chatbox-open");
              djQ(".dave-cb-icon").removeClass("dave-cb-icon-hide");
              djQ('body').css('overflow', originalScroll);
              djQ(".dave-chatbox-cont").width(0);
              djQ('.dave-cb-chatarea').css('padding', "10px 15px 0px 15px");
              window.localStorage.setItem('previous-feedback-time', now);
              djQ(this).prop("disabled", false);

              if (!dave_eng_id_status) {
                djQ('.dave-cb-stickBottom').css('display', 'block');
                botchat_history();
                dave_eng_id_status = true;
              }

              DAVE_SETTINGS.execute_custom_callback("after_close_custom");
            }, 2000);
          } else {
            djQ(this).prop("disabled", false);
            djQ('body').css('overflow', originalScroll);
            djQ('.dave-error').remove();
            djQ('.dave-valid').remove();
            djQ('.rating-star-contt').append("<p class='dave-error'>Please Do Give Star Ratings!</p>");
          }
        });
      } else {
        chatboxOpenStatus = 'close';

        if (DAVE_SETTINGS.on_close() === false) {
          return;
        }

        ;

        if (typeof startTimmer === "function") {
          startTimmer();
        }

        djQ('body').css('overflow', originalScroll);
        close_count = 0;
        djQ('.dave-cb-chatarea').html('');
        djQ('.dave-cb-chatarea').css('padding', '');
        djQ('.dave-cb-stickBottom').css('display', 'none');
        dave_eng_id_status = false;
        djQ(".dave-chatbox-cont").width(0);
        djQ(".dave-chatbox").removeClass("dave-chatbox-open");
        djQ(".dave-cb-icon").removeClass("dave-cb-icon-hide");

        if (!dave_eng_id_status) {
          djQ('.dave-cb-stickBottom').css('display', 'block');
          botchat_history();
          dave_eng_id_status = true;
        }

        DAVE_SETTINGS.execute_custom_callback("after_close_custom");
      } // djQ(".dave-cb-tt-minmax").one("click", function () {
      //     minimize_chatbox(maxLockHeight, 'feedback');
      // });

      /*if (dave_window_width > 450) { djQ(".dave-chatbox-cont").css("width", "5%"); } else { djQ(".dave-chatbox-cont").css("width", "25%"); }*/

    }); //Chat Suggestion Next and Prev Button
    //NEXT BUTTON 

    djQ(".dave-cb-chatsugg").on("click", ".chatsugg-next", function () {
      djQ(".dave-cb-chatsugg").animate({
        scrollLeft: "+=50px"
      }, 300);
      djQ(".dave-cb-chatsugg").on("scroll", function () {
        djQ("span.chatsugg-prev").css("display", "block");

        if (djQ(this).scrollLeft() + djQ(this).innerWidth() >= djQ(this)[0].scrollWidth) {
          djQ("span.chatsugg-next").css("display", "none");
        } else if (djQ(this).scrollLeft() == 0) {
          djQ("span.chatsugg-prev").css("display", "none");
        }
      });
    }); //PREV BUTTON

    djQ(".dave-cb-chatsugg").on("click", ".chatsugg-prev", function () {
      djQ(".dave-cb-chatsugg").animate({
        scrollLeft: "-=50px"
      }, 300);
      djQ(".dave-cb-chatsugg").on("scroll", function () {
        djQ("span.chatsugg-next").css("display", "block");

        if (djQ(this).scrollLeft() == 0) {
          djQ("span.chatsugg-prev").css("display", "none");
        } else if (djQ(this).scrollLeft() + djQ(this).innerWidth() >= djQ(this)[0].scrollWidth) {
          djQ("span.chatsugg-prev").css("display", "block");
        }
      });
    }); //THUMBS FEEDBACK FOR BUBBLE

    djQ(document).on('click', '.dave-thumbsup, .dave-thumbsdown', function () {
      eng_id = djQ(this).siblings('span').text();
      curr_rating = djQ(this).attr('data-thumbType');
      like_dislike(this, curr_rating, "normal");
      DAVE_SETTINGS.feedback({
        "response_rating": l_d_react
      }, eng_id, function (data) {});
      djQ(this).closest('.dave-botchat').append("<p class=\"dave-feedback-msg\">Thank you for your feedback</p>");
    }); // Form File Upload

    djQ(document).on("change", ".file_upload", function (data) {
      //add file uploading loader here
      djQ(this).before("<div class='file_upload_loader'><img src='".concat(typing_gif, "'></div>"));

      if (data.target.value.length > 0) {
        var curr_file = djQ(this);
        var maxUploadSize = djQ(this).attr('data-max') || 1024 * 1024 * 1024 * 0.5;
        var selectedFiles = data.target.files[0];
        var selectedFileName = data.target.files[0].name;
        var selectedFileType = data.target.files[0].type;
        var selectedFileSize = data.target.files[0].size;
        selectedFileSize = selectedFileSize / (1024 * 1024);
        var selectedFilePath = djQ(this).val();
        var fileReader = new FileReader();

        fileReader.onloadend = function (data) {
          var arrayBuffer = data.target.result;
          var blob = blobUtil.arrayBufferToBlob(arrayBuffer, selectedFileType);
          console.debug("File blob is:");
          console.debug(blob);

          if (selectedFileSize <= maxUploadSize) {
            if (selectedFileSize && selectedFilePath && selectedFileName) {
              DAVE_SETTINGS.upload_file(blob, selectedFileName, function (data) {
                console.debug("uploaded file response" + data); //remove loader

                var fileInputNameAttr = curr_file.attr('name');
                djQ(curr_file).data('fileUrl', data['path']);
                curr_file.prev(".file_upload_loader").remove();
                console.debug("upload_file function executed"); //file uploaded successfully

                curr_file.closest('form').find('.dave-error').remove();
                djQ(curr_file).data('fileUploadedStatus', 'true');
              }, function (e) {
                //remove loader and show error
                curr_file.prev().remove();
                curr_file.closest('form').find('.dave-error').remove();
                curr_file.closest('form').append("<p class='dave-error'>Something went wrong while upload file!</p>");
                scrollChatarea(500);
                djQ(curr_file).attr('fileUploadedStatus', 'false');
                console.error("The error while uploading file=> " + e);
              });
            }
          } else {
            //file size exceed error message
            console.error("File Size Exceed");
            curr_file.closest('form').find('.dave-error').remove();
            curr_file.closest('form').append("<p class='dave-error'>File size exceed the maximum allowed size of ".concat(djQ(this).attr('data-max'), " MB!</p>"));
            scrollChatarea(500);
          }
        };

        fileReader.readAsArrayBuffer(selectedFiles);
      } else {
        console.debug("No file selected now");
      }
    }); //FORM SUBMITTING CODE BLOCK

    djQ(document).on('submit', 'form.dave-form', function (e) {
      var formData = {};
      var printData = {};
      var nameMap = {};
      var typeMap = {};
      var dataValidated = true;
      var f_bubbleId = djQ(this).closest('.dave-chattext').prev('.scrollToMsg').attr('data-scrollmsgid');
      var c_form = djQ(this);
      var form_error = '';

      if (djQ(this).length <= 1 && map_enabled) {
        if (typeof userPositon['lat'] == "undefined" || typeof userPositon['lng'] == "undefined") {
          dataValidated = false;
          form_error += djQ(this).prev().attr('error') || 'Input for "' + djQ(this).prev().attr('title') + '" is required!' + '<br/><br/>';
        } else {
          formData['lat'] = userPositon['lat'].toString();
          formData['lng'] = userPositon['lng'].toString();
          printData['lat'] = userPositon['lat'].toString();
          printData['lng'] = userPositon['lng'].toString();
          printData = JSON.stringify(printData);
          console.debug(printData);
          console.warn(formData);
        }
      }

      djQ.each(djQ(this).serializeArray(), function (i, j) {
        var k = djQ(c_form).find("input[name='" + j.name + "']").first();

        if (!k.length) {
          k = djQ(c_form).find("textarea[name='" + j.name + "']").first();
        }

        if (!k.length) {
          k = djQ(c_form).find("select[name='" + j.name + "']").first();
        }

        if ((j.value == "" || j.value == null || j.value == undefined) && k.attr('required') && !map_enabled) {
          dataValidated = false;
          form_error += k.attr('error') || 'Input for "' + k.attr('title') + '" is required!' + '<br/><br/>';
        } else if (map_enabled) {
          if (j.value == "" || j.value == null || j.value == undefined || typeof userPositon['lat'] == "undefined" || typeof userPositon['lng'] == "undefined") {
            dataValidated = false;
            form_error += k.attr('error') || 'Input for "' + k.attr('title') || "form" + '" is required!' + '<br/><br/>';
          }
        } else if (k.attr('ui_element') == 'datetime' && j.value) {
          var a = new Date(k.datetimepicker('getValue'));
          var b = new Date(k.attr('min'));
          var c = new Date(k.attr('max'));

          if (b.toLocaleString() == 'Invalid Date') {
            b = new Date(-8640000000000000);
          }

          if (c.toLocaleString() == 'Invalid Date') {
            c = new Date(8640000000000000);
          }

          if (!(b <= a && c >= a)) {
            dataValidated = false;
            form_error += 'Input for "' + k.attr('title') + '" should be between ' + DAVE_SETTINGS.print_timestamp(b, null, true) + ' and ' + DAVE_SETTINGS.print_timestamp(c, null, true) + '!<br/><br/>';
          } else {
            // If print_timestamp can print the original string taken, then we are good
            j.print_value = a.toString();
          }
        } else if (k.attr('ui_element') == 'date' && j.value) {
          var _a = new Date(k.datetimepicker('getValue')); // If print_timestamp can print the original string taken, then we are good


          j.print_value = _a.toString();
        } else if (k.attr('ui_element') == 'select' && j.value) {
          j.print_value = djQ(k).find("option:selected").text();
        } else if (k.attr('type') == 'file') {
          if (k.data('fileUploadedStatus') == 'false') {
            dataValidated = false;
            form_error += k.attr('title') + ':-' + (k.attr('error') || 'Error in uploading file!') + '<br/><br/>';
          } else if (k.attr('required')) {
            dataValidated = false;
            form_error += k.attr('error') || 'Input for "' + k.attr('title') + '" is required!' + '<br/><br/>';
          } else {
            j.value = k.data('fileUrl');
          }
        }

        if (j.value && k.attr('validate')) {
          j.value = j.value.trim();
          var reg = new RegExp('\\b' + k.attr('validate') + '\\b');

          if (!reg.test(j.value)) {
            dataValidated = false;
            form_error += k.attr('title') + ':-' + (k.attr('error') || 'Value does not match expected pattern!') + '<br/><br/>';
          }
        }

        if (map_enabled) {
          formData['lat'] = userPositon['lat'];
          formData['lng'] = userPositon['lng'];
        }

        formData[j.name] = j.value;
        printData[j.name] = j.print_value || j.value;

        if (k.attr('title')) {
          nameMap[j.name] = k.attr('title');
        }

        if (k.attr('ui_element')) {
          typeMap[j.name] = k.attr('ui_element');
        }
      });
      djQ(this).find("input[type=file]").each(function () {
        var k = djQ(this);

        if (k.data('fileUploadedStatus') == 'false') {
          dataValidated = false;
          form_error += (k.attr('title') || k.attr('name')) + ":- " + k.attr('error') || 'Error in uploading file! <br/><br/>';
        } else if (k.data('fileUploadedStatus') == 'true') {
          formData[k.attr('name')] = k.data('fileUrl');

          if (k.attr('title')) {
            nameMap[k.attr('name')] = k.attr('title');
          }

          if (k.attr('ui_element')) {
            typeMap[k.attr('name')] = k.attr('ui_element');
          }
        } else if (k.attr('required')) {
          dataValidated = false;
          form_error += k.attr('error') || 'Input for "' + (k.attr('title') || k.attr('name')) + '" is required!' + '<br/><br/>';
        }
      });

      if (dataValidated) {
        formData['_name_map'] = nameMap;
        formData['_type_map'] = typeMap;
        c_bubbleId = djQ.now();
        temp_bubbleId.push(c_bubbleId);
        userTextMsgBubble(DAVE_SETTINGS.pretty_print(printData, nameMap, typeMap), 'normal', dateTiming(), c_bubbleId);
        djQ(this).find('.dave-error').remove();
        customerState = djQ(this).next().text();
        botchat_data({
          "customer_response": JSON.stringify(formData),
          "customer_state": customerState,
          "query_type": "click"
        }, c_bubbleId);
        DAVE_SETTINGS.execute_custom_callback('on_form_submit', [formData, customerState]);
        djQ(this).find('input[type="submit"]').prop('disabled', true);
        djQ(this).find('input').prop('disabled', true);
        djQ(this).find('textarea').prop('disabled', true);
        djQ(this).find('select').prop('disabled', true);
        scrollChatarea(500);
      } else {
        djQ(this).find('.dave-error').remove();
        djQ(this).append("<p class='dave-error'>".concat(form_error, "</p>"));
        scrollChatarea(500);
      }

      e.preventDefault();
    });
    dave_nudge(); //End of dave settings env
  });
  DAVE_SETTINGS.execute_custom_callback("after_load_chatbot");
};

djQ(document).ready(function () {
  DAVE_SETTINGS.botchat_data = window.botchat_data;

  if (djQ("#dave-settings").length) {
    window.dave_load_chatbot();
  }
});
/**
 * ##### DAVE_SETTINGS.register_nudge
 * Registering nudges based on activity on the webpage.
 * @param {jQuery} identifier   element to bind the action event to
 * @param {string} identifier (jquery selector) element to bind the action event to
 * @param {string} customer_state
 * @param {string} customer_response
 * @param {string} event_type   The the jQuery event type on which to trigger the nudge, defaults to 'click'
 * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
 * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
 * @param {object} data    Any extra data to be sent to chatbot 
 *
 * e.g. DAVE_SETTINGS.register_nudge("#MyId", "cs_nudge_state", "Nudge me here", "click", "max", null, {"my_new_data": "this data"})
 */

DAVE_SETTINGS.register_nudge = function register_nudge(identifier, customer_state, customer_response, event_type, nudgeopenState, lockHeight, data) {
  event_type = event_type || 'click';
  data = data || null;
  customer_response = customer_response || DAVE_SETTINGS.toTitleCase(customer_state);
  nudgeopenState = nudgeopenState || 'min';
  djQ(identifier).on(event_type, function () {
    DAVE_SETTINGS.create_event(customer_state);
    botchat_data({
      'customer_state': customer_state,
      "customer_response": customer_response,
      "query_type": "auto",
      "temporary_data": data
    }, null, 'open', nudgeopenState, lockHeight);
  });
};

djQ(document).ready(function () {
  if (DAVE_HELP) {
    /**
     * ##### DAVE_HELP.register_help_nudge
     * Registering help nudges based on activity on the webpage. The help text opens up. When we click on the help text, a nudge is sent
     * @param {jQuery} identifier   element to bind the action event to
     * @param {string} identifier (jquery selector) element to bind the action event to
     * @param {string} help_text  Text show in the help pop-up
     * @param {string} customer_state
     * @param {string} customer_response
     * @param {string} event_type   The the jQuery event type on which to trigger the nudge, defaults to 'click'
     * @param {string} help_sub_text  Sub-Text to show in the help pop-up, not required, defaults to "Click here for more help"
     * @param {jQuery} anchor_dom  element relative to which the help text should occur, defaults to 'dom' if evaluates to false
     * @param {string} anchor_dom (jquery selector) element relative to which the help text should occur, defaults to 'dom'
     * @param {object} pos  {position: right/left/above/below, length: distance from dom, xaway: x distance from dom, yaway: y distance form dom}
     * @param {string} pos  right/left/above/below
     * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
     * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
     * @param {object} data    Any extra data to be sent to chatbot 
     *
     * e.g. DAVE_HELP.register_help_nudge("#MyId", "About this topic", "cs_nudge_state", "Nudge me here", "click", "I need more info", "above", "#MyOtherElement", 5000, 'max', "450px", {"other_data": "this other data"})
     */
    DAVE_HELP.register_help_nudge = function register_help_nudge(identifier, help_text, customer_state, customer_response, event_type, help_sub_text, pos, anchor_dom, timeout, nudgeopenState, lockHeight, data) {
      customer_response = customer_response || DAVE_SETTINGS.toTitleCase(customer_state);
      nudgeopenState = nudgeopenState || 'max';
      DAVE_HELP.bind_help_for_element(identifier, help_text, help_sub_text || "Click here for more help", event_type, pos, anchor_dom, timeout, function (elem) {
        DAVE_SETTINGS.create_event(customer_state);
        botchat_data({
          'customer_state': customer_state,
          "customer_response": customer_response,
          "query_type": "auto",
          "temporary_data": data || null
        }, null, 'open', nudgeopenState, lockHeight);
      });
    };
  }
}); // Creates a passive nudge

/**
 * ##### DAVE_SETTINGS.register_passive_nudge
 * Registering nudge to open the chat when there is no activity by the user or the user is passive
 * @param {number} timout   time in milliseconds of user passivity which will trigger this nudge
 * @param {string} customer_state
 * @param {string} customer_response
 * @param {array} cancel_events     Any events we have registered, which will cancel this passive nudge.
 * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
 * @param {string} reset_events  JQuery sequence of events which will cause the timer to be reset, default to most of the common activity events
 * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
 * @param {object} data    Any extra data to be sent to chatbot 
 *
 * e.g. DAVE_SETTINGS.register_passive_nudge(20000, "cs_nudge_state", "Nudge me here", ["User's Next Event", "User's possible event"], 'min', 'touchstart click keydown scroll dblclick resize', "450px", {'extra_data': 'extra_data'} )
 */

DAVE_SETTINGS.register_passive_nudge = function register_passive_nudge(timeout, customer_state, customer_response, cancel_events, nudgeopenState, reset_events, lockHeight, random_string, data) {
  customer_response = customer_response || DAVE_SETTINGS.toTitleCase(customer_state);
  nudgeopenState = nudgeopenState || 'min';
  cancel_events = DAVE_SETTINGS.makeList(cancel_events);
  random_string = random_string || DAVE_SETTINGS.generate_random_string(6); //if ( cancel_events ) {
  //    reset_events = reset_events || '__NULL__';
  //} else {
  //    reset_events = reset_events || 'touchstart click keydown scroll dblclick resize';
  //}

  reset_events = reset_events || 'touchstart click keydown scroll dblclick resize';
  console.log("Starting timer for customer state " + customer_state + " - " + timeout);

  if (!DAVE_SETTINGS.event_timers) {
    DAVE_SETTINGS.event_timers = {};
  }

  var t = random_string + customer_state;

  if (DAVE_SETTINGS.event_timers[t]) {
    console.log("Same passive nudge found already, so clearing the timer");
    clearTimeout(DAVE_SETTINGS.event_timers[t]);
  }

  DAVE_SETTINGS.event_timers[t] = setTimeout(function () {
    console.log("Timeout done for customer state " + customer_state);
    delete DAVE_SETTINGS.event_timers[t];
    botchat_data({
      'customer_state': customer_state,
      "customer_response": customer_response,
      "query_type": "auto",
      "temporary_data": data || null
    }, null, 'open', nudgeopenState, lockHeight);
  }, timeout);

  if (cancel_events && DAVE_SETTINGS.EVENT_MODEL) {
    if (!DAVE_SETTINGS.cancel_events) {
      DAVE_SETTINGS.cancel_events = {};
    }

    var _iterator = _createForOfIteratorHelper(cancel_events),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var k = _step.value;

        if (!DAVE_SETTINGS.cancel_events[k]) {
          DAVE_SETTINGS.cancel_events[k] = [];
        }

        DAVE_SETTINGS.cancel_events[k].push(t);
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  }

  if (reset_events != "__NULL__") {
    djQ(document).one(reset_events, function (e) {
      if (e.type == 'keydown' && (e.which < 30 || e.which > 90 && e.which < 96 || e.which > 105)) {
        return true;
      }

      if (DAVE_SETTINGS.event_timers && DAVE_SETTINGS.event_timers[t]) {
        console.debug("Restarting timer for customer state " + customer_state + " - " + timeout);
        clearTimeout(DAVE_SETTINGS.event_timers[t]);
        delete DAVE_SETTINGS.event_timers[t];
        setTimeout(function () {
          DAVE_SETTINGS.register_passive_nudge(timeout, customer_state, customer_response, cancel_events, nudgeopenState, reset_events, lockHeight, random_string, data);
        }, timeout);
      }

      return true;
    });
  }
}; // Binds a passive nudge

/**
 * ##### DAVE_SETTINGS.bind_passive_nudge
 * Bind the registering of a passive nudge to a user event. After the event occurs, and no other activity occurs after this event, then the chat would open with the given customer_state
 * @param {jQuery} identifier   element to bind the action event to
 * @param {string} identifier (jquery selector) element to bind the action event to
 * @param {number} timout   time in milliseconds of user passivity which will trigger this nudge
 * @param {string} customer_state
 * @param {string} customer_response
 * @param {string} event_type   The the jQuery event type on which to trigger the registration of the nudge, defaults to 'click'
 * @param {array}  cancel_events     Any dave events we have registered, which will cancel this passive nudge.
 * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
 * @param {string} reset_events  JQuery sequence of events which will cause the timer to be reset, default to most of the common activity events
 * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
 * @param {object} data    Any extra data to be sent to chatbot 
 *
 * e.g. DAVE_SETTINGS.bind_passive_nudge("#MyId", 20000, "cs_nudge_state", "Nudge me here", "click", ["User's Next Event", "User's possible event"]], 'min', 'touchstart click keydown scroll dblclick resize', "450px", {'extra_data': 'extra_data'} )
 */


DAVE_SETTINGS.bind_passive_nudge = function initiate_passive_nudge(identifier, timeout, customer_state, customer_response, event_type, cancel_events, nudgeopenState, reset_events, lockHeight, data) {
  event_type = event_type || 'click';
  djQ(identifier).on(event_type, function () {
    DAVE_SETTINGS.register_passive_nudge(timeout, customer_state, customer_response, cancel_events, nudgeopenState, reset_events, lockHeight, identifier + event_type, data);
  });
};
/** @suppress{duplicate} */
DAVE_HELP = {
  timers: {},
  enabled: DAVE_SETTINGS.getCookie('dave_help', true),
  fadeout: 3000,
  on_image: DAVE_SETTINGS.asset_path + 'assets/img/on-image.svg',
  off_image: DAVE_SETTINGS.asset_path + 'assets/img/off-image.svg',
  default_length: 10
}; // namespace

/**
 * Add the help overlay SVG panel.
 */

djQ(document).ready(function () {
  DAVE_HELP.on_image = djQ('#dave-help').attr('data-on-image') || DAVE_SETTINGS.asset_path + 'assets/img/on-image.svg';
  DAVE_HELP.off_image = djQ('#dave-help').attr('data-off-image') || DAVE_SETTINGS.asset_path + 'assets/img/off-image.svg';
  DAVE_HELP.timeout = parseInt(djQ('#dave-help').attr('data-timeout'));
  DAVE_HELP.fadeout = parseInt(djQ('#dave-help').attr('data-fadeout') || DAVE_HELP.fadeout);

  if (!isFinite(DAVE_HELP.timeout)) {
    DAVE_HELP.timeout = null;
  }

  djQ('#dave-help').append("\n    <img src='".concat(DAVE_HELP.on_image, "' class='dave-help-control' id='dave-help-on-image'>\n    <img src='").concat(DAVE_HELP.off_image, "' class='dave-help-control' id='dave-help-off-image'>\n    <p class='dave-help-control'> Help Text</p>\n    "));
  djQ('#dave-help').hide();

  if (DAVE_HELP.enabled) {
    DAVE_HELP.enable();
  } else {
    DAVE_HELP.disable();
  }
});

DAVE_HELP.get_help_text = function (text, subtext, id) {
  id = id || DAVE_SETTINGS.generate_random_string(6);
  var dst = subtext ? 'block' : 'none';
  var dt = text ? 'block' : 'none';
  var r = djQ("\n        <div id='dave-help-text-".concat(id, "' class='dave-help-text dave-help-shadow'>\n            <span id='dave-help-close-").concat(id, "' class='dave-help-close-symbol'>&#x2715;</span>\n            <span class='dave-help-close'>Close Help</span>\n            <p class='dave-help-paragraph' style='display:").concat(dt, "'>").concat(text, "</p>\n            <p class='dave-help-sub-text' style='display:").concat(dst, "'>").concat(subtext, "</p>\n        </div>\n    "));
  r.data('id', id);
  return r;
};

DAVE_HELP.add_help_text = function (text, subtext, left, top, timeout, elem, id, clickCallback) {
  if (!DAVE_HELP.enabled) {
    return;
  }

  id = id || DAVE_SETTINGS.generate_random_string(6);
  elem = elem || DAVE_HELP.get_help_text(text, subtext, id);

  if (!elem.attr('id') == 'dave-help-text-' + id) {
    elem.attr('id', 'dave-help-text-' + id);
    elem.find('.dave-help-close-symbol').each(function () {
      this.attr('id', 'dave-help-close-' + id);
    });
  }

  left = left || 'calc( 50vw - 200px )';
  top = top || '10px';
  timeout = timeout || DAVE_HELP.timeout;
  elem.css('top', top);
  elem.css('left', left);
  djQ(document.body).append(elem);
  djQ('#dave-help-close-' + id).one('click', function () {
    DAVE_HELP.remove(id);
  });

  if (timeout < 3600000) {
    djQ(window).one('scroll resize', function () {
      DAVE_HELP.remove(id, DAVE_HELP.fadeout);
    });
  }

  if (timeout < 3600000) {
    var t = setTimeout(function () {
      DAVE_HELP.remove(id, DAVE_HELP.fadeout);
    }, timeout);
    DAVE_HELP.timers[id] = t;
  }

  if (clickCallback && typeof clickCallback == 'function') {
    djQ(elem).on('click', function () {
      clickCallback($(this));
    });
  }

  return elem;
};

DAVE_HELP.enable = function () {
  DAVE_HELP.enabled = true;
  djQ('#dave-help-off-image').hide();
  djQ('#dave-help-on-image').show();
  DAVE_SETTINGS.setCookie('dave_help', true);
  djQ('#dave-help-on-image').one('click', function () {
    DAVE_HELP.disable();
  });
};

DAVE_HELP.disable = function () {
  DAVE_HELP.enabled = false;
  djQ('#dave-help-on-image').hide();
  djQ('#dave-help-off-image').show();
  DAVE_SETTINGS.setCookie('dave_help', false);
  djQ('#dave-help-off-image').one('click', function () {
    DAVE_HELP.enable();
  });
};

DAVE_HELP.remove = function (id, fadeout) {
  var elem = '.dave-help-text';

  if (id && typeof id == 'string') {
    elem = '#dave-help-text-' + id;
  } else if (id) {
    elem = id;
  }

  var svg = djQ(elem);

  if (!svg || svg.length <= 0) {
    return;
  }

  svg.fadeOut(fadeout || 0, svg.remove);

  if (id) {
    if (DAVE_HELP.timers[id]) {
      clearTimeout(DAVE_HELP.timers[id]);
      delete DAVE_HELP.timers[id];
    }
  } else {
    for (var k in DAVE_HELP.timers) {
      clearTimeout(DAVE_HELP.timers[k]);
      delete DAVE_HELP.timers[k];
    }
  }
};

DAVE_HELP.is_overlap = function (elem1_left, elem1_top, elem1_width, elem1_height, elem2_left, elem2_top, elem2_width, elem2_height) {
  var elem1_right = elem1_left + elem1_width;
  var elem1_bottom = elem1_top + elem1_height;
};
/**
 * Add a help label to the specified position.
 * @param {number} tox
 * @param {number} toy
 * @param {string} label
 * @param {string} pos
 * @param {number=} length
 */


DAVE_HELP.get_position = function (dom, elem, pos, length, xaway, yaway) {
  var offset = dom.offset();
  var width = dom.outerWidth();
  var height = dom.outerHeight();
  var eheight = elem.outerHeight();
  var ewidth = elem.outerWidth();
  var pwidth = djQ(window).width();
  var pheight = djQ(window).height();
  var awayx;
  var awayy;
  xaway = xaway || 1;
  yaway = yaway || 1;
  var left = 0;
  var top = 0;

  if (length) {
    xaway *= length;
    yaway *= length;
  }

  if (pos == "right") {
    left = Math.min(offset.left + width + xaway, pwidth - ewidth - xaway);

    if (left >= offset.left && left <= offset.left + width || left + ewidth >= offset.left && left + ewidth <= offset.left + width || left <= offset.left && left + ewidth >= offset.left + width) {
      if (offset.top < eheight + yaway) {
        top = offset.top + height + yaway;
      } else {
        top = offset.top - eheight - yaway;
      }
    } else {
      top = offset.top;
    }

    top = Math.max(top - djQ(window).scrollTop(), 0);
    return [left, top];
  }

  if (pos == "left") {
    left = Math.max(offset.left - xaway - ewidth, 0);

    if (left >= offset.left && left <= offset.left + width || left + ewidth >= offset.left && left + ewidth <= offset.left + width || left <= offset.left && left + ewidth >= offset.left + width) {
      if (offset.top < eheight + yaway) {
        top = offset.top + height + yaway;
      } else {
        top = offset.top - eheight - yaway;
      }
    } else {
      top = offset.top;
    }

    top = Math.max(top - djQ(window).scrollTop(), 0);
    return [left, top];
  }

  if (pos == "above") {
    top = Math.max(offset.top - yaway - eheight - djQ(window).scrollTop(), 0);

    if (top >= offset.top && top <= offset.top + height || top + eheight >= offset.top && top + eheight <= offset.top + height || top <= offset.top && top + eheight >= offset.top + height) {
      if (offset.left < ewidth + xaway) {
        left = offset.left + width + xaway;
      } else {
        left = offset.left - ewidth - xaway;
      }
    } else {
      left = offset.left;
    }

    left = Math.min(Math.max(left, 0), pwidth);
    return [left, top];
  }

  if (pos == "below") {
    top = Math.min(offset.top + yaway - djQ(window).scrollTop(), pheight);

    if (top >= offset.top && top <= offset.top + height || top + eheight >= offset.top && top + eheight <= offset.top + height) {
      if (offset.left < ewidth + xaway) {
        left = offset.left + width + xaway;
      } else {
        left = offset.left - ewidth - xaway;
      }
    } else {
      left = offset.left;
    }

    left = Math.min(Math.max(left, 0), pwidth);
    return [left, top];
  }

  if (pos == "aboveleft") {
    top = Math.max(offset.top - yaway - eheight - djQ(window).scrollTop(), 0);
    left = Math.max(offset.left - xaway - ewidth, 0);

    if (top >= offset.top && top <= offset.top + height || top + eheight >= offset.top && top + eheight <= offset.top + height || top <= offset.top && top + eheight >= offset.top + height) {
      if (offset.left < ewidth + xaway) {
        left = offset.left + width + xaway;
      } else {
        left = offset.left - ewidth - xaway;
      }
    } else if (left >= offset.left && left <= offset.left + width || left + ewidth >= offset.left && left + ewidth <= offset.left + width || left <= offset.left && left + ewidth >= offset.left + width) {
      if (offset.top < eheight + yaway) {
        top = offset.top + height + yaway;
      } else {
        top = offset.top - eheight - yaway;
      }
    } else {
      left = offset.left;
    }

    left = Math.min(Math.max(left, 0), pwidth);
    return [left, top];
  }

  return [pwidth / 2 - ewidth / 2, length];
};
/**
 * ##### DAVE_HELP.help_for_element
 * #####  Shows help label relative to an element on the screen.   #######
 * @param {jQuery} dom
 * @param {string} dom (jquery selector)
 * @param {string} text
 * @param {string} subtext
 * @param {object} pos  {position: right/left/above/below, length: distance from dom, xaway: x distance from dom, yaway: y distance form dom}
 * @param {string} pos  right/left/above/below  position relative to which the the help-text has to be displayed
 * @param {number} timout 
 * @param {function} clickCallback    function called if help-text is clicked, not required, 
 * @param {function} displayCallback    function called if help-text is displayed, not required, 
 *
 * 
 * e.g. DAVE_HELP.help_for_element("#MyId", "This is a help", "Click me", "above", 5000, function() {console.log("This help text was clicked");}, function() {console.log("This help text was displayed")})
 */


DAVE_HELP.help_for_element = function (dom, text, subtext, pos, timeout, clickCallback, displayCallback) {
  if (!DAVE_HELP.enabled) {
    return;
  }

  dom = DAVE_HELP.process_dom(dom, true);

  if (!dom) {
    return;
  }

  pos = pos || {
    'position': 'right',
    'length': DAVE_HELP.default_length
  };

  if (typeof pos == 'string') {
    pos = {
      'position': pos,
      'length': DAVE_HELP.default_length
    };
  }

  if (!pos.position) {
    pos.position = 'right';
  }

  if (pos.top && pos.left) {
    var elem = DAVE_HELP.add_help_text(text, subtext, pos.left, pos.top, timeout, null, null, clickCallback);

    if (displayCallback && typeof displayCallback == 'function') {
      displayCallback(elem);
    }

    return elem;
  }

  if (['above', 'below', 'left', 'right', 'aboveleft', 'aboveright', 'belowleft', 'belowright'].indexOf(pos.position) < 0) {
    console.error("Incorrect positon information " + pos.position);
    return;
  }

  if (!pos.length) {
    if (pos.xaway || pos.yaway) {
      pos.length == 1;
    }

    pos.length = DAVE_HELP.default_length;
  }

  var id = DAVE_SETTINGS.generate_random_string(6);
  var elem = DAVE_HELP.add_help_text(text, subtext, null, null, timeout, null, null, clickCallback);
  var np = DAVE_HELP.get_position(dom, elem, pos.position, pos.length, pos.xaway, pos.yaway);
  elem.css('left', np[0]);
  elem.css('top', np[1]);

  if (displayCallback && typeof displayCallback == 'function') {
    displayCallback(elem);
  }

  return elem;
};

DAVE_HELP.process_dom = function (dom, one) {
  var sdom = dom;

  if (typeof dom == 'string') {
    dom = djQ(dom);
  } else {
    sdom = dom.first().attr('id');
  }

  if (!dom || dom.length <= 0) {
    console.error("Dom element to add help text not found: " + sdom);
    return;
  }

  if (one) {
    return dom.first();
  }

  return dom;
};
/**
 * ##### DAVE_HELP.bind_help_for_element
 * Function binds the help text event to an element, but shows it only once since the document loads. It works even if the help text is not enabled at the time of binding
 * @param {jQuery} dom   element to bind the action event to
 * @param {string} dom (jquery selector) element to bind the action event to
 * @param {string} text
 * @param {string} subtext
 * @param {string} action   The the jQuery event type on which to trigger the help text, defaults to 'click'
 * @param {jQuery} anchor_dom  element relative to which the help text should occur, defaults to 'dom' if evaluates to false
 * @param {string} anchor_dom (jquery selector) element relative to which the help text should occur, defaults to 'dom'
 * @param {object} pos  {position: right/left/above/below, length: distance from dom, xaway: x distance from dom, yaway: y distance form dom}
 * @param {string} pos  right/left/above/below
 * @param {number} timout   time in milliseconds taken to disappear, not required, default can be set globally in DAVE_HELP.timeout
 * @param {function} clickCallback    function called if help-text is clicked, not required, 
 * @param {function} displayCallback    function called if help-text is displayed, not required, 
 *
 * e.g. DAVE_HELP.bind_help_for_element("#MyId", "This is a help", "Click me", "click", "above", "#MyOtherElement", 5000, function() {console.log("This help text was clicked");}, function() {console.log("This help text was displayed")})
 */


DAVE_HELP.bind_help_for_element = function (dom, text, subtext, action, pos, anchor_dom, timeout, clickCallback, displayCallback) {
  dom = DAVE_HELP.process_dom(dom);

  if (!dom) {
    return;
  }

  djQ('#dave-help').show();
  action = action || 'click';
  djQ(dom).each(function () {
    djQ(this).one(action, function () {
      if (DAVE_HELP.enabled) {
        DAVE_HELP.help_for_element(anchor_dom || djQ(this), text, subtext, pos, timeout, clickCallback, displayCallback);
      } else {
        DAVE_HELP.bind_help_for_element(djQ(this), text, subtext, action, pos, anchor_dom, timeout, clickCallback, displayCallback);
      }
    });
  });
};
/**
 * ##### DAVE_HELP.bind_pop_for_element
 * Function binds the help text event to an element, but shows it everytime the event occurs.
 * @param {jQuery} dom   element to bind the action event to
 * @param {string} dom (jquery selector) element to bind the action event to
 * @param {string} text
 * @param {string} subtext
 * @param {string} action   The the jQuery event type on which to trigger the help text
 * @param {jQuery} anchor_dom  element relative to which the help text should occur, defaults to 'dom' if evaluates to false
 * @param {string} anchor_dom (jquery selector) element relative to which the help text should occur, defaults to 'dom'
 * @param {object} pos  {position: right/left/above/below, length: distance from dom, xaway: x distance from dom, yaway: y distance form dom}
 * @param {string} pos  right/left/above/below
 * @param {number} timout   time in milliseconds taken to disappear, not required, default can be set globally in DAVE_HELP.timeout
 * @param {function} clickCallback    function called if help-text is clicked, not required, 
 * @param {function} displayCallback    function called if help-text is displayed, not required, 
 *
 * e.g. DAVE_HELP.bind_pop_for_element("#MyId", "This is a help", "Click me", "click", "above", "#MyOtherElement", 5000, function() {console.log("This help text was clicked");}, function() {console.log("This help text was displayed")})
 */


DAVE_HELP.bind_pop_for_element = function (dom, text, subtext, action, pos, anchor_dom, timeout, clickCallback, displayCallback) {
  dom = DAVE_HELP.process_dom(dom);

  if (!dom) {
    return;
  }

  djQ('#dave-help').show();
  action = action || 'click';
  djQ(dom).each(function () {
    djQ(this).on(action, function () {
      if (DAVE_HELP.enabled) {
        DAVE_HELP.help_for_element(anchor_dom || djQ(this), text, subtext, pos, timeout, clickCallback, displayCallback);
      }
    });
  });
};
