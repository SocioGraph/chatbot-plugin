
function daveAvatar(aud, iOS, isSafari) {
    var scene = null;
    var talk = false;
    var lastTime = 0
    var shapesIndex = 0;
    var framesIndex = 0;
    var snd = aud || null;
    var IOSsnd = iOS || false;
    var shapes = null;
    var frames = null;
    var camera = null;
    var assetsManager = null;
    var headers = [];
    var volume = 1.0;
    var queue = [];

    this.loadPreScene = function (option, model_path, sceneLoadedCallback) {
        var canvas = document.getElementById(option);
        var engine = new BABYLON.Engine(canvas, true, {
            timeStep: (1.0/10.0),
            deterministicLockstep: true,
            lockstepMaxSteps: 30,
            limitDeviceRatio: 3,
        }, isSafari ? false : true);
        var createScene = function () {
            scene = new BABYLON.Scene(engine);
            scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);
            camera = new BABYLON.FreeCamera('camera_dave', new BABYLON.
                Vector3(0, 1.5, 2), scene);
            camera.setTarget(new BABYLON.Vector3(0, 1.5, 0));
            camera.viewport = new BABYLON.Viewport(0,-1, 1, 2.75);
            //camera.attachControl(canvas, false);
            assetsManager = new BABYLON.AssetsManager(scene);
            return scene;
        }
        var onetime = 1;
        scene = createScene();
        this.scene = scene
        this.camera = camera
        this.canvas = canvas
        this.engine = engine
        this.loadScene(model_path,sceneLoadedCallback)
        var that = this;
        engine.runRenderLoop(function () {
            if (talk) {
                let shape = shapes[shapesIndex];
                let x = (Date.now() - lastTime)/1000;
                if ( !shape ) {
                    talk = false;
                    shapesIndex = 0;
                    framesIndex = 0;
                    scene.render();
                    return;
                }
                while ( x >  shape["timestamp"] ) {
                    console.debug("Went ahead");
                    shapesIndex += 1;
                    shape = shapes[shapesIndex]
                    if ( !shape ) {
                        talk = false;
                        shapesIndex = 0;
                        framesIndex = 0;
                        scene.render();
                        return;
                    }
                }
                if ( x  >= shape["timestamp"]-(1.0/25.0)  && x <= shape["timestamp"] ) {
                    that.setMorphTargets(shape);
                    shapesIndex += 1;
                } else if ( x < shape["timestamp"]-(1.0/25.0) ) {
                    console.debug("Lagging behind");
                }
                frame = frames[framesIndex];
                if (frame != null && (x >= frame["timestamp"] ) ) {
                    that.playAnimation(frame);
                    framesIndex+=1;
                }
                if (shapesIndex >= shapes.length) {
                    talk = false;
                    shapesIndex = 0;
                    framesIndex = 0;
                }
            }
            scene.render();
        });
        this.playIdleAnimation();
    }

    this.loadScene = function (model_path, sceneLoadedCallback) {
        BABYLON.SceneLoader.ImportMesh("", "", model_path, scene, function () {
            scene.createDefaultCameraOrLight(false, false, false);
            scene.animationGroups.forEach(function (animationGroup) {
                animationGroup.stop();
            })
            sceneLoadedCallback();
        });
    }


    this.processData = function (allText) {
        var allTextLines = allText.split(/\r\n|\n/);
        headers = allTextLines[0].split(',');
        var lines = [];
        for (var i = 1; i < allTextLines.length; i++) {
            var data = allTextLines[i].split(',');
            if (data.length == headers.length) {

                var tarr = {};
                for (var j = 0; j < headers.length; j++) {
                    tarr[headers[j]] = parseFloat(data[j]);
                }
                lines.push(tarr);
            }
        }
        return lines;
    }

    this.processDataFrames = function (allText) {
        var allTextLines = allText.split(/\r\n|\n/);
        var headers = allTextLines[0].split(',');
        var lines = [];
        for (var i = 1; i < allTextLines.length; i++) {
            var data = allTextLines[i].split(',');
            if (data.length == headers.length) {

                var tarr = {};
                for (var j = 0; j < headers.length; j++) {
                    console.debug("got ani data = " + headers[j] + " = " + data[j])
                    if (headers[j] == 'timestamp') {
                        tarr[headers[j]] = parseFloat(data[j]);
                    } else {
                        tarr[headers[j]] = data[j];
                    }
                }
                lines.push(tarr);
            }
        }
        return lines;
    }

    this.setMorphTargets = function (shape) {
        for (var key in shape) {
            // if (key != "timestamp" && shape[key] != null) {
            //     s = scene.getMorphTargetByName(key)
            //     if (s !== null) {
            //         s.influence = shape[key];
            //     }
            // }
            this.setMorphTarget(key,shape[key])

        }
    }

    this.setMorphTarget=function(key,value){
        if (key != "timestamp" && value != null && value != undefined) {
            for (let l of scene.morphTargetManagers) {
                let s = l._targets.filter((_) => { return _.name == key });
                if (s.length) {
                    s = s[0];
                    if (s) {
                        s.influence = value;
                    }
                }
            }
        }
    }

    this.playAnimation = function (animationName) {
        if (animationName["animation"] != null) {
            console.log("trying to play = " + animationName["animation"]);
            ani = scene.getAnimationGroupByName(animationName["animation"]);
            if (ani) {
                try {
                    ani.play(false);
                    ani.loopAnimation = false;
                    ani.isAdditive = true;
                } catch (error) {
                    console.error(error)
                }
            }
        }
    }

    this.playIdleAnimation = function () {
        let ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/weight_shift/g) });
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/weight/g) });
        }
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/wieght/g) });
        }
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle/g) });
        }
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idel/g) });
        }
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/thinking_1/g) });
        }
        if (ani && ani.length > 0) {
            ani = ani[0];
            console.debug("trying to play idle animation" );

            try {
                ani.play(true);
                ani.loopAnimation = true;
                return true
            } catch (error) {
                console.log(error)
            }
        }
        return false;
    }

    this.getScene = function(){
        return scene;
    }
    this.pauseAnimsandMorphs = function(){
        talk = false
        that.stopMorphs()
        that.playIdleAnimation()
    }
    //send object here to get audio object back
    this.playAgain = function() {
        shapesIndex = 0;
        framesIndex = 0;
        talk = true;
        that.snd.currentTime = 0;
        lastTime = Date.now();
        that.snd.play();
    }
    let that = this;

    this.play = function (response, force) {
        if ( IOSsnd && that.snd && that.snd.src && that.snd.src == IOSsnd ) {
            force = true;
        }
        if ( that.snd && that.snd.src && !force ) {
            console.warn("Sound already playing");
            queue.push(response);
            return that.snd;
        }
        console.log("src-->",that.snd)
        
        shapes = that.processData(response["shapes"]);
        frames = that.processDataFrames(response["frames"]);
        if ( that.snd ) {
            if ( that.snd.src ) {
                that.snd.removeEventListener('pause', that.on_pause);
                that.on_pause(true);
            }
            that.snd.removeAttribute('src');
            that.snd.src = response["voice"];
            setTimeout(function() {
                if ( that.snd.paused ) {
                    that.on_canplay()
                }
            }, 500);
        } else {
            that.snd = new Audio(response["voice"]);
        }
        that.snd.loop = false;
        that.snd.addEventListener("canplay", that.on_canplay);
        that.snd.addEventListener("pause", that.on_pause);
        that.snd.addEventListener("ended", that.on_ended)
        return that.snd;
    }
    this.on_ended = function () {
        that.on_pause(true);
        if (queue.length > 0) {
            that.play(queue.shift());
        }
    }
    this.on_canplay = function() {
        shapesIndex = 0;
        framesIndex = 0;
        // that.snd.currentTime = 0;
        that.snd.volume = that.volume;
        lastTime = Date.now();
        talk = true;
        try {
            that.snd.play();
            DAVE_SETTINGS.execute_custom_callback('on_canplay_audio', [that.snd])
        } catch(err) {
            console.error(err);
            that.snd.src = null;
            DAVE_SETTINGS.execute_custom_callback('on_play_audio_error', [that.snd])
        }
    }
    this.on_pause = function(do_pause){
        if ( do_pause ) {
            that.snd.pause();
        }
        that.snd.removeEventListener('ended', that.on_ended);
        that.snd.removeEventListener('canplay', that.on_canplay);
        that.snd.removeEventListener('durationchange', that.on_canplay);
        that.snd.removeEventListener('pause', that.on_pause);
        that.pauseAnimsandMorphs();
        that.snd.removeAttribute('src');
        that.snd.volume = 0;
        // that.snd.currentTime = 0;
        that.snd.loop = false;
        if ( IOSsnd ) {
            that.snd.loop = true;
            that.snd.src = IOSsnd;
        }
        DAVE_SETTINGS.execute_custom_callback('on_pause_audio', [that.snd])
    }

    this.stopMorphs = function () {
        // if (headers != []) {
            for (i = 1; i < headers.length; i++) {
                // s = scene.getMorphTargetByName(headers[i])
                // if (s != null) {
                //     s.influence = 0.00000001;
                // }
                s=this.setMorphTarget(headers[i],0.0000001)

            }
        // }
    }
}
