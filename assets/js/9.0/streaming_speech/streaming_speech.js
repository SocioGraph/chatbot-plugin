class Utilities {
    static extendData(data, extend_object){
        extend_object = extend_object || {};
        data = data || {};
        let myPromise = new Promise(function(myResolve, myReject) {
            if(typeof extend_object == 'function'){
                var extend_data = extend_object(data);
                if(typeof extend_data == "object"){
                    myResolve(extend_data);
                }else{
                    myReject("Failed to extend object");
                }
            }else if(typeof extend_object == 'string'){
                var exd = {};
                try {
                    exd = JSON.parse(extend_object);
                    var d = {...data, ...exd}
                    myResolve(d);
                } catch (e) {
                    myReject(e);
                }
            }else{
                var d = {...data, ...extend_object}
                myResolve(d);
            }
        });
        return myPromise;
    }

    static makeid(length) {
        let result           = '';
        const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
    
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
    
        return result;
    }

    static getTime() {
        return (new Date()).getTime()/1000;
    }

    static _base64ToArrayBuffer(base64) {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }

    static encodeFlac(binData, recBuffers, isVerify, isUseOgg){
        var ui8_data = new Uint8Array(binData);
        var sample_rate=0,
            channels=0,
            bps=0,
            total_samples=0,
            block_align,
            position=0,
            recLength = 0,
            meta_data;

        function write_callback_fn(buffer, bytes, samples, current_frame){
            recBuffers.push(buffer);
            recLength += bytes;
            // recLength += buffer.byteLength;
        }

        function metadata_callback_fn(data){
            // console.info('meta data: ', data);
            meta_data = data;
        }


        var wav_parameters = wav_file_processing_read_parameters(ui8_data);	
        // convert the PCM-Data to the appropriate format for the libflac library methods (32-bit array of samples)
        // creates a new array (32-bit) and stores the 16-bit data of the wav-file as 32-bit data
        var buffer_i32 = wav_file_processing_convert_to32bitdata(ui8_data.buffer, wav_parameters.bps, wav_parameters.block_align);

        if(!buffer_i32){
            var msg = 'Unsupported WAV format';
            console.error(msg);
            return {error: msg, status: 1};
        }

        var tot_samples = 0;
        var compression_level = 5;
        var flac_ok = 1;
        var is_verify = isVerify;
        var is_write_ogg = isUseOgg;

        var flac_encoder = Flac.create_libflac_encoder(
            wav_parameters.sample_rate, 
            wav_parameters.channels, 
            wav_parameters.bps, 
            compression_level, 
            tot_samples, 
            is_verify
        );
        if (flac_encoder != 0){
            var init_status = Flac.init_encoder_stream(flac_encoder, write_callback_fn, metadata_callback_fn, is_write_ogg, 0);
            flac_ok &= init_status == 0;
            console.log("flac init: " + flac_ok);
        } else {
            Flac.FLAC__stream_encoder_delete(flac_encoder);
            var msg = 'Error initializing the decoder.';
            console.error(msg);
            return {error: msg, status: 1};
        }


        var isEndocdeInterleaved = true;
        var flac_return;
        if(isEndocdeInterleaved){		
            flac_return = Flac.FLAC__stream_encoder_process_interleaved(
                flac_encoder, 
                buffer_i32, buffer_i32.length / wav_parameters.channels
            );
        } else {	
            var ch = wav_parameters.channels;
            var len = buffer_i32.length;
            var channels = new Array(ch).fill(null).map(function(){ return new Uint32Array(len/ch)});
            for(var i=0; i < len; i+=ch){
                for(var j=0; j < ch; ++j){
                    channels[j][i/ch] = buffer_i32[i+j];
                }
            }

            flac_return = Flac.FLAC__stream_encoder_process(flac_encoder, channels, buffer_i32.length / wav_parameters.channels);
        }

        if (flac_return != true){
            console.error("Error: FLAC__stream_encoder_process_interleaved returned false. " + flac_return);
            flac_ok = Flac.FLAC__stream_encoder_get_state(flac_encoder);
            Flac.FLAC__stream_encoder_delete(flac_encoder);
            return {error: 'Encountered error while encoding.', status: flac_ok};
        }

        flac_ok &= Flac.FLAC__stream_encoder_finish(flac_encoder);

        Flac.FLAC__stream_encoder_delete(flac_encoder);

        return {metaData: meta_data, status: flac_ok};
    }


    static doFLAC(b64String) {
        console.log("Gonna return a promise that will do flac-ing..")
        let myPromise = new Promise(function(myResolve, myReject) {
            var fileInfo = [];

            var arrayBuffer = Utilities._base64ToArrayBuffer(b64String);
    
            var encData = [];
            var result = Utilities.encodeFlac(arrayBuffer, encData, isVerify(), isUseOgg());
            // console.log('encoded data array: ', encData);
            let metaData = result.metaData;
            
            // if(metaData){
                // console.log(metaData);
            // }
            
            var isOk = result.status;
            // console.log("processing finished with return code: ", isOk, (isOk == 1? " (OK)" : " (with problems)"));

            if(!result.error){
                // console.log("Encoded data : ");
                // console.log(encData, metaData);
                myResolve(encData);
                // forceDownload(blob, fileName);
            } else {
                
                myReject("Failed to encode", result.error);

            }
        });
        return myPromise;
    }

}

class Streamer {
    
    BLOB = undefined;
    AUDIO = undefined;
    startRecording = undefined;
    stopRecording  = undefined;
    recordAudio = undefined;
    recognition_sid = undefined;
    recgIntervalMap = undefined;
    is_recording = false;
    selectedRecognizer = undefined;
    socketio = undefined;
    socket = undefined;
    resultpreview = undefined;
    startAudioRecording = undefined;
    stopAudioRecording = undefined;
    audioContext = undefined;
    onSocketConnect = undefined;
    onSocketDisConnect = undefined;
    onTranscriptionAvailable = undefined;
    onError = undefined;
    onStreamingResultsAvailable = undefined;
    onConversationResponseAvailable = undefined;
    uid = undefined;
    
    
    constructor(data) {
        console.info("Setting up streamer.");
        this.activeStream = undefined; //[];
        
        this.asr_enabled = true
        this.wakeup_enabled = true;

        Object.assign(this, data);
        
        this.streamDestinationWSMap = {
            "asr" : undefined,
            "wakeup" : undefined
        };

        if (this.asr_only) {
            this.streamDestination = "asr";
        } else {
            this.streamDestination = "wakeup";
        }

        var that = this;

        navigator.permissions.query(
            // { name: 'camera' }
            { name: 'microphone' }
            // { name: 'geolocation' }
            // { name: 'notifications' } 
            // { name: 'midi', sysex: false }
            // { name: 'midi', sysex: true }
            // { name: 'push', userVisibleOnly: true }
            // { name: 'push' } // without userVisibleOnly isn't supported in chrome M45, yet
        ).then(function(permissionStatus){
        
            if (permissionStatus.state == "granted") { // granted, denied, prompt
                console.log("Mic access granted.");
                that.micAccess = true;

                that.initSocket();
                // if(that.vad == true) {
                //     that.setupVAD();
                // }
            } else {
                console.error("Mic access not granted by user "+permissionStatus.state);
            }
            
            permissionStatus.onchange = function(){
                console.log(this);
                console.log("Permission changed to " + this.state);
            }
        
        })
    }
    
    //VAD
    // Define function called by getUserMedia 
    startUserMedia(stream) {
        var that = this;
        console.log(this.activeStreams);

        // this.activeStreams.push(stream);
        // Create MediaStreamAudioSourceNode
        let source = this.audioContext.createMediaStreamSource(stream);
    
        // Setup options
            let options = {
            source: source,
            voice_stop: function() {
                console.log("auto-stopped-recording")
                if (that.recordAudio) {
                    that.stopAudioRecording(that.conv_params);
                    that.audioContext = undefined;
                    that.executeCallback("auto_stopped_recording")
                }
            }, 
            voice_start: function() {
                console.log('voice_start');
                // startVoiceRecording();
            }
        }; 

        // Create VAD
        let vad = new VAD(options);
    }

    setupVAD() {

        let that = this;
        console.log("Setting up VAD.");
        console.log(this.activeStreams);

        window.AudioContext =window.AudioContext || window.webkitAudioContext;
        this.audioContext = new AudioContext();

        navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
        
        if (!this.activeStream) {
            navigator.getUserMedia(
                {audio: true}, 
                function (stream) {
                    that.activeStream = stream;
                    that.startUserMedia(stream)
                },
                function(e) {
                    // console.log("No live audio input in this browser: " + e);
                });
        } else {
            this.startUserMedia(this.activeStream)
        }
    }

    cacheAndSendData(payload) {
        var that = this;
        console.log("Sending data to astream for "+this.streamDestination)
        Utilities.extendData(payload, this.asr_additional_data).then(
            function (data) {
                console.log(data);
                that.streamDestinationWSMap[that.streamDestination].emit("astream", data);
            },
            function () {
                that.executeCallback("stream_error", "extending payload failed");
                that.streamDestinationWSMap[that.streamDestination].emit("astream", payload);
            }
        )
    }

    changeStreamDestination(dest) {
        console.log("Changing Stream destination to "+dest);
        this.streamDestination = dest;
        
        if (dest == "asr") {
            this.stream_type = this.asr_type
        } else if(dest =="wakeup") {
            this.stream_type = this.wakeup_type            
        }

        //Implement image strem events here
    }

    executeCallback(event, data) {
        try {
            this.event_callback(event,data)
        } catch (e) {
            console.log("Executing callback failed.");
            console.error(e);
        }
        
    }

    initASREvents() {
        let that = this;
        this.streamDestinationWSMap.asr.on('connect', function(data) {
            that.executeCallback("wsconnect", "Websocket connection established.");
        });

     
        this.streamDestinationWSMap.asr.on('disconnect', function () {
            that.executeCallback("wsdisconnect", "Websocket connection is terminated.");
        });
        
        this.streamDestinationWSMap.asr.on('results', function (data) {
            // console.log(data);
            that.executeCallback(data);
        });
        
        this.streamDestinationWSMap.asr.on('intermediateResults', function(data) {
            console.log("ASR from speech server: ");
            console.log(data);
        
            if (data["is_final"] == true) {
                console.log("Final data is received. Stopping polling.");
                that.executeCallback("asr_results",data);
                if (data["final_text"] == "") {
                    console.log("Speech is none.");
                    that.executeCallback("speech_is_none", data);
                }
                // that.stopPolling(that.recgIntervalMap[that.recognition_sid]);
            } else {
                that.executeCallback("asr_intermediate_results",data);
            }            
        });
        
        this.streamDestinationWSMap.asr.on('error', function(data){
            console.log("backend error");
            if (that.onError && typeof(that.onError) == 'function' ) {
                that.executeCallback("asr_error", data);
            }
        });

        this.streamDestinationWSMap.asr.on('recText', function(data){
            // console.log("recText: "+data);
            that.executeCallback("asr_results", JSON.parse(data));
        });
        
        this.streamDestinationWSMap.asr.on('convResp', function(data){
            // console.log("convResp:");
            // console.log(data);
            try {
                that.executeCallback("conversation_response",JSON.parse(data));
            } catch (e) {
                console.error(e);
            }
            that.stopPolling(that.recgIntervalMap[that.recognition_sid]);
            that.recognition_sid = undefined;
        });
    }

    initWakeupEvents() {

        let that = this;

        this.streamDestinationWSMap.wakeup.on('error', function(data){
            console.log("backend error");
            if (that.onError && typeof(that.onError) == 'function' ) {
                that.executeCallback("wakeup_error", data);
            }
        });
        
        
        
        this.streamDestinationWSMap.wakeup.on('hotword', function(data) {
            console.log(data);
            that.detectWakeup = false;
            that.stopPolling(that.recgIntervalMap[that.recognition_sid]);
            that.executeCallback("hotword", data)
            // that.changeStreamDestination("asr");
            if (!that.auto_asr_detect_from_wakeup) {
                that.stopAudioRecording(this.conv_params, false)
            } else {
                that.stopAudioRecording(this.conv_params, false)
                that.start_asr()
            }
        });
    }

    initSocket() {
        var that = this;

        this.recgIntervalMap = {};

        // this.changeStreamDestination("wakeup");
        this.setupRTC();

        this.uid = Utilities.makeid(6);

        console.log("Setting up Socketio.");

        if (this.asr_only) {
            if (this.asr_server == null) {
                console.error("ASR url is null, Abort init.");
                return
            }
            this.streamDestinationWSMap.asr = io(this.asr_server, {query:"uid="+this.uid});
            this.initASREvents();
        } else {
            if (this.asr_server == null || this.wakeup_server ==  null) {
                console.error("ASR url || Wakeup url is null, Abort init.");
                return
            }
            this.streamDestinationWSMap.wakeup = io(this.wakeup_server, {query:"uid="+this.uid});
            this.initWakeupEvents();
            this.streamDestinationWSMap.asr = io(this.asr_server, {query:"uid="+this.uid});
            this.initASREvents();
        }
    }

    start_asr() {
        if (this.asr_only == false) {
            if (this.detectWakeup) {
                console.log("Wakeup is not detected yet.");
                return;
            }
        }
        this.changeStreamDestination('asr');
        this.startAudioRecording();
    }

    check_is_recording() {
        // console.log("record check "+this.is_recording);
        if (this.is_recording == true) {
            return true;
        } else {
            return false;
        }
    }
    //Polling functions.
    //This function polls wesocket server for intermediate results every 2 seconds.
    createPolling(uid, recognition_sid, interval = 2000) {
        var that = this;
        var poll_counter = 50;
        console.log("Creating polling (for "+that.streamDestination+")");

        const intervalObject = setInterval(function() {
            // console.log(that.check_is_recording());
            if (that.check_is_recording() == false) {
                poll_counter = poll_counter - 1;
                console.log("Polling ends after "+poll_counter+" trials.")
                if (poll_counter <= 0) {
                    that.stopPolling(that.recgIntervalMap[that.recognition_sid]);
                }
            }
            // console.log("This messsage gets printed every 2 seconds.");
            if (that.streamDestination == "wakeup") {
                console.log("Polling for wakeup.");
                that.streamDestinationWSMap["wakeup"].emit("hotwordResults",{"uid":uid,"recognition_sid":recognition_sid});
            } else {
                console.log("Polling for asr.");
                that.streamDestinationWSMap["asr"].emit("intermediateResults",{"uid":uid,"sid":recognition_sid});
            }
        }, interval);
    
        return intervalObject;
    }

    stopPolling(intervalObject) {
        console.log("Stopping polling..");
        clearInterval(intervalObject);
        this.recognition_sid = undefined;
    }

    // startRecording(customer_id, system_response) {

    // }

    replaceAudio(src) { 
        var newAudio = document.createElement('audio');
        // var newAudio = document.getElementById("playback");
        newAudio.controls = true; 
        newAudio.autoplay = true; 
        if(src) { 
            newAudio.src = src;
        }
        
        // var parentNode = newAudio.parentNode; 
        // newAudio.innerHTML = ''; 
        // newAudio.appendChild(newAudio); 
        AUDIO = newAudio; 
    }

    setupRTC() {
        var that = this;
        console.log("Setting up RTC.");
        // if (that.vad) {
        //     this.setupVAD();
        // }
        this.startAudioRecording = function(conv_params) {
            conv_params = conv_params || {}
            if(that.micAccess){

                console.log("Record")
                
                // this.startRecordingButton.style.display = 'block';
                // this.stopRecordingButton.style.display = 'none';  
    
                that.is_recording = true;
                navigator.getUserMedia(
                    {audio: true}, 
                    function(stream) {
                        console.log("Active Stream set.");
                        that.activeStream = stream;
                        that.recordAudio = RecordRTC(
                            stream, 
                            {
                                type: 'audio',
                                mimeType: 'audio/wav',
                                sampleRate: 44100,
                                timeSlice: 2000,
                                bufferSize : 1024,
                                recorderType: StereoAudioRecorder,
                                numberOfAudioChannels: 1,
                            
                                ondataavailable: function(blob) {
                                    
                                    // console.log(that.stream_type);
                                    if (!that.recognition_sid) {
                                        that.recognition_sid = Utilities.makeid(8);
                                        that.recgIntervalMap[that.recognition_sid] = that.createPolling(that.uid, that.recognition_sid);
                                    }

                                    if (that.stream_type == "full") {
                                        that.BLOB = blob;
                                    } else if (that.stream_type == "chunks") {

                                        
                                        // console.log("ASR CHUNKS");
                                        conv_params = {...conv_params, ...{
                                            "enterprise_id": that.enterprise_id,//DAVE_SETTINGS.getCookie("dave_authentication")["X-I2CE-ENTERPRISE-ID"],
                                            "conversation_id" : that.conversation_id,//DAVE_SETTINGS.CONVERSATION_ID,
                                            "size":blob.size,
                                            "blob":blob, 
                                            "recognition_sid":that.recognition_sid, 
                                            "is_recording":that.is_recording, 
                                            "recognizer": that.recognizer,//DAVE_SETTINGS.RECOGNIZER,
                                            "model_name" : that.model_name,
                                            "server" : that.base_url.split("//")[1],//DAVE_SETTINGS.BASE_URL.split("//")[1],
                                            "origin" : window.location.href,
                                            "timestamp": Utilities.getTime(),
                                        }};
                                        
                                        
                                        // console.log(payload);
                                        
                                        that.cacheAndSendData(conv_params);
                                        // this.socketio.emit('astream', audio_payload);
                                    }
                                }
                            }
                        );
                        
                        that.recordAudio.startRecording();
                        // this.stopRecordingButton.style.display = 'block';
                    }, 
                    function(error) {console.error(JSON.stringify(error));}
                );
            }
        };
    
        this.stopAudioRecording = function(conv_params={}, call_to_backend = true) {
            if(that.micAccess){
                console.log("Stopping stream to ASR.");
                // console.log("Conversation Params");
                // console.log(conv_params);
                // recording stopped
                // this.startRecordingButton.style.display = 'block';
                // this.stopRecordingButton.style.display = 'none';
                                
                that.is_recording = false;

                if (call_to_backend) {

                    conv_params = { ...conv_params, ...{
                        "enterprise_id": that.enterprise_id, //DAVE_SETTINGS.getCookie("dave_authentication")["X-I2CE-ENTERPRISE-ID"],
                        "conversation_id" : that.conversation_id, //DAVE_SETTINGS.CONVERSATION_ID,
                        "size":0,
                        "blob": "", 
                        "recognition_sid":that.recognition_sid, 
                        "is_recording":that.is_recording, 
                        "recognizer": "kaldi",//DAVE_SETTINGS.RECOGNIZER,
                        "model_name" : that.model_name,
                        "server" : that.base_url.split("//")[1],//DAVE_SETTINGS.BASE_URL.split("//")[1],
                        "origin" : window.location.href,
                        "timestamp": Utilities.getTime(),
                    }};
                    
                    if (that.stream_type == "chunks") {
                        that.streamDestinationWSMap[that.streamDestination].emit('astream', conv_params);
                    }
                }

                //// stop audio recorder
                that.recordAudio.stopRecording(function() {
                    console.log("Stopping recording.");
                    if (that.stream_type == "full") {

                        // replaceAudio(URL.createObjectURL(that.BLOB));
                        // AUDIO.play();
                        // after stopping the audio, get the audio data
                        that.recordAudio.getDataURL(function(audioDataURL) {
                            if (call_to_backend) {
                                if (that.recognizer == "google") {
                                    // console.log("Sending data for google recognition via flacking channel.");
                                    // that.onWavLoad(files.audio.dataURL.split(",")[1], selectedRecognizer);
                                    Utilities.doFLAC(audioDataURL.split(",")[1]).then(
                                        function (encData, metaData) {
                                            // console.log(encData);

                                            let blob = exportFlacFile(encData, metaData, false);
                                            // var fileName = getFileName("flactest", isUseOgg()? 'ogg' : 'flac');
                                            let reader = new FileReader();
                                            reader.onload = function() {
                                                
                                                let payload = { ...conv_params, ...{
                                                    "enterprise_id": that.enterprise_id, //DAVE_SETTINGS.getCookie("dave_authentication")["X-I2CE-ENTERPRISE-ID"],
                                                    "conversation_id" : that.conversation_id, //DAVE_SETTINGS.CONVERSATION_ID,
                                                    "size":0,
                                                    "blob": reader.result, 
                                                    "recognition_sid":that.recognition_sid, 
                                                    "is_recording":that.is_recording, 
                                                    "recognizer": "google",//DAVE_SETTINGS.RECOGNIZER,
                                                    "model_name" : that.model_name,
                                                    "server" : that.base_url.split("//")[1],//DAVE_SETTINGS.BASE_URL.split("//")[1],
                                                    "origin" : window.location.href,
                                                    "timestamp": Utilities.getTime()
                                                }};

                                                // console.log(payload);
                                                that.streamDestinationWSMap["asr"].emit("flacking", payload);
                                            }
                                            reader.readAsDataURL(blob);

                                        },
                                        function (messageString, error) {
                                            console.log("There is an error.");
                                            console.log(messageString);
                                            console.log(error);
                                            that.executeCallback("stream_error", "FLAC encoding failed");
                                        }
                                    )
                                } else {
                                    that.streamDestinationWSMap["asr"].emit('stt', payload);
                                }
                            }
                        });
                    }
                    that.killAudioTracks();

                    // that.stopPolling(that.recgIntervalMap[that.recognition_sid]);
                });
            }
        };
    }

    getMedia(requests, callback){
        var that = this;
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.mediaDevices.getUserMedia;
        
        if(navigator.getUserMedia){
            if(that._request_pending){
                that.executeCallback("interaction", InteractionStageEnum.MIC_REQUESTED);
            }
            navigator.getUserMedia(requests, function(stream) {
                console.debug("Cool:::Got auth for audio input");
                if(that._request_pending){
                    that.executeCallback("interaction", InteractionStageEnum.MIC_ALLOWED);
                    that._request_pending=false;
                }
                callback(stream);
            }, function(error) {
                console.log("error while mic init");
                console.log(error);
                if(that._request_pending){
                    that.executeCallback("interaction", InteractionStageEnum.MIC_REJECTED);
                    that._request_pending=false;
                }
                callback(false, error);
                that.mediaFailed();
            });
        }else{
            callback(false);
        }
    }

    mediaPermissions(){
        //class Streamer
        var that = this;
        function callbacks(sts){
            that._request_pending = false;
            if(sts){
                that.executeCallback("interaction", InteractionStageEnum.MIC_ALLOWED);
            }else{
                that.executeCallback("interaction", InteractionStageEnum.MIC_REJECTED);
                that.mediaFailed();
            }
        }
        // this.getMedia({audio: true}, callbacks);
        
        if(navigator.permissions){
            navigator.permissions.query({name:'microphone'}).then(function(result) {
                console.debug(result.state)
                if(result.state == "denied"){
                    callbacks(false);
                }else if(result.state == "granted") {
                    callbacks(true);
                }
               });
        }
    }

    killAudioTracks() {
        console.log(this.activeStream.getTracks());
        let tracks = this.activeStream.getTracks();
        tracks.forEach(track => {
            console.log("Killing track.");
            track.stop();
        });
        this.recordAudio = undefined;
        this.activeStream = undefined;
    }

    cancelRecordStopTimer(id) {
        clearTimeout(id);
    }   

    startRecordStopTimer(T) {
        this.recordTimer = setTimeout(function() {
            this.stopVoiceRecording();
        }, T * 1000);
    }
       
    startVoiceRecording(conv_params){
        //if any audio/video stream is ON just kill them
        if (this.vad && !this.vadEnabled) {
            this.setupVAD();
            this.vadEnabled = true;
        }
        this.conv_params = conv_params;
        if(!this.micAccess){
            return;
        }
        
        this.changeStreamDestination("asr");
        this.startAudioRecording(conv_params)
        console.log(this.audio_auto_stop);
        // this.startRecordStopTimer(this.audio_auto_stop);
    }
    stopVoiceRecording(){
        this.stopAudioRecording(this.conv_params)
    }
    startWakeupRecording(conv_params){
        //if any audio/video stream is ON just kill them
        //same as startAsrRecording.
        this.conv_params = conv_params;
        if(!this.micAccess){
            return;
        }
        
        this.changeStreamDestination("wakeup");
        this.startAudioRecording(conv_params)
    }
    stopWakeupRecording(){
        this.stopAudioRecording(this.conv_params)
    }

    startVideoRecording(){

    }
    stopVideoRecording(){}
} 
