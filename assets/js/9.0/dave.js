var DAVE_SETTINGS = {
    BASE_URL: "https://staging.iamdave.ai", //test.iamdave.ai
    ENVIRONMENT_IDENTIFIER: null,
    SIGNUP_API_KEY: null,
    ENTERPRISE_ID: null,
    CONVERSATION_ID: null,
    SPEECH_SERVER: null,
    SPEECH_RECOGNIZER: 'google',
    SPEECH_MODEL_NAME: 'indian_english',
    USER_MODEL: 'person',
    USER_ID_ATTR: 'user_id',
    USER_EMAIL_ATTR: 'email',
    USER_PHONE_NUMBER_ATTR: 'mobile_number',
    LOGIN_ATTRS: null,
    DEFAULT_SYSTEM_RESPONSE: null,
    DEFAULT_SYSTEM_RESPONSES: {},
    DEFAULT_USER_DATA: {
        "person_type": "visitor"
    },
    SESSION_MODEL: 'person_session',
    SESSION_ID: 'session_id',
    SESSION_USER_ID: 'user_id',
    DEFAULT_SESSION_DATA: {
        'location_dict': '{agent_info.ip}',
        'browser': '{agent_info.browser}',
        'os': '{agent_info.os}',
        'device_type': '{agent_info.device}'
    },
    LANGUAGE_CONVERSATION_MAP: {},
    INTERACTION_MODEL: 'interaction',
    INTERACTION_USER_ID: 'person_id',
    INTERACTION_SESSION_ID: 'session_id',
    INTERACTION_STAGE_ATTR: 'stage',
    DEFAULT_INTERACTION_DATA: {
    },
    LANGUAGE: null,
    VOICE_ID: null,
    MAX_SPEECH_DURATION: 10000,
    SESSION_ORIGIN: null,
    INTERACTION_ORIGIN: null,
    AVATAR_ID: null,
    AVATAR_MODEL: "avatar",
    LANGUAGE_AVATAR_MAP: {},
    GLB_ATTR: "glb_url",
    EVENT_MODEL: null,
    EVENT_SESSION_ID: 'session_id',
    EVENT_USER_ID: 'user_id',
    EVENT_DESCRIPTION: 'event',
    EVENT_WAIT_TIME: 'wait_time',
    EVENT_ORIGIN: 'origin',
    DEFAULT_EVENT_DATA: null,
    EVENT_TIMER: null,
    GOOGLE_MAP_KEY: null,
    RESPONSE_MODEL: null,
    RESPONSE_GLB_ATTR: "glb_url",
    RESPONSE_ATTR: "response",
    RESPONSE_PLAY_CREDITS_ATTR: "available_play_credits",
    RESPONSE_TOTAL_PLAYS_ATTR: "total_plays",
    MANUAL_LOAD: false,
    PRODUCTION: false,
    UNAUTHORIZED_FUNCTION: false
}
if (typeof (djq) == 'undefined') {
    djq = djQ;
}

if (document.currentScript && isFinite(document.currentScript.src.split('/').slice(-2, -1)[0])) {
    DAVE_SETTINGS.asset_path = (document.currentScript && (document.currentScript.src.split('/').slice(0, -4).join('/') + '/'));
} else {
    DAVE_SETTINGS.asset_path = (document.currentScript && (document.currentScript.src.split('/').slice(0, -3).join('/') + '/')) || "https://chatbot-plugin.iamdave.ai/";
}

let DAVE_SCENE = {};
(function () {
    let platform = navigator ? navigator.userAgent || navigator.platform || 'unknown' : 'unknown'
    DAVE_SETTINGS.isIOS = /iPhone|iPod|iPad|MacIntel|Macintosh/.test(platform)
    DAVE_SETTINGS.isIE = /MSIE/i.test(platform)
    DAVE_SETTINGS.isSafari = /^((?!chrome|android).)*safari/i.test(platform);
})();

if (typeof daveAvatar !== 'undefined') {
    let snd = new Audio(DAVE_SETTINGS.asset_path + "assets/audio/empty.wav");
    DAVE_SCENE = new daveAvatar(DAVE_SCENE.snd, DAVE_SETTINGS.isIOS ? DAVE_SETTINGS.asset_path + "assets/audio/empty.wav" : null, DAVE_SETTINGS.isSafari);
    DAVE_SCENE.snd = snd;
    DAVE_SCENE.snd.volume = 0.0;
    if (DAVE_SETTINGS.isIOS) {
        DAVE_SCENE.snd.loop = true;
    } else {
        DAVE_SCENE.snd.removeAttribute('src');
    }
}


var DAVE_HELP = null;
DAVE_SETTINGS.signup = function signup(data, callbackFunc, errorFunc, force) {
    if (DAVE_SETTINGS.getCookie("dave_started_signup") || DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("started_signup") || DAVE_SETTINGS.getCookie("authentication")) {
        console.warn("Another dave signup going on, so exiting");
        return
    }
    if (!force && DAVE_SETTINGS.custom_signup && typeof (DAVE_SETTINGS.custom_signup) == 'function') {
        DAVE_SETTINGS.custom_signup(data, callbackFunc, errorFunc);
        return
    }
    DAVE_SETTINGS.execute_custom_callback('before_customer_signup', [data]);
    DAVE_SETTINGS.clearCookies();
    DAVE_SETTINGS.setCookie("dave_started_signup", true)
    var signupurl = DAVE_SETTINGS.BASE_URL + "/customer-signup/" + DAVE_SETTINGS.USER_MODEL;
    //Password string generator
    var randomstring = Math.random().toString(36).slice(-8);
    data = data || {}
    if (!data.email) {
        data.email = "ananth+" + randomstring + "@iamdave.ai"
    }
    for (let k in DAVE_SETTINGS.DEFAULT_USER_DATA) {
        if (!data[k]) {
            data[k] = DAVE_SETTINGS.DEFAULT_USER_DATA[k]
        }
    }
    function on_success(data) {
        HEADERS = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
            "X-I2CE-USER-ID": data[DAVE_SETTINGS.USER_ID_ATTR],
            "X-I2CE-API-KEY": data.api_key
        }
        DAVE_SETTINGS.setCookie("dave_authentication", JSON.stringify(HEADERS), 24);
        DAVE_SETTINGS.setCookie("dave_user_id", data[DAVE_SETTINGS.USER_ID_ATTR]);
        DAVE_SETTINGS.setCookie("dave_conversation_id", DAVE_SETTINGS.CONVERSATION_ID)
        DAVE_SETTINGS.clearCookie("dave_started_signup");
        DAVE_SETTINGS.clearCookie("started_signup");
        DAVE_SETTINGS.clearCookie("authentication");
        DAVE_SETTINGS.clearCookie("user_id");
        DAVE_SETTINGS.clearCookie("conversation_id");
        DAVE_SETTINGS.clearCookie("engagement_id");
        DAVE_SETTINGS.clearCookie("dave_history");
        DAVE_SETTINGS.initialize_session();
        if (callbackFunc && typeof (callbackFunc) == 'function') {
            callbackFunc(data);
        }
        DAVE_SETTINGS.predict();
    }
    djQ.ajax({
        url: signupurl,
        method: "POST",
        dataType: "json",
        contentType: "json",
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
            "X-I2CE-SIGNUP-API-KEY": DAVE_SETTINGS.ENVIRONMENT_IDENTIFIER
        },
        data: JSON.stringify(data),
        success: function (data1) {
            on_success(data1);
        },
        error: function (r, e) {
            console.error(e);
            DAVE_SETTINGS.clearCookie("started_signup");
            DAVE_SETTINGS.clearCookie("dave_started_signup");
            if (errorFunc && typeof (callbackFunc) == 'function') {
                errorFunc(r, e);
            }
        }
    });
}

// Login function
DAVE_SETTINGS.login = function login(user_name, password, callbackFunc, errorFunc, unauthorizedFunc, attrs) {
    let params = {};
    params["user_id"] = user_name;
    params["password"] = password;
    params["roles"] = DAVE_SETTINGS.USER_MODEL;
    params["attrs"] = attrs || DAVE_SETTINGS.LOGIN_ATTRS || [DAVE_SETTINGS.USER_ID_ATTR, DAVE_SETTINGS.USER_EMAIL_ATTR, DAVE_SETTINGS.USER_PHONE_NUMBER_ATTR];
    params["enterprise_id"] = DAVE_SETTINGS.ENTERPRISE_ID;
    DAVE_SETTINGS.ajaxRequestWithData(
        "/dave/oauth",
        "POST",
        JSON.stringify(params),
        function (data) {
            let HEADERS = {
                "Content-Type": "application/json",
                "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
                "X-I2CE-USER-ID": data.user_id,
                "X-I2CE-API-KEY": data.api_key,
            };
            DAVE_SETTINGS.setCookie("dave_authentication", JSON.stringify(HEADERS), 24);
            DAVE_SETTINGS.setCookie("dave_user_id", data.user_id);
            DAVE_SETTINGS.setCookie("dave_conversation_id", DAVE_SETTINGS.CONVERSATION_ID)
            DAVE_SETTINGS.clearCookie("dave_started_signup");
            DAVE_SETTINGS.clearCookie("dave_engagement_id");
            DAVE_SETTINGS.setCookie("dave_logged_in", true);
            DAVE_SETTINGS.clearCookie("dave_history");
            DAVE_SETTINGS.initialize_session();
            if (callbackFunc && typeof (callbackFunc) == 'function') {
                callbackFunc(data);
            }
            DAVE_SETTINGS.predict();
            DAVE_SETTINGS.history(null, null, null, null, true);
        },
        errorFunc,
        unauthorizedFunc
    );
}

// You can get the column names of any model with the following API
// DAVE_SETTINGS.ajaxRequestWithData("/attributes/<model_name>/name", "GET", 
//
//
DAVE_SETTINGS.patch_user = function patch_user(params, callbackFunc, errorFunc, repeats) {
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.patch_user, [params, callbackFunc, errorFunc, repeats || 0], "dave_user_id")) {
        return;
    }
    // e.g. patch_user({"pincode": <pincode>, "name": <name of user>, "company_name": <company_name>}, function(data) {console.log(data)});
    // In the response you will get the warehouse_id
    let user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
    params = params || {};
    if (params.person_name && params.email && params.mobile_number) {
        params["signed_up"] = true;
    }
    DAVE_SETTINGS.ajaxRequestWithData("/update/" + DAVE_SETTINGS.USER_MODEL + "/" + user_id, "POST", JSON.stringify(params), callbackFunc, errorFunc);
}

DAVE_SETTINGS.create_session = function create_session(params, callbackFunc, errorFunc, repeats) {
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.create_session, [params, callbackFunc, errorFunc, repeats || 0], "dave_user_id")) {
        return;
    }
    let user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
    params[DAVE_SETTINGS.SESSION_USER_ID] = user_id;
    for (let k in DAVE_SETTINGS.DEFAULT_SESSION_DATA) {
        params[k] = DAVE_SETTINGS.DEFAULT_SESSION_DATA[k]
    }
    DAVE_SETTINGS.get_url_params(params)
    if (DAVE_SETTINGS.SESSION_ORIGIN) {
        params[DAVE_SETTINGS.SESSION_ORIGIN] = window.location.href.split(/[?#]/)[0];
    }
    DAVE_SETTINGS.execute_custom_callback("before_create_session", [params]);
    if (typeof (crypto) !== undefined && !params[DAVE_SETTINGS.SESSION_ID] && typeof (crypto.randomUUID) == 'function') {
        params[DAVE_SETTINGS.SESSION_ID] = crypto.randomUUID();
    }
    function on_success(data) {
        DAVE_SETTINGS.setCookie("dave_session_id", data[DAVE_SETTINGS.SESSION_ID]);
        DAVE_SETTINGS.execute_custom_callback("after_create_session", [data, params]);
        if (callbackFunc && typeof (callbackFunc)) {
            callbackFunc(data);
        }
    }
    if (params[DAVE_SETTINGS.SESSION_ID]) {
        params['_async'] = true;
        on_success(params);
    }
    DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.SESSION_MODEL, "POST", JSON.stringify(params), function (data) {
        if (!params['_async']) {
            on_success(data);
        }
    }, errorFunc);
}
//
//
//
/* API of the Data response for chat
{
    "name": "sr_greeting",
    "title": "Sr Greeting",
    "whiteboard": null,
    "customer_state": "cs_hi",
    "placeholder_aliases": {},
    "state_options": {     // These are the quick access buttons
        "cs_xpress_cash_loan_repayment": "Method of repayment Personal Loan",
        "cs_car_loan_minimum_salary": "Car Loan minimum salaried individual",
        "cs_xpress_home_loan_tenure": "Home Loan tenure",
        "cs_car_loan_eligibility": "Car Loan eligibility",
        "cs_term_deposit_schemes": "Term deposit schemes",
        "cs_xpress_msme_loan_security": "Security for MSME Loan",
        "cs_xpress_home_loan_interest_rate": "Home Loan interest rate",
        "cs_personal_loan_features": "Personal Loan features"
    },
    "to_state_function": {
        "function": "_function_find_customer_state"
    },
    "placeholder": "Hello, hope you're having a good day!",     // This will go into the speech bubble, NOTE: This could be HTML, so it should be HTML safe
    "whiteboard_title": "Sr Greeting",
    "wait": 10000,    //// Time in millisecs to wait for the followup
    "data": {
         "_follow_ups": [  // nudges
            "nu_follow_up_state_1",
            "nu_follow_up_state_2"
         ],
         "response_type": "template",   // Other options are "video", "options", "thumbnails", "url", "form", "template"
         "template": <name of template (default is none)> ,
         "car_list" :[]
         "title"; <title to give the image, video or url or template>
         "sub_title"; <sub_title to give to the template, image or video>
         "description": "this description can be used in the template",
         "image": <url to the image>, //optional
         "video": <url to the video>,
         "video_start_timestamp": HH:MM:SS
         "video_stop_timestamp": HH:MM:SS
         "reply_to_id": <id of the response to which we want to provide the data>,
         "thumbnails": [
          {
             "image": <url to image>,
             "url": <url to redirect to if clicked>,
             "title": <title of the image>,
             "sub_title": <sub title of the image>,
             "description": <description of the image>,
             "target": <open in same window or another window (self or _blank)"
          }, ....
         ],
         "slideshow": [
          {
             "image": <url to image>,
             "title": <title of the image>,
             "sub_title": <sub title of the image>,
             "description": <description of the image>,
             "url": <url to redirect to if clicked>,
             "target": <open in same window or another window (self or _blank)"
          }, ....
         ],
         "url": <url to redirect to in case of clicking on the image or video>,
         "target": <target of the url, default "_blank" other option is self>,
         "form": [
             {
                "name": <name of field to send in json>,
                "title": <title of field, defaults to name>,
                "placeholder": <placeholder of field>,
                "ui_element": <text, textarea, number, switch, datetime-local, mobile_number, email, select, multiselect, tags, multiselect-tags>
                "error": <error message if required, can be null or not available>,
                "min": <min value in case of ui element to be number or date>,
                "max": <min value in case of ui element to be number or date>,
                "step": <resolution in seconds in case of datetime>,
                "required": true/ or false,
             }, ....
         ],    // The data collected from the form will be sent as json string in customer_response
         "customer_response": <custom title for the submit button>
         "options": {
             <customer_state>: <customer_response>,
             <customer_state>: <customer_response>
         },
         "options": [
            <customer_response option>,
            <customer_response option>
         ],
         "options": [
          {"customer_state": <customer_response>}
          {"customer_state": <customer_response>}
          {"customer_state": <customer_response>}
         ],
         "customer_state": <customer state to send in case there are options to select from>,
    },
    "options": null,
    "engagement_id": "YXNsa2RmamFsc2tqZGZkZXBsb3ltZW50IGtpb3NrIGthbm5hZGE_",
    "response_id": <response_id>
}*/
//
// params will contain one or both of
// "customer_state", and/or "customer_response"
// If user clicks on the Quick Access buttons, then both will be sent
// If there are options, then the selected option will be sent along with the customer_state sent in data
// If there is any text input then only customer_response will be sent.
DAVE_SETTINGS.on_having_response = function on_having_response(data, params, callbackFunc, unknownFunc, skip_history) {
    DAVE_SETTINGS.setCookie("dave_latest_response_id", data.response_id || DAVE_SETTINGS.generate_random_string(8));
    DAVE_SETTINGS.setCookie("dave_system_response", data.name);
    console.debug("Response Id: " + data.response_id);
    if (data.console && !DAVE_SETTINGS.PRODUCTION) {
        for (let k of data.console.split('\n')) {
            if (k.indexOf('DEBUG') >= 0) {
                console.debug(k);
            } else if (k.indexOf('WARN') >= 0) {
                console.warn(k);
            } else if (k.indexOf('ERROR') >= 0) {
                console.error(k);
            } else {
                console.log(k);
            }
        }
    }
    if (DAVE_SCENE && DAVE_SCENE.play && DAVE_SCENE.loaded && DAVE_SCENE.playable && DAVE_SCENE.opened && data.response_channels && data.response_channels.voice && data.response_channels.frames && data.response_channels.shapes) {
        DAVE_SCENE.audio_text = data.placeholder;
        DAVE_SCENE.play(data.response_channels, params.query_type != 'auto' || DAVE_SETTINGS.isIOS);
    } else if (DAVE_SCENE && DAVE_SCENE.play && data.response_channels && data.response_channels.voice && data.response_channels.frames && data.response_channels.shapes) {
        DAVE_SCENE.audio_text = data.placeholder;
        DAVE_SCENE.queued_audio = data.response_channels;
    }
    if (data.data && data.data._follow_ups) {
        DAVE_SETTINGS.setup_dave_nudges(data.data._follow_ups, data.wait || 20000)
    } else {
        DAVE_SETTINGS.setup_dave_nudges(null, data.wait || 20000)
    }
    if (!skip_history) {
        let history = DAVE_SETTINGS.getCookie("dave_history") || { 'history': [] }
        let customer_state = params['customer_state'] || data['customer_state'] || "__init__"
        if (history['history'].length > 40) {
            history['history'].shift();
        }
        if (customer_state != '__init__' && !data.hide_in_customer_history) {
            history["history"].push({
                "direction": "user",
                "customer_response": params["customer_response"] || DAVE_SETTINGS.toTitleCase(params["customer_state"] || ''),
                "customer_state": customer_state,
                "user_id": DAVE_SETTINGS.getCookie("dave_user_id"),
                "user_model": DAVE_SETTINGS.USER_MODEL,
                "timestamp": data['start_timestamp'] || new Date(),
                "response_id": data["response_id"] || DAVE_SETTINGS.generate_random_string(8),
            });
        }
        if (data.show_in_history) {
            history["history"].push({
                "direction": "system",
                "name": data["name"],
                "placeholder": data['placeholder'],
                "data": data["data"] || {},
                "title": data["title"],
                "options": data["options"],
                "state_options": data["state_options"],
                "whiteboard": data["whiteboard"],
                "placeholder_aliases": data["placeholder_aliases"],
                "timestamp": data['timestamp'],
                "response_id": data["response_id"],
            });
        }
        let set_history = function () {
            DAVE_SETTINGS.setCookie("dave_history", history, 24, function () {
                if (history['history'].length > 0) {
                    history['history'].shift();
                    set_history();
                } else {
                    alert("Application is out of memory! Please close a few tabs and try again!");
                }
            });
        }
        set_history();
    }
    DAVE_SETTINGS.execute_custom_callback('on_response', [
        data.name || "received chat response", data.customer_state, data, params, callbackFunc, unknownFunc
    ]);
    if (data.customer_state == 'unknown_customer_state' && unknownFunc && typeof (unknownFunc) == 'function') {
        unknownFunc(data);
        return
    }
    if (callbackFunc && typeof (callbackFunc) == 'function') {
        callbackFunc(data);
    }
}

DAVE_SETTINGS.setup_dave_nudges = function setup_dave_nudges(followupList, wait) {
    if (followupList && DAVE_SETTINGS.dave_nudgeTrigger) {
        clearTimeout(DAVE_SETTINGS.dave_nudgeTrigger);
        DAVE_SETTINGS.dave_nudges = [];
    }
    if (followupList) {
    }
}
DAVE_SETTINGS.chat = function chat(params, callbackFunc, errorFunc, unknownFunc, discard_local, repeats) {
    repeats = repeats || 0;
    DAVE_SETTINGS.execute_custom_callback('on_chat_start', [
        params.customer_state || params.customer_response || "sent chat message", params, callbackFunc, errorFunc, unknownFunc
    ]);
    if (DAVE_SETTINGS.DEFAULT_SYSTEM_RESPONSE && !params.customer_state && !params.customer_response && !discard_local) {
        DAVE_SETTINGS.DEFAULT_SYSTEM_RESPONSE.response_id = DAVE_SETTINGS.generate_random_string(8);
        DAVE_SETTINGS.on_having_response(DAVE_SETTINGS.DEFAULT_SYSTEM_RESPONSE, params, callbackFunc, unknownFunc, true);
        DAVE_SETTINGS.chat(params, null, null, null, true);
        return
    }
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.chat, [params, callbackFunc, errorFunc, discard_local, repeats || 0], "dave_user_id")) {
        return;
    }
    let user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
    let conversation_id = params['conversation_id'] || DAVE_SETTINGS.CONVERSATION_ID || DAVE_SETTINGS.getCookie('dave_conversation_id') || DAVE_SETTINGS.getCookie('conversation_id');
    delete params['conversation_id'];
    let engagement_id = params['engagement_id'] || DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id');
    if (engagement_id) {
        params["engagement_id"] = engagement_id
    }
    let system_response = params['system_response'] || DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response');
    if (system_response && (params['customer_state'] || params['customer_response'])) {
        params["system_response"] = system_response
    } else {
        delete params["system_response"];
    }
    if (DAVE_SETTINGS.VOICE_ID && !params['voice_id']) {
        params["voice_id"] = DAVE_SETTINGS.VOICE_ID
    }
    if (DAVE_SETTINGS.LANGUAGE && !params['language']) {
        params["language"] = DAVE_SETTINGS.LANGUAGE
    }
    if (DAVE_SETTINGS.AVATAR_ID || params['avatar_id']) {
        params["synthesize_now"] = true;
        params["csv_response"] = true;
    }
    params["origin"] = window.location.href;
    if (params['query_type'] && params['query_type'] == 'auto') {
        DAVE_SETTINGS.create_event("auto");
    } else {
        DAVE_SETTINGS.create_event("queried");
    }
    if (params.customer_state || params.customer_response) {
        DAVE_SETTINGS.did_interact = true;
    }
    return DAVE_SETTINGS.ajaxRequestWithData("/conversation/" + conversation_id + "/" + user_id, "POST", params, function (data) {
        DAVE_SETTINGS.setCookie("dave_engagement_id", data.engagement_id);
        DAVE_SETTINGS.on_having_response(data, params, callbackFunc, unknownFunc);
    }, errorFunc);
}

DAVE_SETTINGS.set_conversation = function set_conversation(conversation_id, callbackFunc, errorFuc) {
    if (conversation_id == DAVE_SETTINGS.CONVERSATION_ID) {
        return
    }
    DAVE_SETTINGS.CONVERSATION_ID = conversation_id;
    DAVE_SETTINGS.setCookie("dave_conversation_id", conversation_id);
    DAVE_SETTINGS.clearCookie('dave_keywords');
    DAVE_SETTINGS.clearCookie('dave_keyword_time');
    DAVE_SETTINGS.clearCookie('dave_engagement_id');
    DAVE_SETTINGS.chat({}, callbackFunc, errorFunc);
}

// get history function
// e.g. response data
//
// Check direction attribute to check if user or system
// Use placeholder if system and customer_response if user
//{
//    "history": [
//        {
//            "data": {},
//            "direction": "system",
//            "options": null,
//            "placeholder": "sr_init",
//            "placeholder_aliases": {},
//            "timestamp": "Sat, 09 Jan 2021 16:10:55 GMT",
//            "title": "Sr Agriculturists",
//            "whiteboard": null
//        },
//        {
//            "customer_response": "Response 1",
//            "customer_state": {
//                "unknown_customer_state": []
//            },
//            "direction": "user",
//            "timestamp": "Sat, 09 Jan 2021 16:11:57 GMT",
//            "user_id": "ananth+kabank@i2ce.in",
//            "user_model": "admin"
//        },
//        {
//            "data": {},
//            "direction": "system",
//            "options": null,
//            "placeholder": "I'm not sure what you mean. Can you rephrase that?",
//            "placeholder_aliases": {},
//            "timestamp": "Sat, 09 Jan 2021 16:11:57 GMT",
//            "title": "Sr Agriculturists",
//            "whiteboard": null
//        },
//        {
//            "customer_response": "Repsonse 2",
//            "customer_state": {
//                "unknown_customer_state": []
//            },
//            "direction": "user",
//            "timestamp": "Sat, 09 Jan 2021 16:13:40 GMT",
//            "user_id": "ananth+kabank@i2ce.in",
//            "user_model": "admin"
//        },
//        ....
//    ]
//}
DAVE_SETTINGS.history = function history(callbackFunc, errorFunc, reconcileFunction, params, discard_local, repeats) {
    repeats = repeats || 0;
    params = params || {};
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.history, [callbackFunc, errorFunc, reconcileFunction, params, discard_local, repeats || 0], "dave_user_id")) {
        return;
    }
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.history, [callbackFunc, errorFunc, reconcileFunction, params, discard_local, repeats || 0], "dave_engagement_id")) {
        return;
    }
    if (DAVE_SETTINGS.getCookie("dave_history") && !discard_local && (!params._page_number || params._page_number == 1)) {
        DAVE_SETTINGS.execute_custom_callback('on_load_history', [DAVE_SETTINGS.getCookie("dave_history")]);
        if (callbackFunc && typeof (callbackFunc) == "function") {
            callbackFunc(DAVE_SETTINGS.getCookie("dave_history"))
        }
        return DAVE_SETTINGS.history(null, null, reconcileFunction || callbackFunc, params, true);
    }
    let user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
    let engagement_id = DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id');
    DAVE_SETTINGS.ajaxRequestWithData(
        "/conversation-history/" + DAVE_SETTINGS.CONVERSATION_ID + "/" + user_id + "/" + engagement_id,
        "GET",
        params,
        function (data) {
            let history = DAVE_SETTINGS.getCookie("dave_history") || { "history": [] }
            nh = data['history'].filter(function (h) {
                return !history['history'].some(function (i) {
                    if (h.response_id == i.response_id) {
                        return true;
                    } else {
                        return false
                    }
                });
            });
            DAVE_SETTINGS.setCookie("dave_history", data);
            if (callbackFunc) {
                DAVE_SETTINGS.execute_custom_callback('on_load_history', [data]);
            }
            callbackFunc = callbackFunc || reconcileFunction;
            if (callbackFunc && typeof (callbackFunc) == "function") {
                callbackFunc({ 'history': nh });
            }
        },
        errorFunc
    );
}
// DAVE_SETTINGS.feedback({"usefulness_rating": <rating>, "accuracy_rating": <rating>, "feedback": <feedback>}, null, function(){ console.log("Callback") }) to give accuracy rating for entire conversation
// DAVE_SETTINGS.feedback({"response_rating": <rating (true or false)>}, <response_id>, function(){ console.log("Callback") } ) to give rating for specific response
// DAVE_SETTINGS.feedback({"accuracy_rating": <rating (true or false)>}, <response_id>, function(){ console.log("Callback") } ) to give rating for recognition result
DAVE_SETTINGS.feedback = function send_feedback(rating, response_id, callbackFunc, errorFunc, repeats) {
    if (
        !DAVE_SETTINGS.signup_guard(
            DAVE_SETTINGS.feedback,
            [rating, response_id, callbackFunc, errorFunc, repeats || 0],
            "dave_engagement_id"
        )
    ) {
        return;
    }
    let engagement_id = DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id');
    for (let k in rating) {
        if (typeof rating[k] == "boolean") {
            rating[k] = rating[k] ? 5 : 1
        }
    }
    DAVE_SETTINGS.ajaxRequestWithData(
        "/conversation-feedback/dave/" + engagement_id + (response_id ? "/" + response_id : ""),
        "POST",
        JSON.stringify(rating),
        function (data) {
            DAVE_SETTINGS.execute_custom_callback("after_submit_feedback", [rating, response_id])
            if (callbackFunc && typeof (callbackFunc) == "function") {
                callbackFunc(data)
            }
        },
        errorFunc
    )
}

// DAVE_SETTINGS.predict(<partial keywords>, function(response) { console.log(response)})
// response of type: [{sentence match: customer_state}, {sentence_match: customer_state}] 
DAVE_SETTINGS.predict = function predict(typed, callbackFunc, errorFunc, conversation_id, repeats) {
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.predict, [typed, callbackFunc, errorFunc, conversation_id, repeats || 0], "dave_user_id")) {
        return;
    }
    let previous_predict = parseInt(DAVE_SETTINGS.getCookie("dave_keyword_predict_time") || 0)
    let now = djQ.now()
    if (now < previous_predict + 1000) {
        return DAVE_SETTINGS.response || [];
    }
    DAVE_SETTINGS.setCookie('dave_keyword_predict_time', now)
    typed = typed || "";
    repeats = repeats || 0;
    let user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
    conversation_id = conversation_id || DAVE_SETTINGS.CONVERSATION_ID || DAVE_SETTINGS.getCookie('dave_conversation_id') || DAVE_SETTINGS.getCookie('conversation_id');
    function do_set(data) {
        DAVE_SETTINGS.setCookie("dave_keywords", data);
        DAVE_SETTINGS.setCookie("dave_keyword_time", djQ.now());
    }
    otyped = typed;
    typed = typed.split(" ").filter(function (el) { return el.length >= 3 && el != 'and' && el != 'but' && el != 'for' && el != 'the' && el != 'are' });
    if (!DAVE_SETTINGS.otyped || !otyped.startsWith(DAVE_SETTINGS.otyped)) {
        console.debug("User written something different, so starting again");
        DAVE_SETTINGS.response = [];
        DAVE_SETTINGS.typed = typed;
    } else if (DAVE_SETTINGS.typed && DAVE_SETTINGS.predict_length <= otyped.length) {
        console.debug("User added something, so finding the new word");
        let predict_again = 0;
        for (let t in DAVE_SETTINGS.typed) {
            if (typed[t] != DAVE_SETTINGS.typed[t]) {
                break
            }
            predict_again += 1;
        }
        DAVE_SETTINGS.typed = typed;
        typed = typed.slice(predict_again);
    } else {
        console.debug("User has deleted something. So starting again");
        DAVE_SETTINGS.typed = typed;
        DAVE_SETTINGS.response = [];
    }
    DAVE_SETTINGS.predict_length = otyped.length;
    DAVE_SETTINGS.otyped = otyped;
    function do_keywords(data, noset) {
        if (!noset) {
            do_set(data);
        }
        response = [];
        resp = {};
        if (typed.length <= 0) {
            if (callbackFunc) {
                callbackFunc(DAVE_SETTINGS.response);
            }
            return DAVE_SETTINGS.response;
        }
        for (let k in data.keywords) {
            let d = data.keywords[k];
            if (!d[0]) {
                continue;
            }
            let d0 = d[0].split(" ");
            for (let v = 0; v < d0.length; v++) {
                let d1 = d0[v];
                if (typeof d1 != 'string') {
                    continue
                }
                let dl1 = d1.toLowerCase();
                for (let tv = 0; tv < typed.length; tv++) {
                    let t = typed[tv].toLowerCase();
                    if (dl1 == t || (tv == typed.length - 1 && dl1.startsWith(t))) {
                        let r = {};
                        r[d[1]] = d[2];
                        if (!resp[d[2]]) {
                            response.push(r);
                            resp[d[2]] = d[1];
                        }
                        break;
                    }
                }
            }
        }
        DAVE_SETTINGS.response = DAVE_SETTINGS.response.concat(response).filter(function (v, i, a) { return a.findIndex(function (v1) { return Object.keys(v1)[0] == Object.keys(v)[0] }) === i });
        if (callbackFunc) {
            callbackFunc(DAVE_SETTINGS.response);
        }
        return DAVE_SETTINGS.response;
    }
    let keywords = DAVE_SETTINGS.getCookie("dave_keywords");
    let ret = do_keywords(keywords, true);
    if (DAVE_SETTINGS.getCookie("dave_keyword_time") > (djQ.now() - 5 * 60 * 60 * 1000)) {
        return ret
    }
    DAVE_SETTINGS.ajaxRequest(
        "/conversation-keywords/" + DAVE_SETTINGS.CONVERSATION_ID,
        "GET",
        do_set,
        errorFunc
    );
    return ret;
}


// Below this are all internal functions
DAVE_SETTINGS.iupdate_user = function iupdate_user(params, callbackFunc, errorFunc, repeats) {
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.iupdate_user, [params, callbackFunc, errorFunc, repeats || 0], 'dave_user_id')) {
        return;
    }
    // Used to update session duration and session number
    let user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
    params = params || {};
    params['_async'] = true;
    DAVE_SETTINGS.ajaxRequestWithData("/iupdate/" + DAVE_SETTINGS.USER_MODEL + "/" + user_id, "POST", JSON.stringify(params), callbackFunc, errorFunc);
}

DAVE_SETTINGS.iupdate_session = function iupdate_session(params, callbackFunc, errorFunc, repeats) {
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.iupdate_session, [params, callbackFunc, errorFunc, repeats || 0], 'dave_session_id')) {
        return;
    }
    // Used to update session duration and session number
    let session_id = DAVE_SETTINGS.getCookie('dave_session_id') || DAVE_SETTINGS.getCookie('session_id');
    params = params || {};
    params['_async'] = true;
    DAVE_SETTINGS.ajaxRequestWithData("/iupdate/" + DAVE_SETTINGS.SESSION_MODEL + "/" + session_id, "POST", JSON.stringify(params), callbackFunc, errorFunc);
}

DAVE_SETTINGS.create_interaction = function create_interaction(params, callbackFunc, errorFunc, repeats) {
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.create_interaction, [params, callbackFunc, errorFunc, repeats || 0])) {
        return;
    }
    // Used to update session duration and session number
    let session_id = DAVE_SETTINGS.getCookie('dave_session_id') || DAVE_SETTINGS.getCookie('session_id');
    let user_id = DAVE_SETTINGS.getCookie("dave_user_id") || DAVE_SETTINGS.getCookie("user_id");
    params = params || {};
    params['_async'] = true;
    params[DAVE_SETTINGS.INTERACTION_SESSION_ID] = session_id;
    params[DAVE_SETTINGS.INTERACTION_USER_ID] = user_id;
    params["conversation_id"] = DAVE_SETTINGS.CONVERSATION_ID;
    if (DAVE_SETTINGS.INTERACTION_ORIGIN) {
        params[DAVE_SETTINGS.INTERACTION_ORIGIN] = window.location.href
    }
    for (let k in DAVE_SETTINGS.DEFAULT_INTERACTION_DATA) {
        params[k] = DAVE_SETTINGS.DEFAULT_INTERACTION_DATA[k]
    }
    DAVE_SETTINGS.execute_custom_callback("before_create_interaction", [params]);
    DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.INTERACTION_MODEL, "POST", JSON.stringify(params),
        function (data) {
            DAVE_SETTINGS.execute_custom_callback("after_create_interaction", [data, params]);
            if (callbackFunc && typeof (callbackFunc)) {
                callbackFunc(data);
            }
        },
        errorFunc);
}

DAVE_SETTINGS.event_callbacks = {};
DAVE_SETTINGS.create_event = function create_event(description, params, callbackFunc, errorFunc, timestamp, repeats) {
    timestamp = timestamp || Math.floor(new Date().getTime() / 1000);
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.create_event, [description, params, callbackFunc, errorFunc, timestamp, repeats || 0], 'dave_session_id')) {
        return;
    }
    params = params || {};
    if (['opened', 'resumed', 'closed', 'minimized', 'queried', 'created'].indexOf(description) >= 0) {
        params[DAVE_SETTINGS.INTERACTION_STAGE_ATTR] = description;
        DAVE_SETTINGS.create_interaction(params);
    }
    if (DAVE_SETTINGS.cancel_events && DAVE_SETTINGS.cancel_events[description]) {
        for (let k in DAVE_SETTINGS.cancel_events[description]) {
            console.debug("Cancelling timer for event " + description)
            let t = DAVE_SETTINGS.cancel_events[description][k];
            if (t) {
                clearTimeout(DAVE_SETTINGS.reset_timers[t]);
                delete DAVE_SETTINGS.reset_timers[t];
                clearTimeout(DAVE_SETTINGS.event_timers[t]);
                delete DAVE_SETTINGS.event_timers[t];
            }
        }
        DAVE_SETTINGS.cancel_events[description] = [];
    }
    if (!DAVE_SETTINGS.EVENT_MODEL || params["_skip_post"]) {
        return;
    }
    let session_id = DAVE_SETTINGS.getCookie('dave_session_id') || DAVE_SETTINGS.getCookie('session_id');
    let user_id = DAVE_SETTINGS.getCookie("dave_user_id") || DAVE_SETTINGS.getCookie("user_id");
    params['_async'] = true;
    params[DAVE_SETTINGS.EVENT_DESCRIPTION] = description;
    params[DAVE_SETTINGS.EVENT_SESSION_ID] = session_id;
    params[DAVE_SETTINGS.EVENT_USER_ID] = user_id;
    params["conversation_id"] = DAVE_SETTINGS.CONVERSATION_ID;
    params[DAVE_SETTINGS.EVENT_WAIT_TIME] = timestamp - DAVE_SETTINGS.EVENT_TIMER;
    DAVE_SETTINGS.EVENT_TIMER = timestamp;
    if (DAVE_SETTINGS.EVENT_ORIGIN) {
        params[DAVE_SETTINGS.EVENT_ORIGIN] = window.location.href
    }
    let ed = DAVE_SETTINGS.DEFAULT_EVENT_DATA || DAVE_SETTINGS.DEFAULT_INTERACTION_DATA;
    for (let k in (ed)) {
        params[k] = ed[k];
    }
    DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.EVENT_MODEL, "POST", JSON.stringify(params),
        function (data) {
            if (DAVE_SETTINGS.event_callbacks[description]) {
                DAVE_SETTINGS.event_callbacks[description] = DAVE_SETTINGS.makeList(DAVE_SETTINGS.event_callbacks[description], []);
                for (let k of DAVE_SETTINGS.event_callbacks[description]) {
                    k(description, params.event_type);
                }
            }
            if (DAVE_SETTINGS.event_callbacks['__ALL__']) {
                DAVE_SETTINGS.event_callbacks['__ALL__'] = DAVE_SETTINGS.makeList(DAVE_SETTINGS.event_callbacks['__ALL__'], []);
                for (let k of DAVE_SETTINGS.event_callbacks['__ALL__']) {
                    k(description, params.event_type);
                }
            }
            if (callbackFunc && typeof (callbackFunc) == "function") {
                callbackFunc(data)
            }
        },
        errorFunc
    );
}

DAVE_SETTINGS.register_event = function register_event(identifier, description, event_type, params, beforeEventCallback, afterEventCallback) {
    event_type = event_type || 'click';
    params = params || {};
    djQ(identifier).on(event_type, function () {
        if (beforeEventCallback && typeof (beforeEventCallback) == 'function') {
            let d = beforeEventCallback(this, description, params, event_type);
            if (d) {
                description = d;
            }
        }
        params['event_type'] = event_type;
        DAVE_SETTINGS.create_event(description, params, afterEventCallback);
    });
}

DAVE_SETTINGS.play_again = function () {
    if (DAVE_SCENE.snd) {
        DAVE_SCENE.playAgain();
    }
}

DAVE_SETTINGS.execute_custom_callback = function execute_custom_callback(cb, args, base) {
    args = args || [];
    base = base || DAVE_SETTINGS;
    console.debug("Executing custom callback " + cb)
    base[cb] = DAVE_SETTINGS.makeList(base[cb], []);
    for (let k of base[cb]) {
        if (k && typeof (k) == 'function') {
            try {
                let r = k.apply({}, args);
                if (r === false) {
                    return r;
                }
            } catch (err) {
                console.error(err);
            }
        }
    }
    return true;
}

DAVE_SETTINGS.register_custom_callback = function register_custom_callback(cb, f, base) {
    base = base || DAVE_SETTINGS;
    base[cb] = DAVE_SETTINGS.makeList(base[cb], []);
    base[cb].push(f);
}

DAVE_SETTINGS.unregister_custom_callback = function unregister_custom_callback(cb, f, base) {
    base = base || DAVE_SETTINGS;
    base[cb] = DAVE_SETTINGS.makeList(base[cb], []);
    let i = base[cb].indexOf(f);
    if (i >= 0) {
        base[cb].splice(i, 1);
    }
}

DAVE_SETTINGS.on_maximize = function () {
    DAVE_SCENE.opened = true;
    if (DAVE_SCENE.queued_audio && DAVE_SCENE.playable && DAVE_SCENE.opened) {
        let qa = DAVE_SCENE.queued_audio
        delete DAVE_SCENE.queued_audio;
        DAVE_SCENE.play(qa, true);
    }
    DAVE_SETTINGS.ask_microphone();
    if (DAVE_SCENE.opened_once) {
        DAVE_SETTINGS.create_event('resumed');
    } else {
        DAVE_SETTINGS.create_event('opened');
    }
    DAVE_SETTINGS.opened_time = new Date();
    DAVE_SCENE.only_avatar_shown = false;
    DAVE_SCENE.maximized = true;
    DAVE_SCENE.minimized = false;
    DAVE_SCENE.unmute();
    DAVE_SCENE.opened_once = true;
    return DAVE_SETTINGS.execute_custom_callback('on_maximize_custom');
}

DAVE_SETTINGS.ask_microphone = function ask_microphone() {
    if (DAVE_SETTINGS.SPEECH_SERVER && !DAVE_SETTINGS.micAccess) {
        navigator.permissions.query({ name: 'microphone' }).then(function (r) {
            if (r.state === 'granted') {
                DAVE_SETTINGS.micAccess = true;
                DAVE_SETTINGS.execute_custom_callback('on_mic_access_granted');
            } else {
                DAVE_SETTINGS.micAccess = false;
            }
        });
        return
    }
    console.debug('Not asking for microphone permissions because no speech server set or permissions given');
}

DAVE_SETTINGS.on_close = function () {
    DAVE_SCENE.opened = false;
    DAVE_SCENE.only_avatar_shown = false;
    DAVE_SCENE.maximized = false;
    DAVE_SCENE.minimized = false;
    DAVE_SETTINGS.create_event('closed');
    if (DAVE_SETTINGS.opened_time instanceof Date) {
        DAVE_SETTINGS.iupdate_session({ 'session_duration': (new Date().getTime() - DAVE_SETTINGS.opened_time.getTime()) / 1000 }, function () {
            DAVE_SETTINGS.opened_time = null;
        });
    }
    if (DAVE_SCENE.loaded) {
        DAVE_SCENE.stop();
    }
    return DAVE_SETTINGS.execute_custom_callback('on_close_custom');
}

DAVE_SETTINGS.register_event_callback = function (func, event_type) {
    event_type = event_type || '__ALL__';
    if (typeof (func) == "function") {
        if (!DAVE_SETTINGS.event_callbacks[event_type]) {
            DAVE_SETTINGS.event_callbacks[event_type] = [];
        }
        DAVE_SETTINGS.event_callbacks[event_type].push(func);
    }
}

DAVE_SETTINGS.on_minimize = function () {
    if (DAVE_SCENE.is_mobile) {
        DAVE_SETTINGS.on_close();
    } else {
        DAVE_SETTINGS.opened_time = new Date();
        DAVE_SCENE.opened = true;
        DAVE_SCENE.opened_once = true;
        DAVE_SCENE.unmute();
        DAVE_SCENE.only_avatar_shown = false;
        DAVE_SCENE.maximized = false;
        DAVE_SCENE.minimized = true;
    }
    DAVE_SETTINGS.create_event('minimized');
    return DAVE_SETTINGS.execute_custom_callback('on_minimize_custom');
}

DAVE_SETTINGS.on_show_avatar = function () {
    DAVE_SCENE.opened = true;
    DAVE_SCENE.only_avatar_shown = true;
    DAVE_SCENE.maximized = false;
    DAVE_SCENE.minimized = true;
    DAVE_SETTINGS.opened_time = new Date();
    DAVE_SCENE.unmute();
    DAVE_SCENE.opened_once = true;
    return DAVE_SETTINGS.execute_custom_callback('on_show_avatar_custom');
}

DAVE_SETTINGS.getCookie = function (key, def) {
    let result = window.localStorage.getItem(key);
    return DAVE_SETTINGS.get_value(result, def);
}

DAVE_SETTINGS.get_value = function (value, def) {
    if (value === null || value === undefined) {
        return def === 'undefined' ? null : def;
    }
    try {
        return JSON.parse(value);
    } catch (err) {
        return value;
    }
    return value;
}

DAVE_SETTINGS.setCookie = function (key, value, hoursExpire, onExceedCallback) {
    if (hoursExpire === undefined) {
        hoursExpire = 24
    }
    if (value === undefined) {
        return;
    }
    if (typeof value == "object") {
        value = JSON.stringify(value);
    }
    if (hoursExpire < 0 || value === null) {
        window.localStorage.removeItem(key);
    } else {
        try {
            window.localStorage.setItem(key, value);
        } catch (e) {
            if (e.name === 'QUOTA_EXCEEDED_ERR' || e.name === 'NS_ERROR_DOM_QUOTA_REACHED' || e.name == 'QuotaExceededError') {
                if (onExceedCallback && typeof (onExceedCallback) == 'function') {
                    onExceedCallback(key, value, hoursExpire);
                } else {
                    console.error("Memory is full. Cannot save. Maybe delete a few items?");
                }
            } else {
                console.error("Something went wrong trying to save " + key + " with value " + value + ': ' + e.name);
                console.error(e);
            }
        }
    }
}

DAVE_SETTINGS.clearCookie = function (key) {
    let r = DAVE_SETTINGS.getCookie(key, null);
    DAVE_SETTINGS.setCookie(key, null, -1);
    return r
}

DAVE_SETTINGS.clearCookies = function () {
    // This is for older versions so that we can do migration, now it is not required
    //let cks = ["conversation_id", "authentication", "session_id", "keywords", "keyword_time", "user_id", "started_signup", "engagement_id", "history"];
    //for (let key in cks) {
    //    DAVE_SETTINGS.clearCookie(cks[key]);
    //}
    for (let key in window.localStorage) {
        if (key.startsWith('dave_')) {
            DAVE_SETTINGS.clearCookie(key);
        }
    }
}

DAVE_SETTINGS.Trim = function (strValue) {
    if (typeof (strValue) != 'string') {
        return strValue;
    }
    return strValue.replace(/^\s+|\s+$/g, '');
}

DAVE_SETTINGS.toJqId = function (strValue) {
    if (typeof (strValue) != 'string') {
        return strValue;
    }
    return strValue.replace(/\./g, '\\.').replace(/\//g, '\\/');
}


DAVE_SETTINGS.toTitleCase = function (str) {
    if (typeof (str) != 'string') {
        return str
    }
    return str.replace(/_/g, ' ').replace(/(?:^|\s)\w/g, function (match) {
        return match.toUpperCase();
    });
}

DAVE_SETTINGS.makeList = function (inp, def) {
    if (inp == undefined || inp == null) {
        return def;
    }
    if (Array.isArray(inp)) {
        return inp;
    }
    return [inp];
}

DAVE_SETTINGS.makeSingle = function (inp, def) {
    if (inp == undefined || inp == null) {
        return def;
    }
    if (Array.isArray(inp)) {
        if (inp.length == 0) {
            return def
        }
        return inp[0];
    }
    return inp
}


DAVE_SETTINGS.generate_random_string = function (string_length) {
    let random_string = '';
    let random_ascii;
    for (let i = 0; i < string_length; i++) {
        random_ascii = Math.floor((Math.random() * 25) + 97);
        random_string += String.fromCharCode(random_ascii)
    }
    return random_string
}

DAVE_SETTINGS.random_choice = function random_choice(input_array) {
    return input_array[Math.floor(Math.random() * input_array.length)];
}


DAVE_SETTINGS.ajaxRequestSync = function (URL, METHOD, callbackFunc, errorFunc, async, unauthorized, HEADERS) {
    HEADERS = HEADERS || DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication");
    // Global Unauthorized function can be defined as function(callback, errorCallback) {... }
    if (!unauthorized) {
        unauthorized = DAVE_SETTINGS.UNAUTHORIZED_FUNCTION || function (callbackFunc, errorFunc) {
            DAVE_SETTINGS.signup({}, callbackFunc, errorFunc)
        };
    }
    function unauthorized_function() {
        DAVE_SETTINGS.clearCookie('dave_user_id');
        DAVE_SETTINGS.clearCookie('dave_authentication');
        unauthorized(function () {
            DAVE_SETTINGS.ajaxRequestSync(URL, METHOD, callbackFunc, errorFunc, async, function () {
                console.error("Could not get access credentials DaveAI chatbot! Please contact customer support!");
                if (typeof DAVE_SETTINGS.signup_failed == 'function') {
                    DAVE_SETTINGS.signup_failed();
                }
            }, HEADERS);
        }, function () {
            console.error("Could not get access credentials DaveAI chatbot! Please contact customer support!");
            if (typeof DAVE_SETTINGS.signup_failed == 'function') {
                DAVE_SETTINGS.signup_failed();
            }
        });
    }
    djQ.ajax({
        url: DAVE_SETTINGS.BASE_URL + URL,
        method: (METHOD || "GET").toUpperCase(),
        dataType: "json",
        contentType: "json",
        async: async || false,
        headers: HEADERS,
        statusCode: {
            401: unauthorized,
            404: unauthorized
        }
    }).done(function (data) {
        result = data;
        if (callbackFunc) {
            callbackFunc(data);
        }
    }).fail(function (err) {
        if (errorFunc) {
            errorFunc(err)
        }
    });
}

DAVE_SETTINGS.ajaxRequest = function (URL, METHOD, callbackFunc, errorFunc, unauthorized, HEADERS) {
    return DAVE_SETTINGS.ajaxRequestSync(URL, METHOD, callbackFunc, errorFunc, true, unauthorized, HEADERS);
}

DAVE_SETTINGS.ajaxRequestWithData = function (URL, METHOD, DATA, callbackFunc, errorFunc, unauthorized, HEADERS, retries) {
    HEADERS = HEADERS || DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication");
    METHOD = (METHOD || "GET").toUpperCase();
    retries = retries || 0;
    var defaultData = "";
    if (DATA) {
        defaultData = DATA;
    }
    let timeout = 600000;
    if (defaultData._ajax_timeout) {
        timeout = defaultData._ajax_timeout;
        delete defaultData._ajax_timeout;
    }
    if (["POST", "PATCH", "DELETE"].indexOf(METHOD) >= 0 && typeof (defaultData) != 'string') {
        defaultData = JSON.stringify(defaultData);
    }
    if (!unauthorized) {
        unauthorized = function () {
            if (retries <= 5 && !DAVE_SETTINGS.getCookie('dave_started_signup')) {
                setTimeout(
                    function () {
                        DAVE_SETTINGS.clearCookie('dave_user_id');
                        DAVE_SETTINGS.clearCookie('dave_authentication');
                        DAVE_SETTINGS.signup({}, DAVE_SETTINGS.ajaxRequestWithData(URL, METHOD, DATA, callbackFunc, errorFunc, function () {
                            console.error("Could not signup do DaveAI chatbot! Please contact customer support!");
                            if (typeof DAVE_SETTINGS.signup_failed == 'function') {
                                DAVE_SETTINGS.signup_failed();
                            }
                        }, HEADERS, retries + 1));
                    },
                    (retries + 1) * (retries + 1) * 5000
                )
            } else {
                console.error("Failed to authorize DaveAI plugin after 5 attempts");
            }
        }
    }
    return djQ.ajax({
        url: DAVE_SETTINGS.BASE_URL + URL,
        method: (METHOD || "GET").toUpperCase(),
        dataType: "json",
        contentType: "application/json",
        headers: HEADERS,
        data: defaultData,
        timeout: timeout,
        statusCode: {
            401: unauthorized,
            404: unauthorized,
        }
    }).done(function (data) {
        if (callbackFunc) {
            callbackFunc(data);
        }
    }).fail(function (err, st) {
        if (errorFunc) {
            errorFunc(err, st)
        }
    });
}

DAVE_SETTINGS.initialize_session = function (force, callbackFunc, repeats) {
    if (!force && DAVE_SETTINGS.custom_session && typeof (DAVE_SETTINGS.custom_session) == 'function') {
        DAVE_SETTINGS.custom_session(function (data) {
            if (callbackFunc && typeof (callbackFunc) == 'function') {
                callbackFunc(data);
            }
        });
        return
    }
    if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.initialize_session, [force, callbackFunc, repeats || 0])) {
        return;
    }
    // If Page is refreshed, then this should not happen. Which is the scenario if
    // DAVE_SETTINGS.setCookie("system_response", null);
    DAVE_SETTINGS.create_session({}, function (data) {
        DAVE_SETTINGS.setCookie("dave_last_login", data.updated);
        DAVE_SETTINGS.execute_custom_callback('on_initialize_session');
        if (callbackFunc && typeof (callbackFunc) == 'function') {
            callbackFunc(data);
        }
    });
    if (DAVE_SETTINGS.AVATAR_ID && typeof (DAVE_SCENE.loadPreScene) == 'function') {
        function load_avatar(glb_link) {
            DAVE_SCENE.loadPreScene("dave-canvas", glb_link, function () {
                DAVE_SCENE.loaded = true;
                DAVE_SETTINGS.register_custom_callback("on_canplay_audio", function (snd) {
                    let tl = DAVE_SCENE.audio_text.length
                    let nc = 1;
                    for (let k of DAVE_SCENE.audio_text.split(' ')) {
                        DAVE_SCENE.text_timer_word = setTimeout(function () {
                            DAVE_SETTINGS.execute_custom_callback('on_placeholder_word', [k])
                        }, nc)
                        nc += 1000 * ((k.length + 1) * snd.duration / tl);
                    }
                    nc = 1;
                    for (let k of DAVE_SCENE.audio_text.match(/\(?[^\.\?\!]+[\.!\?]\)?/g)) {
                        DAVE_SCENE.text_timer_sentence = setTimeout(function () {
                            DAVE_SETTINGS.execute_custom_callback('on_placeholder_sentence', [k])
                        }, nc)
                        nc += 1000 * ((k.length + 1) * snd.duration / tl);
                    }
                });
                DAVE_SETTINGS.register_custom_callback('on_pause_audio', function (snd) {
                    clearTimeout(DAVE_SCENE.text_timer_word);
                    clearTimeout(DAVE_SCENE.text_timer_sentence);
                });
                if (DAVE_SCENE.queued_audio && DAVE_SCENE.playable && DAVE_SCENE.opened) {
                    let qa = DAVE_SCENE.queued_audio
                    delete DAVE_SCENE.queued_audio;
                    DAVE_SCENE.play(qa, true);
                }
                DAVE_SETTINGS.execute_custom_callback('onload', [], DAVE_SCENE);
                DAVE_SETTINGS.execute_custom_callback('on_load_avatar', [], DAVE_SCENE);
            });
        }
        if (DAVE_SETTINGS.AVATAR_ID.startsWith('http') && (DAVE_SETTINGS.AVATAR_ID.endsWith('glb') || DAVE_SETTINGS.endsWith('fbx'))) {
            load_avatar(DAVE_SETTINGS.AVATAR_ID);
        } else {
            DAVE_SETTINGS.ajaxRequest("/object/" + DAVE_SETTINGS.AVATAR_MODEL + "/" + DAVE_SETTINGS.AVATAR_ID, "GET", function (data) {
                if (data[DAVE_SETTINGS.GLB_ATTR || 'glb_url']) {
                    load_avatar(data[DAVE_SETTINGS.GLB_ATTR || 'glb_url']);
                    DAVE_SETTINGS.LANGUAGE = data.language;
                    DAVE_SETTINGS.VOICE_ID = data.voice_id;
                } else {
                    console.warn("No data with avatar ID: " + DAVE_SETTINGS.AVATAR_ID + " found in model " + DAVE_SETTINGS.AVATAR_MODEL + " and attribute " + DAVE_SETTINGS.GLB_ATTR)
                }
            })
        }
    }
}
DAVE_SETTINGS.get_url_params = function (qd) {
    qd = qd || {};
    if (location.search) location.search.substr(1).split("&").forEach(function (item) {
        var s = item.split("="),
            k = s[0],
            v = s[1] && decodeURIComponent(s[1]); //  null-coalescing / short-circuit
        //(k in qd) ? qd[k].push(v) : qd[k] = [v]
        (qd[k] = qd[k] || []).push(v) // null-coalescing / short-circuit
    })
    return qd;
}

DAVE_SETTINGS.pretty_print = function (data, field_name_map, field_type_map, skip_fields, empty, separator) {
    separator = separator || '<br>';
    skip_fields = skip_fields || [];
    empty = empty || "";
    field_name_map = field_name_map || {};
    field_type_map = field_type_map || {};
    if (data == null || data == undefined) {
        return empty;
    }
    if (typeof (data) == "string") {
        try {
            data = JSON.parse(data);
        } catch (err) {
            return data
        }
    }
    if (typeof (data) == "number") {
        return data;
    }
    if (typeof (field_name_map) == "string") {
        field_name_map = data[field_name_map] || {};
    }
    if (typeof (field_type_map) == "string") {
        field_type_map = field_type_map || data[field_type_map] || {};
    }
    let output_string = [];
    if (Array.isArray(data)) {
        for (let k in data) {
            let d = data[k];
            d = DAVE_SETTINGS.pretty_print(d, field_name_map, field_type_map, skip_fields, empty, separator);
            if (d) {
                output_string.push(d);
            }
        }
    } else if (djQ.type(data) == "object") {
        if (data._name_map && !djQ.isEmptyObject(data._name_map)) {
            field_name_map = data._name_map
        }
        if (data._type_map && !djQ.isEmptyObject(data._type_map)) {
            field_type_map = data._type_map
        }
        for (let k in data) {
            if (skip_fields.indexOf(k) >= 0 || k.startsWith('_')) {
                continue
            }
            let v = data[k];
            if (Array.isArray(v)) {
                v = DAVE_SETTINGS.pretty_print(v, ", ", field_name_map, field_type_map, skip_fields, empty);
            }
            if (djQ.type(v) == "object") {
                if (!djQ.isEmptyObject(v)) {
                    v = '{ ' + DAVE_SETTINGS.pretty_print(v, ", ", field_name_map, field_type_map, skip_fields, empty) + " }";
                } else {
                    v = empty;
                }
            }
            if (field_type_map[k]) {
                if (field_type_map[k] == 'datetime' || field_type_map[k] == 'datetime-local') {
                    v = DAVE_SETTINGS.print_timestamp(v, v, true);
                } else if (field_type_map[k] == 'date') {
                    v = DAVE_SETTINGS.print_timestamp(v, v, true, true);
                } else if (field_type_map[k] == "file") {
                    v = '\<Uploaded File of type' + v.split('.').pop() + '\>';
                }
                // Put here other field type cases
            } else {
                let v1 = (new Date(v));
                if (v1.toLocaleString() != 'Invalid Date') {
                    v = DAVE_SETTINGS.print_timestamp(v1, v1.toLocaleString(), true);
                }
            }
            if (v) {
                output_string.push('<b>' + (field_name_map[k] || DAVE_SETTINGS.toTitleCase(k)) + "</b> : " + v)
            }
        }
        return output_string.join(separator);
    } else {
        output_string.push(data);
    }
    if (output_string.length == 0) {
        if (empty) {
            output_string.push(empty);
        } else {
            return empty;
        }
    }
    return output_string.join(separator);
}
DAVE_SETTINGS.unauthorized = function (f, args) {
    return function () {
        DAVE_SETTINGS.clearCookie('dave_user_id');
        DAVE_SETTINGS.clearCookie('dave_authentication');
        let fu = function () {
            console.error("Could not signup do DaveAI chatbot! Please contact customer support!");
            if (typeof DAVE_SETTINGS.signup_failed == 'function') {
                DAVE_SETTINGS.signup_failed();
            }
        }
        args.push(fu);
        DAVE_SETTINGS.signup({}, function () { f.apply({}, args); }, fu, true);
    }
}

DAVE_SETTINGS.upload_file = function upload_file(blob_data, file_name, callbackFunc, errorFunc, unauthorized) {
    var formData = new FormData();
    formData.append('file', blob_data, file_name);
    var headers = DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication");
    if (!unauthorized) {
        unauthorized = DAVE_SETTINGS.unauthorized(DAVE_SETTINGS.upload_file, [blob_data, file_name, callbackFunc, errorFunc])
    }
    djQ.ajax({
        url: DAVE_SETTINGS.BASE_URL + "/upload_file?large_file=true",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        headers: {
            "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
            "X-I2CE-USER-ID": headers["X-I2CE-USER-ID"],
            "X-I2CE-API-KEY": headers["X-I2CE-API-KEY"]
        },
        data: formData,
        statusCode: {
            401: unauthorized
        }
    }).done(function (data) {
        if (callbackFunc) {
            callbackFunc(data);
        }
    }).fail(function (err) {
        if (errorFunc) {
            errorFunc(err)
        }
    });
}

DAVE_SETTINGS.clean_input = function clean_input(inp) {
    let doc = new DOMParser().parseFromString(inp, 'text/html');
    return doc.body.textContent || "";
}

DAVE_SETTINGS.pad_string = function pad_string(n, width, z) {
    width = width || 2
    z = z || '0'
    return (String(z).repeat(width) + String(n)).slice(String(n).length)
}

DAVE_SETTINGS.print_timestamp = function (timestamp, invalid_date, full_form, only_date) {
    invalid_date = invalid_date || 'N/A';
    td = new Date();
    timestamp = timestamp || td;
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    if (typeof (timestamp) == "string") {
        if (!isNaN(parseFloat(timestamp)) && parseFloat(timestamp) > 1000000000000) {
            timestamp = new Date(parseFloat(timestamp) * 1000);
        } else if (timestamp.indexOf("/") >= 0) { // This is India date format
            let [d, m, y, h, M, s] = timestamp.split(/\D+/);
            if (!s) {
                s = '00'
            }
            if (!M) {
                M = td.getMinutes()
            }
            if (!h) {
                h = td.getHours()
            }
            timestamp = new Date(y, m - 1, d, h, M, s);
        } else if (timestamp.search(/\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d [A|P]M [\+|-]\d\d\d\d/) >= 0) {
            let [y, m, d, H, M, S, O] = timestamp.split(/\D+/);
            if (!S) {
                s = '00'
            }
            if (!M) {
                M = td.getMinutes()
            }
            if (!H) {
                h = td.getHours()
            }
            timestamp = new Date(y + '-' + m + '-' + d + 'T' + H + ':' + M + ':' + S + '.000' + timestamp.slice(-5));
        }
    } else if (typeof (timestamp) == 'number') {
        timestamp = new Date(timestamp * 1000);
    }
    if (djQ.type(timestamp) != 'date') {
        timestamp = new Date(timestamp);
    }
    if (timestamp.toString() == 'Invalid Date') {
        return invalid_date;
    }
    if (full_form || timestamp.getYear() != td.getYear()) {
        if (!only_date) {
            return months[timestamp.getMonth()] + " " + DAVE_SETTINGS.pad_string(timestamp.getDate()) + ', ' + timestamp.getFullYear() + ', ' + DAVE_SETTINGS.pad_string(timestamp.getHours()) + ':' + DAVE_SETTINGS.pad_string(timestamp.getMinutes());
        } else {
            return months[timestamp.getMonth()] + " " + DAVE_SETTINGS.pad_string(timestamp.getDate()) + ', ' + timestamp.getFullYear();
        }
    }
    if (timestamp.getMonth() != td.getMonth() || timestamp.getDate() != td.getDate()) {
        if (!only_date) {
            return months[timestamp.getMonth()] + " " + DAVE_SETTINGS.pad_string(timestamp.getDate()) + ', ' + DAVE_SETTINGS.pad_string(timestamp.getHours()) + ':' + DAVE_SETTINGS.pad_string(timestamp.getMinutes());
        } else {
            return months[timestamp.getMonth()] + " " + DAVE_SETTINGS.pad_string(timestamp.getDate());
        }
    }
    if (!only_date) {
        return DAVE_SETTINGS.pad_string(timestamp.getHours()) + ':' + DAVE_SETTINGS.pad_string(timestamp.getMinutes());
    } else {
        return months[timestamp.getMonth()] + " " + DAVE_SETTINGS.pad_string(timestamp.getDate());
    }
}

DAVE_SETTINGS.signup_guard = function (func, args, cookie_name, redo_function) {
    cookie_name = cookie_name || "dave_authentication"
    if (!DAVE_SETTINGS.getCookie(cookie_name) || !DAVE_SETTINGS.ENTERPRISE_ID || DAVE_SETTINGS.CONVERSATION_ID != DAVE_SETTINGS.getCookie('dave_conversation_id')) {
        console.warn(cookie_name + " headers are not set, but trying to run function " + func.name)
        let repeats = args.slice(-1);
        let args1 = args;
        if (repeats < 30) {
            if ((repeats == 10 || repeats == 20) && (DAVE_SETTINGS.getCookie('dave_started_signup') || DAVE_SETTINGS.getCookie('started_signup'))) {
                if (redo_function && typeof (redo_function) == 'function') {
                    redo_function();
                } else {
                    DAVE_SETTINGS.clearCookie('dave_started_signup');
                    DAVE_SETTINGS.load_dave();
                }
            }
            setTimeout(function () {
                args1.push(args1.pop() + 1);
                func.apply({}, args1);
            }, 1500 * (repeats + 1));
        } else {
            console.error("Stopping trying to run function" + func.name + " after " + repeats + " attempts");
            if (typeof DAVE_SETTINGS.signup_failed == 'function') {
                DAVE_SETTINGS.signup_failed();
            }
        }
        return false
    }
    return true;
}

DAVE_SETTINGS.load_settings = function load_settings(identifier, namespace) {
    if (djQ(identifier).length) {
        let ds = djQ(identifier);
        for (let s in namespace) {
            if (s.toUpperCase() != s) {
                continue;
            }
            let v = ds.attr("data-" + s.toLowerCase().replace(/_/g, '-')) || ds.attr("data-" + s.toLowerCase()) || ds.attr("data-" + s)
            if (v) {
                console.debug("Entered the condition " + s);
                let g = DAVE_SETTINGS.get_value(v);
                if (typeof (g === 'object') && g !== null && typeof (namespace[s]) === 'object' && namespace[s] !== null) {
                    for (let k in g) {
                        namespace[s][k] = g[k];
                    }
                } else {
                    namespace[s] = g;
                }
            } else {
                console.debug("Didn't get condition " + s)
            }
        }
        DAVE_SETTINGS.ENVIRONMENT_IDENTIFIER = DAVE_SETTINGS.SIGNUP_API_KEY;
    }
}

DAVE_SETTINGS.load_dave = function load_dave(callbackFunc, nocallback) {
    if (DAVE_SETTINGS.start_load_dave) {
        console.debug("Load dave called already");
        if (!nocallback && typeof (callbackFunc) == 'function') {
            callbackFunc();
        }
        return
    }
    let callbackFunc1 = function () {
        DAVE_SETTINGS.execute_custom_callback('on_load_dave');
        if (callbackFunc && typeof (callbackFunc) == 'function') {
            callbackFunc()
        }
    }
    DAVE_SETTINGS.execute_custom_callback('before_load_dave');
    DAVE_SETTINGS.start_load_dave = true;
    DAVE_SETTINGS.EVENT_TIMER = Math.floor(new Date().getTime() / 1000);
    DAVE_SETTINGS.load_settings("#dave-settings", DAVE_SETTINGS)
    if (DAVE_SETTINGS.ENTERPRISE_ID && DAVE_SETTINGS.CONVERSATION_ID && DAVE_SETTINGS.ENVIRONMENT_IDENTIFIER) {
        console.debug("Got Enterprise ID, Conversation ID, SignUp API KEY")
        if (!(DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication")) || ((DAVE_SETTINGS.getCookie("dave_conversation_id") || DAVE_SETTINGS.getCookie("conversation_id")) != DAVE_SETTINGS.CONVERSATION_ID)) {
            DAVE_SETTINGS.clearCookies();
            DAVE_SETTINGS.signup({}, callbackFunc1);
        } else if ((DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication"))['X-I2CE-ENTERPRISE-ID'] != DAVE_SETTINGS.ENTERPRISE_ID) {
            DAVE_SETTINGS.clearCookies();
            DAVE_SETTINGS.signup({}, callbackFunc1);
        } else {
            DAVE_SETTINGS.initialize_session(false, callbackFunc1);
        }
    } else {
        console.warn("Did not get one of Enterprise ID, Conversation ID or SignUp API KEY")
    }
    djQ(document).one("click", function () {
        DAVE_SCENE.audio_click();
    });
    if (DAVE_SETTINGS.SPEECH_SERVER) {
        let now_params = null;
        DAVE_SETTINGS.StreamingSpeech = {};
        DAVE_SETTINGS.streamer = new Streamer({
            wakeup_server: null,
            asr_server: DAVE_SETTINGS.SPEECH_SERVER,
            recognizer: DAVE_SETTINGS.SPEECH_RECOGNIZER,
            conversation_id: DAVE_SETTINGS.CONVERSATION_ID,
            enterprise_id: DAVE_SETTINGS.ENTERPRISE_ID,
            base_url: DAVE_SETTINGS.BASE_URL,
            auto_asr_detect_from_wakeup: false,
            vad: true,
            asr_only: true,
            audio_auto_stop: DAVE_SETTINGS.MAX_SPEECH_DURATION / 1000.0,
            asr_type: "full", //full-'flacking'||"chunks"-'asteam'||"direct"-'sending-flag',
            wakeup_type: "chunks", //chunks-'asteam'||"direct"-'sending-flag',
            video_stream_type: "chunks", //chunks-'asteam'||"direct"-'sending-flag'
            asr_additional_info: {},
            model_name: DAVE_SETTINGS.SPEECH_MODEL_NAME,
            event_callback: function (event, data) {
                if (event == 'wsconnect') {
                    if (typeof DAVE_SETTINGS.StreamingSpeech.onSocketConnect == 'function') {
                        DAVE_SETTINGS.StreamingSpeech.onSocketConnect();
                    }
                } else if (event == 'auto_stopped_recording') {
                    if (typeof DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording == 'function') {
                        DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording();
                    }
                } else if (event == 'asr_intermediate_results') {
                    if (typeof DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable == 'function') {
                        DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable(data);
                    }
                } else if (event == 'asr_results') {
                    if (typeof DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable == 'function') {
                        DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable(data);
                    }
                } else if (event == 'conversation_response') {
                    DAVE_SETTINGS.execute_custom_callback('on_response', [
                        data.name || "received chat response", data.customer_state, data, now_params
                    ]);
                    if (typeof DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable == 'function') {
                        DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable(data);
                    }
                } else if (event == 'wsdisconnect') {
                    if (typeof DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect == 'function') {
                        DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect(data);
                    }
                } else if (event == 'asr_error') {
                    if (typeof DAVE_SETTINGS.StreamingSpeech.onError == 'function') {
                        DAVE_SETTINGS.StreamingSpeech.onError(data);
                    }
                } else if (event == 'conv_error') {
                    if (typeof DAVE_SETTINGS.StreamingSpeech.onError == 'function') {
                        DAVE_SETTINGS.StreamingSpeech.onError(data);
                    }
                }
            }
        });
        DAVE_SETTINGS.StreamingSpeech.onStartVoiceRecording = function (params) {
            params = params || {}
            params.engagement_id = DAVE_SETTINGS.getCookie("dave_engagement_id");
            params.customer_id = DAVE_SETTINGS.getCookie("dave_user_id");
            params.api_key = DAVE_SETTINGS.getCookie("dave_authentication")['X-I2CE-API-KEY'];
            params.system_response = DAVE_SETTINGS.getCookie("dave_system_response");
            if (DAVE_SETTINGS.VOICE_ID && !params['voice_id']) {
                params["voice_id"] = DAVE_SETTINGS.VOICE_ID
            }
            if (DAVE_SETTINGS.LANGUAGE && !params['language']) {
                params["language"] = DAVE_SETTINGS.LANGUAGE
            }
            if (DAVE_SETTINGS.AVATAR_ID || params['avatar_id']) {
                params["synthesize_now"] = true;
                params["csv_response"] = true;
            }
            params["origin"] = window.location.href;
            params["query_type"] = 'speech';
            if (!params['temporary_data']) {
                params['temporary_data'] = {};
            }
            if (params.customer_state || params.customer_response) {
                DAVE_SETTINGS.did_interact = true;
            }
            DAVE_SETTINGS.execute_custom_callback("on_start_recording", [params]);
            DAVE_SETTINGS.execute_custom_callback("on_chat_start", [params.customer_state || params.customer_response || "sent chat message", params]);
            DAVE_SETTINGS.streamer.startVoiceRecording(params);
            DAVE_SETTINGS.create_event("queried");
            DAVE_SETTINGS.execute_custom_callback("after_start_recording", [params]);
            now_params = DAVE_SETTINGS.streamer.asr_additional_info
        }
        DAVE_SETTINGS.StreamingSpeech.onStopVoiceRecording = function () {
            DAVE_SETTINGS.execute_custom_callback("on_stop_recording");
            DAVE_SETTINGS.streamer.stopVoiceRecording();
            DAVE_SETTINGS.execute_custom_callback("after_stop_recording");
        };
        DAVE_SETTINGS.execute_custom_callback('on_load_streamer');
    } else {
        DAVE_SETTINGS.execute_custom_callback('on_no_speech_server');
    }

}

DAVE_SCENE.audio_click = function () {
    if (!DAVE_SCENE.snd) {
        return
    }
    DAVE_SCENE.snd.play()
    DAVE_SCENE.playable = true;
    if (DAVE_SCENE.queued_audio && DAVE_SCENE.playable && DAVE_SCENE.opened) {
        let qa = DAVE_SCENE.queued_audio
        delete DAVE_SCENE.queued_audio;
        DAVE_SCENE.play(qa, true);
    }
    if (!DAVE_SETTINGS.isIOS) {
        djQ(DAVE_SCENE.snd).one('ended', function () {
            DAVE_SCENE.snd.src = null;
        });
    }
}
// Pause temporaririly
DAVE_SCENE.pause = function () {
    if (DAVE_SCENE.snd) {
        DAVE_SCENE.snd.pause();
    }
}
// Pause permanently
DAVE_SCENE.stop = function () {
    if (DAVE_SCENE.snd) {
        DAVE_SCENE.snd.pause();
        DAVE_SCENE.queue = [];
    }
}
DAVE_SCENE.mute = function () {
    if (DAVE_SCENE.snd) {
        DAVE_SCENE.snd.volume = 0.0;
        DAVE_SCENE.snd.muted = true;
    }
    DAVE_SCENE.volume = 0.0
    DAVE_SETTINGS.execute_custom_callback('on_dave_mute');
}
DAVE_SCENE.unmute = function () {
    if (DAVE_SCENE.snd) {
        DAVE_SCENE.snd.volume = 1.0;
        DAVE_SCENE.snd.muted = false;
    }
    DAVE_SCENE.volume = 1.0
    DAVE_SETTINGS.execute_custom_callback('on_dave_unmute');
}

djQ(window).blur(function (e) {
    if (DAVE_SCENE.opened && DAVE_SETTINGS.opened_time instanceof Date) {
        DAVE_SETTINGS.iupdate_session({ 'session_duration': (new Date().getTime() - DAVE_SETTINGS.opened_time.getTime()) / 1000 }, function () {
            DAVE_SETTINGS.opened_time = null;
        });
    }
});
djQ(window).focus(function (e) {
    if (DAVE_SCENE.opened && !DAVE_SETTINGS.opened_time) {
        DAVE_SETTINGS.opened_time = new Date();
    }
});
if (djQ().jquery.startsWith('1') || djQ().jquery.startsWith('2')) {
    djQ(window).load(function () {
        if (!DAVE_SETTINGS.PRODUCTION) {
            console.log("Window is loaded now");
        }
        if (!DAVE_SETTINGS.MANUAL_LOAD) {
            DAVE_SETTINGS.load_dave();
        }
    });
} else {
    djQ(window).on('load', function () {
        if (!DAVE_SETTINGS.PRODUCTION) {
            console.log("Window is loaded now");
        }
        if (!DAVE_SETTINGS.MANUAL_LOAD) {
            DAVE_SETTINGS.load_dave();
        }
    });
}
DAVE_SETTINGS.execute_custom_callback('on_load_dave_code');
DAVE_SETTINGS.register_custom_callback('on_load_libraries', function () {
    if (!DAVE_SETTINGS.start_load_dave && !DAVE_SETTINGS.MANUAL_LOAD) {
        DAVE_SETTINGS.load_dave();
    }
});
/* List of CallBacks which we can register are ---
 * on_load_dave_code -- when the code is loaded
 * before_load_dave -- before load dave is called
 * on_load_dave  -- when signup is completed
 * before_customer_signup -- before the anonymous sign up happens, the data to be sent is passed as params, so we can alter them
 * on_initialize_session -- 
 * before_create_session
 * after_create_session
 * on_load_avatar
 * on_load_streamer
 * on_no_speech_server
 * on_start_recording
 * after_start_recording
 * on_stop_recording
 * after_stop_recording
 * on_chat_start
 * on_response
 * on_load_history
 * after_submit_feedback
 * before_create_interaction
 * after_create_interaction
 * on_maximize_custom
 * on_minimize_custom
 * on_close_custom
 * on_show_avatar_custom
 * on_canplay_audio
 * on_placeholder_word
 * on_placeholder_sentence
 * on_load_libraries -- when all the libraries set up in library loader are loaded
 */

