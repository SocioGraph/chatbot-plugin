let DAVE_PLUGIN_ASSET_PATH;
if ( typeof( DAVE_ENVIRONMENT ) != 'undefined' && DAVE_ENVIRONMENT.DAVE_ASSET_PATH ) {
    DAVE_PLUGIN_ASSET_PATH = DAVE_ENVIRONMENT.DAVE_ASSET_PATH.endsWith('/') ? DAVE_ENVIRONMENT.DAVE_ASSET_PATH : DAVE_ENVIRONMENT.DAVE_ASSET_PATH + '/';
} else if (document.currentScript && isFinite(document.currentScript.src.split('/').slice(-2, -1)[0])) {
    DAVE_PLUGIN_ASSET_PATH = (document.currentScript && (document.currentScript.src.split('/').slice(0, -4).join('/') + '/'));
} else {
    DAVE_PLUGIN_ASSET_PATH = (document.currentScript && (document.currentScript.src.split('/').slice(0, -3).join('/') + '/')) || "https://chatbot-plugin.iamdave.ai/";
}
if ( typeof( DAVE_ENVIRONMENT ) != 'undefined' && typeof ( DAVE_ENVIRONMENT.on_load_settings ) == 'function' ) {
    DAVE_ENVIRONMENT.on_load_settings()
}
DAVE_LIBRARY_DEPENDENCY = {
    "base": ["jquery.js", "polyfill.js", "datetimepicker.js", "blobUtil.js", "dave.js"],
    "slim": ["jquery.js", "polyfill.js", "dave.js"],
    "speech": ["streaming_speech/vendor/socket.io/socket.io.js", "streaming_speech/vendor/socket.io/socket.stream.io.js", "streaming_speech/vendor/recordRTC/RecordRTC.js","streaming_speech/vendor/vad/vad.js", "streaming_speech/vendor/libflac/data-util.js", "streaming_speech/vendor/libflac/download-util.js", "streaming_speech/vendor/libflac/file-handler.js", "streaming_speech/vendor/libflac/libflac.js", "streaming_speech/streaming_speech.js"],
    "avatar": [ "babylon.js", "babylonjs.loaders.min.js", "avatar.js"],
    "help": ["jquery.js", "polyfill.js", "help.js"],
    "wjq": ["jqc.js", "polyfill.js", "datetimepicker.js", "blobUtil.js", "dave.js"],
}
DAVE_LIBRARY_DEPENDENCY['speech-avatar'] = DAVE_LIBRARY_DEPENDENCY['speech'].concat(DAVE_LIBRARY_DEPENDENCY['avatar']);
DAVE_LIBRARY_DEPENDENCY.chatbot = DAVE_LIBRARY_DEPENDENCY.base.concat(["new-chat.js"]);
DAVE_LIBRARY_DEPENDENCY['chatbot-v2'] = DAVE_LIBRARY_DEPENDENCY.base.concat(["dynamic_chatbot.js"]);
DAVE_LIBRARY_DEPENDENCY['chatbot-speech'] = DAVE_LIBRARY_DEPENDENCY.speech.concat(DAVE_LIBRARY_DEPENDENCY.base);
DAVE_LIBRARY_DEPENDENCY['chatbot-speech-avatar'] = DAVE_LIBRARY_DEPENDENCY['speech-avatar'].concat( DAVE_LIBRARY_DEPENDENCY.chatbot );
DAVE_LIBRARY_DEPENDENCY['chatbot-speech-avatar-wbl'] = DAVE_LIBRARY_DEPENDENCY['chatbot-speech-avatar'].filter(i => i != 'babylon.js');
DAVE_LIBRARY_DEPENDENCY['chatbot-slim'] = DAVE_LIBRARY_DEPENDENCY.slim.concat(["new-chat.js"]);
for ( let k of ['chatbot', 'chatbot-v2', 'chatbot-speech', 'chatbot-speech-avatar', 'chatbot-slim', 'chatbot-speech-avatar-wbl'] ) {
    DAVE_LIBRARY_DEPENDENCY[k + '-wjq'] = DAVE_LIBRARY_DEPENDENCY[k].map(i => i != 'jquery.js' ? i : 'jqc.js');
    DAVE_LIBRARY_DEPENDENCY[k + '-help'] = DAVE_LIBRARY_DEPENDENCY[k].concat(["help.js"]);
    DAVE_LIBRARY_DEPENDENCY[k + '-wjq' + '-help'] = DAVE_LIBRARY_DEPENDENCY[k + '-wjq'].concat(["help.js"]);
}
DAVE_LIBRARY_DEPENDENCY['libraries'] = DAVE_LIBRARY_DEPENDENCY['speech-avatar'].concat(DAVE_LIBRARY_DEPENDENCY['base'], ['help.js'])


function daveDocReady(fn) {
    // see if DOM is already available
    // if (document.readyState === "complete" || document.readyState === "interactive") {
    //     // call on next available tick
    //     setTimeout(fn, 1);
    // } else {
    //     document.addEventListener("DOMContentLoaded", fn);
    // }
    window.addEventListener("load", ()=>{
        setTimeout(fn, 1);
    });
}    

daveDocReady(function() {
    if ( document.getElementById("dave-settings") && document.getElementById("dave-settings").dataset.daveComponents ) {
        function loadScript(sources, callBack) {
            var script      = document.createElement('script');
            script.src      = sources[0];
            script.async    = false;
            script.type     = 'text/javascript';
            script.addEventListener('load', function() {
                sources.shift()
                if ( sources.length > 0 ) {
                    loadScript(sources, callBack);
                    return
                }
                if(sources.length == 0 && typeof callBack == "function") {
                    callBack();
                    return
                }
            });
            document.body.appendChild(script);
        }
        let components = [];
        let joiner = '/';
        let base_url = '';
        let dave_version = document.getElementById("dave-settings").dataset.daveVersion
        if ( typeof( DAVE_ENVIRONMENT ) != 'undefined' && DAVE_ENVIRONMENT.ENVIRONMENT && DAVE_ENVIRONMENT.ENVIRONMENT.toLowerCase() == 'production' ) {
            joiner = '/min/';
            if (!dave_version) {
                dave_version = '9.0'
            }
        } else {
            if (!dave_version) {
                dave_version = '';
                joiner = ''
            }
        }
        if ( typeof( DAVE_ENVIRONMENT ) != 'undefined' && DAVE_ENVIRONMENT.APP_PATH ) {
            base_url = DAVE_ENVIRONMENT.APP_PATH.endsWith('/') ? DAVE_ENVIRONMENT.APP_PATH : ( DAVE_ENVIRONMENT.APP_PATH + '/' );
        }
        document.getElementById("dave-settings").dataset.daveComponents.split(',').forEach(function(item) {
            if ( DAVE_LIBRARY_DEPENDENCY[item] ) {
                DAVE_LIBRARY_DEPENDENCY[item].forEach(function(item1) {
                    let i = DAVE_PLUGIN_ASSET_PATH + 'assets/js/' + dave_version + joiner + item1;
                    if ( components.indexOf(i) < 0 ) {
                        components.push(i);
                    }
                });
            } else if ( item.startsWith('dave-') ) {
                components.push(DAVE_PLUGIN_ASSET_PATH + 'assets/js/' + dave_version + joiner + item);
            } else {
                components.push(base_url + item);
            }
        });
        let loaded = false;
        let ls = function() {
            DAVE_ENVIRONMENT.started_loading = true;
            if (!loaded) {
                loadScript(components, function() {
                    DAVE_ENVIRONMENT.finished_loading = true;
                    DAVE_SETTINGS.execute_custom_callback('on_load_libraries');
                    loaded = true
                });
            }
        }
        if ( !DAVE_ENVIRONMENT.LOAD_WITH_DOC ) {
            window.addEventListener("click", ()=>{
                ls();
            }, {once: true});
            window.addEventListener("scroll", ()=>{
                ls();
            }, {once: true});
            window.addEventListener("keydown", ()=>{
                ls();
            }, {once: true});
            window.addEventListener("touchstart", ()=>{
                ls();
            }, {once: true});
            window.addEventListener("mousemove", ()=>{
                ls();
            }, {once: true});
        } else {
            ls();
        }
    }
});
