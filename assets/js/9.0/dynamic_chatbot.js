DAVE_SETTINGS.BASE_URL = "https://staging.iamdave.ai";
DAVE_SETTINGS.CONVERSATION_ID = "exhibit_aldo";
DAVE_SETTINGS.ENTERPRISE_ID = "dave_expo";
DAVE_SETTINGS.SIGNUP_API_KEY = "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__";
// DAVE_SETTINGS.AVATAR_ID="hiro_xtreme-english-male";
//DAVE_SETTINGS.SPEECH_SERVER = "https://speech-test.iamdave.ai";
DAVE_SETTINGS.nowRecording = false;

DAVE_SETTINGS.register_custom_callback("on_chat_start", function (x, params) {
  if (!params.temporary_data) {
    params["temporary_data"] = {};
  }
  params["refresh_synth"] = true;
  params.temporary_data["origin"] = window.location.origin;
  let up = DAVE_SETTINGS.get_url_params();
  for (k of ["utm_source", "utm_medium", "utm_campaign", "utm_term"]) {
    if (up[k]) {
      params[k] = up[k];
    }
  }
});
DAVE_SETTINGS.register_custom_callback("on_load_libraries", function () {
  DAVE_SETTINGS.add_dave_elements();
  DAVE_SETTINGS.setup_dave_buttons();
});
DAVE_SETTINGS.register_custom_callback("on_load_dave", function () {
  DAVE_SETTINGS.history();
});
//
// params: {'customer_state': <customer_state>, 'customer_response': <customer_response>, 'query_type': <auto/click/type/
window.botchat_data = function botchat_data(params, callBack) {
  DAVE_SETTINGS.chat(
    params,
    function (data) {
      console.log(data["placeholder"]);
      djQ(".chat-placeholders").empty().text(data["placeholder"]);
      djQ(".dave-cc-text").text(data["placeholder"]);

      if (data["state_options"] && !djQ.isEmptyObject(data["state_options"])) {
        djQ("#chat-services-button").empty();
        for (const [key, value] of Object.entries(data["state_options"])) {
          djQ("#chat-services-button").append(`
                          <div class="col-md-6 col-sm-6 col-xs-6 dave-button" data-customer_state="${key}" data-customer_response="${value}" >
                              <button class="btn btn-default" data-customer_state="${key}" ><span class="glyphicon glyphicon-home"></span>${value}</button>
                          </div>
              `);
        }
        djQ(".dave-button").on("click", function () {
          window.botchat_data({
            customer_state: djQ(this).data("customer_state"),
            customer_response: djQ(this).data("customer_response"),
            query_type: "click",
          });
        });
      }

      if (callBack && typeof callBack == "function") {
        callBack(data);
      }
    },
    function (e) {
      window.minimize_chatbot(true);
      console.error("Could not get a response from chatbot");
      console.error(e);
    }
  );
};
// DAVE_SETTINGS.botchat_data = botchat_data(params, function(data) {

// });
// window.botchat_data = DAVE_SETTINGS.botchat_data;

DAVE_SETTINGS.register_custom_callback(
  "before_create_interaction",
  function (params) {
    params["conversation_id"] = DAVE_SETTINGS.CONVERSATION_ID;
  }
);
DAVE_SETTINGS.register_custom_callback(
  "before_create_session",
  function (params) {
    params["conversation_id"] = DAVE_SETTINGS.CONVERSATION_ID;
  }
);

DAVE_SETTINGS.add_dave_elements = function add_dave_elements(params) {
  // djQ('#dave-settings').append(`
  //     <div class="chat-bot-template" id="chat-bot-template">hi</div>
  // `);
};
DAVE_SETTINGS.setup_dave_buttons = function setup_dave_buttons(params) {
  djQ("#dave-ask-section-close").on("click", function () {
    DAVE_SETTINGS.maximize_chatbot();
  });
  djQ("#dave-chat-dots").on("click", function () {
    DAVE_SETTINGS.maximize_chatbot();
  });
  djQ("#dave-ask-section-open").on("click", function () {
    DAVE_SETTINGS.minimize_chatbot();
  });
  djQ("#dave-chat-maximise").on("click", function () {
    DAVE_SETTINGS.maximize_chatbot();
  });
  DAVE_SETTINGS.maximize_chatbot = function maximize_chatbot(avtar) {
    if (avtar) {
      djQ("#chat-bot-template").show();
    }
    if (!$(".chat-services").is(":visible")) {
      $(".chat-services").show("slow");
      $(".chat-placeholders, .car-section").hide("slow");
    } else {
      $(".chat-services, .car-section").hide("slow");
    }
    DAVE_SETTINGS.on_maximize();
  };
  DAVE_SETTINGS.minimize_chatbot = function minimize_chatbot(avtar) {
    if (avtar) {
      djQ("#chat-bot-template").hide();
    }
    djQ("#dave-ask-section").animate({ height: "0px" });
    // djQ( "dave-ask-section-open" ).hide();
    DAVE_SETTINGS.on_minimize();
  };
  let clicked_pred_custState;
  let dave_predication_send = undefined;
  djQ("#dave-chat-text").on("input", function () {
    text_cleared = false;
    djQ("#dave-chat-send-button")
      .off("click")
      .on("click", function () {
        let dave_user_inputtext = DAVE_SETTINGS.clean_input(
          djQ("#dave-chat-text").val()
        );
        if (dave_user_inputtext.length >= 1) {
          if (!dave_predication_send) {
            window.botchat_data({
              customer_response: dave_user_inputtext,
              query_type: "type",
            });
          } else {
            window.botchat_data({
              customer_response: dave_user_inputtext,
              customer_state: clicked_pred_custState,
              query_type: "click",
            });
          }
          //Chat box height Adjustment according to device
          djQ("#dave-cb-textinput").val("");
          djQ(".dave-cc-text-right").text(dave_user_inputtext);
          text_cleared = true;
        }
      });
  });

  DAVE_SETTINGS.history(function (data) {
    dave_return_size = Object.keys(data["history"]).length;
    if (dave_return_size > 0) {
      //on_load_history(data);
    } else {
      //If no history available
      chat_historyActivated = false;
      window.botchat_data({ query_type: "auto" });
    }
  });

  function on_load_history(data) {
    DAVE_SETTINGS.execute_custom_callback("before_load_history", [data]);
    let count = 0;
    chat_loading = true;
    djQ.each(data["history"], function (his_key, his_value) {
      count++;
      if (his_value["direction"] == "system") {
        dave_response_dataType = his_value["data"]["response_type"];
        his_value["timestamp"] = DAVE_SETTINGS.print_timestamp(
          his_value["timestamp"]
        );
        //print bot reply as message
        c_bubbleId = his_value["response_id"];
        c_bubbleId = "RI" + c_bubbleId;
        if (count > 1) {
          if (typeof his_value["reply_to"] == "undefined") {
            msgBubbles(
              "bot",
              "history",
              his_value["timestamp"],
              "",
              c_bubbleId
            );
          } else {
            msgBubbles(
              "bot",
              "history",
              his_value["timestamp"],
              "",
              c_bubbleId,
              his_value["response_id"]
            );
          }
        } else {
          msgBubbles("bot", "history", his_value["timestamp"], "");
        }
        djQ(".dave-chattext").last().html(his_value["placeholder"]);

        if (
          his_value["state_options"] &&
          !djQ.isEmptyObject(his_value["state_options"])
        ) {
          djQ(".dave-cb-chatsugg").append(
            `<span class='chatsugg-next'><img src='${cb_sugg_next}'></span><span class='chatsugg-prev'><img src='${cb_sugg_prev}'></span>`
          );
        }
        response_elementPrint(
          dave_response_dataType,
          his_value,
          "history",
          count
        );
        if (
          his_value["state_options"] &&
          !djQ.isEmptyObject(his_value["state_options"])
        ) {
          //print bot suggestion
          for (const [key, value] of Object.entries(
            his_value["state_options"]
          )) {
            djQ(".dave-cb-chatsugg").append(
              `<div class='dave-cb-chatsugg-list'>${value}<input type='hidden' value='${key}'></div>`
            );
          }
        }

        //QUICKACCESS BUTTON NEXT & PREV ONLY VISIBLE ON SCROLL IS AVAILABLE
        let chatsugg_scrollWidth = djQ(".dave-cb-chatsugg")[0].scrollWidth;

        if (chatsugg_scrollWidth > chatbox_width - 20) {
          djQ("span.chatsugg-next").css("display", "block");

          if (dave_optionTitle) {
            djQ("span.chatsugg-next").css("top", "10px");
            djQ("span.chatsugg-prev").css("top", "10px");
          } else {
            djQ(".dave-chatsuggTitle").hide();
          }
        }
      } else if (his_value["direction"] == "other") {
        c_bubbleId = his_value["response_id"];
        c_bubbleId = "RI" + c_bubbleId;
        replyTo = "RI" + his_value["reply_to"];
        other_chat_data(
          null,
          his_value["user_icon"],
          his_value["user_name"],
          his_value["timestamp"],
          his_value["placeholder"],
          replyTo,
          his_value,
          c_bubbleId
        );
      } else {
        //user chat printing
        his_value["timestamp"] = DAVE_SETTINGS.print_timestamp(
          his_value["timestamp"]
        );
        c_bubbleId = his_value["response_id"];
        c_bubbleId = "RI" + c_bubbleId;
        msgBubbles(
          "user",
          "history",
          his_value["timestamp"],
          DAVE_SETTINGS.pretty_print(
            DAVE_SETTINGS.clean_input(his_value["customer_response"]),
            "_name_map",
            "_type_map"
          ),
          c_bubbleId,
          "",
          his_value["customer_state"]
        );
      }
      //Chat box height Adjustment according to device
      set_inner_height();
      if (count == dave_return_size) {
        //
        djQ("button#start-recording").prop("disabled", false);
        //
        djQ(".dave-cb-chatarea").animate(
          { scrollTop: djQ(".dave-cb-chatarea").prop("scrollHeight") },
          500
        );
        dave_typingLoaderRemove("bot");
      }
    });

    dave_userchatfunc_exe = false;
    chat_historyActivated = true;
    userchat_data();
    DAVE_SETTINGS.execute_custom_callback("after_load_history", [data]);
  }
};
