/** @suppress{duplicate} */
DAVE_HELP = {
    timers: {},
    enabled: DAVE_SETTINGS.getCookie('dave_help', true),
    fadeout: 3000,
    on_image: DAVE_SETTINGS.asset_path + 'assets/img/on-image.svg',
    off_image: DAVE_SETTINGS.asset_path + 'assets/img/off-image.svg',
    default_length: 10
}; // namespace

/**
 * Add the help overlay SVG panel.
 */




DAVE_HELP.get_help_text = function (text, subtext, id) {
    id = id || DAVE_SETTINGS.generate_random_string(6);
    let dst = subtext ? 'block' : 'none';
    let dt = text ? 'block' : 'none';
    let r = djQ(`
        <div id='dave-help-text-${id}' class='dave-help-text dave-help-shadow'>
            <span id='dave-help-close-${id}' class='dave-help-close-symbol'>&#x2715;</span>
            <span class='dave-help-close'>Close Help</span>
            <p class='dave-help-paragraph' style='display:${dt}'>${text}</p>
            <p class='dave-help-sub-text' style='display:${dst}'>${subtext}</p>
        </div>
    `);
    r.data('id', id);
    return r
}

DAVE_HELP.add_help_text = function (text, subtext, left, top, timeout, elem, id, clickCallback) {
    if ( !DAVE_HELP.enabled ) {
        return;
    }
    id = id || DAVE_SETTINGS.generate_random_string(6);
    elem = elem || DAVE_HELP.get_help_text(text, subtext, id);
    if (!elem.attr('id') == 'dave-help-text-' + id ) {
        elem.attr('id', 'dave-help-text-' + id)
        elem.find('.dave-help-close-symbol').each(function() {
            this.attr('id', 'dave-help-close-' + id)
        })
    }
    left = left || 'calc( 50vw - 200px )';
    top = top || '10px';
    timeout = timeout || DAVE_HELP.timeout
    elem.css('top', top);
    elem.css('left', left);
    djQ( document.body ).append(elem);
    djQ( '#dave-help-close-' + id ).one('click', function() { 
        DAVE_HELP.remove(id); 
    });
    if ( timeout < 3600000 ) {
        djQ( window ).one('scroll resize', function() { 
            DAVE_HELP.remove(id, DAVE_HELP.fadeout); 
        });
    }
    if ( timeout < 3600000 ) {
        let t = setTimeout( function() { DAVE_HELP.remove(id, DAVE_HELP.fadeout); }, timeout )
        DAVE_HELP.timers[id] = t;
    }
    if ( clickCallback && typeof(clickCallback) == 'function' ) {
        djQ ( elem ).on('click', function() {
            clickCallback( $( this ) );
        })
    }
    return elem
}

DAVE_HELP.enable = function() {
    DAVE_HELP.enabled = true;
    djQ( '#dave-help-off-image' ).hide();
    djQ( '#dave-help-on-image' ).show();
    DAVE_SETTINGS.setCookie('dave_help', true);
    djQ( '#dave-help-on-image' ).one('click', function() {
        DAVE_HELP.disable();
    });
}

DAVE_HELP.disable = function() {
    DAVE_HELP.enabled = false;
    djQ( '#dave-help-on-image' ).hide();
    djQ( '#dave-help-off-image' ).show();
    DAVE_SETTINGS.setCookie('dave_help', false);
    djQ( '#dave-help-off-image' ).one('click', function() {
        DAVE_HELP.enable();
    });
}

DAVE_HELP.remove = function (id, fadeout) {
    let elem = '.dave-help-text';
    if ( id && typeof(id) == 'string' ) {
        elem = '#dave-help-text-' + id;
    } else if (id) {
        elem = id;
    }
    var svg = djQ( elem );
    if ( !svg || svg.length <= 0 ) {
        return;
    }
    svg.fadeOut(fadeout || 0, svg.remove);
    if ( id ) {
        if ( DAVE_HELP.timers[id] ) {
            clearTimeout(DAVE_HELP.timers[id])
            delete DAVE_HELP.timers[id];
        }
    } else {
        for ( let k in DAVE_HELP.timers ) {
            clearTimeout(DAVE_HELP.timers[k]);
            delete DAVE_HELP.timers[k];
        }
    }
}

DAVE_HELP.is_overlap = function (elem1_left, elem1_top, elem1_width, elem1_height, elem2_left, elem2_top, elem2_width, elem2_height) {
    let elem1_right = elem1_left + elem1_width;
    let elem1_bottom = elem1_top + elem1_height;
}

/**
 * Add a help label to the specified position.
 * @param {number} tox
 * @param {number} toy
 * @param {string} label
 * @param {string} pos
 * @param {number=} length
 */
DAVE_HELP.get_position = function (dom, elem, pos, length, xaway, yaway) {
    var offset = dom.offset();
    var width = dom.outerWidth();
    var height = dom.outerHeight();
    var eheight = elem.outerHeight();
    var ewidth = elem.outerWidth();
    var pwidth = djQ( window ).width();
    var pheight = djQ( window ).height();
    var awayx;
    var awayy;
    xaway = xaway || 1;
    yaway = yaway || 1;
    let left = 0;
    let top = 0;
    if (length) {
        xaway *= length;
        yaway *= length;
    }
    if ( pos == "right" ) {
        left = Math.min(offset.left + width + xaway, pwidth - ewidth - xaway);
        if ( ( left >= offset.left && left <= offset.left + width ) || ( left + ewidth >= offset.left && left + ewidth <= offset.left + width ) || ( left <= offset.left && left + ewidth >= offset.left + width ) ) {
            if ( offset.top < eheight + yaway ) {
                top = offset.top + height + yaway;
            } else {
                top = offset.top - eheight - yaway;
            }
        } else {
            top = offset.top;
        }
        top = Math.max(top - djQ( window ).scrollTop(), 0);
        return [left, top]
    }
    if ( pos == "left" ) {
        left = Math.max(offset.left - xaway - ewidth, 0);
        if ( ( left >= offset.left && left <= offset.left + width ) || ( left + ewidth >= offset.left && left + ewidth <= offset.left + width ) || ( left <= offset.left && left + ewidth >= offset.left + width ) ) {
            if ( offset.top < eheight + yaway ) {
                top = offset.top + height + yaway;
            } else {
                top = offset.top - eheight - yaway;
            }
        } else {
            top = offset.top;
        }
        top = Math.max(top - djQ( window ).scrollTop(), 0);
        return [left, top]
    }
    if ( pos == "above" ) {
        top = Math.max(offset.top - yaway - eheight - djQ( window ).scrollTop(), 0);
        if ( ( top >= offset.top && top <= offset.top + height ) || ( top + eheight >= offset.top && top + eheight <= offset.top + height ) || ( top <= offset.top && top + eheight >= offset.top + height ) ) {
            if ( offset.left < ewidth + xaway ) {
                left = offset.left + width + xaway;
            } else {
                left = offset.left - ewidth - xaway;
            }
        } else {
            left = offset.left;
        }
        left = Math.min(Math.max(left, 0), pwidth);
        return [left, top]
    } 
    if ( pos == "below" ) {
        top = Math.min(offset.top + yaway - djQ( window ).scrollTop(), pheight);
        if ( ( top >= offset.top && top <= offset.top + height ) || ( top + eheight >= offset.top && top + eheight <= offset.top + height ) ) {
            if ( offset.left < ewidth + xaway ) {
                left = offset.left + width + xaway;
            } else {
                left = offset.left - ewidth - xaway;
            }
        } else {
            left = offset.left;
        }
        left = Math.min(Math.max(left, 0), pwidth);
        return [left, top]
    }
    if ( pos == "aboveleft" ) {
        top = Math.max(offset.top - yaway - eheight - djQ( window ).scrollTop(), 0);
        left = Math.max(offset.left - xaway - ewidth, 0);
        if ( ( top >= offset.top && top <= offset.top + height ) || ( top + eheight >= offset.top && top + eheight <= offset.top + height ) || ( top <= offset.top && top + eheight >= offset.top + height ) ) {
            if ( offset.left < ewidth + xaway ) {
                left = offset.left + width + xaway;
            } else {
                left = offset.left - ewidth - xaway;
            }
        } else if ( ( left >= offset.left && left <= offset.left + width ) || ( left + ewidth >= offset.left && left + ewidth <= offset.left + width ) || ( left <= offset.left && left + ewidth >= offset.left + width ) ) {
            if ( offset.top < eheight + yaway ) {
                top = offset.top + height + yaway;
            } else {
                top = offset.top - eheight - yaway;
            }
        } else {
            left = offset.left;
        }
        left = Math.min(Math.max(left, 0), pwidth);
        return [left, top]
    } 
    return [pwidth/2 - ewidth/2, length]
}

/**
 * ##### DAVE_HELP.help_for_element
 * #####  Shows help label relative to an element on the screen.   #######
 * @param {jQuery} dom
 * @param {string} dom (jquery selector)
 * @param {string} text
 * @param {string} subtext
 * @param {object} pos  {position: right/left/above/below, length: distance from dom, xaway: x distance from dom, yaway: y distance form dom}
 * @param {string} pos  right/left/above/below  position relative to which the the help-text has to be displayed
 * @param {number} timout 
 * @param {function} clickCallback    function called if help-text is clicked, not required, 
 * @param {function} displayCallback    function called if help-text is displayed, not required, 
 *
 * 
 * e.g. DAVE_HELP.help_for_element("#MyId", "This is a help", "Click me", "above", 5000, function() {console.log("This help text was clicked");}, function() {console.log("This help text was displayed")})
 */
DAVE_HELP.help_for_element = function(dom, text, subtext, pos, timeout, clickCallback, displayCallback) {
    if ( !DAVE_HELP.enabled ) {
        return;
    }
    dom = DAVE_HELP.process_dom(dom, true);
    if (!dom) {
        return;
    }
    pos = pos || {'position': 'right', 'length': DAVE_HELP.default_length};
    if (typeof(pos) == 'string') {
        pos = {'position': pos, 'length': DAVE_HELP.default_length};
    }
    if ( !pos.position ) {
        pos.position = 'right';
    }
    if ( pos.top && pos.left ) {
        var elem = DAVE_HELP.add_help_text(text, subtext, pos.left, pos.top, timeout, null, null, clickCallback);
        if ( displayCallback && typeof(displayCallback) == 'function' ) {
            displayCallback(elem);
        }
        return elem
    }
    if (['above', 'below', 'left', 'right', 'aboveleft', 'aboveright', 'belowleft', 'belowright'].indexOf(pos.position) < 0 ) {
        console.error("Incorrect positon information " + pos.position);
        return;
    }
    if ( !pos.length ) {
        if ( pos.xaway || pos.yaway ) {
            pos.length == 1;
        }
        pos.length = DAVE_HELP.default_length;
    }
    var id = DAVE_SETTINGS.generate_random_string(6);
    var elem = DAVE_HELP.add_help_text(text, subtext, null, null, timeout, null, null, clickCallback);
    var np = DAVE_HELP.get_position(dom, elem, pos.position, pos.length, pos.xaway, pos.yaway);
    elem.css('left', np[0]);
    elem.css('top', np[1]);
    if ( displayCallback && typeof(displayCallback) == 'function' ) {
        displayCallback(elem);
    }
    return elem
}

DAVE_HELP.process_dom = function(dom, one) {
    let sdom = dom;
    if ( typeof(dom) == 'string' ) {
        dom = djQ( dom );
    } else {
        sdom = dom.first().attr('id');
    }
    if ( !dom || dom.length <= 0 ) {
        console.error("Dom element to add help text not found: " + sdom);
        return;
    }
    if ( one ) {
        return dom.first();
    }
    return dom;
}

/**
 * ##### DAVE_HELP.bind_help_for_element
 * Function binds the help text event to an element, but shows it only once since the document loads. It works even if the help text is not enabled at the time of binding
 * @param {jQuery} dom   element to bind the action event to
 * @param {string} dom (jquery selector) element to bind the action event to
 * @param {string} text
 * @param {string} subtext
 * @param {string} action   The the jQuery event type on which to trigger the help text, defaults to 'click'
 * @param {jQuery} anchor_dom  element relative to which the help text should occur, defaults to 'dom' if evaluates to false
 * @param {string} anchor_dom (jquery selector) element relative to which the help text should occur, defaults to 'dom'
 * @param {object} pos  {position: right/left/above/below, length: distance from dom, xaway: x distance from dom, yaway: y distance form dom}
 * @param {string} pos  right/left/above/below
 * @param {number} timout   time in milliseconds taken to disappear, not required, default can be set globally in DAVE_HELP.timeout
 * @param {function} clickCallback    function called if help-text is clicked, not required, 
 * @param {function} displayCallback    function called if help-text is displayed, not required, 
 *
 * e.g. DAVE_HELP.bind_help_for_element("#MyId", "This is a help", "Click me", "click", "above", "#MyOtherElement", 5000, function() {console.log("This help text was clicked");}, function() {console.log("This help text was displayed")})
 */
DAVE_HELP.bind_help_for_element = function(dom, text, subtext, action, pos, anchor_dom, timeout, clickCallback, displayCallback) {
    dom = DAVE_HELP.process_dom(dom);
    if (!dom) {
        return;
    }
    djQ( '#dave-help' ).show();
    action = action || 'click';
    djQ( dom ).each(function() {
        djQ( this ).one(action, function() {
            if ( DAVE_HELP.enabled ) {
                DAVE_HELP.help_for_element(anchor_dom || djQ( this ), text, subtext, pos, timeout, clickCallback, displayCallback);
            } else {
                DAVE_HELP.bind_help_for_element(djQ( this ), text, subtext, action, pos, anchor_dom, timeout, clickCallback, displayCallback);
            }
        });
    });
}

/**
 * ##### DAVE_HELP.bind_pop_for_element
 * Function binds the help text event to an element, but shows it everytime the event occurs.
 * @param {jQuery} dom   element to bind the action event to
 * @param {string} dom (jquery selector) element to bind the action event to
 * @param {string} text
 * @param {string} subtext
 * @param {string} action   The the jQuery event type on which to trigger the help text
 * @param {jQuery} anchor_dom  element relative to which the help text should occur, defaults to 'dom' if evaluates to false
 * @param {string} anchor_dom (jquery selector) element relative to which the help text should occur, defaults to 'dom'
 * @param {object} pos  {position: right/left/above/below, length: distance from dom, xaway: x distance from dom, yaway: y distance form dom}
 * @param {string} pos  right/left/above/below
 * @param {number} timout   time in milliseconds taken to disappear, not required, default can be set globally in DAVE_HELP.timeout
 * @param {function} clickCallback    function called if help-text is clicked, not required, 
 * @param {function} displayCallback    function called if help-text is displayed, not required, 
 *
 * e.g. DAVE_HELP.bind_pop_for_element("#MyId", "This is a help", "Click me", "click", "above", "#MyOtherElement", 5000, function() {console.log("This help text was clicked");}, function() {console.log("This help text was displayed")})
 */
DAVE_HELP.bind_pop_for_element = function(dom, text, subtext, action, pos, anchor_dom, timeout, clickCallback, displayCallback) {
    dom = DAVE_HELP.process_dom(dom);
    if (!dom) {
        return;
    }
    djQ( '#dave-help' ).show();
    action = action || 'click';
    djQ( dom ).each(function() {
        djQ( this ).on(action, function() {
            if ( DAVE_HELP.enabled ) {
                DAVE_HELP.help_for_element(anchor_dom || djQ( this ), text, subtext, pos, timeout, clickCallback, displayCallback);
            }
        });
    });
}

djQ( document ).ready( function() {
    DAVE_HELP.on_image = djQ( '#dave-help' ).attr('data-on-image') || DAVE_SETTINGS.asset_path + 'assets/img/on-image.svg';
    DAVE_HELP.off_image = djQ( '#dave-help' ).attr('data-off-image') || DAVE_SETTINGS.asset_path + 'assets/img/off-image.svg';
    DAVE_HELP.timeout = parseInt(djQ( '#dave-help' ).attr( 'data-timeout' ));
    DAVE_HELP.fadeout = parseInt(djQ( '#dave-help' ).attr( 'data-fadeout' ) || DAVE_HELP.fadeout);
    if ( !isFinite(DAVE_HELP.timeout) ) {
        DAVE_HELP.timeout = null;
    }
    djQ( '#dave-help' ).append( `
    <img src='${DAVE_HELP.on_image}' class='dave-help-control' id='dave-help-on-image'>
    <img src='${DAVE_HELP.off_image}' class='dave-help-control' id='dave-help-off-image'>
    <p class='dave-help-control'> Help Text</p>
    `);
    djQ( '#dave-help' ).hide();
    if ( DAVE_HELP.enabled ) {
        DAVE_HELP.enable();
    } else {
        DAVE_HELP.disable();
    }
})
