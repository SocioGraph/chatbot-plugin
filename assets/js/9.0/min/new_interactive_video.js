function ivBody(){return{temp_ques:"",ques_err:"",ques_array:[],xPostForum(){chatFuncExe||(console.log("CHAT func NOT exe"),this.ques_array.push({ques:temp_holder.quesToPost,quesTimestamp:secToHms(temp_holder.quesStartTime)}))},xPostQues(){0<this.temp_ques.length?this.ques_err="":this.ques_err="please enter your question"},qnaTimestamp(e){video.currentTime(hmsToSec(e)),video.play(),console.log(e)},alp_resumeVideo(){video.currentTime(temp_holder.quesStartTime),video.play()},alp_pauseVideo(e){video.pause(),0==e&&video.play()}}}function reviewAnswerFront(){return{reply:!1,yes:void 0,no:void 0,thankyou:!1,after_reply:void 0,questionText:"Satisfied with the answer?",showReviewSec:!0}}djQ(document).ready(function(){const t=djQ("#dave-settings").attr("data-IV-loader")||"./assets/img/loading.gif",a=djQ("#dave-settings").attr("data-IV-closeIcon")||"./assets/img/interactive_video/close.svg";var e=djQ("#dave-settings").attr("data-IV-likeIcon")||"./assets/img/interactive_video/Like.svg",o=djQ("#dave-settings").attr("data-IV-likeActiveIcon")||"./assets/img/interactive_video/Like-active.svg",s=djQ("#dave-settings").attr("data-IV-dislikeIcon")||"./assets/img/interactive_video/Dislike.svg",n=djQ("#dave-settings").attr("data-IV-dislikeActiveIcon")||"./assets/img/interactive_video/Dislike-active.svg",i=djQ("#dave-settings").attr("data-IV-inputSendIcon")||"./assets/img/interactive_video/send.png",d=djQ("#dave-settings").attr("data-IV-forumMinIcon")||"./assets/img/interactive_video/Minimize.svg",r=djQ("#dave-settings").attr("data-IV-forumMaxIcon")||"./assets/img/interactive_video/Max.svg";function l(){temp_holder.f_half_height=djQ(".f-half").height(),djQ(".s-half").height(temp_holder.f_half_height)}function c(){return djQ(window).width()}function u(e){console.log("got Triggered"),console.log(c()),1366<=c()&&("min"==e?(djQ(".bottom-f-half").addClass("bottom-f-half-max"),djQ(".video-container").addClass("video-container-max")):(djQ(".bottom-f-half").removeClass("bottom-f-half-max"),djQ(".video-container").removeClass("video-container-max")))}video=void 0,videoActionAllowed=!1,chatFuncExe=!0,postQuestion=!1,videoResumePoint=!1,branchBtnClickClass=void 0,window.temp_holder={videoCurTime:void 0,f_half_height:void 0,quesToPost:void 0,ansToPost:void 0,chatType:"normal",quesPostTime:void 0,quesStartTime:void 0},djQ(window).resize(function(){console.warn("window resize"),djQ(document).on("fullscreenchange",video,function(){fullScreen_leftPanelAdjustment=void 0,video.isFullscreen()?fullScreen_leftPanelAdjustment=!0:(fullScreen_leftPanelAdjustment=!1,l())}),video.isFullscreen()||(djQ(document).unbind("mousemove",function(){console.log("Mouse move detection stopped")}),l())});function v(e){e?djQ(".daveAvatarIVCont").css("display","block"):djQ(".daveAvatarIVCont").css("display","none")}function m(e,t,a){console.log("DAVE SCENE IS CALLED"),DAVE_SCENE.onload=function(){dave_avatar_loadStatus=!0,DAVE_SCENE.set_avatar_position(e,t,a),console.log("Avatar is loaded")}}function p(e,t,a){t=t||!1,a=a||"",v(chatFuncExe=!0),djQ(".IV_chatBubble").remove(),djQ(".daveAvatarIVCont").append(`
                    <p class="IV_chatBubble ${a}">${e}</p>
                `).ready(function(){l()}),djQ(document).find(".review-answer").remove(),t&&djQ("#dave-videoPlayer").append(`
                    <div class="review-answer" x-data="reviewAnswerFront();">
                        <div x-show="showReviewSec">
                            <p class="review-question" x-text="questionText"></p>
                            <template x-if="!reply">
                                <div class="review-button-holder">
                                    <button class="review-button r_yes" @click="reply = true, yes = true, no = false, thankyou = true, showReviewSec = false, setTimeout(() => thankyou = false, 5000), alp_resumeVideo()">Yes</button>
                                    <button class="review-button r_no"  @click="reply = true, yes = false, no = true, questionText='You can still get the answers'">No</button>
                                </div>
                            </template>
                            <template x-if="no">
                                <div class="review-button-holder">
                                    <button class="review-button a_expert" @click="showReviewSec = false;xPostForum();" id='askExpert'>Ask Expert</button>
                                    <button class="review-button a_community" @click="showReviewSec = false;xPostForum()" id='askCommunity'>Ask Community</button>
                                </div>
                            </template>
                        </div>
                        <p x-show="thankyou" class="review-thankyou">Thank You!</p>
                    </div>
            `),videoResumePoint=!0,video.pause()}window.secToHms=function(e){e=parseInt(e);var t=Math.floor(e/3600),a=Math.floor(e%3600/60),e=Math.floor(e%3600%60);return(0<t?t<10?"0"+t+":":t+":":"")+(0<a?a<10?"0"+a+":":a+":":"00:")+(0<e?e<10?"0"+e:e:"00")},window.hmsToSec=function(e){var t=e.length,a=e.split(":");let o=null;switch(t){case 2:o=parseInt(a[0]);break;case 5:o=parseInt(60*a[0])+parseInt(a[1]);break;case 8:o=parseInt(3600*a[0])+parseInt(60*a[1])+parseInt(a[2])}return o},window.generateRandomNum=function(){return Math.floor(9999*Math.random()+1)};let h=`
    <div class="s-half">
        <div class="maxForum"><img src="${r}" alt="Maximise Forum" /></div>
        <div class="qna-container">
            <div class="qna-title-container">
                <div class="qna-title"><strong>User Question & Answers</strong></div>
                <div class="title-icon dave-forum-minimize"><img src="${d}" alt='Minimize'></div>
            </div>
            <div class="qna-body-container">
                <template x-for="(questions, q_index) in ques_array" :key="q_index">
                    <div class="qna-box">
                        <ol x-data="{showmore : false, ansButton: false}">
                            <li class="qna-video-timestamp" @click="qnaTimestamp($el.textContent)" x-text="questions.quesTimestamp"></li>
                            <li class="qna-question"><strong class="qna-ques-symbol">Q:</strong><strong class="qna-ques-frame" x-text="questions.ques"></strong></li>
                            <li class="qna-answerButton" @click="ansButton = !ansButton, alp_pauseVideo(ansButton)">Answer this quetion</li>
                            <li class="qna-answerInput-container" x-show="ansButton" x-transition.duration.150ms.scale.orign.top>
                                
                                <textarea minlength="2" type="text" name="qna-answerInput" class="qna-answerInput" placeholder="Please write your answer."></textarea>
                                <input type="submit" value="Submit" class="input-answerSubmit">
                            </li>
                            <li class="qna-answers-container">
                                
                            </li>
                        </ol>
                        <hr>
                    </div>
                </template>
            </div>
        </div>
    </div>
    `,f=`
    <div class="bottom-f-half">
        <div class="player-controls">
            <div class="video-title-container">
                <strong id="video-title"></strong>
                 <p id="video-author"></p>
            </div>
            
            <div class="video-cta" x-data='{subscribe: false, like: false, dislike: false}'>
                <button class="video-subscribe" @click="subscribe = !subscribe" :class="{'video-subscribed' : subscribe}" x-text="subscribe ? 'Subscribed' : 'Subscribe'"></button>
                <button class="video-like" @click="like = !like" :class="{'video-liked': like}">
                    <img :src="like ? '${o}' : '${e}'" alt="" class="like-img" />
                    <span x-text="like ? ' Liked' : ' Like'"></span>
                </button>
                <button class="video-dislike" :class="{'video-disliked': dislike}" @click="dislike = !dislike"><img :src="dislike ? '${n}' : '${s}' " alt="" class="dislike-img"><span x-text="dislike ? ' Disliked' : ' Dislike'"></span></button>
            </div>
        </div>
        <hr>
        <div class="qna-input-container" x-data="{mic_active: false, qna_submit_img: false}">
            <div class="qna-input-Button">
                <p class="postQuesErr err_txt" x-text="ques_err"></p>
                <input type="text" class="qna-input" id='communityQuesInput' placeholder="ask your questions here .." x-model="temp_ques" autofocus>
                <button class="qna-input-submit" @mouseover="qna_submit_img = true" @mouseleave="qna_submit_img = false" @click="xPostQues(); temp_ques='' ">
                    <template x-if="qna_submit_img != undefined">
                        <img :src="qna_submit_img ? './assets/img/interactive_video/sendhover.png' : '${i}'" alt="Submit Your Question" class="qna-input-submitSvg">
                    </template>
                </button>
            </div>
            <button class="qna-mic-button" id="start-recording"><img src="./assets/img/interactive_video/Mic.svg" alt="" class="mic-img"></button>
            <button class="qna-mic-button qna-mic-button-active" id="stop-recording"><img src="./assets/img/interactive_video/Mic.svg" alt="" class="mic-img"></button>
        </div>
    </div>
    `;function g(){djQ(".dave-iv-popup").remove(),djQ(".s-half").remove(),djQ(".bottom-f-half").remove(),djQ(".daveWebTagAvatar").remove(),webTagTimeOut=void 0}djQ(".video-anchor").on("click",function(e){e.preventDefault(),djQ("body").css({overflow:"hidden"}),djQ("body").append(`
            <div class='dave-iv-popup'>
            <div class="videoPlayerLoader">
                <img src="${t}" alt="loading dave interactive video player">
            </div>
            <div class="dave-popup-close"><img src="${a}" alt="close dave interactive video" /></div>
                <div class="IV_conatiner" x-data="ivBody()">
                    <div class="f-half">
                        <div class="video-container">
                            <video
                            id="dave-videoPlayer"
                            class="vjs-big-play-centered video-js"
                            data-dave-video-id="">
                            <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a
                            web browser that
                            <a href="https://videojs.com/html5-video-support/" target="_blank"
                                >supports HTML5 video</a>
                            </p>
                            </video>
                        </div>
                        ${f}
                    </div>
                    ${h}
                </div>
            </div>
        </div>
        `).ready(function(){var e=djQ("#dave-settings").attr("data-avatar-id")||!1;dave_avatar_loadStatus=!1,DAVE_SCENE.set_avatar_position=function(e,t,a){e=e||0,t=t||0,a=a||200,djQ(".daveAvatarIVCont").css({left:e,bottom:t}),djQ("canvas#dave-canvas").height(a)},e&&(console.log("THE AVATAR ID => "+e),djQ(document).find(".video-container").append(`
                <div class='daveAvatarIVCont'>
                    <canvas id='dave-canvas'>
                        <p>this is p</p>
                    </canvas>
                </div>
                `),m(djQ("#dave-settings").attr("data-avatar-position-x")||null,djQ("#dave-settings").attr("data-avatar-position-y")||null,djQ("#dave-settings").attr("data-avatar-position-h")||null))}),DAVE_IV_SETTINGS.initialize_video_player("",function(e){console.log(e);let o=e.video;var t=e.video_tag,a=e.video_branch,e=(e.forum_answer,e.forum_question);let i={},d={};videoActionAllowed=!0,video&&video.dispose(),djQ(".videoPlayerLoader").remove(),djQ(".IV_conatiner").addClass("show-IV_container"),video=videojs("#dave-videoPlayer",{autoplay:!1,controls:!0,sources:[{src:o.video_url,type:"video/mp4"}],preload:"auto",aspectRatio:"16:9",loop:!1,fluid:!1,playbackRates:[.25,.5,1,1.25,1.5,2],disablePictureInPicture:!0}),djQ("#video-title").text(o.video_name),djQ("video").attr("data-dave-video-id",o.video_id),djQ(".player-controls").css("opacity","1"),l(),v(!0),e.sort(function(e,t){return e.timestamp<t.timestamp?-1:e.timestamp>t.timestamp?1:0});let r={};function s(e,t){e&&djQ("button#start-recording").css("display",e),t&&djQ("button#stop-recording").css("display",t)}$.each(e,function(e,t){r[secToHms(t.timestamp)]=t}),$.each(t,function(e,t){var a=t.customer_response,o=t.video_tag_id,s=t.customer_state;i[o]=t.position,djQ("#dave-videoPlayer").append(`
                    <button class='video-tag-button video-tags hide-vTags' data-custState="${s}" data-video-tag-id="${o}">
                        <span class="videoTagPlusIcon"><i class="fas fa-plus-circle"></i></span>
                        <span>${a}</span>
                    </button>
                `)}),$.each(a,function(e,t){var a=t.question;let s=t.video_branch_id;vb_timestamp=t.timestamp,d[s]={},d[s].timestamp=vb_timestamp,d[s].responded=!1,djQ("#dave-videoPlayer").append(`
                    <div class="video-branch-container hide-vBranch" data-vbID = "${s}">
                        <p class="video-branch-question">${a}</p>
                        <div class="video-branch-option-container"></div>
                    </div>
                `),$.each(t.options,function(e,t){var a=t.customer_state,o=t.customer_response,t=t.redirect||"";djQ("#dave-videoPlayer").find(`[data-vbID = "${s}"]`).find(".video-branch-option-container").append(`<button class="video-branch-option" data-custState="${a}" data-redirectTime="${t}">${o}</button>`)})}),branchDisplayStartTime=function(e){let a=void 0;return $.each(e,function(e,t){t=t.timestamp;(t<a||void 0===a)&&(a=t)}),a}(d),tagDisplayStartTime=function(e){let a=void 0;return $.each(e,function(e,t){t=t[0].start_time;(t<a||void 0===a)&&(a=t)}),a}(i),djQ(document).one("click",".video-tag-button",function(){var e=djQ(this).text().trim(),t=djQ(this).attr("data-custState");djQ(this).attr("data-video-tag-id");console.log("you clicked on tags"),djQ(this).remove(),DAVE_SETTINGS.chat({customer_state:t,customer_response:e,query_type:"click"},function(e){console.log(e),p(e.placeholder)})}),djQ(document).on("click",".video-branch-option",function(){djQ(".video-branch-option").unbind("click").bind("click",function(e){var t=djQ(this).attr("data-custState"),a=djQ(this).text().trim(),o=djQ(this).closest(".video-branch-container").attr("data-vbid"),s=djQ(this).attr("data-redirectTime");branchBtnClickClass="successChatBubble",s&&(video.currentTime(s),branchBtnClickClass="errorChatBubble",console.warn("REDIRECTED TO TIMESTAMP")),d[o].responded=!0,djQ(this).closest(".video-branch-container").removeClass("show-vBranch").addClass("hide-vBranch"),DAVE_SETTINGS.chat({customer_state:t,customer_response:a,query_type:"click"},function(e){console.log(e),p(e.placeholder,!1,branchBtnClickClass)})})}),djQ(document).on("focus","input",function(){}),video.on("loadedmetadata",function(){video.duration();video.on("play",function(){console.log(temp_holder.quesStartTime+" <======================"),videoResumePoint&&(video.currentTime(temp_holder.quesStartTime),videoResumePoint=!1,console.log(temp_holder.quesStartTime),console.log("resume point is executed")),v(!1)}),window.videoPlaying=function(){let n=0;djQ("video").bind("timeupdate",function(){djQ(".IV_chatBubble").remove();let o=video.currentTime();var e,a,s=Math.floor(o%3600%60);if(Number.isInteger(s)&&s!=n){n=s,o>=tagDisplayStartTime&&$.each(i,function(a,e){$.each(e,function(e,t){o>=t.start_time&&o<=t.end_time?(djQ("#dave-videoPlayer").find(`[data-video-tag-id = ${a}]`).addClass("show-vTags").removeClass("hide-vTags"),djQ("#dave-videoPlayer").find(`[data-video-tag-id = ${a}]`).css({left:t.x+"%",bottom:t.y+"%"})):djQ("#dave-videoPlayer").find(`[data-video-tag-id = ${a}]`).addClass("hide-vTags").removeClass("show-vTags")})}),o>=branchDisplayStartTime&&$.each(d,function(e,t){o>=t.timestamp&&!t.responded&&(djQ("#dave-videoPlayer").find(`[data-vbID = "${e}"]`).removeClass("hide-vBranch").animate({opacity:"1",height:"65%"}),video.pause())});let t=secToHms(o);r.hasOwnProperty(t)&&(r[t].created,s=r[t].forum_question_id,e=r[t].question,a=r[t].timestamp,$(".qna-body-container").append(`
                                        <div class="qna-box" data-quesID='${s}'>
                                            <ol x-data="{showmore : false, ansButton: false}">
                                                <li class="qna-video-timestamp" @click="qnaTimestamp($el.textContent)">${secToHms(a)}</li>
                                                <li class="qna-question"><strong class="qna-ques-symbol">Q:</strong><strong class="qna-ques-frame">${e}</strong></li>
                                                <li class="qna-answerButton" @click="ansButton = !ansButton, alp_pauseVideo(ansButton)">Answer this quetion</li>
                                                <li class="qna-answerInput-container" x-show="ansButton" x-transition.duration.150ms.scale.orign.top>
                                                    <textarea minlength="2" type="text" name="qna-answerInput" class="qna-answerInput" placeholder="Please write your answer."></textarea>
                                                    <input type="submit" value="Submit" class="input-answerSubmit">
                                                </li>
                                                <li class="qna-answers-container">
                                                
                                                </li>
                                                <li class="qna-see-more">
                                                    <img src="./assets/img/interactive_video/Down.svg" alt="" class="see-more-icon" x-show="!showmore">
                                                    <a href="" @click.prevent="showmore = !showmore"><strong class="see-more-title" x-text="showmore ? 'See Less' : 'See More' "></strong></a>
                                                </li>
                                            </ol>
                                            <hr>
                                        </div>
                                    `).ready(function(){let i=0,d=r[t].answers.length;var e;d<1&&$(".qna-body-container").last(".qna-box").find(".qna-see-more").remove(),$.each(r[t].answers,function(e,t){var a=t.answer,o=t.created,s=t.forum_question_id,n=t.forum_answer_id,t=t.person_name;i<1||1==d?$(".qna-body-container").find(`[data-quesID='${s}']`).find(".qna-answers-container ").append(`
                                                    <div class="qna-answers-block" data-ansID="${n}" data-qID = "${s}">
                                                        <div class="qna-answers"><div class="ans-prefix">A:</div><div class="ans-content">${a}</div></div>
                                                        <div class="qna-author-container">
                                                            <div class="qna-author">By&nbsp${t}</div>
                                                            <div class="qna-author-timestamp">${DAVE_SETTINGS.print_timestamp(o,"",!0)}</div>
                                                        </div>
                                                    </div>
                                                `):$(".qna-body-container").find(`[data-quesID='${s}']`).find(".qna-answers-container").append(`
                                                    <div class="qna-answers-block" data-ansID="${n}" data-qID = "${s}" x-show='showmore' x-transition.duration.200ms>
                                                        <div class="qna-answers"><div class="ans-prefix">A:</div><div class="ans-content">${a}</div></div>
                                                        <div class="qna-author-container">
                                                            <div class="qna-author">By&nbsp${t}</div>
                                                            <div class="qna-author-timestamp">${DAVE_SETTINGS.print_timestamp(o,"",!0)}</div>
                                                        </div>
                                                    </div>
                                                `),1==d&&$(".qna-body-container").find(`[data-quesID='${s}']`).find(".qna-see-more").remove(),i++}),e=e||400,djQ(".qna-body-container").animate({scrollTop:djQ(".qna-body-container").prop("scrollHeight")},e),delete r[t]}))}})},videoPlaying(),video.on("pause",function(){v(!0)})}),DAVE_SETTINGS&&(DAVE_SETTINGS.SPEECH_SEVER?(StreamingSpeech.startRecordingButton=djQ("button#start-recording")[0],StreamingSpeech.stopRecordingButton=djQ("button#stop-recording")[0],navigator.permissions.query({name:"microphone"}).then(function(e){"granted"===e.state?(DAVE_SETTINGS.micAccess=!0,console.log("Already We Have MIC")):(DAVE_SETTINGS.micAccess=!1,console.log("Already We Don't have mic"))}),s("block"),djQ(".qna-mic-button").on("click",function(e){video.pause(),djQ("#communityQuesInput").val(""),e.stopPropagation(),DAVE_SETTINGS.micAccess||navigator.mediaDevices.getUserMedia({audio:!0}).then(function(e){DAVE_SETTINGS.micAccess=!0,console.log("Mic Access is provided")}).catch(function(e){console.log("No mic access is provided")}),DAVE_SETTINGS.micAccess?(temp_holder.quesStartTime=video.currentTime(),s("none","block"),StreamingSpeech.onTranscriptionAvailable=function(t){if(t.final_text){let e=JSON.stringify(t.final_text);e=e.slice(1,-1),djQ("#communityQuesInput").val(e),console.log(e),console.log("Voice Question Raised at : "+temp_holder.quesStartTime)}else{let e=JSON.stringify(t.rec_text);e=e.slice(1,-1),djQ("#communityQuesInput").val(djQ("#communityQuesInput").val()+" "+e)}},StreamingSpeech.onStreamingResultsAvailable=function(t){if(!t.final_text){let e=JSON.stringify(t.rec_text);e=e.slice(1,-1),djQ("#communityQuesInput").val(djQ("#communityQuesInput").val()+" "+e)}},StreamingSpeech.onError=function(e){console.log(e),console.log("^^^Speech onError is generated")},StreamingSpeech.onSocketDisConnect=function(e){s("none","none")}):(s("block","none"),console.log("permission is denied"),djQ("button#start-recording").attr("title","You have dis-allowed the microphone. Please go to settings to allow microphone for this website")),djQ("#stop-recording").on("click",function(){s("block","none")}),StreamingSpeech.initSocket()})):(console.log("Speech Server not set."),djQ(".qna-mic-button").css("display","none"))),djQ(document).on("click",".input-answerSubmit",function(){var e=new Date,t=djQ(this).closest(".qna-box").attr("data-quesid"),e=DAVE_SETTINGS.print_timestamp(e,"",!0),a=djQ(this).prev().val();djQ(this).prev().val(""),0<a.length?(DAVE_IV_SETTINGS.post_forum_answer(t,a,o.video_id,{},function(e){console.log(e)},function(e){console.log(JSON.stringify(e))}),djQ(".answerError").remove(),djQ(this).parent().next().prepend(`
                    <div class="qna-answers-block">
                        <div class="qna-answers">A:&nbsp;<span>${a}</span></div>
                        <div class="qna-author-container">
                            <div class="qna-author">By <span>Aman</span></div>
                            <div class="qna-author-timestamp">${e}</div>
                        </div>
                    </div>
                    `)):djQ(this).parent().prepend('<p class="answerError err_txt">Please enter your answer</p>')}),djQ(document).on("click",".review-button.r_no",function(){chatFuncExe=!1,console.log(chatFuncExe+" <= no")}),djQ(document).on("click","#askExpert",function(){chatFuncExe=!0,"unknownResponse"==djQ(this).attr("data-responseBtnType")&&(temp_holder.quesStartTime=secToHms(temp_holder.quesStartTime),djQ(".unknownReviewFormHolder").remove(),djQ(".qna-body-container").prepend(`
                        <div class="qna-box" x-data="{showmore : false, ansButton: false}">
                            <ol>
                                <li class="qna-video-timestamp">${temp_holder.quesStartTime}</li>
                                <li class="qna-question"><strong class="qna-ques-symbol">Q:</strong><strong class="qna-ques-frame">${temp_holder.quesToPost}</strong></li>
                                <li class="qna-answerButton" @click="ansButton = !ansButton, alp_pauseVideo(ansButton)">Answer this quetion</li>
                                <li class="qna-answerInput-container" x-show="ansButton" x-transition.duration.150ms.scale.orign.top>
                                    <textarea minlength="2" type="text" name="qna-answerInput" class="qna-answerInput" placeholder="Please write your answer."></textarea>
                                    <input type="submit" value="Submit" class="input-answerSubmit">
                                </li>
                                <li class="qna-answers-container">
                                    
                                </li>
                            </ol>
                            <hr>
                        </div>
                    `)),DAVE_IV_SETTINGS.post_forum_question(temp_holder.quesToPost,o.video_id,!0,{timestamp:Number(temp_holder.quesStartTime)},function(e){console.log(e),l()},function(e){console.log(JSON.stringify(e))}),video.play()}),djQ(document).on("click","#askCommunity",function(){chatFuncExe=!0,"unknownResponse"==djQ(this).attr("data-responseBtnType")&&(temp_holder.quesStartTime=secToHms(temp_holder.quesStartTime),djQ(".unknownReviewFormHolder").remove(),djQ(".qna-body-container").prepend(`
                        <div class="qna-box" x-data="{showmore : false, ansButton: false}">
                            <ol>
                                <li class="qna-video-timestamp">${temp_holder.quesStartTime}</li>
                                <li class="qna-question"><strong class="qna-ques-symbol">Q:</strong><strong class="qna-ques-frame">${temp_holder.quesToPost}</strong></li>
                                <li class="qna-answerButton" @click="ansButton = !ansButton, alp_pauseVideo(ansButton)">Answer this quetion</li>
                                <li class="qna-answerInput-container" x-show="ansButton" x-transition.duration.150ms.scale.orign.top>
                                    <textarea minlength="2" type="text" name="qna-answerInput" class="qna-answerInput" placeholder="Please write your answer."></textarea>
                                    <input type="submit" value="Submit" class="input-answerSubmit">
                                </li>
                                <li class="qna-answers-container">
                                    
                                </li>
                            </ol>
                            <hr>
                        </div>
                    `)),DAVE_IV_SETTINGS.post_forum_question(temp_holder.quesToPost,o.video_id,!1,{timestamp:Number(temp_holder.quesStartTime)},function(e){console.log(e),l()},function(e){console.log(JSON.stringify(e))}),video.play()}),djQ("input.qna-input").on("keyup",function(e){}),djQ(document).on("click",".qna-input-submit",function(){postQuestion&&(temp_holder.quesPostTime=video.currentTime(),chatFuncExe=!0,l(),"normal"==temp_holder.chatType?DAVE_SETTINGS.chat({customer_response:temp_holder.quesToPost,query_type:"type"},function(e){console.log(e),p(e.placeholder,!0)},function(e){console.log(e)},function(e){console.log(e),console.log("_____________UNKNOWN_________________"),p(e.placeholder,!1),djQ("#dave-videoPlayer").append(`
                                    <div class="unknownReviewFormHolder" x-data="reviewAnswerFront()">
                                        <p>Please Try Asking Our Experts or Community</p>
                                        <div class="review-button-holder">
                                            <button class="review-button a_expert" data-responseBtnType = "unknownResponse" id='askExpert' @click="xPostForum()">Ask Expert</button>
                                            <button class="review-button a_community" data-responseBtnType = "unknownResponse" id='askCommunity' @click="xPostForum()">Ask Community</button>
                                        </div>
                                    </div>
                                `)}):(v(!1),chatFuncExe=!0))}),djQ(document).on("keyup","#communityQuesInput",function(){var e=djQ(this).val(),t=e.length;djQ(document).find(".review-answer").remove(),1==t&&(temp_holder.quesStartTime=video.currentTime(),console.log(temp_holder.quesStartTime)),chatFuncExe&&(temp_holder.quesToPost=e,console.log("CHAT func exe")),postQuestion=0<t}),djQ(document).on("fullscreenchange",video,function(){let e=!0,t=!0,a=djQ(document).find(".s-half"),o=djQ(document).find(".bottom-f-half"),s=void 0;function n(){djQ("video").bind("timeupdate",function(){djQ(document).on("mousemove","#dave-videoPlayer",function(){console.log("mouse moving"),e&&t&&(a.addClass("s-halfFullScreenShow"),o.addClass("bottom-f-halfFullScreenShow"),e=!1,t=!1,s=setTimeout(function(){e=!0,t=!0,a.removeClass("s-halfFullScreenShow"),o.removeClass("bottom-f-halfFullScreenShow")},3e3))})})}video.isFullscreen()?(a.appendTo("#dave-videoPlayer"),a.addClass("s-halfFullScreen"),a.css("height","83vh"),o.appendTo("#dave-videoPlayer"),o.addClass("bottom-f-halfFullScreen"),n(),djQ("video").on("pause",function(){clearTimeout(s),djQ("video").unbind("timeupdate"),a.addClass("s-halfFullScreenShow"),o.addClass("bottom-f-halfFullScreenShow")}),djQ("video").on("play",function(){e=!0,n(),a.removeClass("s-halfFullScreenShow"),o.removeClass("bottom-f-halfFullScreenShow")})):(djQ("video").unbind("timeupdate"),djQ(document).off("mousemove","#dave-videoPlayer"),videoPlaying(),a.appendTo(".IV_conatiner"),a.removeClass("s-halfFullScreen"),o.appendTo(".f-half"),o.removeClass("bottom-f-halfFullScreen"),l())})})}),djQ(document).on("click",".dave-popup-close",function(){djQ(".dave-iv-popup").remove()}),djQ(document).on("click",".dave-forum-minimize",function(){djQ(".s-half").addClass("hide-shalf"),djQ(".qna-container").addClass("hide-qna-container"),djQ(".f-half").width("100%"),djQ(".maxForum").addClass("showMaxForum"),u("min")}),djQ(document).on("click",".showMaxForum",function(){djQ(".s-half").removeClass("hide-shalf"),djQ(".qna-container").removeClass("hide-qna-container"),djQ(".f-half").width("75%"),djQ(".maxForum").removeClass("showMaxForum"),u("max")}),djQ(document).on("click",".webTags-s-half, .webTags-bottom-f-half",function(){clearTimeout(webTagTimeOut)}),djQ(document).on("click",".webTags-popup",function(){g()}),djQ(document).on("click","[data-daveWebTags]",function(){var e=djQ(document).find(".bottom-f-half").height()+40;console.log(e),djQ(".dave-iv-popup").append(`
        <div class='daveAvatarIVCont daveWebTagAvatar' style="position: fixed;bottom: ${e}px; display: block;">
            <canvas id='dave-canvas'>
                <p>this is p</p>
            </canvas>
        </div>
        `),m("0px","0px","200px")}),djQ("[data-daveWebTags]").hover(function(){var e,t,a;e="body",t="webTags-s-half",a="webTags-bottom-f-half","undefined"==typeof webTagTimeOut&&(console.log("added"),djQ(e).append('<div class="dave-iv-popup webTags-popup"></div>'),djQ(e).append(h),djQ(".s-half").addClass(t).attr("x-data","ivBody()"),djQ(e).append(f),djQ(".bottom-f-half").addClass(a).attr("x-data","ivBody()"))},function(){"undefined"==typeof webTagTimeOut&&(webTagTimeOut=setTimeout(function(){console.log("removed"),g()},5e3))})});