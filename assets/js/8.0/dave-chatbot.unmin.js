function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * jQuery JavaScript Library v3.5.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2020-05-04T22:49Z
 */
(function (global, factory) {
  "use strict";

  if ((typeof module === "undefined" ? "undefined" : _typeof(module)) === "object" && _typeof(module.exports) === "object") {
    // For CommonJS and CommonJS-like environments where a proper `window`
    // is present, execute the factory and get jQuery.
    // For environments that do not have a `window` with a `document`
    // (such as Node.js), expose a factory as module.exports.
    // This accentuates the need for the creation of a real `window`.
    // e.g. var jQuery = require("jquery")(window);
    // See ticket #14549 for more info.
    module.exports = global.document ? factory(global, true) : function (w) {
      if (!w.document) {
        throw new Error("jQuery requires a window with a document");
      }

      return factory(w);
    };
  } else {
    factory(global);
  } // Pass this if window is not defined yet

})(typeof window !== "undefined" ? window : this, function (window, noGlobal) {
  // Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
  // throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
  // arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
  // enough that all such attempts are guarded in a try block.
  "use strict";

  var arr = [];
  var getProto = Object.getPrototypeOf;
  var _slice = arr.slice;
  var flat = arr.flat ? function (array) {
    return arr.flat.call(array);
  } : function (array) {
    return arr.concat.apply([], array);
  };
  var push = arr.push;
  var indexOf = arr.indexOf;
  var class2type = {};
  var toString = class2type.toString;
  var hasOwn = class2type.hasOwnProperty;
  var fnToString = hasOwn.toString;
  var ObjectFunctionString = fnToString.call(Object);
  var support = {};

  var isFunction = function isFunction(obj) {
    // Support: Chrome <=57, Firefox <=52
    // In some browsers, typeof returns "function" for HTML <object> elements
    // (i.e., `typeof document.createElement( "object" ) === "function"`).
    // We don't want to classify *any* DOM node as a function.
    return typeof obj === "function" && typeof obj.nodeType !== "number";
  };

  var isWindow = function isWindow(obj) {
    return obj != null && obj === obj.window;
  };

  var document = window.document;
  var preservedScriptAttributes = {
    type: true,
    src: true,
    nonce: true,
    noModule: true
  };

  function DOMEval(code, node, doc) {
    doc = doc || document;
    var i,
        val,
        script = doc.createElement("script");
    script.text = code;

    if (node) {
      for (i in preservedScriptAttributes) {
        // Support: Firefox 64+, Edge 18+
        // Some browsers don't support the "nonce" property on scripts.
        // On the other hand, just using `getAttribute` is not enough as
        // the `nonce` attribute is reset to an empty string whenever it
        // becomes browsing-context connected.
        // See https://github.com/whatwg/html/issues/2369
        // See https://html.spec.whatwg.org/#nonce-attributes
        // The `node.getAttribute` check was added for the sake of
        // `jQuery.globalEval` so that it can fake a nonce-containing node
        // via an object.
        val = node[i] || node.getAttribute && node.getAttribute(i);

        if (val) {
          script.setAttribute(i, val);
        }
      }
    }

    doc.head.appendChild(script).parentNode.removeChild(script);
  }

  function toType(obj) {
    if (obj == null) {
      return obj + "";
    } // Support: Android <=2.3 only (functionish RegExp)


    return _typeof(obj) === "object" || typeof obj === "function" ? class2type[toString.call(obj)] || "object" : _typeof(obj);
  }
  /* global Symbol */
  // Defining this global in .eslintrc.json would create a danger of using the global
  // unguarded in another place, it seems safer to define global only for this module


  var version = "3.5.1",
      // Define a local copy of jQuery
  djQ = function djQ(selector, context) {
    // The jQuery object is actually just the init constructor 'enhanced'
    // Need init if jQuery is called (just allow error to be thrown if not included)
    return new djQ.fn.init(selector, context);
  };

  djQ.fn = djQ.prototype = {
    // The current version of jQuery being used
    jquery: version,
    constructor: djQ,
    // The default length of a jQuery object is 0
    length: 0,
    toArray: function toArray() {
      return _slice.call(this);
    },
    // Get the Nth element in the matched element set OR
    // Get the whole matched element set as a clean array
    get: function get(num) {
      // Return all the elements in a clean array
      if (num == null) {
        return _slice.call(this);
      } // Return just the one element from the set


      return num < 0 ? this[num + this.length] : this[num];
    },
    // Take an array of elements and push it onto the stack
    // (returning the new matched element set)
    pushStack: function pushStack(elems) {
      // Build a new jQuery matched element set
      var ret = djQ.merge(this.constructor(), elems); // Add the old object onto the stack (as a reference)

      ret.prevObject = this; // Return the newly-formed element set

      return ret;
    },
    // Execute a callback for every element in the matched set.
    each: function each(callback) {
      return djQ.each(this, callback);
    },
    map: function map(callback) {
      return this.pushStack(djQ.map(this, function (elem, i) {
        return callback.call(elem, i, elem);
      }));
    },
    slice: function slice() {
      return this.pushStack(_slice.apply(this, arguments));
    },
    first: function first() {
      return this.eq(0);
    },
    last: function last() {
      return this.eq(-1);
    },
    even: function even() {
      return this.pushStack(djQ.grep(this, function (_elem, i) {
        return (i + 1) % 2;
      }));
    },
    odd: function odd() {
      return this.pushStack(djQ.grep(this, function (_elem, i) {
        return i % 2;
      }));
    },
    eq: function eq(i) {
      var len = this.length,
          j = +i + (i < 0 ? len : 0);
      return this.pushStack(j >= 0 && j < len ? [this[j]] : []);
    },
    end: function end() {
      return this.prevObject || this.constructor();
    },
    // For internal use only.
    // Behaves like an Array's method, not like a jQuery method.
    push: push,
    sort: arr.sort,
    splice: arr.splice
  };

  djQ.extend = djQ.fn.extend = function () {
    var options,
        name,
        src,
        copy,
        copyIsArray,
        clone,
        target = arguments[0] || {},
        i = 1,
        length = arguments.length,
        deep = false; // Handle a deep copy situation

    if (typeof target === "boolean") {
      deep = target; // Skip the boolean and the target

      target = arguments[i] || {};
      i++;
    } // Handle case when target is a string or something (possible in deep copy)


    if (_typeof(target) !== "object" && !isFunction(target)) {
      target = {};
    } // Extend jQuery itself if only one argument is passed


    if (i === length) {
      target = this;
      i--;
    }

    for (; i < length; i++) {
      // Only deal with non-null/undefined values
      if ((options = arguments[i]) != null) {
        // Extend the base object
        for (name in options) {
          copy = options[name]; // Prevent Object.prototype pollution
          // Prevent never-ending loop

          if (name === "__proto__" || target === copy) {
            continue;
          } // Recurse if we're merging plain objects or arrays


          if (deep && copy && (djQ.isPlainObject(copy) || (copyIsArray = Array.isArray(copy)))) {
            src = target[name]; // Ensure proper type for the source value

            if (copyIsArray && !Array.isArray(src)) {
              clone = [];
            } else if (!copyIsArray && !djQ.isPlainObject(src)) {
              clone = {};
            } else {
              clone = src;
            }

            copyIsArray = false; // Never move original objects, clone them

            target[name] = djQ.extend(deep, clone, copy); // Don't bring in undefined values
          } else if (copy !== undefined) {
            target[name] = copy;
          }
        }
      }
    } // Return the modified object


    return target;
  };

  djQ.extend({
    // Unique for each copy of jQuery on the page
    expando: "djQ" + (version + Math.random()).replace(/\D/g, ""),
    // Assume jQuery is ready without the ready module
    isReady: true,
    error: function error(msg) {
      throw new Error(msg);
    },
    noop: function noop() {},
    isPlainObject: function isPlainObject(obj) {
      var proto, Ctor; // Detect obvious negatives
      // Use toString instead of jQuery.type to catch host objects

      if (!obj || toString.call(obj) !== "[object Object]") {
        return false;
      }

      proto = getProto(obj); // Objects with no prototype (e.g., `Object.create( null )`) are plain

      if (!proto) {
        return true;
      } // Objects with prototype are plain iff they were constructed by a global Object function


      Ctor = hasOwn.call(proto, "constructor") && proto.constructor;
      return typeof Ctor === "function" && fnToString.call(Ctor) === ObjectFunctionString;
    },
    isEmptyObject: function isEmptyObject(obj) {
      var name;

      for (name in obj) {
        return false;
      }

      return true;
    },
    // Evaluates a script in a provided context; falls back to the global one
    // if not specified.
    globalEval: function globalEval(code, options, doc) {
      DOMEval(code, {
        nonce: options && options.nonce
      }, doc);
    },
    each: function each(obj, callback) {
      var length,
          i = 0;

      if (isArrayLike(obj)) {
        length = obj.length;

        for (; i < length; i++) {
          if (callback.call(obj[i], i, obj[i]) === false) {
            break;
          }
        }
      } else {
        for (i in obj) {
          if (callback.call(obj[i], i, obj[i]) === false) {
            break;
          }
        }
      }

      return obj;
    },
    // results is for internal usage only
    makeArray: function makeArray(arr, results) {
      var ret = results || [];

      if (arr != null) {
        if (isArrayLike(Object(arr))) {
          djQ.merge(ret, typeof arr === "string" ? [arr] : arr);
        } else {
          push.call(ret, arr);
        }
      }

      return ret;
    },
    inArray: function inArray(elem, arr, i) {
      return arr == null ? -1 : indexOf.call(arr, elem, i);
    },
    // Support: Android <=4.0 only, PhantomJS 1 only
    // push.apply(_, arraylike) throws on ancient WebKit
    merge: function merge(first, second) {
      var len = +second.length,
          j = 0,
          i = first.length;

      for (; j < len; j++) {
        first[i++] = second[j];
      }

      first.length = i;
      return first;
    },
    grep: function grep(elems, callback, invert) {
      var callbackInverse,
          matches = [],
          i = 0,
          length = elems.length,
          callbackExpect = !invert; // Go through the array, only saving the items
      // that pass the validator function

      for (; i < length; i++) {
        callbackInverse = !callback(elems[i], i);

        if (callbackInverse !== callbackExpect) {
          matches.push(elems[i]);
        }
      }

      return matches;
    },
    // arg is for internal usage only
    map: function map(elems, callback, arg) {
      var length,
          value,
          i = 0,
          ret = []; // Go through the array, translating each of the items to their new values

      if (isArrayLike(elems)) {
        length = elems.length;

        for (; i < length; i++) {
          value = callback(elems[i], i, arg);

          if (value != null) {
            ret.push(value);
          }
        } // Go through every key on the object,

      } else {
        for (i in elems) {
          value = callback(elems[i], i, arg);

          if (value != null) {
            ret.push(value);
          }
        }
      } // Flatten any nested arrays


      return flat(ret);
    },
    // A global GUID counter for objects
    guid: 1,
    // jQuery.support is not used in Core but other projects attach their
    // properties to it so it needs to exist.
    support: support
  });

  if (typeof Symbol === "function") {
    djQ.fn[Symbol.iterator] = arr[Symbol.iterator];
  } // Populate the class2type map


  djQ.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (_i, name) {
    class2type["[object " + name + "]"] = name.toLowerCase();
  });

  function isArrayLike(obj) {
    // Support: real iOS 8.2 only (not reproducible in simulator)
    // `in` check used to prevent JIT error (gh-2145)
    // hasOwn isn't used here due to false negatives
    // regarding Nodelist length in IE
    var length = !!obj && "length" in obj && obj.length,
        type = toType(obj);

    if (isFunction(obj) || isWindow(obj)) {
      return false;
    }

    return type === "array" || length === 0 || typeof length === "number" && length > 0 && length - 1 in obj;
  }

  var Sizzle =
  /*!
   * Sizzle CSS Selector Engine v2.3.5
   * https://sizzlejs.com/
   *
   * Copyright JS Foundation and other contributors
   * Released under the MIT license
   * https://js.foundation/
   *
   * Date: 2020-03-14
   */
  function (window) {
    var i,
        support,
        Expr,
        getText,
        isXML,
        tokenize,
        compile,
        select,
        outermostContext,
        sortInput,
        hasDuplicate,
        // Local document vars
    setDocument,
        document,
        docElem,
        documentIsHTML,
        rbuggyQSA,
        rbuggyMatches,
        matches,
        contains,
        // Instance-specific data
    expando = "sizzle" + 1 * new Date(),
        preferredDoc = window.document,
        dirruns = 0,
        done = 0,
        classCache = createCache(),
        tokenCache = createCache(),
        compilerCache = createCache(),
        nonnativeSelectorCache = createCache(),
        sortOrder = function sortOrder(a, b) {
      if (a === b) {
        hasDuplicate = true;
      }

      return 0;
    },
        // Instance methods
    hasOwn = {}.hasOwnProperty,
        arr = [],
        pop = arr.pop,
        pushNative = arr.push,
        push = arr.push,
        slice = arr.slice,
        // Use a stripped-down indexOf as it's faster than native
    // https://jsperf.com/thor-indexof-vs-for/5
    indexOf = function indexOf(list, elem) {
      var i = 0,
          len = list.length;

      for (; i < len; i++) {
        if (list[i] === elem) {
          return i;
        }
      }

      return -1;
    },
        booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|" + "ismap|loop|multiple|open|readonly|required|scoped",
        // Regular expressions
    // http://www.w3.org/TR/css3-selectors/#whitespace
    whitespace = "[\\x20\\t\\r\\n\\f]",
        // https://www.w3.org/TR/css-syntax-3/#ident-token-diagram
    identifier = "(?:\\\\[\\da-fA-F]{1,6}" + whitespace + "?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",
        // Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
    attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace + // Operator (capture 2)
    "*([*^$|!~]?=)" + whitespace + // "Attribute values must be CSS identifiers [capture 5]
    // or strings [capture 3 or capture 4]"
    "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace + "*\\]",
        pseudos = ":(" + identifier + ")(?:\\((" + // To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
    // 1. quoted (capture 3; capture 4 or capture 5)
    "('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" + // 2. simple (capture 6)
    "((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" + // 3. anything else (capture 2)
    ".*" + ")\\)|)",
        // Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
    rwhitespace = new RegExp(whitespace + "+", "g"),
        rtrim = new RegExp("^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g"),
        rcomma = new RegExp("^" + whitespace + "*," + whitespace + "*"),
        rcombinators = new RegExp("^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*"),
        rdescend = new RegExp(whitespace + "|>"),
        rpseudo = new RegExp(pseudos),
        ridentifier = new RegExp("^" + identifier + "$"),
        matchExpr = {
      "ID": new RegExp("^#(" + identifier + ")"),
      "CLASS": new RegExp("^\\.(" + identifier + ")"),
      "TAG": new RegExp("^(" + identifier + "|[*])"),
      "ATTR": new RegExp("^" + attributes),
      "PSEUDO": new RegExp("^" + pseudos),
      "CHILD": new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace + "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace + "*(\\d+)|))" + whitespace + "*\\)|)", "i"),
      "bool": new RegExp("^(?:" + booleans + ")$", "i"),
      // For use in libraries implementing .is()
      // We use this for POS matching in `select`
      "needsContext": new RegExp("^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i")
    },
        rhtml = /HTML$/i,
        rinputs = /^(?:input|select|textarea|button)$/i,
        rheader = /^h\d$/i,
        rnative = /^[^{]+\{\s*\[native \w/,
        // Easily-parseable/retrievable ID or TAG or CLASS selectors
    rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
        rsibling = /[+~]/,
        // CSS escapes
    // http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
    runescape = new RegExp("\\\\[\\da-fA-F]{1,6}" + whitespace + "?|\\\\([^\\r\\n\\f])", "g"),
        funescape = function funescape(escape, nonHex) {
      var high = "0x" + escape.slice(1) - 0x10000;
      return nonHex ? // Strip the backslash prefix from a non-hex escape sequence
      nonHex : // Replace a hexadecimal escape sequence with the encoded Unicode code point
      // Support: IE <=11+
      // For values outside the Basic Multilingual Plane (BMP), manually construct a
      // surrogate pair
      high < 0 ? String.fromCharCode(high + 0x10000) : String.fromCharCode(high >> 10 | 0xD800, high & 0x3FF | 0xDC00);
    },
        // CSS string/identifier serialization
    // https://drafts.csswg.org/cssom/#common-serializing-idioms
    rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
        fcssescape = function fcssescape(ch, asCodePoint) {
      if (asCodePoint) {
        // U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
        if (ch === "\0") {
          return "\uFFFD";
        } // Control characters and (dependent upon position) numbers get escaped as code points


        return ch.slice(0, -1) + "\\" + ch.charCodeAt(ch.length - 1).toString(16) + " ";
      } // Other potentially-special ASCII characters get backslash-escaped


      return "\\" + ch;
    },
        // Used for iframes
    // See setDocument()
    // Removing the function wrapper causes a "Permission Denied"
    // error in IE
    unloadHandler = function unloadHandler() {
      setDocument();
    },
        inDisabledFieldset = addCombinator(function (elem) {
      return elem.disabled === true && elem.nodeName.toLowerCase() === "fieldset";
    }, {
      dir: "parentNode",
      next: "legend"
    }); // Optimize for push.apply( _, NodeList )


    try {
      push.apply(arr = slice.call(preferredDoc.childNodes), preferredDoc.childNodes); // Support: Android<4.0
      // Detect silently failing push.apply
      // eslint-disable-next-line no-unused-expressions

      arr[preferredDoc.childNodes.length].nodeType;
    } catch (e) {
      push = {
        apply: arr.length ? // Leverage slice if possible
        function (target, els) {
          pushNative.apply(target, slice.call(els));
        } : // Support: IE<9
        // Otherwise append directly
        function (target, els) {
          var j = target.length,
              i = 0; // Can't trust NodeList.length

          while (target[j++] = els[i++]) {}

          target.length = j - 1;
        }
      };
    }

    function Sizzle(selector, context, results, seed) {
      var m,
          i,
          elem,
          nid,
          match,
          groups,
          newSelector,
          newContext = context && context.ownerDocument,
          // nodeType defaults to 9, since context defaults to document
      nodeType = context ? context.nodeType : 9;
      results = results || []; // Return early from calls with invalid selector or context

      if (typeof selector !== "string" || !selector || nodeType !== 1 && nodeType !== 9 && nodeType !== 11) {
        return results;
      } // Try to shortcut find operations (as opposed to filters) in HTML documents


      if (!seed) {
        setDocument(context);
        context = context || document;

        if (documentIsHTML) {
          // If the selector is sufficiently simple, try using a "get*By*" DOM method
          // (excepting DocumentFragment context, where the methods don't exist)
          if (nodeType !== 11 && (match = rquickExpr.exec(selector))) {
            // ID selector
            if (m = match[1]) {
              // Document context
              if (nodeType === 9) {
                if (elem = context.getElementById(m)) {
                  // Support: IE, Opera, Webkit
                  // TODO: identify versions
                  // getElementById can match elements by name instead of ID
                  if (elem.id === m) {
                    results.push(elem);
                    return results;
                  }
                } else {
                  return results;
                } // Element context

              } else {
                // Support: IE, Opera, Webkit
                // TODO: identify versions
                // getElementById can match elements by name instead of ID
                if (newContext && (elem = newContext.getElementById(m)) && contains(context, elem) && elem.id === m) {
                  results.push(elem);
                  return results;
                }
              } // Type selector

            } else if (match[2]) {
              push.apply(results, context.getElementsByTagName(selector));
              return results; // Class selector
            } else if ((m = match[3]) && support.getElementsByClassName && context.getElementsByClassName) {
              push.apply(results, context.getElementsByClassName(m));
              return results;
            }
          } // Take advantage of querySelectorAll


          if (support.qsa && !nonnativeSelectorCache[selector + " "] && (!rbuggyQSA || !rbuggyQSA.test(selector)) && ( // Support: IE 8 only
          // Exclude object elements
          nodeType !== 1 || context.nodeName.toLowerCase() !== "object")) {
            newSelector = selector;
            newContext = context; // qSA considers elements outside a scoping root when evaluating child or
            // descendant combinators, which is not what we want.
            // In such cases, we work around the behavior by prefixing every selector in the
            // list with an ID selector referencing the scope context.
            // The technique has to be used as well when a leading combinator is used
            // as such selectors are not recognized by querySelectorAll.
            // Thanks to Andrew Dupont for this technique.

            if (nodeType === 1 && (rdescend.test(selector) || rcombinators.test(selector))) {
              // Expand context for sibling selectors
              newContext = rsibling.test(selector) && testContext(context.parentNode) || context; // We can use :scope instead of the ID hack if the browser
              // supports it & if we're not changing the context.

              if (newContext !== context || !support.scope) {
                // Capture the context ID, setting it first if necessary
                if (nid = context.getAttribute("id")) {
                  nid = nid.replace(rcssescape, fcssescape);
                } else {
                  context.setAttribute("id", nid = expando);
                }
              } // Prefix every selector in the list


              groups = tokenize(selector);
              i = groups.length;

              while (i--) {
                groups[i] = (nid ? "#" + nid : ":scope") + " " + toSelector(groups[i]);
              }

              newSelector = groups.join(",");
            }

            try {
              push.apply(results, newContext.querySelectorAll(newSelector));
              return results;
            } catch (qsaError) {
              nonnativeSelectorCache(selector, true);
            } finally {
              if (nid === expando) {
                context.removeAttribute("id");
              }
            }
          }
        }
      } // All others


      return select(selector.replace(rtrim, "$1"), context, results, seed);
    }
    /**
     * Create key-value caches of limited size
     * @returns {function(string, object)} Returns the Object data after storing it on itself with
     *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
     *	deleting the oldest entry
     */


    function createCache() {
      var keys = [];

      function cache(key, value) {
        // Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
        if (keys.push(key + " ") > Expr.cacheLength) {
          // Only keep the most recent entries
          delete cache[keys.shift()];
        }

        return cache[key + " "] = value;
      }

      return cache;
    }
    /**
     * Mark a function for special use by Sizzle
     * @param {Function} fn The function to mark
     */


    function markFunction(fn) {
      fn[expando] = true;
      return fn;
    }
    /**
     * Support testing using an element
     * @param {Function} fn Passed the created element and returns a boolean result
     */


    function assert(fn) {
      var el = document.createElement("fieldset");

      try {
        return !!fn(el);
      } catch (e) {
        return false;
      } finally {
        // Remove from its parent by default
        if (el.parentNode) {
          el.parentNode.removeChild(el);
        } // release memory in IE


        el = null;
      }
    }
    /**
     * Adds the same handler for all of the specified attrs
     * @param {String} attrs Pipe-separated list of attributes
     * @param {Function} handler The method that will be applied
     */


    function addHandle(attrs, handler) {
      var arr = attrs.split("|"),
          i = arr.length;

      while (i--) {
        Expr.attrHandle[arr[i]] = handler;
      }
    }
    /**
     * Checks document order of two siblings
     * @param {Element} a
     * @param {Element} b
     * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
     */


    function siblingCheck(a, b) {
      var cur = b && a,
          diff = cur && a.nodeType === 1 && b.nodeType === 1 && a.sourceIndex - b.sourceIndex; // Use IE sourceIndex if available on both nodes

      if (diff) {
        return diff;
      } // Check if b follows a


      if (cur) {
        while (cur = cur.nextSibling) {
          if (cur === b) {
            return -1;
          }
        }
      }

      return a ? 1 : -1;
    }
    /**
     * Returns a function to use in pseudos for input types
     * @param {String} type
     */


    function createInputPseudo(type) {
      return function (elem) {
        var name = elem.nodeName.toLowerCase();
        return name === "input" && elem.type === type;
      };
    }
    /**
     * Returns a function to use in pseudos for buttons
     * @param {String} type
     */


    function createButtonPseudo(type) {
      return function (elem) {
        var name = elem.nodeName.toLowerCase();
        return (name === "input" || name === "button") && elem.type === type;
      };
    }
    /**
     * Returns a function to use in pseudos for :enabled/:disabled
     * @param {Boolean} disabled true for :disabled; false for :enabled
     */


    function createDisabledPseudo(disabled) {
      // Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
      return function (elem) {
        // Only certain elements can match :enabled or :disabled
        // https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
        // https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
        if ("form" in elem) {
          // Check for inherited disabledness on relevant non-disabled elements:
          // * listed form-associated elements in a disabled fieldset
          //   https://html.spec.whatwg.org/multipage/forms.html#category-listed
          //   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
          // * option elements in a disabled optgroup
          //   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
          // All such elements have a "form" property.
          if (elem.parentNode && elem.disabled === false) {
            // Option elements defer to a parent optgroup if present
            if ("label" in elem) {
              if ("label" in elem.parentNode) {
                return elem.parentNode.disabled === disabled;
              } else {
                return elem.disabled === disabled;
              }
            } // Support: IE 6 - 11
            // Use the isDisabled shortcut property to check for disabled fieldset ancestors


            return elem.isDisabled === disabled || // Where there is no isDisabled, check manually

            /* jshint -W018 */
            elem.isDisabled !== !disabled && inDisabledFieldset(elem) === disabled;
          }

          return elem.disabled === disabled; // Try to winnow out elements that can't be disabled before trusting the disabled property.
          // Some victims get caught in our net (label, legend, menu, track), but it shouldn't
          // even exist on them, let alone have a boolean value.
        } else if ("label" in elem) {
          return elem.disabled === disabled;
        } // Remaining elements are neither :enabled nor :disabled


        return false;
      };
    }
    /**
     * Returns a function to use in pseudos for positionals
     * @param {Function} fn
     */


    function createPositionalPseudo(fn) {
      return markFunction(function (argument) {
        argument = +argument;
        return markFunction(function (seed, matches) {
          var j,
              matchIndexes = fn([], seed.length, argument),
              i = matchIndexes.length; // Match elements found at the specified indexes

          while (i--) {
            if (seed[j = matchIndexes[i]]) {
              seed[j] = !(matches[j] = seed[j]);
            }
          }
        });
      });
    }
    /**
     * Checks a node for validity as a Sizzle context
     * @param {Element|Object=} context
     * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
     */


    function testContext(context) {
      return context && typeof context.getElementsByTagName !== "undefined" && context;
    } // Expose support vars for convenience


    support = Sizzle.support = {};
    /**
     * Detects XML nodes
     * @param {Element|Object} elem An element or a document
     * @returns {Boolean} True iff elem is a non-HTML XML node
     */

    isXML = Sizzle.isXML = function (elem) {
      var namespace = elem.namespaceURI,
          docElem = (elem.ownerDocument || elem).documentElement; // Support: IE <=8
      // Assume HTML when documentElement doesn't yet exist, such as inside loading iframes
      // https://bugs.jquery.com/ticket/4833

      return !rhtml.test(namespace || docElem && docElem.nodeName || "HTML");
    };
    /**
     * Sets document-related variables once based on the current document
     * @param {Element|Object} [doc] An element or document object to use to set the document
     * @returns {Object} Returns the current document
     */


    setDocument = Sizzle.setDocument = function (node) {
      var hasCompare,
          subWindow,
          doc = node ? node.ownerDocument || node : preferredDoc; // Return early if doc is invalid or already selected
      // Support: IE 11+, Edge 17 - 18+
      // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
      // two documents; shallow comparisons work.
      // eslint-disable-next-line eqeqeq

      if (doc == document || doc.nodeType !== 9 || !doc.documentElement) {
        return document;
      } // Update global variables


      document = doc;
      docElem = document.documentElement;
      documentIsHTML = !isXML(document); // Support: IE 9 - 11+, Edge 12 - 18+
      // Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
      // Support: IE 11+, Edge 17 - 18+
      // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
      // two documents; shallow comparisons work.
      // eslint-disable-next-line eqeqeq

      if (preferredDoc != document && (subWindow = document.defaultView) && subWindow.top !== subWindow) {
        // Support: IE 11, Edge
        if (subWindow.addEventListener) {
          subWindow.addEventListener("unload", unloadHandler, false); // Support: IE 9 - 10 only
        } else if (subWindow.attachEvent) {
          subWindow.attachEvent("onunload", unloadHandler);
        }
      } // Support: IE 8 - 11+, Edge 12 - 18+, Chrome <=16 - 25 only, Firefox <=3.6 - 31 only,
      // Safari 4 - 5 only, Opera <=11.6 - 12.x only
      // IE/Edge & older browsers don't support the :scope pseudo-class.
      // Support: Safari 6.0 only
      // Safari 6.0 supports :scope but it's an alias of :root there.


      support.scope = assert(function (el) {
        docElem.appendChild(el).appendChild(document.createElement("div"));
        return typeof el.querySelectorAll !== "undefined" && !el.querySelectorAll(":scope fieldset div").length;
      });
      /* Attributes
      ---------------------------------------------------------------------- */
      // Support: IE<8
      // Verify that getAttribute really returns attributes and not properties
      // (excepting IE8 booleans)

      support.attributes = assert(function (el) {
        el.className = "i";
        return !el.getAttribute("className");
      });
      /* getElement(s)By*
      ---------------------------------------------------------------------- */
      // Check if getElementsByTagName("*") returns only elements

      support.getElementsByTagName = assert(function (el) {
        el.appendChild(document.createComment(""));
        return !el.getElementsByTagName("*").length;
      }); // Support: IE<9

      support.getElementsByClassName = rnative.test(document.getElementsByClassName); // Support: IE<10
      // Check if getElementById returns elements by name
      // The broken getElementById methods don't pick up programmatically-set names,
      // so use a roundabout getElementsByName test

      support.getById = assert(function (el) {
        docElem.appendChild(el).id = expando;
        return !document.getElementsByName || !document.getElementsByName(expando).length;
      }); // ID filter and find

      if (support.getById) {
        Expr.filter["ID"] = function (id) {
          var attrId = id.replace(runescape, funescape);
          return function (elem) {
            return elem.getAttribute("id") === attrId;
          };
        };

        Expr.find["ID"] = function (id, context) {
          if (typeof context.getElementById !== "undefined" && documentIsHTML) {
            var elem = context.getElementById(id);
            return elem ? [elem] : [];
          }
        };
      } else {
        Expr.filter["ID"] = function (id) {
          var attrId = id.replace(runescape, funescape);
          return function (elem) {
            var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
            return node && node.value === attrId;
          };
        }; // Support: IE 6 - 7 only
        // getElementById is not reliable as a find shortcut


        Expr.find["ID"] = function (id, context) {
          if (typeof context.getElementById !== "undefined" && documentIsHTML) {
            var node,
                i,
                elems,
                elem = context.getElementById(id);

            if (elem) {
              // Verify the id attribute
              node = elem.getAttributeNode("id");

              if (node && node.value === id) {
                return [elem];
              } // Fall back on getElementsByName


              elems = context.getElementsByName(id);
              i = 0;

              while (elem = elems[i++]) {
                node = elem.getAttributeNode("id");

                if (node && node.value === id) {
                  return [elem];
                }
              }
            }

            return [];
          }
        };
      } // Tag


      Expr.find["TAG"] = support.getElementsByTagName ? function (tag, context) {
        if (typeof context.getElementsByTagName !== "undefined") {
          return context.getElementsByTagName(tag); // DocumentFragment nodes don't have gEBTN
        } else if (support.qsa) {
          return context.querySelectorAll(tag);
        }
      } : function (tag, context) {
        var elem,
            tmp = [],
            i = 0,
            // By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
        results = context.getElementsByTagName(tag); // Filter out possible comments

        if (tag === "*") {
          while (elem = results[i++]) {
            if (elem.nodeType === 1) {
              tmp.push(elem);
            }
          }

          return tmp;
        }

        return results;
      }; // Class

      Expr.find["CLASS"] = support.getElementsByClassName && function (className, context) {
        if (typeof context.getElementsByClassName !== "undefined" && documentIsHTML) {
          return context.getElementsByClassName(className);
        }
      };
      /* QSA/matchesSelector
      ---------------------------------------------------------------------- */
      // QSA and matchesSelector support
      // matchesSelector(:active) reports false when true (IE9/Opera 11.5)


      rbuggyMatches = []; // qSa(:focus) reports false when true (Chrome 21)
      // We allow this because of a bug in IE8/9 that throws an error
      // whenever `document.activeElement` is accessed on an iframe
      // So, we allow :focus to pass through QSA all the time to avoid the IE error
      // See https://bugs.jquery.com/ticket/13378

      rbuggyQSA = [];

      if (support.qsa = rnative.test(document.querySelectorAll)) {
        // Build QSA regex
        // Regex strategy adopted from Diego Perini
        assert(function (el) {
          var input; // Select is set to empty string on purpose
          // This is to test IE's treatment of not explicitly
          // setting a boolean content attribute,
          // since its presence should be enough
          // https://bugs.jquery.com/ticket/12359

          docElem.appendChild(el).innerHTML = "<a id='" + expando + "'></a>" + "<select id='" + expando + "-\r\\' msallowcapture=''>" + "<option selected=''></option></select>"; // Support: IE8, Opera 11-12.16
          // Nothing should be selected when empty strings follow ^= or $= or *=
          // The test attribute must be unknown in Opera but "safe" for WinRT
          // https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section

          if (el.querySelectorAll("[msallowcapture^='']").length) {
            rbuggyQSA.push("[*^$]=" + whitespace + "*(?:''|\"\")");
          } // Support: IE8
          // Boolean attributes and "value" are not treated correctly


          if (!el.querySelectorAll("[selected]").length) {
            rbuggyQSA.push("\\[" + whitespace + "*(?:value|" + booleans + ")");
          } // Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+


          if (!el.querySelectorAll("[id~=" + expando + "-]").length) {
            rbuggyQSA.push("~=");
          } // Support: IE 11+, Edge 15 - 18+
          // IE 11/Edge don't find elements on a `[name='']` query in some cases.
          // Adding a temporary attribute to the document before the selection works
          // around the issue.
          // Interestingly, IE 10 & older don't seem to have the issue.


          input = document.createElement("input");
          input.setAttribute("name", "");
          el.appendChild(input);

          if (!el.querySelectorAll("[name='']").length) {
            rbuggyQSA.push("\\[" + whitespace + "*name" + whitespace + "*=" + whitespace + "*(?:''|\"\")");
          } // Webkit/Opera - :checked should return selected option elements
          // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
          // IE8 throws error here and will not see later tests


          if (!el.querySelectorAll(":checked").length) {
            rbuggyQSA.push(":checked");
          } // Support: Safari 8+, iOS 8+
          // https://bugs.webkit.org/show_bug.cgi?id=136851
          // In-page `selector#id sibling-combinator selector` fails


          if (!el.querySelectorAll("a#" + expando + "+*").length) {
            rbuggyQSA.push(".#.+[+~]");
          } // Support: Firefox <=3.6 - 5 only
          // Old Firefox doesn't throw on a badly-escaped identifier.


          el.querySelectorAll("\\\f");
          rbuggyQSA.push("[\\r\\n\\f]");
        });
        assert(function (el) {
          el.innerHTML = "<a href='' disabled='disabled'></a>" + "<select disabled='disabled'><option/></select>"; // Support: Windows 8 Native Apps
          // The type and name attributes are restricted during .innerHTML assignment

          var input = document.createElement("input");
          input.setAttribute("type", "hidden");
          el.appendChild(input).setAttribute("name", "D"); // Support: IE8
          // Enforce case-sensitivity of name attribute

          if (el.querySelectorAll("[name=d]").length) {
            rbuggyQSA.push("name" + whitespace + "*[*^$|!~]?=");
          } // FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
          // IE8 throws error here and will not see later tests


          if (el.querySelectorAll(":enabled").length !== 2) {
            rbuggyQSA.push(":enabled", ":disabled");
          } // Support: IE9-11+
          // IE's :disabled selector does not pick up the children of disabled fieldsets


          docElem.appendChild(el).disabled = true;

          if (el.querySelectorAll(":disabled").length !== 2) {
            rbuggyQSA.push(":enabled", ":disabled");
          } // Support: Opera 10 - 11 only
          // Opera 10-11 does not throw on post-comma invalid pseudos


          el.querySelectorAll("*,:x");
          rbuggyQSA.push(",.*:");
        });
      }

      if (support.matchesSelector = rnative.test(matches = docElem.matches || docElem.webkitMatchesSelector || docElem.mozMatchesSelector || docElem.oMatchesSelector || docElem.msMatchesSelector)) {
        assert(function (el) {
          // Check to see if it's possible to do matchesSelector
          // on a disconnected node (IE 9)
          support.disconnectedMatch = matches.call(el, "*"); // This should fail with an exception
          // Gecko does not error, returns false instead

          matches.call(el, "[s!='']:x");
          rbuggyMatches.push("!=", pseudos);
        });
      }

      rbuggyQSA = rbuggyQSA.length && new RegExp(rbuggyQSA.join("|"));
      rbuggyMatches = rbuggyMatches.length && new RegExp(rbuggyMatches.join("|"));
      /* Contains
      ---------------------------------------------------------------------- */

      hasCompare = rnative.test(docElem.compareDocumentPosition); // Element contains another
      // Purposefully self-exclusive
      // As in, an element does not contain itself

      contains = hasCompare || rnative.test(docElem.contains) ? function (a, b) {
        var adown = a.nodeType === 9 ? a.documentElement : a,
            bup = b && b.parentNode;
        return a === bup || !!(bup && bup.nodeType === 1 && (adown.contains ? adown.contains(bup) : a.compareDocumentPosition && a.compareDocumentPosition(bup) & 16));
      } : function (a, b) {
        if (b) {
          while (b = b.parentNode) {
            if (b === a) {
              return true;
            }
          }
        }

        return false;
      };
      /* Sorting
      ---------------------------------------------------------------------- */
      // Document order sorting

      sortOrder = hasCompare ? function (a, b) {
        // Flag for duplicate removal
        if (a === b) {
          hasDuplicate = true;
          return 0;
        } // Sort on method existence if only one input has compareDocumentPosition


        var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;

        if (compare) {
          return compare;
        } // Calculate position if both inputs belong to the same document
        // Support: IE 11+, Edge 17 - 18+
        // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
        // two documents; shallow comparisons work.
        // eslint-disable-next-line eqeqeq


        compare = (a.ownerDocument || a) == (b.ownerDocument || b) ? a.compareDocumentPosition(b) : // Otherwise we know they are disconnected
        1; // Disconnected nodes

        if (compare & 1 || !support.sortDetached && b.compareDocumentPosition(a) === compare) {
          // Choose the first element that is related to our preferred document
          // Support: IE 11+, Edge 17 - 18+
          // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
          // two documents; shallow comparisons work.
          // eslint-disable-next-line eqeqeq
          if (a == document || a.ownerDocument == preferredDoc && contains(preferredDoc, a)) {
            return -1;
          } // Support: IE 11+, Edge 17 - 18+
          // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
          // two documents; shallow comparisons work.
          // eslint-disable-next-line eqeqeq


          if (b == document || b.ownerDocument == preferredDoc && contains(preferredDoc, b)) {
            return 1;
          } // Maintain original order


          return sortInput ? indexOf(sortInput, a) - indexOf(sortInput, b) : 0;
        }

        return compare & 4 ? -1 : 1;
      } : function (a, b) {
        // Exit early if the nodes are identical
        if (a === b) {
          hasDuplicate = true;
          return 0;
        }

        var cur,
            i = 0,
            aup = a.parentNode,
            bup = b.parentNode,
            ap = [a],
            bp = [b]; // Parentless nodes are either documents or disconnected

        if (!aup || !bup) {
          // Support: IE 11+, Edge 17 - 18+
          // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
          // two documents; shallow comparisons work.

          /* eslint-disable eqeqeq */
          return a == document ? -1 : b == document ? 1 :
          /* eslint-enable eqeqeq */
          aup ? -1 : bup ? 1 : sortInput ? indexOf(sortInput, a) - indexOf(sortInput, b) : 0; // If the nodes are siblings, we can do a quick check
        } else if (aup === bup) {
          return siblingCheck(a, b);
        } // Otherwise we need full lists of their ancestors for comparison


        cur = a;

        while (cur = cur.parentNode) {
          ap.unshift(cur);
        }

        cur = b;

        while (cur = cur.parentNode) {
          bp.unshift(cur);
        } // Walk down the tree looking for a discrepancy


        while (ap[i] === bp[i]) {
          i++;
        }

        return i ? // Do a sibling check if the nodes have a common ancestor
        siblingCheck(ap[i], bp[i]) : // Otherwise nodes in our document sort first
        // Support: IE 11+, Edge 17 - 18+
        // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
        // two documents; shallow comparisons work.

        /* eslint-disable eqeqeq */
        ap[i] == preferredDoc ? -1 : bp[i] == preferredDoc ? 1 :
        /* eslint-enable eqeqeq */
        0;
      };
      return document;
    };

    Sizzle.matches = function (expr, elements) {
      return Sizzle(expr, null, null, elements);
    };

    Sizzle.matchesSelector = function (elem, expr) {
      setDocument(elem);

      if (support.matchesSelector && documentIsHTML && !nonnativeSelectorCache[expr + " "] && (!rbuggyMatches || !rbuggyMatches.test(expr)) && (!rbuggyQSA || !rbuggyQSA.test(expr))) {
        try {
          var ret = matches.call(elem, expr); // IE 9's matchesSelector returns false on disconnected nodes

          if (ret || support.disconnectedMatch || // As well, disconnected nodes are said to be in a document
          // fragment in IE 9
          elem.document && elem.document.nodeType !== 11) {
            return ret;
          }
        } catch (e) {
          nonnativeSelectorCache(expr, true);
        }
      }

      return Sizzle(expr, document, null, [elem]).length > 0;
    };

    Sizzle.contains = function (context, elem) {
      // Set document vars if needed
      // Support: IE 11+, Edge 17 - 18+
      // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
      // two documents; shallow comparisons work.
      // eslint-disable-next-line eqeqeq
      if ((context.ownerDocument || context) != document) {
        setDocument(context);
      }

      return contains(context, elem);
    };

    Sizzle.attr = function (elem, name) {
      // Set document vars if needed
      // Support: IE 11+, Edge 17 - 18+
      // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
      // two documents; shallow comparisons work.
      // eslint-disable-next-line eqeqeq
      if ((elem.ownerDocument || elem) != document) {
        setDocument(elem);
      }

      var fn = Expr.attrHandle[name.toLowerCase()],
          // Don't get fooled by Object.prototype properties (jQuery #13807)
      val = fn && hasOwn.call(Expr.attrHandle, name.toLowerCase()) ? fn(elem, name, !documentIsHTML) : undefined;
      return val !== undefined ? val : support.attributes || !documentIsHTML ? elem.getAttribute(name) : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
    };

    Sizzle.escape = function (sel) {
      return (sel + "").replace(rcssescape, fcssescape);
    };

    Sizzle.error = function (msg) {
      throw new Error("Syntax error, unrecognized expression: " + msg);
    };
    /**
     * Document sorting and removing duplicates
     * @param {ArrayLike} results
     */


    Sizzle.uniqueSort = function (results) {
      var elem,
          duplicates = [],
          j = 0,
          i = 0; // Unless we *know* we can detect duplicates, assume their presence

      hasDuplicate = !support.detectDuplicates;
      sortInput = !support.sortStable && results.slice(0);
      results.sort(sortOrder);

      if (hasDuplicate) {
        while (elem = results[i++]) {
          if (elem === results[i]) {
            j = duplicates.push(i);
          }
        }

        while (j--) {
          results.splice(duplicates[j], 1);
        }
      } // Clear input after sorting to release objects
      // See https://github.com/jquery/sizzle/pull/225


      sortInput = null;
      return results;
    };
    /**
     * Utility function for retrieving the text value of an array of DOM nodes
     * @param {Array|Element} elem
     */


    getText = Sizzle.getText = function (elem) {
      var node,
          ret = "",
          i = 0,
          nodeType = elem.nodeType;

      if (!nodeType) {
        // If no nodeType, this is expected to be an array
        while (node = elem[i++]) {
          // Do not traverse comment nodes
          ret += getText(node);
        }
      } else if (nodeType === 1 || nodeType === 9 || nodeType === 11) {
        // Use textContent for elements
        // innerText usage removed for consistency of new lines (jQuery #11153)
        if (typeof elem.textContent === "string") {
          return elem.textContent;
        } else {
          // Traverse its children
          for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
            ret += getText(elem);
          }
        }
      } else if (nodeType === 3 || nodeType === 4) {
        return elem.nodeValue;
      } // Do not include comment or processing instruction nodes


      return ret;
    };

    Expr = Sizzle.selectors = {
      // Can be adjusted by the user
      cacheLength: 50,
      createPseudo: markFunction,
      match: matchExpr,
      attrHandle: {},
      find: {},
      relative: {
        ">": {
          dir: "parentNode",
          first: true
        },
        " ": {
          dir: "parentNode"
        },
        "+": {
          dir: "previousSibling",
          first: true
        },
        "~": {
          dir: "previousSibling"
        }
      },
      preFilter: {
        "ATTR": function ATTR(match) {
          match[1] = match[1].replace(runescape, funescape); // Move the given value to match[3] whether quoted or unquoted

          match[3] = (match[3] || match[4] || match[5] || "").replace(runescape, funescape);

          if (match[2] === "~=") {
            match[3] = " " + match[3] + " ";
          }

          return match.slice(0, 4);
        },
        "CHILD": function CHILD(match) {
          /* matches from matchExpr["CHILD"]
          	1 type (only|nth|...)
          	2 what (child|of-type)
          	3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
          	4 xn-component of xn+y argument ([+-]?\d*n|)
          	5 sign of xn-component
          	6 x of xn-component
          	7 sign of y-component
          	8 y of y-component
          */
          match[1] = match[1].toLowerCase();

          if (match[1].slice(0, 3) === "nth") {
            // nth-* requires argument
            if (!match[3]) {
              Sizzle.error(match[0]);
            } // numeric x and y parameters for Expr.filter.CHILD
            // remember that false/true cast respectively to 0/1


            match[4] = +(match[4] ? match[5] + (match[6] || 1) : 2 * (match[3] === "even" || match[3] === "odd"));
            match[5] = +(match[7] + match[8] || match[3] === "odd"); // other types prohibit arguments
          } else if (match[3]) {
            Sizzle.error(match[0]);
          }

          return match;
        },
        "PSEUDO": function PSEUDO(match) {
          var excess,
              unquoted = !match[6] && match[2];

          if (matchExpr["CHILD"].test(match[0])) {
            return null;
          } // Accept quoted arguments as-is


          if (match[3]) {
            match[2] = match[4] || match[5] || ""; // Strip excess characters from unquoted arguments
          } else if (unquoted && rpseudo.test(unquoted) && ( // Get excess from tokenize (recursively)
          excess = tokenize(unquoted, true)) && ( // advance to the next closing parenthesis
          excess = unquoted.indexOf(")", unquoted.length - excess) - unquoted.length)) {
            // excess is a negative index
            match[0] = match[0].slice(0, excess);
            match[2] = unquoted.slice(0, excess);
          } // Return only captures needed by the pseudo filter method (type and argument)


          return match.slice(0, 3);
        }
      },
      filter: {
        "TAG": function TAG(nodeNameSelector) {
          var nodeName = nodeNameSelector.replace(runescape, funescape).toLowerCase();
          return nodeNameSelector === "*" ? function () {
            return true;
          } : function (elem) {
            return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
          };
        },
        "CLASS": function CLASS(className) {
          var pattern = classCache[className + " "];
          return pattern || (pattern = new RegExp("(^|" + whitespace + ")" + className + "(" + whitespace + "|$)")) && classCache(className, function (elem) {
            return pattern.test(typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "");
          });
        },
        "ATTR": function ATTR(name, operator, check) {
          return function (elem) {
            var result = Sizzle.attr(elem, name);

            if (result == null) {
              return operator === "!=";
            }

            if (!operator) {
              return true;
            }

            result += "";
            /* eslint-disable max-len */

            return operator === "=" ? result === check : operator === "!=" ? result !== check : operator === "^=" ? check && result.indexOf(check) === 0 : operator === "*=" ? check && result.indexOf(check) > -1 : operator === "$=" ? check && result.slice(-check.length) === check : operator === "~=" ? (" " + result.replace(rwhitespace, " ") + " ").indexOf(check) > -1 : operator === "|=" ? result === check || result.slice(0, check.length + 1) === check + "-" : false;
            /* eslint-enable max-len */
          };
        },
        "CHILD": function CHILD(type, what, _argument, first, last) {
          var simple = type.slice(0, 3) !== "nth",
              forward = type.slice(-4) !== "last",
              ofType = what === "of-type";
          return first === 1 && last === 0 ? // Shortcut for :nth-*(n)
          function (elem) {
            return !!elem.parentNode;
          } : function (elem, _context, xml) {
            var cache,
                uniqueCache,
                outerCache,
                node,
                nodeIndex,
                start,
                dir = simple !== forward ? "nextSibling" : "previousSibling",
                parent = elem.parentNode,
                name = ofType && elem.nodeName.toLowerCase(),
                useCache = !xml && !ofType,
                diff = false;

            if (parent) {
              // :(first|last|only)-(child|of-type)
              if (simple) {
                while (dir) {
                  node = elem;

                  while (node = node[dir]) {
                    if (ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) {
                      return false;
                    }
                  } // Reverse direction for :only-* (if we haven't yet done so)


                  start = dir = type === "only" && !start && "nextSibling";
                }

                return true;
              }

              start = [forward ? parent.firstChild : parent.lastChild]; // non-xml :nth-child(...) stores cache data on `parent`

              if (forward && useCache) {
                // Seek `elem` from a previously-cached index
                // ...in a gzip-friendly way
                node = parent;
                outerCache = node[expando] || (node[expando] = {}); // Support: IE <9 only
                // Defend against cloned attroperties (jQuery gh-1709)

                uniqueCache = outerCache[node.uniqueID] || (outerCache[node.uniqueID] = {});
                cache = uniqueCache[type] || [];
                nodeIndex = cache[0] === dirruns && cache[1];
                diff = nodeIndex && cache[2];
                node = nodeIndex && parent.childNodes[nodeIndex];

                while (node = ++nodeIndex && node && node[dir] || ( // Fallback to seeking `elem` from the start
                diff = nodeIndex = 0) || start.pop()) {
                  // When found, cache indexes on `parent` and break
                  if (node.nodeType === 1 && ++diff && node === elem) {
                    uniqueCache[type] = [dirruns, nodeIndex, diff];
                    break;
                  }
                }
              } else {
                // Use previously-cached element index if available
                if (useCache) {
                  // ...in a gzip-friendly way
                  node = elem;
                  outerCache = node[expando] || (node[expando] = {}); // Support: IE <9 only
                  // Defend against cloned attroperties (jQuery gh-1709)

                  uniqueCache = outerCache[node.uniqueID] || (outerCache[node.uniqueID] = {});
                  cache = uniqueCache[type] || [];
                  nodeIndex = cache[0] === dirruns && cache[1];
                  diff = nodeIndex;
                } // xml :nth-child(...)
                // or :nth-last-child(...) or :nth(-last)?-of-type(...)


                if (diff === false) {
                  // Use the same loop as above to seek `elem` from the start
                  while (node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop()) {
                    if ((ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) && ++diff) {
                      // Cache the index of each encountered element
                      if (useCache) {
                        outerCache = node[expando] || (node[expando] = {}); // Support: IE <9 only
                        // Defend against cloned attroperties (jQuery gh-1709)

                        uniqueCache = outerCache[node.uniqueID] || (outerCache[node.uniqueID] = {});
                        uniqueCache[type] = [dirruns, diff];
                      }

                      if (node === elem) {
                        break;
                      }
                    }
                  }
                }
              } // Incorporate the offset, then check against cycle size


              diff -= last;
              return diff === first || diff % first === 0 && diff / first >= 0;
            }
          };
        },
        "PSEUDO": function PSEUDO(pseudo, argument) {
          // pseudo-class names are case-insensitive
          // http://www.w3.org/TR/selectors/#pseudo-classes
          // Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
          // Remember that setFilters inherits from pseudos
          var args,
              fn = Expr.pseudos[pseudo] || Expr.setFilters[pseudo.toLowerCase()] || Sizzle.error("unsupported pseudo: " + pseudo); // The user may use createPseudo to indicate that
          // arguments are needed to create the filter function
          // just as Sizzle does

          if (fn[expando]) {
            return fn(argument);
          } // But maintain support for old signatures


          if (fn.length > 1) {
            args = [pseudo, pseudo, "", argument];
            return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase()) ? markFunction(function (seed, matches) {
              var idx,
                  matched = fn(seed, argument),
                  i = matched.length;

              while (i--) {
                idx = indexOf(seed, matched[i]);
                seed[idx] = !(matches[idx] = matched[i]);
              }
            }) : function (elem) {
              return fn(elem, 0, args);
            };
          }

          return fn;
        }
      },
      pseudos: {
        // Potentially complex pseudos
        "not": markFunction(function (selector) {
          // Trim the selector passed to compile
          // to avoid treating leading and trailing
          // spaces as combinators
          var input = [],
              results = [],
              matcher = compile(selector.replace(rtrim, "$1"));
          return matcher[expando] ? markFunction(function (seed, matches, _context, xml) {
            var elem,
                unmatched = matcher(seed, null, xml, []),
                i = seed.length; // Match elements unmatched by `matcher`

            while (i--) {
              if (elem = unmatched[i]) {
                seed[i] = !(matches[i] = elem);
              }
            }
          }) : function (elem, _context, xml) {
            input[0] = elem;
            matcher(input, null, xml, results); // Don't keep the element (issue #299)

            input[0] = null;
            return !results.pop();
          };
        }),
        "has": markFunction(function (selector) {
          return function (elem) {
            return Sizzle(selector, elem).length > 0;
          };
        }),
        "contains": markFunction(function (text) {
          text = text.replace(runescape, funescape);
          return function (elem) {
            return (elem.textContent || getText(elem)).indexOf(text) > -1;
          };
        }),
        // "Whether an element is represented by a :lang() selector
        // is based solely on the element's language value
        // being equal to the identifier C,
        // or beginning with the identifier C immediately followed by "-".
        // The matching of C against the element's language value is performed case-insensitively.
        // The identifier C does not have to be a valid language name."
        // http://www.w3.org/TR/selectors/#lang-pseudo
        "lang": markFunction(function (lang) {
          // lang value must be a valid identifier
          if (!ridentifier.test(lang || "")) {
            Sizzle.error("unsupported lang: " + lang);
          }

          lang = lang.replace(runescape, funescape).toLowerCase();
          return function (elem) {
            var elemLang;

            do {
              if (elemLang = documentIsHTML ? elem.lang : elem.getAttribute("xml:lang") || elem.getAttribute("lang")) {
                elemLang = elemLang.toLowerCase();
                return elemLang === lang || elemLang.indexOf(lang + "-") === 0;
              }
            } while ((elem = elem.parentNode) && elem.nodeType === 1);

            return false;
          };
        }),
        // Miscellaneous
        "target": function target(elem) {
          var hash = window.location && window.location.hash;
          return hash && hash.slice(1) === elem.id;
        },
        "root": function root(elem) {
          return elem === docElem;
        },
        "focus": function focus(elem) {
          return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
        },
        // Boolean properties
        "enabled": createDisabledPseudo(false),
        "disabled": createDisabledPseudo(true),
        "checked": function checked(elem) {
          // In CSS3, :checked should return both checked and selected elements
          // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
          var nodeName = elem.nodeName.toLowerCase();
          return nodeName === "input" && !!elem.checked || nodeName === "option" && !!elem.selected;
        },
        "selected": function selected(elem) {
          // Accessing this property makes selected-by-default
          // options in Safari work properly
          if (elem.parentNode) {
            // eslint-disable-next-line no-unused-expressions
            elem.parentNode.selectedIndex;
          }

          return elem.selected === true;
        },
        // Contents
        "empty": function empty(elem) {
          // http://www.w3.org/TR/selectors/#empty-pseudo
          // :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
          //   but not by others (comment: 8; processing instruction: 7; etc.)
          // nodeType < 6 works because attributes (2) do not appear as children
          for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
            if (elem.nodeType < 6) {
              return false;
            }
          }

          return true;
        },
        "parent": function parent(elem) {
          return !Expr.pseudos["empty"](elem);
        },
        // Element/input types
        "header": function header(elem) {
          return rheader.test(elem.nodeName);
        },
        "input": function input(elem) {
          return rinputs.test(elem.nodeName);
        },
        "button": function button(elem) {
          var name = elem.nodeName.toLowerCase();
          return name === "input" && elem.type === "button" || name === "button";
        },
        "text": function text(elem) {
          var attr;
          return elem.nodeName.toLowerCase() === "input" && elem.type === "text" && ( // Support: IE<8
          // New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
          (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text");
        },
        // Position-in-collection
        "first": createPositionalPseudo(function () {
          return [0];
        }),
        "last": createPositionalPseudo(function (_matchIndexes, length) {
          return [length - 1];
        }),
        "eq": createPositionalPseudo(function (_matchIndexes, length, argument) {
          return [argument < 0 ? argument + length : argument];
        }),
        "even": createPositionalPseudo(function (matchIndexes, length) {
          var i = 0;

          for (; i < length; i += 2) {
            matchIndexes.push(i);
          }

          return matchIndexes;
        }),
        "odd": createPositionalPseudo(function (matchIndexes, length) {
          var i = 1;

          for (; i < length; i += 2) {
            matchIndexes.push(i);
          }

          return matchIndexes;
        }),
        "lt": createPositionalPseudo(function (matchIndexes, length, argument) {
          var i = argument < 0 ? argument + length : argument > length ? length : argument;

          for (; --i >= 0;) {
            matchIndexes.push(i);
          }

          return matchIndexes;
        }),
        "gt": createPositionalPseudo(function (matchIndexes, length, argument) {
          var i = argument < 0 ? argument + length : argument;

          for (; ++i < length;) {
            matchIndexes.push(i);
          }

          return matchIndexes;
        })
      }
    };
    Expr.pseudos["nth"] = Expr.pseudos["eq"]; // Add button/input type pseudos

    for (i in {
      radio: true,
      checkbox: true,
      file: true,
      password: true,
      image: true
    }) {
      Expr.pseudos[i] = createInputPseudo(i);
    }

    for (i in {
      submit: true,
      reset: true
    }) {
      Expr.pseudos[i] = createButtonPseudo(i);
    } // Easy API for creating new setFilters


    function setFilters() {}

    setFilters.prototype = Expr.filters = Expr.pseudos;
    Expr.setFilters = new setFilters();

    tokenize = Sizzle.tokenize = function (selector, parseOnly) {
      var matched,
          match,
          tokens,
          type,
          soFar,
          groups,
          preFilters,
          cached = tokenCache[selector + " "];

      if (cached) {
        return parseOnly ? 0 : cached.slice(0);
      }

      soFar = selector;
      groups = [];
      preFilters = Expr.preFilter;

      while (soFar) {
        // Comma and first run
        if (!matched || (match = rcomma.exec(soFar))) {
          if (match) {
            // Don't consume trailing commas as valid
            soFar = soFar.slice(match[0].length) || soFar;
          }

          groups.push(tokens = []);
        }

        matched = false; // Combinators

        if (match = rcombinators.exec(soFar)) {
          matched = match.shift();
          tokens.push({
            value: matched,
            // Cast descendant combinators to space
            type: match[0].replace(rtrim, " ")
          });
          soFar = soFar.slice(matched.length);
        } // Filters


        for (type in Expr.filter) {
          if ((match = matchExpr[type].exec(soFar)) && (!preFilters[type] || (match = preFilters[type](match)))) {
            matched = match.shift();
            tokens.push({
              value: matched,
              type: type,
              matches: match
            });
            soFar = soFar.slice(matched.length);
          }
        }

        if (!matched) {
          break;
        }
      } // Return the length of the invalid excess
      // if we're just parsing
      // Otherwise, throw an error or return tokens


      return parseOnly ? soFar.length : soFar ? Sizzle.error(selector) : // Cache the tokens
      tokenCache(selector, groups).slice(0);
    };

    function toSelector(tokens) {
      var i = 0,
          len = tokens.length,
          selector = "";

      for (; i < len; i++) {
        selector += tokens[i].value;
      }

      return selector;
    }

    function addCombinator(matcher, combinator, base) {
      var dir = combinator.dir,
          skip = combinator.next,
          key = skip || dir,
          checkNonElements = base && key === "parentNode",
          doneName = done++;
      return combinator.first ? // Check against closest ancestor/preceding element
      function (elem, context, xml) {
        while (elem = elem[dir]) {
          if (elem.nodeType === 1 || checkNonElements) {
            return matcher(elem, context, xml);
          }
        }

        return false;
      } : // Check against all ancestor/preceding elements
      function (elem, context, xml) {
        var oldCache,
            uniqueCache,
            outerCache,
            newCache = [dirruns, doneName]; // We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching

        if (xml) {
          while (elem = elem[dir]) {
            if (elem.nodeType === 1 || checkNonElements) {
              if (matcher(elem, context, xml)) {
                return true;
              }
            }
          }
        } else {
          while (elem = elem[dir]) {
            if (elem.nodeType === 1 || checkNonElements) {
              outerCache = elem[expando] || (elem[expando] = {}); // Support: IE <9 only
              // Defend against cloned attroperties (jQuery gh-1709)

              uniqueCache = outerCache[elem.uniqueID] || (outerCache[elem.uniqueID] = {});

              if (skip && skip === elem.nodeName.toLowerCase()) {
                elem = elem[dir] || elem;
              } else if ((oldCache = uniqueCache[key]) && oldCache[0] === dirruns && oldCache[1] === doneName) {
                // Assign to newCache so results back-propagate to previous elements
                return newCache[2] = oldCache[2];
              } else {
                // Reuse newcache so results back-propagate to previous elements
                uniqueCache[key] = newCache; // A match means we're done; a fail means we have to keep checking

                if (newCache[2] = matcher(elem, context, xml)) {
                  return true;
                }
              }
            }
          }
        }

        return false;
      };
    }

    function elementMatcher(matchers) {
      return matchers.length > 1 ? function (elem, context, xml) {
        var i = matchers.length;

        while (i--) {
          if (!matchers[i](elem, context, xml)) {
            return false;
          }
        }

        return true;
      } : matchers[0];
    }

    function multipleContexts(selector, contexts, results) {
      var i = 0,
          len = contexts.length;

      for (; i < len; i++) {
        Sizzle(selector, contexts[i], results);
      }

      return results;
    }

    function condense(unmatched, map, filter, context, xml) {
      var elem,
          newUnmatched = [],
          i = 0,
          len = unmatched.length,
          mapped = map != null;

      for (; i < len; i++) {
        if (elem = unmatched[i]) {
          if (!filter || filter(elem, context, xml)) {
            newUnmatched.push(elem);

            if (mapped) {
              map.push(i);
            }
          }
        }
      }

      return newUnmatched;
    }

    function setMatcher(preFilter, selector, matcher, postFilter, postFinder, postSelector) {
      if (postFilter && !postFilter[expando]) {
        postFilter = setMatcher(postFilter);
      }

      if (postFinder && !postFinder[expando]) {
        postFinder = setMatcher(postFinder, postSelector);
      }

      return markFunction(function (seed, results, context, xml) {
        var temp,
            i,
            elem,
            preMap = [],
            postMap = [],
            preexisting = results.length,
            // Get initial elements from seed or context
        elems = seed || multipleContexts(selector || "*", context.nodeType ? [context] : context, []),
            // Prefilter to get matcher input, preserving a map for seed-results synchronization
        matcherIn = preFilter && (seed || !selector) ? condense(elems, preMap, preFilter, context, xml) : elems,
            matcherOut = matcher ? // If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
        postFinder || (seed ? preFilter : preexisting || postFilter) ? // ...intermediate processing is necessary
        [] : // ...otherwise use results directly
        results : matcherIn; // Find primary matches

        if (matcher) {
          matcher(matcherIn, matcherOut, context, xml);
        } // Apply postFilter


        if (postFilter) {
          temp = condense(matcherOut, postMap);
          postFilter(temp, [], context, xml); // Un-match failing elements by moving them back to matcherIn

          i = temp.length;

          while (i--) {
            if (elem = temp[i]) {
              matcherOut[postMap[i]] = !(matcherIn[postMap[i]] = elem);
            }
          }
        }

        if (seed) {
          if (postFinder || preFilter) {
            if (postFinder) {
              // Get the final matcherOut by condensing this intermediate into postFinder contexts
              temp = [];
              i = matcherOut.length;

              while (i--) {
                if (elem = matcherOut[i]) {
                  // Restore matcherIn since elem is not yet a final match
                  temp.push(matcherIn[i] = elem);
                }
              }

              postFinder(null, matcherOut = [], temp, xml);
            } // Move matched elements from seed to results to keep them synchronized


            i = matcherOut.length;

            while (i--) {
              if ((elem = matcherOut[i]) && (temp = postFinder ? indexOf(seed, elem) : preMap[i]) > -1) {
                seed[temp] = !(results[temp] = elem);
              }
            }
          } // Add elements to results, through postFinder if defined

        } else {
          matcherOut = condense(matcherOut === results ? matcherOut.splice(preexisting, matcherOut.length) : matcherOut);

          if (postFinder) {
            postFinder(null, results, matcherOut, xml);
          } else {
            push.apply(results, matcherOut);
          }
        }
      });
    }

    function matcherFromTokens(tokens) {
      var checkContext,
          matcher,
          j,
          len = tokens.length,
          leadingRelative = Expr.relative[tokens[0].type],
          implicitRelative = leadingRelative || Expr.relative[" "],
          i = leadingRelative ? 1 : 0,
          // The foundational matcher ensures that elements are reachable from top-level context(s)
      matchContext = addCombinator(function (elem) {
        return elem === checkContext;
      }, implicitRelative, true),
          matchAnyContext = addCombinator(function (elem) {
        return indexOf(checkContext, elem) > -1;
      }, implicitRelative, true),
          matchers = [function (elem, context, xml) {
        var ret = !leadingRelative && (xml || context !== outermostContext) || ((checkContext = context).nodeType ? matchContext(elem, context, xml) : matchAnyContext(elem, context, xml)); // Avoid hanging onto element (issue #299)

        checkContext = null;
        return ret;
      }];

      for (; i < len; i++) {
        if (matcher = Expr.relative[tokens[i].type]) {
          matchers = [addCombinator(elementMatcher(matchers), matcher)];
        } else {
          matcher = Expr.filter[tokens[i].type].apply(null, tokens[i].matches); // Return special upon seeing a positional matcher

          if (matcher[expando]) {
            // Find the next relative operator (if any) for proper handling
            j = ++i;

            for (; j < len; j++) {
              if (Expr.relative[tokens[j].type]) {
                break;
              }
            }

            return setMatcher(i > 1 && elementMatcher(matchers), i > 1 && toSelector( // If the preceding token was a descendant combinator, insert an implicit any-element `*`
            tokens.slice(0, i - 1).concat({
              value: tokens[i - 2].type === " " ? "*" : ""
            })).replace(rtrim, "$1"), matcher, i < j && matcherFromTokens(tokens.slice(i, j)), j < len && matcherFromTokens(tokens = tokens.slice(j)), j < len && toSelector(tokens));
          }

          matchers.push(matcher);
        }
      }

      return elementMatcher(matchers);
    }

    function matcherFromGroupMatchers(elementMatchers, setMatchers) {
      var bySet = setMatchers.length > 0,
          byElement = elementMatchers.length > 0,
          superMatcher = function superMatcher(seed, context, xml, results, outermost) {
        var elem,
            j,
            matcher,
            matchedCount = 0,
            i = "0",
            unmatched = seed && [],
            setMatched = [],
            contextBackup = outermostContext,
            // We must always have either seed elements or outermost context
        elems = seed || byElement && Expr.find["TAG"]("*", outermost),
            // Use integer dirruns iff this is the outermost matcher
        dirrunsUnique = dirruns += contextBackup == null ? 1 : Math.random() || 0.1,
            len = elems.length;

        if (outermost) {
          // Support: IE 11+, Edge 17 - 18+
          // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
          // two documents; shallow comparisons work.
          // eslint-disable-next-line eqeqeq
          outermostContext = context == document || context || outermost;
        } // Add elements passing elementMatchers directly to results
        // Support: IE<9, Safari
        // Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id


        for (; i !== len && (elem = elems[i]) != null; i++) {
          if (byElement && elem) {
            j = 0; // Support: IE 11+, Edge 17 - 18+
            // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
            // two documents; shallow comparisons work.
            // eslint-disable-next-line eqeqeq

            if (!context && elem.ownerDocument != document) {
              setDocument(elem);
              xml = !documentIsHTML;
            }

            while (matcher = elementMatchers[j++]) {
              if (matcher(elem, context || document, xml)) {
                results.push(elem);
                break;
              }
            }

            if (outermost) {
              dirruns = dirrunsUnique;
            }
          } // Track unmatched elements for set filters


          if (bySet) {
            // They will have gone through all possible matchers
            if (elem = !matcher && elem) {
              matchedCount--;
            } // Lengthen the array for every element, matched or not


            if (seed) {
              unmatched.push(elem);
            }
          }
        } // `i` is now the count of elements visited above, and adding it to `matchedCount`
        // makes the latter nonnegative.


        matchedCount += i; // Apply set filters to unmatched elements
        // NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
        // equals `i`), unless we didn't visit _any_ elements in the above loop because we have
        // no element matchers and no seed.
        // Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
        // case, which will result in a "00" `matchedCount` that differs from `i` but is also
        // numerically zero.

        if (bySet && i !== matchedCount) {
          j = 0;

          while (matcher = setMatchers[j++]) {
            matcher(unmatched, setMatched, context, xml);
          }

          if (seed) {
            // Reintegrate element matches to eliminate the need for sorting
            if (matchedCount > 0) {
              while (i--) {
                if (!(unmatched[i] || setMatched[i])) {
                  setMatched[i] = pop.call(results);
                }
              }
            } // Discard index placeholder values to get only actual matches


            setMatched = condense(setMatched);
          } // Add matches to results


          push.apply(results, setMatched); // Seedless set matches succeeding multiple successful matchers stipulate sorting

          if (outermost && !seed && setMatched.length > 0 && matchedCount + setMatchers.length > 1) {
            Sizzle.uniqueSort(results);
          }
        } // Override manipulation of globals by nested matchers


        if (outermost) {
          dirruns = dirrunsUnique;
          outermostContext = contextBackup;
        }

        return unmatched;
      };

      return bySet ? markFunction(superMatcher) : superMatcher;
    }

    compile = Sizzle.compile = function (selector, match
    /* Internal Use Only */
    ) {
      var i,
          setMatchers = [],
          elementMatchers = [],
          cached = compilerCache[selector + " "];

      if (!cached) {
        // Generate a function of recursive functions that can be used to check each element
        if (!match) {
          match = tokenize(selector);
        }

        i = match.length;

        while (i--) {
          cached = matcherFromTokens(match[i]);

          if (cached[expando]) {
            setMatchers.push(cached);
          } else {
            elementMatchers.push(cached);
          }
        } // Cache the compiled function


        cached = compilerCache(selector, matcherFromGroupMatchers(elementMatchers, setMatchers)); // Save selector and tokenization

        cached.selector = selector;
      }

      return cached;
    };
    /**
     * A low-level selection function that works with Sizzle's compiled
     *  selector functions
     * @param {String|Function} selector A selector or a pre-compiled
     *  selector function built with Sizzle.compile
     * @param {Element} context
     * @param {Array} [results]
     * @param {Array} [seed] A set of elements to match against
     */


    select = Sizzle.select = function (selector, context, results, seed) {
      var i,
          tokens,
          token,
          type,
          find,
          compiled = typeof selector === "function" && selector,
          match = !seed && tokenize(selector = compiled.selector || selector);
      results = results || []; // Try to minimize operations if there is only one selector in the list and no seed
      // (the latter of which guarantees us context)

      if (match.length === 1) {
        // Reduce context if the leading compound selector is an ID
        tokens = match[0] = match[0].slice(0);

        if (tokens.length > 2 && (token = tokens[0]).type === "ID" && context.nodeType === 9 && documentIsHTML && Expr.relative[tokens[1].type]) {
          context = (Expr.find["ID"](token.matches[0].replace(runescape, funescape), context) || [])[0];

          if (!context) {
            return results; // Precompiled matchers will still verify ancestry, so step up a level
          } else if (compiled) {
            context = context.parentNode;
          }

          selector = selector.slice(tokens.shift().value.length);
        } // Fetch a seed set for right-to-left matching


        i = matchExpr["needsContext"].test(selector) ? 0 : tokens.length;

        while (i--) {
          token = tokens[i]; // Abort if we hit a combinator

          if (Expr.relative[type = token.type]) {
            break;
          }

          if (find = Expr.find[type]) {
            // Search, expanding context for leading sibling combinators
            if (seed = find(token.matches[0].replace(runescape, funescape), rsibling.test(tokens[0].type) && testContext(context.parentNode) || context)) {
              // If seed is empty or no tokens remain, we can return early
              tokens.splice(i, 1);
              selector = seed.length && toSelector(tokens);

              if (!selector) {
                push.apply(results, seed);
                return results;
              }

              break;
            }
          }
        }
      } // Compile and execute a filtering function if one is not provided
      // Provide `match` to avoid retokenization if we modified the selector above


      (compiled || compile(selector, match))(seed, context, !documentIsHTML, results, !context || rsibling.test(selector) && testContext(context.parentNode) || context);
      return results;
    }; // One-time assignments
    // Sort stability


    support.sortStable = expando.split("").sort(sortOrder).join("") === expando; // Support: Chrome 14-35+
    // Always assume duplicates if they aren't passed to the comparison function

    support.detectDuplicates = !!hasDuplicate; // Initialize against the default document

    setDocument(); // Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
    // Detached nodes confoundingly follow *each other*

    support.sortDetached = assert(function (el) {
      // Should return 1, but returns 4 (following)
      return el.compareDocumentPosition(document.createElement("fieldset")) & 1;
    }); // Support: IE<8
    // Prevent attribute/property "interpolation"
    // https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx

    if (!assert(function (el) {
      el.innerHTML = "<a href='#'></a>";
      return el.firstChild.getAttribute("href") === "#";
    })) {
      addHandle("type|href|height|width", function (elem, name, isXML) {
        if (!isXML) {
          return elem.getAttribute(name, name.toLowerCase() === "type" ? 1 : 2);
        }
      });
    } // Support: IE<9
    // Use defaultValue in place of getAttribute("value")


    if (!support.attributes || !assert(function (el) {
      el.innerHTML = "<input/>";
      el.firstChild.setAttribute("value", "");
      return el.firstChild.getAttribute("value") === "";
    })) {
      addHandle("value", function (elem, _name, isXML) {
        if (!isXML && elem.nodeName.toLowerCase() === "input") {
          return elem.defaultValue;
        }
      });
    } // Support: IE<9
    // Use getAttributeNode to fetch booleans when getAttribute lies


    if (!assert(function (el) {
      return el.getAttribute("disabled") == null;
    })) {
      addHandle(booleans, function (elem, name, isXML) {
        var val;

        if (!isXML) {
          return elem[name] === true ? name.toLowerCase() : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
        }
      });
    }

    return Sizzle;
  }(window);

  djQ.find = Sizzle;
  djQ.expr = Sizzle.selectors; // Deprecated

  djQ.expr[":"] = djQ.expr.pseudos;
  djQ.uniqueSort = djQ.unique = Sizzle.uniqueSort;
  djQ.text = Sizzle.getText;
  djQ.isXMLDoc = Sizzle.isXML;
  djQ.contains = Sizzle.contains;
  djQ.escapeSelector = Sizzle.escape;

  var dir = function dir(elem, _dir, until) {
    var matched = [],
        truncate = until !== undefined;

    while ((elem = elem[_dir]) && elem.nodeType !== 9) {
      if (elem.nodeType === 1) {
        if (truncate && djQ(elem).is(until)) {
          break;
        }

        matched.push(elem);
      }
    }

    return matched;
  };

  var _siblings = function siblings(n, elem) {
    var matched = [];

    for (; n; n = n.nextSibling) {
      if (n.nodeType === 1 && n !== elem) {
        matched.push(n);
      }
    }

    return matched;
  };

  var rneedsContext = djQ.expr.match.needsContext;

  function nodeName(elem, name) {
    return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
  }

  ;
  var rsingleTag = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i; // Implement the identical functionality for filter and not

  function winnow(elements, qualifier, not) {
    if (isFunction(qualifier)) {
      return djQ.grep(elements, function (elem, i) {
        return !!qualifier.call(elem, i, elem) !== not;
      });
    } // Single element


    if (qualifier.nodeType) {
      return djQ.grep(elements, function (elem) {
        return elem === qualifier !== not;
      });
    } // Arraylike of elements (djQ, arguments, Array)


    if (typeof qualifier !== "string") {
      return djQ.grep(elements, function (elem) {
        return indexOf.call(qualifier, elem) > -1 !== not;
      });
    } // Filtered directly for both simple and complex selectors


    return djQ.filter(qualifier, elements, not);
  }

  djQ.filter = function (expr, elems, not) {
    var elem = elems[0];

    if (not) {
      expr = ":not(" + expr + ")";
    }

    if (elems.length === 1 && elem.nodeType === 1) {
      return djQ.find.matchesSelector(elem, expr) ? [elem] : [];
    }

    return djQ.find.matches(expr, djQ.grep(elems, function (elem) {
      return elem.nodeType === 1;
    }));
  };

  djQ.fn.extend({
    find: function find(selector) {
      var i,
          ret,
          len = this.length,
          self = this;

      if (typeof selector !== "string") {
        return this.pushStack(djQ(selector).filter(function () {
          for (i = 0; i < len; i++) {
            if (djQ.contains(self[i], this)) {
              return true;
            }
          }
        }));
      }

      ret = this.pushStack([]);

      for (i = 0; i < len; i++) {
        djQ.find(selector, self[i], ret);
      }

      return len > 1 ? djQ.uniqueSort(ret) : ret;
    },
    filter: function filter(selector) {
      return this.pushStack(winnow(this, selector || [], false));
    },
    not: function not(selector) {
      return this.pushStack(winnow(this, selector || [], true));
    },
    is: function is(selector) {
      return !!winnow(this, // If this is a positional/relative selector, check membership in the returned set
      // so $("p:first").is("p:last") won't return true for a doc with two "p".
      typeof selector === "string" && rneedsContext.test(selector) ? djQ(selector) : selector || [], false).length;
    }
  }); // Initialize a jQuery object
  // A central reference to the root jQuery(document)

  var rootdjQ,
      // A simple way to check for HTML strings
  // Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
  // Strict HTML recognition (#11290: must start with <)
  // Shortcut simple #id case for speed
  rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
      init = djQ.fn.init = function (selector, context, root) {
    var match, elem; // HANDLE: $(""), $(null), $(undefined), $(false)

    if (!selector) {
      return this;
    } // Method init() accepts an alternate rootjQuery
    // so migrate can support jQuery.sub (gh-2101)


    root = root || rootdjQ; // Handle HTML strings

    if (typeof selector === "string") {
      if (selector[0] === "<" && selector[selector.length - 1] === ">" && selector.length >= 3) {
        // Assume that strings that start and end with <> are HTML and skip the regex check
        match = [null, selector, null];
      } else {
        match = rquickExpr.exec(selector);
      } // Match html or make sure no context is specified for #id


      if (match && (match[1] || !context)) {
        // HANDLE: $(html) -> $(array)
        if (match[1]) {
          context = context instanceof djQ ? context[0] : context; // Option to run scripts is true for back-compat
          // Intentionally let the error be thrown if parseHTML is not present

          djQ.merge(this, djQ.parseHTML(match[1], context && context.nodeType ? context.ownerDocument || context : document, true)); // HANDLE: $(html, props)

          if (rsingleTag.test(match[1]) && djQ.isPlainObject(context)) {
            for (match in context) {
              // Properties of context are called as methods if possible
              if (isFunction(this[match])) {
                this[match](context[match]); // ...and otherwise set as attributes
              } else {
                this.attr(match, context[match]);
              }
            }
          }

          return this; // HANDLE: $(#id)
        } else {
          elem = document.getElementById(match[2]);

          if (elem) {
            // Inject the element directly into the jQuery object
            this[0] = elem;
            this.length = 1;
          }

          return this;
        } // HANDLE: $(expr, $(...))

      } else if (!context || context.jquery) {
        return (context || root).find(selector); // HANDLE: $(expr, context)
        // (which is just equivalent to: $(context).find(expr)
      } else {
        return this.constructor(context).find(selector);
      } // HANDLE: $(DOMElement)

    } else if (selector.nodeType) {
      this[0] = selector;
      this.length = 1;
      return this; // HANDLE: $(function)
      // Shortcut for document ready
    } else if (isFunction(selector)) {
      return root.ready !== undefined ? root.ready(selector) : // Execute immediately if ready is not present
      selector(djQ);
    }

    return djQ.makeArray(selector, this);
  }; // Give the init function the jQuery prototype for later instantiation


  init.prototype = djQ.fn; // Initialize central reference

  rootdjQ = djQ(document);
  var rparentsprev = /^(?:parents|prev(?:Until|All))/,
      // Methods guaranteed to produce a unique set when starting from a unique set
  guaranteedUnique = {
    children: true,
    contents: true,
    next: true,
    prev: true
  };
  djQ.fn.extend({
    has: function has(target) {
      var targets = djQ(target, this),
          l = targets.length;
      return this.filter(function () {
        var i = 0;

        for (; i < l; i++) {
          if (djQ.contains(this, targets[i])) {
            return true;
          }
        }
      });
    },
    closest: function closest(selectors, context) {
      var cur,
          i = 0,
          l = this.length,
          matched = [],
          targets = typeof selectors !== "string" && djQ(selectors); // Positional selectors never match, since there's no _selection_ context

      if (!rneedsContext.test(selectors)) {
        for (; i < l; i++) {
          for (cur = this[i]; cur && cur !== context; cur = cur.parentNode) {
            // Always skip document fragments
            if (cur.nodeType < 11 && (targets ? targets.index(cur) > -1 : // Don't pass non-elements to Sizzle
            cur.nodeType === 1 && djQ.find.matchesSelector(cur, selectors))) {
              matched.push(cur);
              break;
            }
          }
        }
      }

      return this.pushStack(matched.length > 1 ? djQ.uniqueSort(matched) : matched);
    },
    // Determine the position of an element within the set
    index: function index(elem) {
      // No argument, return index in parent
      if (!elem) {
        return this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
      } // Index in selector


      if (typeof elem === "string") {
        return indexOf.call(djQ(elem), this[0]);
      } // Locate the position of the desired element


      return indexOf.call(this, // If it receives a jQuery object, the first element is used
      elem.jquery ? elem[0] : elem);
    },
    add: function add(selector, context) {
      return this.pushStack(djQ.uniqueSort(djQ.merge(this.get(), djQ(selector, context))));
    },
    addBack: function addBack(selector) {
      return this.add(selector == null ? this.prevObject : this.prevObject.filter(selector));
    }
  });

  function sibling(cur, dir) {
    while ((cur = cur[dir]) && cur.nodeType !== 1) {}

    return cur;
  }

  djQ.each({
    parent: function parent(elem) {
      var parent = elem.parentNode;
      return parent && parent.nodeType !== 11 ? parent : null;
    },
    parents: function parents(elem) {
      return dir(elem, "parentNode");
    },
    parentsUntil: function parentsUntil(elem, _i, until) {
      return dir(elem, "parentNode", until);
    },
    next: function next(elem) {
      return sibling(elem, "nextSibling");
    },
    prev: function prev(elem) {
      return sibling(elem, "previousSibling");
    },
    nextAll: function nextAll(elem) {
      return dir(elem, "nextSibling");
    },
    prevAll: function prevAll(elem) {
      return dir(elem, "previousSibling");
    },
    nextUntil: function nextUntil(elem, _i, until) {
      return dir(elem, "nextSibling", until);
    },
    prevUntil: function prevUntil(elem, _i, until) {
      return dir(elem, "previousSibling", until);
    },
    siblings: function siblings(elem) {
      return _siblings((elem.parentNode || {}).firstChild, elem);
    },
    children: function children(elem) {
      return _siblings(elem.firstChild);
    },
    contents: function contents(elem) {
      if (elem.contentDocument != null && // Support: IE 11+
      // <object> elements with no `data` attribute has an object
      // `contentDocument` with a `null` prototype.
      getProto(elem.contentDocument)) {
        return elem.contentDocument;
      } // Support: IE 9 - 11 only, iOS 7 only, Android Browser <=4.3 only
      // Treat the template element as a regular one in browsers that
      // don't support it.


      if (nodeName(elem, "template")) {
        elem = elem.content || elem;
      }

      return djQ.merge([], elem.childNodes);
    }
  }, function (name, fn) {
    djQ.fn[name] = function (until, selector) {
      var matched = djQ.map(this, fn, until);

      if (name.slice(-5) !== "Until") {
        selector = until;
      }

      if (selector && typeof selector === "string") {
        matched = djQ.filter(selector, matched);
      }

      if (this.length > 1) {
        // Remove duplicates
        if (!guaranteedUnique[name]) {
          djQ.uniqueSort(matched);
        } // Reverse order for parents* and prev-derivatives


        if (rparentsprev.test(name)) {
          matched.reverse();
        }
      }

      return this.pushStack(matched);
    };
  });
  var rnothtmlwhite = /[^\x20\t\r\n\f]+/g; // Convert String-formatted options into Object-formatted ones

  function createOptions(options) {
    var object = {};
    djQ.each(options.match(rnothtmlwhite) || [], function (_, flag) {
      object[flag] = true;
    });
    return object;
  }
  /*
   * Create a callback list using the following parameters:
   *
   *	options: an optional list of space-separated options that will change how
   *			the callback list behaves or a more traditional option object
   *
   * By default a callback list will act like an event callback list and can be
   * "fired" multiple times.
   *
   * Possible options:
   *
   *	once:			will ensure the callback list can only be fired once (like a Deferred)
   *
   *	memory:			will keep track of previous values and will call any callback added
   *					after the list has been fired right away with the latest "memorized"
   *					values (like a Deferred)
   *
   *	unique:			will ensure a callback can only be added once (no duplicate in the list)
   *
   *	stopOnFalse:	interrupt callings when a callback returns false
   *
   */


  djQ.Callbacks = function (options) {
    // Convert options from String-formatted to Object-formatted if needed
    // (we check in cache first)
    options = typeof options === "string" ? createOptions(options) : djQ.extend({}, options);

    var // Flag to know if list is currently firing
    firing,
        // Last fire value for non-forgettable lists
    memory,
        // Flag to know if list was already fired
    _fired,
        // Flag to prevent firing
    _locked,
        // Actual callback list
    list = [],
        // Queue of execution data for repeatable lists
    queue = [],
        // Index of currently firing callback (modified by add/remove as needed)
    firingIndex = -1,
        // Fire callbacks
    fire = function fire() {
      // Enforce single-firing
      _locked = _locked || options.once; // Execute callbacks for all pending executions,
      // respecting firingIndex overrides and runtime changes

      _fired = firing = true;

      for (; queue.length; firingIndex = -1) {
        memory = queue.shift();

        while (++firingIndex < list.length) {
          // Run callback and check for early termination
          if (list[firingIndex].apply(memory[0], memory[1]) === false && options.stopOnFalse) {
            // Jump to end and forget the data so .add doesn't re-fire
            firingIndex = list.length;
            memory = false;
          }
        }
      } // Forget the data if we're done with it


      if (!options.memory) {
        memory = false;
      }

      firing = false; // Clean up if we're done firing for good

      if (_locked) {
        // Keep an empty list if we have data for future add calls
        if (memory) {
          list = []; // Otherwise, this object is spent
        } else {
          list = "";
        }
      }
    },
        // Actual Callbacks object
    self = {
      // Add a callback or a collection of callbacks to the list
      add: function add() {
        if (list) {
          // If we have memory from a past run, we should fire after adding
          if (memory && !firing) {
            firingIndex = list.length - 1;
            queue.push(memory);
          }

          (function add(args) {
            djQ.each(args, function (_, arg) {
              if (isFunction(arg)) {
                if (!options.unique || !self.has(arg)) {
                  list.push(arg);
                }
              } else if (arg && arg.length && toType(arg) !== "string") {
                // Inspect recursively
                add(arg);
              }
            });
          })(arguments);

          if (memory && !firing) {
            fire();
          }
        }

        return this;
      },
      // Remove a callback from the list
      remove: function remove() {
        djQ.each(arguments, function (_, arg) {
          var index;

          while ((index = djQ.inArray(arg, list, index)) > -1) {
            list.splice(index, 1); // Handle firing indexes

            if (index <= firingIndex) {
              firingIndex--;
            }
          }
        });
        return this;
      },
      // Check if a given callback is in the list.
      // If no argument is given, return whether or not list has callbacks attached.
      has: function has(fn) {
        return fn ? djQ.inArray(fn, list) > -1 : list.length > 0;
      },
      // Remove all callbacks from the list
      empty: function empty() {
        if (list) {
          list = [];
        }

        return this;
      },
      // Disable .fire and .add
      // Abort any current/pending executions
      // Clear all callbacks and values
      disable: function disable() {
        _locked = queue = [];
        list = memory = "";
        return this;
      },
      disabled: function disabled() {
        return !list;
      },
      // Disable .fire
      // Also disable .add unless we have memory (since it would have no effect)
      // Abort any pending executions
      lock: function lock() {
        _locked = queue = [];

        if (!memory && !firing) {
          list = memory = "";
        }

        return this;
      },
      locked: function locked() {
        return !!_locked;
      },
      // Call all callbacks with the given context and arguments
      fireWith: function fireWith(context, args) {
        if (!_locked) {
          args = args || [];
          args = [context, args.slice ? args.slice() : args];
          queue.push(args);

          if (!firing) {
            fire();
          }
        }

        return this;
      },
      // Call all the callbacks with the given arguments
      fire: function fire() {
        self.fireWith(this, arguments);
        return this;
      },
      // To know if the callbacks have already been called at least once
      fired: function fired() {
        return !!_fired;
      }
    };

    return self;
  };

  function Identity(v) {
    return v;
  }

  function Thrower(ex) {
    throw ex;
  }

  function adoptValue(value, resolve, reject, noValue) {
    var method;

    try {
      // Check for promise aspect first to privilege synchronous behavior
      if (value && isFunction(method = value.promise)) {
        method.call(value).done(resolve).fail(reject); // Other thenables
      } else if (value && isFunction(method = value.then)) {
        method.call(value, resolve, reject); // Other non-thenables
      } else {
        // Control `resolve` arguments by letting Array#slice cast boolean `noValue` to integer:
        // * false: [ value ].slice( 0 ) => resolve( value )
        // * true: [ value ].slice( 1 ) => resolve()
        resolve.apply(undefined, [value].slice(noValue));
      } // For Promises/A+, convert exceptions into rejections
      // Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
      // Deferred#then to conditionally suppress rejection.

    } catch (value) {
      // Support: Android 4.0 only
      // Strict mode functions invoked without .call/.apply get global-object context
      reject.apply(undefined, [value]);
    }
  }

  djQ.extend({
    Deferred: function Deferred(func) {
      var tuples = [// action, add listener, callbacks,
      // ... .then handlers, argument index, [final state]
      ["notify", "progress", djQ.Callbacks("memory"), djQ.Callbacks("memory"), 2], ["resolve", "done", djQ.Callbacks("once memory"), djQ.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", djQ.Callbacks("once memory"), djQ.Callbacks("once memory"), 1, "rejected"]],
          _state = "pending",
          _promise = {
        state: function state() {
          return _state;
        },
        always: function always() {
          deferred.done(arguments).fail(arguments);
          return this;
        },
        "catch": function _catch(fn) {
          return _promise.then(null, fn);
        },
        // Keep pipe for back-compat
        pipe: function pipe()
        /* fnDone, fnFail, fnProgress */
        {
          var fns = arguments;
          return djQ.Deferred(function (newDefer) {
            djQ.each(tuples, function (_i, tuple) {
              // Map tuples (progress, done, fail) to arguments (done, fail, progress)
              var fn = isFunction(fns[tuple[4]]) && fns[tuple[4]]; // deferred.progress(function() { bind to newDefer or newDefer.notify })
              // deferred.done(function() { bind to newDefer or newDefer.resolve })
              // deferred.fail(function() { bind to newDefer or newDefer.reject })

              deferred[tuple[1]](function () {
                var returned = fn && fn.apply(this, arguments);

                if (returned && isFunction(returned.promise)) {
                  returned.promise().progress(newDefer.notify).done(newDefer.resolve).fail(newDefer.reject);
                } else {
                  newDefer[tuple[0] + "With"](this, fn ? [returned] : arguments);
                }
              });
            });
            fns = null;
          }).promise();
        },
        then: function then(onFulfilled, onRejected, onProgress) {
          var maxDepth = 0;

          function resolve(depth, deferred, handler, special) {
            return function () {
              var that = this,
                  args = arguments,
                  mightThrow = function mightThrow() {
                var returned, then; // Support: Promises/A+ section 2.3.3.3.3
                // https://promisesaplus.com/#point-59
                // Ignore double-resolution attempts

                if (depth < maxDepth) {
                  return;
                }

                returned = handler.apply(that, args); // Support: Promises/A+ section 2.3.1
                // https://promisesaplus.com/#point-48

                if (returned === deferred.promise()) {
                  throw new TypeError("Thenable self-resolution");
                } // Support: Promises/A+ sections 2.3.3.1, 3.5
                // https://promisesaplus.com/#point-54
                // https://promisesaplus.com/#point-75
                // Retrieve `then` only once


                then = returned && ( // Support: Promises/A+ section 2.3.4
                // https://promisesaplus.com/#point-64
                // Only check objects and functions for thenability
                _typeof(returned) === "object" || typeof returned === "function") && returned.then; // Handle a returned thenable

                if (isFunction(then)) {
                  // Special processors (notify) just wait for resolution
                  if (special) {
                    then.call(returned, resolve(maxDepth, deferred, Identity, special), resolve(maxDepth, deferred, Thrower, special)); // Normal processors (resolve) also hook into progress
                  } else {
                    // ...and disregard older resolution values
                    maxDepth++;
                    then.call(returned, resolve(maxDepth, deferred, Identity, special), resolve(maxDepth, deferred, Thrower, special), resolve(maxDepth, deferred, Identity, deferred.notifyWith));
                  } // Handle all other returned values

                } else {
                  // Only substitute handlers pass on context
                  // and multiple values (non-spec behavior)
                  if (handler !== Identity) {
                    that = undefined;
                    args = [returned];
                  } // Process the value(s)
                  // Default process is resolve


                  (special || deferred.resolveWith)(that, args);
                }
              },
                  // Only normal processors (resolve) catch and reject exceptions
              process = special ? mightThrow : function () {
                try {
                  mightThrow();
                } catch (e) {
                  if (djQ.Deferred.exceptionHook) {
                    djQ.Deferred.exceptionHook(e, process.stackTrace);
                  } // Support: Promises/A+ section 2.3.3.3.4.1
                  // https://promisesaplus.com/#point-61
                  // Ignore post-resolution exceptions


                  if (depth + 1 >= maxDepth) {
                    // Only substitute handlers pass on context
                    // and multiple values (non-spec behavior)
                    if (handler !== Thrower) {
                      that = undefined;
                      args = [e];
                    }

                    deferred.rejectWith(that, args);
                  }
                }
              }; // Support: Promises/A+ section 2.3.3.3.1
              // https://promisesaplus.com/#point-57
              // Re-resolve promises immediately to dodge false rejection from
              // subsequent errors


              if (depth) {
                process();
              } else {
                // Call an optional hook to record the stack, in case of exception
                // since it's otherwise lost when execution goes async
                if (djQ.Deferred.getStackHook) {
                  process.stackTrace = djQ.Deferred.getStackHook();
                }

                window.setTimeout(process);
              }
            };
          }

          return djQ.Deferred(function (newDefer) {
            // progress_handlers.add( ... )
            tuples[0][3].add(resolve(0, newDefer, isFunction(onProgress) ? onProgress : Identity, newDefer.notifyWith)); // fulfilled_handlers.add( ... )

            tuples[1][3].add(resolve(0, newDefer, isFunction(onFulfilled) ? onFulfilled : Identity)); // rejected_handlers.add( ... )

            tuples[2][3].add(resolve(0, newDefer, isFunction(onRejected) ? onRejected : Thrower));
          }).promise();
        },
        // Get a promise for this deferred
        // If obj is provided, the promise aspect is added to the object
        promise: function promise(obj) {
          return obj != null ? djQ.extend(obj, _promise) : _promise;
        }
      },
          deferred = {}; // Add list-specific methods

      djQ.each(tuples, function (i, tuple) {
        var list = tuple[2],
            stateString = tuple[5]; // promise.progress = list.add
        // promise.done = list.add
        // promise.fail = list.add

        _promise[tuple[1]] = list.add; // Handle state

        if (stateString) {
          list.add(function () {
            // state = "resolved" (i.e., fulfilled)
            // state = "rejected"
            _state = stateString;
          }, // rejected_callbacks.disable
          // fulfilled_callbacks.disable
          tuples[3 - i][2].disable, // rejected_handlers.disable
          // fulfilled_handlers.disable
          tuples[3 - i][3].disable, // progress_callbacks.lock
          tuples[0][2].lock, // progress_handlers.lock
          tuples[0][3].lock);
        } // progress_handlers.fire
        // fulfilled_handlers.fire
        // rejected_handlers.fire


        list.add(tuple[3].fire); // deferred.notify = function() { deferred.notifyWith(...) }
        // deferred.resolve = function() { deferred.resolveWith(...) }
        // deferred.reject = function() { deferred.rejectWith(...) }

        deferred[tuple[0]] = function () {
          deferred[tuple[0] + "With"](this === deferred ? undefined : this, arguments);
          return this;
        }; // deferred.notifyWith = list.fireWith
        // deferred.resolveWith = list.fireWith
        // deferred.rejectWith = list.fireWith


        deferred[tuple[0] + "With"] = list.fireWith;
      }); // Make the deferred a promise

      _promise.promise(deferred); // Call given func if any


      if (func) {
        func.call(deferred, deferred);
      } // All done!


      return deferred;
    },
    // Deferred helper
    when: function when(singleValue) {
      var // count of uncompleted subordinates
      remaining = arguments.length,
          // count of unprocessed arguments
      i = remaining,
          // subordinate fulfillment data
      resolveContexts = Array(i),
          resolveValues = _slice.call(arguments),
          // the master Deferred
      master = djQ.Deferred(),
          // subordinate callback factory
      updateFunc = function updateFunc(i) {
        return function (value) {
          resolveContexts[i] = this;
          resolveValues[i] = arguments.length > 1 ? _slice.call(arguments) : value;

          if (! --remaining) {
            master.resolveWith(resolveContexts, resolveValues);
          }
        };
      }; // Single- and empty arguments are adopted like Promise.resolve


      if (remaining <= 1) {
        adoptValue(singleValue, master.done(updateFunc(i)).resolve, master.reject, !remaining); // Use .then() to unwrap secondary thenables (cf. gh-3000)

        if (master.state() === "pending" || isFunction(resolveValues[i] && resolveValues[i].then)) {
          return master.then();
        }
      } // Multiple arguments are aggregated like Promise.all array elements


      while (i--) {
        adoptValue(resolveValues[i], updateFunc(i), master.reject);
      }

      return master.promise();
    }
  }); // These usually indicate a programmer mistake during development,
  // warn about them ASAP rather than swallowing them by default.

  var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;

  djQ.Deferred.exceptionHook = function (error, stack) {
    // Support: IE 8 - 9 only
    // Console exists when dev tools are open, which can happen at any time
    if (window.console && window.console.warn && error && rerrorNames.test(error.name)) {
      window.console.warn("jQuery.Deferred exception: " + error.message, error.stack, stack);
    }
  };

  djQ.readyException = function (error) {
    window.setTimeout(function () {
      throw error;
    });
  }; // The deferred used on DOM ready


  var readyList = djQ.Deferred();

  djQ.fn.ready = function (fn) {
    readyList.then(fn) // Wrap jQuery.readyException in a function so that the lookup
    // happens at the time of error handling instead of callback
    // registration.
    .catch(function (error) {
      djQ.readyException(error);
    });
    return this;
  };

  djQ.extend({
    // Is the DOM ready to be used? Set to true once it occurs.
    isReady: false,
    // A counter to track how many items to wait for before
    // the ready event fires. See #6781
    readyWait: 1,
    // Handle when the DOM is ready
    ready: function ready(wait) {
      // Abort if there are pending holds or we're already ready
      if (wait === true ? --djQ.readyWait : djQ.isReady) {
        return;
      } // Remember that the DOM is ready


      djQ.isReady = true; // If a normal DOM Ready event fired, decrement, and wait if need be

      if (wait !== true && --djQ.readyWait > 0) {
        return;
      } // If there are functions bound, to execute


      readyList.resolveWith(document, [djQ]);
    }
  });
  djQ.ready.then = readyList.then; // The ready event handler and self cleanup method

  function completed() {
    document.removeEventListener("DOMContentLoaded", completed);
    window.removeEventListener("load", completed);
    djQ.ready();
  } // Catch cases where $(document).ready() is called
  // after the browser event has already occurred.
  // Support: IE <=9 - 10 only
  // Older IE sometimes signals "interactive" too soon


  if (document.readyState === "complete" || document.readyState !== "loading" && !document.documentElement.doScroll) {
    // Handle it asynchronously to allow scripts the opportunity to delay ready
    window.setTimeout(djQ.ready);
  } else {
    // Use the handy event callback
    document.addEventListener("DOMContentLoaded", completed); // A fallback to window.onload, that will always work

    window.addEventListener("load", completed);
  } // Multifunctional method to get and set values of a collection
  // The value/s can optionally be executed if it's a function


  var access = function access(elems, fn, key, value, chainable, emptyGet, raw) {
    var i = 0,
        len = elems.length,
        bulk = key == null; // Sets many values

    if (toType(key) === "object") {
      chainable = true;

      for (i in key) {
        access(elems, fn, i, key[i], true, emptyGet, raw);
      } // Sets one value

    } else if (value !== undefined) {
      chainable = true;

      if (!isFunction(value)) {
        raw = true;
      }

      if (bulk) {
        // Bulk operations run against the entire set
        if (raw) {
          fn.call(elems, value);
          fn = null; // ...except when executing function values
        } else {
          bulk = fn;

          fn = function fn(elem, _key, value) {
            return bulk.call(djQ(elem), value);
          };
        }
      }

      if (fn) {
        for (; i < len; i++) {
          fn(elems[i], key, raw ? value : value.call(elems[i], i, fn(elems[i], key)));
        }
      }
    }

    if (chainable) {
      return elems;
    } // Gets


    if (bulk) {
      return fn.call(elems);
    }

    return len ? fn(elems[0], key) : emptyGet;
  }; // Matches dashed string for camelizing


  var rmsPrefix = /^-ms-/,
      rdashAlpha = /-([a-z])/g; // Used by camelCase as callback to replace()

  function fcamelCase(_all, letter) {
    return letter.toUpperCase();
  } // Convert dashed to camelCase; used by the css and data modules
  // Support: IE <=9 - 11, Edge 12 - 15
  // Microsoft forgot to hump their vendor prefix (#9572)


  function camelCase(string) {
    return string.replace(rmsPrefix, "ms-").replace(rdashAlpha, fcamelCase);
  }

  var acceptData = function acceptData(owner) {
    // Accepts only:
    //  - Node
    //    - Node.ELEMENT_NODE
    //    - Node.DOCUMENT_NODE
    //  - Object
    //    - Any
    return owner.nodeType === 1 || owner.nodeType === 9 || !+owner.nodeType;
  };

  function Data() {
    this.expando = djQ.expando + Data.uid++;
  }

  Data.uid = 1;
  Data.prototype = {
    cache: function cache(owner) {
      // Check if the owner object already has a cache
      var value = owner[this.expando]; // If not, create one

      if (!value) {
        value = {}; // We can accept data for non-element nodes in modern browsers,
        // but we should not, see #8335.
        // Always return an empty object.

        if (acceptData(owner)) {
          // If it is a node unlikely to be stringify-ed or looped over
          // use plain assignment
          if (owner.nodeType) {
            owner[this.expando] = value; // Otherwise secure it in a non-enumerable property
            // configurable must be true to allow the property to be
            // deleted when data is removed
          } else {
            Object.defineProperty(owner, this.expando, {
              value: value,
              configurable: true
            });
          }
        }
      }

      return value;
    },
    set: function set(owner, data, value) {
      var prop,
          cache = this.cache(owner); // Handle: [ owner, key, value ] args
      // Always use camelCase key (gh-2257)

      if (typeof data === "string") {
        cache[camelCase(data)] = value; // Handle: [ owner, { properties } ] args
      } else {
        // Copy the properties one-by-one to the cache object
        for (prop in data) {
          cache[camelCase(prop)] = data[prop];
        }
      }

      return cache;
    },
    get: function get(owner, key) {
      return key === undefined ? this.cache(owner) : // Always use camelCase key (gh-2257)
      owner[this.expando] && owner[this.expando][camelCase(key)];
    },
    access: function access(owner, key, value) {
      // In cases where either:
      //
      //   1. No key was specified
      //   2. A string key was specified, but no value provided
      //
      // Take the "read" path and allow the get method to determine
      // which value to return, respectively either:
      //
      //   1. The entire cache object
      //   2. The data stored at the key
      //
      if (key === undefined || key && typeof key === "string" && value === undefined) {
        return this.get(owner, key);
      } // When the key is not a string, or both a key and value
      // are specified, set or extend (existing objects) with either:
      //
      //   1. An object of properties
      //   2. A key and value
      //


      this.set(owner, key, value); // Since the "set" path can have two possible entry points
      // return the expected data based on which path was taken[*]

      return value !== undefined ? value : key;
    },
    remove: function remove(owner, key) {
      var i,
          cache = owner[this.expando];

      if (cache === undefined) {
        return;
      }

      if (key !== undefined) {
        // Support array or space separated string of keys
        if (Array.isArray(key)) {
          // If key is an array of keys...
          // We always set camelCase keys, so remove that.
          key = key.map(camelCase);
        } else {
          key = camelCase(key); // If a key with the spaces exists, use it.
          // Otherwise, create an array by matching non-whitespace

          key = key in cache ? [key] : key.match(rnothtmlwhite) || [];
        }

        i = key.length;

        while (i--) {
          delete cache[key[i]];
        }
      } // Remove the expando if there's no more data


      if (key === undefined || djQ.isEmptyObject(cache)) {
        // Support: Chrome <=35 - 45
        // Webkit & Blink performance suffers when deleting properties
        // from DOM nodes, so set to undefined instead
        // https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
        if (owner.nodeType) {
          owner[this.expando] = undefined;
        } else {
          delete owner[this.expando];
        }
      }
    },
    hasData: function hasData(owner) {
      var cache = owner[this.expando];
      return cache !== undefined && !djQ.isEmptyObject(cache);
    }
  };
  var dataPriv = new Data();
  var dataUser = new Data(); //	Implementation Summary
  //
  //	1. Enforce API surface and semantic compatibility with 1.9.x branch
  //	2. Improve the module's maintainability by reducing the storage
  //		paths to a single mechanism.
  //	3. Use the same single mechanism to support "private" and "user" data.
  //	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
  //	5. Avoid exposing implementation details on user objects (eg. expando properties)
  //	6. Provide a clear path for implementation upgrade to WeakMap in 2014

  var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
      rmultiDash = /[A-Z]/g;

  function getData(data) {
    if (data === "true") {
      return true;
    }

    if (data === "false") {
      return false;
    }

    if (data === "null") {
      return null;
    } // Only convert to a number if it doesn't change the string


    if (data === +data + "") {
      return +data;
    }

    if (rbrace.test(data)) {
      return JSON.parse(data);
    }

    return data;
  }

  function dataAttr(elem, key, data) {
    var name; // If nothing was found internally, try to fetch any
    // data from the HTML5 data-* attribute

    if (data === undefined && elem.nodeType === 1) {
      name = "data-" + key.replace(rmultiDash, "-$&").toLowerCase();
      data = elem.getAttribute(name);

      if (typeof data === "string") {
        try {
          data = getData(data);
        } catch (e) {} // Make sure we set the data so it isn't changed later


        dataUser.set(elem, key, data);
      } else {
        data = undefined;
      }
    }

    return data;
  }

  djQ.extend({
    hasData: function hasData(elem) {
      return dataUser.hasData(elem) || dataPriv.hasData(elem);
    },
    data: function data(elem, name, _data) {
      return dataUser.access(elem, name, _data);
    },
    removeData: function removeData(elem, name) {
      dataUser.remove(elem, name);
    },
    // TODO: Now that all calls to _data and _removeData have been replaced
    // with direct calls to dataPriv methods, these can be deprecated.
    _data: function _data(elem, name, data) {
      return dataPriv.access(elem, name, data);
    },
    _removeData: function _removeData(elem, name) {
      dataPriv.remove(elem, name);
    }
  });
  djQ.fn.extend({
    data: function data(key, value) {
      var i,
          name,
          data,
          elem = this[0],
          attrs = elem && elem.attributes; // Gets all values

      if (key === undefined) {
        if (this.length) {
          data = dataUser.get(elem);

          if (elem.nodeType === 1 && !dataPriv.get(elem, "hasDataAttrs")) {
            i = attrs.length;

            while (i--) {
              // Support: IE 11 only
              // The attrs elements can be null (#14894)
              if (attrs[i]) {
                name = attrs[i].name;

                if (name.indexOf("data-") === 0) {
                  name = camelCase(name.slice(5));
                  dataAttr(elem, name, data[name]);
                }
              }
            }

            dataPriv.set(elem, "hasDataAttrs", true);
          }
        }

        return data;
      } // Sets multiple values


      if (_typeof(key) === "object") {
        return this.each(function () {
          dataUser.set(this, key);
        });
      }

      return access(this, function (value) {
        var data; // The calling jQuery object (element matches) is not empty
        // (and therefore has an element appears at this[ 0 ]) and the
        // `value` parameter was not undefined. An empty jQuery object
        // will result in `undefined` for elem = this[ 0 ] which will
        // throw an exception if an attempt to read a data cache is made.

        if (elem && value === undefined) {
          // Attempt to get data from the cache
          // The key will always be camelCased in Data
          data = dataUser.get(elem, key);

          if (data !== undefined) {
            return data;
          } // Attempt to "discover" the data in
          // HTML5 custom data-* attrs


          data = dataAttr(elem, key);

          if (data !== undefined) {
            return data;
          } // We tried really hard, but the data doesn't exist.


          return;
        } // Set the data...


        this.each(function () {
          // We always store the camelCased key
          dataUser.set(this, key, value);
        });
      }, null, value, arguments.length > 1, null, true);
    },
    removeData: function removeData(key) {
      return this.each(function () {
        dataUser.remove(this, key);
      });
    }
  });
  djQ.extend({
    queue: function queue(elem, type, data) {
      var queue;

      if (elem) {
        type = (type || "fx") + "queue";
        queue = dataPriv.get(elem, type); // Speed up dequeue by getting out quickly if this is just a lookup

        if (data) {
          if (!queue || Array.isArray(data)) {
            queue = dataPriv.access(elem, type, djQ.makeArray(data));
          } else {
            queue.push(data);
          }
        }

        return queue || [];
      }
    },
    dequeue: function dequeue(elem, type) {
      type = type || "fx";

      var queue = djQ.queue(elem, type),
          startLength = queue.length,
          fn = queue.shift(),
          hooks = djQ._queueHooks(elem, type),
          next = function next() {
        djQ.dequeue(elem, type);
      }; // If the fx queue is dequeued, always remove the progress sentinel


      if (fn === "inprogress") {
        fn = queue.shift();
        startLength--;
      }

      if (fn) {
        // Add a progress sentinel to prevent the fx queue from being
        // automatically dequeued
        if (type === "fx") {
          queue.unshift("inprogress");
        } // Clear up the last queue stop function


        delete hooks.stop;
        fn.call(elem, next, hooks);
      }

      if (!startLength && hooks) {
        hooks.empty.fire();
      }
    },
    // Not public - generate a queueHooks object, or return the current one
    _queueHooks: function _queueHooks(elem, type) {
      var key = type + "queueHooks";
      return dataPriv.get(elem, key) || dataPriv.access(elem, key, {
        empty: djQ.Callbacks("once memory").add(function () {
          dataPriv.remove(elem, [type + "queue", key]);
        })
      });
    }
  });
  djQ.fn.extend({
    queue: function queue(type, data) {
      var setter = 2;

      if (typeof type !== "string") {
        data = type;
        type = "fx";
        setter--;
      }

      if (arguments.length < setter) {
        return djQ.queue(this[0], type);
      }

      return data === undefined ? this : this.each(function () {
        var queue = djQ.queue(this, type, data); // Ensure a hooks for this queue

        djQ._queueHooks(this, type);

        if (type === "fx" && queue[0] !== "inprogress") {
          djQ.dequeue(this, type);
        }
      });
    },
    dequeue: function dequeue(type) {
      return this.each(function () {
        djQ.dequeue(this, type);
      });
    },
    clearQueue: function clearQueue(type) {
      return this.queue(type || "fx", []);
    },
    // Get a promise resolved when queues of a certain type
    // are emptied (fx is the type by default)
    promise: function promise(type, obj) {
      var tmp,
          count = 1,
          defer = djQ.Deferred(),
          elements = this,
          i = this.length,
          resolve = function resolve() {
        if (! --count) {
          defer.resolveWith(elements, [elements]);
        }
      };

      if (typeof type !== "string") {
        obj = type;
        type = undefined;
      }

      type = type || "fx";

      while (i--) {
        tmp = dataPriv.get(elements[i], type + "queueHooks");

        if (tmp && tmp.empty) {
          count++;
          tmp.empty.add(resolve);
        }
      }

      resolve();
      return defer.promise(obj);
    }
  });
  var pnum = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source;
  var rcssNum = new RegExp("^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i");
  var cssExpand = ["Top", "Right", "Bottom", "Left"];
  var documentElement = document.documentElement;

  var isAttached = function isAttached(elem) {
    return djQ.contains(elem.ownerDocument, elem);
  },
      composed = {
    composed: true
  }; // Support: IE 9 - 11+, Edge 12 - 18+, iOS 10.0 - 10.2 only
  // Check attachment across shadow DOM boundaries when possible (gh-3504)
  // Support: iOS 10.0-10.2 only
  // Early iOS 10 versions support `attachShadow` but not `getRootNode`,
  // leading to errors. We need to check for `getRootNode`.


  if (documentElement.getRootNode) {
    isAttached = function isAttached(elem) {
      return djQ.contains(elem.ownerDocument, elem) || elem.getRootNode(composed) === elem.ownerDocument;
    };
  }

  var isHiddenWithinTree = function isHiddenWithinTree(elem, el) {
    // isHiddenWithinTree might be called from jQuery#filter function;
    // in that case, element will be second argument
    elem = el || elem; // Inline style trumps all

    return elem.style.display === "none" || elem.style.display === "" && // Otherwise, check computed style
    // Support: Firefox <=43 - 45
    // Disconnected elements can have computed display: none, so first confirm that elem is
    // in the document.
    isAttached(elem) && djQ.css(elem, "display") === "none";
  };

  function adjustCSS(elem, prop, valueParts, tween) {
    var adjusted,
        scale,
        maxIterations = 20,
        currentValue = tween ? function () {
      return tween.cur();
    } : function () {
      return djQ.css(elem, prop, "");
    },
        initial = currentValue(),
        unit = valueParts && valueParts[3] || (djQ.cssNumber[prop] ? "" : "px"),
        // Starting value computation is required for potential unit mismatches
    initialInUnit = elem.nodeType && (djQ.cssNumber[prop] || unit !== "px" && +initial) && rcssNum.exec(djQ.css(elem, prop));

    if (initialInUnit && initialInUnit[3] !== unit) {
      // Support: Firefox <=54
      // Halve the iteration target value to prevent interference from CSS upper bounds (gh-2144)
      initial = initial / 2; // Trust units reported by djQ.css

      unit = unit || initialInUnit[3]; // Iteratively approximate from a nonzero starting point

      initialInUnit = +initial || 1;

      while (maxIterations--) {
        // Evaluate and update our best guess (doubling guesses that zero out).
        // Finish if the scale equals or crosses 1 (making the old*new product non-positive).
        djQ.style(elem, prop, initialInUnit + unit);

        if ((1 - scale) * (1 - (scale = currentValue() / initial || 0.5)) <= 0) {
          maxIterations = 0;
        }

        initialInUnit = initialInUnit / scale;
      }

      initialInUnit = initialInUnit * 2;
      djQ.style(elem, prop, initialInUnit + unit); // Make sure we update the tween properties later on

      valueParts = valueParts || [];
    }

    if (valueParts) {
      initialInUnit = +initialInUnit || +initial || 0; // Apply relative offset (+=/-=) if specified

      adjusted = valueParts[1] ? initialInUnit + (valueParts[1] + 1) * valueParts[2] : +valueParts[2];

      if (tween) {
        tween.unit = unit;
        tween.start = initialInUnit;
        tween.end = adjusted;
      }
    }

    return adjusted;
  }

  var defaultDisplayMap = {};

  function getDefaultDisplay(elem) {
    var temp,
        doc = elem.ownerDocument,
        nodeName = elem.nodeName,
        display = defaultDisplayMap[nodeName];

    if (display) {
      return display;
    }

    temp = doc.body.appendChild(doc.createElement(nodeName));
    display = djQ.css(temp, "display");
    temp.parentNode.removeChild(temp);

    if (display === "none") {
      display = "block";
    }

    defaultDisplayMap[nodeName] = display;
    return display;
  }

  function showHide(elements, show) {
    var display,
        elem,
        values = [],
        index = 0,
        length = elements.length; // Determine new display value for elements that need to change

    for (; index < length; index++) {
      elem = elements[index];

      if (!elem.style) {
        continue;
      }

      display = elem.style.display;

      if (show) {
        // Since we force visibility upon cascade-hidden elements, an immediate (and slow)
        // check is required in this first loop unless we have a nonempty display value (either
        // inline or about-to-be-restored)
        if (display === "none") {
          values[index] = dataPriv.get(elem, "display") || null;

          if (!values[index]) {
            elem.style.display = "";
          }
        }

        if (elem.style.display === "" && isHiddenWithinTree(elem)) {
          values[index] = getDefaultDisplay(elem);
        }
      } else {
        if (display !== "none") {
          values[index] = "none"; // Remember what we're overwriting

          dataPriv.set(elem, "display", display);
        }
      }
    } // Set the display of the elements in a second loop to avoid constant reflow


    for (index = 0; index < length; index++) {
      if (values[index] != null) {
        elements[index].style.display = values[index];
      }
    }

    return elements;
  }

  djQ.fn.extend({
    show: function show() {
      return showHide(this, true);
    },
    hide: function hide() {
      return showHide(this);
    },
    toggle: function toggle(state) {
      if (typeof state === "boolean") {
        return state ? this.show() : this.hide();
      }

      return this.each(function () {
        if (isHiddenWithinTree(this)) {
          djQ(this).show();
        } else {
          djQ(this).hide();
        }
      });
    }
  });
  var rcheckableType = /^(?:checkbox|radio)$/i;
  var rtagName = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i;
  var rscriptType = /^$|^module$|\/(?:java|ecma)script/i;

  (function () {
    var fragment = document.createDocumentFragment(),
        div = fragment.appendChild(document.createElement("div")),
        input = document.createElement("input"); // Support: Android 4.0 - 4.3 only
    // Check state lost if the name is set (#11217)
    // Support: Windows Web Apps (WWA)
    // `name` and `type` must use .setAttribute for WWA (#14901)

    input.setAttribute("type", "radio");
    input.setAttribute("checked", "checked");
    input.setAttribute("name", "t");
    div.appendChild(input); // Support: Android <=4.1 only
    // Older WebKit doesn't clone checked state correctly in fragments

    support.checkClone = div.cloneNode(true).cloneNode(true).lastChild.checked; // Support: IE <=11 only
    // Make sure textarea (and checkbox) defaultValue is properly cloned

    div.innerHTML = "<textarea>x</textarea>";
    support.noCloneChecked = !!div.cloneNode(true).lastChild.defaultValue; // Support: IE <=9 only
    // IE <=9 replaces <option> tags with their contents when inserted outside of
    // the select element.

    div.innerHTML = "<option></option>";
    support.option = !!div.lastChild;
  })(); // We have to close these tags to support XHTML (#13200)


  var wrapMap = {
    // XHTML parsers do not magically insert elements in the
    // same way that tag soup parsers do. So we cannot shorten
    // this by omitting <tbody> or other required elements.
    thead: [1, "<table>", "</table>"],
    col: [2, "<table><colgroup>", "</colgroup></table>"],
    tr: [2, "<table><tbody>", "</tbody></table>"],
    td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
    _default: [0, "", ""]
  };
  wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
  wrapMap.th = wrapMap.td; // Support: IE <=9 only

  if (!support.option) {
    wrapMap.optgroup = wrapMap.option = [1, "<select multiple='multiple'>", "</select>"];
  }

  function getAll(context, tag) {
    // Support: IE <=9 - 11 only
    // Use typeof to avoid zero-argument method invocation on host objects (#15151)
    var ret;

    if (typeof context.getElementsByTagName !== "undefined") {
      ret = context.getElementsByTagName(tag || "*");
    } else if (typeof context.querySelectorAll !== "undefined") {
      ret = context.querySelectorAll(tag || "*");
    } else {
      ret = [];
    }

    if (tag === undefined || tag && nodeName(context, tag)) {
      return djQ.merge([context], ret);
    }

    return ret;
  } // Mark scripts as having already been evaluated


  function setGlobalEval(elems, refElements) {
    var i = 0,
        l = elems.length;

    for (; i < l; i++) {
      dataPriv.set(elems[i], "globalEval", !refElements || dataPriv.get(refElements[i], "globalEval"));
    }
  }

  var rhtml = /<|&#?\w+;/;

  function buildFragment(elems, context, scripts, selection, ignored) {
    var elem,
        tmp,
        tag,
        wrap,
        attached,
        j,
        fragment = context.createDocumentFragment(),
        nodes = [],
        i = 0,
        l = elems.length;

    for (; i < l; i++) {
      elem = elems[i];

      if (elem || elem === 0) {
        // Add nodes directly
        if (toType(elem) === "object") {
          // Support: Android <=4.0 only, PhantomJS 1 only
          // push.apply(_, arraylike) throws on ancient WebKit
          djQ.merge(nodes, elem.nodeType ? [elem] : elem); // Convert non-html into a text node
        } else if (!rhtml.test(elem)) {
          nodes.push(context.createTextNode(elem)); // Convert html into DOM nodes
        } else {
          tmp = tmp || fragment.appendChild(context.createElement("div")); // Deserialize a standard representation

          tag = (rtagName.exec(elem) || ["", ""])[1].toLowerCase();
          wrap = wrapMap[tag] || wrapMap._default;
          tmp.innerHTML = wrap[1] + djQ.htmlPrefilter(elem) + wrap[2]; // Descend through wrappers to the right content

          j = wrap[0];

          while (j--) {
            tmp = tmp.lastChild;
          } // Support: Android <=4.0 only, PhantomJS 1 only
          // push.apply(_, arraylike) throws on ancient WebKit


          djQ.merge(nodes, tmp.childNodes); // Remember the top-level container

          tmp = fragment.firstChild; // Ensure the created nodes are orphaned (#12392)

          tmp.textContent = "";
        }
      }
    } // Remove wrapper from fragment


    fragment.textContent = "";
    i = 0;

    while (elem = nodes[i++]) {
      // Skip elements already in the context collection (trac-4087)
      if (selection && djQ.inArray(elem, selection) > -1) {
        if (ignored) {
          ignored.push(elem);
        }

        continue;
      }

      attached = isAttached(elem); // Append to fragment

      tmp = getAll(fragment.appendChild(elem), "script"); // Preserve script evaluation history

      if (attached) {
        setGlobalEval(tmp);
      } // Capture executables


      if (scripts) {
        j = 0;

        while (elem = tmp[j++]) {
          if (rscriptType.test(elem.type || "")) {
            scripts.push(elem);
          }
        }
      }
    }

    return fragment;
  }

  var rkeyEvent = /^key/,
      rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
      rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

  function returnTrue() {
    return true;
  }

  function returnFalse() {
    return false;
  } // Support: IE <=9 - 11+
  // focus() and blur() are asynchronous, except when they are no-op.
  // So expect focus to be synchronous when the element is already active,
  // and blur to be synchronous when the element is not already active.
  // (focus and blur are always synchronous in other supported browsers,
  // this just defines when we can count on it).


  function expectSync(elem, type) {
    return elem === safeActiveElement() === (type === "focus");
  } // Support: IE <=9 only
  // Accessing document.activeElement can throw unexpectedly
  // https://bugs.jquery.com/ticket/13393


  function safeActiveElement() {
    try {
      return document.activeElement;
    } catch (err) {}
  }

  function _on(elem, types, selector, data, fn, one) {
    var origFn, type; // Types can be a map of types/handlers

    if (_typeof(types) === "object") {
      // ( types-Object, selector, data )
      if (typeof selector !== "string") {
        // ( types-Object, data )
        data = data || selector;
        selector = undefined;
      }

      for (type in types) {
        _on(elem, type, selector, data, types[type], one);
      }

      return elem;
    }

    if (data == null && fn == null) {
      // ( types, fn )
      fn = selector;
      data = selector = undefined;
    } else if (fn == null) {
      if (typeof selector === "string") {
        // ( types, selector, fn )
        fn = data;
        data = undefined;
      } else {
        // ( types, data, fn )
        fn = data;
        data = selector;
        selector = undefined;
      }
    }

    if (fn === false) {
      fn = returnFalse;
    } else if (!fn) {
      return elem;
    }

    if (one === 1) {
      origFn = fn;

      fn = function fn(event) {
        // Can use an empty set, since event contains the info
        djQ().off(event);
        return origFn.apply(this, arguments);
      }; // Use same guid so caller can remove using origFn


      fn.guid = origFn.guid || (origFn.guid = djQ.guid++);
    }

    return elem.each(function () {
      djQ.event.add(this, types, fn, data, selector);
    });
  }
  /*
   * Helper functions for managing events -- not part of the public interface.
   * Props to Dean Edwards' addEvent library for many of the ideas.
   */


  djQ.event = {
    global: {},
    add: function add(elem, types, handler, data, selector) {
      var handleObjIn,
          eventHandle,
          tmp,
          events,
          t,
          handleObj,
          special,
          handlers,
          type,
          namespaces,
          origType,
          elemData = dataPriv.get(elem); // Only attach events to objects that accept data

      if (!acceptData(elem)) {
        return;
      } // Caller can pass in an object of custom data in lieu of the handler


      if (handler.handler) {
        handleObjIn = handler;
        handler = handleObjIn.handler;
        selector = handleObjIn.selector;
      } // Ensure that invalid selectors throw exceptions at attach time
      // Evaluate against documentElement in case elem is a non-element node (e.g., document)


      if (selector) {
        djQ.find.matchesSelector(documentElement, selector);
      } // Make sure that the handler has a unique ID, used to find/remove it later


      if (!handler.guid) {
        handler.guid = djQ.guid++;
      } // Init the element's event structure and main handler, if this is the first


      if (!(events = elemData.events)) {
        events = elemData.events = Object.create(null);
      }

      if (!(eventHandle = elemData.handle)) {
        eventHandle = elemData.handle = function (e) {
          // Discard the second event of a djQ.event.trigger() and
          // when an event is called after a page has unloaded
          return typeof djQ !== "undefined" && djQ.event.triggered !== e.type ? djQ.event.dispatch.apply(elem, arguments) : undefined;
        };
      } // Handle multiple events separated by a space


      types = (types || "").match(rnothtmlwhite) || [""];
      t = types.length;

      while (t--) {
        tmp = rtypenamespace.exec(types[t]) || [];
        type = origType = tmp[1];
        namespaces = (tmp[2] || "").split(".").sort(); // There *must* be a type, no attaching namespace-only handlers

        if (!type) {
          continue;
        } // If event changes its type, use the special event handlers for the changed type


        special = djQ.event.special[type] || {}; // If selector defined, determine special event api type, otherwise given type

        type = (selector ? special.delegateType : special.bindType) || type; // Update special based on newly reset type

        special = djQ.event.special[type] || {}; // handleObj is passed to all event handlers

        handleObj = djQ.extend({
          type: type,
          origType: origType,
          data: data,
          handler: handler,
          guid: handler.guid,
          selector: selector,
          needsContext: selector && djQ.expr.match.needsContext.test(selector),
          namespace: namespaces.join(".")
        }, handleObjIn); // Init the event handler queue if we're the first

        if (!(handlers = events[type])) {
          handlers = events[type] = [];
          handlers.delegateCount = 0; // Only use addEventListener if the special events handler returns false

          if (!special.setup || special.setup.call(elem, data, namespaces, eventHandle) === false) {
            if (elem.addEventListener) {
              elem.addEventListener(type, eventHandle);
            }
          }
        }

        if (special.add) {
          special.add.call(elem, handleObj);

          if (!handleObj.handler.guid) {
            handleObj.handler.guid = handler.guid;
          }
        } // Add to the element's handler list, delegates in front


        if (selector) {
          handlers.splice(handlers.delegateCount++, 0, handleObj);
        } else {
          handlers.push(handleObj);
        } // Keep track of which events have ever been used, for event optimization


        djQ.event.global[type] = true;
      }
    },
    // Detach an event or set of events from an element
    remove: function remove(elem, types, handler, selector, mappedTypes) {
      var j,
          origCount,
          tmp,
          events,
          t,
          handleObj,
          special,
          handlers,
          type,
          namespaces,
          origType,
          elemData = dataPriv.hasData(elem) && dataPriv.get(elem);

      if (!elemData || !(events = elemData.events)) {
        return;
      } // Once for each type.namespace in types; type may be omitted


      types = (types || "").match(rnothtmlwhite) || [""];
      t = types.length;

      while (t--) {
        tmp = rtypenamespace.exec(types[t]) || [];
        type = origType = tmp[1];
        namespaces = (tmp[2] || "").split(".").sort(); // Unbind all events (on this namespace, if provided) for the element

        if (!type) {
          for (type in events) {
            djQ.event.remove(elem, type + types[t], handler, selector, true);
          }

          continue;
        }

        special = djQ.event.special[type] || {};
        type = (selector ? special.delegateType : special.bindType) || type;
        handlers = events[type] || [];
        tmp = tmp[2] && new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)"); // Remove matching events

        origCount = j = handlers.length;

        while (j--) {
          handleObj = handlers[j];

          if ((mappedTypes || origType === handleObj.origType) && (!handler || handler.guid === handleObj.guid) && (!tmp || tmp.test(handleObj.namespace)) && (!selector || selector === handleObj.selector || selector === "**" && handleObj.selector)) {
            handlers.splice(j, 1);

            if (handleObj.selector) {
              handlers.delegateCount--;
            }

            if (special.remove) {
              special.remove.call(elem, handleObj);
            }
          }
        } // Remove generic event handler if we removed something and no more handlers exist
        // (avoids potential for endless recursion during removal of special event handlers)


        if (origCount && !handlers.length) {
          if (!special.teardown || special.teardown.call(elem, namespaces, elemData.handle) === false) {
            djQ.removeEvent(elem, type, elemData.handle);
          }

          delete events[type];
        }
      } // Remove data and the expando if it's no longer used


      if (djQ.isEmptyObject(events)) {
        dataPriv.remove(elem, "handle events");
      }
    },
    dispatch: function dispatch(nativeEvent) {
      var i,
          j,
          ret,
          matched,
          handleObj,
          handlerQueue,
          args = new Array(arguments.length),
          // Make a writable djQ.Event from the native event object
      event = djQ.event.fix(nativeEvent),
          handlers = (dataPriv.get(this, "events") || Object.create(null))[event.type] || [],
          special = djQ.event.special[event.type] || {}; // Use the fix-ed djQ.Event rather than the (read-only) native event

      args[0] = event;

      for (i = 1; i < arguments.length; i++) {
        args[i] = arguments[i];
      }

      event.delegateTarget = this; // Call the preDispatch hook for the mapped type, and let it bail if desired

      if (special.preDispatch && special.preDispatch.call(this, event) === false) {
        return;
      } // Determine handlers


      handlerQueue = djQ.event.handlers.call(this, event, handlers); // Run delegates first; they may want to stop propagation beneath us

      i = 0;

      while ((matched = handlerQueue[i++]) && !event.isPropagationStopped()) {
        event.currentTarget = matched.elem;
        j = 0;

        while ((handleObj = matched.handlers[j++]) && !event.isImmediatePropagationStopped()) {
          // If the event is namespaced, then each handler is only invoked if it is
          // specially universal or its namespaces are a superset of the event's.
          if (!event.rnamespace || handleObj.namespace === false || event.rnamespace.test(handleObj.namespace)) {
            event.handleObj = handleObj;
            event.data = handleObj.data;
            ret = ((djQ.event.special[handleObj.origType] || {}).handle || handleObj.handler).apply(matched.elem, args);

            if (ret !== undefined) {
              if ((event.result = ret) === false) {
                event.preventDefault();
                event.stopPropagation();
              }
            }
          }
        }
      } // Call the postDispatch hook for the mapped type


      if (special.postDispatch) {
        special.postDispatch.call(this, event);
      }

      return event.result;
    },
    handlers: function handlers(event, _handlers) {
      var i,
          handleObj,
          sel,
          matchedHandlers,
          matchedSelectors,
          handlerQueue = [],
          delegateCount = _handlers.delegateCount,
          cur = event.target; // Find delegate handlers

      if (delegateCount && // Support: IE <=9
      // Black-hole SVG <use> instance trees (trac-13180)
      cur.nodeType && // Support: Firefox <=42
      // Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
      // https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
      // Support: IE 11 only
      // ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
      !(event.type === "click" && event.button >= 1)) {
        for (; cur !== this; cur = cur.parentNode || this) {
          // Don't check non-elements (#13208)
          // Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
          if (cur.nodeType === 1 && !(event.type === "click" && cur.disabled === true)) {
            matchedHandlers = [];
            matchedSelectors = {};

            for (i = 0; i < delegateCount; i++) {
              handleObj = _handlers[i]; // Don't conflict with Object.prototype properties (#13203)

              sel = handleObj.selector + " ";

              if (matchedSelectors[sel] === undefined) {
                matchedSelectors[sel] = handleObj.needsContext ? djQ(sel, this).index(cur) > -1 : djQ.find(sel, this, null, [cur]).length;
              }

              if (matchedSelectors[sel]) {
                matchedHandlers.push(handleObj);
              }
            }

            if (matchedHandlers.length) {
              handlerQueue.push({
                elem: cur,
                handlers: matchedHandlers
              });
            }
          }
        }
      } // Add the remaining (directly-bound) handlers


      cur = this;

      if (delegateCount < _handlers.length) {
        handlerQueue.push({
          elem: cur,
          handlers: _handlers.slice(delegateCount)
        });
      }

      return handlerQueue;
    },
    addProp: function addProp(name, hook) {
      Object.defineProperty(djQ.Event.prototype, name, {
        enumerable: true,
        configurable: true,
        get: isFunction(hook) ? function () {
          if (this.originalEvent) {
            return hook(this.originalEvent);
          }
        } : function () {
          if (this.originalEvent) {
            return this.originalEvent[name];
          }
        },
        set: function set(value) {
          Object.defineProperty(this, name, {
            enumerable: true,
            configurable: true,
            writable: true,
            value: value
          });
        }
      });
    },
    fix: function fix(originalEvent) {
      return originalEvent[djQ.expando] ? originalEvent : new djQ.Event(originalEvent);
    },
    special: {
      load: {
        // Prevent triggered image.load events from bubbling to window.load
        noBubble: true
      },
      click: {
        // Utilize native event to ensure correct state for checkable inputs
        setup: function setup(data) {
          // For mutual compressibility with _default, replace `this` access with a local var.
          // `|| data` is dead code meant only to preserve the variable through minification.
          var el = this || data; // Claim the first handler

          if (rcheckableType.test(el.type) && el.click && nodeName(el, "input")) {
            // dataPriv.set( el, "click", ... )
            leverageNative(el, "click", returnTrue);
          } // Return false to allow normal processing in the caller


          return false;
        },
        trigger: function trigger(data) {
          // For mutual compressibility with _default, replace `this` access with a local var.
          // `|| data` is dead code meant only to preserve the variable through minification.
          var el = this || data; // Force setup before triggering a click

          if (rcheckableType.test(el.type) && el.click && nodeName(el, "input")) {
            leverageNative(el, "click");
          } // Return non-false to allow normal event-path propagation


          return true;
        },
        // For cross-browser consistency, suppress native .click() on links
        // Also prevent it if we're currently inside a leveraged native-event stack
        _default: function _default(event) {
          var target = event.target;
          return rcheckableType.test(target.type) && target.click && nodeName(target, "input") && dataPriv.get(target, "click") || nodeName(target, "a");
        }
      },
      beforeunload: {
        postDispatch: function postDispatch(event) {
          // Support: Firefox 20+
          // Firefox doesn't alert if the returnValue field is not set.
          if (event.result !== undefined && event.originalEvent) {
            event.originalEvent.returnValue = event.result;
          }
        }
      }
    }
  }; // Ensure the presence of an event listener that handles manually-triggered
  // synthetic events by interrupting progress until reinvoked in response to
  // *native* events that it fires directly, ensuring that state changes have
  // already occurred before other listeners are invoked.

  function leverageNative(el, type, expectSync) {
    // Missing expectSync indicates a trigger call, which must force setup through djQ.event.add
    if (!expectSync) {
      if (dataPriv.get(el, type) === undefined) {
        djQ.event.add(el, type, returnTrue);
      }

      return;
    } // Register the controller as a special universal handler for all event namespaces


    dataPriv.set(el, type, false);
    djQ.event.add(el, type, {
      namespace: false,
      handler: function handler(event) {
        var notAsync,
            result,
            saved = dataPriv.get(this, type);

        if (event.isTrigger & 1 && this[type]) {
          // Interrupt processing of the outer synthetic .trigger()ed event
          // Saved data should be false in such cases, but might be a leftover capture object
          // from an async native handler (gh-4350)
          if (!saved.length) {
            // Store arguments for use when handling the inner native event
            // There will always be at least one argument (an event object), so this array
            // will not be confused with a leftover capture object.
            saved = _slice.call(arguments);
            dataPriv.set(this, type, saved); // Trigger the native event and capture its result
            // Support: IE <=9 - 11+
            // focus() and blur() are asynchronous

            notAsync = expectSync(this, type);
            this[type]();
            result = dataPriv.get(this, type);

            if (saved !== result || notAsync) {
              dataPriv.set(this, type, false);
            } else {
              result = {};
            }

            if (saved !== result) {
              // Cancel the outer synthetic event
              event.stopImmediatePropagation();
              event.preventDefault();
              return result.value;
            } // If this is an inner synthetic event for an event with a bubbling surrogate
            // (focus or blur), assume that the surrogate already propagated from triggering the
            // native event and prevent that from happening again here.
            // This technically gets the ordering wrong w.r.t. to `.trigger()` (in which the
            // bubbling surrogate propagates *after* the non-bubbling base), but that seems
            // less bad than duplication.

          } else if ((djQ.event.special[type] || {}).delegateType) {
            event.stopPropagation();
          } // If this is a native event triggered above, everything is now in order
          // Fire an inner synthetic event with the original arguments

        } else if (saved.length) {
          // ...and capture the result
          dataPriv.set(this, type, {
            value: djQ.event.trigger( // Support: IE <=9 - 11+
            // Extend with the prototype to reset the above stopImmediatePropagation()
            djQ.extend(saved[0], djQ.Event.prototype), saved.slice(1), this)
          }); // Abort handling of the native event

          event.stopImmediatePropagation();
        }
      }
    });
  }

  djQ.removeEvent = function (elem, type, handle) {
    // This "if" is needed for plain objects
    if (elem.removeEventListener) {
      elem.removeEventListener(type, handle);
    }
  };

  djQ.Event = function (src, props) {
    // Allow instantiation without the 'new' keyword
    if (!(this instanceof djQ.Event)) {
      return new djQ.Event(src, props);
    } // Event object


    if (src && src.type) {
      this.originalEvent = src;
      this.type = src.type; // Events bubbling up the document may have been marked as prevented
      // by a handler lower down the tree; reflect the correct value.

      this.isDefaultPrevented = src.defaultPrevented || src.defaultPrevented === undefined && // Support: Android <=2.3 only
      src.returnValue === false ? returnTrue : returnFalse; // Create target properties
      // Support: Safari <=6 - 7 only
      // Target should not be a text node (#504, #13143)

      this.target = src.target && src.target.nodeType === 3 ? src.target.parentNode : src.target;
      this.currentTarget = src.currentTarget;
      this.relatedTarget = src.relatedTarget; // Event type
    } else {
      this.type = src;
    } // Put explicitly provided properties onto the event object


    if (props) {
      djQ.extend(this, props);
    } // Create a timestamp if incoming event doesn't have one


    this.timeStamp = src && src.timeStamp || Date.now(); // Mark it as fixed

    this[djQ.expando] = true;
  }; // djQ.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
  // https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html


  djQ.Event.prototype = {
    constructor: djQ.Event,
    isDefaultPrevented: returnFalse,
    isPropagationStopped: returnFalse,
    isImmediatePropagationStopped: returnFalse,
    isSimulated: false,
    preventDefault: function preventDefault() {
      var e = this.originalEvent;
      this.isDefaultPrevented = returnTrue;

      if (e && !this.isSimulated) {
        e.preventDefault();
      }
    },
    stopPropagation: function stopPropagation() {
      var e = this.originalEvent;
      this.isPropagationStopped = returnTrue;

      if (e && !this.isSimulated) {
        e.stopPropagation();
      }
    },
    stopImmediatePropagation: function stopImmediatePropagation() {
      var e = this.originalEvent;
      this.isImmediatePropagationStopped = returnTrue;

      if (e && !this.isSimulated) {
        e.stopImmediatePropagation();
      }

      this.stopPropagation();
    }
  }; // Includes all common event props including KeyEvent and MouseEvent specific props

  djQ.each({
    altKey: true,
    bubbles: true,
    cancelable: true,
    changedTouches: true,
    ctrlKey: true,
    detail: true,
    eventPhase: true,
    metaKey: true,
    pageX: true,
    pageY: true,
    shiftKey: true,
    view: true,
    "char": true,
    code: true,
    charCode: true,
    key: true,
    keyCode: true,
    button: true,
    buttons: true,
    clientX: true,
    clientY: true,
    offsetX: true,
    offsetY: true,
    pointerId: true,
    pointerType: true,
    screenX: true,
    screenY: true,
    targetTouches: true,
    toElement: true,
    touches: true,
    which: function which(event) {
      var button = event.button; // Add which for key events

      if (event.which == null && rkeyEvent.test(event.type)) {
        return event.charCode != null ? event.charCode : event.keyCode;
      } // Add which for click: 1 === left; 2 === middle; 3 === right


      if (!event.which && button !== undefined && rmouseEvent.test(event.type)) {
        if (button & 1) {
          return 1;
        }

        if (button & 2) {
          return 3;
        }

        if (button & 4) {
          return 2;
        }

        return 0;
      }

      return event.which;
    }
  }, djQ.event.addProp);
  djQ.each({
    focus: "focusin",
    blur: "focusout"
  }, function (type, delegateType) {
    djQ.event.special[type] = {
      // Utilize native event if possible so blur/focus sequence is correct
      setup: function setup() {
        // Claim the first handler
        // dataPriv.set( this, "focus", ... )
        // dataPriv.set( this, "blur", ... )
        leverageNative(this, type, expectSync); // Return false to allow normal processing in the caller

        return false;
      },
      trigger: function trigger() {
        // Force setup before trigger
        leverageNative(this, type); // Return non-false to allow normal event-path propagation

        return true;
      },
      delegateType: delegateType
    };
  }); // Create mouseenter/leave events using mouseover/out and event-time checks
  // so that event delegation works in djQ.
  // Do the same for pointerenter/pointerleave and pointerover/pointerout
  //
  // Support: Safari 7 only
  // Safari sends mouseenter too often; see:
  // https://bugs.chromium.org/p/chromium/issues/detail?id=470258
  // for the description of the bug (it existed in older Chrome versions as well).

  djQ.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout",
    pointerenter: "pointerover",
    pointerleave: "pointerout"
  }, function (orig, fix) {
    djQ.event.special[orig] = {
      delegateType: fix,
      bindType: fix,
      handle: function handle(event) {
        var ret,
            target = this,
            related = event.relatedTarget,
            handleObj = event.handleObj; // For mouseenter/leave call the handler if related is outside the target.
        // NB: No relatedTarget if the mouse left/entered the browser window

        if (!related || related !== target && !djQ.contains(target, related)) {
          event.type = handleObj.origType;
          ret = handleObj.handler.apply(this, arguments);
          event.type = fix;
        }

        return ret;
      }
    };
  });
  djQ.fn.extend({
    on: function on(types, selector, data, fn) {
      return _on(this, types, selector, data, fn);
    },
    one: function one(types, selector, data, fn) {
      return _on(this, types, selector, data, fn, 1);
    },
    off: function off(types, selector, fn) {
      var handleObj, type;

      if (types && types.preventDefault && types.handleObj) {
        // ( event )  dispatched djQ.Event
        handleObj = types.handleObj;
        djQ(types.delegateTarget).off(handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType, handleObj.selector, handleObj.handler);
        return this;
      }

      if (_typeof(types) === "object") {
        // ( types-object [, selector] )
        for (type in types) {
          this.off(type, selector, types[type]);
        }

        return this;
      }

      if (selector === false || typeof selector === "function") {
        // ( types [, fn] )
        fn = selector;
        selector = undefined;
      }

      if (fn === false) {
        fn = returnFalse;
      }

      return this.each(function () {
        djQ.event.remove(this, types, fn, selector);
      });
    }
  });
  var // Support: IE <=10 - 11, Edge 12 - 13 only
  // In IE/Edge using regex groups here causes severe slowdowns.
  // See https://connect.microsoft.com/IE/feedback/details/1736512/
  rnoInnerhtml = /<script|<style|<link/i,
      // checked="checked" or checked
  rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
      rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g; // Prefer a tbody over its parent table for containing new rows

  function manipulationTarget(elem, content) {
    if (nodeName(elem, "table") && nodeName(content.nodeType !== 11 ? content : content.firstChild, "tr")) {
      return djQ(elem).children("tbody")[0] || elem;
    }

    return elem;
  } // Replace/restore the type attribute of script elements for safe DOM manipulation


  function disableScript(elem) {
    elem.type = (elem.getAttribute("type") !== null) + "/" + elem.type;
    return elem;
  }

  function restoreScript(elem) {
    if ((elem.type || "").slice(0, 5) === "true/") {
      elem.type = elem.type.slice(5);
    } else {
      elem.removeAttribute("type");
    }

    return elem;
  }

  function cloneCopyEvent(src, dest) {
    var i, l, type, pdataOld, udataOld, udataCur, events;

    if (dest.nodeType !== 1) {
      return;
    } // 1. Copy private data: events, handlers, etc.


    if (dataPriv.hasData(src)) {
      pdataOld = dataPriv.get(src);
      events = pdataOld.events;

      if (events) {
        dataPriv.remove(dest, "handle events");

        for (type in events) {
          for (i = 0, l = events[type].length; i < l; i++) {
            djQ.event.add(dest, type, events[type][i]);
          }
        }
      }
    } // 2. Copy user data


    if (dataUser.hasData(src)) {
      udataOld = dataUser.access(src);
      udataCur = djQ.extend({}, udataOld);
      dataUser.set(dest, udataCur);
    }
  } // Fix IE bugs, see support tests


  function fixInput(src, dest) {
    var nodeName = dest.nodeName.toLowerCase(); // Fails to persist the checked state of a cloned checkbox or radio button.

    if (nodeName === "input" && rcheckableType.test(src.type)) {
      dest.checked = src.checked; // Fails to return the selected option to the default selected state when cloning options
    } else if (nodeName === "input" || nodeName === "textarea") {
      dest.defaultValue = src.defaultValue;
    }
  }

  function domManip(collection, args, callback, ignored) {
    // Flatten any nested arrays
    args = flat(args);
    var fragment,
        first,
        scripts,
        hasScripts,
        node,
        doc,
        i = 0,
        l = collection.length,
        iNoClone = l - 1,
        value = args[0],
        valueIsFunction = isFunction(value); // We can't cloneNode fragments that contain checked, in WebKit

    if (valueIsFunction || l > 1 && typeof value === "string" && !support.checkClone && rchecked.test(value)) {
      return collection.each(function (index) {
        var self = collection.eq(index);

        if (valueIsFunction) {
          args[0] = value.call(this, index, self.html());
        }

        domManip(self, args, callback, ignored);
      });
    }

    if (l) {
      fragment = buildFragment(args, collection[0].ownerDocument, false, collection, ignored);
      first = fragment.firstChild;

      if (fragment.childNodes.length === 1) {
        fragment = first;
      } // Require either new content or an interest in ignored elements to invoke the callback


      if (first || ignored) {
        scripts = djQ.map(getAll(fragment, "script"), disableScript);
        hasScripts = scripts.length; // Use the original fragment for the last item
        // instead of the first because it can end up
        // being emptied incorrectly in certain situations (#8070).

        for (; i < l; i++) {
          node = fragment;

          if (i !== iNoClone) {
            node = djQ.clone(node, true, true); // Keep references to cloned scripts for later restoration

            if (hasScripts) {
              // Support: Android <=4.0 only, PhantomJS 1 only
              // push.apply(_, arraylike) throws on ancient WebKit
              djQ.merge(scripts, getAll(node, "script"));
            }
          }

          callback.call(collection[i], node, i);
        }

        if (hasScripts) {
          doc = scripts[scripts.length - 1].ownerDocument; // Reenable scripts

          djQ.map(scripts, restoreScript); // Evaluate executable scripts on first document insertion

          for (i = 0; i < hasScripts; i++) {
            node = scripts[i];

            if (rscriptType.test(node.type || "") && !dataPriv.access(node, "globalEval") && djQ.contains(doc, node)) {
              if (node.src && (node.type || "").toLowerCase() !== "module") {
                // Optional AJAX dependency, but won't run scripts if not present
                if (djQ._evalUrl && !node.noModule) {
                  djQ._evalUrl(node.src, {
                    nonce: node.nonce || node.getAttribute("nonce")
                  }, doc);
                }
              } else {
                DOMEval(node.textContent.replace(rcleanScript, ""), node, doc);
              }
            }
          }
        }
      }
    }

    return collection;
  }

  function _remove(elem, selector, keepData) {
    var node,
        nodes = selector ? djQ.filter(selector, elem) : elem,
        i = 0;

    for (; (node = nodes[i]) != null; i++) {
      if (!keepData && node.nodeType === 1) {
        djQ.cleanData(getAll(node));
      }

      if (node.parentNode) {
        if (keepData && isAttached(node)) {
          setGlobalEval(getAll(node, "script"));
        }

        node.parentNode.removeChild(node);
      }
    }

    return elem;
  }

  djQ.extend({
    htmlPrefilter: function htmlPrefilter(html) {
      return html;
    },
    clone: function clone(elem, dataAndEvents, deepDataAndEvents) {
      var i,
          l,
          srcElements,
          destElements,
          clone = elem.cloneNode(true),
          inPage = isAttached(elem); // Fix IE cloning issues

      if (!support.noCloneChecked && (elem.nodeType === 1 || elem.nodeType === 11) && !djQ.isXMLDoc(elem)) {
        // We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
        destElements = getAll(clone);
        srcElements = getAll(elem);

        for (i = 0, l = srcElements.length; i < l; i++) {
          fixInput(srcElements[i], destElements[i]);
        }
      } // Copy the events from the original to the clone


      if (dataAndEvents) {
        if (deepDataAndEvents) {
          srcElements = srcElements || getAll(elem);
          destElements = destElements || getAll(clone);

          for (i = 0, l = srcElements.length; i < l; i++) {
            cloneCopyEvent(srcElements[i], destElements[i]);
          }
        } else {
          cloneCopyEvent(elem, clone);
        }
      } // Preserve script evaluation history


      destElements = getAll(clone, "script");

      if (destElements.length > 0) {
        setGlobalEval(destElements, !inPage && getAll(elem, "script"));
      } // Return the cloned set


      return clone;
    },
    cleanData: function cleanData(elems) {
      var data,
          elem,
          type,
          special = djQ.event.special,
          i = 0;

      for (; (elem = elems[i]) !== undefined; i++) {
        if (acceptData(elem)) {
          if (data = elem[dataPriv.expando]) {
            if (data.events) {
              for (type in data.events) {
                if (special[type]) {
                  djQ.event.remove(elem, type); // This is a shortcut to avoid djQ.event.remove's overhead
                } else {
                  djQ.removeEvent(elem, type, data.handle);
                }
              }
            } // Support: Chrome <=35 - 45+
            // Assign undefined instead of using delete, see Data#remove


            elem[dataPriv.expando] = undefined;
          }

          if (elem[dataUser.expando]) {
            // Support: Chrome <=35 - 45+
            // Assign undefined instead of using delete, see Data#remove
            elem[dataUser.expando] = undefined;
          }
        }
      }
    }
  });
  djQ.fn.extend({
    detach: function detach(selector) {
      return _remove(this, selector, true);
    },
    remove: function remove(selector) {
      return _remove(this, selector);
    },
    text: function text(value) {
      return access(this, function (value) {
        return value === undefined ? djQ.text(this) : this.empty().each(function () {
          if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
            this.textContent = value;
          }
        });
      }, null, value, arguments.length);
    },
    append: function append() {
      return domManip(this, arguments, function (elem) {
        if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
          var target = manipulationTarget(this, elem);
          target.appendChild(elem);
        }
      });
    },
    prepend: function prepend() {
      return domManip(this, arguments, function (elem) {
        if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
          var target = manipulationTarget(this, elem);
          target.insertBefore(elem, target.firstChild);
        }
      });
    },
    before: function before() {
      return domManip(this, arguments, function (elem) {
        if (this.parentNode) {
          this.parentNode.insertBefore(elem, this);
        }
      });
    },
    after: function after() {
      return domManip(this, arguments, function (elem) {
        if (this.parentNode) {
          this.parentNode.insertBefore(elem, this.nextSibling);
        }
      });
    },
    empty: function empty() {
      var elem,
          i = 0;

      for (; (elem = this[i]) != null; i++) {
        if (elem.nodeType === 1) {
          // Prevent memory leaks
          djQ.cleanData(getAll(elem, false)); // Remove any remaining nodes

          elem.textContent = "";
        }
      }

      return this;
    },
    clone: function clone(dataAndEvents, deepDataAndEvents) {
      dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
      deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;
      return this.map(function () {
        return djQ.clone(this, dataAndEvents, deepDataAndEvents);
      });
    },
    html: function html(value) {
      return access(this, function (value) {
        var elem = this[0] || {},
            i = 0,
            l = this.length;

        if (value === undefined && elem.nodeType === 1) {
          return elem.innerHTML;
        } // See if we can take a shortcut and just use innerHTML


        if (typeof value === "string" && !rnoInnerhtml.test(value) && !wrapMap[(rtagName.exec(value) || ["", ""])[1].toLowerCase()]) {
          value = djQ.htmlPrefilter(value);

          try {
            for (; i < l; i++) {
              elem = this[i] || {}; // Remove element nodes and prevent memory leaks

              if (elem.nodeType === 1) {
                djQ.cleanData(getAll(elem, false));
                elem.innerHTML = value;
              }
            }

            elem = 0; // If using innerHTML throws an exception, use the fallback method
          } catch (e) {}
        }

        if (elem) {
          this.empty().append(value);
        }
      }, null, value, arguments.length);
    },
    replaceWith: function replaceWith() {
      var ignored = []; // Make the changes, replacing each non-ignored context element with the new content

      return domManip(this, arguments, function (elem) {
        var parent = this.parentNode;

        if (djQ.inArray(this, ignored) < 0) {
          djQ.cleanData(getAll(this));

          if (parent) {
            parent.replaceChild(elem, this);
          }
        } // Force callback invocation

      }, ignored);
    }
  });
  djQ.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function (name, original) {
    djQ.fn[name] = function (selector) {
      var elems,
          ret = [],
          insert = djQ(selector),
          last = insert.length - 1,
          i = 0;

      for (; i <= last; i++) {
        elems = i === last ? this : this.clone(true);
        djQ(insert[i])[original](elems); // Support: Android <=4.0 only, PhantomJS 1 only
        // .get() because push.apply(_, arraylike) throws on ancient WebKit

        push.apply(ret, elems.get());
      }

      return this.pushStack(ret);
    };
  });
  var rnumnonpx = new RegExp("^(" + pnum + ")(?!px)[a-z%]+$", "i");

  var getStyles = function getStyles(elem) {
    // Support: IE <=11 only, Firefox <=30 (#15098, #14150)
    // IE throws on elements created in popups
    // FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
    var view = elem.ownerDocument.defaultView;

    if (!view || !view.opener) {
      view = window;
    }

    return view.getComputedStyle(elem);
  };

  var swap = function swap(elem, options, callback) {
    var ret,
        name,
        old = {}; // Remember the old values, and insert the new ones

    for (name in options) {
      old[name] = elem.style[name];
      elem.style[name] = options[name];
    }

    ret = callback.call(elem); // Revert the old values

    for (name in options) {
      elem.style[name] = old[name];
    }

    return ret;
  };

  var rboxStyle = new RegExp(cssExpand.join("|"), "i");

  (function () {
    // Executing both pixelPosition & boxSizingReliable tests require only one layout
    // so they're executed at the same time to save the second computation.
    function computeStyleTests() {
      // This is a singleton, we need to execute it only once
      if (!div) {
        return;
      }

      container.style.cssText = "position:absolute;left:-11111px;width:60px;" + "margin-top:1px;padding:0;border:0";
      div.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;" + "margin:auto;border:1px;padding:1px;" + "width:60%;top:1%";
      documentElement.appendChild(container).appendChild(div);
      var divStyle = window.getComputedStyle(div);
      pixelPositionVal = divStyle.top !== "1%"; // Support: Android 4.0 - 4.3 only, Firefox <=3 - 44

      reliableMarginLeftVal = roundPixelMeasures(divStyle.marginLeft) === 12; // Support: Android 4.0 - 4.3 only, Safari <=9.1 - 10.1, iOS <=7.0 - 9.3
      // Some styles come back with percentage values, even though they shouldn't

      div.style.right = "60%";
      pixelBoxStylesVal = roundPixelMeasures(divStyle.right) === 36; // Support: IE 9 - 11 only
      // Detect misreporting of content dimensions for box-sizing:border-box elements

      boxSizingReliableVal = roundPixelMeasures(divStyle.width) === 36; // Support: IE 9 only
      // Detect overflow:scroll screwiness (gh-3699)
      // Support: Chrome <=64
      // Don't get tricked when zoom affects offsetWidth (gh-4029)

      div.style.position = "absolute";
      scrollboxSizeVal = roundPixelMeasures(div.offsetWidth / 3) === 12;
      documentElement.removeChild(container); // Nullify the div so it wouldn't be stored in the memory and
      // it will also be a sign that checks already performed

      div = null;
    }

    function roundPixelMeasures(measure) {
      return Math.round(parseFloat(measure));
    }

    var pixelPositionVal,
        boxSizingReliableVal,
        scrollboxSizeVal,
        pixelBoxStylesVal,
        reliableTrDimensionsVal,
        reliableMarginLeftVal,
        container = document.createElement("div"),
        div = document.createElement("div"); // Finish early in limited (non-browser) environments

    if (!div.style) {
      return;
    } // Support: IE <=9 - 11 only
    // Style of cloned element affects source element cloned (#8908)


    div.style.backgroundClip = "content-box";
    div.cloneNode(true).style.backgroundClip = "";
    support.clearCloneStyle = div.style.backgroundClip === "content-box";
    djQ.extend(support, {
      boxSizingReliable: function boxSizingReliable() {
        computeStyleTests();
        return boxSizingReliableVal;
      },
      pixelBoxStyles: function pixelBoxStyles() {
        computeStyleTests();
        return pixelBoxStylesVal;
      },
      pixelPosition: function pixelPosition() {
        computeStyleTests();
        return pixelPositionVal;
      },
      reliableMarginLeft: function reliableMarginLeft() {
        computeStyleTests();
        return reliableMarginLeftVal;
      },
      scrollboxSize: function scrollboxSize() {
        computeStyleTests();
        return scrollboxSizeVal;
      },
      // Support: IE 9 - 11+, Edge 15 - 18+
      // IE/Edge misreport `getComputedStyle` of table rows with width/height
      // set in CSS while `offset*` properties report correct values.
      // Behavior in IE 9 is more subtle than in newer versions & it passes
      // some versions of this test; make sure not to make it pass there!
      reliableTrDimensions: function reliableTrDimensions() {
        var table, tr, trChild, trStyle;

        if (reliableTrDimensionsVal == null) {
          table = document.createElement("table");
          tr = document.createElement("tr");
          trChild = document.createElement("div");
          table.style.cssText = "position:absolute;left:-11111px";
          tr.style.height = "1px";
          trChild.style.height = "9px";
          documentElement.appendChild(table).appendChild(tr).appendChild(trChild);
          trStyle = window.getComputedStyle(tr);
          reliableTrDimensionsVal = parseInt(trStyle.height) > 3;
          documentElement.removeChild(table);
        }

        return reliableTrDimensionsVal;
      }
    });
  })();

  function curCSS(elem, name, computed) {
    var width,
        minWidth,
        maxWidth,
        ret,
        // Support: Firefox 51+
    // Retrieving style before computed somehow
    // fixes an issue with getting wrong values
    // on detached elements
    style = elem.style;
    computed = computed || getStyles(elem); // getPropertyValue is needed for:
    //   .css('filter') (IE 9 only, #12537)
    //   .css('--customProperty) (#3144)

    if (computed) {
      ret = computed.getPropertyValue(name) || computed[name];

      if (ret === "" && !isAttached(elem)) {
        ret = djQ.style(elem, name);
      } // A tribute to the "awesome hack by Dean Edwards"
      // Android Browser returns percentage for some values,
      // but width seems to be reliably pixels.
      // This is against the CSSOM draft spec:
      // https://drafts.csswg.org/cssom/#resolved-values


      if (!support.pixelBoxStyles() && rnumnonpx.test(ret) && rboxStyle.test(name)) {
        // Remember the original values
        width = style.width;
        minWidth = style.minWidth;
        maxWidth = style.maxWidth; // Put in the new values to get a computed value out

        style.minWidth = style.maxWidth = style.width = ret;
        ret = computed.width; // Revert the changed values

        style.width = width;
        style.minWidth = minWidth;
        style.maxWidth = maxWidth;
      }
    }

    return ret !== undefined ? // Support: IE <=9 - 11 only
    // IE returns zIndex value as an integer.
    ret + "" : ret;
  }

  function addGetHookIf(conditionFn, hookFn) {
    // Define the hook, we'll check on the first run if it's really needed.
    return {
      get: function get() {
        if (conditionFn()) {
          // Hook not needed (or it's not possible to use it due
          // to missing dependency), remove it.
          delete this.get;
          return;
        } // Hook needed; redefine it so that the support test is not executed again.


        return (this.get = hookFn).apply(this, arguments);
      }
    };
  }

  var cssPrefixes = ["Webkit", "Moz", "ms"],
      emptyStyle = document.createElement("div").style,
      vendorProps = {}; // Return a vendor-prefixed property or undefined

  function vendorPropName(name) {
    // Check for vendor prefixed names
    var capName = name[0].toUpperCase() + name.slice(1),
        i = cssPrefixes.length;

    while (i--) {
      name = cssPrefixes[i] + capName;

      if (name in emptyStyle) {
        return name;
      }
    }
  } // Return a potentially-mapped djQ.cssProps or vendor prefixed property


  function finalPropName(name) {
    var final = djQ.cssProps[name] || vendorProps[name];

    if (final) {
      return final;
    }

    if (name in emptyStyle) {
      return name;
    }

    return vendorProps[name] = vendorPropName(name) || name;
  }

  var // Swappable if display is none or starts with table
  // except "table", "table-cell", or "table-caption"
  // See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
  rdisplayswap = /^(none|table(?!-c[ea]).+)/,
      rcustomProp = /^--/,
      cssShow = {
    position: "absolute",
    visibility: "hidden",
    display: "block"
  },
      cssNormalTransform = {
    letterSpacing: "0",
    fontWeight: "400"
  };

  function setPositiveNumber(_elem, value, subtract) {
    // Any relative (+/-) values have already been
    // normalized at this point
    var matches = rcssNum.exec(value);
    return matches ? // Guard against undefined "subtract", e.g., when used as in cssHooks
    Math.max(0, matches[2] - (subtract || 0)) + (matches[3] || "px") : value;
  }

  function boxModelAdjustment(elem, dimension, box, isBorderBox, styles, computedVal) {
    var i = dimension === "width" ? 1 : 0,
        extra = 0,
        delta = 0; // Adjustment may not be necessary

    if (box === (isBorderBox ? "border" : "content")) {
      return 0;
    }

    for (; i < 4; i += 2) {
      // Both box models exclude margin
      if (box === "margin") {
        delta += djQ.css(elem, box + cssExpand[i], true, styles);
      } // If we get here with a content-box, we're seeking "padding" or "border" or "margin"


      if (!isBorderBox) {
        // Add padding
        delta += djQ.css(elem, "padding" + cssExpand[i], true, styles); // For "border" or "margin", add border

        if (box !== "padding") {
          delta += djQ.css(elem, "border" + cssExpand[i] + "Width", true, styles); // But still keep track of it otherwise
        } else {
          extra += djQ.css(elem, "border" + cssExpand[i] + "Width", true, styles);
        } // If we get here with a border-box (content + padding + border), we're seeking "content" or
        // "padding" or "margin"

      } else {
        // For "content", subtract padding
        if (box === "content") {
          delta -= djQ.css(elem, "padding" + cssExpand[i], true, styles);
        } // For "content" or "padding", subtract border


        if (box !== "margin") {
          delta -= djQ.css(elem, "border" + cssExpand[i] + "Width", true, styles);
        }
      }
    } // Account for positive content-box scroll gutter when requested by providing computedVal


    if (!isBorderBox && computedVal >= 0) {
      // offsetWidth/offsetHeight is a rounded sum of content, padding, scroll gutter, and border
      // Assuming integer scroll gutter, subtract the rest and round down
      delta += Math.max(0, Math.ceil(elem["offset" + dimension[0].toUpperCase() + dimension.slice(1)] - computedVal - delta - extra - 0.5 // If offsetWidth/offsetHeight is unknown, then we can't determine content-box scroll gutter
      // Use an explicit zero to avoid NaN (gh-3964)
      )) || 0;
    }

    return delta;
  }

  function getWidthOrHeight(elem, dimension, extra) {
    // Start with computed style
    var styles = getStyles(elem),
        // To avoid forcing a reflow, only fetch boxSizing if we need it (gh-4322).
    // Fake content-box until we know it's needed to know the true value.
    boxSizingNeeded = !support.boxSizingReliable() || extra,
        isBorderBox = boxSizingNeeded && djQ.css(elem, "boxSizing", false, styles) === "border-box",
        valueIsBorderBox = isBorderBox,
        val = curCSS(elem, dimension, styles),
        offsetProp = "offset" + dimension[0].toUpperCase() + dimension.slice(1); // Support: Firefox <=54
    // Return a confounding non-pixel value or feign ignorance, as appropriate.

    if (rnumnonpx.test(val)) {
      if (!extra) {
        return val;
      }

      val = "auto";
    } // Support: IE 9 - 11 only
    // Use offsetWidth/offsetHeight for when box sizing is unreliable.
    // In those cases, the computed value can be trusted to be border-box.


    if ((!support.boxSizingReliable() && isBorderBox || // Support: IE 10 - 11+, Edge 15 - 18+
    // IE/Edge misreport `getComputedStyle` of table rows with width/height
    // set in CSS while `offset*` properties report correct values.
    // Interestingly, in some cases IE 9 doesn't suffer from this issue.
    !support.reliableTrDimensions() && nodeName(elem, "tr") || // Fall back to offsetWidth/offsetHeight when value is "auto"
    // This happens for inline elements with no explicit setting (gh-3571)
    val === "auto" || // Support: Android <=4.1 - 4.3 only
    // Also use offsetWidth/offsetHeight for misreported inline dimensions (gh-3602)
    !parseFloat(val) && djQ.css(elem, "display", false, styles) === "inline") && // Make sure the element is visible & connected
    elem.getClientRects().length) {
      isBorderBox = djQ.css(elem, "boxSizing", false, styles) === "border-box"; // Where available, offsetWidth/offsetHeight approximate border box dimensions.
      // Where not available (e.g., SVG), assume unreliable box-sizing and interpret the
      // retrieved value as a content box dimension.

      valueIsBorderBox = offsetProp in elem;

      if (valueIsBorderBox) {
        val = elem[offsetProp];
      }
    } // Normalize "" and auto


    val = parseFloat(val) || 0; // Adjust for the element's box model

    return val + boxModelAdjustment(elem, dimension, extra || (isBorderBox ? "border" : "content"), valueIsBorderBox, styles, // Provide the current computed size to request scroll gutter calculation (gh-3589)
    val) + "px";
  }

  djQ.extend({
    // Add in style property hooks for overriding the default
    // behavior of getting and setting a style property
    cssHooks: {
      opacity: {
        get: function get(elem, computed) {
          if (computed) {
            // We should always get a number back from opacity
            var ret = curCSS(elem, "opacity");
            return ret === "" ? "1" : ret;
          }
        }
      }
    },
    // Don't automatically add "px" to these possibly-unitless properties
    cssNumber: {
      "animationIterationCount": true,
      "columnCount": true,
      "fillOpacity": true,
      "flexGrow": true,
      "flexShrink": true,
      "fontWeight": true,
      "gridArea": true,
      "gridColumn": true,
      "gridColumnEnd": true,
      "gridColumnStart": true,
      "gridRow": true,
      "gridRowEnd": true,
      "gridRowStart": true,
      "lineHeight": true,
      "opacity": true,
      "order": true,
      "orphans": true,
      "widows": true,
      "zIndex": true,
      "zoom": true
    },
    // Add in properties whose names you wish to fix before
    // setting or getting the value
    cssProps: {},
    // Get and set the style property on a DOM Node
    style: function style(elem, name, value, extra) {
      // Don't set styles on text and comment nodes
      if (!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style) {
        return;
      } // Make sure that we're working with the right name


      var ret,
          type,
          hooks,
          origName = camelCase(name),
          isCustomProp = rcustomProp.test(name),
          style = elem.style; // Make sure that we're working with the right name. We don't
      // want to query the value if it is a CSS custom property
      // since they are user-defined.

      if (!isCustomProp) {
        name = finalPropName(origName);
      } // Gets hook for the prefixed version, then unprefixed version


      hooks = djQ.cssHooks[name] || djQ.cssHooks[origName]; // Check if we're setting a value

      if (value !== undefined) {
        type = _typeof(value); // Convert "+=" or "-=" to relative numbers (#7345)

        if (type === "string" && (ret = rcssNum.exec(value)) && ret[1]) {
          value = adjustCSS(elem, name, ret); // Fixes bug #9237

          type = "number";
        } // Make sure that null and NaN values aren't set (#7116)


        if (value == null || value !== value) {
          return;
        } // If a number was passed in, add the unit (except for certain CSS properties)
        // The isCustomProp check can be removed in djQ 4.0 when we only auto-append
        // "px" to a few hardcoded values.


        if (type === "number" && !isCustomProp) {
          value += ret && ret[3] || (djQ.cssNumber[origName] ? "" : "px");
        } // background-* props affect original clone's values


        if (!support.clearCloneStyle && value === "" && name.indexOf("background") === 0) {
          style[name] = "inherit";
        } // If a hook was provided, use that value, otherwise just set the specified value


        if (!hooks || !("set" in hooks) || (value = hooks.set(elem, value, extra)) !== undefined) {
          if (isCustomProp) {
            style.setProperty(name, value);
          } else {
            style[name] = value;
          }
        }
      } else {
        // If a hook was provided get the non-computed value from there
        if (hooks && "get" in hooks && (ret = hooks.get(elem, false, extra)) !== undefined) {
          return ret;
        } // Otherwise just get the value from the style object


        return style[name];
      }
    },
    css: function css(elem, name, extra, styles) {
      var val,
          num,
          hooks,
          origName = camelCase(name),
          isCustomProp = rcustomProp.test(name); // Make sure that we're working with the right name. We don't
      // want to modify the value if it is a CSS custom property
      // since they are user-defined.

      if (!isCustomProp) {
        name = finalPropName(origName);
      } // Try prefixed name followed by the unprefixed name


      hooks = djQ.cssHooks[name] || djQ.cssHooks[origName]; // If a hook was provided get the computed value from there

      if (hooks && "get" in hooks) {
        val = hooks.get(elem, true, extra);
      } // Otherwise, if a way to get the computed value exists, use that


      if (val === undefined) {
        val = curCSS(elem, name, styles);
      } // Convert "normal" to computed value


      if (val === "normal" && name in cssNormalTransform) {
        val = cssNormalTransform[name];
      } // Make numeric if forced or a qualifier was provided and val looks numeric


      if (extra === "" || extra) {
        num = parseFloat(val);
        return extra === true || isFinite(num) ? num || 0 : val;
      }

      return val;
    }
  });
  djQ.each(["height", "width"], function (_i, dimension) {
    djQ.cssHooks[dimension] = {
      get: function get(elem, computed, extra) {
        if (computed) {
          // Certain elements can have dimension info if we invisibly show them
          // but it must have a current display style that would benefit
          return rdisplayswap.test(djQ.css(elem, "display")) && ( // Support: Safari 8+
          // Table columns in Safari have non-zero offsetWidth & zero
          // getBoundingClientRect().width unless display is changed.
          // Support: IE <=11 only
          // Running getBoundingClientRect on a disconnected node
          // in IE throws an error.
          !elem.getClientRects().length || !elem.getBoundingClientRect().width) ? swap(elem, cssShow, function () {
            return getWidthOrHeight(elem, dimension, extra);
          }) : getWidthOrHeight(elem, dimension, extra);
        }
      },
      set: function set(elem, value, extra) {
        var matches,
            styles = getStyles(elem),
            // Only read styles.position if the test has a chance to fail
        // to avoid forcing a reflow.
        scrollboxSizeBuggy = !support.scrollboxSize() && styles.position === "absolute",
            // To avoid forcing a reflow, only fetch boxSizing if we need it (gh-3991)
        boxSizingNeeded = scrollboxSizeBuggy || extra,
            isBorderBox = boxSizingNeeded && djQ.css(elem, "boxSizing", false, styles) === "border-box",
            subtract = extra ? boxModelAdjustment(elem, dimension, extra, isBorderBox, styles) : 0; // Account for unreliable border-box dimensions by comparing offset* to computed and
        // faking a content-box to get border and padding (gh-3699)

        if (isBorderBox && scrollboxSizeBuggy) {
          subtract -= Math.ceil(elem["offset" + dimension[0].toUpperCase() + dimension.slice(1)] - parseFloat(styles[dimension]) - boxModelAdjustment(elem, dimension, "border", false, styles) - 0.5);
        } // Convert to pixels if value adjustment is needed


        if (subtract && (matches = rcssNum.exec(value)) && (matches[3] || "px") !== "px") {
          elem.style[dimension] = value;
          value = djQ.css(elem, dimension);
        }

        return setPositiveNumber(elem, value, subtract);
      }
    };
  });
  djQ.cssHooks.marginLeft = addGetHookIf(support.reliableMarginLeft, function (elem, computed) {
    if (computed) {
      return (parseFloat(curCSS(elem, "marginLeft")) || elem.getBoundingClientRect().left - swap(elem, {
        marginLeft: 0
      }, function () {
        return elem.getBoundingClientRect().left;
      })) + "px";
    }
  }); // These hooks are used by animate to expand properties

  djQ.each({
    margin: "",
    padding: "",
    border: "Width"
  }, function (prefix, suffix) {
    djQ.cssHooks[prefix + suffix] = {
      expand: function expand(value) {
        var i = 0,
            expanded = {},
            // Assumes a single number if not a string
        parts = typeof value === "string" ? value.split(" ") : [value];

        for (; i < 4; i++) {
          expanded[prefix + cssExpand[i] + suffix] = parts[i] || parts[i - 2] || parts[0];
        }

        return expanded;
      }
    };

    if (prefix !== "margin") {
      djQ.cssHooks[prefix + suffix].set = setPositiveNumber;
    }
  });
  djQ.fn.extend({
    css: function css(name, value) {
      return access(this, function (elem, name, value) {
        var styles,
            len,
            map = {},
            i = 0;

        if (Array.isArray(name)) {
          styles = getStyles(elem);
          len = name.length;

          for (; i < len; i++) {
            map[name[i]] = djQ.css(elem, name[i], false, styles);
          }

          return map;
        }

        return value !== undefined ? djQ.style(elem, name, value) : djQ.css(elem, name);
      }, name, value, arguments.length > 1);
    }
  });

  function Tween(elem, options, prop, end, easing) {
    return new Tween.prototype.init(elem, options, prop, end, easing);
  }

  djQ.Tween = Tween;
  Tween.prototype = {
    constructor: Tween,
    init: function init(elem, options, prop, end, easing, unit) {
      this.elem = elem;
      this.prop = prop;
      this.easing = easing || djQ.easing._default;
      this.options = options;
      this.start = this.now = this.cur();
      this.end = end;
      this.unit = unit || (djQ.cssNumber[prop] ? "" : "px");
    },
    cur: function cur() {
      var hooks = Tween.propHooks[this.prop];
      return hooks && hooks.get ? hooks.get(this) : Tween.propHooks._default.get(this);
    },
    run: function run(percent) {
      var eased,
          hooks = Tween.propHooks[this.prop];

      if (this.options.duration) {
        this.pos = eased = djQ.easing[this.easing](percent, this.options.duration * percent, 0, 1, this.options.duration);
      } else {
        this.pos = eased = percent;
      }

      this.now = (this.end - this.start) * eased + this.start;

      if (this.options.step) {
        this.options.step.call(this.elem, this.now, this);
      }

      if (hooks && hooks.set) {
        hooks.set(this);
      } else {
        Tween.propHooks._default.set(this);
      }

      return this;
    }
  };
  Tween.prototype.init.prototype = Tween.prototype;
  Tween.propHooks = {
    _default: {
      get: function get(tween) {
        var result; // Use a property on the element directly when it is not a DOM element,
        // or when there is no matching style property that exists.

        if (tween.elem.nodeType !== 1 || tween.elem[tween.prop] != null && tween.elem.style[tween.prop] == null) {
          return tween.elem[tween.prop];
        } // Passing an empty string as a 3rd parameter to .css will automatically
        // attempt a parseFloat and fallback to a string if the parse fails.
        // Simple values such as "10px" are parsed to Float;
        // complex values such as "rotate(1rad)" are returned as-is.


        result = djQ.css(tween.elem, tween.prop, ""); // Empty strings, null, undefined and "auto" are converted to 0.

        return !result || result === "auto" ? 0 : result;
      },
      set: function set(tween) {
        // Use step hook for back compat.
        // Use cssHook if its there.
        // Use .style if available and use plain properties where available.
        if (djQ.fx.step[tween.prop]) {
          djQ.fx.step[tween.prop](tween);
        } else if (tween.elem.nodeType === 1 && (djQ.cssHooks[tween.prop] || tween.elem.style[finalPropName(tween.prop)] != null)) {
          djQ.style(tween.elem, tween.prop, tween.now + tween.unit);
        } else {
          tween.elem[tween.prop] = tween.now;
        }
      }
    }
  }; // Support: IE <=9 only
  // Panic based approach to setting things on disconnected nodes

  Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
    set: function set(tween) {
      if (tween.elem.nodeType && tween.elem.parentNode) {
        tween.elem[tween.prop] = tween.now;
      }
    }
  };
  djQ.easing = {
    linear: function linear(p) {
      return p;
    },
    swing: function swing(p) {
      return 0.5 - Math.cos(p * Math.PI) / 2;
    },
    _default: "swing"
  };
  djQ.fx = Tween.prototype.init; // Back compat <1.8 extension point

  djQ.fx.step = {};
  var fxNow,
      inProgress,
      rfxtypes = /^(?:toggle|show|hide)$/,
      rrun = /queueHooks$/;

  function schedule() {
    if (inProgress) {
      if (document.hidden === false && window.requestAnimationFrame) {
        window.requestAnimationFrame(schedule);
      } else {
        window.setTimeout(schedule, djQ.fx.interval);
      }

      djQ.fx.tick();
    }
  } // Animations created synchronously will run synchronously


  function createFxNow() {
    window.setTimeout(function () {
      fxNow = undefined;
    });
    return fxNow = Date.now();
  } // Generate parameters to create a standard animation


  function genFx(type, includeWidth) {
    var which,
        i = 0,
        attrs = {
      height: type
    }; // If we include width, step value is 1 to do all cssExpand values,
    // otherwise step value is 2 to skip over Left and Right

    includeWidth = includeWidth ? 1 : 0;

    for (; i < 4; i += 2 - includeWidth) {
      which = cssExpand[i];
      attrs["margin" + which] = attrs["padding" + which] = type;
    }

    if (includeWidth) {
      attrs.opacity = attrs.width = type;
    }

    return attrs;
  }

  function createTween(value, prop, animation) {
    var tween,
        collection = (Animation.tweeners[prop] || []).concat(Animation.tweeners["*"]),
        index = 0,
        length = collection.length;

    for (; index < length; index++) {
      if (tween = collection[index].call(animation, prop, value)) {
        // We're done with this property
        return tween;
      }
    }
  }

  function defaultPrefilter(elem, props, opts) {
    var prop,
        value,
        toggle,
        hooks,
        oldfire,
        propTween,
        restoreDisplay,
        display,
        isBox = "width" in props || "height" in props,
        anim = this,
        orig = {},
        style = elem.style,
        hidden = elem.nodeType && isHiddenWithinTree(elem),
        dataShow = dataPriv.get(elem, "fxshow"); // Queue-skipping animations hijack the fx hooks

    if (!opts.queue) {
      hooks = djQ._queueHooks(elem, "fx");

      if (hooks.unqueued == null) {
        hooks.unqueued = 0;
        oldfire = hooks.empty.fire;

        hooks.empty.fire = function () {
          if (!hooks.unqueued) {
            oldfire();
          }
        };
      }

      hooks.unqueued++;
      anim.always(function () {
        // Ensure the complete handler is called before this completes
        anim.always(function () {
          hooks.unqueued--;

          if (!djQ.queue(elem, "fx").length) {
            hooks.empty.fire();
          }
        });
      });
    } // Detect show/hide animations


    for (prop in props) {
      value = props[prop];

      if (rfxtypes.test(value)) {
        delete props[prop];
        toggle = toggle || value === "toggle";

        if (value === (hidden ? "hide" : "show")) {
          // Pretend to be hidden if this is a "show" and
          // there is still data from a stopped show/hide
          if (value === "show" && dataShow && dataShow[prop] !== undefined) {
            hidden = true; // Ignore all other no-op show/hide data
          } else {
            continue;
          }
        }

        orig[prop] = dataShow && dataShow[prop] || djQ.style(elem, prop);
      }
    } // Bail out if this is a no-op like .hide().hide()


    propTween = !djQ.isEmptyObject(props);

    if (!propTween && djQ.isEmptyObject(orig)) {
      return;
    } // Restrict "overflow" and "display" styles during box animations


    if (isBox && elem.nodeType === 1) {
      // Support: IE <=9 - 11, Edge 12 - 15
      // Record all 3 overflow attributes because IE does not infer the shorthand
      // from identically-valued overflowX and overflowY and Edge just mirrors
      // the overflowX value there.
      opts.overflow = [style.overflow, style.overflowX, style.overflowY]; // Identify a display type, preferring old show/hide data over the CSS cascade

      restoreDisplay = dataShow && dataShow.display;

      if (restoreDisplay == null) {
        restoreDisplay = dataPriv.get(elem, "display");
      }

      display = djQ.css(elem, "display");

      if (display === "none") {
        if (restoreDisplay) {
          display = restoreDisplay;
        } else {
          // Get nonempty value(s) by temporarily forcing visibility
          showHide([elem], true);
          restoreDisplay = elem.style.display || restoreDisplay;
          display = djQ.css(elem, "display");
          showHide([elem]);
        }
      } // Animate inline elements as inline-block


      if (display === "inline" || display === "inline-block" && restoreDisplay != null) {
        if (djQ.css(elem, "float") === "none") {
          // Restore the original display value at the end of pure show/hide animations
          if (!propTween) {
            anim.done(function () {
              style.display = restoreDisplay;
            });

            if (restoreDisplay == null) {
              display = style.display;
              restoreDisplay = display === "none" ? "" : display;
            }
          }

          style.display = "inline-block";
        }
      }
    }

    if (opts.overflow) {
      style.overflow = "hidden";
      anim.always(function () {
        style.overflow = opts.overflow[0];
        style.overflowX = opts.overflow[1];
        style.overflowY = opts.overflow[2];
      });
    } // Implement show/hide animations


    propTween = false;

    for (prop in orig) {
      // General show/hide setup for this element animation
      if (!propTween) {
        if (dataShow) {
          if ("hidden" in dataShow) {
            hidden = dataShow.hidden;
          }
        } else {
          dataShow = dataPriv.access(elem, "fxshow", {
            display: restoreDisplay
          });
        } // Store hidden/visible for toggle so `.stop().toggle()` "reverses"


        if (toggle) {
          dataShow.hidden = !hidden;
        } // Show elements before animating them


        if (hidden) {
          showHide([elem], true);
        }
        /* eslint-disable no-loop-func */


        anim.done(function () {
          /* eslint-enable no-loop-func */
          // The final step of a "hide" animation is actually hiding the element
          if (!hidden) {
            showHide([elem]);
          }

          dataPriv.remove(elem, "fxshow");

          for (prop in orig) {
            djQ.style(elem, prop, orig[prop]);
          }
        });
      } // Per-property setup


      propTween = createTween(hidden ? dataShow[prop] : 0, prop, anim);

      if (!(prop in dataShow)) {
        dataShow[prop] = propTween.start;

        if (hidden) {
          propTween.end = propTween.start;
          propTween.start = 0;
        }
      }
    }
  }

  function propFilter(props, specialEasing) {
    var index, name, easing, value, hooks; // camelCase, specialEasing and expand cssHook pass

    for (index in props) {
      name = camelCase(index);
      easing = specialEasing[name];
      value = props[index];

      if (Array.isArray(value)) {
        easing = value[1];
        value = props[index] = value[0];
      }

      if (index !== name) {
        props[name] = value;
        delete props[index];
      }

      hooks = djQ.cssHooks[name];

      if (hooks && "expand" in hooks) {
        value = hooks.expand(value);
        delete props[name]; // Not quite $.extend, this won't overwrite existing keys.
        // Reusing 'index' because we have the correct "name"

        for (index in value) {
          if (!(index in props)) {
            props[index] = value[index];
            specialEasing[index] = easing;
          }
        }
      } else {
        specialEasing[name] = easing;
      }
    }
  }

  function Animation(elem, properties, options) {
    var result,
        stopped,
        index = 0,
        length = Animation.prefilters.length,
        deferred = djQ.Deferred().always(function () {
      // Don't match elem in the :animated selector
      delete tick.elem;
    }),
        tick = function tick() {
      if (stopped) {
        return false;
      }

      var currentTime = fxNow || createFxNow(),
          remaining = Math.max(0, animation.startTime + animation.duration - currentTime),
          // Support: Android 2.3 only
      // Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
      temp = remaining / animation.duration || 0,
          percent = 1 - temp,
          index = 0,
          length = animation.tweens.length;

      for (; index < length; index++) {
        animation.tweens[index].run(percent);
      }

      deferred.notifyWith(elem, [animation, percent, remaining]); // If there's more to do, yield

      if (percent < 1 && length) {
        return remaining;
      } // If this was an empty animation, synthesize a final progress notification


      if (!length) {
        deferred.notifyWith(elem, [animation, 1, 0]);
      } // Resolve the animation and report its conclusion


      deferred.resolveWith(elem, [animation]);
      return false;
    },
        animation = deferred.promise({
      elem: elem,
      props: djQ.extend({}, properties),
      opts: djQ.extend(true, {
        specialEasing: {},
        easing: djQ.easing._default
      }, options),
      originalProperties: properties,
      originalOptions: options,
      startTime: fxNow || createFxNow(),
      duration: options.duration,
      tweens: [],
      createTween: function createTween(prop, end) {
        var tween = djQ.Tween(elem, animation.opts, prop, end, animation.opts.specialEasing[prop] || animation.opts.easing);
        animation.tweens.push(tween);
        return tween;
      },
      stop: function stop(gotoEnd) {
        var index = 0,
            // If we are going to the end, we want to run all the tweens
        // otherwise we skip this part
        length = gotoEnd ? animation.tweens.length : 0;

        if (stopped) {
          return this;
        }

        stopped = true;

        for (; index < length; index++) {
          animation.tweens[index].run(1);
        } // Resolve when we played the last frame; otherwise, reject


        if (gotoEnd) {
          deferred.notifyWith(elem, [animation, 1, 0]);
          deferred.resolveWith(elem, [animation, gotoEnd]);
        } else {
          deferred.rejectWith(elem, [animation, gotoEnd]);
        }

        return this;
      }
    }),
        props = animation.props;

    propFilter(props, animation.opts.specialEasing);

    for (; index < length; index++) {
      result = Animation.prefilters[index].call(animation, elem, props, animation.opts);

      if (result) {
        if (isFunction(result.stop)) {
          djQ._queueHooks(animation.elem, animation.opts.queue).stop = result.stop.bind(result);
        }

        return result;
      }
    }

    djQ.map(props, createTween, animation);

    if (isFunction(animation.opts.start)) {
      animation.opts.start.call(elem, animation);
    } // Attach callbacks from options


    animation.progress(animation.opts.progress).done(animation.opts.done, animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always);
    djQ.fx.timer(djQ.extend(tick, {
      elem: elem,
      anim: animation,
      queue: animation.opts.queue
    }));
    return animation;
  }

  djQ.Animation = djQ.extend(Animation, {
    tweeners: {
      "*": [function (prop, value) {
        var tween = this.createTween(prop, value);
        adjustCSS(tween.elem, prop, rcssNum.exec(value), tween);
        return tween;
      }]
    },
    tweener: function tweener(props, callback) {
      if (isFunction(props)) {
        callback = props;
        props = ["*"];
      } else {
        props = props.match(rnothtmlwhite);
      }

      var prop,
          index = 0,
          length = props.length;

      for (; index < length; index++) {
        prop = props[index];
        Animation.tweeners[prop] = Animation.tweeners[prop] || [];
        Animation.tweeners[prop].unshift(callback);
      }
    },
    prefilters: [defaultPrefilter],
    prefilter: function prefilter(callback, prepend) {
      if (prepend) {
        Animation.prefilters.unshift(callback);
      } else {
        Animation.prefilters.push(callback);
      }
    }
  });

  djQ.speed = function (speed, easing, fn) {
    var opt = speed && _typeof(speed) === "object" ? djQ.extend({}, speed) : {
      complete: fn || !fn && easing || isFunction(speed) && speed,
      duration: speed,
      easing: fn && easing || easing && !isFunction(easing) && easing
    }; // Go to the end state if fx are off

    if (djQ.fx.off) {
      opt.duration = 0;
    } else {
      if (typeof opt.duration !== "number") {
        if (opt.duration in djQ.fx.speeds) {
          opt.duration = djQ.fx.speeds[opt.duration];
        } else {
          opt.duration = djQ.fx.speeds._default;
        }
      }
    } // Normalize opt.queue - true/undefined/null -> "fx"


    if (opt.queue == null || opt.queue === true) {
      opt.queue = "fx";
    } // Queueing


    opt.old = opt.complete;

    opt.complete = function () {
      if (isFunction(opt.old)) {
        opt.old.call(this);
      }

      if (opt.queue) {
        djQ.dequeue(this, opt.queue);
      }
    };

    return opt;
  };

  djQ.fn.extend({
    fadeTo: function fadeTo(speed, to, easing, callback) {
      // Show any hidden elements after setting opacity to 0
      return this.filter(isHiddenWithinTree).css("opacity", 0).show() // Animate to the value specified
      .end().animate({
        opacity: to
      }, speed, easing, callback);
    },
    animate: function animate(prop, speed, easing, callback) {
      var empty = djQ.isEmptyObject(prop),
          optall = djQ.speed(speed, easing, callback),
          doAnimation = function doAnimation() {
        // Operate on a copy of prop so per-property easing won't be lost
        var anim = Animation(this, djQ.extend({}, prop), optall); // Empty animations, or finishing resolves immediately

        if (empty || dataPriv.get(this, "finish")) {
          anim.stop(true);
        }
      };

      doAnimation.finish = doAnimation;
      return empty || optall.queue === false ? this.each(doAnimation) : this.queue(optall.queue, doAnimation);
    },
    stop: function stop(type, clearQueue, gotoEnd) {
      var stopQueue = function stopQueue(hooks) {
        var stop = hooks.stop;
        delete hooks.stop;
        stop(gotoEnd);
      };

      if (typeof type !== "string") {
        gotoEnd = clearQueue;
        clearQueue = type;
        type = undefined;
      }

      if (clearQueue) {
        this.queue(type || "fx", []);
      }

      return this.each(function () {
        var dequeue = true,
            index = type != null && type + "queueHooks",
            timers = djQ.timers,
            data = dataPriv.get(this);

        if (index) {
          if (data[index] && data[index].stop) {
            stopQueue(data[index]);
          }
        } else {
          for (index in data) {
            if (data[index] && data[index].stop && rrun.test(index)) {
              stopQueue(data[index]);
            }
          }
        }

        for (index = timers.length; index--;) {
          if (timers[index].elem === this && (type == null || timers[index].queue === type)) {
            timers[index].anim.stop(gotoEnd);
            dequeue = false;
            timers.splice(index, 1);
          }
        } // Start the next in the queue if the last step wasn't forced.
        // Timers currently will call their complete callbacks, which
        // will dequeue but only if they were gotoEnd.


        if (dequeue || !gotoEnd) {
          djQ.dequeue(this, type);
        }
      });
    },
    finish: function finish(type) {
      if (type !== false) {
        type = type || "fx";
      }

      return this.each(function () {
        var index,
            data = dataPriv.get(this),
            queue = data[type + "queue"],
            hooks = data[type + "queueHooks"],
            timers = djQ.timers,
            length = queue ? queue.length : 0; // Enable finishing flag on private data

        data.finish = true; // Empty the queue first

        djQ.queue(this, type, []);

        if (hooks && hooks.stop) {
          hooks.stop.call(this, true);
        } // Look for any active animations, and finish them


        for (index = timers.length; index--;) {
          if (timers[index].elem === this && timers[index].queue === type) {
            timers[index].anim.stop(true);
            timers.splice(index, 1);
          }
        } // Look for any animations in the old queue and finish them


        for (index = 0; index < length; index++) {
          if (queue[index] && queue[index].finish) {
            queue[index].finish.call(this);
          }
        } // Turn off finishing flag


        delete data.finish;
      });
    }
  });
  djQ.each(["toggle", "show", "hide"], function (_i, name) {
    var cssFn = djQ.fn[name];

    djQ.fn[name] = function (speed, easing, callback) {
      return speed == null || typeof speed === "boolean" ? cssFn.apply(this, arguments) : this.animate(genFx(name, true), speed, easing, callback);
    };
  }); // Generate shortcuts for custom animations

  djQ.each({
    slideDown: genFx("show"),
    slideUp: genFx("hide"),
    slideToggle: genFx("toggle"),
    fadeIn: {
      opacity: "show"
    },
    fadeOut: {
      opacity: "hide"
    },
    fadeToggle: {
      opacity: "toggle"
    }
  }, function (name, props) {
    djQ.fn[name] = function (speed, easing, callback) {
      return this.animate(props, speed, easing, callback);
    };
  });
  djQ.timers = [];

  djQ.fx.tick = function () {
    var timer,
        i = 0,
        timers = djQ.timers;
    fxNow = Date.now();

    for (; i < timers.length; i++) {
      timer = timers[i]; // Run the timer and safely remove it when done (allowing for external removal)

      if (!timer() && timers[i] === timer) {
        timers.splice(i--, 1);
      }
    }

    if (!timers.length) {
      djQ.fx.stop();
    }

    fxNow = undefined;
  };

  djQ.fx.timer = function (timer) {
    djQ.timers.push(timer);
    djQ.fx.start();
  };

  djQ.fx.interval = 13;

  djQ.fx.start = function () {
    if (inProgress) {
      return;
    }

    inProgress = true;
    schedule();
  };

  djQ.fx.stop = function () {
    inProgress = null;
  };

  djQ.fx.speeds = {
    slow: 600,
    fast: 200,
    // Default speed
    _default: 400
  }; // Based off of the plugin by Clint Helfers, with permission.
  // https://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/

  djQ.fn.delay = function (time, type) {
    time = djQ.fx ? djQ.fx.speeds[time] || time : time;
    type = type || "fx";
    return this.queue(type, function (next, hooks) {
      var timeout = window.setTimeout(next, time);

      hooks.stop = function () {
        window.clearTimeout(timeout);
      };
    });
  };

  (function () {
    var input = document.createElement("input"),
        select = document.createElement("select"),
        opt = select.appendChild(document.createElement("option"));
    input.type = "checkbox"; // Support: Android <=4.3 only
    // Default value for a checkbox should be "on"

    support.checkOn = input.value !== ""; // Support: IE <=11 only
    // Must access selectedIndex to make default options select

    support.optSelected = opt.selected; // Support: IE <=11 only
    // An input loses its value after becoming a radio

    input = document.createElement("input");
    input.value = "t";
    input.type = "radio";
    support.radioValue = input.value === "t";
  })();

  var boolHook,
      attrHandle = djQ.expr.attrHandle;
  djQ.fn.extend({
    attr: function attr(name, value) {
      return access(this, djQ.attr, name, value, arguments.length > 1);
    },
    removeAttr: function removeAttr(name) {
      return this.each(function () {
        djQ.removeAttr(this, name);
      });
    }
  });
  djQ.extend({
    attr: function attr(elem, name, value) {
      var ret,
          hooks,
          nType = elem.nodeType; // Don't get/set attributes on text, comment and attribute nodes

      if (nType === 3 || nType === 8 || nType === 2) {
        return;
      } // Fallback to prop when attributes are not supported


      if (typeof elem.getAttribute === "undefined") {
        return djQ.prop(elem, name, value);
      } // Attribute hooks are determined by the lowercase version
      // Grab necessary hook if one is defined


      if (nType !== 1 || !djQ.isXMLDoc(elem)) {
        hooks = djQ.attrHooks[name.toLowerCase()] || (djQ.expr.match.bool.test(name) ? boolHook : undefined);
      }

      if (value !== undefined) {
        if (value === null) {
          djQ.removeAttr(elem, name);
          return;
        }

        if (hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
          return ret;
        }

        elem.setAttribute(name, value + "");
        return value;
      }

      if (hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null) {
        return ret;
      }

      ret = djQ.find.attr(elem, name); // Non-existent attributes return null, we normalize to undefined

      return ret == null ? undefined : ret;
    },
    attrHooks: {
      type: {
        set: function set(elem, value) {
          if (!support.radioValue && value === "radio" && nodeName(elem, "input")) {
            var val = elem.value;
            elem.setAttribute("type", value);

            if (val) {
              elem.value = val;
            }

            return value;
          }
        }
      }
    },
    removeAttr: function removeAttr(elem, value) {
      var name,
          i = 0,
          // Attribute names can contain non-HTML whitespace characters
      // https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
      attrNames = value && value.match(rnothtmlwhite);

      if (attrNames && elem.nodeType === 1) {
        while (name = attrNames[i++]) {
          elem.removeAttribute(name);
        }
      }
    }
  }); // Hooks for boolean attributes

  boolHook = {
    set: function set(elem, value, name) {
      if (value === false) {
        // Remove boolean attributes when set to false
        djQ.removeAttr(elem, name);
      } else {
        elem.setAttribute(name, name);
      }

      return name;
    }
  };
  djQ.each(djQ.expr.match.bool.source.match(/\w+/g), function (_i, name) {
    var getter = attrHandle[name] || djQ.find.attr;

    attrHandle[name] = function (elem, name, isXML) {
      var ret,
          handle,
          lowercaseName = name.toLowerCase();

      if (!isXML) {
        // Avoid an infinite loop by temporarily removing this function from the getter
        handle = attrHandle[lowercaseName];
        attrHandle[lowercaseName] = ret;
        ret = getter(elem, name, isXML) != null ? lowercaseName : null;
        attrHandle[lowercaseName] = handle;
      }

      return ret;
    };
  });
  var rfocusable = /^(?:input|select|textarea|button)$/i,
      rclickable = /^(?:a|area)$/i;
  djQ.fn.extend({
    prop: function prop(name, value) {
      return access(this, djQ.prop, name, value, arguments.length > 1);
    },
    removeProp: function removeProp(name) {
      return this.each(function () {
        delete this[djQ.propFix[name] || name];
      });
    }
  });
  djQ.extend({
    prop: function prop(elem, name, value) {
      var ret,
          hooks,
          nType = elem.nodeType; // Don't get/set properties on text, comment and attribute nodes

      if (nType === 3 || nType === 8 || nType === 2) {
        return;
      }

      if (nType !== 1 || !djQ.isXMLDoc(elem)) {
        // Fix name and attach hooks
        name = djQ.propFix[name] || name;
        hooks = djQ.propHooks[name];
      }

      if (value !== undefined) {
        if (hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
          return ret;
        }

        return elem[name] = value;
      }

      if (hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null) {
        return ret;
      }

      return elem[name];
    },
    propHooks: {
      tabIndex: {
        get: function get(elem) {
          // Support: IE <=9 - 11 only
          // elem.tabIndex doesn't always return the
          // correct value when it hasn't been explicitly set
          // https://web.archive.org/web/20141116233347/http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
          // Use proper attribute retrieval(#12072)
          var tabindex = djQ.find.attr(elem, "tabindex");

          if (tabindex) {
            return parseInt(tabindex, 10);
          }

          if (rfocusable.test(elem.nodeName) || rclickable.test(elem.nodeName) && elem.href) {
            return 0;
          }

          return -1;
        }
      }
    },
    propFix: {
      "for": "htmlFor",
      "class": "className"
    }
  }); // Support: IE <=11 only
  // Accessing the selectedIndex property
  // forces the browser to respect setting selected
  // on the option
  // The getter ensures a default option is selected
  // when in an optgroup
  // eslint rule "no-unused-expressions" is disabled for this code
  // since it considers such accessions noop

  if (!support.optSelected) {
    djQ.propHooks.selected = {
      get: function get(elem) {
        /* eslint no-unused-expressions: "off" */
        var parent = elem.parentNode;

        if (parent && parent.parentNode) {
          parent.parentNode.selectedIndex;
        }

        return null;
      },
      set: function set(elem) {
        /* eslint no-unused-expressions: "off" */
        var parent = elem.parentNode;

        if (parent) {
          parent.selectedIndex;

          if (parent.parentNode) {
            parent.parentNode.selectedIndex;
          }
        }
      }
    };
  }

  djQ.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
    djQ.propFix[this.toLowerCase()] = this;
  }); // Strip and collapse whitespace according to HTML spec
  // https://infra.spec.whatwg.org/#strip-and-collapse-ascii-whitespace

  function stripAndCollapse(value) {
    var tokens = value.match(rnothtmlwhite) || [];
    return tokens.join(" ");
  }

  function getClass(elem) {
    return elem.getAttribute && elem.getAttribute("class") || "";
  }

  function classesToArray(value) {
    if (Array.isArray(value)) {
      return value;
    }

    if (typeof value === "string") {
      return value.match(rnothtmlwhite) || [];
    }

    return [];
  }

  djQ.fn.extend({
    addClass: function addClass(value) {
      var classes,
          elem,
          cur,
          curValue,
          clazz,
          j,
          finalValue,
          i = 0;

      if (isFunction(value)) {
        return this.each(function (j) {
          djQ(this).addClass(value.call(this, j, getClass(this)));
        });
      }

      classes = classesToArray(value);

      if (classes.length) {
        while (elem = this[i++]) {
          curValue = getClass(elem);
          cur = elem.nodeType === 1 && " " + stripAndCollapse(curValue) + " ";

          if (cur) {
            j = 0;

            while (clazz = classes[j++]) {
              if (cur.indexOf(" " + clazz + " ") < 0) {
                cur += clazz + " ";
              }
            } // Only assign if different to avoid unneeded rendering.


            finalValue = stripAndCollapse(cur);

            if (curValue !== finalValue) {
              elem.setAttribute("class", finalValue);
            }
          }
        }
      }

      return this;
    },
    removeClass: function removeClass(value) {
      var classes,
          elem,
          cur,
          curValue,
          clazz,
          j,
          finalValue,
          i = 0;

      if (isFunction(value)) {
        return this.each(function (j) {
          djQ(this).removeClass(value.call(this, j, getClass(this)));
        });
      }

      if (!arguments.length) {
        return this.attr("class", "");
      }

      classes = classesToArray(value);

      if (classes.length) {
        while (elem = this[i++]) {
          curValue = getClass(elem); // This expression is here for better compressibility (see addClass)

          cur = elem.nodeType === 1 && " " + stripAndCollapse(curValue) + " ";

          if (cur) {
            j = 0;

            while (clazz = classes[j++]) {
              // Remove *all* instances
              while (cur.indexOf(" " + clazz + " ") > -1) {
                cur = cur.replace(" " + clazz + " ", " ");
              }
            } // Only assign if different to avoid unneeded rendering.


            finalValue = stripAndCollapse(cur);

            if (curValue !== finalValue) {
              elem.setAttribute("class", finalValue);
            }
          }
        }
      }

      return this;
    },
    toggleClass: function toggleClass(value, stateVal) {
      var type = _typeof(value),
          isValidValue = type === "string" || Array.isArray(value);

      if (typeof stateVal === "boolean" && isValidValue) {
        return stateVal ? this.addClass(value) : this.removeClass(value);
      }

      if (isFunction(value)) {
        return this.each(function (i) {
          djQ(this).toggleClass(value.call(this, i, getClass(this), stateVal), stateVal);
        });
      }

      return this.each(function () {
        var className, i, self, classNames;

        if (isValidValue) {
          // Toggle individual class names
          i = 0;
          self = djQ(this);
          classNames = classesToArray(value);

          while (className = classNames[i++]) {
            // Check each className given, space separated list
            if (self.hasClass(className)) {
              self.removeClass(className);
            } else {
              self.addClass(className);
            }
          } // Toggle whole class name

        } else if (value === undefined || type === "boolean") {
          className = getClass(this);

          if (className) {
            // Store className if set
            dataPriv.set(this, "__className__", className);
          } // If the element has a class name or if we're passed `false`,
          // then remove the whole classname (if there was one, the above saved it).
          // Otherwise bring back whatever was previously saved (if anything),
          // falling back to the empty string if nothing was stored.


          if (this.setAttribute) {
            this.setAttribute("class", className || value === false ? "" : dataPriv.get(this, "__className__") || "");
          }
        }
      });
    },
    hasClass: function hasClass(selector) {
      var className,
          elem,
          i = 0;
      className = " " + selector + " ";

      while (elem = this[i++]) {
        if (elem.nodeType === 1 && (" " + stripAndCollapse(getClass(elem)) + " ").indexOf(className) > -1) {
          return true;
        }
      }

      return false;
    }
  });
  var rreturn = /\r/g;
  djQ.fn.extend({
    val: function val(value) {
      var hooks,
          ret,
          valueIsFunction,
          elem = this[0];

      if (!arguments.length) {
        if (elem) {
          hooks = djQ.valHooks[elem.type] || djQ.valHooks[elem.nodeName.toLowerCase()];

          if (hooks && "get" in hooks && (ret = hooks.get(elem, "value")) !== undefined) {
            return ret;
          }

          ret = elem.value; // Handle most common string cases

          if (typeof ret === "string") {
            return ret.replace(rreturn, "");
          } // Handle cases where value is null/undef or number


          return ret == null ? "" : ret;
        }

        return;
      }

      valueIsFunction = isFunction(value);
      return this.each(function (i) {
        var val;

        if (this.nodeType !== 1) {
          return;
        }

        if (valueIsFunction) {
          val = value.call(this, i, djQ(this).val());
        } else {
          val = value;
        } // Treat null/undefined as ""; convert numbers to string


        if (val == null) {
          val = "";
        } else if (typeof val === "number") {
          val += "";
        } else if (Array.isArray(val)) {
          val = djQ.map(val, function (value) {
            return value == null ? "" : value + "";
          });
        }

        hooks = djQ.valHooks[this.type] || djQ.valHooks[this.nodeName.toLowerCase()]; // If set returns undefined, fall back to normal setting

        if (!hooks || !("set" in hooks) || hooks.set(this, val, "value") === undefined) {
          this.value = val;
        }
      });
    }
  });
  djQ.extend({
    valHooks: {
      option: {
        get: function get(elem) {
          var val = djQ.find.attr(elem, "value");
          return val != null ? val : // Support: IE <=10 - 11 only
          // option.text throws exceptions (#14686, #14858)
          // Strip and collapse whitespace
          // https://html.spec.whatwg.org/#strip-and-collapse-whitespace
          stripAndCollapse(djQ.text(elem));
        }
      },
      select: {
        get: function get(elem) {
          var value,
              option,
              i,
              options = elem.options,
              index = elem.selectedIndex,
              one = elem.type === "select-one",
              values = one ? null : [],
              max = one ? index + 1 : options.length;

          if (index < 0) {
            i = max;
          } else {
            i = one ? index : 0;
          } // Loop through all the selected options


          for (; i < max; i++) {
            option = options[i]; // Support: IE <=9 only
            // IE8-9 doesn't update selected after form reset (#2551)

            if ((option.selected || i === index) && // Don't return options that are disabled or in a disabled optgroup
            !option.disabled && (!option.parentNode.disabled || !nodeName(option.parentNode, "optgroup"))) {
              // Get the specific value for the option
              value = djQ(option).val(); // We don't need an array for one selects

              if (one) {
                return value;
              } // Multi-Selects return an array


              values.push(value);
            }
          }

          return values;
        },
        set: function set(elem, value) {
          var optionSet,
              option,
              options = elem.options,
              values = djQ.makeArray(value),
              i = options.length;

          while (i--) {
            option = options[i];
            /* eslint-disable no-cond-assign */

            if (option.selected = djQ.inArray(djQ.valHooks.option.get(option), values) > -1) {
              optionSet = true;
            }
            /* eslint-enable no-cond-assign */

          } // Force browsers to behave consistently when non-matching value is set


          if (!optionSet) {
            elem.selectedIndex = -1;
          }

          return values;
        }
      }
    }
  }); // Radios and checkboxes getter/setter

  djQ.each(["radio", "checkbox"], function () {
    djQ.valHooks[this] = {
      set: function set(elem, value) {
        if (Array.isArray(value)) {
          return elem.checked = djQ.inArray(djQ(elem).val(), value) > -1;
        }
      }
    };

    if (!support.checkOn) {
      djQ.valHooks[this].get = function (elem) {
        return elem.getAttribute("value") === null ? "on" : elem.value;
      };
    }
  }); // Return djQ for attributes-only inclusion

  support.focusin = "onfocusin" in window;

  var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
      stopPropagationCallback = function stopPropagationCallback(e) {
    e.stopPropagation();
  };

  djQ.extend(djQ.event, {
    trigger: function trigger(event, data, elem, onlyHandlers) {
      var i,
          cur,
          tmp,
          bubbleType,
          ontype,
          handle,
          special,
          lastElement,
          eventPath = [elem || document],
          type = hasOwn.call(event, "type") ? event.type : event,
          namespaces = hasOwn.call(event, "namespace") ? event.namespace.split(".") : [];
      cur = lastElement = tmp = elem = elem || document; // Don't do events on text and comment nodes

      if (elem.nodeType === 3 || elem.nodeType === 8) {
        return;
      } // focus/blur morphs to focusin/out; ensure we're not firing them right now


      if (rfocusMorph.test(type + djQ.event.triggered)) {
        return;
      }

      if (type.indexOf(".") > -1) {
        // Namespaced trigger; create a regexp to match event type in handle()
        namespaces = type.split(".");
        type = namespaces.shift();
        namespaces.sort();
      }

      ontype = type.indexOf(":") < 0 && "on" + type; // Caller can pass in a djQ.Event object, Object, or just an event type string

      event = event[djQ.expando] ? event : new djQ.Event(type, _typeof(event) === "object" && event); // Trigger bitmask: & 1 for native handlers; & 2 for djQ (always true)

      event.isTrigger = onlyHandlers ? 2 : 3;
      event.namespace = namespaces.join(".");
      event.rnamespace = event.namespace ? new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)") : null; // Clean up the event in case it is being reused

      event.result = undefined;

      if (!event.target) {
        event.target = elem;
      } // Clone any incoming data and prepend the event, creating the handler arg list


      data = data == null ? [event] : djQ.makeArray(data, [event]); // Allow special events to draw outside the lines

      special = djQ.event.special[type] || {};

      if (!onlyHandlers && special.trigger && special.trigger.apply(elem, data) === false) {
        return;
      } // Determine event propagation path in advance, per W3C events spec (#9951)
      // Bubble up to document, then to window; watch for a global ownerDocument var (#9724)


      if (!onlyHandlers && !special.noBubble && !isWindow(elem)) {
        bubbleType = special.delegateType || type;

        if (!rfocusMorph.test(bubbleType + type)) {
          cur = cur.parentNode;
        }

        for (; cur; cur = cur.parentNode) {
          eventPath.push(cur);
          tmp = cur;
        } // Only add window if we got to document (e.g., not plain obj or detached DOM)


        if (tmp === (elem.ownerDocument || document)) {
          eventPath.push(tmp.defaultView || tmp.parentWindow || window);
        }
      } // Fire handlers on the event path


      i = 0;

      while ((cur = eventPath[i++]) && !event.isPropagationStopped()) {
        lastElement = cur;
        event.type = i > 1 ? bubbleType : special.bindType || type; // djQ handler

        handle = (dataPriv.get(cur, "events") || Object.create(null))[event.type] && dataPriv.get(cur, "handle");

        if (handle) {
          handle.apply(cur, data);
        } // Native handler


        handle = ontype && cur[ontype];

        if (handle && handle.apply && acceptData(cur)) {
          event.result = handle.apply(cur, data);

          if (event.result === false) {
            event.preventDefault();
          }
        }
      }

      event.type = type; // If nobody prevented the default action, do it now

      if (!onlyHandlers && !event.isDefaultPrevented()) {
        if ((!special._default || special._default.apply(eventPath.pop(), data) === false) && acceptData(elem)) {
          // Call a native DOM method on the target with the same name as the event.
          // Don't do default actions on window, that's where global variables be (#6170)
          if (ontype && isFunction(elem[type]) && !isWindow(elem)) {
            // Don't re-trigger an onFOO event when we call its FOO() method
            tmp = elem[ontype];

            if (tmp) {
              elem[ontype] = null;
            } // Prevent re-triggering of the same event, since we already bubbled it above


            djQ.event.triggered = type;

            if (event.isPropagationStopped()) {
              lastElement.addEventListener(type, stopPropagationCallback);
            }

            elem[type]();

            if (event.isPropagationStopped()) {
              lastElement.removeEventListener(type, stopPropagationCallback);
            }

            djQ.event.triggered = undefined;

            if (tmp) {
              elem[ontype] = tmp;
            }
          }
        }
      }

      return event.result;
    },
    // Piggyback on a donor event to simulate a different one
    // Used only for `focus(in | out)` events
    simulate: function simulate(type, elem, event) {
      var e = djQ.extend(new djQ.Event(), event, {
        type: type,
        isSimulated: true
      });
      djQ.event.trigger(e, null, elem);
    }
  });
  djQ.fn.extend({
    trigger: function trigger(type, data) {
      return this.each(function () {
        djQ.event.trigger(type, data, this);
      });
    },
    triggerHandler: function triggerHandler(type, data) {
      var elem = this[0];

      if (elem) {
        return djQ.event.trigger(type, data, elem, true);
      }
    }
  }); // Support: Firefox <=44
  // Firefox doesn't have focus(in | out) events
  // Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
  //
  // Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
  // focus(in | out) events fire after focus & blur events,
  // which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
  // Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857

  if (!support.focusin) {
    djQ.each({
      focus: "focusin",
      blur: "focusout"
    }, function (orig, fix) {
      // Attach a single capturing handler on the document while someone wants focusin/focusout
      var handler = function handler(event) {
        djQ.event.simulate(fix, event.target, djQ.event.fix(event));
      };

      djQ.event.special[fix] = {
        setup: function setup() {
          // Handle: regular nodes (via `this.ownerDocument`), window
          // (via `this.document`) & document (via `this`).
          var doc = this.ownerDocument || this.document || this,
              attaches = dataPriv.access(doc, fix);

          if (!attaches) {
            doc.addEventListener(orig, handler, true);
          }

          dataPriv.access(doc, fix, (attaches || 0) + 1);
        },
        teardown: function teardown() {
          var doc = this.ownerDocument || this.document || this,
              attaches = dataPriv.access(doc, fix) - 1;

          if (!attaches) {
            doc.removeEventListener(orig, handler, true);
            dataPriv.remove(doc, fix);
          } else {
            dataPriv.access(doc, fix, attaches);
          }
        }
      };
    });
  }

  var location = window.location;
  var nonce = {
    guid: Date.now()
  };
  var rquery = /\?/; // Cross-browser xml parsing

  djQ.parseXML = function (data) {
    var xml;

    if (!data || typeof data !== "string") {
      return null;
    } // Support: IE 9 - 11 only
    // IE throws on parseFromString with invalid input.


    try {
      xml = new window.DOMParser().parseFromString(data, "text/xml");
    } catch (e) {
      xml = undefined;
    }

    if (!xml || xml.getElementsByTagName("parsererror").length) {
      djQ.error("Invalid XML: " + data);
    }

    return xml;
  };

  var rbracket = /\[\]$/,
      rCRLF = /\r?\n/g,
      rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
      rsubmittable = /^(?:input|select|textarea|keygen)/i;

  function buildParams(prefix, obj, traditional, add) {
    var name;

    if (Array.isArray(obj)) {
      // Serialize array item.
      djQ.each(obj, function (i, v) {
        if (traditional || rbracket.test(prefix)) {
          // Treat each array item as a scalar.
          add(prefix, v);
        } else {
          // Item is non-scalar (array or object), encode its numeric index.
          buildParams(prefix + "[" + (_typeof(v) === "object" && v != null ? i : "") + "]", v, traditional, add);
        }
      });
    } else if (!traditional && toType(obj) === "object") {
      // Serialize object item.
      for (name in obj) {
        buildParams(prefix + "[" + name + "]", obj[name], traditional, add);
      }
    } else {
      // Serialize scalar item.
      add(prefix, obj);
    }
  } // Serialize an array of form elements or a set of
  // key/values into a query string


  djQ.param = function (a, traditional) {
    var prefix,
        s = [],
        add = function add(key, valueOrFunction) {
      // If value is a function, invoke it and use its return value
      var value = isFunction(valueOrFunction) ? valueOrFunction() : valueOrFunction;
      s[s.length] = encodeURIComponent(key) + "=" + encodeURIComponent(value == null ? "" : value);
    };

    if (a == null) {
      return "";
    } // If an array was passed in, assume that it is an array of form elements.


    if (Array.isArray(a) || a.jquery && !djQ.isPlainObject(a)) {
      // Serialize the form elements
      djQ.each(a, function () {
        add(this.name, this.value);
      });
    } else {
      // If traditional, encode the "old" way (the way 1.3.2 or older
      // did it), otherwise encode params recursively.
      for (prefix in a) {
        buildParams(prefix, a[prefix], traditional, add);
      }
    } // Return the resulting serialization


    return s.join("&");
  };

  djQ.fn.extend({
    serialize: function serialize() {
      return djQ.param(this.serializeArray());
    },
    serializeArray: function serializeArray() {
      return this.map(function () {
        // Can add propHook for "elements" to filter or add form elements
        var elements = djQ.prop(this, "elements");
        return elements ? djQ.makeArray(elements) : this;
      }).filter(function () {
        var type = this.type; // Use .is( ":disabled" ) so that fieldset[disabled] works

        return this.name && !djQ(this).is(":disabled") && rsubmittable.test(this.nodeName) && !rsubmitterTypes.test(type) && (this.checked || !rcheckableType.test(type));
      }).map(function (_i, elem) {
        var val = djQ(this).val();

        if (val == null) {
          return null;
        }

        if (Array.isArray(val)) {
          return djQ.map(val, function (val) {
            return {
              name: elem.name,
              value: val.replace(rCRLF, "\r\n")
            };
          });
        }

        return {
          name: elem.name,
          value: val.replace(rCRLF, "\r\n")
        };
      }).get();
    }
  });
  var r20 = /%20/g,
      rhash = /#.*$/,
      rantiCache = /([?&])_=[^&]*/,
      rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,
      // #7653, #8125, #8152: local protocol detection
  rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
      rnoContent = /^(?:GET|HEAD)$/,
      rprotocol = /^\/\//,

  /* Prefilters
   * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
   * 2) These are called:
   *    - BEFORE asking for a transport
   *    - AFTER param serialization (s.data is a string if s.processData is true)
   * 3) key is the dataType
   * 4) the catchall symbol "*" can be used
   * 5) execution will start with transport dataType and THEN continue down to "*" if needed
   */
  prefilters = {},

  /* Transports bindings
   * 1) key is the dataType
   * 2) the catchall symbol "*" can be used
   * 3) selection will start with transport dataType and THEN go to "*" if needed
   */
  transports = {},
      // Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
  allTypes = "*/".concat("*"),
      // Anchor tag for parsing the document origin
  originAnchor = document.createElement("a");
  originAnchor.href = location.href; // Base "constructor" for djQ.ajaxPrefilter and djQ.ajaxTransport

  function addToPrefiltersOrTransports(structure) {
    // dataTypeExpression is optional and defaults to "*"
    return function (dataTypeExpression, func) {
      if (typeof dataTypeExpression !== "string") {
        func = dataTypeExpression;
        dataTypeExpression = "*";
      }

      var dataType,
          i = 0,
          dataTypes = dataTypeExpression.toLowerCase().match(rnothtmlwhite) || [];

      if (isFunction(func)) {
        // For each dataType in the dataTypeExpression
        while (dataType = dataTypes[i++]) {
          // Prepend if requested
          if (dataType[0] === "+") {
            dataType = dataType.slice(1) || "*";
            (structure[dataType] = structure[dataType] || []).unshift(func); // Otherwise append
          } else {
            (structure[dataType] = structure[dataType] || []).push(func);
          }
        }
      }
    };
  } // Base inspection function for prefilters and transports


  function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR) {
    var inspected = {},
        seekingTransport = structure === transports;

    function inspect(dataType) {
      var selected;
      inspected[dataType] = true;
      djQ.each(structure[dataType] || [], function (_, prefilterOrFactory) {
        var dataTypeOrTransport = prefilterOrFactory(options, originalOptions, jqXHR);

        if (typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[dataTypeOrTransport]) {
          options.dataTypes.unshift(dataTypeOrTransport);
          inspect(dataTypeOrTransport);
          return false;
        } else if (seekingTransport) {
          return !(selected = dataTypeOrTransport);
        }
      });
      return selected;
    }

    return inspect(options.dataTypes[0]) || !inspected["*"] && inspect("*");
  } // A special extend for ajax options
  // that takes "flat" options (not to be deep extended)
  // Fixes #9887


  function ajaxExtend(target, src) {
    var key,
        deep,
        flatOptions = djQ.ajaxSettings.flatOptions || {};

    for (key in src) {
      if (src[key] !== undefined) {
        (flatOptions[key] ? target : deep || (deep = {}))[key] = src[key];
      }
    }

    if (deep) {
      djQ.extend(true, target, deep);
    }

    return target;
  }
  /* Handles responses to an ajax request:
   * - finds the right dataType (mediates between content-type and expected dataType)
   * - returns the corresponding response
   */


  function ajaxHandleResponses(s, jqXHR, responses) {
    var ct,
        type,
        finalDataType,
        firstDataType,
        contents = s.contents,
        dataTypes = s.dataTypes; // Remove auto dataType and get content-type in the process

    while (dataTypes[0] === "*") {
      dataTypes.shift();

      if (ct === undefined) {
        ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
      }
    } // Check if we're dealing with a known content-type


    if (ct) {
      for (type in contents) {
        if (contents[type] && contents[type].test(ct)) {
          dataTypes.unshift(type);
          break;
        }
      }
    } // Check to see if we have a response for the expected dataType


    if (dataTypes[0] in responses) {
      finalDataType = dataTypes[0];
    } else {
      // Try convertible dataTypes
      for (type in responses) {
        if (!dataTypes[0] || s.converters[type + " " + dataTypes[0]]) {
          finalDataType = type;
          break;
        }

        if (!firstDataType) {
          firstDataType = type;
        }
      } // Or just use first one


      finalDataType = finalDataType || firstDataType;
    } // If we found a dataType
    // We add the dataType to the list if needed
    // and return the corresponding response


    if (finalDataType) {
      if (finalDataType !== dataTypes[0]) {
        dataTypes.unshift(finalDataType);
      }

      return responses[finalDataType];
    }
  }
  /* Chain conversions given the request and the original response
   * Also sets the responseXXX fields on the jqXHR instance
   */


  function ajaxConvert(s, response, jqXHR, isSuccess) {
    var conv2,
        current,
        conv,
        tmp,
        prev,
        converters = {},
        // Work with a copy of dataTypes in case we need to modify it for conversion
    dataTypes = s.dataTypes.slice(); // Create converters map with lowercased keys

    if (dataTypes[1]) {
      for (conv in s.converters) {
        converters[conv.toLowerCase()] = s.converters[conv];
      }
    }

    current = dataTypes.shift(); // Convert to each sequential dataType

    while (current) {
      if (s.responseFields[current]) {
        jqXHR[s.responseFields[current]] = response;
      } // Apply the dataFilter if provided


      if (!prev && isSuccess && s.dataFilter) {
        response = s.dataFilter(response, s.dataType);
      }

      prev = current;
      current = dataTypes.shift();

      if (current) {
        // There's only work to do if current dataType is non-auto
        if (current === "*") {
          current = prev; // Convert response if prev dataType is non-auto and differs from current
        } else if (prev !== "*" && prev !== current) {
          // Seek a direct converter
          conv = converters[prev + " " + current] || converters["* " + current]; // If none found, seek a pair

          if (!conv) {
            for (conv2 in converters) {
              // If conv2 outputs current
              tmp = conv2.split(" ");

              if (tmp[1] === current) {
                // If prev can be converted to accepted input
                conv = converters[prev + " " + tmp[0]] || converters["* " + tmp[0]];

                if (conv) {
                  // Condense equivalence converters
                  if (conv === true) {
                    conv = converters[conv2]; // Otherwise, insert the intermediate dataType
                  } else if (converters[conv2] !== true) {
                    current = tmp[0];
                    dataTypes.unshift(tmp[1]);
                  }

                  break;
                }
              }
            }
          } // Apply converter (if not an equivalence)


          if (conv !== true) {
            // Unless errors are allowed to bubble, catch and return them
            if (conv && s.throws) {
              response = conv(response);
            } else {
              try {
                response = conv(response);
              } catch (e) {
                return {
                  state: "parsererror",
                  error: conv ? e : "No conversion from " + prev + " to " + current
                };
              }
            }
          }
        }
      }
    }

    return {
      state: "success",
      data: response
    };
  }

  djQ.extend({
    // Counter for holding the number of active queries
    active: 0,
    // Last-Modified header cache for next request
    lastModified: {},
    etag: {},
    ajaxSettings: {
      url: location.href,
      type: "GET",
      isLocal: rlocalProtocol.test(location.protocol),
      global: true,
      processData: true,
      async: true,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",

      /*
      timeout: 0,
      data: null,
      dataType: null,
      username: null,
      password: null,
      cache: null,
      throws: false,
      traditional: false,
      headers: {},
      */
      accepts: {
        "*": allTypes,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript"
      },
      contents: {
        xml: /\bxml\b/,
        html: /\bhtml/,
        json: /\bjson\b/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON"
      },
      // Data converters
      // Keys separate source (or catchall "*") and destination types with a single space
      converters: {
        // Convert anything to text
        "* text": String,
        // Text to html (true = no transformation)
        "text html": true,
        // Evaluate text as a json expression
        "text json": JSON.parse,
        // Parse text as xml
        "text xml": djQ.parseXML
      },
      // For options that shouldn't be deep extended:
      // you can add your own custom options here if
      // and when you create one that shouldn't be
      // deep extended (see ajaxExtend)
      flatOptions: {
        url: true,
        context: true
      }
    },
    // Creates a full fledged settings object into target
    // with both ajaxSettings and settings fields.
    // If target is omitted, writes into ajaxSettings.
    ajaxSetup: function ajaxSetup(target, settings) {
      return settings ? // Building a settings object
      ajaxExtend(ajaxExtend(target, djQ.ajaxSettings), settings) : // Extending ajaxSettings
      ajaxExtend(djQ.ajaxSettings, target);
    },
    ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
    ajaxTransport: addToPrefiltersOrTransports(transports),
    // Main method
    ajax: function ajax(url, options) {
      // If url is an object, simulate pre-1.5 signature
      if (_typeof(url) === "object") {
        options = url;
        url = undefined;
      } // Force options to be an object


      options = options || {};

      var transport,
          // URL without anti-cache param
      cacheURL,
          // Response headers
      responseHeadersString,
          responseHeaders,
          // timeout handle
      timeoutTimer,
          // Url cleanup var
      urlAnchor,
          // Request state (becomes false upon send and true upon completion)
      completed,
          // To know if global events are to be dispatched
      fireGlobals,
          // Loop variable
      i,
          // uncached part of the url
      uncached,
          // Create the final options object
      s = djQ.ajaxSetup({}, options),
          // Callbacks context
      callbackContext = s.context || s,
          // Context for global events is callbackContext if it is a DOM node or djQ collection
      globalEventContext = s.context && (callbackContext.nodeType || callbackContext.jquery) ? djQ(callbackContext) : djQ.event,
          // Deferreds
      deferred = djQ.Deferred(),
          completeDeferred = djQ.Callbacks("once memory"),
          // Status-dependent callbacks
      _statusCode = s.statusCode || {},
          // Headers (they are sent all at once)
      requestHeaders = {},
          requestHeadersNames = {},
          // Default abort message
      strAbort = "canceled",
          // Fake xhr
      jqXHR = {
        readyState: 0,
        // Builds headers hashtable if needed
        getResponseHeader: function getResponseHeader(key) {
          var match;

          if (completed) {
            if (!responseHeaders) {
              responseHeaders = {};

              while (match = rheaders.exec(responseHeadersString)) {
                responseHeaders[match[1].toLowerCase() + " "] = (responseHeaders[match[1].toLowerCase() + " "] || []).concat(match[2]);
              }
            }

            match = responseHeaders[key.toLowerCase() + " "];
          }

          return match == null ? null : match.join(", ");
        },
        // Raw string
        getAllResponseHeaders: function getAllResponseHeaders() {
          return completed ? responseHeadersString : null;
        },
        // Caches the header
        setRequestHeader: function setRequestHeader(name, value) {
          if (completed == null) {
            name = requestHeadersNames[name.toLowerCase()] = requestHeadersNames[name.toLowerCase()] || name;
            requestHeaders[name] = value;
          }

          return this;
        },
        // Overrides response content-type header
        overrideMimeType: function overrideMimeType(type) {
          if (completed == null) {
            s.mimeType = type;
          }

          return this;
        },
        // Status-dependent callbacks
        statusCode: function statusCode(map) {
          var code;

          if (map) {
            if (completed) {
              // Execute the appropriate callbacks
              jqXHR.always(map[jqXHR.status]);
            } else {
              // Lazy-add the new callbacks in a way that preserves old ones
              for (code in map) {
                _statusCode[code] = [_statusCode[code], map[code]];
              }
            }
          }

          return this;
        },
        // Cancel the request
        abort: function abort(statusText) {
          var finalText = statusText || strAbort;

          if (transport) {
            transport.abort(finalText);
          }

          done(0, finalText);
          return this;
        }
      }; // Attach deferreds


      deferred.promise(jqXHR); // Add protocol if not provided (prefilters might expect it)
      // Handle falsy url in the settings object (#10093: consistency with old signature)
      // We also use the url parameter if available

      s.url = ((url || s.url || location.href) + "").replace(rprotocol, location.protocol + "//"); // Alias method option to type as per ticket #12004

      s.type = options.method || options.type || s.method || s.type; // Extract dataTypes list

      s.dataTypes = (s.dataType || "*").toLowerCase().match(rnothtmlwhite) || [""]; // A cross-domain request is in order when the origin doesn't match the current origin.

      if (s.crossDomain == null) {
        urlAnchor = document.createElement("a"); // Support: IE <=8 - 11, Edge 12 - 15
        // IE throws exception on accessing the href property if url is malformed,
        // e.g. http://example.com:80x/

        try {
          urlAnchor.href = s.url; // Support: IE <=8 - 11 only
          // Anchor's host property isn't correctly set when s.url is relative

          urlAnchor.href = urlAnchor.href;
          s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !== urlAnchor.protocol + "//" + urlAnchor.host;
        } catch (e) {
          // If there is an error parsing the URL, assume it is crossDomain,
          // it can be rejected by the transport if it is invalid
          s.crossDomain = true;
        }
      } // Convert data if not already a string


      if (s.data && s.processData && typeof s.data !== "string") {
        s.data = djQ.param(s.data, s.traditional);
      } // Apply prefilters


      inspectPrefiltersOrTransports(prefilters, s, options, jqXHR); // If request was aborted inside a prefilter, stop there

      if (completed) {
        return jqXHR;
      } // We can fire global events as of now if asked to
      // Don't fire events if djQ.event is undefined in an AMD-usage scenario (#15118)


      fireGlobals = djQ.event && s.global; // Watch for a new set of requests

      if (fireGlobals && djQ.active++ === 0) {
        djQ.event.trigger("ajaxStart");
      } // Uppercase the type


      s.type = s.type.toUpperCase(); // Determine if request has content

      s.hasContent = !rnoContent.test(s.type); // Save the URL in case we're toying with the If-Modified-Since
      // and/or If-None-Match header later on
      // Remove hash to simplify url manipulation

      cacheURL = s.url.replace(rhash, ""); // More options handling for requests with no content

      if (!s.hasContent) {
        // Remember the hash so we can put it back
        uncached = s.url.slice(cacheURL.length); // If data is available and should be processed, append data to url

        if (s.data && (s.processData || typeof s.data === "string")) {
          cacheURL += (rquery.test(cacheURL) ? "&" : "?") + s.data; // #9682: remove data so that it's not used in an eventual retry

          delete s.data;
        } // Add or update anti-cache param if needed


        if (s.cache === false) {
          cacheURL = cacheURL.replace(rantiCache, "$1");
          uncached = (rquery.test(cacheURL) ? "&" : "?") + "_=" + nonce.guid++ + uncached;
        } // Put hash and anti-cache on the URL that will be requested (gh-1732)


        s.url = cacheURL + uncached; // Change '%20' to '+' if this is encoded form body content (gh-2658)
      } else if (s.data && s.processData && (s.contentType || "").indexOf("application/x-www-form-urlencoded") === 0) {
        s.data = s.data.replace(r20, "+");
      } // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.


      if (s.ifModified) {
        if (djQ.lastModified[cacheURL]) {
          jqXHR.setRequestHeader("If-Modified-Since", djQ.lastModified[cacheURL]);
        }

        if (djQ.etag[cacheURL]) {
          jqXHR.setRequestHeader("If-None-Match", djQ.etag[cacheURL]);
        }
      } // Set the correct header, if data is being sent


      if (s.data && s.hasContent && s.contentType !== false || options.contentType) {
        jqXHR.setRequestHeader("Content-Type", s.contentType);
      } // Set the Accepts header for the server, depending on the dataType


      jqXHR.setRequestHeader("Accept", s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + (s.dataTypes[0] !== "*" ? ", " + allTypes + "; q=0.01" : "") : s.accepts["*"]); // Check for headers option

      for (i in s.headers) {
        jqXHR.setRequestHeader(i, s.headers[i]);
      } // Allow custom headers/mimetypes and early abort


      if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === false || completed)) {
        // Abort if not done already and return
        return jqXHR.abort();
      } // Aborting is no longer a cancellation


      strAbort = "abort"; // Install callbacks on deferreds

      completeDeferred.add(s.complete);
      jqXHR.done(s.success);
      jqXHR.fail(s.error); // Get transport

      transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR); // If no transport, we auto-abort

      if (!transport) {
        done(-1, "No Transport");
      } else {
        jqXHR.readyState = 1; // Send global event

        if (fireGlobals) {
          globalEventContext.trigger("ajaxSend", [jqXHR, s]);
        } // If request was aborted inside ajaxSend, stop there


        if (completed) {
          return jqXHR;
        } // Timeout


        if (s.async && s.timeout > 0) {
          timeoutTimer = window.setTimeout(function () {
            jqXHR.abort("timeout");
          }, s.timeout);
        }

        try {
          completed = false;
          transport.send(requestHeaders, done);
        } catch (e) {
          // Rethrow post-completion exceptions
          if (completed) {
            throw e;
          } // Propagate others as results


          done(-1, e);
        }
      } // Callback for when everything is done


      function done(status, nativeStatusText, responses, headers) {
        var isSuccess,
            success,
            error,
            response,
            modified,
            statusText = nativeStatusText; // Ignore repeat invocations

        if (completed) {
          return;
        }

        completed = true; // Clear timeout if it exists

        if (timeoutTimer) {
          window.clearTimeout(timeoutTimer);
        } // Dereference transport for early garbage collection
        // (no matter how long the jqXHR object will be used)


        transport = undefined; // Cache response headers

        responseHeadersString = headers || ""; // Set readyState

        jqXHR.readyState = status > 0 ? 4 : 0; // Determine if successful

        isSuccess = status >= 200 && status < 300 || status === 304; // Get response data

        if (responses) {
          response = ajaxHandleResponses(s, jqXHR, responses);
        } // Use a noop converter for missing script


        if (!isSuccess && djQ.inArray("script", s.dataTypes) > -1) {
          s.converters["text script"] = function () {};
        } // Convert no matter what (that way responseXXX fields are always set)


        response = ajaxConvert(s, response, jqXHR, isSuccess); // If successful, handle type chaining

        if (isSuccess) {
          // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
          if (s.ifModified) {
            modified = jqXHR.getResponseHeader("Last-Modified");

            if (modified) {
              djQ.lastModified[cacheURL] = modified;
            }

            modified = jqXHR.getResponseHeader("etag");

            if (modified) {
              djQ.etag[cacheURL] = modified;
            }
          } // if no content


          if (status === 204 || s.type === "HEAD") {
            statusText = "nocontent"; // if not modified
          } else if (status === 304) {
            statusText = "notmodified"; // If we have data, let's convert it
          } else {
            statusText = response.state;
            success = response.data;
            error = response.error;
            isSuccess = !error;
          }
        } else {
          // Extract error from statusText and normalize for non-aborts
          error = statusText;

          if (status || !statusText) {
            statusText = "error";

            if (status < 0) {
              status = 0;
            }
          }
        } // Set data for the fake xhr object


        jqXHR.status = status;
        jqXHR.statusText = (nativeStatusText || statusText) + ""; // Success/Error

        if (isSuccess) {
          deferred.resolveWith(callbackContext, [success, statusText, jqXHR]);
        } else {
          deferred.rejectWith(callbackContext, [jqXHR, statusText, error]);
        } // Status-dependent callbacks


        jqXHR.statusCode(_statusCode);
        _statusCode = undefined;

        if (fireGlobals) {
          globalEventContext.trigger(isSuccess ? "ajaxSuccess" : "ajaxError", [jqXHR, s, isSuccess ? success : error]);
        } // Complete


        completeDeferred.fireWith(callbackContext, [jqXHR, statusText]);

        if (fireGlobals) {
          globalEventContext.trigger("ajaxComplete", [jqXHR, s]); // Handle the global AJAX counter

          if (! --djQ.active) {
            djQ.event.trigger("ajaxStop");
          }
        }
      }

      return jqXHR;
    },
    getJSON: function getJSON(url, data, callback) {
      return djQ.get(url, data, callback, "json");
    },
    getScript: function getScript(url, callback) {
      return djQ.get(url, undefined, callback, "script");
    }
  });
  djQ.each(["get", "post"], function (_i, method) {
    djQ[method] = function (url, data, callback, type) {
      // Shift arguments if data argument was omitted
      if (isFunction(data)) {
        type = type || callback;
        callback = data;
        data = undefined;
      } // The url can be an options object (which then must have .url)


      return djQ.ajax(djQ.extend({
        url: url,
        type: method,
        dataType: type,
        data: data,
        success: callback
      }, djQ.isPlainObject(url) && url));
    };
  });
  djQ.ajaxPrefilter(function (s) {
    var i;

    for (i in s.headers) {
      if (i.toLowerCase() === "content-type") {
        s.contentType = s.headers[i] || "";
      }
    }
  });

  djQ._evalUrl = function (url, options, doc) {
    return djQ.ajax({
      url: url,
      // Make this explicit, since user can override this through ajaxSetup (#11264)
      type: "GET",
      dataType: "script",
      cache: true,
      async: false,
      global: false,
      // Only evaluate the response if it is successful (gh-4126)
      // dataFilter is not invoked for failure responses, so using it instead
      // of the default converter is kludgy but it works.
      converters: {
        "text script": function textScript() {}
      },
      dataFilter: function dataFilter(response) {
        djQ.globalEval(response, options, doc);
      }
    });
  };

  djQ.fn.extend({
    wrapAll: function wrapAll(html) {
      var wrap;

      if (this[0]) {
        if (isFunction(html)) {
          html = html.call(this[0]);
        } // The elements to wrap the target around


        wrap = djQ(html, this[0].ownerDocument).eq(0).clone(true);

        if (this[0].parentNode) {
          wrap.insertBefore(this[0]);
        }

        wrap.map(function () {
          var elem = this;

          while (elem.firstElementChild) {
            elem = elem.firstElementChild;
          }

          return elem;
        }).append(this);
      }

      return this;
    },
    wrapInner: function wrapInner(html) {
      if (isFunction(html)) {
        return this.each(function (i) {
          djQ(this).wrapInner(html.call(this, i));
        });
      }

      return this.each(function () {
        var self = djQ(this),
            contents = self.contents();

        if (contents.length) {
          contents.wrapAll(html);
        } else {
          self.append(html);
        }
      });
    },
    wrap: function wrap(html) {
      var htmlIsFunction = isFunction(html);
      return this.each(function (i) {
        djQ(this).wrapAll(htmlIsFunction ? html.call(this, i) : html);
      });
    },
    unwrap: function unwrap(selector) {
      this.parent(selector).not("body").each(function () {
        djQ(this).replaceWith(this.childNodes);
      });
      return this;
    }
  });

  djQ.expr.pseudos.hidden = function (elem) {
    return !djQ.expr.pseudos.visible(elem);
  };

  djQ.expr.pseudos.visible = function (elem) {
    return !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
  };

  djQ.ajaxSettings.xhr = function () {
    try {
      return new window.XMLHttpRequest();
    } catch (e) {}
  };

  var xhrSuccessStatus = {
    // File protocol always yields status code 0, assume 200
    0: 200,
    // Support: IE <=9 only
    // #1450: sometimes IE returns 1223 when it should be 204
    1223: 204
  },
      xhrSupported = djQ.ajaxSettings.xhr();
  support.cors = !!xhrSupported && "withCredentials" in xhrSupported;
  support.ajax = xhrSupported = !!xhrSupported;
  djQ.ajaxTransport(function (options) {
    var _callback, errorCallback; // Cross domain only allowed if supported through XMLHttpRequest


    if (support.cors || xhrSupported && !options.crossDomain) {
      return {
        send: function send(headers, complete) {
          var i,
              xhr = options.xhr();
          xhr.open(options.type, options.url, options.async, options.username, options.password); // Apply custom fields if provided

          if (options.xhrFields) {
            for (i in options.xhrFields) {
              xhr[i] = options.xhrFields[i];
            }
          } // Override mime type if needed


          if (options.mimeType && xhr.overrideMimeType) {
            xhr.overrideMimeType(options.mimeType);
          } // X-Requested-With header
          // For cross-domain requests, seeing as conditions for a preflight are
          // akin to a jigsaw puzzle, we simply never set it to be sure.
          // (it can always be set on a per-request basis or even using ajaxSetup)
          // For same-domain requests, won't change header if already provided.


          if (!options.crossDomain && !headers["X-Requested-With"]) {
            headers["X-Requested-With"] = "XMLHttpRequest";
          } // Set headers


          for (i in headers) {
            xhr.setRequestHeader(i, headers[i]);
          } // Callback


          _callback = function callback(type) {
            return function () {
              if (_callback) {
                _callback = errorCallback = xhr.onload = xhr.onerror = xhr.onabort = xhr.ontimeout = xhr.onreadystatechange = null;

                if (type === "abort") {
                  xhr.abort();
                } else if (type === "error") {
                  // Support: IE <=9 only
                  // On a manual native abort, IE9 throws
                  // errors on any property access that is not readyState
                  if (typeof xhr.status !== "number") {
                    complete(0, "error");
                  } else {
                    complete( // File: protocol always yields status 0; see #8605, #14207
                    xhr.status, xhr.statusText);
                  }
                } else {
                  complete(xhrSuccessStatus[xhr.status] || xhr.status, xhr.statusText, // Support: IE <=9 only
                  // IE9 has no XHR2 but throws on binary (trac-11426)
                  // For XHR2 non-text, let the caller handle it (gh-2498)
                  (xhr.responseType || "text") !== "text" || typeof xhr.responseText !== "string" ? {
                    binary: xhr.response
                  } : {
                    text: xhr.responseText
                  }, xhr.getAllResponseHeaders());
                }
              }
            };
          }; // Listen to events


          xhr.onload = _callback();
          errorCallback = xhr.onerror = xhr.ontimeout = _callback("error"); // Support: IE 9 only
          // Use onreadystatechange to replace onabort
          // to handle uncaught aborts

          if (xhr.onabort !== undefined) {
            xhr.onabort = errorCallback;
          } else {
            xhr.onreadystatechange = function () {
              // Check readyState before timeout as it changes
              if (xhr.readyState === 4) {
                // Allow onerror to be called first,
                // but that will not handle a native abort
                // Also, save errorCallback to a variable
                // as xhr.onerror cannot be accessed
                window.setTimeout(function () {
                  if (_callback) {
                    errorCallback();
                  }
                });
              }
            };
          } // Create the abort callback


          _callback = _callback("abort");

          try {
            // Do send the request (this may raise an exception)
            xhr.send(options.hasContent && options.data || null);
          } catch (e) {
            // #14683: Only rethrow if this hasn't been notified as an error yet
            if (_callback) {
              throw e;
            }
          }
        },
        abort: function abort() {
          if (_callback) {
            _callback();
          }
        }
      };
    }
  }); // Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)

  djQ.ajaxPrefilter(function (s) {
    if (s.crossDomain) {
      s.contents.script = false;
    }
  }); // Install script dataType

  djQ.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, " + "application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /\b(?:java|ecma)script\b/
    },
    converters: {
      "text script": function textScript(text) {
        djQ.globalEval(text);
        return text;
      }
    }
  }); // Handle cache's special case and crossDomain

  djQ.ajaxPrefilter("script", function (s) {
    if (s.cache === undefined) {
      s.cache = false;
    }

    if (s.crossDomain) {
      s.type = "GET";
    }
  }); // Bind script tag hack transport

  djQ.ajaxTransport("script", function (s) {
    // This transport only deals with cross domain or forced-by-attrs requests
    if (s.crossDomain || s.scriptAttrs) {
      var script, _callback2;

      return {
        send: function send(_, complete) {
          script = djQ("<script>").attr(s.scriptAttrs || {}).prop({
            charset: s.scriptCharset,
            src: s.url
          }).on("load error", _callback2 = function callback(evt) {
            script.remove();
            _callback2 = null;

            if (evt) {
              complete(evt.type === "error" ? 404 : 200, evt.type);
            }
          }); // Use native DOM manipulation to avoid our domManip AJAX trickery

          document.head.appendChild(script[0]);
        },
        abort: function abort() {
          if (_callback2) {
            _callback2();
          }
        }
      };
    }
  });
  var oldCallbacks = [],
      rjsonp = /(=)\?(?=&|$)|\?\?/; // Default jsonp settings

  djQ.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function jsonpCallback() {
      var callback = oldCallbacks.pop() || djQ.expando + "_" + nonce.guid++;
      this[callback] = true;
      return callback;
    }
  }); // Detect, normalize options and install callbacks for jsonp requests

  djQ.ajaxPrefilter("json jsonp", function (s, originalSettings, jqXHR) {
    var callbackName,
        overwritten,
        responseContainer,
        jsonProp = s.jsonp !== false && (rjsonp.test(s.url) ? "url" : typeof s.data === "string" && (s.contentType || "").indexOf("application/x-www-form-urlencoded") === 0 && rjsonp.test(s.data) && "data"); // Handle iff the expected data type is "jsonp" or we have a parameter to set

    if (jsonProp || s.dataTypes[0] === "jsonp") {
      // Get callback name, remembering preexisting value associated with it
      callbackName = s.jsonpCallback = isFunction(s.jsonpCallback) ? s.jsonpCallback() : s.jsonpCallback; // Insert callback into url or form data

      if (jsonProp) {
        s[jsonProp] = s[jsonProp].replace(rjsonp, "$1" + callbackName);
      } else if (s.jsonp !== false) {
        s.url += (rquery.test(s.url) ? "&" : "?") + s.jsonp + "=" + callbackName;
      } // Use data converter to retrieve json after script execution


      s.converters["script json"] = function () {
        if (!responseContainer) {
          djQ.error(callbackName + " was not called");
        }

        return responseContainer[0];
      }; // Force json dataType


      s.dataTypes[0] = "json"; // Install callback

      overwritten = window[callbackName];

      window[callbackName] = function () {
        responseContainer = arguments;
      }; // Clean-up function (fires after converters)


      jqXHR.always(function () {
        // If previous value didn't exist - remove it
        if (overwritten === undefined) {
          djQ(window).removeProp(callbackName); // Otherwise restore preexisting value
        } else {
          window[callbackName] = overwritten;
        } // Save back as free


        if (s[callbackName]) {
          // Make sure that re-using the options doesn't screw things around
          s.jsonpCallback = originalSettings.jsonpCallback; // Save the callback name for future use

          oldCallbacks.push(callbackName);
        } // Call if it was a function and we have a response


        if (responseContainer && isFunction(overwritten)) {
          overwritten(responseContainer[0]);
        }

        responseContainer = overwritten = undefined;
      }); // Delegate to script

      return "script";
    }
  }); // Support: Safari 8 only
  // In Safari 8 documents created via document.implementation.createHTMLDocument
  // collapse sibling forms: the second one becomes a child of the first one.
  // Because of that, this security measure has to be disabled in Safari 8.
  // https://bugs.webkit.org/show_bug.cgi?id=137337

  support.createHTMLDocument = function () {
    var body = document.implementation.createHTMLDocument("").body;
    body.innerHTML = "<form></form><form></form>";
    return body.childNodes.length === 2;
  }(); // Argument "data" should be string of html
  // context (optional): If specified, the fragment will be created in this context,
  // defaults to document
  // keepScripts (optional): If true, will include scripts passed in the html string


  djQ.parseHTML = function (data, context, keepScripts) {
    if (typeof data !== "string") {
      return [];
    }

    if (typeof context === "boolean") {
      keepScripts = context;
      context = false;
    }

    var base, parsed, scripts;

    if (!context) {
      // Stop scripts or inline event handlers from being executed immediately
      // by using document.implementation
      if (support.createHTMLDocument) {
        context = document.implementation.createHTMLDocument(""); // Set the base href for the created document
        // so any parsed elements with URLs
        // are based on the document's URL (gh-2965)

        base = context.createElement("base");
        base.href = document.location.href;
        context.head.appendChild(base);
      } else {
        context = document;
      }
    }

    parsed = rsingleTag.exec(data);
    scripts = !keepScripts && []; // Single tag

    if (parsed) {
      return [context.createElement(parsed[1])];
    }

    parsed = buildFragment([data], context, scripts);

    if (scripts && scripts.length) {
      djQ(scripts).remove();
    }

    return djQ.merge([], parsed.childNodes);
  };
  /**
   * Load a url into a page
   */


  djQ.fn.load = function (url, params, callback) {
    var selector,
        type,
        response,
        self = this,
        off = url.indexOf(" ");

    if (off > -1) {
      selector = stripAndCollapse(url.slice(off));
      url = url.slice(0, off);
    } // If it's a function


    if (isFunction(params)) {
      // We assume that it's the callback
      callback = params;
      params = undefined; // Otherwise, build a param string
    } else if (params && _typeof(params) === "object") {
      type = "POST";
    } // If we have elements to modify, make the request


    if (self.length > 0) {
      djQ.ajax({
        url: url,
        // If "type" variable is undefined, then "GET" method will be used.
        // Make value of this field explicit since
        // user can override it through ajaxSetup method
        type: type || "GET",
        dataType: "html",
        data: params
      }).done(function (responseText) {
        // Save response for use in complete callback
        response = arguments;
        self.html(selector ? // If a selector was specified, locate the right elements in a dummy div
        // Exclude scripts to avoid IE 'Permission Denied' errors
        djQ("<div>").append(djQ.parseHTML(responseText)).find(selector) : // Otherwise use the full result
        responseText); // If the request succeeds, this function gets "data", "status", "jqXHR"
        // but they are ignored because response was set above.
        // If it fails, this function gets "jqXHR", "status", "error"
      }).always(callback && function (jqXHR, status) {
        self.each(function () {
          callback.apply(this, response || [jqXHR.responseText, status, jqXHR]);
        });
      });
    }

    return this;
  };

  djQ.expr.pseudos.animated = function (elem) {
    return djQ.grep(djQ.timers, function (fn) {
      return elem === fn.elem;
    }).length;
  };

  djQ.offset = {
    setOffset: function setOffset(elem, options, i) {
      var curPosition,
          curLeft,
          curCSSTop,
          curTop,
          curOffset,
          curCSSLeft,
          calculatePosition,
          position = djQ.css(elem, "position"),
          curElem = djQ(elem),
          props = {}; // Set position first, in-case top/left are set even on static elem

      if (position === "static") {
        elem.style.position = "relative";
      }

      curOffset = curElem.offset();
      curCSSTop = djQ.css(elem, "top");
      curCSSLeft = djQ.css(elem, "left");
      calculatePosition = (position === "absolute" || position === "fixed") && (curCSSTop + curCSSLeft).indexOf("auto") > -1; // Need to be able to calculate position if either
      // top or left is auto and position is either absolute or fixed

      if (calculatePosition) {
        curPosition = curElem.position();
        curTop = curPosition.top;
        curLeft = curPosition.left;
      } else {
        curTop = parseFloat(curCSSTop) || 0;
        curLeft = parseFloat(curCSSLeft) || 0;
      }

      if (isFunction(options)) {
        // Use djQ.extend here to allow modification of coordinates argument (gh-1848)
        options = options.call(elem, i, djQ.extend({}, curOffset));
      }

      if (options.top != null) {
        props.top = options.top - curOffset.top + curTop;
      }

      if (options.left != null) {
        props.left = options.left - curOffset.left + curLeft;
      }

      if ("using" in options) {
        options.using.call(elem, props);
      } else {
        if (typeof props.top === "number") {
          props.top += "px";
        }

        if (typeof props.left === "number") {
          props.left += "px";
        }

        curElem.css(props);
      }
    }
  };
  djQ.fn.extend({
    // offset() relates an element's border box to the document origin
    offset: function offset(options) {
      // Preserve chaining for setter
      if (arguments.length) {
        return options === undefined ? this : this.each(function (i) {
          djQ.offset.setOffset(this, options, i);
        });
      }

      var rect,
          win,
          elem = this[0];

      if (!elem) {
        return;
      } // Return zeros for disconnected and hidden (display: none) elements (gh-2310)
      // Support: IE <=11 only
      // Running getBoundingClientRect on a
      // disconnected node in IE throws an error


      if (!elem.getClientRects().length) {
        return {
          top: 0,
          left: 0
        };
      } // Get document-relative position by adding viewport scroll to viewport-relative gBCR


      rect = elem.getBoundingClientRect();
      win = elem.ownerDocument.defaultView;
      return {
        top: rect.top + win.pageYOffset,
        left: rect.left + win.pageXOffset
      };
    },
    // position() relates an element's margin box to its offset parent's padding box
    // This corresponds to the behavior of CSS absolute positioning
    position: function position() {
      if (!this[0]) {
        return;
      }

      var offsetParent,
          offset,
          doc,
          elem = this[0],
          parentOffset = {
        top: 0,
        left: 0
      }; // position:fixed elements are offset from the viewport, which itself always has zero offset

      if (djQ.css(elem, "position") === "fixed") {
        // Assume position:fixed implies availability of getBoundingClientRect
        offset = elem.getBoundingClientRect();
      } else {
        offset = this.offset(); // Account for the *real* offset parent, which can be the document or its root element
        // when a statically positioned element is identified

        doc = elem.ownerDocument;
        offsetParent = elem.offsetParent || doc.documentElement;

        while (offsetParent && (offsetParent === doc.body || offsetParent === doc.documentElement) && djQ.css(offsetParent, "position") === "static") {
          offsetParent = offsetParent.parentNode;
        }

        if (offsetParent && offsetParent !== elem && offsetParent.nodeType === 1) {
          // Incorporate borders into its offset, since they are outside its content origin
          parentOffset = djQ(offsetParent).offset();
          parentOffset.top += djQ.css(offsetParent, "borderTopWidth", true);
          parentOffset.left += djQ.css(offsetParent, "borderLeftWidth", true);
        }
      } // Subtract parent offsets and element margins


      return {
        top: offset.top - parentOffset.top - djQ.css(elem, "marginTop", true),
        left: offset.left - parentOffset.left - djQ.css(elem, "marginLeft", true)
      };
    },
    // This method will return documentElement in the following cases:
    // 1) For the element inside the iframe without offsetParent, this method will return
    //    documentElement of the parent window
    // 2) For the hidden or detached element
    // 3) For body or html element, i.e. in case of the html node - it will return itself
    //
    // but those exceptions were never presented as a real life use-cases
    // and might be considered as more preferable results.
    //
    // This logic, however, is not guaranteed and can change at any point in the future
    offsetParent: function offsetParent() {
      return this.map(function () {
        var offsetParent = this.offsetParent;

        while (offsetParent && djQ.css(offsetParent, "position") === "static") {
          offsetParent = offsetParent.offsetParent;
        }

        return offsetParent || documentElement;
      });
    }
  }); // Create scrollLeft and scrollTop methods

  djQ.each({
    scrollLeft: "pageXOffset",
    scrollTop: "pageYOffset"
  }, function (method, prop) {
    var top = "pageYOffset" === prop;

    djQ.fn[method] = function (val) {
      return access(this, function (elem, method, val) {
        // Coalesce documents and windows
        var win;

        if (isWindow(elem)) {
          win = elem;
        } else if (elem.nodeType === 9) {
          win = elem.defaultView;
        }

        if (val === undefined) {
          return win ? win[prop] : elem[method];
        }

        if (win) {
          win.scrollTo(!top ? val : win.pageXOffset, top ? val : win.pageYOffset);
        } else {
          elem[method] = val;
        }
      }, method, val, arguments.length);
    };
  }); // Support: Safari <=7 - 9.1, Chrome <=37 - 49
  // Add the top/left cssHooks using djQ.fn.position
  // Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
  // Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
  // getComputedStyle returns percent when specified for top/left/bottom/right;
  // rather than make the css module depend on the offset module, just check for it here

  djQ.each(["top", "left"], function (_i, prop) {
    djQ.cssHooks[prop] = addGetHookIf(support.pixelPosition, function (elem, computed) {
      if (computed) {
        computed = curCSS(elem, prop); // If curCSS returns percentage, fallback to offset

        return rnumnonpx.test(computed) ? djQ(elem).position()[prop] + "px" : computed;
      }
    });
  }); // Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods

  djQ.each({
    Height: "height",
    Width: "width"
  }, function (name, type) {
    djQ.each({
      padding: "inner" + name,
      content: type,
      "": "outer" + name
    }, function (defaultExtra, funcName) {
      // Margin is only for outerHeight, outerWidth
      djQ.fn[funcName] = function (margin, value) {
        var chainable = arguments.length && (defaultExtra || typeof margin !== "boolean"),
            extra = defaultExtra || (margin === true || value === true ? "margin" : "border");
        return access(this, function (elem, type, value) {
          var doc;

          if (isWindow(elem)) {
            // $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
            return funcName.indexOf("outer") === 0 ? elem["inner" + name] : elem.document.documentElement["client" + name];
          } // Get document width or height


          if (elem.nodeType === 9) {
            doc = elem.documentElement; // Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
            // whichever is greatest

            return Math.max(elem.body["scroll" + name], doc["scroll" + name], elem.body["offset" + name], doc["offset" + name], doc["client" + name]);
          }

          return value === undefined ? // Get width or height on the element, requesting but not forcing parseFloat
          djQ.css(elem, type, extra) : // Set width or height on the element
          djQ.style(elem, type, value, extra);
        }, type, chainable ? margin : undefined, chainable);
      };
    });
  });
  djQ.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (_i, type) {
    djQ.fn[type] = function (fn) {
      return this.on(type, fn);
    };
  });
  djQ.fn.extend({
    bind: function bind(types, data, fn) {
      return this.on(types, null, data, fn);
    },
    unbind: function unbind(types, fn) {
      return this.off(types, null, fn);
    },
    delegate: function delegate(selector, types, data, fn) {
      return this.on(types, selector, data, fn);
    },
    undelegate: function undelegate(selector, types, fn) {
      // ( namespace ) or ( selector, types [, fn] )
      return arguments.length === 1 ? this.off(selector, "**") : this.off(types, selector || "**", fn);
    },
    hover: function hover(fnOver, fnOut) {
      return this.mouseenter(fnOver).mouseleave(fnOut || fnOver);
    }
  });
  djQ.each(("blur focus focusin focusout resize scroll click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select submit keydown keypress keyup contextmenu").split(" "), function (_i, name) {
    // Handle event binding
    djQ.fn[name] = function (data, fn) {
      return arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name);
    };
  }); // Support: Android <=4.0 only
  // Make sure we trim BOM and NBSP

  var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g; // Bind a function to a context, optionally partially applying any
  // arguments.
  // djQ.proxy is deprecated to promote standards (specifically Function#bind)
  // However, it is not slated for removal any time soon

  djQ.proxy = function (fn, context) {
    var tmp, args, proxy;

    if (typeof context === "string") {
      tmp = fn[context];
      context = fn;
      fn = tmp;
    } // Quick check to determine if target is callable, in the spec
    // this throws a TypeError, but we will just return undefined.


    if (!isFunction(fn)) {
      return undefined;
    } // Simulated bind


    args = _slice.call(arguments, 2);

    proxy = function proxy() {
      return fn.apply(context || this, args.concat(_slice.call(arguments)));
    }; // Set the guid of unique handler to the same of original handler, so it can be removed


    proxy.guid = fn.guid = fn.guid || djQ.guid++;
    return proxy;
  };

  djQ.holdReady = function (hold) {
    if (hold) {
      djQ.readyWait++;
    } else {
      djQ.ready(true);
    }
  };

  djQ.isArray = Array.isArray;
  djQ.parseJSON = JSON.parse;
  djQ.nodeName = nodeName;
  djQ.isFunction = isFunction;
  djQ.isWindow = isWindow;
  djQ.camelCase = camelCase;
  djQ.type = toType;
  djQ.now = Date.now;

  djQ.isNumeric = function (obj) {
    // As of djQ 3.0, isNumeric is limited to
    // strings and numbers (primitives or objects)
    // that can be coerced to finite numbers (gh-2662)
    var type = djQ.type(obj);
    return (type === "number" || type === "string") && // parseFloat NaNs numeric-cast false positives ("")
    // ...but misinterprets leading-number strings, particularly hex literals ("0x...")
    // subtraction forces infinities to NaN
    !isNaN(obj - parseFloat(obj));
  };

  djQ.trim = function (text) {
    return text == null ? "" : (text + "").replace(rtrim, "");
  }; // Register as a named AMD module, since djQ can be concatenated with other
  // files that may use define, but not via a proper concatenation script that
  // understands anonymous AMD modules. A named AMD is safest and most robust
  // way to register. Lowercase jquery is used because AMD module names are
  // derived from file names, and djQ is normally delivered in a lowercase
  // file name. Do this after creating the global so that if an AMD module wants
  // to call noConflict to hide this version of djQ, it will work.
  // Note that for maximum portability, libraries that are not djQ should
  // declare themselves as anonymous modules, and avoid setting a global if an
  // AMD loader is present. djQ is a special case. For more information, see
  // https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon


  if (typeof define === "function" && define.amd) {
    define("jquery", [], function () {
      return djQ;
    });
  }

  var // Map over djQ in case of overwrite
  _djQ = window.djQ,
      // Map over the $ in case of overwrite
  _$ = window.$;

  djQ.noConflict = function (deep) {
    if (window.$ === djQ) {
      window.$ = _$;
    }

    if (deep && window.djQ === djQ) {
      window.djQ = _djQ;
    }

    return djQ;
  }; // Expose djQ and $ identifiers, even in AMD
  // (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
  // and CommonJS for browser emulators (#13566)


  if (typeof noGlobal === "undefined") {
    window.djQ = window.$ = djQ;
  }

  return djQ;
});
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function () {
  function r(e, n, t) {
    function o(i, f) {
      if (!n[i]) {
        if (!e[i]) {
          var c = "function" == typeof require && require;
          if (!f && c) return c(i, !0);
          if (u) return u(i, !0);
          var a = new Error("Cannot find module '" + i + "'");
          throw a.code = "MODULE_NOT_FOUND", a;
        }

        var p = n[i] = {
          exports: {}
        };
        e[i][0].call(p.exports, function (r) {
          var n = e[i][1][r];
          return o(n || r);
        }, p, p.exports, r, e, n, t);
      }

      return n[i].exports;
    }

    for (var u = "function" == typeof require && require, i = 0; i < t.length; i++) {
      o(t[i]);
    }

    return o;
  }

  return r;
})()({
  1: [function (_dereq_, module, exports) {
    _dereq_(276);

    _dereq_(212);

    _dereq_(214);

    _dereq_(213);

    _dereq_(216);

    _dereq_(218);

    _dereq_(223);

    _dereq_(217);

    _dereq_(215);

    _dereq_(225);

    _dereq_(224);

    _dereq_(220);

    _dereq_(221);

    _dereq_(219);

    _dereq_(211);

    _dereq_(222);

    _dereq_(226);

    _dereq_(227);

    _dereq_(178);

    _dereq_(180);

    _dereq_(179);

    _dereq_(229);

    _dereq_(228);

    _dereq_(199);

    _dereq_(209);

    _dereq_(210);

    _dereq_(200);

    _dereq_(201);

    _dereq_(202);

    _dereq_(203);

    _dereq_(204);

    _dereq_(205);

    _dereq_(206);

    _dereq_(207);

    _dereq_(208);

    _dereq_(182);

    _dereq_(183);

    _dereq_(184);

    _dereq_(185);

    _dereq_(186);

    _dereq_(187);

    _dereq_(188);

    _dereq_(189);

    _dereq_(190);

    _dereq_(191);

    _dereq_(192);

    _dereq_(193);

    _dereq_(194);

    _dereq_(195);

    _dereq_(196);

    _dereq_(197);

    _dereq_(198);

    _dereq_(263);

    _dereq_(268);

    _dereq_(275);

    _dereq_(266);

    _dereq_(258);

    _dereq_(259);

    _dereq_(264);

    _dereq_(269);

    _dereq_(271);

    _dereq_(254);

    _dereq_(255);

    _dereq_(256);

    _dereq_(257);

    _dereq_(260);

    _dereq_(261);

    _dereq_(262);

    _dereq_(265);

    _dereq_(267);

    _dereq_(270);

    _dereq_(272);

    _dereq_(273);

    _dereq_(274);

    _dereq_(173);

    _dereq_(175);

    _dereq_(174);

    _dereq_(177);

    _dereq_(176);

    _dereq_(161);

    _dereq_(159);

    _dereq_(166);

    _dereq_(163);

    _dereq_(169);

    _dereq_(171);

    _dereq_(158);

    _dereq_(165);

    _dereq_(155);

    _dereq_(170);

    _dereq_(153);

    _dereq_(168);

    _dereq_(167);

    _dereq_(160);

    _dereq_(164);

    _dereq_(152);

    _dereq_(154);

    _dereq_(157);

    _dereq_(156);

    _dereq_(172);

    _dereq_(162);

    _dereq_(245);

    _dereq_(246);

    _dereq_(252);

    _dereq_(247);

    _dereq_(248);

    _dereq_(249);

    _dereq_(250);

    _dereq_(251);

    _dereq_(230);

    _dereq_(181);

    _dereq_(253);

    _dereq_(288);

    _dereq_(289);

    _dereq_(277);

    _dereq_(278);

    _dereq_(283);

    _dereq_(286);

    _dereq_(287);

    _dereq_(281);

    _dereq_(284);

    _dereq_(282);

    _dereq_(285);

    _dereq_(279);

    _dereq_(280);

    _dereq_(231);

    _dereq_(232);

    _dereq_(233);

    _dereq_(234);

    _dereq_(235);

    _dereq_(238);

    _dereq_(236);

    _dereq_(237);

    _dereq_(239);

    _dereq_(240);

    _dereq_(241);

    _dereq_(242);

    _dereq_(244);

    _dereq_(243);

    module.exports = _dereq_(50);
  }, {
    "152": 152,
    "153": 153,
    "154": 154,
    "155": 155,
    "156": 156,
    "157": 157,
    "158": 158,
    "159": 159,
    "160": 160,
    "161": 161,
    "162": 162,
    "163": 163,
    "164": 164,
    "165": 165,
    "166": 166,
    "167": 167,
    "168": 168,
    "169": 169,
    "170": 170,
    "171": 171,
    "172": 172,
    "173": 173,
    "174": 174,
    "175": 175,
    "176": 176,
    "177": 177,
    "178": 178,
    "179": 179,
    "180": 180,
    "181": 181,
    "182": 182,
    "183": 183,
    "184": 184,
    "185": 185,
    "186": 186,
    "187": 187,
    "188": 188,
    "189": 189,
    "190": 190,
    "191": 191,
    "192": 192,
    "193": 193,
    "194": 194,
    "195": 195,
    "196": 196,
    "197": 197,
    "198": 198,
    "199": 199,
    "200": 200,
    "201": 201,
    "202": 202,
    "203": 203,
    "204": 204,
    "205": 205,
    "206": 206,
    "207": 207,
    "208": 208,
    "209": 209,
    "210": 210,
    "211": 211,
    "212": 212,
    "213": 213,
    "214": 214,
    "215": 215,
    "216": 216,
    "217": 217,
    "218": 218,
    "219": 219,
    "220": 220,
    "221": 221,
    "222": 222,
    "223": 223,
    "224": 224,
    "225": 225,
    "226": 226,
    "227": 227,
    "228": 228,
    "229": 229,
    "230": 230,
    "231": 231,
    "232": 232,
    "233": 233,
    "234": 234,
    "235": 235,
    "236": 236,
    "237": 237,
    "238": 238,
    "239": 239,
    "240": 240,
    "241": 241,
    "242": 242,
    "243": 243,
    "244": 244,
    "245": 245,
    "246": 246,
    "247": 247,
    "248": 248,
    "249": 249,
    "250": 250,
    "251": 251,
    "252": 252,
    "253": 253,
    "254": 254,
    "255": 255,
    "256": 256,
    "257": 257,
    "258": 258,
    "259": 259,
    "260": 260,
    "261": 261,
    "262": 262,
    "263": 263,
    "264": 264,
    "265": 265,
    "266": 266,
    "267": 267,
    "268": 268,
    "269": 269,
    "270": 270,
    "271": 271,
    "272": 272,
    "273": 273,
    "274": 274,
    "275": 275,
    "276": 276,
    "277": 277,
    "278": 278,
    "279": 279,
    "280": 280,
    "281": 281,
    "282": 282,
    "283": 283,
    "284": 284,
    "285": 285,
    "286": 286,
    "287": 287,
    "288": 288,
    "289": 289,
    "50": 50
  }],
  2: [function (_dereq_, module, exports) {
    _dereq_(290);

    module.exports = _dereq_(50).Array.flatMap;
  }, {
    "290": 290,
    "50": 50
  }],
  3: [function (_dereq_, module, exports) {
    _dereq_(291);

    module.exports = _dereq_(50).Array.includes;
  }, {
    "291": 291,
    "50": 50
  }],
  4: [function (_dereq_, module, exports) {
    _dereq_(292);

    module.exports = _dereq_(50).Object.entries;
  }, {
    "292": 292,
    "50": 50
  }],
  5: [function (_dereq_, module, exports) {
    _dereq_(293);

    module.exports = _dereq_(50).Object.getOwnPropertyDescriptors;
  }, {
    "293": 293,
    "50": 50
  }],
  6: [function (_dereq_, module, exports) {
    _dereq_(294);

    module.exports = _dereq_(50).Object.values;
  }, {
    "294": 294,
    "50": 50
  }],
  7: [function (_dereq_, module, exports) {
    'use strict';

    _dereq_(230);

    _dereq_(295);

    module.exports = _dereq_(50).Promise['finally'];
  }, {
    "230": 230,
    "295": 295,
    "50": 50
  }],
  8: [function (_dereq_, module, exports) {
    _dereq_(296);

    module.exports = _dereq_(50).String.padEnd;
  }, {
    "296": 296,
    "50": 50
  }],
  9: [function (_dereq_, module, exports) {
    _dereq_(297);

    module.exports = _dereq_(50).String.padStart;
  }, {
    "297": 297,
    "50": 50
  }],
  10: [function (_dereq_, module, exports) {
    _dereq_(299);

    module.exports = _dereq_(50).String.trimRight;
  }, {
    "299": 299,
    "50": 50
  }],
  11: [function (_dereq_, module, exports) {
    _dereq_(298);

    module.exports = _dereq_(50).String.trimLeft;
  }, {
    "298": 298,
    "50": 50
  }],
  12: [function (_dereq_, module, exports) {
    _dereq_(300);

    module.exports = _dereq_(149).f('asyncIterator');
  }, {
    "149": 149,
    "300": 300
  }],
  13: [function (_dereq_, module, exports) {
    _dereq_(30);

    module.exports = _dereq_(16).global;
  }, {
    "16": 16,
    "30": 30
  }],
  14: [function (_dereq_, module, exports) {
    module.exports = function (it) {
      if (typeof it != 'function') throw TypeError(it + ' is not a function!');
      return it;
    };
  }, {}],
  15: [function (_dereq_, module, exports) {
    var isObject = _dereq_(26);

    module.exports = function (it) {
      if (!isObject(it)) throw TypeError(it + ' is not an object!');
      return it;
    };
  }, {
    "26": 26
  }],
  16: [function (_dereq_, module, exports) {
    var core = module.exports = {
      version: '2.6.11'
    };
    if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef
  }, {}],
  17: [function (_dereq_, module, exports) {
    // optional / simple context binding
    var aFunction = _dereq_(14);

    module.exports = function (fn, that, length) {
      aFunction(fn);
      if (that === undefined) return fn;

      switch (length) {
        case 1:
          return function (a) {
            return fn.call(that, a);
          };

        case 2:
          return function (a, b) {
            return fn.call(that, a, b);
          };

        case 3:
          return function (a, b, c) {
            return fn.call(that, a, b, c);
          };
      }

      return function ()
      /* ...args */
      {
        return fn.apply(that, arguments);
      };
    };
  }, {
    "14": 14
  }],
  18: [function (_dereq_, module, exports) {
    // Thank's IE8 for his funny defineProperty
    module.exports = !_dereq_(21)(function () {
      return Object.defineProperty({}, 'a', {
        get: function get() {
          return 7;
        }
      }).a != 7;
    });
  }, {
    "21": 21
  }],
  19: [function (_dereq_, module, exports) {
    var isObject = _dereq_(26);

    var document = _dereq_(22).document; // typeof document.createElement is 'object' in old IE


    var is = isObject(document) && isObject(document.createElement);

    module.exports = function (it) {
      return is ? document.createElement(it) : {};
    };
  }, {
    "22": 22,
    "26": 26
  }],
  20: [function (_dereq_, module, exports) {
    var global = _dereq_(22);

    var core = _dereq_(16);

    var ctx = _dereq_(17);

    var hide = _dereq_(24);

    var has = _dereq_(23);

    var PROTOTYPE = 'prototype';

    var $export = function $export(type, name, source) {
      var IS_FORCED = type & $export.F;
      var IS_GLOBAL = type & $export.G;
      var IS_STATIC = type & $export.S;
      var IS_PROTO = type & $export.P;
      var IS_BIND = type & $export.B;
      var IS_WRAP = type & $export.W;
      var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
      var expProto = exports[PROTOTYPE];
      var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
      var key, own, out;
      if (IS_GLOBAL) source = name;

      for (key in source) {
        // contains in native
        own = !IS_FORCED && target && target[key] !== undefined;
        if (own && has(exports, key)) continue; // export native or passed

        out = own ? target[key] : source[key]; // prevent global pollution for namespaces

        exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key] // bind timers to global for call from export context
        : IS_BIND && own ? ctx(out, global) // wrap global constructors for prevent change them in library
        : IS_WRAP && target[key] == out ? function (C) {
          var F = function F(a, b, c) {
            if (this instanceof C) {
              switch (arguments.length) {
                case 0:
                  return new C();

                case 1:
                  return new C(a);

                case 2:
                  return new C(a, b);
              }

              return new C(a, b, c);
            }

            return C.apply(this, arguments);
          };

          F[PROTOTYPE] = C[PROTOTYPE];
          return F; // make static versions for prototype methods
        }(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out; // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%

        if (IS_PROTO) {
          (exports.virtual || (exports.virtual = {}))[key] = out; // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%

          if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
        }
      }
    }; // type bitmap


    $export.F = 1; // forced

    $export.G = 2; // global

    $export.S = 4; // static

    $export.P = 8; // proto

    $export.B = 16; // bind

    $export.W = 32; // wrap

    $export.U = 64; // safe

    $export.R = 128; // real proto method for `library`

    module.exports = $export;
  }, {
    "16": 16,
    "17": 17,
    "22": 22,
    "23": 23,
    "24": 24
  }],
  21: [function (_dereq_, module, exports) {
    module.exports = function (exec) {
      try {
        return !!exec();
      } catch (e) {
        return true;
      }
    };
  }, {}],
  22: [function (_dereq_, module, exports) {
    // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
    var global = module.exports = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self // eslint-disable-next-line no-new-func
    : Function('return this')();
    if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef
  }, {}],
  23: [function (_dereq_, module, exports) {
    var hasOwnProperty = {}.hasOwnProperty;

    module.exports = function (it, key) {
      return hasOwnProperty.call(it, key);
    };
  }, {}],
  24: [function (_dereq_, module, exports) {
    var dP = _dereq_(27);

    var createDesc = _dereq_(28);

    module.exports = _dereq_(18) ? function (object, key, value) {
      return dP.f(object, key, createDesc(1, value));
    } : function (object, key, value) {
      object[key] = value;
      return object;
    };
  }, {
    "18": 18,
    "27": 27,
    "28": 28
  }],
  25: [function (_dereq_, module, exports) {
    module.exports = !_dereq_(18) && !_dereq_(21)(function () {
      return Object.defineProperty(_dereq_(19)('div'), 'a', {
        get: function get() {
          return 7;
        }
      }).a != 7;
    });
  }, {
    "18": 18,
    "19": 19,
    "21": 21
  }],
  26: [function (_dereq_, module, exports) {
    module.exports = function (it) {
      return _typeof(it) === 'object' ? it !== null : typeof it === 'function';
    };
  }, {}],
  27: [function (_dereq_, module, exports) {
    var anObject = _dereq_(15);

    var IE8_DOM_DEFINE = _dereq_(25);

    var toPrimitive = _dereq_(29);

    var dP = Object.defineProperty;
    exports.f = _dereq_(18) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
      anObject(O);
      P = toPrimitive(P, true);
      anObject(Attributes);
      if (IE8_DOM_DEFINE) try {
        return dP(O, P, Attributes);
      } catch (e) {
        /* empty */
      }
      if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
      if ('value' in Attributes) O[P] = Attributes.value;
      return O;
    };
  }, {
    "15": 15,
    "18": 18,
    "25": 25,
    "29": 29
  }],
  28: [function (_dereq_, module, exports) {
    module.exports = function (bitmap, value) {
      return {
        enumerable: !(bitmap & 1),
        configurable: !(bitmap & 2),
        writable: !(bitmap & 4),
        value: value
      };
    };
  }, {}],
  29: [function (_dereq_, module, exports) {
    // 7.1.1 ToPrimitive(input [, PreferredType])
    var isObject = _dereq_(26); // instead of the ES6 spec version, we didn't implement @@toPrimitive case
    // and the second argument - flag - preferred type is a string


    module.exports = function (it, S) {
      if (!isObject(it)) return it;
      var fn, val;
      if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
      if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
      if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
      throw TypeError("Can't convert object to primitive value");
    };
  }, {
    "26": 26
  }],
  30: [function (_dereq_, module, exports) {
    // https://github.com/tc39/proposal-global
    var $export = _dereq_(20);

    $export($export.G, {
      global: _dereq_(22)
    });
  }, {
    "20": 20,
    "22": 22
  }],
  31: [function (_dereq_, module, exports) {
    arguments[4][14][0].apply(exports, arguments);
  }, {
    "14": 14
  }],
  32: [function (_dereq_, module, exports) {
    var cof = _dereq_(46);

    module.exports = function (it, msg) {
      if (typeof it != 'number' && cof(it) != 'Number') throw TypeError(msg);
      return +it;
    };
  }, {
    "46": 46
  }],
  33: [function (_dereq_, module, exports) {
    // 22.1.3.31 Array.prototype[@@unscopables]
    var UNSCOPABLES = _dereq_(150)('unscopables');

    var ArrayProto = Array.prototype;
    if (ArrayProto[UNSCOPABLES] == undefined) _dereq_(70)(ArrayProto, UNSCOPABLES, {});

    module.exports = function (key) {
      ArrayProto[UNSCOPABLES][key] = true;
    };
  }, {
    "150": 150,
    "70": 70
  }],
  34: [function (_dereq_, module, exports) {
    'use strict';

    var at = _dereq_(127)(true); // `AdvanceStringIndex` abstract operation
    // https://tc39.github.io/ecma262/#sec-advancestringindex


    module.exports = function (S, index, unicode) {
      return index + (unicode ? at(S, index).length : 1);
    };
  }, {
    "127": 127
  }],
  35: [function (_dereq_, module, exports) {
    module.exports = function (it, Constructor, name, forbiddenField) {
      if (!(it instanceof Constructor) || forbiddenField !== undefined && forbiddenField in it) {
        throw TypeError(name + ': incorrect invocation!');
      }

      return it;
    };
  }, {}],
  36: [function (_dereq_, module, exports) {
    arguments[4][15][0].apply(exports, arguments);
  }, {
    "15": 15,
    "79": 79
  }],
  37: [function (_dereq_, module, exports) {
    // 22.1.3.3 Array.prototype.copyWithin(target, start, end = this.length)
    'use strict';

    var toObject = _dereq_(140);

    var toAbsoluteIndex = _dereq_(135);

    var toLength = _dereq_(139);

    module.exports = [].copyWithin || function copyWithin(target
    /* = 0 */
    , start
    /* = 0, end = @length */
    ) {
      var O = toObject(this);
      var len = toLength(O.length);
      var to = toAbsoluteIndex(target, len);
      var from = toAbsoluteIndex(start, len);
      var end = arguments.length > 2 ? arguments[2] : undefined;
      var count = Math.min((end === undefined ? len : toAbsoluteIndex(end, len)) - from, len - to);
      var inc = 1;

      if (from < to && to < from + count) {
        inc = -1;
        from += count - 1;
        to += count - 1;
      }

      while (count-- > 0) {
        if (from in O) O[to] = O[from];else delete O[to];
        to += inc;
        from += inc;
      }

      return O;
    };
  }, {
    "135": 135,
    "139": 139,
    "140": 140
  }],
  38: [function (_dereq_, module, exports) {
    // 22.1.3.6 Array.prototype.fill(value, start = 0, end = this.length)
    'use strict';

    var toObject = _dereq_(140);

    var toAbsoluteIndex = _dereq_(135);

    var toLength = _dereq_(139);

    module.exports = function fill(value
    /* , start = 0, end = @length */
    ) {
      var O = toObject(this);
      var length = toLength(O.length);
      var aLen = arguments.length;
      var index = toAbsoluteIndex(aLen > 1 ? arguments[1] : undefined, length);
      var end = aLen > 2 ? arguments[2] : undefined;
      var endPos = end === undefined ? length : toAbsoluteIndex(end, length);

      while (endPos > index) {
        O[index++] = value;
      }

      return O;
    };
  }, {
    "135": 135,
    "139": 139,
    "140": 140
  }],
  39: [function (_dereq_, module, exports) {
    // false -> Array#indexOf
    // true  -> Array#includes
    var toIObject = _dereq_(138);

    var toLength = _dereq_(139);

    var toAbsoluteIndex = _dereq_(135);

    module.exports = function (IS_INCLUDES) {
      return function ($this, el, fromIndex) {
        var O = toIObject($this);
        var length = toLength(O.length);
        var index = toAbsoluteIndex(fromIndex, length);
        var value; // Array#includes uses SameValueZero equality algorithm
        // eslint-disable-next-line no-self-compare

        if (IS_INCLUDES && el != el) while (length > index) {
          value = O[index++]; // eslint-disable-next-line no-self-compare

          if (value != value) return true; // Array#indexOf ignores holes, Array#includes - not
        } else for (; length > index; index++) {
          if (IS_INCLUDES || index in O) {
            if (O[index] === el) return IS_INCLUDES || index || 0;
          }
        }
        return !IS_INCLUDES && -1;
      };
    };
  }, {
    "135": 135,
    "138": 138,
    "139": 139
  }],
  40: [function (_dereq_, module, exports) {
    // 0 -> Array#forEach
    // 1 -> Array#map
    // 2 -> Array#filter
    // 3 -> Array#some
    // 4 -> Array#every
    // 5 -> Array#find
    // 6 -> Array#findIndex
    var ctx = _dereq_(52);

    var IObject = _dereq_(75);

    var toObject = _dereq_(140);

    var toLength = _dereq_(139);

    var asc = _dereq_(43);

    module.exports = function (TYPE, $create) {
      var IS_MAP = TYPE == 1;
      var IS_FILTER = TYPE == 2;
      var IS_SOME = TYPE == 3;
      var IS_EVERY = TYPE == 4;
      var IS_FIND_INDEX = TYPE == 6;
      var NO_HOLES = TYPE == 5 || IS_FIND_INDEX;
      var create = $create || asc;
      return function ($this, callbackfn, that) {
        var O = toObject($this);
        var self = IObject(O);
        var f = ctx(callbackfn, that, 3);
        var length = toLength(self.length);
        var index = 0;
        var result = IS_MAP ? create($this, length) : IS_FILTER ? create($this, 0) : undefined;
        var val, res;

        for (; length > index; index++) {
          if (NO_HOLES || index in self) {
            val = self[index];
            res = f(val, index, O);

            if (TYPE) {
              if (IS_MAP) result[index] = res; // map
              else if (res) switch (TYPE) {
                  case 3:
                    return true;
                  // some

                  case 5:
                    return val;
                  // find

                  case 6:
                    return index;
                  // findIndex

                  case 2:
                    result.push(val);
                  // filter
                } else if (IS_EVERY) return false; // every
            }
          }
        }

        return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : result;
      };
    };
  }, {
    "139": 139,
    "140": 140,
    "43": 43,
    "52": 52,
    "75": 75
  }],
  41: [function (_dereq_, module, exports) {
    var aFunction = _dereq_(31);

    var toObject = _dereq_(140);

    var IObject = _dereq_(75);

    var toLength = _dereq_(139);

    module.exports = function (that, callbackfn, aLen, memo, isRight) {
      aFunction(callbackfn);
      var O = toObject(that);
      var self = IObject(O);
      var length = toLength(O.length);
      var index = isRight ? length - 1 : 0;
      var i = isRight ? -1 : 1;
      if (aLen < 2) for (;;) {
        if (index in self) {
          memo = self[index];
          index += i;
          break;
        }

        index += i;

        if (isRight ? index < 0 : length <= index) {
          throw TypeError('Reduce of empty array with no initial value');
        }
      }

      for (; isRight ? index >= 0 : length > index; index += i) {
        if (index in self) {
          memo = callbackfn(memo, self[index], index, O);
        }
      }

      return memo;
    };
  }, {
    "139": 139,
    "140": 140,
    "31": 31,
    "75": 75
  }],
  42: [function (_dereq_, module, exports) {
    var isObject = _dereq_(79);

    var isArray = _dereq_(77);

    var SPECIES = _dereq_(150)('species');

    module.exports = function (original) {
      var C;

      if (isArray(original)) {
        C = original.constructor; // cross-realm fallback

        if (typeof C == 'function' && (C === Array || isArray(C.prototype))) C = undefined;

        if (isObject(C)) {
          C = C[SPECIES];
          if (C === null) C = undefined;
        }
      }

      return C === undefined ? Array : C;
    };
  }, {
    "150": 150,
    "77": 77,
    "79": 79
  }],
  43: [function (_dereq_, module, exports) {
    // 9.4.2.3 ArraySpeciesCreate(originalArray, length)
    var speciesConstructor = _dereq_(42);

    module.exports = function (original, length) {
      return new (speciesConstructor(original))(length);
    };
  }, {
    "42": 42
  }],
  44: [function (_dereq_, module, exports) {
    'use strict';

    var aFunction = _dereq_(31);

    var isObject = _dereq_(79);

    var invoke = _dereq_(74);

    var arraySlice = [].slice;
    var factories = {};

    var construct = function construct(F, len, args) {
      if (!(len in factories)) {
        for (var n = [], i = 0; i < len; i++) {
          n[i] = 'a[' + i + ']';
        } // eslint-disable-next-line no-new-func


        factories[len] = Function('F,a', 'return new F(' + n.join(',') + ')');
      }

      return factories[len](F, args);
    };

    module.exports = Function.bind || function bind(that
    /* , ...args */
    ) {
      var fn = aFunction(this);
      var partArgs = arraySlice.call(arguments, 1);

      var bound = function bound()
      /* args... */
      {
        var args = partArgs.concat(arraySlice.call(arguments));
        return this instanceof bound ? construct(fn, args.length, args) : invoke(fn, args, that);
      };

      if (isObject(fn.prototype)) bound.prototype = fn.prototype;
      return bound;
    };
  }, {
    "31": 31,
    "74": 74,
    "79": 79
  }],
  45: [function (_dereq_, module, exports) {
    // getting tag from 19.1.3.6 Object.prototype.toString()
    var cof = _dereq_(46);

    var TAG = _dereq_(150)('toStringTag'); // ES3 wrong here


    var ARG = cof(function () {
      return arguments;
    }()) == 'Arguments'; // fallback for IE11 Script Access Denied error

    var tryGet = function tryGet(it, key) {
      try {
        return it[key];
      } catch (e) {
        /* empty */
      }
    };

    module.exports = function (it) {
      var O, T, B;
      return it === undefined ? 'Undefined' : it === null ? 'Null' // @@toStringTag case
      : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T // builtinTag case
      : ARG ? cof(O) // ES3 arguments fallback
      : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
    };
  }, {
    "150": 150,
    "46": 46
  }],
  46: [function (_dereq_, module, exports) {
    var toString = {}.toString;

    module.exports = function (it) {
      return toString.call(it).slice(8, -1);
    };
  }, {}],
  47: [function (_dereq_, module, exports) {
    'use strict';

    var dP = _dereq_(97).f;

    var create = _dereq_(96);

    var redefineAll = _dereq_(115);

    var ctx = _dereq_(52);

    var anInstance = _dereq_(35);

    var forOf = _dereq_(66);

    var $iterDefine = _dereq_(83);

    var step = _dereq_(85);

    var setSpecies = _dereq_(121);

    var DESCRIPTORS = _dereq_(56);

    var fastKey = _dereq_(92).fastKey;

    var validate = _dereq_(147);

    var SIZE = DESCRIPTORS ? '_s' : 'size';

    var getEntry = function getEntry(that, key) {
      // fast case
      var index = fastKey(key);
      var entry;
      if (index !== 'F') return that._i[index]; // frozen object case

      for (entry = that._f; entry; entry = entry.n) {
        if (entry.k == key) return entry;
      }
    };

    module.exports = {
      getConstructor: function getConstructor(wrapper, NAME, IS_MAP, ADDER) {
        var C = wrapper(function (that, iterable) {
          anInstance(that, C, NAME, '_i');
          that._t = NAME; // collection type

          that._i = create(null); // index

          that._f = undefined; // first entry

          that._l = undefined; // last entry

          that[SIZE] = 0; // size

          if (iterable != undefined) forOf(iterable, IS_MAP, that[ADDER], that);
        });
        redefineAll(C.prototype, {
          // 23.1.3.1 Map.prototype.clear()
          // 23.2.3.2 Set.prototype.clear()
          clear: function clear() {
            for (var that = validate(this, NAME), data = that._i, entry = that._f; entry; entry = entry.n) {
              entry.r = true;
              if (entry.p) entry.p = entry.p.n = undefined;
              delete data[entry.i];
            }

            that._f = that._l = undefined;
            that[SIZE] = 0;
          },
          // 23.1.3.3 Map.prototype.delete(key)
          // 23.2.3.4 Set.prototype.delete(value)
          'delete': function _delete(key) {
            var that = validate(this, NAME);
            var entry = getEntry(that, key);

            if (entry) {
              var next = entry.n;
              var prev = entry.p;
              delete that._i[entry.i];
              entry.r = true;
              if (prev) prev.n = next;
              if (next) next.p = prev;
              if (that._f == entry) that._f = next;
              if (that._l == entry) that._l = prev;
              that[SIZE]--;
            }

            return !!entry;
          },
          // 23.2.3.6 Set.prototype.forEach(callbackfn, thisArg = undefined)
          // 23.1.3.5 Map.prototype.forEach(callbackfn, thisArg = undefined)
          forEach: function forEach(callbackfn
          /* , that = undefined */
          ) {
            validate(this, NAME);
            var f = ctx(callbackfn, arguments.length > 1 ? arguments[1] : undefined, 3);
            var entry;

            while (entry = entry ? entry.n : this._f) {
              f(entry.v, entry.k, this); // revert to the last existing entry

              while (entry && entry.r) {
                entry = entry.p;
              }
            }
          },
          // 23.1.3.7 Map.prototype.has(key)
          // 23.2.3.7 Set.prototype.has(value)
          has: function has(key) {
            return !!getEntry(validate(this, NAME), key);
          }
        });
        if (DESCRIPTORS) dP(C.prototype, 'size', {
          get: function get() {
            return validate(this, NAME)[SIZE];
          }
        });
        return C;
      },
      def: function def(that, key, value) {
        var entry = getEntry(that, key);
        var prev, index; // change existing entry

        if (entry) {
          entry.v = value; // create new entry
        } else {
          that._l = entry = {
            i: index = fastKey(key, true),
            // <- index
            k: key,
            // <- key
            v: value,
            // <- value
            p: prev = that._l,
            // <- previous entry
            n: undefined,
            // <- next entry
            r: false // <- removed

          };
          if (!that._f) that._f = entry;
          if (prev) prev.n = entry;
          that[SIZE]++; // add to index

          if (index !== 'F') that._i[index] = entry;
        }

        return that;
      },
      getEntry: getEntry,
      setStrong: function setStrong(C, NAME, IS_MAP) {
        // add .keys, .values, .entries, [@@iterator]
        // 23.1.3.4, 23.1.3.8, 23.1.3.11, 23.1.3.12, 23.2.3.5, 23.2.3.8, 23.2.3.10, 23.2.3.11
        $iterDefine(C, NAME, function (iterated, kind) {
          this._t = validate(iterated, NAME); // target

          this._k = kind; // kind

          this._l = undefined; // previous
        }, function () {
          var that = this;
          var kind = that._k;
          var entry = that._l; // revert to the last existing entry

          while (entry && entry.r) {
            entry = entry.p;
          } // get next entry


          if (!that._t || !(that._l = entry = entry ? entry.n : that._t._f)) {
            // or finish the iteration
            that._t = undefined;
            return step(1);
          } // return step by kind


          if (kind == 'keys') return step(0, entry.k);
          if (kind == 'values') return step(0, entry.v);
          return step(0, [entry.k, entry.v]);
        }, IS_MAP ? 'entries' : 'values', !IS_MAP, true); // add [@@species], 23.1.2.2, 23.2.2.2

        setSpecies(NAME);
      }
    };
  }, {
    "115": 115,
    "121": 121,
    "147": 147,
    "35": 35,
    "52": 52,
    "56": 56,
    "66": 66,
    "83": 83,
    "85": 85,
    "92": 92,
    "96": 96,
    "97": 97
  }],
  48: [function (_dereq_, module, exports) {
    'use strict';

    var redefineAll = _dereq_(115);

    var getWeak = _dereq_(92).getWeak;

    var anObject = _dereq_(36);

    var isObject = _dereq_(79);

    var anInstance = _dereq_(35);

    var forOf = _dereq_(66);

    var createArrayMethod = _dereq_(40);

    var $has = _dereq_(69);

    var validate = _dereq_(147);

    var arrayFind = createArrayMethod(5);
    var arrayFindIndex = createArrayMethod(6);
    var id = 0; // fallback for uncaught frozen keys

    var uncaughtFrozenStore = function uncaughtFrozenStore(that) {
      return that._l || (that._l = new UncaughtFrozenStore());
    };

    var UncaughtFrozenStore = function UncaughtFrozenStore() {
      this.a = [];
    };

    var findUncaughtFrozen = function findUncaughtFrozen(store, key) {
      return arrayFind(store.a, function (it) {
        return it[0] === key;
      });
    };

    UncaughtFrozenStore.prototype = {
      get: function get(key) {
        var entry = findUncaughtFrozen(this, key);
        if (entry) return entry[1];
      },
      has: function has(key) {
        return !!findUncaughtFrozen(this, key);
      },
      set: function set(key, value) {
        var entry = findUncaughtFrozen(this, key);
        if (entry) entry[1] = value;else this.a.push([key, value]);
      },
      'delete': function _delete(key) {
        var index = arrayFindIndex(this.a, function (it) {
          return it[0] === key;
        });
        if (~index) this.a.splice(index, 1);
        return !!~index;
      }
    };
    module.exports = {
      getConstructor: function getConstructor(wrapper, NAME, IS_MAP, ADDER) {
        var C = wrapper(function (that, iterable) {
          anInstance(that, C, NAME, '_i');
          that._t = NAME; // collection type

          that._i = id++; // collection id

          that._l = undefined; // leak store for uncaught frozen objects

          if (iterable != undefined) forOf(iterable, IS_MAP, that[ADDER], that);
        });
        redefineAll(C.prototype, {
          // 23.3.3.2 WeakMap.prototype.delete(key)
          // 23.4.3.3 WeakSet.prototype.delete(value)
          'delete': function _delete(key) {
            if (!isObject(key)) return false;
            var data = getWeak(key);
            if (data === true) return uncaughtFrozenStore(validate(this, NAME))['delete'](key);
            return data && $has(data, this._i) && delete data[this._i];
          },
          // 23.3.3.4 WeakMap.prototype.has(key)
          // 23.4.3.4 WeakSet.prototype.has(value)
          has: function has(key) {
            if (!isObject(key)) return false;
            var data = getWeak(key);
            if (data === true) return uncaughtFrozenStore(validate(this, NAME)).has(key);
            return data && $has(data, this._i);
          }
        });
        return C;
      },
      def: function def(that, key, value) {
        var data = getWeak(anObject(key), true);
        if (data === true) uncaughtFrozenStore(that).set(key, value);else data[that._i] = value;
        return that;
      },
      ufstore: uncaughtFrozenStore
    };
  }, {
    "115": 115,
    "147": 147,
    "35": 35,
    "36": 36,
    "40": 40,
    "66": 66,
    "69": 69,
    "79": 79,
    "92": 92
  }],
  49: [function (_dereq_, module, exports) {
    'use strict';

    var global = _dereq_(68);

    var $export = _dereq_(60);

    var redefine = _dereq_(116);

    var redefineAll = _dereq_(115);

    var meta = _dereq_(92);

    var forOf = _dereq_(66);

    var anInstance = _dereq_(35);

    var isObject = _dereq_(79);

    var fails = _dereq_(62);

    var $iterDetect = _dereq_(84);

    var setToStringTag = _dereq_(122);

    var inheritIfRequired = _dereq_(73);

    module.exports = function (NAME, wrapper, methods, common, IS_MAP, IS_WEAK) {
      var Base = global[NAME];
      var C = Base;
      var ADDER = IS_MAP ? 'set' : 'add';
      var proto = C && C.prototype;
      var O = {};

      var fixMethod = function fixMethod(KEY) {
        var fn = proto[KEY];
        redefine(proto, KEY, KEY == 'delete' ? function (a) {
          return IS_WEAK && !isObject(a) ? false : fn.call(this, a === 0 ? 0 : a);
        } : KEY == 'has' ? function has(a) {
          return IS_WEAK && !isObject(a) ? false : fn.call(this, a === 0 ? 0 : a);
        } : KEY == 'get' ? function get(a) {
          return IS_WEAK && !isObject(a) ? undefined : fn.call(this, a === 0 ? 0 : a);
        } : KEY == 'add' ? function add(a) {
          fn.call(this, a === 0 ? 0 : a);
          return this;
        } : function set(a, b) {
          fn.call(this, a === 0 ? 0 : a, b);
          return this;
        });
      };

      if (typeof C != 'function' || !(IS_WEAK || proto.forEach && !fails(function () {
        new C().entries().next();
      }))) {
        // create collection constructor
        C = common.getConstructor(wrapper, NAME, IS_MAP, ADDER);
        redefineAll(C.prototype, methods);
        meta.NEED = true;
      } else {
        var instance = new C(); // early implementations not supports chaining

        var HASNT_CHAINING = instance[ADDER](IS_WEAK ? {} : -0, 1) != instance; // V8 ~  Chromium 40- weak-collections throws on primitives, but should return false

        var THROWS_ON_PRIMITIVES = fails(function () {
          instance.has(1);
        }); // most early implementations doesn't supports iterables, most modern - not close it correctly

        var ACCEPT_ITERABLES = $iterDetect(function (iter) {
          new C(iter);
        }); // eslint-disable-line no-new
        // for early implementations -0 and +0 not the same

        var BUGGY_ZERO = !IS_WEAK && fails(function () {
          // V8 ~ Chromium 42- fails only with 5+ elements
          var $instance = new C();
          var index = 5;

          while (index--) {
            $instance[ADDER](index, index);
          }

          return !$instance.has(-0);
        });

        if (!ACCEPT_ITERABLES) {
          C = wrapper(function (target, iterable) {
            anInstance(target, C, NAME);
            var that = inheritIfRequired(new Base(), target, C);
            if (iterable != undefined) forOf(iterable, IS_MAP, that[ADDER], that);
            return that;
          });
          C.prototype = proto;
          proto.constructor = C;
        }

        if (THROWS_ON_PRIMITIVES || BUGGY_ZERO) {
          fixMethod('delete');
          fixMethod('has');
          IS_MAP && fixMethod('get');
        }

        if (BUGGY_ZERO || HASNT_CHAINING) fixMethod(ADDER); // weak collections should not contains .clear method

        if (IS_WEAK && proto.clear) delete proto.clear;
      }

      setToStringTag(C, NAME);
      O[NAME] = C;
      $export($export.G + $export.W + $export.F * (C != Base), O);
      if (!IS_WEAK) common.setStrong(C, NAME, IS_MAP);
      return C;
    };
  }, {
    "115": 115,
    "116": 116,
    "122": 122,
    "35": 35,
    "60": 60,
    "62": 62,
    "66": 66,
    "68": 68,
    "73": 73,
    "79": 79,
    "84": 84,
    "92": 92
  }],
  50: [function (_dereq_, module, exports) {
    arguments[4][16][0].apply(exports, arguments);
  }, {
    "16": 16
  }],
  51: [function (_dereq_, module, exports) {
    'use strict';

    var $defineProperty = _dereq_(97);

    var createDesc = _dereq_(114);

    module.exports = function (object, index, value) {
      if (index in object) $defineProperty.f(object, index, createDesc(0, value));else object[index] = value;
    };
  }, {
    "114": 114,
    "97": 97
  }],
  52: [function (_dereq_, module, exports) {
    arguments[4][17][0].apply(exports, arguments);
  }, {
    "17": 17,
    "31": 31
  }],
  53: [function (_dereq_, module, exports) {
    'use strict'; // 20.3.4.36 / 15.9.5.43 Date.prototype.toISOString()

    var fails = _dereq_(62);

    var getTime = Date.prototype.getTime;
    var $toISOString = Date.prototype.toISOString;

    var lz = function lz(num) {
      return num > 9 ? num : '0' + num;
    }; // PhantomJS / old WebKit has a broken implementations


    module.exports = fails(function () {
      return $toISOString.call(new Date(-5e13 - 1)) != '0385-07-25T07:06:39.999Z';
    }) || !fails(function () {
      $toISOString.call(new Date(NaN));
    }) ? function toISOString() {
      if (!isFinite(getTime.call(this))) throw RangeError('Invalid time value');
      var d = this;
      var y = d.getUTCFullYear();
      var m = d.getUTCMilliseconds();
      var s = y < 0 ? '-' : y > 9999 ? '+' : '';
      return s + ('00000' + Math.abs(y)).slice(s ? -6 : -4) + '-' + lz(d.getUTCMonth() + 1) + '-' + lz(d.getUTCDate()) + 'T' + lz(d.getUTCHours()) + ':' + lz(d.getUTCMinutes()) + ':' + lz(d.getUTCSeconds()) + '.' + (m > 99 ? m : '0' + lz(m)) + 'Z';
    } : $toISOString;
  }, {
    "62": 62
  }],
  54: [function (_dereq_, module, exports) {
    'use strict';

    var anObject = _dereq_(36);

    var toPrimitive = _dereq_(141);

    var NUMBER = 'number';

    module.exports = function (hint) {
      if (hint !== 'string' && hint !== NUMBER && hint !== 'default') throw TypeError('Incorrect hint');
      return toPrimitive(anObject(this), hint != NUMBER);
    };
  }, {
    "141": 141,
    "36": 36
  }],
  55: [function (_dereq_, module, exports) {
    // 7.2.1 RequireObjectCoercible(argument)
    module.exports = function (it) {
      if (it == undefined) throw TypeError("Can't call method on  " + it);
      return it;
    };
  }, {}],
  56: [function (_dereq_, module, exports) {
    arguments[4][18][0].apply(exports, arguments);
  }, {
    "18": 18,
    "62": 62
  }],
  57: [function (_dereq_, module, exports) {
    arguments[4][19][0].apply(exports, arguments);
  }, {
    "19": 19,
    "68": 68,
    "79": 79
  }],
  58: [function (_dereq_, module, exports) {
    // IE 8- don't enum bug keys
    module.exports = 'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'.split(',');
  }, {}],
  59: [function (_dereq_, module, exports) {
    // all enumerable object keys, includes symbols
    var getKeys = _dereq_(105);

    var gOPS = _dereq_(102);

    var pIE = _dereq_(106);

    module.exports = function (it) {
      var result = getKeys(it);
      var getSymbols = gOPS.f;

      if (getSymbols) {
        var symbols = getSymbols(it);
        var isEnum = pIE.f;
        var i = 0;
        var key;

        while (symbols.length > i) {
          if (isEnum.call(it, key = symbols[i++])) result.push(key);
        }
      }

      return result;
    };
  }, {
    "102": 102,
    "105": 105,
    "106": 106
  }],
  60: [function (_dereq_, module, exports) {
    var global = _dereq_(68);

    var core = _dereq_(50);

    var hide = _dereq_(70);

    var redefine = _dereq_(116);

    var ctx = _dereq_(52);

    var PROTOTYPE = 'prototype';

    var $export = function $export(type, name, source) {
      var IS_FORCED = type & $export.F;
      var IS_GLOBAL = type & $export.G;
      var IS_STATIC = type & $export.S;
      var IS_PROTO = type & $export.P;
      var IS_BIND = type & $export.B;
      var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
      var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
      var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
      var key, own, out, exp;
      if (IS_GLOBAL) source = name;

      for (key in source) {
        // contains in native
        own = !IS_FORCED && target && target[key] !== undefined; // export native or passed

        out = (own ? target : source)[key]; // bind timers to global for call from export context

        exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out; // extend global

        if (target) redefine(target, key, out, type & $export.U); // export

        if (exports[key] != out) hide(exports, key, exp);
        if (IS_PROTO && expProto[key] != out) expProto[key] = out;
      }
    };

    global.core = core; // type bitmap

    $export.F = 1; // forced

    $export.G = 2; // global

    $export.S = 4; // static

    $export.P = 8; // proto

    $export.B = 16; // bind

    $export.W = 32; // wrap

    $export.U = 64; // safe

    $export.R = 128; // real proto method for `library`

    module.exports = $export;
  }, {
    "116": 116,
    "50": 50,
    "52": 52,
    "68": 68,
    "70": 70
  }],
  61: [function (_dereq_, module, exports) {
    var MATCH = _dereq_(150)('match');

    module.exports = function (KEY) {
      var re = /./;

      try {
        '/./'[KEY](re);
      } catch (e) {
        try {
          re[MATCH] = false;
          return !'/./'[KEY](re);
        } catch (f) {
          /* empty */
        }
      }

      return true;
    };
  }, {
    "150": 150
  }],
  62: [function (_dereq_, module, exports) {
    arguments[4][21][0].apply(exports, arguments);
  }, {
    "21": 21
  }],
  63: [function (_dereq_, module, exports) {
    'use strict';

    _dereq_(246);

    var redefine = _dereq_(116);

    var hide = _dereq_(70);

    var fails = _dereq_(62);

    var defined = _dereq_(55);

    var wks = _dereq_(150);

    var regexpExec = _dereq_(118);

    var SPECIES = wks('species');
    var REPLACE_SUPPORTS_NAMED_GROUPS = !fails(function () {
      // #replace needs built-in support for named groups.
      // #match works fine because it just return the exec results, even if it has
      // a "grops" property.
      var re = /./;

      re.exec = function () {
        var result = [];
        result.groups = {
          a: '7'
        };
        return result;
      };

      return ''.replace(re, '$<a>') !== '7';
    });

    var SPLIT_WORKS_WITH_OVERWRITTEN_EXEC = function () {
      // Chrome 51 has a buggy "split" implementation when RegExp#exec !== nativeExec
      var re = /(?:)/;
      var originalExec = re.exec;

      re.exec = function () {
        return originalExec.apply(this, arguments);
      };

      var result = 'ab'.split(re);
      return result.length === 2 && result[0] === 'a' && result[1] === 'b';
    }();

    module.exports = function (KEY, length, exec) {
      var SYMBOL = wks(KEY);
      var DELEGATES_TO_SYMBOL = !fails(function () {
        // String methods call symbol-named RegEp methods
        var O = {};

        O[SYMBOL] = function () {
          return 7;
        };

        return ''[KEY](O) != 7;
      });
      var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL ? !fails(function () {
        // Symbol-named RegExp methods call .exec
        var execCalled = false;
        var re = /a/;

        re.exec = function () {
          execCalled = true;
          return null;
        };

        if (KEY === 'split') {
          // RegExp[@@split] doesn't call the regex's exec method, but first creates
          // a new one. We need to return the patched regex when creating the new one.
          re.constructor = {};

          re.constructor[SPECIES] = function () {
            return re;
          };
        }

        re[SYMBOL]('');
        return !execCalled;
      }) : undefined;

      if (!DELEGATES_TO_SYMBOL || !DELEGATES_TO_EXEC || KEY === 'replace' && !REPLACE_SUPPORTS_NAMED_GROUPS || KEY === 'split' && !SPLIT_WORKS_WITH_OVERWRITTEN_EXEC) {
        var nativeRegExpMethod = /./[SYMBOL];
        var fns = exec(defined, SYMBOL, ''[KEY], function maybeCallNative(nativeMethod, regexp, str, arg2, forceStringMethod) {
          if (regexp.exec === regexpExec) {
            if (DELEGATES_TO_SYMBOL && !forceStringMethod) {
              // The native String method already delegates to @@method (this
              // polyfilled function), leasing to infinite recursion.
              // We avoid it by directly calling the native @@method method.
              return {
                done: true,
                value: nativeRegExpMethod.call(regexp, str, arg2)
              };
            }

            return {
              done: true,
              value: nativeMethod.call(str, regexp, arg2)
            };
          }

          return {
            done: false
          };
        });
        var strfn = fns[0];
        var rxfn = fns[1];
        redefine(String.prototype, KEY, strfn);
        hide(RegExp.prototype, SYMBOL, length == 2 // 21.2.5.8 RegExp.prototype[@@replace](string, replaceValue)
        // 21.2.5.11 RegExp.prototype[@@split](string, limit)
        ? function (string, arg) {
          return rxfn.call(string, this, arg);
        } // 21.2.5.6 RegExp.prototype[@@match](string)
        // 21.2.5.9 RegExp.prototype[@@search](string)
        : function (string) {
          return rxfn.call(string, this);
        });
      }
    };
  }, {
    "116": 116,
    "118": 118,
    "150": 150,
    "246": 246,
    "55": 55,
    "62": 62,
    "70": 70
  }],
  64: [function (_dereq_, module, exports) {
    'use strict'; // 21.2.5.3 get RegExp.prototype.flags

    var anObject = _dereq_(36);

    module.exports = function () {
      var that = anObject(this);
      var result = '';
      if (that.global) result += 'g';
      if (that.ignoreCase) result += 'i';
      if (that.multiline) result += 'm';
      if (that.unicode) result += 'u';
      if (that.sticky) result += 'y';
      return result;
    };
  }, {
    "36": 36
  }],
  65: [function (_dereq_, module, exports) {
    'use strict'; // https://tc39.github.io/proposal-flatMap/#sec-FlattenIntoArray

    var isArray = _dereq_(77);

    var isObject = _dereq_(79);

    var toLength = _dereq_(139);

    var ctx = _dereq_(52);

    var IS_CONCAT_SPREADABLE = _dereq_(150)('isConcatSpreadable');

    function flattenIntoArray(target, original, source, sourceLen, start, depth, mapper, thisArg) {
      var targetIndex = start;
      var sourceIndex = 0;
      var mapFn = mapper ? ctx(mapper, thisArg, 3) : false;
      var element, spreadable;

      while (sourceIndex < sourceLen) {
        if (sourceIndex in source) {
          element = mapFn ? mapFn(source[sourceIndex], sourceIndex, original) : source[sourceIndex];
          spreadable = false;

          if (isObject(element)) {
            spreadable = element[IS_CONCAT_SPREADABLE];
            spreadable = spreadable !== undefined ? !!spreadable : isArray(element);
          }

          if (spreadable && depth > 0) {
            targetIndex = flattenIntoArray(target, original, element, toLength(element.length), targetIndex, depth - 1) - 1;
          } else {
            if (targetIndex >= 0x1fffffffffffff) throw TypeError();
            target[targetIndex] = element;
          }

          targetIndex++;
        }

        sourceIndex++;
      }

      return targetIndex;
    }

    module.exports = flattenIntoArray;
  }, {
    "139": 139,
    "150": 150,
    "52": 52,
    "77": 77,
    "79": 79
  }],
  66: [function (_dereq_, module, exports) {
    var ctx = _dereq_(52);

    var call = _dereq_(81);

    var isArrayIter = _dereq_(76);

    var anObject = _dereq_(36);

    var toLength = _dereq_(139);

    var getIterFn = _dereq_(151);

    var BREAK = {};
    var RETURN = {};

    var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
      var iterFn = ITERATOR ? function () {
        return iterable;
      } : getIterFn(iterable);
      var f = ctx(fn, that, entries ? 2 : 1);
      var index = 0;
      var length, step, iterator, result;
      if (typeof iterFn != 'function') throw TypeError(iterable + ' is not iterable!'); // fast case for arrays with default iterator

      if (isArrayIter(iterFn)) for (length = toLength(iterable.length); length > index; index++) {
        result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
        if (result === BREAK || result === RETURN) return result;
      } else for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
        result = call(iterator, f, step.value, entries);
        if (result === BREAK || result === RETURN) return result;
      }
    };

    exports.BREAK = BREAK;
    exports.RETURN = RETURN;
  }, {
    "139": 139,
    "151": 151,
    "36": 36,
    "52": 52,
    "76": 76,
    "81": 81
  }],
  67: [function (_dereq_, module, exports) {
    module.exports = _dereq_(124)('native-function-to-string', Function.toString);
  }, {
    "124": 124
  }],
  68: [function (_dereq_, module, exports) {
    arguments[4][22][0].apply(exports, arguments);
  }, {
    "22": 22
  }],
  69: [function (_dereq_, module, exports) {
    arguments[4][23][0].apply(exports, arguments);
  }, {
    "23": 23
  }],
  70: [function (_dereq_, module, exports) {
    arguments[4][24][0].apply(exports, arguments);
  }, {
    "114": 114,
    "24": 24,
    "56": 56,
    "97": 97
  }],
  71: [function (_dereq_, module, exports) {
    var document = _dereq_(68).document;

    module.exports = document && document.documentElement;
  }, {
    "68": 68
  }],
  72: [function (_dereq_, module, exports) {
    arguments[4][25][0].apply(exports, arguments);
  }, {
    "25": 25,
    "56": 56,
    "57": 57,
    "62": 62
  }],
  73: [function (_dereq_, module, exports) {
    var isObject = _dereq_(79);

    var setPrototypeOf = _dereq_(120).set;

    module.exports = function (that, target, C) {
      var S = target.constructor;
      var P;

      if (S !== C && typeof S == 'function' && (P = S.prototype) !== C.prototype && isObject(P) && setPrototypeOf) {
        setPrototypeOf(that, P);
      }

      return that;
    };
  }, {
    "120": 120,
    "79": 79
  }],
  74: [function (_dereq_, module, exports) {
    // fast apply, http://jsperf.lnkit.com/fast-apply/5
    module.exports = function (fn, args, that) {
      var un = that === undefined;

      switch (args.length) {
        case 0:
          return un ? fn() : fn.call(that);

        case 1:
          return un ? fn(args[0]) : fn.call(that, args[0]);

        case 2:
          return un ? fn(args[0], args[1]) : fn.call(that, args[0], args[1]);

        case 3:
          return un ? fn(args[0], args[1], args[2]) : fn.call(that, args[0], args[1], args[2]);

        case 4:
          return un ? fn(args[0], args[1], args[2], args[3]) : fn.call(that, args[0], args[1], args[2], args[3]);
      }

      return fn.apply(that, args);
    };
  }, {}],
  75: [function (_dereq_, module, exports) {
    // fallback for non-array-like ES3 and non-enumerable old V8 strings
    var cof = _dereq_(46); // eslint-disable-next-line no-prototype-builtins


    module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
      return cof(it) == 'String' ? it.split('') : Object(it);
    };
  }, {
    "46": 46
  }],
  76: [function (_dereq_, module, exports) {
    // check on default Array iterator
    var Iterators = _dereq_(86);

    var ITERATOR = _dereq_(150)('iterator');

    var ArrayProto = Array.prototype;

    module.exports = function (it) {
      return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
    };
  }, {
    "150": 150,
    "86": 86
  }],
  77: [function (_dereq_, module, exports) {
    // 7.2.2 IsArray(argument)
    var cof = _dereq_(46);

    module.exports = Array.isArray || function isArray(arg) {
      return cof(arg) == 'Array';
    };
  }, {
    "46": 46
  }],
  78: [function (_dereq_, module, exports) {
    // 20.1.2.3 Number.isInteger(number)
    var isObject = _dereq_(79);

    var floor = Math.floor;

    module.exports = function isInteger(it) {
      return !isObject(it) && isFinite(it) && floor(it) === it;
    };
  }, {
    "79": 79
  }],
  79: [function (_dereq_, module, exports) {
    arguments[4][26][0].apply(exports, arguments);
  }, {
    "26": 26
  }],
  80: [function (_dereq_, module, exports) {
    // 7.2.8 IsRegExp(argument)
    var isObject = _dereq_(79);

    var cof = _dereq_(46);

    var MATCH = _dereq_(150)('match');

    module.exports = function (it) {
      var isRegExp;
      return isObject(it) && ((isRegExp = it[MATCH]) !== undefined ? !!isRegExp : cof(it) == 'RegExp');
    };
  }, {
    "150": 150,
    "46": 46,
    "79": 79
  }],
  81: [function (_dereq_, module, exports) {
    // call something on iterator step with safe closing on error
    var anObject = _dereq_(36);

    module.exports = function (iterator, fn, value, entries) {
      try {
        return entries ? fn(anObject(value)[0], value[1]) : fn(value); // 7.4.6 IteratorClose(iterator, completion)
      } catch (e) {
        var ret = iterator['return'];
        if (ret !== undefined) anObject(ret.call(iterator));
        throw e;
      }
    };
  }, {
    "36": 36
  }],
  82: [function (_dereq_, module, exports) {
    'use strict';

    var create = _dereq_(96);

    var descriptor = _dereq_(114);

    var setToStringTag = _dereq_(122);

    var IteratorPrototype = {}; // 25.1.2.1.1 %IteratorPrototype%[@@iterator]()

    _dereq_(70)(IteratorPrototype, _dereq_(150)('iterator'), function () {
      return this;
    });

    module.exports = function (Constructor, NAME, next) {
      Constructor.prototype = create(IteratorPrototype, {
        next: descriptor(1, next)
      });
      setToStringTag(Constructor, NAME + ' Iterator');
    };
  }, {
    "114": 114,
    "122": 122,
    "150": 150,
    "70": 70,
    "96": 96
  }],
  83: [function (_dereq_, module, exports) {
    'use strict';

    var LIBRARY = _dereq_(87);

    var $export = _dereq_(60);

    var redefine = _dereq_(116);

    var hide = _dereq_(70);

    var Iterators = _dereq_(86);

    var $iterCreate = _dereq_(82);

    var setToStringTag = _dereq_(122);

    var getPrototypeOf = _dereq_(103);

    var ITERATOR = _dereq_(150)('iterator');

    var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`

    var FF_ITERATOR = '@@iterator';
    var KEYS = 'keys';
    var VALUES = 'values';

    var returnThis = function returnThis() {
      return this;
    };

    module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
      $iterCreate(Constructor, NAME, next);

      var getMethod = function getMethod(kind) {
        if (!BUGGY && kind in proto) return proto[kind];

        switch (kind) {
          case KEYS:
            return function keys() {
              return new Constructor(this, kind);
            };

          case VALUES:
            return function values() {
              return new Constructor(this, kind);
            };
        }

        return function entries() {
          return new Constructor(this, kind);
        };
      };

      var TAG = NAME + ' Iterator';
      var DEF_VALUES = DEFAULT == VALUES;
      var VALUES_BUG = false;
      var proto = Base.prototype;
      var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
      var $default = $native || getMethod(DEFAULT);
      var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
      var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
      var methods, key, IteratorPrototype; // Fix native

      if ($anyNative) {
        IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));

        if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
          // Set @@toStringTag to native iterators
          setToStringTag(IteratorPrototype, TAG, true); // fix for some old engines

          if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
        }
      } // fix Array#{values, @@iterator}.name in V8 / FF


      if (DEF_VALUES && $native && $native.name !== VALUES) {
        VALUES_BUG = true;

        $default = function values() {
          return $native.call(this);
        };
      } // Define iterator


      if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
        hide(proto, ITERATOR, $default);
      } // Plug for library


      Iterators[NAME] = $default;
      Iterators[TAG] = returnThis;

      if (DEFAULT) {
        methods = {
          values: DEF_VALUES ? $default : getMethod(VALUES),
          keys: IS_SET ? $default : getMethod(KEYS),
          entries: $entries
        };
        if (FORCED) for (key in methods) {
          if (!(key in proto)) redefine(proto, key, methods[key]);
        } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
      }

      return methods;
    };
  }, {
    "103": 103,
    "116": 116,
    "122": 122,
    "150": 150,
    "60": 60,
    "70": 70,
    "82": 82,
    "86": 86,
    "87": 87
  }],
  84: [function (_dereq_, module, exports) {
    var ITERATOR = _dereq_(150)('iterator');

    var SAFE_CLOSING = false;

    try {
      var riter = [7][ITERATOR]();

      riter['return'] = function () {
        SAFE_CLOSING = true;
      }; // eslint-disable-next-line no-throw-literal


      Array.from(riter, function () {
        throw 2;
      });
    } catch (e) {
      /* empty */
    }

    module.exports = function (exec, skipClosing) {
      if (!skipClosing && !SAFE_CLOSING) return false;
      var safe = false;

      try {
        var arr = [7];
        var iter = arr[ITERATOR]();

        iter.next = function () {
          return {
            done: safe = true
          };
        };

        arr[ITERATOR] = function () {
          return iter;
        };

        exec(arr);
      } catch (e) {
        /* empty */
      }

      return safe;
    };
  }, {
    "150": 150
  }],
  85: [function (_dereq_, module, exports) {
    module.exports = function (done, value) {
      return {
        value: value,
        done: !!done
      };
    };
  }, {}],
  86: [function (_dereq_, module, exports) {
    module.exports = {};
  }, {}],
  87: [function (_dereq_, module, exports) {
    module.exports = false;
  }, {}],
  88: [function (_dereq_, module, exports) {
    // 20.2.2.14 Math.expm1(x)
    var $expm1 = Math.expm1;
    module.exports = !$expm1 // Old FF bug
    || $expm1(10) > 22025.465794806719 || $expm1(10) < 22025.4657948067165168 // Tor Browser bug
    || $expm1(-2e-17) != -2e-17 ? function expm1(x) {
      return (x = +x) == 0 ? x : x > -1e-6 && x < 1e-6 ? x + x * x / 2 : Math.exp(x) - 1;
    } : $expm1;
  }, {}],
  89: [function (_dereq_, module, exports) {
    // 20.2.2.16 Math.fround(x)
    var sign = _dereq_(91);

    var pow = Math.pow;
    var EPSILON = pow(2, -52);
    var EPSILON32 = pow(2, -23);
    var MAX32 = pow(2, 127) * (2 - EPSILON32);
    var MIN32 = pow(2, -126);

    var roundTiesToEven = function roundTiesToEven(n) {
      return n + 1 / EPSILON - 1 / EPSILON;
    };

    module.exports = Math.fround || function fround(x) {
      var $abs = Math.abs(x);
      var $sign = sign(x);
      var a, result;
      if ($abs < MIN32) return $sign * roundTiesToEven($abs / MIN32 / EPSILON32) * MIN32 * EPSILON32;
      a = (1 + EPSILON32 / EPSILON) * $abs;
      result = a - (a - $abs); // eslint-disable-next-line no-self-compare

      if (result > MAX32 || result != result) return $sign * Infinity;
      return $sign * result;
    };
  }, {
    "91": 91
  }],
  90: [function (_dereq_, module, exports) {
    // 20.2.2.20 Math.log1p(x)
    module.exports = Math.log1p || function log1p(x) {
      return (x = +x) > -1e-8 && x < 1e-8 ? x - x * x / 2 : Math.log(1 + x);
    };
  }, {}],
  91: [function (_dereq_, module, exports) {
    // 20.2.2.28 Math.sign(x)
    module.exports = Math.sign || function sign(x) {
      // eslint-disable-next-line no-self-compare
      return (x = +x) == 0 || x != x ? x : x < 0 ? -1 : 1;
    };
  }, {}],
  92: [function (_dereq_, module, exports) {
    var META = _dereq_(145)('meta');

    var isObject = _dereq_(79);

    var has = _dereq_(69);

    var setDesc = _dereq_(97).f;

    var id = 0;

    var isExtensible = Object.isExtensible || function () {
      return true;
    };

    var FREEZE = !_dereq_(62)(function () {
      return isExtensible(Object.preventExtensions({}));
    });

    var setMeta = function setMeta(it) {
      setDesc(it, META, {
        value: {
          i: 'O' + ++id,
          // object ID
          w: {} // weak collections IDs

        }
      });
    };

    var fastKey = function fastKey(it, create) {
      // return primitive with prefix
      if (!isObject(it)) return _typeof(it) == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;

      if (!has(it, META)) {
        // can't set metadata to uncaught frozen object
        if (!isExtensible(it)) return 'F'; // not necessary to add metadata

        if (!create) return 'E'; // add missing metadata

        setMeta(it); // return object ID
      }

      return it[META].i;
    };

    var getWeak = function getWeak(it, create) {
      if (!has(it, META)) {
        // can't set metadata to uncaught frozen object
        if (!isExtensible(it)) return true; // not necessary to add metadata

        if (!create) return false; // add missing metadata

        setMeta(it); // return hash weak collections IDs
      }

      return it[META].w;
    }; // add metadata on freeze-family methods calling


    var onFreeze = function onFreeze(it) {
      if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
      return it;
    };

    var meta = module.exports = {
      KEY: META,
      NEED: false,
      fastKey: fastKey,
      getWeak: getWeak,
      onFreeze: onFreeze
    };
  }, {
    "145": 145,
    "62": 62,
    "69": 69,
    "79": 79,
    "97": 97
  }],
  93: [function (_dereq_, module, exports) {
    var global = _dereq_(68);

    var macrotask = _dereq_(134).set;

    var Observer = global.MutationObserver || global.WebKitMutationObserver;
    var process = global.process;
    var Promise = global.Promise;
    var isNode = _dereq_(46)(process) == 'process';

    module.exports = function () {
      var head, last, notify;

      var flush = function flush() {
        var parent, fn;
        if (isNode && (parent = process.domain)) parent.exit();

        while (head) {
          fn = head.fn;
          head = head.next;

          try {
            fn();
          } catch (e) {
            if (head) notify();else last = undefined;
            throw e;
          }
        }

        last = undefined;
        if (parent) parent.enter();
      }; // Node.js


      if (isNode) {
        notify = function notify() {
          process.nextTick(flush);
        }; // browsers with MutationObserver, except iOS Safari - https://github.com/zloirock/core-js/issues/339

      } else if (Observer && !(global.navigator && global.navigator.standalone)) {
        var toggle = true;
        var node = document.createTextNode('');
        new Observer(flush).observe(node, {
          characterData: true
        }); // eslint-disable-line no-new

        notify = function notify() {
          node.data = toggle = !toggle;
        }; // environments with maybe non-completely correct, but existent Promise

      } else if (Promise && Promise.resolve) {
        // Promise.resolve without an argument throws an error in LG WebOS 2
        var promise = Promise.resolve(undefined);

        notify = function notify() {
          promise.then(flush);
        }; // for other environments - macrotask based on:
        // - setImmediate
        // - MessageChannel
        // - window.postMessag
        // - onreadystatechange
        // - setTimeout

      } else {
        notify = function notify() {
          // strange IE + webpack dev server bug - use .call(global)
          macrotask.call(global, flush);
        };
      }

      return function (fn) {
        var task = {
          fn: fn,
          next: undefined
        };
        if (last) last.next = task;

        if (!head) {
          head = task;
          notify();
        }

        last = task;
      };
    };
  }, {
    "134": 134,
    "46": 46,
    "68": 68
  }],
  94: [function (_dereq_, module, exports) {
    'use strict'; // 25.4.1.5 NewPromiseCapability(C)

    var aFunction = _dereq_(31);

    function PromiseCapability(C) {
      var resolve, reject;
      this.promise = new C(function ($$resolve, $$reject) {
        if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor');
        resolve = $$resolve;
        reject = $$reject;
      });
      this.resolve = aFunction(resolve);
      this.reject = aFunction(reject);
    }

    module.exports.f = function (C) {
      return new PromiseCapability(C);
    };
  }, {
    "31": 31
  }],
  95: [function (_dereq_, module, exports) {
    'use strict'; // 19.1.2.1 Object.assign(target, source, ...)

    var DESCRIPTORS = _dereq_(56);

    var getKeys = _dereq_(105);

    var gOPS = _dereq_(102);

    var pIE = _dereq_(106);

    var toObject = _dereq_(140);

    var IObject = _dereq_(75);

    var $assign = Object.assign; // should work with symbols and should have deterministic property order (V8 bug)

    module.exports = !$assign || _dereq_(62)(function () {
      var A = {};
      var B = {}; // eslint-disable-next-line no-undef

      var S = Symbol();
      var K = 'abcdefghijklmnopqrst';
      A[S] = 7;
      K.split('').forEach(function (k) {
        B[k] = k;
      });
      return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
    }) ? function assign(target, source) {
      // eslint-disable-line no-unused-vars
      var T = toObject(target);
      var aLen = arguments.length;
      var index = 1;
      var getSymbols = gOPS.f;
      var isEnum = pIE.f;

      while (aLen > index) {
        var S = IObject(arguments[index++]);
        var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
        var length = keys.length;
        var j = 0;
        var key;

        while (length > j) {
          key = keys[j++];
          if (!DESCRIPTORS || isEnum.call(S, key)) T[key] = S[key];
        }
      }

      return T;
    } : $assign;
  }, {
    "102": 102,
    "105": 105,
    "106": 106,
    "140": 140,
    "56": 56,
    "62": 62,
    "75": 75
  }],
  96: [function (_dereq_, module, exports) {
    // 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
    var anObject = _dereq_(36);

    var dPs = _dereq_(98);

    var enumBugKeys = _dereq_(58);

    var IE_PROTO = _dereq_(123)('IE_PROTO');

    var Empty = function Empty() {
      /* empty */
    };

    var PROTOTYPE = 'prototype'; // Create object with fake `null` prototype: use iframe Object with cleared prototype

    var _createDict = function createDict() {
      // Thrash, waste and sodomy: IE GC bug
      var iframe = _dereq_(57)('iframe');

      var i = enumBugKeys.length;
      var lt = '<';
      var gt = '>';
      var iframeDocument;
      iframe.style.display = 'none';

      _dereq_(71).appendChild(iframe);

      iframe.src = 'javascript:'; // eslint-disable-line no-script-url
      // createDict = iframe.contentWindow.Object;
      // html.removeChild(iframe);

      iframeDocument = iframe.contentWindow.document;
      iframeDocument.open();
      iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
      iframeDocument.close();
      _createDict = iframeDocument.F;

      while (i--) {
        delete _createDict[PROTOTYPE][enumBugKeys[i]];
      }

      return _createDict();
    };

    module.exports = Object.create || function create(O, Properties) {
      var result;

      if (O !== null) {
        Empty[PROTOTYPE] = anObject(O);
        result = new Empty();
        Empty[PROTOTYPE] = null; // add "__proto__" for Object.getPrototypeOf polyfill

        result[IE_PROTO] = O;
      } else result = _createDict();

      return Properties === undefined ? result : dPs(result, Properties);
    };
  }, {
    "123": 123,
    "36": 36,
    "57": 57,
    "58": 58,
    "71": 71,
    "98": 98
  }],
  97: [function (_dereq_, module, exports) {
    arguments[4][27][0].apply(exports, arguments);
  }, {
    "141": 141,
    "27": 27,
    "36": 36,
    "56": 56,
    "72": 72
  }],
  98: [function (_dereq_, module, exports) {
    var dP = _dereq_(97);

    var anObject = _dereq_(36);

    var getKeys = _dereq_(105);

    module.exports = _dereq_(56) ? Object.defineProperties : function defineProperties(O, Properties) {
      anObject(O);
      var keys = getKeys(Properties);
      var length = keys.length;
      var i = 0;
      var P;

      while (length > i) {
        dP.f(O, P = keys[i++], Properties[P]);
      }

      return O;
    };
  }, {
    "105": 105,
    "36": 36,
    "56": 56,
    "97": 97
  }],
  99: [function (_dereq_, module, exports) {
    var pIE = _dereq_(106);

    var createDesc = _dereq_(114);

    var toIObject = _dereq_(138);

    var toPrimitive = _dereq_(141);

    var has = _dereq_(69);

    var IE8_DOM_DEFINE = _dereq_(72);

    var gOPD = Object.getOwnPropertyDescriptor;
    exports.f = _dereq_(56) ? gOPD : function getOwnPropertyDescriptor(O, P) {
      O = toIObject(O);
      P = toPrimitive(P, true);
      if (IE8_DOM_DEFINE) try {
        return gOPD(O, P);
      } catch (e) {
        /* empty */
      }
      if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
    };
  }, {
    "106": 106,
    "114": 114,
    "138": 138,
    "141": 141,
    "56": 56,
    "69": 69,
    "72": 72
  }],
  100: [function (_dereq_, module, exports) {
    // fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
    var toIObject = _dereq_(138);

    var gOPN = _dereq_(101).f;

    var toString = {}.toString;
    var windowNames = (typeof window === "undefined" ? "undefined" : _typeof(window)) == 'object' && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [];

    var getWindowNames = function getWindowNames(it) {
      try {
        return gOPN(it);
      } catch (e) {
        return windowNames.slice();
      }
    };

    module.exports.f = function getOwnPropertyNames(it) {
      return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
    };
  }, {
    "101": 101,
    "138": 138
  }],
  101: [function (_dereq_, module, exports) {
    // 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
    var $keys = _dereq_(104);

    var hiddenKeys = _dereq_(58).concat('length', 'prototype');

    exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
      return $keys(O, hiddenKeys);
    };
  }, {
    "104": 104,
    "58": 58
  }],
  102: [function (_dereq_, module, exports) {
    exports.f = Object.getOwnPropertySymbols;
  }, {}],
  103: [function (_dereq_, module, exports) {
    // 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
    var has = _dereq_(69);

    var toObject = _dereq_(140);

    var IE_PROTO = _dereq_(123)('IE_PROTO');

    var ObjectProto = Object.prototype;

    module.exports = Object.getPrototypeOf || function (O) {
      O = toObject(O);
      if (has(O, IE_PROTO)) return O[IE_PROTO];

      if (typeof O.constructor == 'function' && O instanceof O.constructor) {
        return O.constructor.prototype;
      }

      return O instanceof Object ? ObjectProto : null;
    };
  }, {
    "123": 123,
    "140": 140,
    "69": 69
  }],
  104: [function (_dereq_, module, exports) {
    var has = _dereq_(69);

    var toIObject = _dereq_(138);

    var arrayIndexOf = _dereq_(39)(false);

    var IE_PROTO = _dereq_(123)('IE_PROTO');

    module.exports = function (object, names) {
      var O = toIObject(object);
      var i = 0;
      var result = [];
      var key;

      for (key in O) {
        if (key != IE_PROTO) has(O, key) && result.push(key);
      } // Don't enum bug & hidden keys


      while (names.length > i) {
        if (has(O, key = names[i++])) {
          ~arrayIndexOf(result, key) || result.push(key);
        }
      }

      return result;
    };
  }, {
    "123": 123,
    "138": 138,
    "39": 39,
    "69": 69
  }],
  105: [function (_dereq_, module, exports) {
    // 19.1.2.14 / 15.2.3.14 Object.keys(O)
    var $keys = _dereq_(104);

    var enumBugKeys = _dereq_(58);

    module.exports = Object.keys || function keys(O) {
      return $keys(O, enumBugKeys);
    };
  }, {
    "104": 104,
    "58": 58
  }],
  106: [function (_dereq_, module, exports) {
    exports.f = {}.propertyIsEnumerable;
  }, {}],
  107: [function (_dereq_, module, exports) {
    // most Object methods by ES6 should accept primitives
    var $export = _dereq_(60);

    var core = _dereq_(50);

    var fails = _dereq_(62);

    module.exports = function (KEY, exec) {
      var fn = (core.Object || {})[KEY] || Object[KEY];
      var exp = {};
      exp[KEY] = exec(fn);
      $export($export.S + $export.F * fails(function () {
        fn(1);
      }), 'Object', exp);
    };
  }, {
    "50": 50,
    "60": 60,
    "62": 62
  }],
  108: [function (_dereq_, module, exports) {
    var DESCRIPTORS = _dereq_(56);

    var getKeys = _dereq_(105);

    var toIObject = _dereq_(138);

    var isEnum = _dereq_(106).f;

    module.exports = function (isEntries) {
      return function (it) {
        var O = toIObject(it);
        var keys = getKeys(O);
        var length = keys.length;
        var i = 0;
        var result = [];
        var key;

        while (length > i) {
          key = keys[i++];

          if (!DESCRIPTORS || isEnum.call(O, key)) {
            result.push(isEntries ? [key, O[key]] : O[key]);
          }
        }

        return result;
      };
    };
  }, {
    "105": 105,
    "106": 106,
    "138": 138,
    "56": 56
  }],
  109: [function (_dereq_, module, exports) {
    // all object keys, includes non-enumerable and symbols
    var gOPN = _dereq_(101);

    var gOPS = _dereq_(102);

    var anObject = _dereq_(36);

    var Reflect = _dereq_(68).Reflect;

    module.exports = Reflect && Reflect.ownKeys || function ownKeys(it) {
      var keys = gOPN.f(anObject(it));
      var getSymbols = gOPS.f;
      return getSymbols ? keys.concat(getSymbols(it)) : keys;
    };
  }, {
    "101": 101,
    "102": 102,
    "36": 36,
    "68": 68
  }],
  110: [function (_dereq_, module, exports) {
    var $parseFloat = _dereq_(68).parseFloat;

    var $trim = _dereq_(132).trim;

    module.exports = 1 / $parseFloat(_dereq_(133) + '-0') !== -Infinity ? function parseFloat(str) {
      var string = $trim(String(str), 3);
      var result = $parseFloat(string);
      return result === 0 && string.charAt(0) == '-' ? -0 : result;
    } : $parseFloat;
  }, {
    "132": 132,
    "133": 133,
    "68": 68
  }],
  111: [function (_dereq_, module, exports) {
    var $parseInt = _dereq_(68).parseInt;

    var $trim = _dereq_(132).trim;

    var ws = _dereq_(133);

    var hex = /^[-+]?0[xX]/;
    module.exports = $parseInt(ws + '08') !== 8 || $parseInt(ws + '0x16') !== 22 ? function parseInt(str, radix) {
      var string = $trim(String(str), 3);
      return $parseInt(string, radix >>> 0 || (hex.test(string) ? 16 : 10));
    } : $parseInt;
  }, {
    "132": 132,
    "133": 133,
    "68": 68
  }],
  112: [function (_dereq_, module, exports) {
    module.exports = function (exec) {
      try {
        return {
          e: false,
          v: exec()
        };
      } catch (e) {
        return {
          e: true,
          v: e
        };
      }
    };
  }, {}],
  113: [function (_dereq_, module, exports) {
    var anObject = _dereq_(36);

    var isObject = _dereq_(79);

    var newPromiseCapability = _dereq_(94);

    module.exports = function (C, x) {
      anObject(C);
      if (isObject(x) && x.constructor === C) return x;
      var promiseCapability = newPromiseCapability.f(C);
      var resolve = promiseCapability.resolve;
      resolve(x);
      return promiseCapability.promise;
    };
  }, {
    "36": 36,
    "79": 79,
    "94": 94
  }],
  114: [function (_dereq_, module, exports) {
    arguments[4][28][0].apply(exports, arguments);
  }, {
    "28": 28
  }],
  115: [function (_dereq_, module, exports) {
    var redefine = _dereq_(116);

    module.exports = function (target, src, safe) {
      for (var key in src) {
        redefine(target, key, src[key], safe);
      }

      return target;
    };
  }, {
    "116": 116
  }],
  116: [function (_dereq_, module, exports) {
    var global = _dereq_(68);

    var hide = _dereq_(70);

    var has = _dereq_(69);

    var SRC = _dereq_(145)('src');

    var $toString = _dereq_(67);

    var TO_STRING = 'toString';
    var TPL = ('' + $toString).split(TO_STRING);

    _dereq_(50).inspectSource = function (it) {
      return $toString.call(it);
    };

    (module.exports = function (O, key, val, safe) {
      var isFunction = typeof val == 'function';
      if (isFunction) has(val, 'name') || hide(val, 'name', key);
      if (O[key] === val) return;
      if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));

      if (O === global) {
        O[key] = val;
      } else if (!safe) {
        delete O[key];
        hide(O, key, val);
      } else if (O[key]) {
        O[key] = val;
      } else {
        hide(O, key, val);
      } // add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative

    })(Function.prototype, TO_STRING, function toString() {
      return typeof this == 'function' && this[SRC] || $toString.call(this);
    });
  }, {
    "145": 145,
    "50": 50,
    "67": 67,
    "68": 68,
    "69": 69,
    "70": 70
  }],
  117: [function (_dereq_, module, exports) {
    'use strict';

    var classof = _dereq_(45);

    var builtinExec = RegExp.prototype.exec; // `RegExpExec` abstract operation
    // https://tc39.github.io/ecma262/#sec-regexpexec

    module.exports = function (R, S) {
      var exec = R.exec;

      if (typeof exec === 'function') {
        var result = exec.call(R, S);

        if (_typeof(result) !== 'object') {
          throw new TypeError('RegExp exec method returned something other than an Object or null');
        }

        return result;
      }

      if (classof(R) !== 'RegExp') {
        throw new TypeError('RegExp#exec called on incompatible receiver');
      }

      return builtinExec.call(R, S);
    };
  }, {
    "45": 45
  }],
  118: [function (_dereq_, module, exports) {
    'use strict';

    var regexpFlags = _dereq_(64);

    var nativeExec = RegExp.prototype.exec; // This always refers to the native implementation, because the
    // String#replace polyfill uses ./fix-regexp-well-known-symbol-logic.js,
    // which loads this file before patching the method.

    var nativeReplace = String.prototype.replace;
    var patchedExec = nativeExec;
    var LAST_INDEX = 'lastIndex';

    var UPDATES_LAST_INDEX_WRONG = function () {
      var re1 = /a/,
          re2 = /b*/g;
      nativeExec.call(re1, 'a');
      nativeExec.call(re2, 'a');
      return re1[LAST_INDEX] !== 0 || re2[LAST_INDEX] !== 0;
    }(); // nonparticipating capturing group, copied from es5-shim's String#split patch.


    var NPCG_INCLUDED = /()??/.exec('')[1] !== undefined;
    var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED;

    if (PATCH) {
      patchedExec = function exec(str) {
        var re = this;
        var lastIndex, reCopy, match, i;

        if (NPCG_INCLUDED) {
          reCopy = new RegExp('^' + re.source + '$(?!\\s)', regexpFlags.call(re));
        }

        if (UPDATES_LAST_INDEX_WRONG) lastIndex = re[LAST_INDEX];
        match = nativeExec.call(re, str);

        if (UPDATES_LAST_INDEX_WRONG && match) {
          re[LAST_INDEX] = re.global ? match.index + match[0].length : lastIndex;
        }

        if (NPCG_INCLUDED && match && match.length > 1) {
          // Fix browsers whose `exec` methods don't consistently return `undefined`
          // for NPCG, like IE8. NOTE: This doesn' work for /(.?)?/
          // eslint-disable-next-line no-loop-func
          nativeReplace.call(match[0], reCopy, function () {
            for (i = 1; i < arguments.length - 2; i++) {
              if (arguments[i] === undefined) match[i] = undefined;
            }
          });
        }

        return match;
      };
    }

    module.exports = patchedExec;
  }, {
    "64": 64
  }],
  119: [function (_dereq_, module, exports) {
    // 7.2.9 SameValue(x, y)
    module.exports = Object.is || function is(x, y) {
      // eslint-disable-next-line no-self-compare
      return x === y ? x !== 0 || 1 / x === 1 / y : x != x && y != y;
    };
  }, {}],
  120: [function (_dereq_, module, exports) {
    // Works with __proto__ only. Old v8 can't work with null proto objects.

    /* eslint-disable no-proto */
    var isObject = _dereq_(79);

    var anObject = _dereq_(36);

    var check = function check(O, proto) {
      anObject(O);
      if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
    };

    module.exports = {
      set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
      function (test, buggy, set) {
        try {
          set = _dereq_(52)(Function.call, _dereq_(99).f(Object.prototype, '__proto__').set, 2);
          set(test, []);
          buggy = !(test instanceof Array);
        } catch (e) {
          buggy = true;
        }

        return function setPrototypeOf(O, proto) {
          check(O, proto);
          if (buggy) O.__proto__ = proto;else set(O, proto);
          return O;
        };
      }({}, false) : undefined),
      check: check
    };
  }, {
    "36": 36,
    "52": 52,
    "79": 79,
    "99": 99
  }],
  121: [function (_dereq_, module, exports) {
    'use strict';

    var global = _dereq_(68);

    var dP = _dereq_(97);

    var DESCRIPTORS = _dereq_(56);

    var SPECIES = _dereq_(150)('species');

    module.exports = function (KEY) {
      var C = global[KEY];
      if (DESCRIPTORS && C && !C[SPECIES]) dP.f(C, SPECIES, {
        configurable: true,
        get: function get() {
          return this;
        }
      });
    };
  }, {
    "150": 150,
    "56": 56,
    "68": 68,
    "97": 97
  }],
  122: [function (_dereq_, module, exports) {
    var def = _dereq_(97).f;

    var has = _dereq_(69);

    var TAG = _dereq_(150)('toStringTag');

    module.exports = function (it, tag, stat) {
      if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, {
        configurable: true,
        value: tag
      });
    };
  }, {
    "150": 150,
    "69": 69,
    "97": 97
  }],
  123: [function (_dereq_, module, exports) {
    var shared = _dereq_(124)('keys');

    var uid = _dereq_(145);

    module.exports = function (key) {
      return shared[key] || (shared[key] = uid(key));
    };
  }, {
    "124": 124,
    "145": 145
  }],
  124: [function (_dereq_, module, exports) {
    var core = _dereq_(50);

    var global = _dereq_(68);

    var SHARED = '__core-js_shared__';
    var store = global[SHARED] || (global[SHARED] = {});
    (module.exports = function (key, value) {
      return store[key] || (store[key] = value !== undefined ? value : {});
    })('versions', []).push({
      version: core.version,
      mode: _dereq_(87) ? 'pure' : 'global',
      copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
    });
  }, {
    "50": 50,
    "68": 68,
    "87": 87
  }],
  125: [function (_dereq_, module, exports) {
    // 7.3.20 SpeciesConstructor(O, defaultConstructor)
    var anObject = _dereq_(36);

    var aFunction = _dereq_(31);

    var SPECIES = _dereq_(150)('species');

    module.exports = function (O, D) {
      var C = anObject(O).constructor;
      var S;
      return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
    };
  }, {
    "150": 150,
    "31": 31,
    "36": 36
  }],
  126: [function (_dereq_, module, exports) {
    'use strict';

    var fails = _dereq_(62);

    module.exports = function (method, arg) {
      return !!method && fails(function () {
        // eslint-disable-next-line no-useless-call
        arg ? method.call(null, function () {
          /* empty */
        }, 1) : method.call(null);
      });
    };
  }, {
    "62": 62
  }],
  127: [function (_dereq_, module, exports) {
    var toInteger = _dereq_(137);

    var defined = _dereq_(55); // true  -> String#at
    // false -> String#codePointAt


    module.exports = function (TO_STRING) {
      return function (that, pos) {
        var s = String(defined(that));
        var i = toInteger(pos);
        var l = s.length;
        var a, b;
        if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
        a = s.charCodeAt(i);
        return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff ? TO_STRING ? s.charAt(i) : a : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
      };
    };
  }, {
    "137": 137,
    "55": 55
  }],
  128: [function (_dereq_, module, exports) {
    // helper for String#{startsWith, endsWith, includes}
    var isRegExp = _dereq_(80);

    var defined = _dereq_(55);

    module.exports = function (that, searchString, NAME) {
      if (isRegExp(searchString)) throw TypeError('String#' + NAME + " doesn't accept regex!");
      return String(defined(that));
    };
  }, {
    "55": 55,
    "80": 80
  }],
  129: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    var fails = _dereq_(62);

    var defined = _dereq_(55);

    var quot = /"/g; // B.2.3.2.1 CreateHTML(string, tag, attribute, value)

    var createHTML = function createHTML(string, tag, attribute, value) {
      var S = String(defined(string));
      var p1 = '<' + tag;
      if (attribute !== '') p1 += ' ' + attribute + '="' + String(value).replace(quot, '&quot;') + '"';
      return p1 + '>' + S + '</' + tag + '>';
    };

    module.exports = function (NAME, exec) {
      var O = {};
      O[NAME] = exec(createHTML);
      $export($export.P + $export.F * fails(function () {
        var test = ''[NAME]('"');
        return test !== test.toLowerCase() || test.split('"').length > 3;
      }), 'String', O);
    };
  }, {
    "55": 55,
    "60": 60,
    "62": 62
  }],
  130: [function (_dereq_, module, exports) {
    // https://github.com/tc39/proposal-string-pad-start-end
    var toLength = _dereq_(139);

    var repeat = _dereq_(131);

    var defined = _dereq_(55);

    module.exports = function (that, maxLength, fillString, left) {
      var S = String(defined(that));
      var stringLength = S.length;
      var fillStr = fillString === undefined ? ' ' : String(fillString);
      var intMaxLength = toLength(maxLength);
      if (intMaxLength <= stringLength || fillStr == '') return S;
      var fillLen = intMaxLength - stringLength;
      var stringFiller = repeat.call(fillStr, Math.ceil(fillLen / fillStr.length));
      if (stringFiller.length > fillLen) stringFiller = stringFiller.slice(0, fillLen);
      return left ? stringFiller + S : S + stringFiller;
    };
  }, {
    "131": 131,
    "139": 139,
    "55": 55
  }],
  131: [function (_dereq_, module, exports) {
    'use strict';

    var toInteger = _dereq_(137);

    var defined = _dereq_(55);

    module.exports = function repeat(count) {
      var str = String(defined(this));
      var res = '';
      var n = toInteger(count);
      if (n < 0 || n == Infinity) throw RangeError("Count can't be negative");

      for (; n > 0; (n >>>= 1) && (str += str)) {
        if (n & 1) res += str;
      }

      return res;
    };
  }, {
    "137": 137,
    "55": 55
  }],
  132: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    var defined = _dereq_(55);

    var fails = _dereq_(62);

    var spaces = _dereq_(133);

    var space = '[' + spaces + ']';
    var non = "\u200B\x85";
    var ltrim = RegExp('^' + space + space + '*');
    var rtrim = RegExp(space + space + '*$');

    var exporter = function exporter(KEY, exec, ALIAS) {
      var exp = {};
      var FORCE = fails(function () {
        return !!spaces[KEY]() || non[KEY]() != non;
      });
      var fn = exp[KEY] = FORCE ? exec(trim) : spaces[KEY];
      if (ALIAS) exp[ALIAS] = fn;
      $export($export.P + $export.F * FORCE, 'String', exp);
    }; // 1 -> String#trimLeft
    // 2 -> String#trimRight
    // 3 -> String#trim


    var trim = exporter.trim = function (string, TYPE) {
      string = String(defined(string));
      if (TYPE & 1) string = string.replace(ltrim, '');
      if (TYPE & 2) string = string.replace(rtrim, '');
      return string;
    };

    module.exports = exporter;
  }, {
    "133": 133,
    "55": 55,
    "60": 60,
    "62": 62
  }],
  133: [function (_dereq_, module, exports) {
    module.exports = "\t\n\x0B\f\r \xA0\u1680\u180E\u2000\u2001\u2002\u2003" + "\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF";
  }, {}],
  134: [function (_dereq_, module, exports) {
    var ctx = _dereq_(52);

    var invoke = _dereq_(74);

    var html = _dereq_(71);

    var cel = _dereq_(57);

    var global = _dereq_(68);

    var process = global.process;
    var setTask = global.setImmediate;
    var clearTask = global.clearImmediate;
    var MessageChannel = global.MessageChannel;
    var Dispatch = global.Dispatch;
    var counter = 0;
    var queue = {};
    var ONREADYSTATECHANGE = 'onreadystatechange';
    var defer, channel, port;

    var run = function run() {
      var id = +this; // eslint-disable-next-line no-prototype-builtins

      if (queue.hasOwnProperty(id)) {
        var fn = queue[id];
        delete queue[id];
        fn();
      }
    };

    var listener = function listener(event) {
      run.call(event.data);
    }; // Node.js 0.9+ & IE10+ has setImmediate, otherwise:


    if (!setTask || !clearTask) {
      setTask = function setImmediate(fn) {
        var args = [];
        var i = 1;

        while (arguments.length > i) {
          args.push(arguments[i++]);
        }

        queue[++counter] = function () {
          // eslint-disable-next-line no-new-func
          invoke(typeof fn == 'function' ? fn : Function(fn), args);
        };

        defer(counter);
        return counter;
      };

      clearTask = function clearImmediate(id) {
        delete queue[id];
      }; // Node.js 0.8-


      if (_dereq_(46)(process) == 'process') {
        defer = function defer(id) {
          process.nextTick(ctx(run, id, 1));
        }; // Sphere (JS game engine) Dispatch API

      } else if (Dispatch && Dispatch.now) {
        defer = function defer(id) {
          Dispatch.now(ctx(run, id, 1));
        }; // Browsers with MessageChannel, includes WebWorkers

      } else if (MessageChannel) {
        channel = new MessageChannel();
        port = channel.port2;
        channel.port1.onmessage = listener;
        defer = ctx(port.postMessage, port, 1); // Browsers with postMessage, skip WebWorkers
        // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
      } else if (global.addEventListener && typeof postMessage == 'function' && !global.importScripts) {
        defer = function defer(id) {
          global.postMessage(id + '', '*');
        };

        global.addEventListener('message', listener, false); // IE8-
      } else if (ONREADYSTATECHANGE in cel('script')) {
        defer = function defer(id) {
          html.appendChild(cel('script'))[ONREADYSTATECHANGE] = function () {
            html.removeChild(this);
            run.call(id);
          };
        }; // Rest old browsers

      } else {
        defer = function defer(id) {
          setTimeout(ctx(run, id, 1), 0);
        };
      }
    }

    module.exports = {
      set: setTask,
      clear: clearTask
    };
  }, {
    "46": 46,
    "52": 52,
    "57": 57,
    "68": 68,
    "71": 71,
    "74": 74
  }],
  135: [function (_dereq_, module, exports) {
    var toInteger = _dereq_(137);

    var max = Math.max;
    var min = Math.min;

    module.exports = function (index, length) {
      index = toInteger(index);
      return index < 0 ? max(index + length, 0) : min(index, length);
    };
  }, {
    "137": 137
  }],
  136: [function (_dereq_, module, exports) {
    // https://tc39.github.io/ecma262/#sec-toindex
    var toInteger = _dereq_(137);

    var toLength = _dereq_(139);

    module.exports = function (it) {
      if (it === undefined) return 0;
      var number = toInteger(it);
      var length = toLength(number);
      if (number !== length) throw RangeError('Wrong length!');
      return length;
    };
  }, {
    "137": 137,
    "139": 139
  }],
  137: [function (_dereq_, module, exports) {
    // 7.1.4 ToInteger
    var ceil = Math.ceil;
    var floor = Math.floor;

    module.exports = function (it) {
      return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
    };
  }, {}],
  138: [function (_dereq_, module, exports) {
    // to indexed object, toObject with fallback for non-array-like ES3 strings
    var IObject = _dereq_(75);

    var defined = _dereq_(55);

    module.exports = function (it) {
      return IObject(defined(it));
    };
  }, {
    "55": 55,
    "75": 75
  }],
  139: [function (_dereq_, module, exports) {
    // 7.1.15 ToLength
    var toInteger = _dereq_(137);

    var min = Math.min;

    module.exports = function (it) {
      return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
    };
  }, {
    "137": 137
  }],
  140: [function (_dereq_, module, exports) {
    // 7.1.13 ToObject(argument)
    var defined = _dereq_(55);

    module.exports = function (it) {
      return Object(defined(it));
    };
  }, {
    "55": 55
  }],
  141: [function (_dereq_, module, exports) {
    arguments[4][29][0].apply(exports, arguments);
  }, {
    "29": 29,
    "79": 79
  }],
  142: [function (_dereq_, module, exports) {
    'use strict';

    if (_dereq_(56)) {
      var LIBRARY = _dereq_(87);

      var global = _dereq_(68);

      var fails = _dereq_(62);

      var $export = _dereq_(60);

      var $typed = _dereq_(144);

      var $buffer = _dereq_(143);

      var ctx = _dereq_(52);

      var anInstance = _dereq_(35);

      var propertyDesc = _dereq_(114);

      var hide = _dereq_(70);

      var redefineAll = _dereq_(115);

      var toInteger = _dereq_(137);

      var toLength = _dereq_(139);

      var toIndex = _dereq_(136);

      var toAbsoluteIndex = _dereq_(135);

      var toPrimitive = _dereq_(141);

      var has = _dereq_(69);

      var classof = _dereq_(45);

      var isObject = _dereq_(79);

      var toObject = _dereq_(140);

      var isArrayIter = _dereq_(76);

      var create = _dereq_(96);

      var getPrototypeOf = _dereq_(103);

      var gOPN = _dereq_(101).f;

      var getIterFn = _dereq_(151);

      var uid = _dereq_(145);

      var wks = _dereq_(150);

      var createArrayMethod = _dereq_(40);

      var createArrayIncludes = _dereq_(39);

      var speciesConstructor = _dereq_(125);

      var ArrayIterators = _dereq_(162);

      var Iterators = _dereq_(86);

      var $iterDetect = _dereq_(84);

      var setSpecies = _dereq_(121);

      var arrayFill = _dereq_(38);

      var arrayCopyWithin = _dereq_(37);

      var $DP = _dereq_(97);

      var $GOPD = _dereq_(99);

      var dP = $DP.f;
      var gOPD = $GOPD.f;
      var RangeError = global.RangeError;
      var TypeError = global.TypeError;
      var Uint8Array = global.Uint8Array;
      var ARRAY_BUFFER = 'ArrayBuffer';
      var SHARED_BUFFER = 'Shared' + ARRAY_BUFFER;
      var BYTES_PER_ELEMENT = 'BYTES_PER_ELEMENT';
      var PROTOTYPE = 'prototype';
      var ArrayProto = Array[PROTOTYPE];
      var $ArrayBuffer = $buffer.ArrayBuffer;
      var $DataView = $buffer.DataView;
      var arrayForEach = createArrayMethod(0);
      var arrayFilter = createArrayMethod(2);
      var arraySome = createArrayMethod(3);
      var arrayEvery = createArrayMethod(4);
      var arrayFind = createArrayMethod(5);
      var arrayFindIndex = createArrayMethod(6);
      var arrayIncludes = createArrayIncludes(true);
      var arrayIndexOf = createArrayIncludes(false);
      var arrayValues = ArrayIterators.values;
      var arrayKeys = ArrayIterators.keys;
      var arrayEntries = ArrayIterators.entries;
      var arrayLastIndexOf = ArrayProto.lastIndexOf;
      var arrayReduce = ArrayProto.reduce;
      var arrayReduceRight = ArrayProto.reduceRight;
      var arrayJoin = ArrayProto.join;
      var arraySort = ArrayProto.sort;
      var arraySlice = ArrayProto.slice;
      var arrayToString = ArrayProto.toString;
      var arrayToLocaleString = ArrayProto.toLocaleString;
      var ITERATOR = wks('iterator');
      var TAG = wks('toStringTag');
      var TYPED_CONSTRUCTOR = uid('typed_constructor');
      var DEF_CONSTRUCTOR = uid('def_constructor');
      var ALL_CONSTRUCTORS = $typed.CONSTR;
      var TYPED_ARRAY = $typed.TYPED;
      var VIEW = $typed.VIEW;
      var WRONG_LENGTH = 'Wrong length!';
      var $map = createArrayMethod(1, function (O, length) {
        return allocate(speciesConstructor(O, O[DEF_CONSTRUCTOR]), length);
      });
      var LITTLE_ENDIAN = fails(function () {
        // eslint-disable-next-line no-undef
        return new Uint8Array(new Uint16Array([1]).buffer)[0] === 1;
      });
      var FORCED_SET = !!Uint8Array && !!Uint8Array[PROTOTYPE].set && fails(function () {
        new Uint8Array(1).set({});
      });

      var toOffset = function toOffset(it, BYTES) {
        var offset = toInteger(it);
        if (offset < 0 || offset % BYTES) throw RangeError('Wrong offset!');
        return offset;
      };

      var validate = function validate(it) {
        if (isObject(it) && TYPED_ARRAY in it) return it;
        throw TypeError(it + ' is not a typed array!');
      };

      var allocate = function allocate(C, length) {
        if (!(isObject(C) && TYPED_CONSTRUCTOR in C)) {
          throw TypeError('It is not a typed array constructor!');
        }

        return new C(length);
      };

      var speciesFromList = function speciesFromList(O, list) {
        return fromList(speciesConstructor(O, O[DEF_CONSTRUCTOR]), list);
      };

      var fromList = function fromList(C, list) {
        var index = 0;
        var length = list.length;
        var result = allocate(C, length);

        while (length > index) {
          result[index] = list[index++];
        }

        return result;
      };

      var addGetter = function addGetter(it, key, internal) {
        dP(it, key, {
          get: function get() {
            return this._d[internal];
          }
        });
      };

      var $from = function from(source
      /* , mapfn, thisArg */
      ) {
        var O = toObject(source);
        var aLen = arguments.length;
        var mapfn = aLen > 1 ? arguments[1] : undefined;
        var mapping = mapfn !== undefined;
        var iterFn = getIterFn(O);
        var i, length, values, result, step, iterator;

        if (iterFn != undefined && !isArrayIter(iterFn)) {
          for (iterator = iterFn.call(O), values = [], i = 0; !(step = iterator.next()).done; i++) {
            values.push(step.value);
          }

          O = values;
        }

        if (mapping && aLen > 2) mapfn = ctx(mapfn, arguments[2], 2);

        for (i = 0, length = toLength(O.length), result = allocate(this, length); length > i; i++) {
          result[i] = mapping ? mapfn(O[i], i) : O[i];
        }

        return result;
      };

      var $of = function of()
      /* ...items */
      {
        var index = 0;
        var length = arguments.length;
        var result = allocate(this, length);

        while (length > index) {
          result[index] = arguments[index++];
        }

        return result;
      }; // iOS Safari 6.x fails here


      var TO_LOCALE_BUG = !!Uint8Array && fails(function () {
        arrayToLocaleString.call(new Uint8Array(1));
      });

      var $toLocaleString = function toLocaleString() {
        return arrayToLocaleString.apply(TO_LOCALE_BUG ? arraySlice.call(validate(this)) : validate(this), arguments);
      };

      var proto = {
        copyWithin: function copyWithin(target, start
        /* , end */
        ) {
          return arrayCopyWithin.call(validate(this), target, start, arguments.length > 2 ? arguments[2] : undefined);
        },
        every: function every(callbackfn
        /* , thisArg */
        ) {
          return arrayEvery(validate(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
        },
        fill: function fill(value
        /* , start, end */
        ) {
          // eslint-disable-line no-unused-vars
          return arrayFill.apply(validate(this), arguments);
        },
        filter: function filter(callbackfn
        /* , thisArg */
        ) {
          return speciesFromList(this, arrayFilter(validate(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined));
        },
        find: function find(predicate
        /* , thisArg */
        ) {
          return arrayFind(validate(this), predicate, arguments.length > 1 ? arguments[1] : undefined);
        },
        findIndex: function findIndex(predicate
        /* , thisArg */
        ) {
          return arrayFindIndex(validate(this), predicate, arguments.length > 1 ? arguments[1] : undefined);
        },
        forEach: function forEach(callbackfn
        /* , thisArg */
        ) {
          arrayForEach(validate(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
        },
        indexOf: function indexOf(searchElement
        /* , fromIndex */
        ) {
          return arrayIndexOf(validate(this), searchElement, arguments.length > 1 ? arguments[1] : undefined);
        },
        includes: function includes(searchElement
        /* , fromIndex */
        ) {
          return arrayIncludes(validate(this), searchElement, arguments.length > 1 ? arguments[1] : undefined);
        },
        join: function join(separator) {
          // eslint-disable-line no-unused-vars
          return arrayJoin.apply(validate(this), arguments);
        },
        lastIndexOf: function lastIndexOf(searchElement
        /* , fromIndex */
        ) {
          // eslint-disable-line no-unused-vars
          return arrayLastIndexOf.apply(validate(this), arguments);
        },
        map: function map(mapfn
        /* , thisArg */
        ) {
          return $map(validate(this), mapfn, arguments.length > 1 ? arguments[1] : undefined);
        },
        reduce: function reduce(callbackfn
        /* , initialValue */
        ) {
          // eslint-disable-line no-unused-vars
          return arrayReduce.apply(validate(this), arguments);
        },
        reduceRight: function reduceRight(callbackfn
        /* , initialValue */
        ) {
          // eslint-disable-line no-unused-vars
          return arrayReduceRight.apply(validate(this), arguments);
        },
        reverse: function reverse() {
          var that = this;
          var length = validate(that).length;
          var middle = Math.floor(length / 2);
          var index = 0;
          var value;

          while (index < middle) {
            value = that[index];
            that[index++] = that[--length];
            that[length] = value;
          }

          return that;
        },
        some: function some(callbackfn
        /* , thisArg */
        ) {
          return arraySome(validate(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
        },
        sort: function sort(comparefn) {
          return arraySort.call(validate(this), comparefn);
        },
        subarray: function subarray(begin, end) {
          var O = validate(this);
          var length = O.length;
          var $begin = toAbsoluteIndex(begin, length);
          return new (speciesConstructor(O, O[DEF_CONSTRUCTOR]))(O.buffer, O.byteOffset + $begin * O.BYTES_PER_ELEMENT, toLength((end === undefined ? length : toAbsoluteIndex(end, length)) - $begin));
        }
      };

      var $slice = function slice(start, end) {
        return speciesFromList(this, arraySlice.call(validate(this), start, end));
      };

      var $set = function set(arrayLike
      /* , offset */
      ) {
        validate(this);
        var offset = toOffset(arguments[1], 1);
        var length = this.length;
        var src = toObject(arrayLike);
        var len = toLength(src.length);
        var index = 0;
        if (len + offset > length) throw RangeError(WRONG_LENGTH);

        while (index < len) {
          this[offset + index] = src[index++];
        }
      };

      var $iterators = {
        entries: function entries() {
          return arrayEntries.call(validate(this));
        },
        keys: function keys() {
          return arrayKeys.call(validate(this));
        },
        values: function values() {
          return arrayValues.call(validate(this));
        }
      };

      var isTAIndex = function isTAIndex(target, key) {
        return isObject(target) && target[TYPED_ARRAY] && _typeof(key) != 'symbol' && key in target && String(+key) == String(key);
      };

      var $getDesc = function getOwnPropertyDescriptor(target, key) {
        return isTAIndex(target, key = toPrimitive(key, true)) ? propertyDesc(2, target[key]) : gOPD(target, key);
      };

      var $setDesc = function defineProperty(target, key, desc) {
        if (isTAIndex(target, key = toPrimitive(key, true)) && isObject(desc) && has(desc, 'value') && !has(desc, 'get') && !has(desc, 'set') // TODO: add validation descriptor w/o calling accessors
        && !desc.configurable && (!has(desc, 'writable') || desc.writable) && (!has(desc, 'enumerable') || desc.enumerable)) {
          target[key] = desc.value;
          return target;
        }

        return dP(target, key, desc);
      };

      if (!ALL_CONSTRUCTORS) {
        $GOPD.f = $getDesc;
        $DP.f = $setDesc;
      }

      $export($export.S + $export.F * !ALL_CONSTRUCTORS, 'Object', {
        getOwnPropertyDescriptor: $getDesc,
        defineProperty: $setDesc
      });

      if (fails(function () {
        arrayToString.call({});
      })) {
        arrayToString = arrayToLocaleString = function toString() {
          return arrayJoin.call(this);
        };
      }

      var $TypedArrayPrototype$ = redefineAll({}, proto);
      redefineAll($TypedArrayPrototype$, $iterators);
      hide($TypedArrayPrototype$, ITERATOR, $iterators.values);
      redefineAll($TypedArrayPrototype$, {
        slice: $slice,
        set: $set,
        constructor: function constructor() {
          /* noop */
        },
        toString: arrayToString,
        toLocaleString: $toLocaleString
      });
      addGetter($TypedArrayPrototype$, 'buffer', 'b');
      addGetter($TypedArrayPrototype$, 'byteOffset', 'o');
      addGetter($TypedArrayPrototype$, 'byteLength', 'l');
      addGetter($TypedArrayPrototype$, 'length', 'e');
      dP($TypedArrayPrototype$, TAG, {
        get: function get() {
          return this[TYPED_ARRAY];
        }
      }); // eslint-disable-next-line max-statements

      module.exports = function (KEY, BYTES, wrapper, CLAMPED) {
        CLAMPED = !!CLAMPED;
        var NAME = KEY + (CLAMPED ? 'Clamped' : '') + 'Array';
        var GETTER = 'get' + KEY;
        var SETTER = 'set' + KEY;
        var TypedArray = global[NAME];
        var Base = TypedArray || {};
        var TAC = TypedArray && getPrototypeOf(TypedArray);
        var FORCED = !TypedArray || !$typed.ABV;
        var O = {};
        var TypedArrayPrototype = TypedArray && TypedArray[PROTOTYPE];

        var getter = function getter(that, index) {
          var data = that._d;
          return data.v[GETTER](index * BYTES + data.o, LITTLE_ENDIAN);
        };

        var setter = function setter(that, index, value) {
          var data = that._d;
          if (CLAMPED) value = (value = Math.round(value)) < 0 ? 0 : value > 0xff ? 0xff : value & 0xff;
          data.v[SETTER](index * BYTES + data.o, value, LITTLE_ENDIAN);
        };

        var addElement = function addElement(that, index) {
          dP(that, index, {
            get: function get() {
              return getter(this, index);
            },
            set: function set(value) {
              return setter(this, index, value);
            },
            enumerable: true
          });
        };

        if (FORCED) {
          TypedArray = wrapper(function (that, data, $offset, $length) {
            anInstance(that, TypedArray, NAME, '_d');
            var index = 0;
            var offset = 0;
            var buffer, byteLength, length, klass;

            if (!isObject(data)) {
              length = toIndex(data);
              byteLength = length * BYTES;
              buffer = new $ArrayBuffer(byteLength);
            } else if (data instanceof $ArrayBuffer || (klass = classof(data)) == ARRAY_BUFFER || klass == SHARED_BUFFER) {
              buffer = data;
              offset = toOffset($offset, BYTES);
              var $len = data.byteLength;

              if ($length === undefined) {
                if ($len % BYTES) throw RangeError(WRONG_LENGTH);
                byteLength = $len - offset;
                if (byteLength < 0) throw RangeError(WRONG_LENGTH);
              } else {
                byteLength = toLength($length) * BYTES;
                if (byteLength + offset > $len) throw RangeError(WRONG_LENGTH);
              }

              length = byteLength / BYTES;
            } else if (TYPED_ARRAY in data) {
              return fromList(TypedArray, data);
            } else {
              return $from.call(TypedArray, data);
            }

            hide(that, '_d', {
              b: buffer,
              o: offset,
              l: byteLength,
              e: length,
              v: new $DataView(buffer)
            });

            while (index < length) {
              addElement(that, index++);
            }
          });
          TypedArrayPrototype = TypedArray[PROTOTYPE] = create($TypedArrayPrototype$);
          hide(TypedArrayPrototype, 'constructor', TypedArray);
        } else if (!fails(function () {
          TypedArray(1);
        }) || !fails(function () {
          new TypedArray(-1); // eslint-disable-line no-new
        }) || !$iterDetect(function (iter) {
          new TypedArray(); // eslint-disable-line no-new

          new TypedArray(null); // eslint-disable-line no-new

          new TypedArray(1.5); // eslint-disable-line no-new

          new TypedArray(iter); // eslint-disable-line no-new
        }, true)) {
          TypedArray = wrapper(function (that, data, $offset, $length) {
            anInstance(that, TypedArray, NAME);
            var klass; // `ws` module bug, temporarily remove validation length for Uint8Array
            // https://github.com/websockets/ws/pull/645

            if (!isObject(data)) return new Base(toIndex(data));

            if (data instanceof $ArrayBuffer || (klass = classof(data)) == ARRAY_BUFFER || klass == SHARED_BUFFER) {
              return $length !== undefined ? new Base(data, toOffset($offset, BYTES), $length) : $offset !== undefined ? new Base(data, toOffset($offset, BYTES)) : new Base(data);
            }

            if (TYPED_ARRAY in data) return fromList(TypedArray, data);
            return $from.call(TypedArray, data);
          });
          arrayForEach(TAC !== Function.prototype ? gOPN(Base).concat(gOPN(TAC)) : gOPN(Base), function (key) {
            if (!(key in TypedArray)) hide(TypedArray, key, Base[key]);
          });
          TypedArray[PROTOTYPE] = TypedArrayPrototype;
          if (!LIBRARY) TypedArrayPrototype.constructor = TypedArray;
        }

        var $nativeIterator = TypedArrayPrototype[ITERATOR];
        var CORRECT_ITER_NAME = !!$nativeIterator && ($nativeIterator.name == 'values' || $nativeIterator.name == undefined);
        var $iterator = $iterators.values;
        hide(TypedArray, TYPED_CONSTRUCTOR, true);
        hide(TypedArrayPrototype, TYPED_ARRAY, NAME);
        hide(TypedArrayPrototype, VIEW, true);
        hide(TypedArrayPrototype, DEF_CONSTRUCTOR, TypedArray);

        if (CLAMPED ? new TypedArray(1)[TAG] != NAME : !(TAG in TypedArrayPrototype)) {
          dP(TypedArrayPrototype, TAG, {
            get: function get() {
              return NAME;
            }
          });
        }

        O[NAME] = TypedArray;
        $export($export.G + $export.W + $export.F * (TypedArray != Base), O);
        $export($export.S, NAME, {
          BYTES_PER_ELEMENT: BYTES
        });
        $export($export.S + $export.F * fails(function () {
          Base.of.call(TypedArray, 1);
        }), NAME, {
          from: $from,
          of: $of
        });
        if (!(BYTES_PER_ELEMENT in TypedArrayPrototype)) hide(TypedArrayPrototype, BYTES_PER_ELEMENT, BYTES);
        $export($export.P, NAME, proto);
        setSpecies(NAME);
        $export($export.P + $export.F * FORCED_SET, NAME, {
          set: $set
        });
        $export($export.P + $export.F * !CORRECT_ITER_NAME, NAME, $iterators);
        if (!LIBRARY && TypedArrayPrototype.toString != arrayToString) TypedArrayPrototype.toString = arrayToString;
        $export($export.P + $export.F * fails(function () {
          new TypedArray(1).slice();
        }), NAME, {
          slice: $slice
        });
        $export($export.P + $export.F * (fails(function () {
          return [1, 2].toLocaleString() != new TypedArray([1, 2]).toLocaleString();
        }) || !fails(function () {
          TypedArrayPrototype.toLocaleString.call([1, 2]);
        })), NAME, {
          toLocaleString: $toLocaleString
        });
        Iterators[NAME] = CORRECT_ITER_NAME ? $nativeIterator : $iterator;
        if (!LIBRARY && !CORRECT_ITER_NAME) hide(TypedArrayPrototype, ITERATOR, $iterator);
      };
    } else module.exports = function () {
      /* empty */
    };
  }, {
    "101": 101,
    "103": 103,
    "114": 114,
    "115": 115,
    "121": 121,
    "125": 125,
    "135": 135,
    "136": 136,
    "137": 137,
    "139": 139,
    "140": 140,
    "141": 141,
    "143": 143,
    "144": 144,
    "145": 145,
    "150": 150,
    "151": 151,
    "162": 162,
    "35": 35,
    "37": 37,
    "38": 38,
    "39": 39,
    "40": 40,
    "45": 45,
    "52": 52,
    "56": 56,
    "60": 60,
    "62": 62,
    "68": 68,
    "69": 69,
    "70": 70,
    "76": 76,
    "79": 79,
    "84": 84,
    "86": 86,
    "87": 87,
    "96": 96,
    "97": 97,
    "99": 99
  }],
  143: [function (_dereq_, module, exports) {
    'use strict';

    var global = _dereq_(68);

    var DESCRIPTORS = _dereq_(56);

    var LIBRARY = _dereq_(87);

    var $typed = _dereq_(144);

    var hide = _dereq_(70);

    var redefineAll = _dereq_(115);

    var fails = _dereq_(62);

    var anInstance = _dereq_(35);

    var toInteger = _dereq_(137);

    var toLength = _dereq_(139);

    var toIndex = _dereq_(136);

    var gOPN = _dereq_(101).f;

    var dP = _dereq_(97).f;

    var arrayFill = _dereq_(38);

    var setToStringTag = _dereq_(122);

    var ARRAY_BUFFER = 'ArrayBuffer';
    var DATA_VIEW = 'DataView';
    var PROTOTYPE = 'prototype';
    var WRONG_LENGTH = 'Wrong length!';
    var WRONG_INDEX = 'Wrong index!';
    var $ArrayBuffer = global[ARRAY_BUFFER];
    var $DataView = global[DATA_VIEW];
    var Math = global.Math;
    var RangeError = global.RangeError; // eslint-disable-next-line no-shadow-restricted-names

    var Infinity = global.Infinity;
    var BaseBuffer = $ArrayBuffer;
    var abs = Math.abs;
    var pow = Math.pow;
    var floor = Math.floor;
    var log = Math.log;
    var LN2 = Math.LN2;
    var BUFFER = 'buffer';
    var BYTE_LENGTH = 'byteLength';
    var BYTE_OFFSET = 'byteOffset';
    var $BUFFER = DESCRIPTORS ? '_b' : BUFFER;
    var $LENGTH = DESCRIPTORS ? '_l' : BYTE_LENGTH;
    var $OFFSET = DESCRIPTORS ? '_o' : BYTE_OFFSET; // IEEE754 conversions based on https://github.com/feross/ieee754

    function packIEEE754(value, mLen, nBytes) {
      var buffer = new Array(nBytes);
      var eLen = nBytes * 8 - mLen - 1;
      var eMax = (1 << eLen) - 1;
      var eBias = eMax >> 1;
      var rt = mLen === 23 ? pow(2, -24) - pow(2, -77) : 0;
      var i = 0;
      var s = value < 0 || value === 0 && 1 / value < 0 ? 1 : 0;
      var e, m, c;
      value = abs(value); // eslint-disable-next-line no-self-compare

      if (value != value || value === Infinity) {
        // eslint-disable-next-line no-self-compare
        m = value != value ? 1 : 0;
        e = eMax;
      } else {
        e = floor(log(value) / LN2);

        if (value * (c = pow(2, -e)) < 1) {
          e--;
          c *= 2;
        }

        if (e + eBias >= 1) {
          value += rt / c;
        } else {
          value += rt * pow(2, 1 - eBias);
        }

        if (value * c >= 2) {
          e++;
          c /= 2;
        }

        if (e + eBias >= eMax) {
          m = 0;
          e = eMax;
        } else if (e + eBias >= 1) {
          m = (value * c - 1) * pow(2, mLen);
          e = e + eBias;
        } else {
          m = value * pow(2, eBias - 1) * pow(2, mLen);
          e = 0;
        }
      }

      for (; mLen >= 8; buffer[i++] = m & 255, m /= 256, mLen -= 8) {
        ;
      }

      e = e << mLen | m;
      eLen += mLen;

      for (; eLen > 0; buffer[i++] = e & 255, e /= 256, eLen -= 8) {
        ;
      }

      buffer[--i] |= s * 128;
      return buffer;
    }

    function unpackIEEE754(buffer, mLen, nBytes) {
      var eLen = nBytes * 8 - mLen - 1;
      var eMax = (1 << eLen) - 1;
      var eBias = eMax >> 1;
      var nBits = eLen - 7;
      var i = nBytes - 1;
      var s = buffer[i--];
      var e = s & 127;
      var m;
      s >>= 7;

      for (; nBits > 0; e = e * 256 + buffer[i], i--, nBits -= 8) {
        ;
      }

      m = e & (1 << -nBits) - 1;
      e >>= -nBits;
      nBits += mLen;

      for (; nBits > 0; m = m * 256 + buffer[i], i--, nBits -= 8) {
        ;
      }

      if (e === 0) {
        e = 1 - eBias;
      } else if (e === eMax) {
        return m ? NaN : s ? -Infinity : Infinity;
      } else {
        m = m + pow(2, mLen);
        e = e - eBias;
      }

      return (s ? -1 : 1) * m * pow(2, e - mLen);
    }

    function unpackI32(bytes) {
      return bytes[3] << 24 | bytes[2] << 16 | bytes[1] << 8 | bytes[0];
    }

    function packI8(it) {
      return [it & 0xff];
    }

    function packI16(it) {
      return [it & 0xff, it >> 8 & 0xff];
    }

    function packI32(it) {
      return [it & 0xff, it >> 8 & 0xff, it >> 16 & 0xff, it >> 24 & 0xff];
    }

    function packF64(it) {
      return packIEEE754(it, 52, 8);
    }

    function packF32(it) {
      return packIEEE754(it, 23, 4);
    }

    function addGetter(C, key, internal) {
      dP(C[PROTOTYPE], key, {
        get: function get() {
          return this[internal];
        }
      });
    }

    function get(view, bytes, index, isLittleEndian) {
      var numIndex = +index;
      var intIndex = toIndex(numIndex);
      if (intIndex + bytes > view[$LENGTH]) throw RangeError(WRONG_INDEX);
      var store = view[$BUFFER]._b;
      var start = intIndex + view[$OFFSET];
      var pack = store.slice(start, start + bytes);
      return isLittleEndian ? pack : pack.reverse();
    }

    function set(view, bytes, index, conversion, value, isLittleEndian) {
      var numIndex = +index;
      var intIndex = toIndex(numIndex);
      if (intIndex + bytes > view[$LENGTH]) throw RangeError(WRONG_INDEX);
      var store = view[$BUFFER]._b;
      var start = intIndex + view[$OFFSET];
      var pack = conversion(+value);

      for (var i = 0; i < bytes; i++) {
        store[start + i] = pack[isLittleEndian ? i : bytes - i - 1];
      }
    }

    if (!$typed.ABV) {
      $ArrayBuffer = function ArrayBuffer(length) {
        anInstance(this, $ArrayBuffer, ARRAY_BUFFER);
        var byteLength = toIndex(length);
        this._b = arrayFill.call(new Array(byteLength), 0);
        this[$LENGTH] = byteLength;
      };

      $DataView = function DataView(buffer, byteOffset, byteLength) {
        anInstance(this, $DataView, DATA_VIEW);
        anInstance(buffer, $ArrayBuffer, DATA_VIEW);
        var bufferLength = buffer[$LENGTH];
        var offset = toInteger(byteOffset);
        if (offset < 0 || offset > bufferLength) throw RangeError('Wrong offset!');
        byteLength = byteLength === undefined ? bufferLength - offset : toLength(byteLength);
        if (offset + byteLength > bufferLength) throw RangeError(WRONG_LENGTH);
        this[$BUFFER] = buffer;
        this[$OFFSET] = offset;
        this[$LENGTH] = byteLength;
      };

      if (DESCRIPTORS) {
        addGetter($ArrayBuffer, BYTE_LENGTH, '_l');
        addGetter($DataView, BUFFER, '_b');
        addGetter($DataView, BYTE_LENGTH, '_l');
        addGetter($DataView, BYTE_OFFSET, '_o');
      }

      redefineAll($DataView[PROTOTYPE], {
        getInt8: function getInt8(byteOffset) {
          return get(this, 1, byteOffset)[0] << 24 >> 24;
        },
        getUint8: function getUint8(byteOffset) {
          return get(this, 1, byteOffset)[0];
        },
        getInt16: function getInt16(byteOffset
        /* , littleEndian */
        ) {
          var bytes = get(this, 2, byteOffset, arguments[1]);
          return (bytes[1] << 8 | bytes[0]) << 16 >> 16;
        },
        getUint16: function getUint16(byteOffset
        /* , littleEndian */
        ) {
          var bytes = get(this, 2, byteOffset, arguments[1]);
          return bytes[1] << 8 | bytes[0];
        },
        getInt32: function getInt32(byteOffset
        /* , littleEndian */
        ) {
          return unpackI32(get(this, 4, byteOffset, arguments[1]));
        },
        getUint32: function getUint32(byteOffset
        /* , littleEndian */
        ) {
          return unpackI32(get(this, 4, byteOffset, arguments[1])) >>> 0;
        },
        getFloat32: function getFloat32(byteOffset
        /* , littleEndian */
        ) {
          return unpackIEEE754(get(this, 4, byteOffset, arguments[1]), 23, 4);
        },
        getFloat64: function getFloat64(byteOffset
        /* , littleEndian */
        ) {
          return unpackIEEE754(get(this, 8, byteOffset, arguments[1]), 52, 8);
        },
        setInt8: function setInt8(byteOffset, value) {
          set(this, 1, byteOffset, packI8, value);
        },
        setUint8: function setUint8(byteOffset, value) {
          set(this, 1, byteOffset, packI8, value);
        },
        setInt16: function setInt16(byteOffset, value
        /* , littleEndian */
        ) {
          set(this, 2, byteOffset, packI16, value, arguments[2]);
        },
        setUint16: function setUint16(byteOffset, value
        /* , littleEndian */
        ) {
          set(this, 2, byteOffset, packI16, value, arguments[2]);
        },
        setInt32: function setInt32(byteOffset, value
        /* , littleEndian */
        ) {
          set(this, 4, byteOffset, packI32, value, arguments[2]);
        },
        setUint32: function setUint32(byteOffset, value
        /* , littleEndian */
        ) {
          set(this, 4, byteOffset, packI32, value, arguments[2]);
        },
        setFloat32: function setFloat32(byteOffset, value
        /* , littleEndian */
        ) {
          set(this, 4, byteOffset, packF32, value, arguments[2]);
        },
        setFloat64: function setFloat64(byteOffset, value
        /* , littleEndian */
        ) {
          set(this, 8, byteOffset, packF64, value, arguments[2]);
        }
      });
    } else {
      if (!fails(function () {
        $ArrayBuffer(1);
      }) || !fails(function () {
        new $ArrayBuffer(-1); // eslint-disable-line no-new
      }) || fails(function () {
        new $ArrayBuffer(); // eslint-disable-line no-new

        new $ArrayBuffer(1.5); // eslint-disable-line no-new

        new $ArrayBuffer(NaN); // eslint-disable-line no-new

        return $ArrayBuffer.name != ARRAY_BUFFER;
      })) {
        $ArrayBuffer = function ArrayBuffer(length) {
          anInstance(this, $ArrayBuffer);
          return new BaseBuffer(toIndex(length));
        };

        var ArrayBufferProto = $ArrayBuffer[PROTOTYPE] = BaseBuffer[PROTOTYPE];

        for (var keys = gOPN(BaseBuffer), j = 0, key; keys.length > j;) {
          if (!((key = keys[j++]) in $ArrayBuffer)) hide($ArrayBuffer, key, BaseBuffer[key]);
        }

        if (!LIBRARY) ArrayBufferProto.constructor = $ArrayBuffer;
      } // iOS Safari 7.x bug


      var view = new $DataView(new $ArrayBuffer(2));
      var $setInt8 = $DataView[PROTOTYPE].setInt8;
      view.setInt8(0, 2147483648);
      view.setInt8(1, 2147483649);
      if (view.getInt8(0) || !view.getInt8(1)) redefineAll($DataView[PROTOTYPE], {
        setInt8: function setInt8(byteOffset, value) {
          $setInt8.call(this, byteOffset, value << 24 >> 24);
        },
        setUint8: function setUint8(byteOffset, value) {
          $setInt8.call(this, byteOffset, value << 24 >> 24);
        }
      }, true);
    }

    setToStringTag($ArrayBuffer, ARRAY_BUFFER);
    setToStringTag($DataView, DATA_VIEW);
    hide($DataView[PROTOTYPE], $typed.VIEW, true);
    exports[ARRAY_BUFFER] = $ArrayBuffer;
    exports[DATA_VIEW] = $DataView;
  }, {
    "101": 101,
    "115": 115,
    "122": 122,
    "136": 136,
    "137": 137,
    "139": 139,
    "144": 144,
    "35": 35,
    "38": 38,
    "56": 56,
    "62": 62,
    "68": 68,
    "70": 70,
    "87": 87,
    "97": 97
  }],
  144: [function (_dereq_, module, exports) {
    var global = _dereq_(68);

    var hide = _dereq_(70);

    var uid = _dereq_(145);

    var TYPED = uid('typed_array');
    var VIEW = uid('view');
    var ABV = !!(global.ArrayBuffer && global.DataView);
    var CONSTR = ABV;
    var i = 0;
    var l = 9;
    var Typed;
    var TypedArrayConstructors = 'Int8Array,Uint8Array,Uint8ClampedArray,Int16Array,Uint16Array,Int32Array,Uint32Array,Float32Array,Float64Array'.split(',');

    while (i < l) {
      if (Typed = global[TypedArrayConstructors[i++]]) {
        hide(Typed.prototype, TYPED, true);
        hide(Typed.prototype, VIEW, true);
      } else CONSTR = false;
    }

    module.exports = {
      ABV: ABV,
      CONSTR: CONSTR,
      TYPED: TYPED,
      VIEW: VIEW
    };
  }, {
    "145": 145,
    "68": 68,
    "70": 70
  }],
  145: [function (_dereq_, module, exports) {
    var id = 0;
    var px = Math.random();

    module.exports = function (key) {
      return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
    };
  }, {}],
  146: [function (_dereq_, module, exports) {
    var global = _dereq_(68);

    var navigator = global.navigator;
    module.exports = navigator && navigator.userAgent || '';
  }, {
    "68": 68
  }],
  147: [function (_dereq_, module, exports) {
    var isObject = _dereq_(79);

    module.exports = function (it, TYPE) {
      if (!isObject(it) || it._t !== TYPE) throw TypeError('Incompatible receiver, ' + TYPE + ' required!');
      return it;
    };
  }, {
    "79": 79
  }],
  148: [function (_dereq_, module, exports) {
    var global = _dereq_(68);

    var core = _dereq_(50);

    var LIBRARY = _dereq_(87);

    var wksExt = _dereq_(149);

    var defineProperty = _dereq_(97).f;

    module.exports = function (name) {
      var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
      if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, {
        value: wksExt.f(name)
      });
    };
  }, {
    "149": 149,
    "50": 50,
    "68": 68,
    "87": 87,
    "97": 97
  }],
  149: [function (_dereq_, module, exports) {
    exports.f = _dereq_(150);
  }, {
    "150": 150
  }],
  150: [function (_dereq_, module, exports) {
    var store = _dereq_(124)('wks');

    var uid = _dereq_(145);

    var _Symbol = _dereq_(68).Symbol;

    var USE_SYMBOL = typeof _Symbol == 'function';

    var $exports = module.exports = function (name) {
      return store[name] || (store[name] = USE_SYMBOL && _Symbol[name] || (USE_SYMBOL ? _Symbol : uid)('Symbol.' + name));
    };

    $exports.store = store;
  }, {
    "124": 124,
    "145": 145,
    "68": 68
  }],
  151: [function (_dereq_, module, exports) {
    var classof = _dereq_(45);

    var ITERATOR = _dereq_(150)('iterator');

    var Iterators = _dereq_(86);

    module.exports = _dereq_(50).getIteratorMethod = function (it) {
      if (it != undefined) return it[ITERATOR] || it['@@iterator'] || Iterators[classof(it)];
    };
  }, {
    "150": 150,
    "45": 45,
    "50": 50,
    "86": 86
  }],
  152: [function (_dereq_, module, exports) {
    // 22.1.3.3 Array.prototype.copyWithin(target, start, end = this.length)
    var $export = _dereq_(60);

    $export($export.P, 'Array', {
      copyWithin: _dereq_(37)
    });

    _dereq_(33)('copyWithin');
  }, {
    "33": 33,
    "37": 37,
    "60": 60
  }],
  153: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $every = _dereq_(40)(4);

    $export($export.P + $export.F * !_dereq_(126)([].every, true), 'Array', {
      // 22.1.3.5 / 15.4.4.16 Array.prototype.every(callbackfn [, thisArg])
      every: function every(callbackfn
      /* , thisArg */
      ) {
        return $every(this, callbackfn, arguments[1]);
      }
    });
  }, {
    "126": 126,
    "40": 40,
    "60": 60
  }],
  154: [function (_dereq_, module, exports) {
    // 22.1.3.6 Array.prototype.fill(value, start = 0, end = this.length)
    var $export = _dereq_(60);

    $export($export.P, 'Array', {
      fill: _dereq_(38)
    });

    _dereq_(33)('fill');
  }, {
    "33": 33,
    "38": 38,
    "60": 60
  }],
  155: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $filter = _dereq_(40)(2);

    $export($export.P + $export.F * !_dereq_(126)([].filter, true), 'Array', {
      // 22.1.3.7 / 15.4.4.20 Array.prototype.filter(callbackfn [, thisArg])
      filter: function filter(callbackfn
      /* , thisArg */
      ) {
        return $filter(this, callbackfn, arguments[1]);
      }
    });
  }, {
    "126": 126,
    "40": 40,
    "60": 60
  }],
  156: [function (_dereq_, module, exports) {
    'use strict'; // 22.1.3.9 Array.prototype.findIndex(predicate, thisArg = undefined)

    var $export = _dereq_(60);

    var $find = _dereq_(40)(6);

    var KEY = 'findIndex';
    var forced = true; // Shouldn't skip holes

    if (KEY in []) Array(1)[KEY](function () {
      forced = false;
    });
    $export($export.P + $export.F * forced, 'Array', {
      findIndex: function findIndex(callbackfn
      /* , that = undefined */
      ) {
        return $find(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
      }
    });

    _dereq_(33)(KEY);
  }, {
    "33": 33,
    "40": 40,
    "60": 60
  }],
  157: [function (_dereq_, module, exports) {
    'use strict'; // 22.1.3.8 Array.prototype.find(predicate, thisArg = undefined)

    var $export = _dereq_(60);

    var $find = _dereq_(40)(5);

    var KEY = 'find';
    var forced = true; // Shouldn't skip holes

    if (KEY in []) Array(1)[KEY](function () {
      forced = false;
    });
    $export($export.P + $export.F * forced, 'Array', {
      find: function find(callbackfn
      /* , that = undefined */
      ) {
        return $find(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
      }
    });

    _dereq_(33)(KEY);
  }, {
    "33": 33,
    "40": 40,
    "60": 60
  }],
  158: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $forEach = _dereq_(40)(0);

    var STRICT = _dereq_(126)([].forEach, true);

    $export($export.P + $export.F * !STRICT, 'Array', {
      // 22.1.3.10 / 15.4.4.18 Array.prototype.forEach(callbackfn [, thisArg])
      forEach: function forEach(callbackfn
      /* , thisArg */
      ) {
        return $forEach(this, callbackfn, arguments[1]);
      }
    });
  }, {
    "126": 126,
    "40": 40,
    "60": 60
  }],
  159: [function (_dereq_, module, exports) {
    'use strict';

    var ctx = _dereq_(52);

    var $export = _dereq_(60);

    var toObject = _dereq_(140);

    var call = _dereq_(81);

    var isArrayIter = _dereq_(76);

    var toLength = _dereq_(139);

    var createProperty = _dereq_(51);

    var getIterFn = _dereq_(151);

    $export($export.S + $export.F * !_dereq_(84)(function (iter) {
      Array.from(iter);
    }), 'Array', {
      // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
      from: function from(arrayLike
      /* , mapfn = undefined, thisArg = undefined */
      ) {
        var O = toObject(arrayLike);
        var C = typeof this == 'function' ? this : Array;
        var aLen = arguments.length;
        var mapfn = aLen > 1 ? arguments[1] : undefined;
        var mapping = mapfn !== undefined;
        var index = 0;
        var iterFn = getIterFn(O);
        var length, result, step, iterator;
        if (mapping) mapfn = ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2); // if object isn't iterable or it's array with default iterator - use simple case

        if (iterFn != undefined && !(C == Array && isArrayIter(iterFn))) {
          for (iterator = iterFn.call(O), result = new C(); !(step = iterator.next()).done; index++) {
            createProperty(result, index, mapping ? call(iterator, mapfn, [step.value, index], true) : step.value);
          }
        } else {
          length = toLength(O.length);

          for (result = new C(length); length > index; index++) {
            createProperty(result, index, mapping ? mapfn(O[index], index) : O[index]);
          }
        }

        result.length = index;
        return result;
      }
    });
  }, {
    "139": 139,
    "140": 140,
    "151": 151,
    "51": 51,
    "52": 52,
    "60": 60,
    "76": 76,
    "81": 81,
    "84": 84
  }],
  160: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $indexOf = _dereq_(39)(false);

    var $native = [].indexOf;
    var NEGATIVE_ZERO = !!$native && 1 / [1].indexOf(1, -0) < 0;
    $export($export.P + $export.F * (NEGATIVE_ZERO || !_dereq_(126)($native)), 'Array', {
      // 22.1.3.11 / 15.4.4.14 Array.prototype.indexOf(searchElement [, fromIndex])
      indexOf: function indexOf(searchElement
      /* , fromIndex = 0 */
      ) {
        return NEGATIVE_ZERO // convert -0 to +0
        ? $native.apply(this, arguments) || 0 : $indexOf(this, searchElement, arguments[1]);
      }
    });
  }, {
    "126": 126,
    "39": 39,
    "60": 60
  }],
  161: [function (_dereq_, module, exports) {
    // 22.1.2.2 / 15.4.3.2 Array.isArray(arg)
    var $export = _dereq_(60);

    $export($export.S, 'Array', {
      isArray: _dereq_(77)
    });
  }, {
    "60": 60,
    "77": 77
  }],
  162: [function (_dereq_, module, exports) {
    'use strict';

    var addToUnscopables = _dereq_(33);

    var step = _dereq_(85);

    var Iterators = _dereq_(86);

    var toIObject = _dereq_(138); // 22.1.3.4 Array.prototype.entries()
    // 22.1.3.13 Array.prototype.keys()
    // 22.1.3.29 Array.prototype.values()
    // 22.1.3.30 Array.prototype[@@iterator]()


    module.exports = _dereq_(83)(Array, 'Array', function (iterated, kind) {
      this._t = toIObject(iterated); // target

      this._i = 0; // next index

      this._k = kind; // kind
      // 22.1.5.2.1 %ArrayIteratorPrototype%.next()
    }, function () {
      var O = this._t;
      var kind = this._k;
      var index = this._i++;

      if (!O || index >= O.length) {
        this._t = undefined;
        return step(1);
      }

      if (kind == 'keys') return step(0, index);
      if (kind == 'values') return step(0, O[index]);
      return step(0, [index, O[index]]);
    }, 'values'); // argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)

    Iterators.Arguments = Iterators.Array;
    addToUnscopables('keys');
    addToUnscopables('values');
    addToUnscopables('entries');
  }, {
    "138": 138,
    "33": 33,
    "83": 83,
    "85": 85,
    "86": 86
  }],
  163: [function (_dereq_, module, exports) {
    'use strict'; // 22.1.3.13 Array.prototype.join(separator)

    var $export = _dereq_(60);

    var toIObject = _dereq_(138);

    var arrayJoin = [].join; // fallback for not array-like strings

    $export($export.P + $export.F * (_dereq_(75) != Object || !_dereq_(126)(arrayJoin)), 'Array', {
      join: function join(separator) {
        return arrayJoin.call(toIObject(this), separator === undefined ? ',' : separator);
      }
    });
  }, {
    "126": 126,
    "138": 138,
    "60": 60,
    "75": 75
  }],
  164: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var toIObject = _dereq_(138);

    var toInteger = _dereq_(137);

    var toLength = _dereq_(139);

    var $native = [].lastIndexOf;
    var NEGATIVE_ZERO = !!$native && 1 / [1].lastIndexOf(1, -0) < 0;
    $export($export.P + $export.F * (NEGATIVE_ZERO || !_dereq_(126)($native)), 'Array', {
      // 22.1.3.14 / 15.4.4.15 Array.prototype.lastIndexOf(searchElement [, fromIndex])
      lastIndexOf: function lastIndexOf(searchElement
      /* , fromIndex = @[*-1] */
      ) {
        // convert -0 to +0
        if (NEGATIVE_ZERO) return $native.apply(this, arguments) || 0;
        var O = toIObject(this);
        var length = toLength(O.length);
        var index = length - 1;
        if (arguments.length > 1) index = Math.min(index, toInteger(arguments[1]));
        if (index < 0) index = length + index;

        for (; index >= 0; index--) {
          if (index in O) if (O[index] === searchElement) return index || 0;
        }

        return -1;
      }
    });
  }, {
    "126": 126,
    "137": 137,
    "138": 138,
    "139": 139,
    "60": 60
  }],
  165: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $map = _dereq_(40)(1);

    $export($export.P + $export.F * !_dereq_(126)([].map, true), 'Array', {
      // 22.1.3.15 / 15.4.4.19 Array.prototype.map(callbackfn [, thisArg])
      map: function map(callbackfn
      /* , thisArg */
      ) {
        return $map(this, callbackfn, arguments[1]);
      }
    });
  }, {
    "126": 126,
    "40": 40,
    "60": 60
  }],
  166: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var createProperty = _dereq_(51); // WebKit Array.of isn't generic


    $export($export.S + $export.F * _dereq_(62)(function () {
      function F() {
        /* empty */
      }

      return !(Array.of.call(F) instanceof F);
    }), 'Array', {
      // 22.1.2.3 Array.of( ...items)
      of: function of()
      /* ...args */
      {
        var index = 0;
        var aLen = arguments.length;
        var result = new (typeof this == 'function' ? this : Array)(aLen);

        while (aLen > index) {
          createProperty(result, index, arguments[index++]);
        }

        result.length = aLen;
        return result;
      }
    });
  }, {
    "51": 51,
    "60": 60,
    "62": 62
  }],
  167: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $reduce = _dereq_(41);

    $export($export.P + $export.F * !_dereq_(126)([].reduceRight, true), 'Array', {
      // 22.1.3.19 / 15.4.4.22 Array.prototype.reduceRight(callbackfn [, initialValue])
      reduceRight: function reduceRight(callbackfn
      /* , initialValue */
      ) {
        return $reduce(this, callbackfn, arguments.length, arguments[1], true);
      }
    });
  }, {
    "126": 126,
    "41": 41,
    "60": 60
  }],
  168: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $reduce = _dereq_(41);

    $export($export.P + $export.F * !_dereq_(126)([].reduce, true), 'Array', {
      // 22.1.3.18 / 15.4.4.21 Array.prototype.reduce(callbackfn [, initialValue])
      reduce: function reduce(callbackfn
      /* , initialValue */
      ) {
        return $reduce(this, callbackfn, arguments.length, arguments[1], false);
      }
    });
  }, {
    "126": 126,
    "41": 41,
    "60": 60
  }],
  169: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var html = _dereq_(71);

    var cof = _dereq_(46);

    var toAbsoluteIndex = _dereq_(135);

    var toLength = _dereq_(139);

    var arraySlice = [].slice; // fallback for not array-like ES3 strings and DOM objects

    $export($export.P + $export.F * _dereq_(62)(function () {
      if (html) arraySlice.call(html);
    }), 'Array', {
      slice: function slice(begin, end) {
        var len = toLength(this.length);
        var klass = cof(this);
        end = end === undefined ? len : end;
        if (klass == 'Array') return arraySlice.call(this, begin, end);
        var start = toAbsoluteIndex(begin, len);
        var upTo = toAbsoluteIndex(end, len);
        var size = toLength(upTo - start);
        var cloned = new Array(size);
        var i = 0;

        for (; i < size; i++) {
          cloned[i] = klass == 'String' ? this.charAt(start + i) : this[start + i];
        }

        return cloned;
      }
    });
  }, {
    "135": 135,
    "139": 139,
    "46": 46,
    "60": 60,
    "62": 62,
    "71": 71
  }],
  170: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $some = _dereq_(40)(3);

    $export($export.P + $export.F * !_dereq_(126)([].some, true), 'Array', {
      // 22.1.3.23 / 15.4.4.17 Array.prototype.some(callbackfn [, thisArg])
      some: function some(callbackfn
      /* , thisArg */
      ) {
        return $some(this, callbackfn, arguments[1]);
      }
    });
  }, {
    "126": 126,
    "40": 40,
    "60": 60
  }],
  171: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var aFunction = _dereq_(31);

    var toObject = _dereq_(140);

    var fails = _dereq_(62);

    var $sort = [].sort;
    var test = [1, 2, 3];
    $export($export.P + $export.F * (fails(function () {
      // IE8-
      test.sort(undefined);
    }) || !fails(function () {
      // V8 bug
      test.sort(null); // Old WebKit
    }) || !_dereq_(126)($sort)), 'Array', {
      // 22.1.3.25 Array.prototype.sort(comparefn)
      sort: function sort(comparefn) {
        return comparefn === undefined ? $sort.call(toObject(this)) : $sort.call(toObject(this), aFunction(comparefn));
      }
    });
  }, {
    "126": 126,
    "140": 140,
    "31": 31,
    "60": 60,
    "62": 62
  }],
  172: [function (_dereq_, module, exports) {
    _dereq_(121)('Array');
  }, {
    "121": 121
  }],
  173: [function (_dereq_, module, exports) {
    // 20.3.3.1 / 15.9.4.4 Date.now()
    var $export = _dereq_(60);

    $export($export.S, 'Date', {
      now: function now() {
        return new Date().getTime();
      }
    });
  }, {
    "60": 60
  }],
  174: [function (_dereq_, module, exports) {
    // 20.3.4.36 / 15.9.5.43 Date.prototype.toISOString()
    var $export = _dereq_(60);

    var toISOString = _dereq_(53); // PhantomJS / old WebKit has a broken implementations


    $export($export.P + $export.F * (Date.prototype.toISOString !== toISOString), 'Date', {
      toISOString: toISOString
    });
  }, {
    "53": 53,
    "60": 60
  }],
  175: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var toObject = _dereq_(140);

    var toPrimitive = _dereq_(141);

    $export($export.P + $export.F * _dereq_(62)(function () {
      return new Date(NaN).toJSON() !== null || Date.prototype.toJSON.call({
        toISOString: function toISOString() {
          return 1;
        }
      }) !== 1;
    }), 'Date', {
      // eslint-disable-next-line no-unused-vars
      toJSON: function toJSON(key) {
        var O = toObject(this);
        var pv = toPrimitive(O);
        return typeof pv == 'number' && !isFinite(pv) ? null : O.toISOString();
      }
    });
  }, {
    "140": 140,
    "141": 141,
    "60": 60,
    "62": 62
  }],
  176: [function (_dereq_, module, exports) {
    var TO_PRIMITIVE = _dereq_(150)('toPrimitive');

    var proto = Date.prototype;
    if (!(TO_PRIMITIVE in proto)) _dereq_(70)(proto, TO_PRIMITIVE, _dereq_(54));
  }, {
    "150": 150,
    "54": 54,
    "70": 70
  }],
  177: [function (_dereq_, module, exports) {
    var DateProto = Date.prototype;
    var INVALID_DATE = 'Invalid Date';
    var TO_STRING = 'toString';
    var $toString = DateProto[TO_STRING];
    var getTime = DateProto.getTime;

    if (new Date(NaN) + '' != INVALID_DATE) {
      _dereq_(116)(DateProto, TO_STRING, function toString() {
        var value = getTime.call(this); // eslint-disable-next-line no-self-compare

        return value === value ? $toString.call(this) : INVALID_DATE;
      });
    }
  }, {
    "116": 116
  }],
  178: [function (_dereq_, module, exports) {
    // 19.2.3.2 / 15.3.4.5 Function.prototype.bind(thisArg, args...)
    var $export = _dereq_(60);

    $export($export.P, 'Function', {
      bind: _dereq_(44)
    });
  }, {
    "44": 44,
    "60": 60
  }],
  179: [function (_dereq_, module, exports) {
    'use strict';

    var isObject = _dereq_(79);

    var getPrototypeOf = _dereq_(103);

    var HAS_INSTANCE = _dereq_(150)('hasInstance');

    var FunctionProto = Function.prototype; // 19.2.3.6 Function.prototype[@@hasInstance](V)

    if (!(HAS_INSTANCE in FunctionProto)) _dereq_(97).f(FunctionProto, HAS_INSTANCE, {
      value: function value(O) {
        if (typeof this != 'function' || !isObject(O)) return false;
        if (!isObject(this.prototype)) return O instanceof this; // for environment w/o native `@@hasInstance` logic enough `instanceof`, but add this:

        while (O = getPrototypeOf(O)) {
          if (this.prototype === O) return true;
        }

        return false;
      }
    });
  }, {
    "103": 103,
    "150": 150,
    "79": 79,
    "97": 97
  }],
  180: [function (_dereq_, module, exports) {
    var dP = _dereq_(97).f;

    var FProto = Function.prototype;
    var nameRE = /^\s*function ([^ (]*)/;
    var NAME = 'name'; // 19.2.4.2 name

    NAME in FProto || _dereq_(56) && dP(FProto, NAME, {
      configurable: true,
      get: function get() {
        try {
          return ('' + this).match(nameRE)[1];
        } catch (e) {
          return '';
        }
      }
    });
  }, {
    "56": 56,
    "97": 97
  }],
  181: [function (_dereq_, module, exports) {
    'use strict';

    var strong = _dereq_(47);

    var validate = _dereq_(147);

    var MAP = 'Map'; // 23.1 Map Objects

    module.exports = _dereq_(49)(MAP, function (get) {
      return function Map() {
        return get(this, arguments.length > 0 ? arguments[0] : undefined);
      };
    }, {
      // 23.1.3.6 Map.prototype.get(key)
      get: function get(key) {
        var entry = strong.getEntry(validate(this, MAP), key);
        return entry && entry.v;
      },
      // 23.1.3.9 Map.prototype.set(key, value)
      set: function set(key, value) {
        return strong.def(validate(this, MAP), key === 0 ? 0 : key, value);
      }
    }, strong, true);
  }, {
    "147": 147,
    "47": 47,
    "49": 49
  }],
  182: [function (_dereq_, module, exports) {
    // 20.2.2.3 Math.acosh(x)
    var $export = _dereq_(60);

    var log1p = _dereq_(90);

    var sqrt = Math.sqrt;
    var $acosh = Math.acosh;
    $export($export.S + $export.F * !($acosh // V8 bug: https://code.google.com/p/v8/issues/detail?id=3509
    && Math.floor($acosh(Number.MAX_VALUE)) == 710 // Tor Browser bug: Math.acosh(Infinity) -> NaN
    && $acosh(Infinity) == Infinity), 'Math', {
      acosh: function acosh(x) {
        return (x = +x) < 1 ? NaN : x > 94906265.62425156 ? Math.log(x) + Math.LN2 : log1p(x - 1 + sqrt(x - 1) * sqrt(x + 1));
      }
    });
  }, {
    "60": 60,
    "90": 90
  }],
  183: [function (_dereq_, module, exports) {
    // 20.2.2.5 Math.asinh(x)
    var $export = _dereq_(60);

    var $asinh = Math.asinh;

    function asinh(x) {
      return !isFinite(x = +x) || x == 0 ? x : x < 0 ? -asinh(-x) : Math.log(x + Math.sqrt(x * x + 1));
    } // Tor Browser bug: Math.asinh(0) -> -0


    $export($export.S + $export.F * !($asinh && 1 / $asinh(0) > 0), 'Math', {
      asinh: asinh
    });
  }, {
    "60": 60
  }],
  184: [function (_dereq_, module, exports) {
    // 20.2.2.7 Math.atanh(x)
    var $export = _dereq_(60);

    var $atanh = Math.atanh; // Tor Browser bug: Math.atanh(-0) -> 0

    $export($export.S + $export.F * !($atanh && 1 / $atanh(-0) < 0), 'Math', {
      atanh: function atanh(x) {
        return (x = +x) == 0 ? x : Math.log((1 + x) / (1 - x)) / 2;
      }
    });
  }, {
    "60": 60
  }],
  185: [function (_dereq_, module, exports) {
    // 20.2.2.9 Math.cbrt(x)
    var $export = _dereq_(60);

    var sign = _dereq_(91);

    $export($export.S, 'Math', {
      cbrt: function cbrt(x) {
        return sign(x = +x) * Math.pow(Math.abs(x), 1 / 3);
      }
    });
  }, {
    "60": 60,
    "91": 91
  }],
  186: [function (_dereq_, module, exports) {
    // 20.2.2.11 Math.clz32(x)
    var $export = _dereq_(60);

    $export($export.S, 'Math', {
      clz32: function clz32(x) {
        return (x >>>= 0) ? 31 - Math.floor(Math.log(x + 0.5) * Math.LOG2E) : 32;
      }
    });
  }, {
    "60": 60
  }],
  187: [function (_dereq_, module, exports) {
    // 20.2.2.12 Math.cosh(x)
    var $export = _dereq_(60);

    var exp = Math.exp;
    $export($export.S, 'Math', {
      cosh: function cosh(x) {
        return (exp(x = +x) + exp(-x)) / 2;
      }
    });
  }, {
    "60": 60
  }],
  188: [function (_dereq_, module, exports) {
    // 20.2.2.14 Math.expm1(x)
    var $export = _dereq_(60);

    var $expm1 = _dereq_(88);

    $export($export.S + $export.F * ($expm1 != Math.expm1), 'Math', {
      expm1: $expm1
    });
  }, {
    "60": 60,
    "88": 88
  }],
  189: [function (_dereq_, module, exports) {
    // 20.2.2.16 Math.fround(x)
    var $export = _dereq_(60);

    $export($export.S, 'Math', {
      fround: _dereq_(89)
    });
  }, {
    "60": 60,
    "89": 89
  }],
  190: [function (_dereq_, module, exports) {
    // 20.2.2.17 Math.hypot([value1[, value2[, … ]]])
    var $export = _dereq_(60);

    var abs = Math.abs;
    $export($export.S, 'Math', {
      hypot: function hypot(value1, value2) {
        // eslint-disable-line no-unused-vars
        var sum = 0;
        var i = 0;
        var aLen = arguments.length;
        var larg = 0;
        var arg, div;

        while (i < aLen) {
          arg = abs(arguments[i++]);

          if (larg < arg) {
            div = larg / arg;
            sum = sum * div * div + 1;
            larg = arg;
          } else if (arg > 0) {
            div = arg / larg;
            sum += div * div;
          } else sum += arg;
        }

        return larg === Infinity ? Infinity : larg * Math.sqrt(sum);
      }
    });
  }, {
    "60": 60
  }],
  191: [function (_dereq_, module, exports) {
    // 20.2.2.18 Math.imul(x, y)
    var $export = _dereq_(60);

    var $imul = Math.imul; // some WebKit versions fails with big numbers, some has wrong arity

    $export($export.S + $export.F * _dereq_(62)(function () {
      return $imul(0xffffffff, 5) != -5 || $imul.length != 2;
    }), 'Math', {
      imul: function imul(x, y) {
        var UINT16 = 0xffff;
        var xn = +x;
        var yn = +y;
        var xl = UINT16 & xn;
        var yl = UINT16 & yn;
        return 0 | xl * yl + ((UINT16 & xn >>> 16) * yl + xl * (UINT16 & yn >>> 16) << 16 >>> 0);
      }
    });
  }, {
    "60": 60,
    "62": 62
  }],
  192: [function (_dereq_, module, exports) {
    // 20.2.2.21 Math.log10(x)
    var $export = _dereq_(60);

    $export($export.S, 'Math', {
      log10: function log10(x) {
        return Math.log(x) * Math.LOG10E;
      }
    });
  }, {
    "60": 60
  }],
  193: [function (_dereq_, module, exports) {
    // 20.2.2.20 Math.log1p(x)
    var $export = _dereq_(60);

    $export($export.S, 'Math', {
      log1p: _dereq_(90)
    });
  }, {
    "60": 60,
    "90": 90
  }],
  194: [function (_dereq_, module, exports) {
    // 20.2.2.22 Math.log2(x)
    var $export = _dereq_(60);

    $export($export.S, 'Math', {
      log2: function log2(x) {
        return Math.log(x) / Math.LN2;
      }
    });
  }, {
    "60": 60
  }],
  195: [function (_dereq_, module, exports) {
    // 20.2.2.28 Math.sign(x)
    var $export = _dereq_(60);

    $export($export.S, 'Math', {
      sign: _dereq_(91)
    });
  }, {
    "60": 60,
    "91": 91
  }],
  196: [function (_dereq_, module, exports) {
    // 20.2.2.30 Math.sinh(x)
    var $export = _dereq_(60);

    var expm1 = _dereq_(88);

    var exp = Math.exp; // V8 near Chromium 38 has a problem with very small numbers

    $export($export.S + $export.F * _dereq_(62)(function () {
      return !Math.sinh(-2e-17) != -2e-17;
    }), 'Math', {
      sinh: function sinh(x) {
        return Math.abs(x = +x) < 1 ? (expm1(x) - expm1(-x)) / 2 : (exp(x - 1) - exp(-x - 1)) * (Math.E / 2);
      }
    });
  }, {
    "60": 60,
    "62": 62,
    "88": 88
  }],
  197: [function (_dereq_, module, exports) {
    // 20.2.2.33 Math.tanh(x)
    var $export = _dereq_(60);

    var expm1 = _dereq_(88);

    var exp = Math.exp;
    $export($export.S, 'Math', {
      tanh: function tanh(x) {
        var a = expm1(x = +x);
        var b = expm1(-x);
        return a == Infinity ? 1 : b == Infinity ? -1 : (a - b) / (exp(x) + exp(-x));
      }
    });
  }, {
    "60": 60,
    "88": 88
  }],
  198: [function (_dereq_, module, exports) {
    // 20.2.2.34 Math.trunc(x)
    var $export = _dereq_(60);

    $export($export.S, 'Math', {
      trunc: function trunc(it) {
        return (it > 0 ? Math.floor : Math.ceil)(it);
      }
    });
  }, {
    "60": 60
  }],
  199: [function (_dereq_, module, exports) {
    'use strict';

    var global = _dereq_(68);

    var has = _dereq_(69);

    var cof = _dereq_(46);

    var inheritIfRequired = _dereq_(73);

    var toPrimitive = _dereq_(141);

    var fails = _dereq_(62);

    var gOPN = _dereq_(101).f;

    var gOPD = _dereq_(99).f;

    var dP = _dereq_(97).f;

    var $trim = _dereq_(132).trim;

    var NUMBER = 'Number';
    var $Number = global[NUMBER];
    var Base = $Number;
    var proto = $Number.prototype; // Opera ~12 has broken Object#toString

    var BROKEN_COF = cof(_dereq_(96)(proto)) == NUMBER;
    var TRIM = ('trim' in String.prototype); // 7.1.3 ToNumber(argument)

    var toNumber = function toNumber(argument) {
      var it = toPrimitive(argument, false);

      if (typeof it == 'string' && it.length > 2) {
        it = TRIM ? it.trim() : $trim(it, 3);
        var first = it.charCodeAt(0);
        var third, radix, maxCode;

        if (first === 43 || first === 45) {
          third = it.charCodeAt(2);
          if (third === 88 || third === 120) return NaN; // Number('+0x1') should be NaN, old V8 fix
        } else if (first === 48) {
          switch (it.charCodeAt(1)) {
            case 66:
            case 98:
              radix = 2;
              maxCode = 49;
              break;
            // fast equal /^0b[01]+$/i

            case 79:
            case 111:
              radix = 8;
              maxCode = 55;
              break;
            // fast equal /^0o[0-7]+$/i

            default:
              return +it;
          }

          for (var digits = it.slice(2), i = 0, l = digits.length, code; i < l; i++) {
            code = digits.charCodeAt(i); // parseInt parses a string to a first unavailable symbol
            // but ToNumber should return NaN if a string contains unavailable symbols

            if (code < 48 || code > maxCode) return NaN;
          }

          return parseInt(digits, radix);
        }
      }

      return +it;
    };

    if (!$Number(' 0o1') || !$Number('0b1') || $Number('+0x1')) {
      $Number = function Number(value) {
        var it = arguments.length < 1 ? 0 : value;
        var that = this;
        return that instanceof $Number // check on 1..constructor(foo) case
        && (BROKEN_COF ? fails(function () {
          proto.valueOf.call(that);
        }) : cof(that) != NUMBER) ? inheritIfRequired(new Base(toNumber(it)), that, $Number) : toNumber(it);
      };

      for (var keys = _dereq_(56) ? gOPN(Base) : ( // ES3:
      'MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,' + // ES6 (in case, if modules with ES6 Number statics required before):
      'EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,' + 'MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger').split(','), j = 0, key; keys.length > j; j++) {
        if (has(Base, key = keys[j]) && !has($Number, key)) {
          dP($Number, key, gOPD(Base, key));
        }
      }

      $Number.prototype = proto;
      proto.constructor = $Number;

      _dereq_(116)(global, NUMBER, $Number);
    }
  }, {
    "101": 101,
    "116": 116,
    "132": 132,
    "141": 141,
    "46": 46,
    "56": 56,
    "62": 62,
    "68": 68,
    "69": 69,
    "73": 73,
    "96": 96,
    "97": 97,
    "99": 99
  }],
  200: [function (_dereq_, module, exports) {
    // 20.1.2.1 Number.EPSILON
    var $export = _dereq_(60);

    $export($export.S, 'Number', {
      EPSILON: Math.pow(2, -52)
    });
  }, {
    "60": 60
  }],
  201: [function (_dereq_, module, exports) {
    // 20.1.2.2 Number.isFinite(number)
    var $export = _dereq_(60);

    var _isFinite = _dereq_(68).isFinite;

    $export($export.S, 'Number', {
      isFinite: function isFinite(it) {
        return typeof it == 'number' && _isFinite(it);
      }
    });
  }, {
    "60": 60,
    "68": 68
  }],
  202: [function (_dereq_, module, exports) {
    // 20.1.2.3 Number.isInteger(number)
    var $export = _dereq_(60);

    $export($export.S, 'Number', {
      isInteger: _dereq_(78)
    });
  }, {
    "60": 60,
    "78": 78
  }],
  203: [function (_dereq_, module, exports) {
    // 20.1.2.4 Number.isNaN(number)
    var $export = _dereq_(60);

    $export($export.S, 'Number', {
      isNaN: function isNaN(number) {
        // eslint-disable-next-line no-self-compare
        return number != number;
      }
    });
  }, {
    "60": 60
  }],
  204: [function (_dereq_, module, exports) {
    // 20.1.2.5 Number.isSafeInteger(number)
    var $export = _dereq_(60);

    var isInteger = _dereq_(78);

    var abs = Math.abs;
    $export($export.S, 'Number', {
      isSafeInteger: function isSafeInteger(number) {
        return isInteger(number) && abs(number) <= 0x1fffffffffffff;
      }
    });
  }, {
    "60": 60,
    "78": 78
  }],
  205: [function (_dereq_, module, exports) {
    // 20.1.2.6 Number.MAX_SAFE_INTEGER
    var $export = _dereq_(60);

    $export($export.S, 'Number', {
      MAX_SAFE_INTEGER: 0x1fffffffffffff
    });
  }, {
    "60": 60
  }],
  206: [function (_dereq_, module, exports) {
    // 20.1.2.10 Number.MIN_SAFE_INTEGER
    var $export = _dereq_(60);

    $export($export.S, 'Number', {
      MIN_SAFE_INTEGER: -0x1fffffffffffff
    });
  }, {
    "60": 60
  }],
  207: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    var $parseFloat = _dereq_(110); // 20.1.2.12 Number.parseFloat(string)


    $export($export.S + $export.F * (Number.parseFloat != $parseFloat), 'Number', {
      parseFloat: $parseFloat
    });
  }, {
    "110": 110,
    "60": 60
  }],
  208: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    var $parseInt = _dereq_(111); // 20.1.2.13 Number.parseInt(string, radix)


    $export($export.S + $export.F * (Number.parseInt != $parseInt), 'Number', {
      parseInt: $parseInt
    });
  }, {
    "111": 111,
    "60": 60
  }],
  209: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var toInteger = _dereq_(137);

    var aNumberValue = _dereq_(32);

    var repeat = _dereq_(131);

    var $toFixed = 1.0.toFixed;
    var floor = Math.floor;
    var data = [0, 0, 0, 0, 0, 0];
    var ERROR = 'Number.toFixed: incorrect invocation!';
    var ZERO = '0';

    var multiply = function multiply(n, c) {
      var i = -1;
      var c2 = c;

      while (++i < 6) {
        c2 += n * data[i];
        data[i] = c2 % 1e7;
        c2 = floor(c2 / 1e7);
      }
    };

    var divide = function divide(n) {
      var i = 6;
      var c = 0;

      while (--i >= 0) {
        c += data[i];
        data[i] = floor(c / n);
        c = c % n * 1e7;
      }
    };

    var numToString = function numToString() {
      var i = 6;
      var s = '';

      while (--i >= 0) {
        if (s !== '' || i === 0 || data[i] !== 0) {
          var t = String(data[i]);
          s = s === '' ? t : s + repeat.call(ZERO, 7 - t.length) + t;
        }
      }

      return s;
    };

    var pow = function pow(x, n, acc) {
      return n === 0 ? acc : n % 2 === 1 ? pow(x, n - 1, acc * x) : pow(x * x, n / 2, acc);
    };

    var log = function log(x) {
      var n = 0;
      var x2 = x;

      while (x2 >= 4096) {
        n += 12;
        x2 /= 4096;
      }

      while (x2 >= 2) {
        n += 1;
        x2 /= 2;
      }

      return n;
    };

    $export($export.P + $export.F * (!!$toFixed && (0.00008.toFixed(3) !== '0.000' || 0.9.toFixed(0) !== '1' || 1.255.toFixed(2) !== '1.25' || 1000000000000000128.0.toFixed(0) !== '1000000000000000128') || !_dereq_(62)(function () {
      // V8 ~ Android 4.3-
      $toFixed.call({});
    })), 'Number', {
      toFixed: function toFixed(fractionDigits) {
        var x = aNumberValue(this, ERROR);
        var f = toInteger(fractionDigits);
        var s = '';
        var m = ZERO;
        var e, z, j, k;
        if (f < 0 || f > 20) throw RangeError(ERROR); // eslint-disable-next-line no-self-compare

        if (x != x) return 'NaN';
        if (x <= -1e21 || x >= 1e21) return String(x);

        if (x < 0) {
          s = '-';
          x = -x;
        }

        if (x > 1e-21) {
          e = log(x * pow(2, 69, 1)) - 69;
          z = e < 0 ? x * pow(2, -e, 1) : x / pow(2, e, 1);
          z *= 0x10000000000000;
          e = 52 - e;

          if (e > 0) {
            multiply(0, z);
            j = f;

            while (j >= 7) {
              multiply(1e7, 0);
              j -= 7;
            }

            multiply(pow(10, j, 1), 0);
            j = e - 1;

            while (j >= 23) {
              divide(1 << 23);
              j -= 23;
            }

            divide(1 << j);
            multiply(1, 1);
            divide(2);
            m = numToString();
          } else {
            multiply(0, z);
            multiply(1 << -e, 0);
            m = numToString() + repeat.call(ZERO, f);
          }
        }

        if (f > 0) {
          k = m.length;
          m = s + (k <= f ? '0.' + repeat.call(ZERO, f - k) + m : m.slice(0, k - f) + '.' + m.slice(k - f));
        } else {
          m = s + m;
        }

        return m;
      }
    });
  }, {
    "131": 131,
    "137": 137,
    "32": 32,
    "60": 60,
    "62": 62
  }],
  210: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $fails = _dereq_(62);

    var aNumberValue = _dereq_(32);

    var $toPrecision = 1.0.toPrecision;
    $export($export.P + $export.F * ($fails(function () {
      // IE7-
      return $toPrecision.call(1, undefined) !== '1';
    }) || !$fails(function () {
      // V8 ~ Android 4.3-
      $toPrecision.call({});
    })), 'Number', {
      toPrecision: function toPrecision(precision) {
        var that = aNumberValue(this, 'Number#toPrecision: incorrect invocation!');
        return precision === undefined ? $toPrecision.call(that) : $toPrecision.call(that, precision);
      }
    });
  }, {
    "32": 32,
    "60": 60,
    "62": 62
  }],
  211: [function (_dereq_, module, exports) {
    // 19.1.3.1 Object.assign(target, source)
    var $export = _dereq_(60);

    $export($export.S + $export.F, 'Object', {
      assign: _dereq_(95)
    });
  }, {
    "60": 60,
    "95": 95
  }],
  212: [function (_dereq_, module, exports) {
    var $export = _dereq_(60); // 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])


    $export($export.S, 'Object', {
      create: _dereq_(96)
    });
  }, {
    "60": 60,
    "96": 96
  }],
  213: [function (_dereq_, module, exports) {
    var $export = _dereq_(60); // 19.1.2.3 / 15.2.3.7 Object.defineProperties(O, Properties)


    $export($export.S + $export.F * !_dereq_(56), 'Object', {
      defineProperties: _dereq_(98)
    });
  }, {
    "56": 56,
    "60": 60,
    "98": 98
  }],
  214: [function (_dereq_, module, exports) {
    var $export = _dereq_(60); // 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)


    $export($export.S + $export.F * !_dereq_(56), 'Object', {
      defineProperty: _dereq_(97).f
    });
  }, {
    "56": 56,
    "60": 60,
    "97": 97
  }],
  215: [function (_dereq_, module, exports) {
    // 19.1.2.5 Object.freeze(O)
    var isObject = _dereq_(79);

    var meta = _dereq_(92).onFreeze;

    _dereq_(107)('freeze', function ($freeze) {
      return function freeze(it) {
        return $freeze && isObject(it) ? $freeze(meta(it)) : it;
      };
    });
  }, {
    "107": 107,
    "79": 79,
    "92": 92
  }],
  216: [function (_dereq_, module, exports) {
    // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
    var toIObject = _dereq_(138);

    var $getOwnPropertyDescriptor = _dereq_(99).f;

    _dereq_(107)('getOwnPropertyDescriptor', function () {
      return function getOwnPropertyDescriptor(it, key) {
        return $getOwnPropertyDescriptor(toIObject(it), key);
      };
    });
  }, {
    "107": 107,
    "138": 138,
    "99": 99
  }],
  217: [function (_dereq_, module, exports) {
    // 19.1.2.7 Object.getOwnPropertyNames(O)
    _dereq_(107)('getOwnPropertyNames', function () {
      return _dereq_(100).f;
    });
  }, {
    "100": 100,
    "107": 107
  }],
  218: [function (_dereq_, module, exports) {
    // 19.1.2.9 Object.getPrototypeOf(O)
    var toObject = _dereq_(140);

    var $getPrototypeOf = _dereq_(103);

    _dereq_(107)('getPrototypeOf', function () {
      return function getPrototypeOf(it) {
        return $getPrototypeOf(toObject(it));
      };
    });
  }, {
    "103": 103,
    "107": 107,
    "140": 140
  }],
  219: [function (_dereq_, module, exports) {
    // 19.1.2.11 Object.isExtensible(O)
    var isObject = _dereq_(79);

    _dereq_(107)('isExtensible', function ($isExtensible) {
      return function isExtensible(it) {
        return isObject(it) ? $isExtensible ? $isExtensible(it) : true : false;
      };
    });
  }, {
    "107": 107,
    "79": 79
  }],
  220: [function (_dereq_, module, exports) {
    // 19.1.2.12 Object.isFrozen(O)
    var isObject = _dereq_(79);

    _dereq_(107)('isFrozen', function ($isFrozen) {
      return function isFrozen(it) {
        return isObject(it) ? $isFrozen ? $isFrozen(it) : false : true;
      };
    });
  }, {
    "107": 107,
    "79": 79
  }],
  221: [function (_dereq_, module, exports) {
    // 19.1.2.13 Object.isSealed(O)
    var isObject = _dereq_(79);

    _dereq_(107)('isSealed', function ($isSealed) {
      return function isSealed(it) {
        return isObject(it) ? $isSealed ? $isSealed(it) : false : true;
      };
    });
  }, {
    "107": 107,
    "79": 79
  }],
  222: [function (_dereq_, module, exports) {
    // 19.1.3.10 Object.is(value1, value2)
    var $export = _dereq_(60);

    $export($export.S, 'Object', {
      is: _dereq_(119)
    });
  }, {
    "119": 119,
    "60": 60
  }],
  223: [function (_dereq_, module, exports) {
    // 19.1.2.14 Object.keys(O)
    var toObject = _dereq_(140);

    var $keys = _dereq_(105);

    _dereq_(107)('keys', function () {
      return function keys(it) {
        return $keys(toObject(it));
      };
    });
  }, {
    "105": 105,
    "107": 107,
    "140": 140
  }],
  224: [function (_dereq_, module, exports) {
    // 19.1.2.15 Object.preventExtensions(O)
    var isObject = _dereq_(79);

    var meta = _dereq_(92).onFreeze;

    _dereq_(107)('preventExtensions', function ($preventExtensions) {
      return function preventExtensions(it) {
        return $preventExtensions && isObject(it) ? $preventExtensions(meta(it)) : it;
      };
    });
  }, {
    "107": 107,
    "79": 79,
    "92": 92
  }],
  225: [function (_dereq_, module, exports) {
    // 19.1.2.17 Object.seal(O)
    var isObject = _dereq_(79);

    var meta = _dereq_(92).onFreeze;

    _dereq_(107)('seal', function ($seal) {
      return function seal(it) {
        return $seal && isObject(it) ? $seal(meta(it)) : it;
      };
    });
  }, {
    "107": 107,
    "79": 79,
    "92": 92
  }],
  226: [function (_dereq_, module, exports) {
    // 19.1.3.19 Object.setPrototypeOf(O, proto)
    var $export = _dereq_(60);

    $export($export.S, 'Object', {
      setPrototypeOf: _dereq_(120).set
    });
  }, {
    "120": 120,
    "60": 60
  }],
  227: [function (_dereq_, module, exports) {
    'use strict'; // 19.1.3.6 Object.prototype.toString()

    var classof = _dereq_(45);

    var test = {};
    test[_dereq_(150)('toStringTag')] = 'z';

    if (test + '' != '[object z]') {
      _dereq_(116)(Object.prototype, 'toString', function toString() {
        return '[object ' + classof(this) + ']';
      }, true);
    }
  }, {
    "116": 116,
    "150": 150,
    "45": 45
  }],
  228: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    var $parseFloat = _dereq_(110); // 18.2.4 parseFloat(string)


    $export($export.G + $export.F * (parseFloat != $parseFloat), {
      parseFloat: $parseFloat
    });
  }, {
    "110": 110,
    "60": 60
  }],
  229: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    var $parseInt = _dereq_(111); // 18.2.5 parseInt(string, radix)


    $export($export.G + $export.F * (parseInt != $parseInt), {
      parseInt: $parseInt
    });
  }, {
    "111": 111,
    "60": 60
  }],
  230: [function (_dereq_, module, exports) {
    'use strict';

    var LIBRARY = _dereq_(87);

    var global = _dereq_(68);

    var ctx = _dereq_(52);

    var classof = _dereq_(45);

    var $export = _dereq_(60);

    var isObject = _dereq_(79);

    var aFunction = _dereq_(31);

    var anInstance = _dereq_(35);

    var forOf = _dereq_(66);

    var speciesConstructor = _dereq_(125);

    var task = _dereq_(134).set;

    var microtask = _dereq_(93)();

    var newPromiseCapabilityModule = _dereq_(94);

    var perform = _dereq_(112);

    var userAgent = _dereq_(146);

    var promiseResolve = _dereq_(113);

    var PROMISE = 'Promise';
    var TypeError = global.TypeError;
    var process = global.process;
    var versions = process && process.versions;
    var v8 = versions && versions.v8 || '';
    var $Promise = global[PROMISE];
    var isNode = classof(process) == 'process';

    var empty = function empty() {
      /* empty */
    };

    var Internal, newGenericPromiseCapability, OwnPromiseCapability, Wrapper;
    var newPromiseCapability = newGenericPromiseCapability = newPromiseCapabilityModule.f;
    var USE_NATIVE = !!function () {
      try {
        // correct subclassing with @@species support
        var promise = $Promise.resolve(1);

        var FakePromise = (promise.constructor = {})[_dereq_(150)('species')] = function (exec) {
          exec(empty, empty);
        }; // unhandled rejections tracking support, NodeJS Promise without it fails @@species test


        return (isNode || typeof PromiseRejectionEvent == 'function') && promise.then(empty) instanceof FakePromise // v8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
        // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
        // we can't detect it synchronously, so just check versions
        && v8.indexOf('6.6') !== 0 && userAgent.indexOf('Chrome/66') === -1;
      } catch (e) {
        /* empty */
      }
    }(); // helpers

    var isThenable = function isThenable(it) {
      var then;
      return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
    };

    var notify = function notify(promise, isReject) {
      if (promise._n) return;
      promise._n = true;
      var chain = promise._c;
      microtask(function () {
        var value = promise._v;
        var ok = promise._s == 1;
        var i = 0;

        var run = function run(reaction) {
          var handler = ok ? reaction.ok : reaction.fail;
          var resolve = reaction.resolve;
          var reject = reaction.reject;
          var domain = reaction.domain;
          var result, then, exited;

          try {
            if (handler) {
              if (!ok) {
                if (promise._h == 2) onHandleUnhandled(promise);
                promise._h = 1;
              }

              if (handler === true) result = value;else {
                if (domain) domain.enter();
                result = handler(value); // may throw

                if (domain) {
                  domain.exit();
                  exited = true;
                }
              }

              if (result === reaction.promise) {
                reject(TypeError('Promise-chain cycle'));
              } else if (then = isThenable(result)) {
                then.call(result, resolve, reject);
              } else resolve(result);
            } else reject(value);
          } catch (e) {
            if (domain && !exited) domain.exit();
            reject(e);
          }
        };

        while (chain.length > i) {
          run(chain[i++]);
        } // variable length - can't use forEach


        promise._c = [];
        promise._n = false;
        if (isReject && !promise._h) onUnhandled(promise);
      });
    };

    var onUnhandled = function onUnhandled(promise) {
      task.call(global, function () {
        var value = promise._v;
        var unhandled = isUnhandled(promise);
        var result, handler, console;

        if (unhandled) {
          result = perform(function () {
            if (isNode) {
              process.emit('unhandledRejection', value, promise);
            } else if (handler = global.onunhandledrejection) {
              handler({
                promise: promise,
                reason: value
              });
            } else if ((console = global.console) && console.error) {
              console.error('Unhandled promise rejection', value);
            }
          }); // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should

          promise._h = isNode || isUnhandled(promise) ? 2 : 1;
        }

        promise._a = undefined;
        if (unhandled && result.e) throw result.v;
      });
    };

    var isUnhandled = function isUnhandled(promise) {
      return promise._h !== 1 && (promise._a || promise._c).length === 0;
    };

    var onHandleUnhandled = function onHandleUnhandled(promise) {
      task.call(global, function () {
        var handler;

        if (isNode) {
          process.emit('rejectionHandled', promise);
        } else if (handler = global.onrejectionhandled) {
          handler({
            promise: promise,
            reason: promise._v
          });
        }
      });
    };

    var $reject = function $reject(value) {
      var promise = this;
      if (promise._d) return;
      promise._d = true;
      promise = promise._w || promise; // unwrap

      promise._v = value;
      promise._s = 2;
      if (!promise._a) promise._a = promise._c.slice();
      notify(promise, true);
    };

    var $resolve = function $resolve(value) {
      var promise = this;
      var then;
      if (promise._d) return;
      promise._d = true;
      promise = promise._w || promise; // unwrap

      try {
        if (promise === value) throw TypeError("Promise can't be resolved itself");

        if (then = isThenable(value)) {
          microtask(function () {
            var wrapper = {
              _w: promise,
              _d: false
            }; // wrap

            try {
              then.call(value, ctx($resolve, wrapper, 1), ctx($reject, wrapper, 1));
            } catch (e) {
              $reject.call(wrapper, e);
            }
          });
        } else {
          promise._v = value;
          promise._s = 1;
          notify(promise, false);
        }
      } catch (e) {
        $reject.call({
          _w: promise,
          _d: false
        }, e); // wrap
      }
    }; // constructor polyfill


    if (!USE_NATIVE) {
      // 25.4.3.1 Promise(executor)
      $Promise = function Promise(executor) {
        anInstance(this, $Promise, PROMISE, '_h');
        aFunction(executor);
        Internal.call(this);

        try {
          executor(ctx($resolve, this, 1), ctx($reject, this, 1));
        } catch (err) {
          $reject.call(this, err);
        }
      }; // eslint-disable-next-line no-unused-vars


      Internal = function Promise(executor) {
        this._c = []; // <- awaiting reactions

        this._a = undefined; // <- checked in isUnhandled reactions

        this._s = 0; // <- state

        this._d = false; // <- done

        this._v = undefined; // <- value

        this._h = 0; // <- rejection state, 0 - default, 1 - handled, 2 - unhandled

        this._n = false; // <- notify
      };

      Internal.prototype = _dereq_(115)($Promise.prototype, {
        // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
        then: function then(onFulfilled, onRejected) {
          var reaction = newPromiseCapability(speciesConstructor(this, $Promise));
          reaction.ok = typeof onFulfilled == 'function' ? onFulfilled : true;
          reaction.fail = typeof onRejected == 'function' && onRejected;
          reaction.domain = isNode ? process.domain : undefined;

          this._c.push(reaction);

          if (this._a) this._a.push(reaction);
          if (this._s) notify(this, false);
          return reaction.promise;
        },
        // 25.4.5.1 Promise.prototype.catch(onRejected)
        'catch': function _catch(onRejected) {
          return this.then(undefined, onRejected);
        }
      });

      OwnPromiseCapability = function OwnPromiseCapability() {
        var promise = new Internal();
        this.promise = promise;
        this.resolve = ctx($resolve, promise, 1);
        this.reject = ctx($reject, promise, 1);
      };

      newPromiseCapabilityModule.f = newPromiseCapability = function newPromiseCapability(C) {
        return C === $Promise || C === Wrapper ? new OwnPromiseCapability(C) : newGenericPromiseCapability(C);
      };
    }

    $export($export.G + $export.W + $export.F * !USE_NATIVE, {
      Promise: $Promise
    });

    _dereq_(122)($Promise, PROMISE);

    _dereq_(121)(PROMISE);

    Wrapper = _dereq_(50)[PROMISE]; // statics

    $export($export.S + $export.F * !USE_NATIVE, PROMISE, {
      // 25.4.4.5 Promise.reject(r)
      reject: function reject(r) {
        var capability = newPromiseCapability(this);
        var $$reject = capability.reject;
        $$reject(r);
        return capability.promise;
      }
    });
    $export($export.S + $export.F * (LIBRARY || !USE_NATIVE), PROMISE, {
      // 25.4.4.6 Promise.resolve(x)
      resolve: function resolve(x) {
        return promiseResolve(LIBRARY && this === Wrapper ? $Promise : this, x);
      }
    });
    $export($export.S + $export.F * !(USE_NATIVE && _dereq_(84)(function (iter) {
      $Promise.all(iter)['catch'](empty);
    })), PROMISE, {
      // 25.4.4.1 Promise.all(iterable)
      all: function all(iterable) {
        var C = this;
        var capability = newPromiseCapability(C);
        var resolve = capability.resolve;
        var reject = capability.reject;
        var result = perform(function () {
          var values = [];
          var index = 0;
          var remaining = 1;
          forOf(iterable, false, function (promise) {
            var $index = index++;
            var alreadyCalled = false;
            values.push(undefined);
            remaining++;
            C.resolve(promise).then(function (value) {
              if (alreadyCalled) return;
              alreadyCalled = true;
              values[$index] = value;
              --remaining || resolve(values);
            }, reject);
          });
          --remaining || resolve(values);
        });
        if (result.e) reject(result.v);
        return capability.promise;
      },
      // 25.4.4.4 Promise.race(iterable)
      race: function race(iterable) {
        var C = this;
        var capability = newPromiseCapability(C);
        var reject = capability.reject;
        var result = perform(function () {
          forOf(iterable, false, function (promise) {
            C.resolve(promise).then(capability.resolve, reject);
          });
        });
        if (result.e) reject(result.v);
        return capability.promise;
      }
    });
  }, {
    "112": 112,
    "113": 113,
    "115": 115,
    "121": 121,
    "122": 122,
    "125": 125,
    "134": 134,
    "146": 146,
    "150": 150,
    "31": 31,
    "35": 35,
    "45": 45,
    "50": 50,
    "52": 52,
    "60": 60,
    "66": 66,
    "68": 68,
    "79": 79,
    "84": 84,
    "87": 87,
    "93": 93,
    "94": 94
  }],
  231: [function (_dereq_, module, exports) {
    // 26.1.1 Reflect.apply(target, thisArgument, argumentsList)
    var $export = _dereq_(60);

    var aFunction = _dereq_(31);

    var anObject = _dereq_(36);

    var rApply = (_dereq_(68).Reflect || {}).apply;
    var fApply = Function.apply; // MS Edge argumentsList argument is optional

    $export($export.S + $export.F * !_dereq_(62)(function () {
      rApply(function () {
        /* empty */
      });
    }), 'Reflect', {
      apply: function apply(target, thisArgument, argumentsList) {
        var T = aFunction(target);
        var L = anObject(argumentsList);
        return rApply ? rApply(T, thisArgument, L) : fApply.call(T, thisArgument, L);
      }
    });
  }, {
    "31": 31,
    "36": 36,
    "60": 60,
    "62": 62,
    "68": 68
  }],
  232: [function (_dereq_, module, exports) {
    // 26.1.2 Reflect.construct(target, argumentsList [, newTarget])
    var $export = _dereq_(60);

    var create = _dereq_(96);

    var aFunction = _dereq_(31);

    var anObject = _dereq_(36);

    var isObject = _dereq_(79);

    var fails = _dereq_(62);

    var bind = _dereq_(44);

    var rConstruct = (_dereq_(68).Reflect || {}).construct; // MS Edge supports only 2 arguments and argumentsList argument is optional
    // FF Nightly sets third argument as `new.target`, but does not create `this` from it

    var NEW_TARGET_BUG = fails(function () {
      function F() {
        /* empty */
      }

      return !(rConstruct(function () {
        /* empty */
      }, [], F) instanceof F);
    });
    var ARGS_BUG = !fails(function () {
      rConstruct(function () {
        /* empty */
      });
    });
    $export($export.S + $export.F * (NEW_TARGET_BUG || ARGS_BUG), 'Reflect', {
      construct: function construct(Target, args
      /* , newTarget */
      ) {
        aFunction(Target);
        anObject(args);
        var newTarget = arguments.length < 3 ? Target : aFunction(arguments[2]);
        if (ARGS_BUG && !NEW_TARGET_BUG) return rConstruct(Target, args, newTarget);

        if (Target == newTarget) {
          // w/o altered newTarget, optimization for 0-4 arguments
          switch (args.length) {
            case 0:
              return new Target();

            case 1:
              return new Target(args[0]);

            case 2:
              return new Target(args[0], args[1]);

            case 3:
              return new Target(args[0], args[1], args[2]);

            case 4:
              return new Target(args[0], args[1], args[2], args[3]);
          } // w/o altered newTarget, lot of arguments case


          var $args = [null];
          $args.push.apply($args, args);
          return new (bind.apply(Target, $args))();
        } // with altered newTarget, not support built-in constructors


        var proto = newTarget.prototype;
        var instance = create(isObject(proto) ? proto : Object.prototype);
        var result = Function.apply.call(Target, instance, args);
        return isObject(result) ? result : instance;
      }
    });
  }, {
    "31": 31,
    "36": 36,
    "44": 44,
    "60": 60,
    "62": 62,
    "68": 68,
    "79": 79,
    "96": 96
  }],
  233: [function (_dereq_, module, exports) {
    // 26.1.3 Reflect.defineProperty(target, propertyKey, attributes)
    var dP = _dereq_(97);

    var $export = _dereq_(60);

    var anObject = _dereq_(36);

    var toPrimitive = _dereq_(141); // MS Edge has broken Reflect.defineProperty - throwing instead of returning false


    $export($export.S + $export.F * _dereq_(62)(function () {
      // eslint-disable-next-line no-undef
      Reflect.defineProperty(dP.f({}, 1, {
        value: 1
      }), 1, {
        value: 2
      });
    }), 'Reflect', {
      defineProperty: function defineProperty(target, propertyKey, attributes) {
        anObject(target);
        propertyKey = toPrimitive(propertyKey, true);
        anObject(attributes);

        try {
          dP.f(target, propertyKey, attributes);
          return true;
        } catch (e) {
          return false;
        }
      }
    });
  }, {
    "141": 141,
    "36": 36,
    "60": 60,
    "62": 62,
    "97": 97
  }],
  234: [function (_dereq_, module, exports) {
    // 26.1.4 Reflect.deleteProperty(target, propertyKey)
    var $export = _dereq_(60);

    var gOPD = _dereq_(99).f;

    var anObject = _dereq_(36);

    $export($export.S, 'Reflect', {
      deleteProperty: function deleteProperty(target, propertyKey) {
        var desc = gOPD(anObject(target), propertyKey);
        return desc && !desc.configurable ? false : delete target[propertyKey];
      }
    });
  }, {
    "36": 36,
    "60": 60,
    "99": 99
  }],
  235: [function (_dereq_, module, exports) {
    'use strict'; // 26.1.5 Reflect.enumerate(target)

    var $export = _dereq_(60);

    var anObject = _dereq_(36);

    var Enumerate = function Enumerate(iterated) {
      this._t = anObject(iterated); // target

      this._i = 0; // next index

      var keys = this._k = []; // keys

      var key;

      for (key in iterated) {
        keys.push(key);
      }
    };

    _dereq_(82)(Enumerate, 'Object', function () {
      var that = this;
      var keys = that._k;
      var key;

      do {
        if (that._i >= keys.length) return {
          value: undefined,
          done: true
        };
      } while (!((key = keys[that._i++]) in that._t));

      return {
        value: key,
        done: false
      };
    });

    $export($export.S, 'Reflect', {
      enumerate: function enumerate(target) {
        return new Enumerate(target);
      }
    });
  }, {
    "36": 36,
    "60": 60,
    "82": 82
  }],
  236: [function (_dereq_, module, exports) {
    // 26.1.7 Reflect.getOwnPropertyDescriptor(target, propertyKey)
    var gOPD = _dereq_(99);

    var $export = _dereq_(60);

    var anObject = _dereq_(36);

    $export($export.S, 'Reflect', {
      getOwnPropertyDescriptor: function getOwnPropertyDescriptor(target, propertyKey) {
        return gOPD.f(anObject(target), propertyKey);
      }
    });
  }, {
    "36": 36,
    "60": 60,
    "99": 99
  }],
  237: [function (_dereq_, module, exports) {
    // 26.1.8 Reflect.getPrototypeOf(target)
    var $export = _dereq_(60);

    var getProto = _dereq_(103);

    var anObject = _dereq_(36);

    $export($export.S, 'Reflect', {
      getPrototypeOf: function getPrototypeOf(target) {
        return getProto(anObject(target));
      }
    });
  }, {
    "103": 103,
    "36": 36,
    "60": 60
  }],
  238: [function (_dereq_, module, exports) {
    // 26.1.6 Reflect.get(target, propertyKey [, receiver])
    var gOPD = _dereq_(99);

    var getPrototypeOf = _dereq_(103);

    var has = _dereq_(69);

    var $export = _dereq_(60);

    var isObject = _dereq_(79);

    var anObject = _dereq_(36);

    function get(target, propertyKey
    /* , receiver */
    ) {
      var receiver = arguments.length < 3 ? target : arguments[2];
      var desc, proto;
      if (anObject(target) === receiver) return target[propertyKey];
      if (desc = gOPD.f(target, propertyKey)) return has(desc, 'value') ? desc.value : desc.get !== undefined ? desc.get.call(receiver) : undefined;
      if (isObject(proto = getPrototypeOf(target))) return get(proto, propertyKey, receiver);
    }

    $export($export.S, 'Reflect', {
      get: get
    });
  }, {
    "103": 103,
    "36": 36,
    "60": 60,
    "69": 69,
    "79": 79,
    "99": 99
  }],
  239: [function (_dereq_, module, exports) {
    // 26.1.9 Reflect.has(target, propertyKey)
    var $export = _dereq_(60);

    $export($export.S, 'Reflect', {
      has: function has(target, propertyKey) {
        return propertyKey in target;
      }
    });
  }, {
    "60": 60
  }],
  240: [function (_dereq_, module, exports) {
    // 26.1.10 Reflect.isExtensible(target)
    var $export = _dereq_(60);

    var anObject = _dereq_(36);

    var $isExtensible = Object.isExtensible;
    $export($export.S, 'Reflect', {
      isExtensible: function isExtensible(target) {
        anObject(target);
        return $isExtensible ? $isExtensible(target) : true;
      }
    });
  }, {
    "36": 36,
    "60": 60
  }],
  241: [function (_dereq_, module, exports) {
    // 26.1.11 Reflect.ownKeys(target)
    var $export = _dereq_(60);

    $export($export.S, 'Reflect', {
      ownKeys: _dereq_(109)
    });
  }, {
    "109": 109,
    "60": 60
  }],
  242: [function (_dereq_, module, exports) {
    // 26.1.12 Reflect.preventExtensions(target)
    var $export = _dereq_(60);

    var anObject = _dereq_(36);

    var $preventExtensions = Object.preventExtensions;
    $export($export.S, 'Reflect', {
      preventExtensions: function preventExtensions(target) {
        anObject(target);

        try {
          if ($preventExtensions) $preventExtensions(target);
          return true;
        } catch (e) {
          return false;
        }
      }
    });
  }, {
    "36": 36,
    "60": 60
  }],
  243: [function (_dereq_, module, exports) {
    // 26.1.14 Reflect.setPrototypeOf(target, proto)
    var $export = _dereq_(60);

    var setProto = _dereq_(120);

    if (setProto) $export($export.S, 'Reflect', {
      setPrototypeOf: function setPrototypeOf(target, proto) {
        setProto.check(target, proto);

        try {
          setProto.set(target, proto);
          return true;
        } catch (e) {
          return false;
        }
      }
    });
  }, {
    "120": 120,
    "60": 60
  }],
  244: [function (_dereq_, module, exports) {
    // 26.1.13 Reflect.set(target, propertyKey, V [, receiver])
    var dP = _dereq_(97);

    var gOPD = _dereq_(99);

    var getPrototypeOf = _dereq_(103);

    var has = _dereq_(69);

    var $export = _dereq_(60);

    var createDesc = _dereq_(114);

    var anObject = _dereq_(36);

    var isObject = _dereq_(79);

    function set(target, propertyKey, V
    /* , receiver */
    ) {
      var receiver = arguments.length < 4 ? target : arguments[3];
      var ownDesc = gOPD.f(anObject(target), propertyKey);
      var existingDescriptor, proto;

      if (!ownDesc) {
        if (isObject(proto = getPrototypeOf(target))) {
          return set(proto, propertyKey, V, receiver);
        }

        ownDesc = createDesc(0);
      }

      if (has(ownDesc, 'value')) {
        if (ownDesc.writable === false || !isObject(receiver)) return false;

        if (existingDescriptor = gOPD.f(receiver, propertyKey)) {
          if (existingDescriptor.get || existingDescriptor.set || existingDescriptor.writable === false) return false;
          existingDescriptor.value = V;
          dP.f(receiver, propertyKey, existingDescriptor);
        } else dP.f(receiver, propertyKey, createDesc(0, V));

        return true;
      }

      return ownDesc.set === undefined ? false : (ownDesc.set.call(receiver, V), true);
    }

    $export($export.S, 'Reflect', {
      set: set
    });
  }, {
    "103": 103,
    "114": 114,
    "36": 36,
    "60": 60,
    "69": 69,
    "79": 79,
    "97": 97,
    "99": 99
  }],
  245: [function (_dereq_, module, exports) {
    var global = _dereq_(68);

    var inheritIfRequired = _dereq_(73);

    var dP = _dereq_(97).f;

    var gOPN = _dereq_(101).f;

    var isRegExp = _dereq_(80);

    var $flags = _dereq_(64);

    var $RegExp = global.RegExp;
    var Base = $RegExp;
    var proto = $RegExp.prototype;
    var re1 = /a/g;
    var re2 = /a/g; // "new" creates a new object, old webkit buggy here

    var CORRECT_NEW = new $RegExp(re1) !== re1;

    if (_dereq_(56) && (!CORRECT_NEW || _dereq_(62)(function () {
      re2[_dereq_(150)('match')] = false; // RegExp constructor can alter flags and IsRegExp works correct with @@match

      return $RegExp(re1) != re1 || $RegExp(re2) == re2 || $RegExp(re1, 'i') != '/a/i';
    }))) {
      $RegExp = function RegExp(p, f) {
        var tiRE = this instanceof $RegExp;
        var piRE = isRegExp(p);
        var fiU = f === undefined;
        return !tiRE && piRE && p.constructor === $RegExp && fiU ? p : inheritIfRequired(CORRECT_NEW ? new Base(piRE && !fiU ? p.source : p, f) : Base((piRE = p instanceof $RegExp) ? p.source : p, piRE && fiU ? $flags.call(p) : f), tiRE ? this : proto, $RegExp);
      };

      var proxy = function proxy(key) {
        key in $RegExp || dP($RegExp, key, {
          configurable: true,
          get: function get() {
            return Base[key];
          },
          set: function set(it) {
            Base[key] = it;
          }
        });
      };

      for (var keys = gOPN(Base), i = 0; keys.length > i;) {
        proxy(keys[i++]);
      }

      proto.constructor = $RegExp;
      $RegExp.prototype = proto;

      _dereq_(116)(global, 'RegExp', $RegExp);
    }

    _dereq_(121)('RegExp');
  }, {
    "101": 101,
    "116": 116,
    "121": 121,
    "150": 150,
    "56": 56,
    "62": 62,
    "64": 64,
    "68": 68,
    "73": 73,
    "80": 80,
    "97": 97
  }],
  246: [function (_dereq_, module, exports) {
    'use strict';

    var regexpExec = _dereq_(118);

    _dereq_(60)({
      target: 'RegExp',
      proto: true,
      forced: regexpExec !== /./.exec
    }, {
      exec: regexpExec
    });
  }, {
    "118": 118,
    "60": 60
  }],
  247: [function (_dereq_, module, exports) {
    // 21.2.5.3 get RegExp.prototype.flags()
    if (_dereq_(56) && /./g.flags != 'g') _dereq_(97).f(RegExp.prototype, 'flags', {
      configurable: true,
      get: _dereq_(64)
    });
  }, {
    "56": 56,
    "64": 64,
    "97": 97
  }],
  248: [function (_dereq_, module, exports) {
    'use strict';

    var anObject = _dereq_(36);

    var toLength = _dereq_(139);

    var advanceStringIndex = _dereq_(34);

    var regExpExec = _dereq_(117); // @@match logic


    _dereq_(63)('match', 1, function (defined, MATCH, $match, maybeCallNative) {
      return [// `String.prototype.match` method
      // https://tc39.github.io/ecma262/#sec-string.prototype.match
      function match(regexp) {
        var O = defined(this);
        var fn = regexp == undefined ? undefined : regexp[MATCH];
        return fn !== undefined ? fn.call(regexp, O) : new RegExp(regexp)[MATCH](String(O));
      }, // `RegExp.prototype[@@match]` method
      // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@match
      function (regexp) {
        var res = maybeCallNative($match, regexp, this);
        if (res.done) return res.value;
        var rx = anObject(regexp);
        var S = String(this);
        if (!rx.global) return regExpExec(rx, S);
        var fullUnicode = rx.unicode;
        rx.lastIndex = 0;
        var A = [];
        var n = 0;
        var result;

        while ((result = regExpExec(rx, S)) !== null) {
          var matchStr = String(result[0]);
          A[n] = matchStr;
          if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
          n++;
        }

        return n === 0 ? null : A;
      }];
    });
  }, {
    "117": 117,
    "139": 139,
    "34": 34,
    "36": 36,
    "63": 63
  }],
  249: [function (_dereq_, module, exports) {
    'use strict';

    var anObject = _dereq_(36);

    var toObject = _dereq_(140);

    var toLength = _dereq_(139);

    var toInteger = _dereq_(137);

    var advanceStringIndex = _dereq_(34);

    var regExpExec = _dereq_(117);

    var max = Math.max;
    var min = Math.min;
    var floor = Math.floor;
    var SUBSTITUTION_SYMBOLS = /\$([$&`']|\d\d?|<[^>]*>)/g;
    var SUBSTITUTION_SYMBOLS_NO_NAMED = /\$([$&`']|\d\d?)/g;

    var maybeToString = function maybeToString(it) {
      return it === undefined ? it : String(it);
    }; // @@replace logic


    _dereq_(63)('replace', 2, function (defined, REPLACE, $replace, maybeCallNative) {
      return [// `String.prototype.replace` method
      // https://tc39.github.io/ecma262/#sec-string.prototype.replace
      function replace(searchValue, replaceValue) {
        var O = defined(this);
        var fn = searchValue == undefined ? undefined : searchValue[REPLACE];
        return fn !== undefined ? fn.call(searchValue, O, replaceValue) : $replace.call(String(O), searchValue, replaceValue);
      }, // `RegExp.prototype[@@replace]` method
      // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@replace
      function (regexp, replaceValue) {
        var res = maybeCallNative($replace, regexp, this, replaceValue);
        if (res.done) return res.value;
        var rx = anObject(regexp);
        var S = String(this);
        var functionalReplace = typeof replaceValue === 'function';
        if (!functionalReplace) replaceValue = String(replaceValue);
        var global = rx.global;

        if (global) {
          var fullUnicode = rx.unicode;
          rx.lastIndex = 0;
        }

        var results = [];

        while (true) {
          var result = regExpExec(rx, S);
          if (result === null) break;
          results.push(result);
          if (!global) break;
          var matchStr = String(result[0]);
          if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
        }

        var accumulatedResult = '';
        var nextSourcePosition = 0;

        for (var i = 0; i < results.length; i++) {
          result = results[i];
          var matched = String(result[0]);
          var position = max(min(toInteger(result.index), S.length), 0);
          var captures = []; // NOTE: This is equivalent to
          //   captures = result.slice(1).map(maybeToString)
          // but for some reason `nativeSlice.call(result, 1, result.length)` (called in
          // the slice polyfill when slicing native arrays) "doesn't work" in safari 9 and
          // causes a crash (https://pastebin.com/N21QzeQA) when trying to debug it.

          for (var j = 1; j < result.length; j++) {
            captures.push(maybeToString(result[j]));
          }

          var namedCaptures = result.groups;

          if (functionalReplace) {
            var replacerArgs = [matched].concat(captures, position, S);
            if (namedCaptures !== undefined) replacerArgs.push(namedCaptures);
            var replacement = String(replaceValue.apply(undefined, replacerArgs));
          } else {
            replacement = getSubstitution(matched, S, position, captures, namedCaptures, replaceValue);
          }

          if (position >= nextSourcePosition) {
            accumulatedResult += S.slice(nextSourcePosition, position) + replacement;
            nextSourcePosition = position + matched.length;
          }
        }

        return accumulatedResult + S.slice(nextSourcePosition);
      }]; // https://tc39.github.io/ecma262/#sec-getsubstitution

      function getSubstitution(matched, str, position, captures, namedCaptures, replacement) {
        var tailPos = position + matched.length;
        var m = captures.length;
        var symbols = SUBSTITUTION_SYMBOLS_NO_NAMED;

        if (namedCaptures !== undefined) {
          namedCaptures = toObject(namedCaptures);
          symbols = SUBSTITUTION_SYMBOLS;
        }

        return $replace.call(replacement, symbols, function (match, ch) {
          var capture;

          switch (ch.charAt(0)) {
            case '$':
              return '$';

            case '&':
              return matched;

            case '`':
              return str.slice(0, position);

            case "'":
              return str.slice(tailPos);

            case '<':
              capture = namedCaptures[ch.slice(1, -1)];
              break;

            default:
              // \d\d?
              var n = +ch;
              if (n === 0) return match;

              if (n > m) {
                var f = floor(n / 10);
                if (f === 0) return match;
                if (f <= m) return captures[f - 1] === undefined ? ch.charAt(1) : captures[f - 1] + ch.charAt(1);
                return match;
              }

              capture = captures[n - 1];
          }

          return capture === undefined ? '' : capture;
        });
      }
    });
  }, {
    "117": 117,
    "137": 137,
    "139": 139,
    "140": 140,
    "34": 34,
    "36": 36,
    "63": 63
  }],
  250: [function (_dereq_, module, exports) {
    'use strict';

    var anObject = _dereq_(36);

    var sameValue = _dereq_(119);

    var regExpExec = _dereq_(117); // @@search logic


    _dereq_(63)('search', 1, function (defined, SEARCH, $search, maybeCallNative) {
      return [// `String.prototype.search` method
      // https://tc39.github.io/ecma262/#sec-string.prototype.search
      function search(regexp) {
        var O = defined(this);
        var fn = regexp == undefined ? undefined : regexp[SEARCH];
        return fn !== undefined ? fn.call(regexp, O) : new RegExp(regexp)[SEARCH](String(O));
      }, // `RegExp.prototype[@@search]` method
      // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@search
      function (regexp) {
        var res = maybeCallNative($search, regexp, this);
        if (res.done) return res.value;
        var rx = anObject(regexp);
        var S = String(this);
        var previousLastIndex = rx.lastIndex;
        if (!sameValue(previousLastIndex, 0)) rx.lastIndex = 0;
        var result = regExpExec(rx, S);
        if (!sameValue(rx.lastIndex, previousLastIndex)) rx.lastIndex = previousLastIndex;
        return result === null ? -1 : result.index;
      }];
    });
  }, {
    "117": 117,
    "119": 119,
    "36": 36,
    "63": 63
  }],
  251: [function (_dereq_, module, exports) {
    'use strict';

    var isRegExp = _dereq_(80);

    var anObject = _dereq_(36);

    var speciesConstructor = _dereq_(125);

    var advanceStringIndex = _dereq_(34);

    var toLength = _dereq_(139);

    var callRegExpExec = _dereq_(117);

    var regexpExec = _dereq_(118);

    var fails = _dereq_(62);

    var $min = Math.min;
    var $push = [].push;
    var $SPLIT = 'split';
    var LENGTH = 'length';
    var LAST_INDEX = 'lastIndex';
    var MAX_UINT32 = 0xffffffff; // babel-minify transpiles RegExp('x', 'y') -> /x/y and it causes SyntaxError

    var SUPPORTS_Y = !fails(function () {
      RegExp(MAX_UINT32, 'y');
    }); // @@split logic

    _dereq_(63)('split', 2, function (defined, SPLIT, $split, maybeCallNative) {
      var internalSplit;

      if ('abbc'[$SPLIT](/(b)*/)[1] == 'c' || 'test'[$SPLIT](/(?:)/, -1)[LENGTH] != 4 || 'ab'[$SPLIT](/(?:ab)*/)[LENGTH] != 2 || '.'[$SPLIT](/(.?)(.?)/)[LENGTH] != 4 || '.'[$SPLIT](/()()/)[LENGTH] > 1 || ''[$SPLIT](/.?/)[LENGTH]) {
        // based on es5-shim implementation, need to rework it
        internalSplit = function internalSplit(separator, limit) {
          var string = String(this);
          if (separator === undefined && limit === 0) return []; // If `separator` is not a regex, use native split

          if (!isRegExp(separator)) return $split.call(string, separator, limit);
          var output = [];
          var flags = (separator.ignoreCase ? 'i' : '') + (separator.multiline ? 'm' : '') + (separator.unicode ? 'u' : '') + (separator.sticky ? 'y' : '');
          var lastLastIndex = 0;
          var splitLimit = limit === undefined ? MAX_UINT32 : limit >>> 0; // Make `global` and avoid `lastIndex` issues by working with a copy

          var separatorCopy = new RegExp(separator.source, flags + 'g');
          var match, lastIndex, lastLength;

          while (match = regexpExec.call(separatorCopy, string)) {
            lastIndex = separatorCopy[LAST_INDEX];

            if (lastIndex > lastLastIndex) {
              output.push(string.slice(lastLastIndex, match.index));
              if (match[LENGTH] > 1 && match.index < string[LENGTH]) $push.apply(output, match.slice(1));
              lastLength = match[0][LENGTH];
              lastLastIndex = lastIndex;
              if (output[LENGTH] >= splitLimit) break;
            }

            if (separatorCopy[LAST_INDEX] === match.index) separatorCopy[LAST_INDEX]++; // Avoid an infinite loop
          }

          if (lastLastIndex === string[LENGTH]) {
            if (lastLength || !separatorCopy.test('')) output.push('');
          } else output.push(string.slice(lastLastIndex));

          return output[LENGTH] > splitLimit ? output.slice(0, splitLimit) : output;
        }; // Chakra, V8

      } else if ('0'[$SPLIT](undefined, 0)[LENGTH]) {
        internalSplit = function internalSplit(separator, limit) {
          return separator === undefined && limit === 0 ? [] : $split.call(this, separator, limit);
        };
      } else {
        internalSplit = $split;
      }

      return [// `String.prototype.split` method
      // https://tc39.github.io/ecma262/#sec-string.prototype.split
      function split(separator, limit) {
        var O = defined(this);
        var splitter = separator == undefined ? undefined : separator[SPLIT];
        return splitter !== undefined ? splitter.call(separator, O, limit) : internalSplit.call(String(O), separator, limit);
      }, // `RegExp.prototype[@@split]` method
      // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@split
      //
      // NOTE: This cannot be properly polyfilled in engines that don't support
      // the 'y' flag.
      function (regexp, limit) {
        var res = maybeCallNative(internalSplit, regexp, this, limit, internalSplit !== $split);
        if (res.done) return res.value;
        var rx = anObject(regexp);
        var S = String(this);
        var C = speciesConstructor(rx, RegExp);
        var unicodeMatching = rx.unicode;
        var flags = (rx.ignoreCase ? 'i' : '') + (rx.multiline ? 'm' : '') + (rx.unicode ? 'u' : '') + (SUPPORTS_Y ? 'y' : 'g'); // ^(? + rx + ) is needed, in combination with some S slicing, to
        // simulate the 'y' flag.

        var splitter = new C(SUPPORTS_Y ? rx : '^(?:' + rx.source + ')', flags);
        var lim = limit === undefined ? MAX_UINT32 : limit >>> 0;
        if (lim === 0) return [];
        if (S.length === 0) return callRegExpExec(splitter, S) === null ? [S] : [];
        var p = 0;
        var q = 0;
        var A = [];

        while (q < S.length) {
          splitter.lastIndex = SUPPORTS_Y ? q : 0;
          var z = callRegExpExec(splitter, SUPPORTS_Y ? S : S.slice(q));
          var e;

          if (z === null || (e = $min(toLength(splitter.lastIndex + (SUPPORTS_Y ? 0 : q)), S.length)) === p) {
            q = advanceStringIndex(S, q, unicodeMatching);
          } else {
            A.push(S.slice(p, q));
            if (A.length === lim) return A;

            for (var i = 1; i <= z.length - 1; i++) {
              A.push(z[i]);
              if (A.length === lim) return A;
            }

            q = p = e;
          }
        }

        A.push(S.slice(p));
        return A;
      }];
    });
  }, {
    "117": 117,
    "118": 118,
    "125": 125,
    "139": 139,
    "34": 34,
    "36": 36,
    "62": 62,
    "63": 63,
    "80": 80
  }],
  252: [function (_dereq_, module, exports) {
    'use strict';

    _dereq_(247);

    var anObject = _dereq_(36);

    var $flags = _dereq_(64);

    var DESCRIPTORS = _dereq_(56);

    var TO_STRING = 'toString';
    var $toString = /./[TO_STRING];

    var define = function define(fn) {
      _dereq_(116)(RegExp.prototype, TO_STRING, fn, true);
    }; // 21.2.5.14 RegExp.prototype.toString()


    if (_dereq_(62)(function () {
      return $toString.call({
        source: 'a',
        flags: 'b'
      }) != '/a/b';
    })) {
      define(function toString() {
        var R = anObject(this);
        return '/'.concat(R.source, '/', 'flags' in R ? R.flags : !DESCRIPTORS && R instanceof RegExp ? $flags.call(R) : undefined);
      }); // FF44- RegExp#toString has a wrong name
    } else if ($toString.name != TO_STRING) {
      define(function toString() {
        return $toString.call(this);
      });
    }
  }, {
    "116": 116,
    "247": 247,
    "36": 36,
    "56": 56,
    "62": 62,
    "64": 64
  }],
  253: [function (_dereq_, module, exports) {
    'use strict';

    var strong = _dereq_(47);

    var validate = _dereq_(147);

    var SET = 'Set'; // 23.2 Set Objects

    module.exports = _dereq_(49)(SET, function (get) {
      return function Set() {
        return get(this, arguments.length > 0 ? arguments[0] : undefined);
      };
    }, {
      // 23.2.3.1 Set.prototype.add(value)
      add: function add(value) {
        return strong.def(validate(this, SET), value = value === 0 ? 0 : value, value);
      }
    }, strong);
  }, {
    "147": 147,
    "47": 47,
    "49": 49
  }],
  254: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.2 String.prototype.anchor(name)

    _dereq_(129)('anchor', function (createHTML) {
      return function anchor(name) {
        return createHTML(this, 'a', 'name', name);
      };
    });
  }, {
    "129": 129
  }],
  255: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.3 String.prototype.big()

    _dereq_(129)('big', function (createHTML) {
      return function big() {
        return createHTML(this, 'big', '', '');
      };
    });
  }, {
    "129": 129
  }],
  256: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.4 String.prototype.blink()

    _dereq_(129)('blink', function (createHTML) {
      return function blink() {
        return createHTML(this, 'blink', '', '');
      };
    });
  }, {
    "129": 129
  }],
  257: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.5 String.prototype.bold()

    _dereq_(129)('bold', function (createHTML) {
      return function bold() {
        return createHTML(this, 'b', '', '');
      };
    });
  }, {
    "129": 129
  }],
  258: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $at = _dereq_(127)(false);

    $export($export.P, 'String', {
      // 21.1.3.3 String.prototype.codePointAt(pos)
      codePointAt: function codePointAt(pos) {
        return $at(this, pos);
      }
    });
  }, {
    "127": 127,
    "60": 60
  }],
  259: [function (_dereq_, module, exports) {
    // 21.1.3.6 String.prototype.endsWith(searchString [, endPosition])
    'use strict';

    var $export = _dereq_(60);

    var toLength = _dereq_(139);

    var context = _dereq_(128);

    var ENDS_WITH = 'endsWith';
    var $endsWith = ''[ENDS_WITH];
    $export($export.P + $export.F * _dereq_(61)(ENDS_WITH), 'String', {
      endsWith: function endsWith(searchString
      /* , endPosition = @length */
      ) {
        var that = context(this, searchString, ENDS_WITH);
        var endPosition = arguments.length > 1 ? arguments[1] : undefined;
        var len = toLength(that.length);
        var end = endPosition === undefined ? len : Math.min(toLength(endPosition), len);
        var search = String(searchString);
        return $endsWith ? $endsWith.call(that, search, end) : that.slice(end - search.length, end) === search;
      }
    });
  }, {
    "128": 128,
    "139": 139,
    "60": 60,
    "61": 61
  }],
  260: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.6 String.prototype.fixed()

    _dereq_(129)('fixed', function (createHTML) {
      return function fixed() {
        return createHTML(this, 'tt', '', '');
      };
    });
  }, {
    "129": 129
  }],
  261: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.7 String.prototype.fontcolor(color)

    _dereq_(129)('fontcolor', function (createHTML) {
      return function fontcolor(color) {
        return createHTML(this, 'font', 'color', color);
      };
    });
  }, {
    "129": 129
  }],
  262: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.8 String.prototype.fontsize(size)

    _dereq_(129)('fontsize', function (createHTML) {
      return function fontsize(size) {
        return createHTML(this, 'font', 'size', size);
      };
    });
  }, {
    "129": 129
  }],
  263: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    var toAbsoluteIndex = _dereq_(135);

    var fromCharCode = String.fromCharCode;
    var $fromCodePoint = String.fromCodePoint; // length should be 1, old FF problem

    $export($export.S + $export.F * (!!$fromCodePoint && $fromCodePoint.length != 1), 'String', {
      // 21.1.2.2 String.fromCodePoint(...codePoints)
      fromCodePoint: function fromCodePoint(x) {
        // eslint-disable-line no-unused-vars
        var res = [];
        var aLen = arguments.length;
        var i = 0;
        var code;

        while (aLen > i) {
          code = +arguments[i++];
          if (toAbsoluteIndex(code, 0x10ffff) !== code) throw RangeError(code + ' is not a valid code point');
          res.push(code < 0x10000 ? fromCharCode(code) : fromCharCode(((code -= 0x10000) >> 10) + 0xd800, code % 0x400 + 0xdc00));
        }

        return res.join('');
      }
    });
  }, {
    "135": 135,
    "60": 60
  }],
  264: [function (_dereq_, module, exports) {
    // 21.1.3.7 String.prototype.includes(searchString, position = 0)
    'use strict';

    var $export = _dereq_(60);

    var context = _dereq_(128);

    var INCLUDES = 'includes';
    $export($export.P + $export.F * _dereq_(61)(INCLUDES), 'String', {
      includes: function includes(searchString
      /* , position = 0 */
      ) {
        return !!~context(this, searchString, INCLUDES).indexOf(searchString, arguments.length > 1 ? arguments[1] : undefined);
      }
    });
  }, {
    "128": 128,
    "60": 60,
    "61": 61
  }],
  265: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.9 String.prototype.italics()

    _dereq_(129)('italics', function (createHTML) {
      return function italics() {
        return createHTML(this, 'i', '', '');
      };
    });
  }, {
    "129": 129
  }],
  266: [function (_dereq_, module, exports) {
    'use strict';

    var $at = _dereq_(127)(true); // 21.1.3.27 String.prototype[@@iterator]()


    _dereq_(83)(String, 'String', function (iterated) {
      this._t = String(iterated); // target

      this._i = 0; // next index
      // 21.1.5.2.1 %StringIteratorPrototype%.next()
    }, function () {
      var O = this._t;
      var index = this._i;
      var point;
      if (index >= O.length) return {
        value: undefined,
        done: true
      };
      point = $at(O, index);
      this._i += point.length;
      return {
        value: point,
        done: false
      };
    });
  }, {
    "127": 127,
    "83": 83
  }],
  267: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.10 String.prototype.link(url)

    _dereq_(129)('link', function (createHTML) {
      return function link(url) {
        return createHTML(this, 'a', 'href', url);
      };
    });
  }, {
    "129": 129
  }],
  268: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    var toIObject = _dereq_(138);

    var toLength = _dereq_(139);

    $export($export.S, 'String', {
      // 21.1.2.4 String.raw(callSite, ...substitutions)
      raw: function raw(callSite) {
        var tpl = toIObject(callSite.raw);
        var len = toLength(tpl.length);
        var aLen = arguments.length;
        var res = [];
        var i = 0;

        while (len > i) {
          res.push(String(tpl[i++]));
          if (i < aLen) res.push(String(arguments[i]));
        }

        return res.join('');
      }
    });
  }, {
    "138": 138,
    "139": 139,
    "60": 60
  }],
  269: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    $export($export.P, 'String', {
      // 21.1.3.13 String.prototype.repeat(count)
      repeat: _dereq_(131)
    });
  }, {
    "131": 131,
    "60": 60
  }],
  270: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.11 String.prototype.small()

    _dereq_(129)('small', function (createHTML) {
      return function small() {
        return createHTML(this, 'small', '', '');
      };
    });
  }, {
    "129": 129
  }],
  271: [function (_dereq_, module, exports) {
    // 21.1.3.18 String.prototype.startsWith(searchString [, position ])
    'use strict';

    var $export = _dereq_(60);

    var toLength = _dereq_(139);

    var context = _dereq_(128);

    var STARTS_WITH = 'startsWith';
    var $startsWith = ''[STARTS_WITH];
    $export($export.P + $export.F * _dereq_(61)(STARTS_WITH), 'String', {
      startsWith: function startsWith(searchString
      /* , position = 0 */
      ) {
        var that = context(this, searchString, STARTS_WITH);
        var index = toLength(Math.min(arguments.length > 1 ? arguments[1] : undefined, that.length));
        var search = String(searchString);
        return $startsWith ? $startsWith.call(that, search, index) : that.slice(index, index + search.length) === search;
      }
    });
  }, {
    "128": 128,
    "139": 139,
    "60": 60,
    "61": 61
  }],
  272: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.12 String.prototype.strike()

    _dereq_(129)('strike', function (createHTML) {
      return function strike() {
        return createHTML(this, 'strike', '', '');
      };
    });
  }, {
    "129": 129
  }],
  273: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.13 String.prototype.sub()

    _dereq_(129)('sub', function (createHTML) {
      return function sub() {
        return createHTML(this, 'sub', '', '');
      };
    });
  }, {
    "129": 129
  }],
  274: [function (_dereq_, module, exports) {
    'use strict'; // B.2.3.14 String.prototype.sup()

    _dereq_(129)('sup', function (createHTML) {
      return function sup() {
        return createHTML(this, 'sup', '', '');
      };
    });
  }, {
    "129": 129
  }],
  275: [function (_dereq_, module, exports) {
    'use strict'; // 21.1.3.25 String.prototype.trim()

    _dereq_(132)('trim', function ($trim) {
      return function trim() {
        return $trim(this, 3);
      };
    });
  }, {
    "132": 132
  }],
  276: [function (_dereq_, module, exports) {
    'use strict'; // ECMAScript 6 symbols shim

    var global = _dereq_(68);

    var has = _dereq_(69);

    var DESCRIPTORS = _dereq_(56);

    var $export = _dereq_(60);

    var redefine = _dereq_(116);

    var META = _dereq_(92).KEY;

    var $fails = _dereq_(62);

    var shared = _dereq_(124);

    var setToStringTag = _dereq_(122);

    var uid = _dereq_(145);

    var wks = _dereq_(150);

    var wksExt = _dereq_(149);

    var wksDefine = _dereq_(148);

    var enumKeys = _dereq_(59);

    var isArray = _dereq_(77);

    var anObject = _dereq_(36);

    var isObject = _dereq_(79);

    var toObject = _dereq_(140);

    var toIObject = _dereq_(138);

    var toPrimitive = _dereq_(141);

    var createDesc = _dereq_(114);

    var _create = _dereq_(96);

    var gOPNExt = _dereq_(100);

    var $GOPD = _dereq_(99);

    var $GOPS = _dereq_(102);

    var $DP = _dereq_(97);

    var $keys = _dereq_(105);

    var gOPD = $GOPD.f;
    var dP = $DP.f;
    var gOPN = gOPNExt.f;
    var $Symbol = global.Symbol;
    var $JSON = global.JSON;

    var _stringify = $JSON && $JSON.stringify;

    var PROTOTYPE = 'prototype';
    var HIDDEN = wks('_hidden');
    var TO_PRIMITIVE = wks('toPrimitive');
    var isEnum = {}.propertyIsEnumerable;
    var SymbolRegistry = shared('symbol-registry');
    var AllSymbols = shared('symbols');
    var OPSymbols = shared('op-symbols');
    var ObjectProto = Object[PROTOTYPE];
    var USE_NATIVE = typeof $Symbol == 'function' && !!$GOPS.f;
    var QObject = global.QObject; // Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173

    var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild; // fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687

    var setSymbolDesc = DESCRIPTORS && $fails(function () {
      return _create(dP({}, 'a', {
        get: function get() {
          return dP(this, 'a', {
            value: 7
          }).a;
        }
      })).a != 7;
    }) ? function (it, key, D) {
      var protoDesc = gOPD(ObjectProto, key);
      if (protoDesc) delete ObjectProto[key];
      dP(it, key, D);
      if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
    } : dP;

    var wrap = function wrap(tag) {
      var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);

      sym._k = tag;
      return sym;
    };

    var isSymbol = USE_NATIVE && _typeof($Symbol.iterator) == 'symbol' ? function (it) {
      return _typeof(it) == 'symbol';
    } : function (it) {
      return it instanceof $Symbol;
    };

    var $defineProperty = function defineProperty(it, key, D) {
      if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
      anObject(it);
      key = toPrimitive(key, true);
      anObject(D);

      if (has(AllSymbols, key)) {
        if (!D.enumerable) {
          if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
          it[HIDDEN][key] = true;
        } else {
          if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
          D = _create(D, {
            enumerable: createDesc(0, false)
          });
        }

        return setSymbolDesc(it, key, D);
      }

      return dP(it, key, D);
    };

    var $defineProperties = function defineProperties(it, P) {
      anObject(it);
      var keys = enumKeys(P = toIObject(P));
      var i = 0;
      var l = keys.length;
      var key;

      while (l > i) {
        $defineProperty(it, key = keys[i++], P[key]);
      }

      return it;
    };

    var $create = function create(it, P) {
      return P === undefined ? _create(it) : $defineProperties(_create(it), P);
    };

    var $propertyIsEnumerable = function propertyIsEnumerable(key) {
      var E = isEnum.call(this, key = toPrimitive(key, true));
      if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
      return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
    };

    var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
      it = toIObject(it);
      key = toPrimitive(key, true);
      if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
      var D = gOPD(it, key);
      if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
      return D;
    };

    var $getOwnPropertyNames = function getOwnPropertyNames(it) {
      var names = gOPN(toIObject(it));
      var result = [];
      var i = 0;
      var key;

      while (names.length > i) {
        if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
      }

      return result;
    };

    var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
      var IS_OP = it === ObjectProto;
      var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
      var result = [];
      var i = 0;
      var key;

      while (names.length > i) {
        if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
      }

      return result;
    }; // 19.4.1.1 Symbol([description])


    if (!USE_NATIVE) {
      $Symbol = function _Symbol2() {
        if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
        var tag = uid(arguments.length > 0 ? arguments[0] : undefined);

        var $set = function $set(value) {
          if (this === ObjectProto) $set.call(OPSymbols, value);
          if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
          setSymbolDesc(this, tag, createDesc(1, value));
        };

        if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, {
          configurable: true,
          set: $set
        });
        return wrap(tag);
      };

      redefine($Symbol[PROTOTYPE], 'toString', function toString() {
        return this._k;
      });
      $GOPD.f = $getOwnPropertyDescriptor;
      $DP.f = $defineProperty;
      _dereq_(101).f = gOPNExt.f = $getOwnPropertyNames;
      _dereq_(106).f = $propertyIsEnumerable;
      $GOPS.f = $getOwnPropertySymbols;

      if (DESCRIPTORS && !_dereq_(87)) {
        redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
      }

      wksExt.f = function (name) {
        return wrap(wks(name));
      };
    }

    $export($export.G + $export.W + $export.F * !USE_NATIVE, {
      Symbol: $Symbol
    });

    for (var es6Symbols = // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
    'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'.split(','), j = 0; es6Symbols.length > j;) {
      wks(es6Symbols[j++]);
    }

    for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) {
      wksDefine(wellKnownSymbols[k++]);
    }

    $export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
      // 19.4.2.1 Symbol.for(key)
      'for': function _for(key) {
        return has(SymbolRegistry, key += '') ? SymbolRegistry[key] : SymbolRegistry[key] = $Symbol(key);
      },
      // 19.4.2.5 Symbol.keyFor(sym)
      keyFor: function keyFor(sym) {
        if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');

        for (var key in SymbolRegistry) {
          if (SymbolRegistry[key] === sym) return key;
        }
      },
      useSetter: function useSetter() {
        setter = true;
      },
      useSimple: function useSimple() {
        setter = false;
      }
    });
    $export($export.S + $export.F * !USE_NATIVE, 'Object', {
      // 19.1.2.2 Object.create(O [, Properties])
      create: $create,
      // 19.1.2.4 Object.defineProperty(O, P, Attributes)
      defineProperty: $defineProperty,
      // 19.1.2.3 Object.defineProperties(O, Properties)
      defineProperties: $defineProperties,
      // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
      getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
      // 19.1.2.7 Object.getOwnPropertyNames(O)
      getOwnPropertyNames: $getOwnPropertyNames,
      // 19.1.2.8 Object.getOwnPropertySymbols(O)
      getOwnPropertySymbols: $getOwnPropertySymbols
    }); // Chrome 38 and 39 `Object.getOwnPropertySymbols` fails on primitives
    // https://bugs.chromium.org/p/v8/issues/detail?id=3443

    var FAILS_ON_PRIMITIVES = $fails(function () {
      $GOPS.f(1);
    });
    $export($export.S + $export.F * FAILS_ON_PRIMITIVES, 'Object', {
      getOwnPropertySymbols: function getOwnPropertySymbols(it) {
        return $GOPS.f(toObject(it));
      }
    }); // 24.3.2 JSON.stringify(value [, replacer [, space]])

    $JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
      var S = $Symbol(); // MS Edge converts symbol values to JSON as {}
      // WebKit converts symbol values to JSON as null
      // V8 throws on boxed symbols

      return _stringify([S]) != '[null]' || _stringify({
        a: S
      }) != '{}' || _stringify(Object(S)) != '{}';
    })), 'JSON', {
      stringify: function stringify(it) {
        var args = [it];
        var i = 1;
        var replacer, $replacer;

        while (arguments.length > i) {
          args.push(arguments[i++]);
        }

        $replacer = replacer = args[1];
        if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined

        if (!isArray(replacer)) replacer = function replacer(key, value) {
          if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
          if (!isSymbol(value)) return value;
        };
        args[1] = replacer;
        return _stringify.apply($JSON, args);
      }
    }); // 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)

    $Symbol[PROTOTYPE][TO_PRIMITIVE] || _dereq_(70)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf); // 19.4.3.5 Symbol.prototype[@@toStringTag]

    setToStringTag($Symbol, 'Symbol'); // 20.2.1.9 Math[@@toStringTag]

    setToStringTag(Math, 'Math', true); // 24.3.3 JSON[@@toStringTag]

    setToStringTag(global.JSON, 'JSON', true);
  }, {
    "100": 100,
    "101": 101,
    "102": 102,
    "105": 105,
    "106": 106,
    "114": 114,
    "116": 116,
    "122": 122,
    "124": 124,
    "138": 138,
    "140": 140,
    "141": 141,
    "145": 145,
    "148": 148,
    "149": 149,
    "150": 150,
    "36": 36,
    "56": 56,
    "59": 59,
    "60": 60,
    "62": 62,
    "68": 68,
    "69": 69,
    "70": 70,
    "77": 77,
    "79": 79,
    "87": 87,
    "92": 92,
    "96": 96,
    "97": 97,
    "99": 99
  }],
  277: [function (_dereq_, module, exports) {
    'use strict';

    var $export = _dereq_(60);

    var $typed = _dereq_(144);

    var buffer = _dereq_(143);

    var anObject = _dereq_(36);

    var toAbsoluteIndex = _dereq_(135);

    var toLength = _dereq_(139);

    var isObject = _dereq_(79);

    var ArrayBuffer = _dereq_(68).ArrayBuffer;

    var speciesConstructor = _dereq_(125);

    var $ArrayBuffer = buffer.ArrayBuffer;
    var $DataView = buffer.DataView;
    var $isView = $typed.ABV && ArrayBuffer.isView;
    var $slice = $ArrayBuffer.prototype.slice;
    var VIEW = $typed.VIEW;
    var ARRAY_BUFFER = 'ArrayBuffer';
    $export($export.G + $export.W + $export.F * (ArrayBuffer !== $ArrayBuffer), {
      ArrayBuffer: $ArrayBuffer
    });
    $export($export.S + $export.F * !$typed.CONSTR, ARRAY_BUFFER, {
      // 24.1.3.1 ArrayBuffer.isView(arg)
      isView: function isView(it) {
        return $isView && $isView(it) || isObject(it) && VIEW in it;
      }
    });
    $export($export.P + $export.U + $export.F * _dereq_(62)(function () {
      return !new $ArrayBuffer(2).slice(1, undefined).byteLength;
    }), ARRAY_BUFFER, {
      // 24.1.4.3 ArrayBuffer.prototype.slice(start, end)
      slice: function slice(start, end) {
        if ($slice !== undefined && end === undefined) return $slice.call(anObject(this), start); // FF fix

        var len = anObject(this).byteLength;
        var first = toAbsoluteIndex(start, len);
        var fin = toAbsoluteIndex(end === undefined ? len : end, len);
        var result = new (speciesConstructor(this, $ArrayBuffer))(toLength(fin - first));
        var viewS = new $DataView(this);
        var viewT = new $DataView(result);
        var index = 0;

        while (first < fin) {
          viewT.setUint8(index++, viewS.getUint8(first++));
        }

        return result;
      }
    });

    _dereq_(121)(ARRAY_BUFFER);
  }, {
    "121": 121,
    "125": 125,
    "135": 135,
    "139": 139,
    "143": 143,
    "144": 144,
    "36": 36,
    "60": 60,
    "62": 62,
    "68": 68,
    "79": 79
  }],
  278: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    $export($export.G + $export.W + $export.F * !_dereq_(144).ABV, {
      DataView: _dereq_(143).DataView
    });
  }, {
    "143": 143,
    "144": 144,
    "60": 60
  }],
  279: [function (_dereq_, module, exports) {
    _dereq_(142)('Float32', 4, function (init) {
      return function Float32Array(data, byteOffset, length) {
        return init(this, data, byteOffset, length);
      };
    });
  }, {
    "142": 142
  }],
  280: [function (_dereq_, module, exports) {
    _dereq_(142)('Float64', 8, function (init) {
      return function Float64Array(data, byteOffset, length) {
        return init(this, data, byteOffset, length);
      };
    });
  }, {
    "142": 142
  }],
  281: [function (_dereq_, module, exports) {
    _dereq_(142)('Int16', 2, function (init) {
      return function Int16Array(data, byteOffset, length) {
        return init(this, data, byteOffset, length);
      };
    });
  }, {
    "142": 142
  }],
  282: [function (_dereq_, module, exports) {
    _dereq_(142)('Int32', 4, function (init) {
      return function Int32Array(data, byteOffset, length) {
        return init(this, data, byteOffset, length);
      };
    });
  }, {
    "142": 142
  }],
  283: [function (_dereq_, module, exports) {
    _dereq_(142)('Int8', 1, function (init) {
      return function Int8Array(data, byteOffset, length) {
        return init(this, data, byteOffset, length);
      };
    });
  }, {
    "142": 142
  }],
  284: [function (_dereq_, module, exports) {
    _dereq_(142)('Uint16', 2, function (init) {
      return function Uint16Array(data, byteOffset, length) {
        return init(this, data, byteOffset, length);
      };
    });
  }, {
    "142": 142
  }],
  285: [function (_dereq_, module, exports) {
    _dereq_(142)('Uint32', 4, function (init) {
      return function Uint32Array(data, byteOffset, length) {
        return init(this, data, byteOffset, length);
      };
    });
  }, {
    "142": 142
  }],
  286: [function (_dereq_, module, exports) {
    _dereq_(142)('Uint8', 1, function (init) {
      return function Uint8Array(data, byteOffset, length) {
        return init(this, data, byteOffset, length);
      };
    });
  }, {
    "142": 142
  }],
  287: [function (_dereq_, module, exports) {
    _dereq_(142)('Uint8', 1, function (init) {
      return function Uint8ClampedArray(data, byteOffset, length) {
        return init(this, data, byteOffset, length);
      };
    }, true);
  }, {
    "142": 142
  }],
  288: [function (_dereq_, module, exports) {
    'use strict';

    var global = _dereq_(68);

    var each = _dereq_(40)(0);

    var redefine = _dereq_(116);

    var meta = _dereq_(92);

    var assign = _dereq_(95);

    var weak = _dereq_(48);

    var isObject = _dereq_(79);

    var validate = _dereq_(147);

    var NATIVE_WEAK_MAP = _dereq_(147);

    var IS_IE11 = !global.ActiveXObject && 'ActiveXObject' in global;
    var WEAK_MAP = 'WeakMap';
    var getWeak = meta.getWeak;
    var isExtensible = Object.isExtensible;
    var uncaughtFrozenStore = weak.ufstore;
    var InternalMap;

    var wrapper = function wrapper(get) {
      return function WeakMap() {
        return get(this, arguments.length > 0 ? arguments[0] : undefined);
      };
    };

    var methods = {
      // 23.3.3.3 WeakMap.prototype.get(key)
      get: function get(key) {
        if (isObject(key)) {
          var data = getWeak(key);
          if (data === true) return uncaughtFrozenStore(validate(this, WEAK_MAP)).get(key);
          return data ? data[this._i] : undefined;
        }
      },
      // 23.3.3.5 WeakMap.prototype.set(key, value)
      set: function set(key, value) {
        return weak.def(validate(this, WEAK_MAP), key, value);
      }
    }; // 23.3 WeakMap Objects

    var $WeakMap = module.exports = _dereq_(49)(WEAK_MAP, wrapper, methods, weak, true, true); // IE11 WeakMap frozen keys fix


    if (NATIVE_WEAK_MAP && IS_IE11) {
      InternalMap = weak.getConstructor(wrapper, WEAK_MAP);
      assign(InternalMap.prototype, methods);
      meta.NEED = true;
      each(['delete', 'has', 'get', 'set'], function (key) {
        var proto = $WeakMap.prototype;
        var method = proto[key];
        redefine(proto, key, function (a, b) {
          // store frozen objects on internal weakmap shim
          if (isObject(a) && !isExtensible(a)) {
            if (!this._f) this._f = new InternalMap();

            var result = this._f[key](a, b);

            return key == 'set' ? this : result; // store all the rest on native weakmap
          }

          return method.call(this, a, b);
        });
      });
    }
  }, {
    "116": 116,
    "147": 147,
    "40": 40,
    "48": 48,
    "49": 49,
    "68": 68,
    "79": 79,
    "92": 92,
    "95": 95
  }],
  289: [function (_dereq_, module, exports) {
    'use strict';

    var weak = _dereq_(48);

    var validate = _dereq_(147);

    var WEAK_SET = 'WeakSet'; // 23.4 WeakSet Objects

    _dereq_(49)(WEAK_SET, function (get) {
      return function WeakSet() {
        return get(this, arguments.length > 0 ? arguments[0] : undefined);
      };
    }, {
      // 23.4.3.1 WeakSet.prototype.add(value)
      add: function add(value) {
        return weak.def(validate(this, WEAK_SET), value, true);
      }
    }, weak, false, true);
  }, {
    "147": 147,
    "48": 48,
    "49": 49
  }],
  290: [function (_dereq_, module, exports) {
    'use strict'; // https://tc39.github.io/proposal-flatMap/#sec-Array.prototype.flatMap

    var $export = _dereq_(60);

    var flattenIntoArray = _dereq_(65);

    var toObject = _dereq_(140);

    var toLength = _dereq_(139);

    var aFunction = _dereq_(31);

    var arraySpeciesCreate = _dereq_(43);

    $export($export.P, 'Array', {
      flatMap: function flatMap(callbackfn
      /* , thisArg */
      ) {
        var O = toObject(this);
        var sourceLen, A;
        aFunction(callbackfn);
        sourceLen = toLength(O.length);
        A = arraySpeciesCreate(O, 0);
        flattenIntoArray(A, O, O, sourceLen, 0, 1, callbackfn, arguments[1]);
        return A;
      }
    });

    _dereq_(33)('flatMap');
  }, {
    "139": 139,
    "140": 140,
    "31": 31,
    "33": 33,
    "43": 43,
    "60": 60,
    "65": 65
  }],
  291: [function (_dereq_, module, exports) {
    'use strict'; // https://github.com/tc39/Array.prototype.includes

    var $export = _dereq_(60);

    var $includes = _dereq_(39)(true);

    $export($export.P, 'Array', {
      includes: function includes(el
      /* , fromIndex = 0 */
      ) {
        return $includes(this, el, arguments.length > 1 ? arguments[1] : undefined);
      }
    });

    _dereq_(33)('includes');
  }, {
    "33": 33,
    "39": 39,
    "60": 60
  }],
  292: [function (_dereq_, module, exports) {
    // https://github.com/tc39/proposal-object-values-entries
    var $export = _dereq_(60);

    var $entries = _dereq_(108)(true);

    $export($export.S, 'Object', {
      entries: function entries(it) {
        return $entries(it);
      }
    });
  }, {
    "108": 108,
    "60": 60
  }],
  293: [function (_dereq_, module, exports) {
    // https://github.com/tc39/proposal-object-getownpropertydescriptors
    var $export = _dereq_(60);

    var ownKeys = _dereq_(109);

    var toIObject = _dereq_(138);

    var gOPD = _dereq_(99);

    var createProperty = _dereq_(51);

    $export($export.S, 'Object', {
      getOwnPropertyDescriptors: function getOwnPropertyDescriptors(object) {
        var O = toIObject(object);
        var getDesc = gOPD.f;
        var keys = ownKeys(O);
        var result = {};
        var i = 0;
        var key, desc;

        while (keys.length > i) {
          desc = getDesc(O, key = keys[i++]);
          if (desc !== undefined) createProperty(result, key, desc);
        }

        return result;
      }
    });
  }, {
    "109": 109,
    "138": 138,
    "51": 51,
    "60": 60,
    "99": 99
  }],
  294: [function (_dereq_, module, exports) {
    // https://github.com/tc39/proposal-object-values-entries
    var $export = _dereq_(60);

    var $values = _dereq_(108)(false);

    $export($export.S, 'Object', {
      values: function values(it) {
        return $values(it);
      }
    });
  }, {
    "108": 108,
    "60": 60
  }],
  295: [function (_dereq_, module, exports) {
    // https://github.com/tc39/proposal-promise-finally
    'use strict';

    var $export = _dereq_(60);

    var core = _dereq_(50);

    var global = _dereq_(68);

    var speciesConstructor = _dereq_(125);

    var promiseResolve = _dereq_(113);

    $export($export.P + $export.R, 'Promise', {
      'finally': function _finally(onFinally) {
        var C = speciesConstructor(this, core.Promise || global.Promise);
        var isFunction = typeof onFinally == 'function';
        return this.then(isFunction ? function (x) {
          return promiseResolve(C, onFinally()).then(function () {
            return x;
          });
        } : onFinally, isFunction ? function (e) {
          return promiseResolve(C, onFinally()).then(function () {
            throw e;
          });
        } : onFinally);
      }
    });
  }, {
    "113": 113,
    "125": 125,
    "50": 50,
    "60": 60,
    "68": 68
  }],
  296: [function (_dereq_, module, exports) {
    'use strict'; // https://github.com/tc39/proposal-string-pad-start-end

    var $export = _dereq_(60);

    var $pad = _dereq_(130);

    var userAgent = _dereq_(146); // https://github.com/zloirock/core-js/issues/280


    var WEBKIT_BUG = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(userAgent);
    $export($export.P + $export.F * WEBKIT_BUG, 'String', {
      padEnd: function padEnd(maxLength
      /* , fillString = ' ' */
      ) {
        return $pad(this, maxLength, arguments.length > 1 ? arguments[1] : undefined, false);
      }
    });
  }, {
    "130": 130,
    "146": 146,
    "60": 60
  }],
  297: [function (_dereq_, module, exports) {
    'use strict'; // https://github.com/tc39/proposal-string-pad-start-end

    var $export = _dereq_(60);

    var $pad = _dereq_(130);

    var userAgent = _dereq_(146); // https://github.com/zloirock/core-js/issues/280


    var WEBKIT_BUG = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(userAgent);
    $export($export.P + $export.F * WEBKIT_BUG, 'String', {
      padStart: function padStart(maxLength
      /* , fillString = ' ' */
      ) {
        return $pad(this, maxLength, arguments.length > 1 ? arguments[1] : undefined, true);
      }
    });
  }, {
    "130": 130,
    "146": 146,
    "60": 60
  }],
  298: [function (_dereq_, module, exports) {
    'use strict'; // https://github.com/sebmarkbage/ecmascript-string-left-right-trim

    _dereq_(132)('trimLeft', function ($trim) {
      return function trimLeft() {
        return $trim(this, 1);
      };
    }, 'trimStart');
  }, {
    "132": 132
  }],
  299: [function (_dereq_, module, exports) {
    'use strict'; // https://github.com/sebmarkbage/ecmascript-string-left-right-trim

    _dereq_(132)('trimRight', function ($trim) {
      return function trimRight() {
        return $trim(this, 2);
      };
    }, 'trimEnd');
  }, {
    "132": 132
  }],
  300: [function (_dereq_, module, exports) {
    _dereq_(148)('asyncIterator');
  }, {
    "148": 148
  }],
  301: [function (_dereq_, module, exports) {
    var $iterators = _dereq_(162);

    var getKeys = _dereq_(105);

    var redefine = _dereq_(116);

    var global = _dereq_(68);

    var hide = _dereq_(70);

    var Iterators = _dereq_(86);

    var wks = _dereq_(150);

    var ITERATOR = wks('iterator');
    var TO_STRING_TAG = wks('toStringTag');
    var ArrayValues = Iterators.Array;
    var DOMIterables = {
      CSSRuleList: true,
      // TODO: Not spec compliant, should be false.
      CSSStyleDeclaration: false,
      CSSValueList: false,
      ClientRectList: false,
      DOMRectList: false,
      DOMStringList: false,
      DOMTokenList: true,
      DataTransferItemList: false,
      FileList: false,
      HTMLAllCollection: false,
      HTMLCollection: false,
      HTMLFormElement: false,
      HTMLSelectElement: false,
      MediaList: true,
      // TODO: Not spec compliant, should be false.
      MimeTypeArray: false,
      NamedNodeMap: false,
      NodeList: true,
      PaintRequestList: false,
      Plugin: false,
      PluginArray: false,
      SVGLengthList: false,
      SVGNumberList: false,
      SVGPathSegList: false,
      SVGPointList: false,
      SVGStringList: false,
      SVGTransformList: false,
      SourceBufferList: false,
      StyleSheetList: true,
      // TODO: Not spec compliant, should be false.
      TextTrackCueList: false,
      TextTrackList: false,
      TouchList: false
    };

    for (var collections = getKeys(DOMIterables), i = 0; i < collections.length; i++) {
      var NAME = collections[i];
      var explicit = DOMIterables[NAME];
      var Collection = global[NAME];
      var proto = Collection && Collection.prototype;
      var key;

      if (proto) {
        if (!proto[ITERATOR]) hide(proto, ITERATOR, ArrayValues);
        if (!proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
        Iterators[NAME] = ArrayValues;
        if (explicit) for (key in $iterators) {
          if (!proto[key]) redefine(proto, key, $iterators[key], true);
        }
      }
    }
  }, {
    "105": 105,
    "116": 116,
    "150": 150,
    "162": 162,
    "68": 68,
    "70": 70,
    "86": 86
  }],
  302: [function (_dereq_, module, exports) {
    var $export = _dereq_(60);

    var $task = _dereq_(134);

    $export($export.G + $export.B, {
      setImmediate: $task.set,
      clearImmediate: $task.clear
    });
  }, {
    "134": 134,
    "60": 60
  }],
  303: [function (_dereq_, module, exports) {
    // ie9- setTimeout & setInterval additional parameters fix
    var global = _dereq_(68);

    var $export = _dereq_(60);

    var userAgent = _dereq_(146);

    var slice = [].slice;
    var MSIE = /MSIE .\./.test(userAgent); // <- dirty ie9- check

    var wrap = function wrap(set) {
      return function (fn, time
      /* , ...args */
      ) {
        var boundArgs = arguments.length > 2;
        var args = boundArgs ? slice.call(arguments, 2) : false;
        return set(boundArgs ? function () {
          // eslint-disable-next-line no-new-func
          (typeof fn == 'function' ? fn : Function(fn)).apply(this, args);
        } : fn, time);
      };
    };

    $export($export.G + $export.B + $export.F * MSIE, {
      setTimeout: wrap(global.setTimeout),
      setInterval: wrap(global.setInterval)
    });
  }, {
    "146": 146,
    "60": 60,
    "68": 68
  }],
  304: [function (_dereq_, module, exports) {
    _dereq_(303);

    _dereq_(302);

    _dereq_(301);

    module.exports = _dereq_(50);
  }, {
    "301": 301,
    "302": 302,
    "303": 303,
    "50": 50
  }],
  305: [function (_dereq_, module, exports) {
    /**
     * Copyright (c) 2014-present, Facebook, Inc.
     *
     * This source code is licensed under the MIT license found in the
     * LICENSE file in the root directory of this source tree.
     */
    var runtime = function (exports) {
      "use strict";

      var Op = Object.prototype;
      var hasOwn = Op.hasOwnProperty;
      var undefined; // More compressible than void 0.

      var $Symbol = typeof Symbol === "function" ? Symbol : {};
      var iteratorSymbol = $Symbol.iterator || "@@iterator";
      var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
      var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

      function wrap(innerFn, outerFn, self, tryLocsList) {
        // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
        var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
        var generator = Object.create(protoGenerator.prototype);
        var context = new Context(tryLocsList || []); // The ._invoke method unifies the implementations of the .next,
        // .throw, and .return methods.

        generator._invoke = makeInvokeMethod(innerFn, self, context);
        return generator;
      }

      exports.wrap = wrap; // Try/catch helper to minimize deoptimizations. Returns a completion
      // record like context.tryEntries[i].completion. This interface could
      // have been (and was previously) designed to take a closure to be
      // invoked without arguments, but in all the cases we care about we
      // already have an existing method we want to call, so there's no need
      // to create a new function object. We can even get away with assuming
      // the method takes exactly one argument, since that happens to be true
      // in every case, so we don't have to touch the arguments object. The
      // only additional allocation required is the completion record, which
      // has a stable shape and so hopefully should be cheap to allocate.

      function tryCatch(fn, obj, arg) {
        try {
          return {
            type: "normal",
            arg: fn.call(obj, arg)
          };
        } catch (err) {
          return {
            type: "throw",
            arg: err
          };
        }
      }

      var GenStateSuspendedStart = "suspendedStart";
      var GenStateSuspendedYield = "suspendedYield";
      var GenStateExecuting = "executing";
      var GenStateCompleted = "completed"; // Returning this object from the innerFn has the same effect as
      // breaking out of the dispatch switch statement.

      var ContinueSentinel = {}; // Dummy constructor functions that we use as the .constructor and
      // .constructor.prototype properties for functions that return Generator
      // objects. For full spec compliance, you may wish to configure your
      // minifier not to mangle the names of these two functions.

      function Generator() {}

      function GeneratorFunction() {}

      function GeneratorFunctionPrototype() {} // This is a polyfill for %IteratorPrototype% for environments that
      // don't natively support it.


      var IteratorPrototype = {};

      IteratorPrototype[iteratorSymbol] = function () {
        return this;
      };

      var getProto = Object.getPrototypeOf;
      var NativeIteratorPrototype = getProto && getProto(getProto(values([])));

      if (NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
        // This environment has a native %IteratorPrototype%; use it instead
        // of the polyfill.
        IteratorPrototype = NativeIteratorPrototype;
      }

      var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype);
      GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
      GeneratorFunctionPrototype.constructor = GeneratorFunction;
      GeneratorFunctionPrototype[toStringTagSymbol] = GeneratorFunction.displayName = "GeneratorFunction"; // Helper for defining the .next, .throw, and .return methods of the
      // Iterator interface in terms of a single ._invoke method.

      function defineIteratorMethods(prototype) {
        ["next", "throw", "return"].forEach(function (method) {
          prototype[method] = function (arg) {
            return this._invoke(method, arg);
          };
        });
      }

      exports.isGeneratorFunction = function (genFun) {
        var ctor = typeof genFun === "function" && genFun.constructor;
        return ctor ? ctor === GeneratorFunction || // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction" : false;
      };

      exports.mark = function (genFun) {
        if (Object.setPrototypeOf) {
          Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
        } else {
          genFun.__proto__ = GeneratorFunctionPrototype;

          if (!(toStringTagSymbol in genFun)) {
            genFun[toStringTagSymbol] = "GeneratorFunction";
          }
        }

        genFun.prototype = Object.create(Gp);
        return genFun;
      }; // Within the body of any async function, `await x` is transformed to
      // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
      // `hasOwn.call(value, "__await")` to determine if the yielded value is
      // meant to be awaited.


      exports.awrap = function (arg) {
        return {
          __await: arg
        };
      };

      function AsyncIterator(generator, PromiseImpl) {
        function invoke(method, arg, resolve, reject) {
          var record = tryCatch(generator[method], generator, arg);

          if (record.type === "throw") {
            reject(record.arg);
          } else {
            var result = record.arg;
            var value = result.value;

            if (value && _typeof(value) === "object" && hasOwn.call(value, "__await")) {
              return PromiseImpl.resolve(value.__await).then(function (value) {
                invoke("next", value, resolve, reject);
              }, function (err) {
                invoke("throw", err, resolve, reject);
              });
            }

            return PromiseImpl.resolve(value).then(function (unwrapped) {
              // When a yielded Promise is resolved, its final value becomes
              // the .value of the Promise<{value,done}> result for the
              // current iteration.
              result.value = unwrapped;
              resolve(result);
            }, function (error) {
              // If a rejected Promise was yielded, throw the rejection back
              // into the async generator function so it can be handled there.
              return invoke("throw", error, resolve, reject);
            });
          }
        }

        var previousPromise;

        function enqueue(method, arg) {
          function callInvokeWithMethodAndArg() {
            return new PromiseImpl(function (resolve, reject) {
              invoke(method, arg, resolve, reject);
            });
          }

          return previousPromise = // If enqueue has been called before, then we want to wait until
          // all previous Promises have been resolved before calling invoke,
          // so that results are always delivered in the correct order. If
          // enqueue has not been called before, then it is important to
          // call invoke immediately, without waiting on a callback to fire,
          // so that the async generator function has the opportunity to do
          // any necessary setup in a predictable way. This predictability
          // is why the Promise constructor synchronously invokes its
          // executor callback, and why async functions synchronously
          // execute code before the first await. Since we implement simple
          // async functions in terms of async generators, it is especially
          // important to get this right, even though it requires care.
          previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
        } // Define the unified helper method that is used to implement .next,
        // .throw, and .return (see defineIteratorMethods).


        this._invoke = enqueue;
      }

      defineIteratorMethods(AsyncIterator.prototype);

      AsyncIterator.prototype[asyncIteratorSymbol] = function () {
        return this;
      };

      exports.AsyncIterator = AsyncIterator; // Note that simple async functions are implemented on top of
      // AsyncIterator objects; they just return a Promise for the value of
      // the final result produced by the iterator.

      exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) {
        if (PromiseImpl === void 0) PromiseImpl = Promise;
        var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl);
        return exports.isGeneratorFunction(outerFn) ? iter // If outerFn is a generator, return the full iterator.
        : iter.next().then(function (result) {
          return result.done ? result.value : iter.next();
        });
      };

      function makeInvokeMethod(innerFn, self, context) {
        var state = GenStateSuspendedStart;
        return function invoke(method, arg) {
          if (state === GenStateExecuting) {
            throw new Error("Generator is already running");
          }

          if (state === GenStateCompleted) {
            if (method === "throw") {
              throw arg;
            } // Be forgiving, per 25.3.3.3.3 of the spec:
            // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume


            return doneResult();
          }

          context.method = method;
          context.arg = arg;

          while (true) {
            var delegate = context.delegate;

            if (delegate) {
              var delegateResult = maybeInvokeDelegate(delegate, context);

              if (delegateResult) {
                if (delegateResult === ContinueSentinel) continue;
                return delegateResult;
              }
            }

            if (context.method === "next") {
              // Setting context._sent for legacy support of Babel's
              // function.sent implementation.
              context.sent = context._sent = context.arg;
            } else if (context.method === "throw") {
              if (state === GenStateSuspendedStart) {
                state = GenStateCompleted;
                throw context.arg;
              }

              context.dispatchException(context.arg);
            } else if (context.method === "return") {
              context.abrupt("return", context.arg);
            }

            state = GenStateExecuting;
            var record = tryCatch(innerFn, self, context);

            if (record.type === "normal") {
              // If an exception is thrown from innerFn, we leave state ===
              // GenStateExecuting and loop back for another invocation.
              state = context.done ? GenStateCompleted : GenStateSuspendedYield;

              if (record.arg === ContinueSentinel) {
                continue;
              }

              return {
                value: record.arg,
                done: context.done
              };
            } else if (record.type === "throw") {
              state = GenStateCompleted; // Dispatch the exception by looping back around to the
              // context.dispatchException(context.arg) call above.

              context.method = "throw";
              context.arg = record.arg;
            }
          }
        };
      } // Call delegate.iterator[context.method](context.arg) and handle the
      // result, either by returning a { value, done } result from the
      // delegate iterator, or by modifying context.method and context.arg,
      // setting context.delegate to null, and returning the ContinueSentinel.


      function maybeInvokeDelegate(delegate, context) {
        var method = delegate.iterator[context.method];

        if (method === undefined) {
          // A .throw or .return when the delegate iterator has no .throw
          // method always terminates the yield* loop.
          context.delegate = null;

          if (context.method === "throw") {
            // Note: ["return"] must be used for ES3 parsing compatibility.
            if (delegate.iterator["return"]) {
              // If the delegate iterator has a return method, give it a
              // chance to clean up.
              context.method = "return";
              context.arg = undefined;
              maybeInvokeDelegate(delegate, context);

              if (context.method === "throw") {
                // If maybeInvokeDelegate(context) changed context.method from
                // "return" to "throw", let that override the TypeError below.
                return ContinueSentinel;
              }
            }

            context.method = "throw";
            context.arg = new TypeError("The iterator does not provide a 'throw' method");
          }

          return ContinueSentinel;
        }

        var record = tryCatch(method, delegate.iterator, context.arg);

        if (record.type === "throw") {
          context.method = "throw";
          context.arg = record.arg;
          context.delegate = null;
          return ContinueSentinel;
        }

        var info = record.arg;

        if (!info) {
          context.method = "throw";
          context.arg = new TypeError("iterator result is not an object");
          context.delegate = null;
          return ContinueSentinel;
        }

        if (info.done) {
          // Assign the result of the finished delegate to the temporary
          // variable specified by delegate.resultName (see delegateYield).
          context[delegate.resultName] = info.value; // Resume execution at the desired location (see delegateYield).

          context.next = delegate.nextLoc; // If context.method was "throw" but the delegate handled the
          // exception, let the outer generator proceed normally. If
          // context.method was "next", forget context.arg since it has been
          // "consumed" by the delegate iterator. If context.method was
          // "return", allow the original .return call to continue in the
          // outer generator.

          if (context.method !== "return") {
            context.method = "next";
            context.arg = undefined;
          }
        } else {
          // Re-yield the result returned by the delegate method.
          return info;
        } // The delegate iterator is finished, so forget it and continue with
        // the outer generator.


        context.delegate = null;
        return ContinueSentinel;
      } // Define Generator.prototype.{next,throw,return} in terms of the
      // unified ._invoke helper method.


      defineIteratorMethods(Gp);
      Gp[toStringTagSymbol] = "Generator"; // A Generator should always return itself as the iterator object when the
      // @@iterator function is called on it. Some browsers' implementations of the
      // iterator prototype chain incorrectly implement this, causing the Generator
      // object to not be returned from this call. This ensures that doesn't happen.
      // See https://github.com/facebook/regenerator/issues/274 for more details.

      Gp[iteratorSymbol] = function () {
        return this;
      };

      Gp.toString = function () {
        return "[object Generator]";
      };

      function pushTryEntry(locs) {
        var entry = {
          tryLoc: locs[0]
        };

        if (1 in locs) {
          entry.catchLoc = locs[1];
        }

        if (2 in locs) {
          entry.finallyLoc = locs[2];
          entry.afterLoc = locs[3];
        }

        this.tryEntries.push(entry);
      }

      function resetTryEntry(entry) {
        var record = entry.completion || {};
        record.type = "normal";
        delete record.arg;
        entry.completion = record;
      }

      function Context(tryLocsList) {
        // The root entry object (effectively a try statement without a catch
        // or a finally block) gives us a place to store values thrown from
        // locations where there is no enclosing try statement.
        this.tryEntries = [{
          tryLoc: "root"
        }];
        tryLocsList.forEach(pushTryEntry, this);
        this.reset(true);
      }

      exports.keys = function (object) {
        var keys = [];

        for (var key in object) {
          keys.push(key);
        }

        keys.reverse(); // Rather than returning an object with a next method, we keep
        // things simple and return the next function itself.

        return function next() {
          while (keys.length) {
            var key = keys.pop();

            if (key in object) {
              next.value = key;
              next.done = false;
              return next;
            }
          } // To avoid creating an additional object, we just hang the .value
          // and .done properties off the next function object itself. This
          // also ensures that the minifier will not anonymize the function.


          next.done = true;
          return next;
        };
      };

      function values(iterable) {
        if (iterable) {
          var iteratorMethod = iterable[iteratorSymbol];

          if (iteratorMethod) {
            return iteratorMethod.call(iterable);
          }

          if (typeof iterable.next === "function") {
            return iterable;
          }

          if (!isNaN(iterable.length)) {
            var i = -1,
                next = function next() {
              while (++i < iterable.length) {
                if (hasOwn.call(iterable, i)) {
                  next.value = iterable[i];
                  next.done = false;
                  return next;
                }
              }

              next.value = undefined;
              next.done = true;
              return next;
            };

            return next.next = next;
          }
        } // Return an iterator with no values.


        return {
          next: doneResult
        };
      }

      exports.values = values;

      function doneResult() {
        return {
          value: undefined,
          done: true
        };
      }

      Context.prototype = {
        constructor: Context,
        reset: function reset(skipTempReset) {
          this.prev = 0;
          this.next = 0; // Resetting context._sent for legacy support of Babel's
          // function.sent implementation.

          this.sent = this._sent = undefined;
          this.done = false;
          this.delegate = null;
          this.method = "next";
          this.arg = undefined;
          this.tryEntries.forEach(resetTryEntry);

          if (!skipTempReset) {
            for (var name in this) {
              // Not sure about the optimal order of these conditions:
              if (name.charAt(0) === "t" && hasOwn.call(this, name) && !isNaN(+name.slice(1))) {
                this[name] = undefined;
              }
            }
          }
        },
        stop: function stop() {
          this.done = true;
          var rootEntry = this.tryEntries[0];
          var rootRecord = rootEntry.completion;

          if (rootRecord.type === "throw") {
            throw rootRecord.arg;
          }

          return this.rval;
        },
        dispatchException: function dispatchException(exception) {
          if (this.done) {
            throw exception;
          }

          var context = this;

          function handle(loc, caught) {
            record.type = "throw";
            record.arg = exception;
            context.next = loc;

            if (caught) {
              // If the dispatched exception was caught by a catch block,
              // then let that catch block handle the exception normally.
              context.method = "next";
              context.arg = undefined;
            }

            return !!caught;
          }

          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];
            var record = entry.completion;

            if (entry.tryLoc === "root") {
              // Exception thrown outside of any try block that could handle
              // it, so set the completion value of the entire function to
              // throw the exception.
              return handle("end");
            }

            if (entry.tryLoc <= this.prev) {
              var hasCatch = hasOwn.call(entry, "catchLoc");
              var hasFinally = hasOwn.call(entry, "finallyLoc");

              if (hasCatch && hasFinally) {
                if (this.prev < entry.catchLoc) {
                  return handle(entry.catchLoc, true);
                } else if (this.prev < entry.finallyLoc) {
                  return handle(entry.finallyLoc);
                }
              } else if (hasCatch) {
                if (this.prev < entry.catchLoc) {
                  return handle(entry.catchLoc, true);
                }
              } else if (hasFinally) {
                if (this.prev < entry.finallyLoc) {
                  return handle(entry.finallyLoc);
                }
              } else {
                throw new Error("try statement without catch or finally");
              }
            }
          }
        },
        abrupt: function abrupt(type, arg) {
          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];

            if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
              var finallyEntry = entry;
              break;
            }
          }

          if (finallyEntry && (type === "break" || type === "continue") && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc) {
            // Ignore the finally entry if control is not jumping to a
            // location outside the try/catch block.
            finallyEntry = null;
          }

          var record = finallyEntry ? finallyEntry.completion : {};
          record.type = type;
          record.arg = arg;

          if (finallyEntry) {
            this.method = "next";
            this.next = finallyEntry.finallyLoc;
            return ContinueSentinel;
          }

          return this.complete(record);
        },
        complete: function complete(record, afterLoc) {
          if (record.type === "throw") {
            throw record.arg;
          }

          if (record.type === "break" || record.type === "continue") {
            this.next = record.arg;
          } else if (record.type === "return") {
            this.rval = this.arg = record.arg;
            this.method = "return";
            this.next = "end";
          } else if (record.type === "normal" && afterLoc) {
            this.next = afterLoc;
          }

          return ContinueSentinel;
        },
        finish: function finish(finallyLoc) {
          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];

            if (entry.finallyLoc === finallyLoc) {
              this.complete(entry.completion, entry.afterLoc);
              resetTryEntry(entry);
              return ContinueSentinel;
            }
          }
        },
        "catch": function _catch(tryLoc) {
          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];

            if (entry.tryLoc === tryLoc) {
              var record = entry.completion;

              if (record.type === "throw") {
                var thrown = record.arg;
                resetTryEntry(entry);
              }

              return thrown;
            }
          } // The context.catch method must only be called with a location
          // argument that corresponds to a known catch block.


          throw new Error("illegal catch attempt");
        },
        delegateYield: function delegateYield(iterable, resultName, nextLoc) {
          this.delegate = {
            iterator: values(iterable),
            resultName: resultName,
            nextLoc: nextLoc
          };

          if (this.method === "next") {
            // Deliberately forget the last sent value so that we don't
            // accidentally pass it on to the delegate.
            this.arg = undefined;
          }

          return ContinueSentinel;
        }
      }; // Regardless of whether this script is executing as a CommonJS module
      // or not, return the runtime object so that we can declare the variable
      // regeneratorRuntime in the outer scope, which allows this module to be
      // injected easily by `bin/regenerator --include-runtime script.js`.

      return exports;
    }( // If this script is executing as a CommonJS module, use module.exports
    // as the regeneratorRuntime namespace. Otherwise create a new empty
    // object. Either way, the resulting object will be used to initialize
    // the regeneratorRuntime variable at the top of this file.
    _typeof(module) === "object" ? module.exports : {});

    try {
      regeneratorRuntime = runtime;
    } catch (accidentalStrictMode) {
      // This module should not be running in strict mode, so the above
      // assignment should always work unless something is misconfigured. Just
      // in case runtime.js accidentally runs in strict mode, we can escape
      // strict mode using a global Function call. This could conceivably fail
      // if a Content Security Policy forbids using Function, but in that case
      // the proper solution is to fix the accidental strict mode problem. If
      // you've misconfigured your bundler to force strict mode and applied a
      // CSP to forbid Function, and you're not willing to fix either of those
      // problems, please detail your unique predicament in a GitHub issue.
      Function("r", "regeneratorRuntime = r")(runtime);
    }
  }, {}],
  306: [function (_dereq_, module, exports) {
    "use strict";

    _dereq_(307);

    var _global = _interopRequireDefault(_dereq_(13));

    function _interopRequireDefault(obj) {
      return obj && obj.__esModule ? obj : {
        "default": obj
      };
    }

    if (_global["default"]._babelPolyfill && typeof console !== "undefined" && console.warn) {
      console.warn("@babel/polyfill is loaded more than once on this page. This is probably not desirable/intended " + "and may have consequences if different versions of the polyfills are applied sequentially. " + "If you do need to load the polyfill more than once, use @babel/polyfill/noConflict " + "instead to bypass the warning.");
    }

    _global["default"]._babelPolyfill = true;
  }, {
    "13": 13,
    "307": 307
  }],
  307: [function (_dereq_, module, exports) {
    "use strict";

    _dereq_(1);

    _dereq_(3);

    _dereq_(2);

    _dereq_(9);

    _dereq_(8);

    _dereq_(11);

    _dereq_(10);

    _dereq_(12);

    _dereq_(5);

    _dereq_(6);

    _dereq_(4);

    _dereq_(7);

    _dereq_(304);

    _dereq_(305);
  }, {
    "1": 1,
    "10": 10,
    "11": 11,
    "12": 12,
    "2": 2,
    "3": 3,
    "304": 304,
    "305": 305,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9
  }]
}, {}, [306]);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2016
 * @version 1.3.4
 *
 * Date formatter utility library that allows formatting date/time variables or Date objects using PHP DateTime format.
 * @see http://php.net/manual/en/function.date.php
 *
 * For more JQuery plugins visit http://plugins.krajee.com
 * For more Yii related demos visit http://demos.krajee.com
 */
var DateFormatter;
!function () {
  "use strict";

  var t, _e, _r, n, a, u, i;

  u = 864e5, i = 3600, t = function t(_t, e) {
    return "string" == typeof _t && "string" == typeof e && _t.toLowerCase() === e.toLowerCase();
  }, _e = function e(t, r, n) {
    var a = n || "0",
        u = t.toString();
    return u.length < r ? _e(a + u, r) : u;
  }, _r = function r(t) {
    var e, n;

    for (t = t || {}, e = 1; e < arguments.length; e++) {
      if (n = arguments[e]) for (var a in n) {
        n.hasOwnProperty(a) && ("object" == _typeof(n[a]) ? _r(t[a], n[a]) : t[a] = n[a]);
      }
    }

    return t;
  }, n = function n(t, e) {
    for (var r = 0; r < e.length; r++) {
      if (e[r].toLowerCase() === t.toLowerCase()) return r;
    }

    return -1;
  }, a = {
    dateSettings: {
      days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      meridiem: ["AM", "PM"],
      ordinal: function ordinal(t) {
        var e = t % 10,
            r = {
          1: "st",
          2: "nd",
          3: "rd"
        };
        return 1 !== Math.floor(t % 100 / 10) && r[e] ? r[e] : "th";
      }
    },
    separators: /[ \-+\/\.T:@]/g,
    validParts: /[dDjlNSwzWFmMntLoYyaABgGhHisueTIOPZcrU]/g,
    intParts: /[djwNzmnyYhHgGis]/g,
    tzParts: /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
    tzClip: /[^-+\dA-Z]/g
  }, DateFormatter = function DateFormatter(t) {
    var e = this,
        n = _r(a, t);

    e.dateSettings = n.dateSettings, e.separators = n.separators, e.validParts = n.validParts, e.intParts = n.intParts, e.tzParts = n.tzParts, e.tzClip = n.tzClip;
  }, DateFormatter.prototype = {
    constructor: DateFormatter,
    getMonth: function getMonth(t) {
      var e,
          r = this;
      return e = n(t, r.dateSettings.monthsShort) + 1, 0 === e && (e = n(t, r.dateSettings.months) + 1), e;
    },
    parseDate: function parseDate(e, r) {
      var n,
          a,
          u,
          i,
          s,
          o,
          c,
          f,
          l,
          h,
          d = this,
          g = !1,
          m = !1,
          p = d.dateSettings,
          y = {
        date: null,
        year: null,
        month: null,
        day: null,
        hour: 0,
        min: 0,
        sec: 0
      };
      if (!e) return null;
      if (e instanceof Date) return e;
      if ("U" === r) return u = parseInt(e), u ? new Date(1e3 * u) : e;

      switch (_typeof(e)) {
        case "number":
          return new Date(e);

        case "string":
          break;

        default:
          return null;
      }

      if (n = r.match(d.validParts), !n || 0 === n.length) throw new Error("Invalid date format definition.");

      for (a = e.replace(d.separators, "\x00").split("\x00"), u = 0; u < a.length; u++) {
        switch (i = a[u], s = parseInt(i), n[u]) {
          case "y":
          case "Y":
            if (!s) return null;
            l = i.length, y.year = 2 === l ? parseInt((70 > s ? "20" : "19") + i) : s, g = !0;
            break;

          case "m":
          case "n":
          case "M":
          case "F":
            if (isNaN(s)) {
              if (o = d.getMonth(i), !(o > 0)) return null;
              y.month = o;
            } else {
              if (!(s >= 1 && 12 >= s)) return null;
              y.month = s;
            }

            g = !0;
            break;

          case "d":
          case "j":
            if (!(s >= 1 && 31 >= s)) return null;
            y.day = s, g = !0;
            break;

          case "g":
          case "h":
            if (c = n.indexOf("a") > -1 ? n.indexOf("a") : n.indexOf("A") > -1 ? n.indexOf("A") : -1, h = a[c], c > -1) f = t(h, p.meridiem[0]) ? 0 : t(h, p.meridiem[1]) ? 12 : -1, s >= 1 && 12 >= s && f > -1 ? y.hour = s + f - 1 : s >= 0 && 23 >= s && (y.hour = s);else {
              if (!(s >= 0 && 23 >= s)) return null;
              y.hour = s;
            }
            m = !0;
            break;

          case "G":
          case "H":
            if (!(s >= 0 && 23 >= s)) return null;
            y.hour = s, m = !0;
            break;

          case "i":
            if (!(s >= 0 && 59 >= s)) return null;
            y.min = s, m = !0;
            break;

          case "s":
            if (!(s >= 0 && 59 >= s)) return null;
            y.sec = s, m = !0;
        }
      }

      if (g === !0 && y.year && y.month && y.day) y.date = new Date(y.year, y.month - 1, y.day, y.hour, y.min, y.sec, 0);else {
        if (m !== !0) return null;
        y.date = new Date(0, 0, 0, y.hour, y.min, y.sec, 0);
      }
      return y.date;
    },
    guessDate: function guessDate(t, e) {
      if ("string" != typeof t) return t;
      var r,
          n,
          a,
          u,
          i,
          s,
          o = this,
          c = t.replace(o.separators, "\x00").split("\x00"),
          f = /^[djmn]/g,
          l = e.match(o.validParts),
          h = new Date(),
          d = 0;
      if (!f.test(l[0])) return t;

      for (a = 0; a < c.length; a++) {
        if (d = 2, i = c[a], s = parseInt(i.substr(0, 2)), isNaN(s)) return null;

        switch (a) {
          case 0:
            "m" === l[0] || "n" === l[0] ? h.setMonth(s - 1) : h.setDate(s);
            break;

          case 1:
            "m" === l[0] || "n" === l[0] ? h.setDate(s) : h.setMonth(s - 1);
            break;

          case 2:
            if (n = h.getFullYear(), r = i.length, d = 4 > r ? r : 4, n = parseInt(4 > r ? n.toString().substr(0, 4 - r) + i : i.substr(0, 4)), !n) return null;
            h.setFullYear(n);
            break;

          case 3:
            h.setHours(s);
            break;

          case 4:
            h.setMinutes(s);
            break;

          case 5:
            h.setSeconds(s);
        }

        u = i.substr(d), u.length > 0 && c.splice(a + 1, 0, u);
      }

      return h;
    },
    parseFormat: function parseFormat(t, r) {
      var n,
          a = this,
          s = a.dateSettings,
          o = /\\?(.?)/gi,
          _c = function c(t, e) {
        return n[t] ? n[t]() : e;
      };

      return n = {
        d: function d() {
          return _e(n.j(), 2);
        },
        D: function D() {
          return s.daysShort[n.w()];
        },
        j: function j() {
          return r.getDate();
        },
        l: function l() {
          return s.days[n.w()];
        },
        N: function N() {
          return n.w() || 7;
        },
        w: function w() {
          return r.getDay();
        },
        z: function z() {
          var t = new Date(n.Y(), n.n() - 1, n.j()),
              e = new Date(n.Y(), 0, 1);
          return Math.round((t - e) / u);
        },
        W: function W() {
          var t = new Date(n.Y(), n.n() - 1, n.j() - n.N() + 3),
              r = new Date(t.getFullYear(), 0, 4);
          return _e(1 + Math.round((t - r) / u / 7), 2);
        },
        F: function F() {
          return s.months[r.getMonth()];
        },
        m: function m() {
          return _e(n.n(), 2);
        },
        M: function M() {
          return s.monthsShort[r.getMonth()];
        },
        n: function n() {
          return r.getMonth() + 1;
        },
        t: function t() {
          return new Date(n.Y(), n.n(), 0).getDate();
        },
        L: function L() {
          var t = n.Y();
          return t % 4 === 0 && t % 100 !== 0 || t % 400 === 0 ? 1 : 0;
        },
        o: function o() {
          var t = n.n(),
              e = n.W(),
              r = n.Y();
          return r + (12 === t && 9 > e ? 1 : 1 === t && e > 9 ? -1 : 0);
        },
        Y: function Y() {
          return r.getFullYear();
        },
        y: function y() {
          return n.Y().toString().slice(-2);
        },
        a: function a() {
          return n.A().toLowerCase();
        },
        A: function A() {
          var t = n.G() < 12 ? 0 : 1;
          return s.meridiem[t];
        },
        B: function B() {
          var t = r.getUTCHours() * i,
              n = 60 * r.getUTCMinutes(),
              a = r.getUTCSeconds();
          return _e(Math.floor((t + n + a + i) / 86.4) % 1e3, 3);
        },
        g: function g() {
          return n.G() % 12 || 12;
        },
        G: function G() {
          return r.getHours();
        },
        h: function h() {
          return _e(n.g(), 2);
        },
        H: function H() {
          return _e(n.G(), 2);
        },
        i: function i() {
          return _e(r.getMinutes(), 2);
        },
        s: function s() {
          return _e(r.getSeconds(), 2);
        },
        u: function u() {
          return _e(1e3 * r.getMilliseconds(), 6);
        },
        e: function e() {
          var t = /\((.*)\)/.exec(String(r))[1];
          return t || "Coordinated Universal Time";
        },
        I: function I() {
          var t = new Date(n.Y(), 0),
              e = Date.UTC(n.Y(), 0),
              r = new Date(n.Y(), 6),
              a = Date.UTC(n.Y(), 6);
          return t - e !== r - a ? 1 : 0;
        },
        O: function O() {
          var t = r.getTimezoneOffset(),
              n = Math.abs(t);
          return (t > 0 ? "-" : "+") + _e(100 * Math.floor(n / 60) + n % 60, 4);
        },
        P: function P() {
          var t = n.O();
          return t.substr(0, 3) + ":" + t.substr(3, 2);
        },
        T: function T() {
          var t = (String(r).match(a.tzParts) || [""]).pop().replace(a.tzClip, "");
          return t || "UTC";
        },
        Z: function Z() {
          return 60 * -r.getTimezoneOffset();
        },
        c: function c() {
          return "Y-m-d\\TH:i:sP".replace(o, _c);
        },
        r: function r() {
          return "D, d M Y H:i:s O".replace(o, _c);
        },
        U: function U() {
          return r.getTime() / 1e3 || 0;
        }
      }, _c(t, t);
    },
    formatDate: function formatDate(t, e) {
      var r,
          n,
          a,
          u,
          i,
          s = this,
          o = "",
          c = "\\";
      if ("string" == typeof t && (t = s.parseDate(t, e), !t)) return null;

      if (t instanceof Date) {
        for (a = e.length, r = 0; a > r; r++) {
          i = e.charAt(r), "S" !== i && i !== c && (r > 0 && e.charAt(r - 1) === c ? o += i : (u = s.parseFormat(i, t), r !== a - 1 && s.intParts.test(i) && "S" === e.charAt(r + 1) && (n = parseInt(u) || 0, u += s.dateSettings.ordinal(n)), o += u));
        }

        return o;
      }

      return "";
    }
  };
}();
/**
 * @preserve jQuery DateTimePicker
 * @homepage http://xdsoft.net/jqplugins/datetimepicker/
 * @author Chupurnov Valeriy (<chupurnov@gmail.com>)
 */

/**
 * @param {jQuery} $
 */

var datetimepickerFactory = function datetimepickerFactory($) {
  'use strict';

  var default_options = {
    i18n: {
      ar: {
        // Arabic
        months: ["كانون الثاني", "شباط", "آذار", "نيسان", "مايو", "حزيران", "تموز", "آب", "أيلول", "تشرين الأول", "تشرين الثاني", "كانون الأول"],
        dayOfWeekShort: ["ن", "ث", "ع", "خ", "ج", "س", "ح"],
        dayOfWeek: ["الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت", "الأحد"]
      },
      ro: {
        // Romanian
        months: ["Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"],
        dayOfWeekShort: ["Du", "Lu", "Ma", "Mi", "Jo", "Vi", "Sâ"],
        dayOfWeek: ["Duminică", "Luni", "Marţi", "Miercuri", "Joi", "Vineri", "Sâmbătă"]
      },
      id: {
        // Indonesian
        months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        dayOfWeekShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
        dayOfWeek: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"]
      },
      is: {
        // Icelandic
        months: ["Janúar", "Febrúar", "Mars", "Apríl", "Maí", "Júní", "Júlí", "Ágúst", "September", "Október", "Nóvember", "Desember"],
        dayOfWeekShort: ["Sun", "Mán", "Þrið", "Mið", "Fim", "Fös", "Lau"],
        dayOfWeek: ["Sunnudagur", "Mánudagur", "Þriðjudagur", "Miðvikudagur", "Fimmtudagur", "Föstudagur", "Laugardagur"]
      },
      bg: {
        // Bulgarian
        months: ["Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември"],
        dayOfWeekShort: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        dayOfWeek: ["Неделя", "Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък", "Събота"]
      },
      fa: {
        // Persian/Farsi
        months: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],
        dayOfWeekShort: ['یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه', 'شنبه'],
        dayOfWeek: ["یک‌شنبه", "دوشنبه", "سه‌شنبه", "چهارشنبه", "پنج‌شنبه", "جمعه", "شنبه", "یک‌شنبه"]
      },
      ru: {
        // Russian
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        dayOfWeekShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        dayOfWeek: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"]
      },
      uk: {
        // Ukrainian
        months: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
        dayOfWeekShort: ["Ндл", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Сбт"],
        dayOfWeek: ["Неділя", "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота"]
      },
      en: {
        // English
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        dayOfWeekShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
      },
      el: {
        // Ελληνικά
        months: ["Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"],
        dayOfWeekShort: ["Κυρ", "Δευ", "Τρι", "Τετ", "Πεμ", "Παρ", "Σαβ"],
        dayOfWeek: ["Κυριακή", "Δευτέρα", "Τρίτη", "Τετάρτη", "Πέμπτη", "Παρασκευή", "Σάββατο"]
      },
      de: {
        // German
        months: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        dayOfWeekShort: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
        dayOfWeek: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"]
      },
      nl: {
        // Dutch
        months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
        dayOfWeekShort: ["zo", "ma", "di", "wo", "do", "vr", "za"],
        dayOfWeek: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"]
      },
      tr: {
        // Turkish
        months: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayOfWeekShort: ["Paz", "Pts", "Sal", "Çar", "Per", "Cum", "Cts"],
        dayOfWeek: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"]
      },
      fr: {
        //French
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        dayOfWeekShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
        dayOfWeek: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"]
      },
      es: {
        // Spanish
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        dayOfWeekShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
        dayOfWeek: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]
      },
      th: {
        // Thai
        months: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
        dayOfWeekShort: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
        dayOfWeek: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์", "อาทิตย์"]
      },
      pl: {
        // Polish
        months: ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"],
        dayOfWeekShort: ["nd", "pn", "wt", "śr", "cz", "pt", "sb"],
        dayOfWeek: ["niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"]
      },
      pt: {
        // Portuguese
        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        dayOfWeekShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
        dayOfWeek: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"]
      },
      ch: {
        // Simplified Chinese
        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"]
      },
      se: {
        // Swedish
        months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
        dayOfWeekShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"]
      },
      km: {
        // Khmer (ភាសាខ្មែរ)
        months: ["មករា​", "កុម្ភៈ", "មិនា​", "មេសា​", "ឧសភា​", "មិថុនា​", "កក្កដា​", "សីហា​", "កញ្ញា​", "តុលា​", "វិច្ឆិកា", "ធ្នូ​"],
        dayOfWeekShort: ["អាទិ​", "ច័ន្ទ​", "អង្គារ​", "ពុធ​", "ព្រហ​​", "សុក្រ​", "សៅរ៍"],
        dayOfWeek: ["អាទិត្យ​", "ច័ន្ទ​", "អង្គារ​", "ពុធ​", "ព្រហស្បតិ៍​", "សុក្រ​", "សៅរ៍"]
      },
      kr: {
        // Korean
        months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
        dayOfWeekShort: ["일", "월", "화", "수", "목", "금", "토"],
        dayOfWeek: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"]
      },
      it: {
        // Italian
        months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
        dayOfWeekShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
        dayOfWeek: ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"]
      },
      da: {
        // Dansk
        months: ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"],
        dayOfWeekShort: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
        dayOfWeek: ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"]
      },
      no: {
        // Norwegian
        months: ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"],
        dayOfWeekShort: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
        dayOfWeek: ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag']
      },
      ja: {
        // Japanese
        months: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
        dayOfWeekShort: ["日", "月", "火", "水", "木", "金", "土"],
        dayOfWeek: ["日曜", "月曜", "火曜", "水曜", "木曜", "金曜", "土曜"]
      },
      vi: {
        // Vietnamese
        months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        dayOfWeekShort: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        dayOfWeek: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"]
      },
      sl: {
        // Slovenščina
        months: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
        dayOfWeekShort: ["Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob"],
        dayOfWeek: ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Četrtek", "Petek", "Sobota"]
      },
      cs: {
        // Čeština
        months: ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"],
        dayOfWeekShort: ["Ne", "Po", "Út", "St", "Čt", "Pá", "So"]
      },
      hu: {
        // Hungarian
        months: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
        dayOfWeekShort: ["Va", "Hé", "Ke", "Sze", "Cs", "Pé", "Szo"],
        dayOfWeek: ["vasárnap", "hétfő", "kedd", "szerda", "csütörtök", "péntek", "szombat"]
      },
      az: {
        //Azerbaijanian (Azeri)
        months: ["Yanvar", "Fevral", "Mart", "Aprel", "May", "Iyun", "Iyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"],
        dayOfWeekShort: ["B", "Be", "Ça", "Ç", "Ca", "C", "Ş"],
        dayOfWeek: ["Bazar", "Bazar ertəsi", "Çərşənbə axşamı", "Çərşənbə", "Cümə axşamı", "Cümə", "Şənbə"]
      },
      bs: {
        //Bosanski
        months: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
        dayOfWeekShort: ["Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub"],
        dayOfWeek: ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota"]
      },
      ca: {
        //Català
        months: ["Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"],
        dayOfWeekShort: ["Dg", "Dl", "Dt", "Dc", "Dj", "Dv", "Ds"],
        dayOfWeek: ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"]
      },
      'en-GB': {
        //English (British)
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        dayOfWeekShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
      },
      et: {
        //"Eesti"
        months: ["Jaanuar", "Veebruar", "Märts", "Aprill", "Mai", "Juuni", "Juuli", "August", "September", "Oktoober", "November", "Detsember"],
        dayOfWeekShort: ["P", "E", "T", "K", "N", "R", "L"],
        dayOfWeek: ["Pühapäev", "Esmaspäev", "Teisipäev", "Kolmapäev", "Neljapäev", "Reede", "Laupäev"]
      },
      eu: {
        //Euskara
        months: ["Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua"],
        dayOfWeekShort: ["Ig.", "Al.", "Ar.", "Az.", "Og.", "Or.", "La."],
        dayOfWeek: ['Igandea', 'Astelehena', 'Asteartea', 'Asteazkena', 'Osteguna', 'Ostirala', 'Larunbata']
      },
      fi: {
        //Finnish (Suomi)
        months: ["Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "Kesäkuu", "Heinäkuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"],
        dayOfWeekShort: ["Su", "Ma", "Ti", "Ke", "To", "Pe", "La"],
        dayOfWeek: ["sunnuntai", "maanantai", "tiistai", "keskiviikko", "torstai", "perjantai", "lauantai"]
      },
      gl: {
        //Galego
        months: ["Xan", "Feb", "Maz", "Abr", "Mai", "Xun", "Xul", "Ago", "Set", "Out", "Nov", "Dec"],
        dayOfWeekShort: ["Dom", "Lun", "Mar", "Mer", "Xov", "Ven", "Sab"],
        dayOfWeek: ["Domingo", "Luns", "Martes", "Mércores", "Xoves", "Venres", "Sábado"]
      },
      hr: {
        //Hrvatski
        months: ["Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"],
        dayOfWeekShort: ["Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub"],
        dayOfWeek: ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota"]
      },
      ko: {
        //Korean (한국어)
        months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
        dayOfWeekShort: ["일", "월", "화", "수", "목", "금", "토"],
        dayOfWeek: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"]
      },
      lt: {
        //Lithuanian (lietuvių)
        months: ["Sausio", "Vasario", "Kovo", "Balandžio", "Gegužės", "Birželio", "Liepos", "Rugpjūčio", "Rugsėjo", "Spalio", "Lapkričio", "Gruodžio"],
        dayOfWeekShort: ["Sek", "Pir", "Ant", "Tre", "Ket", "Pen", "Šeš"],
        dayOfWeek: ["Sekmadienis", "Pirmadienis", "Antradienis", "Trečiadienis", "Ketvirtadienis", "Penktadienis", "Šeštadienis"]
      },
      lv: {
        //Latvian (Latviešu)
        months: ["Janvāris", "Februāris", "Marts", "Aprīlis ", "Maijs", "Jūnijs", "Jūlijs", "Augusts", "Septembris", "Oktobris", "Novembris", "Decembris"],
        dayOfWeekShort: ["Sv", "Pr", "Ot", "Tr", "Ct", "Pk", "St"],
        dayOfWeek: ["Svētdiena", "Pirmdiena", "Otrdiena", "Trešdiena", "Ceturtdiena", "Piektdiena", "Sestdiena"]
      },
      mk: {
        //Macedonian (Македонски)
        months: ["јануари", "февруари", "март", "април", "мај", "јуни", "јули", "август", "септември", "октомври", "ноември", "декември"],
        dayOfWeekShort: ["нед", "пон", "вто", "сре", "чет", "пет", "саб"],
        dayOfWeek: ["Недела", "Понеделник", "Вторник", "Среда", "Четврток", "Петок", "Сабота"]
      },
      mn: {
        //Mongolian (Монгол)
        months: ["1-р сар", "2-р сар", "3-р сар", "4-р сар", "5-р сар", "6-р сар", "7-р сар", "8-р сар", "9-р сар", "10-р сар", "11-р сар", "12-р сар"],
        dayOfWeekShort: ["Дав", "Мяг", "Лха", "Пүр", "Бсн", "Бям", "Ням"],
        dayOfWeek: ["Даваа", "Мягмар", "Лхагва", "Пүрэв", "Баасан", "Бямба", "Ням"]
      },
      'pt-BR': {
        //Português(Brasil)
        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        dayOfWeekShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
        dayOfWeek: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"]
      },
      sk: {
        //Slovenčina
        months: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
        dayOfWeekShort: ["Ne", "Po", "Ut", "St", "Št", "Pi", "So"],
        dayOfWeek: ["Nedeľa", "Pondelok", "Utorok", "Streda", "Štvrtok", "Piatok", "Sobota"]
      },
      sq: {
        //Albanian (Shqip)
        months: ["Janar", "Shkurt", "Mars", "Prill", "Maj", "Qershor", "Korrik", "Gusht", "Shtator", "Tetor", "Nëntor", "Dhjetor"],
        dayOfWeekShort: ["Die", "Hën", "Mar", "Mër", "Enj", "Pre", "Shtu"],
        dayOfWeek: ["E Diel", "E Hënë", "E Martē", "E Mërkurë", "E Enjte", "E Premte", "E Shtunë"]
      },
      'sr-YU': {
        //Serbian (Srpski)
        months: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
        dayOfWeekShort: ["Ned", "Pon", "Uto", "Sre", "čet", "Pet", "Sub"],
        dayOfWeek: ["Nedelja", "Ponedeljak", "Utorak", "Sreda", "Četvrtak", "Petak", "Subota"]
      },
      sr: {
        //Serbian Cyrillic (Српски)
        months: ["јануар", "фебруар", "март", "април", "мај", "јун", "јул", "август", "септембар", "октобар", "новембар", "децембар"],
        dayOfWeekShort: ["нед", "пон", "уто", "сре", "чет", "пет", "суб"],
        dayOfWeek: ["Недеља", "Понедељак", "Уторак", "Среда", "Четвртак", "Петак", "Субота"]
      },
      sv: {
        //Svenska
        months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
        dayOfWeekShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"],
        dayOfWeek: ["Söndag", "Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag"]
      },
      'zh-TW': {
        //Traditional Chinese (繁體中文)
        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"],
        dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
      },
      zh: {
        //Simplified Chinese (简体中文)
        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"],
        dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
      },
      ug: {
        // Uyghur(ئۇيغۇرچە)
        months: ["1-ئاي", "2-ئاي", "3-ئاي", "4-ئاي", "5-ئاي", "6-ئاي", "7-ئاي", "8-ئاي", "9-ئاي", "10-ئاي", "11-ئاي", "12-ئاي"],
        dayOfWeek: ["يەكشەنبە", "دۈشەنبە", "سەيشەنبە", "چارشەنبە", "پەيشەنبە", "جۈمە", "شەنبە"]
      },
      he: {
        //Hebrew (עברית)
        months: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'],
        dayOfWeekShort: ['א\'', 'ב\'', 'ג\'', 'ד\'', 'ה\'', 'ו\'', 'שבת'],
        dayOfWeek: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת", "ראשון"]
      },
      hy: {
        // Armenian
        months: ["Հունվար", "Փետրվար", "Մարտ", "Ապրիլ", "Մայիս", "Հունիս", "Հուլիս", "Օգոստոս", "Սեպտեմբեր", "Հոկտեմբեր", "Նոյեմբեր", "Դեկտեմբեր"],
        dayOfWeekShort: ["Կի", "Երկ", "Երք", "Չոր", "Հնգ", "Ուրբ", "Շբթ"],
        dayOfWeek: ["Կիրակի", "Երկուշաբթի", "Երեքշաբթի", "Չորեքշաբթի", "Հինգշաբթի", "Ուրբաթ", "Շաբաթ"]
      },
      kg: {
        // Kyrgyz
        months: ['Үчтүн айы', 'Бирдин айы', 'Жалган Куран', 'Чын Куран', 'Бугу', 'Кулжа', 'Теке', 'Баш Оона', 'Аяк Оона', 'Тогуздун айы', 'Жетинин айы', 'Бештин айы'],
        dayOfWeekShort: ["Жек", "Дүй", "Шей", "Шар", "Бей", "Жум", "Ише"],
        dayOfWeek: ["Жекшемб", "Дүйшөмб", "Шейшемб", "Шаршемб", "Бейшемби", "Жума", "Ишенб"]
      },
      rm: {
        // Romansh
        months: ["Schaner", "Favrer", "Mars", "Avrigl", "Matg", "Zercladur", "Fanadur", "Avust", "Settember", "October", "November", "December"],
        dayOfWeekShort: ["Du", "Gli", "Ma", "Me", "Gie", "Ve", "So"],
        dayOfWeek: ["Dumengia", "Glindesdi", "Mardi", "Mesemna", "Gievgia", "Venderdi", "Sonda"]
      },
      ka: {
        // Georgian
        months: ['იანვარი', 'თებერვალი', 'მარტი', 'აპრილი', 'მაისი', 'ივნისი', 'ივლისი', 'აგვისტო', 'სექტემბერი', 'ოქტომბერი', 'ნოემბერი', 'დეკემბერი'],
        dayOfWeekShort: ["კვ", "ორშ", "სამშ", "ოთხ", "ხუთ", "პარ", "შაბ"],
        dayOfWeek: ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი"]
      }
    },
    ownerDocument: document,
    contentWindow: window,
    value: '',
    rtl: false,
    format: 'Y/m/d H:i',
    formatTime: 'H:i',
    formatDate: 'Y/m/d',
    startDate: false,
    // new Date(), '1986/12/08', '-1970/01/05','-1970/01/05',
    step: 60,
    monthChangeSpinner: true,
    closeOnDateSelect: false,
    closeOnTimeSelect: true,
    closeOnWithoutClick: true,
    closeOnInputClick: true,
    openOnFocus: true,
    timepicker: true,
    datepicker: true,
    weeks: false,
    defaultTime: false,
    // use formatTime format (ex. '10:00' for formatTime:	'H:i')
    defaultDate: false,
    // use formatDate format (ex new Date() or '1986/12/08' or '-1970/01/05' or '-1970/01/05')
    minDate: false,
    maxDate: false,
    minTime: false,
    maxTime: false,
    minDateTime: false,
    maxDateTime: false,
    allowTimes: [],
    opened: false,
    initTime: true,
    inline: false,
    theme: '',
    touchMovedThreshold: 5,
    onSelectDate: function onSelectDate() {},
    onSelectTime: function onSelectTime() {},
    onChangeMonth: function onChangeMonth() {},
    onGetWeekOfYear: function onGetWeekOfYear() {},
    onChangeYear: function onChangeYear() {},
    onChangeDateTime: function onChangeDateTime() {},
    onShow: function onShow() {},
    onClose: function onClose() {},
    onGenerate: function onGenerate() {},
    withoutCopyright: true,
    inverseButton: false,
    hours12: false,
    next: 'xdsoft_next',
    prev: 'xdsoft_prev',
    dayOfWeekStart: 0,
    parentID: 'body',
    timeHeightInTimePicker: 25,
    timepickerScrollbar: true,
    todayButton: true,
    prevButton: true,
    nextButton: true,
    defaultSelect: true,
    scrollMonth: true,
    scrollTime: true,
    scrollInput: true,
    lazyInit: false,
    mask: false,
    validateOnBlur: true,
    allowBlank: true,
    yearStart: 1950,
    yearEnd: 2050,
    monthStart: 0,
    monthEnd: 11,
    style: '',
    id: '',
    fixed: false,
    roundTime: 'round',
    // ceil, floor
    className: '',
    weekends: [],
    highlightedDates: [],
    highlightedPeriods: [],
    allowDates: [],
    allowDateRe: null,
    disabledDates: [],
    disabledWeekDays: [],
    yearOffset: 0,
    beforeShowDay: null,
    enterLikeTab: true,
    showApplyButton: false,
    insideParent: false
  };
  var dateHelper = null,
      defaultDateHelper = null,
      globalLocaleDefault = 'en',
      globalLocale = 'en';
  var dateFormatterOptionsDefault = {
    meridiem: ['AM', 'PM']
  };

  var initDateFormatter = function initDateFormatter() {
    var locale = default_options.i18n[globalLocale],
        opts = {
      days: locale.dayOfWeek,
      daysShort: locale.dayOfWeekShort,
      months: locale.months,
      monthsShort: djQ.map(locale.months, function (n) {
        return n.substring(0, 3);
      })
    };

    if (typeof DateFormatter === 'function') {
      dateHelper = defaultDateHelper = new DateFormatter({
        dateSettings: djQ.extend({}, dateFormatterOptionsDefault, opts)
      });
    }
  };

  var dateFormatters = {
    moment: {
      default_options: {
        format: 'YYYY/MM/DD HH:mm',
        formatDate: 'YYYY/MM/DD',
        formatTime: 'HH:mm'
      },
      formatter: {
        parseDate: function parseDate(date, format) {
          if (isFormatStandard(format)) {
            return defaultDateHelper.parseDate(date, format);
          }

          var d = moment(date, format);
          return d.isValid() ? d.toDate() : false;
        },
        formatDate: function formatDate(date, format) {
          if (isFormatStandard(format)) {
            return defaultDateHelper.formatDate(date, format);
          }

          return moment(date).format(format);
        },
        formatMask: function formatMask(format) {
          return format.replace(/Y{4}/g, '9999').replace(/Y{2}/g, '99').replace(/M{2}/g, '19').replace(/D{2}/g, '39').replace(/H{2}/g, '29').replace(/m{2}/g, '59').replace(/s{2}/g, '59');
        }
      }
    }
  }; // for locale settings

  djQ.datetimepicker = {
    setLocale: function setLocale(locale) {
      var newLocale = default_options.i18n[locale] ? locale : globalLocaleDefault;

      if (globalLocale !== newLocale) {
        globalLocale = newLocale; // reinit date formatter

        initDateFormatter();
      }
    },
    setDateFormatter: function setDateFormatter(dateFormatter) {
      if (typeof dateFormatter === 'string' && dateFormatters.hasOwnProperty(dateFormatter)) {
        var df = dateFormatters[dateFormatter];
        djQ.extend(default_options, df.default_options);
        dateHelper = df.formatter;
      } else {
        dateHelper = dateFormatter;
      }
    }
  };
  var standardFormats = {
    RFC_2822: 'D, d M Y H:i:s O',
    ATOM: 'Y-m-d\TH:i:sP',
    ISO_8601: 'Y-m-d\TH:i:sO',
    RFC_822: 'D, d M y H:i:s O',
    RFC_850: 'l, d-M-y H:i:s T',
    RFC_1036: 'D, d M y H:i:s O',
    RFC_1123: 'D, d M Y H:i:s O',
    RSS: 'D, d M Y H:i:s O',
    W3C: 'Y-m-d\TH:i:sP'
  };

  var isFormatStandard = function isFormatStandard(format) {
    return Object.values(standardFormats).indexOf(format) === -1 ? false : true;
  };

  djQ.extend(djQ.datetimepicker, standardFormats); // first init date formatter

  initDateFormatter(); // fix for ie8

  if (!window.getComputedStyle) {
    window.getComputedStyle = function (el) {
      this.el = el;

      this.getPropertyValue = function (prop) {
        var re = /(-([a-z]))/g;

        if (prop === 'float') {
          prop = 'styleFloat';
        }

        if (re.test(prop)) {
          prop = prop.replace(re, function (a, b, c) {
            return c.toUpperCase();
          });
        }

        return el.currentStyle[prop] || null;
      };

      return this;
    };
  }

  if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
      var i, j;

      for (i = start || 0, j = this.length; i < j; i += 1) {
        if (this[i] === obj) {
          return i;
        }
      }

      return -1;
    };
  }

  Date.prototype.countDaysInMonth = function () {
    return new Date(this.getFullYear(), this.getMonth() + 1, 0).getDate();
  };

  djQ.fn.xdsoftScroller = function (options, percent) {
    return this.each(function () {
      var timeboxparent = djQ(this),
          pointerEventToXY = function pointerEventToXY(e) {
        var out = {
          x: 0,
          y: 0
        },
            touch;

        if (e.type === 'touchstart' || e.type === 'touchmove' || e.type === 'touchend' || e.type === 'touchcancel') {
          touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
          out.x = touch.clientX;
          out.y = touch.clientY;
        } else if (e.type === 'mousedown' || e.type === 'mouseup' || e.type === 'mousemove' || e.type === 'mouseover' || e.type === 'mouseout' || e.type === 'mouseenter' || e.type === 'mouseleave') {
          out.x = e.clientX;
          out.y = e.clientY;
        }

        return out;
      },
          getWheelDelta = function getWheelDelta(e) {
        var deltaY = 0;

        if ('detail' in e) {
          deltaY = e.detail;
        }

        if ('wheelDelta' in e) {
          deltaY = -e.wheelDelta / 120;
        }

        if ('wheelDeltaY' in e) {
          deltaY = -e.wheelDeltaY / 120;
        }

        if ('axis' in e && e.axis === e.HORIZONTAL_AXIS) {
          deltaY = 0;
        }

        deltaY *= 10;

        if ('deltaY' in e) {
          deltaY = e.deltaY;
        }

        if (deltaY && e.deltaMode) {
          if (e.deltaMode === 1) {
            deltaY *= 40;
          } else {
            deltaY *= 800;
          }
        }

        return deltaY;
      },
          timebox,
          timeboxTop = 0,
          parentHeight,
          height,
          scrollbar,
          scroller,
          maximumOffset = 100,
          start = false,
          startY = 0,
          startTop = 0,
          h1 = 0,
          touchStart = false,
          startTopScroll = 0,
          calcOffset = function calcOffset() {};

      if (percent === 'hide') {
        timeboxparent.find('.xdsoft_scrollbar').hide();
        return;
      }

      if (!djQ(this).hasClass('xdsoft_scroller_box')) {
        timebox = timeboxparent.children().eq(0);
        timeboxTop = Math.abs(parseInt(timebox.css('marginTop'), 10));
        parentHeight = timeboxparent[0].clientHeight;
        height = timebox[0].offsetHeight;
        scrollbar = djQ('<div class="xdsoft_scrollbar"></div>');
        scroller = djQ('<div class="xdsoft_scroller"></div>');
        scrollbar.append(scroller);
        timeboxparent.addClass('xdsoft_scroller_box').append(scrollbar);

        calcOffset = function calcOffset(event) {
          var offset = pointerEventToXY(event).y - startY + startTopScroll;

          if (offset < 0) {
            offset = 0;
          }

          if (offset + scroller[0].offsetHeight > h1) {
            offset = h1 - scroller[0].offsetHeight;
          }

          timeboxparent.trigger('scroll_element.xdsoft_scroller', [maximumOffset ? offset / maximumOffset : 0]);
        };

        scroller.on('touchstart.xdsoft_scroller mousedown.xdsoft_scroller', function (event) {
          if (!parentHeight) {
            timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
          }

          startY = pointerEventToXY(event).y;
          startTopScroll = parseInt(scroller.css('marginTop'), 10);
          h1 = scrollbar[0].offsetHeight;

          if (event.type === 'mousedown' || event.type === 'touchstart') {
            if (options.ownerDocument) {
              djQ(options.ownerDocument.body).addClass('xdsoft_noselect');
            }

            djQ([options.ownerDocument.body, options.contentWindow]).on('touchend mouseup.xdsoft_scroller', function arguments_callee() {
              djQ([options.ownerDocument.body, options.contentWindow]).off('touchend mouseup.xdsoft_scroller', arguments_callee).off('mousemove.xdsoft_scroller', calcOffset).removeClass('xdsoft_noselect');
            });
            djQ(options.ownerDocument.body).on('mousemove.xdsoft_scroller', calcOffset);
          } else {
            touchStart = true;
            event.stopPropagation();
            event.preventDefault();
          }
        }).on('touchmove', function (event) {
          if (touchStart) {
            event.preventDefault();
            calcOffset(event);
          }
        }).on('touchend touchcancel', function () {
          touchStart = false;
          startTopScroll = 0;
        });
        timeboxparent.on('scroll_element.xdsoft_scroller', function (event, percentage) {
          if (!parentHeight) {
            timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percentage, true]);
          }

          percentage = percentage > 1 ? 1 : percentage < 0 || isNaN(percentage) ? 0 : percentage;
          timeboxTop = parseFloat(Math.abs((timebox[0].offsetHeight - parentHeight) * percentage).toFixed(4));
          scroller.css('marginTop', maximumOffset * percentage);
          timebox.css('marginTop', -timeboxTop);
        }).on('resize_scroll.xdsoft_scroller', function (event, percentage, noTriggerScroll) {
          var percent, sh;
          parentHeight = timeboxparent[0].clientHeight;
          height = timebox[0].offsetHeight;
          percent = parentHeight / height;
          sh = percent * scrollbar[0].offsetHeight;

          if (percent > 1) {
            scroller.hide();
          } else {
            scroller.show();
            scroller.css('height', parseInt(sh > 10 ? sh : 10, 10));
            maximumOffset = scrollbar[0].offsetHeight - scroller[0].offsetHeight;

            if (noTriggerScroll !== true) {
              timeboxparent.trigger('scroll_element.xdsoft_scroller', [percentage || timeboxTop / (height - parentHeight)]);
            }
          }
        });
        timeboxparent.on('mousewheel', function (event) {
          var deltaY = getWheelDelta(event.originalEvent);
          var top = Math.max(0, timeboxTop - deltaY);
          timeboxparent.trigger('scroll_element.xdsoft_scroller', [top / (height - parentHeight)]);
          event.stopPropagation();
          return false;
        });
        timeboxparent.on('touchstart', function (event) {
          start = pointerEventToXY(event);
          startTop = timeboxTop;
        });
        timeboxparent.on('touchmove', function (event) {
          if (start) {
            event.preventDefault();
            var coord = pointerEventToXY(event);
            timeboxparent.trigger('scroll_element.xdsoft_scroller', [(startTop - (coord.y - start.y)) / (height - parentHeight)]);
          }
        });
        timeboxparent.on('touchend touchcancel', function () {
          start = false;
          startTop = 0;
        });
      }

      timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
    });
  };

  djQ.fn.datetimepicker = function (opt, opt2) {
    var result = this,
        KEY0 = 48,
        KEY9 = 57,
        _KEY0 = 96,
        _KEY9 = 105,
        CTRLKEY = 17,
        CMDKEY = 91,
        DEL = 46,
        ENTER = 13,
        ESC = 27,
        BACKSPACE = 8,
        ARROWLEFT = 37,
        ARROWUP = 38,
        ARROWRIGHT = 39,
        ARROWDOWN = 40,
        TAB = 9,
        F5 = 116,
        AKEY = 65,
        CKEY = 67,
        VKEY = 86,
        ZKEY = 90,
        YKEY = 89,
        ctrlDown = false,
        cmdDown = false,
        options = djQ.isPlainObject(opt) || !opt ? djQ.extend(true, {}, default_options, opt) : djQ.extend(true, {}, default_options),
        lazyInitTimer = 0,
        createDateTimePicker,
        destroyDateTimePicker,
        lazyInit = function lazyInit(input) {
      input.on('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', function initOnActionCallback() {
        if (input.is(':disabled') || input.data('xdsoft_datetimepicker')) {
          return;
        }

        clearTimeout(lazyInitTimer);
        lazyInitTimer = setTimeout(function () {
          if (!input.data('xdsoft_datetimepicker')) {
            createDateTimePicker(input);
          }

          input.off('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', initOnActionCallback).trigger('open.xdsoft');
        }, 100);
      });
    };

    createDateTimePicker = function createDateTimePicker(input) {
      var datetimepicker = djQ('<div class="xdsoft_datetimepicker xdsoft_noselect"></div>'),
          xdsoft_copyright = djQ('<div class="xdsoft_copyright"><a target="_blank" href="http://xdsoft.net/jqplugins/datetimepicker/">xdsoft.net</a></div>'),
          datepicker = djQ('<div class="xdsoft_datepicker active"></div>'),
          month_picker = djQ('<div class="xdsoft_monthpicker"><button type="button" class="xdsoft_prev"></button><button type="button" class="xdsoft_today_button"></button>' + '<div class="xdsoft_label xdsoft_month"><span></span><i></i></div>' + '<div class="xdsoft_label xdsoft_year"><span></span><i></i></div>' + '<button type="button" class="xdsoft_next"></button></div>'),
          calendar = djQ('<div class="xdsoft_calendar"></div>'),
          timepicker = djQ('<div class="xdsoft_timepicker active"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_time_box"></div><button type="button" class="xdsoft_next"></button></div>'),
          timeboxparent = timepicker.find('.xdsoft_time_box').eq(0),
          timebox = djQ('<div class="xdsoft_time_variant"></div>'),
          applyButton = djQ('<button type="button" class="xdsoft_save_selected blue-gradient-button">Save Selected</button>'),
          monthselect = djQ('<div class="xdsoft_select xdsoft_monthselect"><div></div></div>'),
          yearselect = djQ('<div class="xdsoft_select xdsoft_yearselect"><div></div></div>'),
          triggerAfterOpen = false,
          XDSoft_datetime,
          xchangeTimer,
          timerclick,
          current_time_index,
          setPos,
          timer = 0,
          _xdsoft_datetime,
          forEachAncestorOf;

      if (options.id) {
        datetimepicker.attr('id', options.id);
      }

      if (options.style) {
        datetimepicker.attr('style', options.style);
      }

      if (options.weeks) {
        datetimepicker.addClass('xdsoft_showweeks');
      }

      if (options.rtl) {
        datetimepicker.addClass('xdsoft_rtl');
      }

      datetimepicker.addClass('xdsoft_' + options.theme);
      datetimepicker.addClass(options.className);
      month_picker.find('.xdsoft_month span').after(monthselect);
      month_picker.find('.xdsoft_year span').after(yearselect);
      month_picker.find('.xdsoft_month,.xdsoft_year').on('touchstart mousedown.xdsoft', function (event) {
        var select = djQ(this).find('.xdsoft_select').eq(0),
            val = 0,
            top = 0,
            visible = select.is(':visible'),
            items,
            i;
        month_picker.find('.xdsoft_select').hide();

        if (_xdsoft_datetime.currentTime) {
          val = _xdsoft_datetime.currentTime[djQ(this).hasClass('xdsoft_month') ? 'getMonth' : 'getFullYear']();
        }

        select[visible ? 'hide' : 'show']();

        for (items = select.find('div.xdsoft_option'), i = 0; i < items.length; i += 1) {
          if (items.eq(i).data('value') === val) {
            break;
          } else {
            top += items[0].offsetHeight;
          }
        }

        select.xdsoftScroller(options, top / (select.children()[0].offsetHeight - select[0].clientHeight));
        event.stopPropagation();
        return false;
      });

      var handleTouchMoved = function handleTouchMoved(event) {
        var evt = event.originalEvent;
        var touchPosition = evt.touches ? evt.touches[0] : evt;
        this.touchStartPosition = this.touchStartPosition || touchPosition;
        var xMovement = Math.abs(this.touchStartPosition.clientX - touchPosition.clientX);
        var yMovement = Math.abs(this.touchStartPosition.clientY - touchPosition.clientY);
        var distance = Math.sqrt(xMovement * xMovement + yMovement * yMovement);

        if (distance > options.touchMovedThreshold) {
          this.touchMoved = true;
        }
      };

      month_picker.find('.xdsoft_select').xdsoftScroller(options).on('touchstart mousedown.xdsoft', function (event) {
        var evt = event.originalEvent;
        this.touchMoved = false;
        this.touchStartPosition = evt.touches ? evt.touches[0] : evt;
        event.stopPropagation();
        event.preventDefault();
      }).on('touchmove', '.xdsoft_option', handleTouchMoved).on('touchend mousedown.xdsoft', '.xdsoft_option', function () {
        if (!this.touchMoved) {
          if (_xdsoft_datetime.currentTime === undefined || _xdsoft_datetime.currentTime === null) {
            _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
          }

          var year = _xdsoft_datetime.currentTime.getFullYear();

          if (_xdsoft_datetime && _xdsoft_datetime.currentTime) {
            _xdsoft_datetime.currentTime[djQ(this).parent().parent().hasClass('xdsoft_monthselect') ? 'setMonth' : 'setFullYear'](djQ(this).data('value'));
          }

          djQ(this).parent().parent().hide();
          datetimepicker.trigger('xchange.xdsoft');

          if (options.onChangeMonth && djQ.isFunction(options.onChangeMonth)) {
            options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
          }

          if (year !== _xdsoft_datetime.currentTime.getFullYear() && djQ.isFunction(options.onChangeYear)) {
            options.onChangeYear.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
          }
        }
      });

      datetimepicker.getValue = function () {
        return _xdsoft_datetime.getCurrentTime();
      };

      datetimepicker.setOptions = function (_options) {
        var highlightedDates = {};
        options = djQ.extend(true, {}, options, _options);

        if (_options.allowTimes && djQ.isArray(_options.allowTimes) && _options.allowTimes.length) {
          options.allowTimes = djQ.extend(true, [], _options.allowTimes);
        }

        if (_options.weekends && djQ.isArray(_options.weekends) && _options.weekends.length) {
          options.weekends = djQ.extend(true, [], _options.weekends);
        }

        if (_options.allowDates && djQ.isArray(_options.allowDates) && _options.allowDates.length) {
          options.allowDates = djQ.extend(true, [], _options.allowDates);
        }

        if (_options.allowDateRe && Object.prototype.toString.call(_options.allowDateRe) === "[object String]") {
          options.allowDateRe = new RegExp(_options.allowDateRe);
        }

        if (_options.highlightedDates && djQ.isArray(_options.highlightedDates) && _options.highlightedDates.length) {
          djQ.each(_options.highlightedDates, function (index, value) {
            var splitData = djQ.map(value.split(','), djQ.trim),
                exDesc,
                hDate = new HighlightedDate(dateHelper.parseDate(splitData[0], options.formatDate), splitData[1], splitData[2]),
                // date, desc, style
            keyDate = dateHelper.formatDate(hDate.date, options.formatDate);

            if (highlightedDates[keyDate] !== undefined) {
              exDesc = highlightedDates[keyDate].desc;

              if (exDesc && exDesc.length && hDate.desc && hDate.desc.length) {
                highlightedDates[keyDate].desc = exDesc + "\n" + hDate.desc;
              }
            } else {
              highlightedDates[keyDate] = hDate;
            }
          });
          options.highlightedDates = djQ.extend(true, [], highlightedDates);
        }

        if (_options.highlightedPeriods && djQ.isArray(_options.highlightedPeriods) && _options.highlightedPeriods.length) {
          highlightedDates = djQ.extend(true, [], options.highlightedDates);
          djQ.each(_options.highlightedPeriods, function (index, value) {
            var dateTest, // start date
            dateEnd, desc, hDate, keyDate, exDesc, style;

            if (djQ.isArray(value)) {
              dateTest = value[0];
              dateEnd = value[1];
              desc = value[2];
              style = value[3];
            } else {
              var splitData = djQ.map(value.split(','), djQ.trim);
              dateTest = dateHelper.parseDate(splitData[0], options.formatDate);
              dateEnd = dateHelper.parseDate(splitData[1], options.formatDate);
              desc = splitData[2];
              style = splitData[3];
            }

            while (dateTest <= dateEnd) {
              hDate = new HighlightedDate(dateTest, desc, style);
              keyDate = dateHelper.formatDate(dateTest, options.formatDate);
              dateTest.setDate(dateTest.getDate() + 1);

              if (highlightedDates[keyDate] !== undefined) {
                exDesc = highlightedDates[keyDate].desc;

                if (exDesc && exDesc.length && hDate.desc && hDate.desc.length) {
                  highlightedDates[keyDate].desc = exDesc + "\n" + hDate.desc;
                }
              } else {
                highlightedDates[keyDate] = hDate;
              }
            }
          });
          options.highlightedDates = djQ.extend(true, [], highlightedDates);
        }

        if (_options.disabledDates && djQ.isArray(_options.disabledDates) && _options.disabledDates.length) {
          options.disabledDates = djQ.extend(true, [], _options.disabledDates);
        }

        if (_options.disabledWeekDays && djQ.isArray(_options.disabledWeekDays) && _options.disabledWeekDays.length) {
          options.disabledWeekDays = djQ.extend(true, [], _options.disabledWeekDays);
        }

        if ((options.open || options.opened) && !options.inline) {
          input.trigger('open.xdsoft');
        }

        if (options.inline) {
          triggerAfterOpen = true;
          datetimepicker.addClass('xdsoft_inline');
          input.after(datetimepicker).hide();
        }

        if (options.inverseButton) {
          options.next = 'xdsoft_prev';
          options.prev = 'xdsoft_next';
        }

        if (options.datepicker) {
          datepicker.addClass('active');
        } else {
          datepicker.removeClass('active');
        }

        if (options.timepicker) {
          timepicker.addClass('active');
        } else {
          timepicker.removeClass('active');
        }

        if (options.value) {
          _xdsoft_datetime.setCurrentTime(options.value);

          if (input && input.val) {
            input.val(_xdsoft_datetime.str);
          }
        }

        if (isNaN(options.dayOfWeekStart)) {
          options.dayOfWeekStart = 0;
        } else {
          options.dayOfWeekStart = parseInt(options.dayOfWeekStart, 10) % 7;
        }

        if (!options.timepickerScrollbar) {
          timeboxparent.xdsoftScroller(options, 'hide');
        }

        if (options.minDate && /^[\+\-](.*)$/.test(options.minDate)) {
          options.minDate = dateHelper.formatDate(_xdsoft_datetime.strToDateTime(options.minDate), options.formatDate);
        }

        if (options.maxDate && /^[\+\-](.*)$/.test(options.maxDate)) {
          options.maxDate = dateHelper.formatDate(_xdsoft_datetime.strToDateTime(options.maxDate), options.formatDate);
        }

        if (options.minDateTime && /^\+(.*)$/.test(options.minDateTime)) {
          options.minDateTime = _xdsoft_datetime.strToDateTime(options.minDateTime).dateFormat(options.formatDate);
        }

        if (options.maxDateTime && /^\+(.*)$/.test(options.maxDateTime)) {
          options.maxDateTime = _xdsoft_datetime.strToDateTime(options.maxDateTime).dateFormat(options.formatDate);
        }

        applyButton.toggle(options.showApplyButton);
        month_picker.find('.xdsoft_today_button').css('visibility', !options.todayButton ? 'hidden' : 'visible');
        month_picker.find('.' + options.prev).css('visibility', !options.prevButton ? 'hidden' : 'visible');
        month_picker.find('.' + options.next).css('visibility', !options.nextButton ? 'hidden' : 'visible');
        setMask(options);

        if (options.validateOnBlur) {
          input.off('blur.xdsoft').on('blur.xdsoft', function () {
            if (options.allowBlank && (!djQ.trim(djQ(this).val()).length || typeof options.mask === "string" && djQ.trim(djQ(this).val()) === options.mask.replace(/[0-9]/g, '_'))) {
              djQ(this).val(null);
              datetimepicker.data('xdsoft_datetime').empty();
            } else {
              var d = dateHelper.parseDate(djQ(this).val(), options.format);

              if (d) {
                // parseDate() may skip some invalid parts like date or time, so make it clear for user: show parsed date/time
                djQ(this).val(dateHelper.formatDate(d, options.format));
              } else {
                var splittedHours = +[djQ(this).val()[0], djQ(this).val()[1]].join(''),
                    splittedMinutes = +[djQ(this).val()[2], djQ(this).val()[3]].join(''); // parse the numbers as 0312 => 03:12

                if (!options.datepicker && options.timepicker && splittedHours >= 0 && splittedHours < 24 && splittedMinutes >= 0 && splittedMinutes < 60) {
                  djQ(this).val([splittedHours, splittedMinutes].map(function (item) {
                    return item > 9 ? item : '0' + item;
                  }).join(':'));
                } else {
                  djQ(this).val(dateHelper.formatDate(_xdsoft_datetime.now(), options.format));
                }
              }

              datetimepicker.data('xdsoft_datetime').setCurrentTime(djQ(this).val());
            }

            datetimepicker.trigger('changedatetime.xdsoft');
            datetimepicker.trigger('close.xdsoft');
          });
        }

        options.dayOfWeekStartPrev = options.dayOfWeekStart === 0 ? 6 : options.dayOfWeekStart - 1;
        datetimepicker.trigger('xchange.xdsoft').trigger('afterOpen.xdsoft');
      };

      datetimepicker.data('options', options).on('touchstart mousedown.xdsoft', function (event) {
        event.stopPropagation();
        event.preventDefault();
        yearselect.hide();
        monthselect.hide();
        return false;
      }); //scroll_element = timepicker.find('.xdsoft_time_box');

      timeboxparent.append(timebox);
      timeboxparent.xdsoftScroller(options);
      datetimepicker.on('afterOpen.xdsoft', function () {
        timeboxparent.xdsoftScroller(options);
      });
      datetimepicker.append(datepicker).append(timepicker);

      if (options.withoutCopyright !== true) {
        datetimepicker.append(xdsoft_copyright);
      }

      datepicker.append(month_picker).append(calendar).append(applyButton);

      if (options.insideParent) {
        djQ(input).parent().append(datetimepicker);
      } else {
        djQ(options.parentID).append(datetimepicker);
      }

      XDSoft_datetime = function XDSoft_datetime() {
        var _this = this;

        _this.now = function (norecursion) {
          var d = new Date(),
              date,
              time;

          if (!norecursion && options.defaultDate) {
            date = _this.strToDateTime(options.defaultDate);
            d.setFullYear(date.getFullYear());
            d.setMonth(date.getMonth());
            d.setDate(date.getDate());
          }

          d.setFullYear(d.getFullYear());

          if (!norecursion && options.defaultTime) {
            time = _this.strtotime(options.defaultTime);
            d.setHours(time.getHours());
            d.setMinutes(time.getMinutes());
            d.setSeconds(time.getSeconds());
            d.setMilliseconds(time.getMilliseconds());
          }

          return d;
        };

        _this.isValidDate = function (d) {
          if (Object.prototype.toString.call(d) !== "[object Date]") {
            return false;
          }

          return !isNaN(d.getTime());
        };

        _this.setCurrentTime = function (dTime, requireValidDate) {
          if (typeof dTime === 'string') {
            _this.currentTime = _this.strToDateTime(dTime);
          } else if (_this.isValidDate(dTime)) {
            _this.currentTime = dTime;
          } else if (!dTime && !requireValidDate && options.allowBlank && !options.inline) {
            _this.currentTime = null;
          } else {
            _this.currentTime = _this.now();
          }

          datetimepicker.trigger('xchange.xdsoft');
        };

        _this.empty = function () {
          _this.currentTime = null;
        };

        _this.getCurrentTime = function () {
          return _this.currentTime;
        };

        _this.nextMonth = function () {
          if (_this.currentTime === undefined || _this.currentTime === null) {
            _this.currentTime = _this.now();
          }

          var month = _this.currentTime.getMonth() + 1,
              year;

          if (month === 12) {
            _this.currentTime.setFullYear(_this.currentTime.getFullYear() + 1);

            month = 0;
          }

          year = _this.currentTime.getFullYear();

          _this.currentTime.setDate(Math.min(new Date(_this.currentTime.getFullYear(), month + 1, 0).getDate(), _this.currentTime.getDate()));

          _this.currentTime.setMonth(month);

          if (options.onChangeMonth && djQ.isFunction(options.onChangeMonth)) {
            options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
          }

          if (year !== _this.currentTime.getFullYear() && djQ.isFunction(options.onChangeYear)) {
            options.onChangeYear.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
          }

          datetimepicker.trigger('xchange.xdsoft');
          return month;
        };

        _this.prevMonth = function () {
          if (_this.currentTime === undefined || _this.currentTime === null) {
            _this.currentTime = _this.now();
          }

          var month = _this.currentTime.getMonth() - 1;

          if (month === -1) {
            _this.currentTime.setFullYear(_this.currentTime.getFullYear() - 1);

            month = 11;
          }

          _this.currentTime.setDate(Math.min(new Date(_this.currentTime.getFullYear(), month + 1, 0).getDate(), _this.currentTime.getDate()));

          _this.currentTime.setMonth(month);

          if (options.onChangeMonth && djQ.isFunction(options.onChangeMonth)) {
            options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
          }

          datetimepicker.trigger('xchange.xdsoft');
          return month;
        };

        _this.getWeekOfYear = function (datetime) {
          if (options.onGetWeekOfYear && djQ.isFunction(options.onGetWeekOfYear)) {
            var week = options.onGetWeekOfYear.call(datetimepicker, datetime);

            if (typeof week !== 'undefined') {
              return week;
            }
          }

          var onejan = new Date(datetime.getFullYear(), 0, 1); //First week of the year is th one with the first Thursday according to ISO8601

          if (onejan.getDay() !== 4) {
            onejan.setMonth(0, 1 + (4 - onejan.getDay() + 7) % 7);
          }

          return Math.ceil(((datetime - onejan) / 86400000 + onejan.getDay() + 1) / 7);
        };

        _this.strToDateTime = function (sDateTime) {
          var tmpDate = [],
              timeOffset,
              currentTime;

          if (sDateTime && sDateTime instanceof Date && _this.isValidDate(sDateTime)) {
            return sDateTime;
          }

          tmpDate = /^([+-]{1})(.*)$/.exec(sDateTime);

          if (tmpDate) {
            tmpDate[2] = dateHelper.parseDate(tmpDate[2], options.formatDate);
          }

          if (tmpDate && tmpDate[2]) {
            timeOffset = tmpDate[2].getTime() - tmpDate[2].getTimezoneOffset() * 60000;
            currentTime = new Date(_this.now(true).getTime() + parseInt(tmpDate[1] + '1', 10) * timeOffset);
          } else {
            currentTime = sDateTime ? dateHelper.parseDate(sDateTime, options.format) : _this.now();
          }

          if (!_this.isValidDate(currentTime)) {
            currentTime = _this.now(true);
          }

          return currentTime;
        };

        _this.strToDate = function (sDate) {
          if (sDate && sDate instanceof Date && _this.isValidDate(sDate)) {
            return sDate;
          }

          var currentTime = sDate ? dateHelper.parseDate(sDate, options.formatDate) : _this.now(true);

          if (!_this.isValidDate(currentTime)) {
            currentTime = _this.now(true);
          }

          return currentTime;
        };

        _this.strtotime = function (sTime) {
          if (sTime && sTime instanceof Date && _this.isValidDate(sTime)) {
            return sTime;
          }

          var currentTime = sTime ? dateHelper.parseDate(sTime, options.formatTime) : _this.now(true);

          if (!_this.isValidDate(currentTime)) {
            currentTime = _this.now(true);
          }

          return currentTime;
        };

        _this.str = function () {
          var format = options.format;

          if (options.yearOffset) {
            format = format.replace('Y', _this.currentTime.getFullYear() + options.yearOffset);
            format = format.replace('y', String(_this.currentTime.getFullYear() + options.yearOffset).substring(2, 4));
          }

          return dateHelper.formatDate(_this.currentTime, format);
        };

        _this.currentTime = this.now();
      };

      _xdsoft_datetime = new XDSoft_datetime();
      applyButton.on('touchend click', function (e) {
        //pathbrite
        e.preventDefault();
        datetimepicker.data('changed', true);

        _xdsoft_datetime.setCurrentTime(getCurrentValue());

        input.val(_xdsoft_datetime.str());
        datetimepicker.trigger('close.xdsoft');
      });
      month_picker.find('.xdsoft_today_button').on('touchend mousedown.xdsoft', function () {
        datetimepicker.data('changed', true);

        _xdsoft_datetime.setCurrentTime(0, true);

        datetimepicker.trigger('afterOpen.xdsoft');
      }).on('dblclick.xdsoft', function () {
        var currentDate = _xdsoft_datetime.getCurrentTime(),
            minDate,
            maxDate;

        currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
        minDate = _xdsoft_datetime.strToDate(options.minDate);
        minDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate());

        if (currentDate < minDate) {
          return;
        }

        maxDate = _xdsoft_datetime.strToDate(options.maxDate);
        maxDate = new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate());

        if (currentDate > maxDate) {
          return;
        }

        input.val(_xdsoft_datetime.str());
        input.trigger('change');
        datetimepicker.trigger('close.xdsoft');
      });
      month_picker.find('.xdsoft_prev,.xdsoft_next').on('touchend mousedown.xdsoft', function () {
        var $this = djQ(this),
            timer = 0,
            stop = false;

        (function arguments_callee1(v) {
          if ($this.hasClass(options.next)) {
            _xdsoft_datetime.nextMonth();
          } else if ($this.hasClass(options.prev)) {
            _xdsoft_datetime.prevMonth();
          }

          if (options.monthChangeSpinner) {
            if (!stop) {
              timer = setTimeout(arguments_callee1, v || 100);
            }
          }
        })(500);

        djQ([options.ownerDocument.body, options.contentWindow]).on('touchend mouseup.xdsoft', function arguments_callee2() {
          clearTimeout(timer);
          stop = true;
          djQ([options.ownerDocument.body, options.contentWindow]).off('touchend mouseup.xdsoft', arguments_callee2);
        });
      });
      timepicker.find('.xdsoft_prev,.xdsoft_next').on('touchend mousedown.xdsoft', function () {
        var $this = djQ(this),
            timer = 0,
            stop = false,
            period = 110;

        (function arguments_callee4(v) {
          var pheight = timeboxparent[0].clientHeight,
              height = timebox[0].offsetHeight,
              top = Math.abs(parseInt(timebox.css('marginTop'), 10));

          if ($this.hasClass(options.next) && height - pheight - options.timeHeightInTimePicker >= top) {
            timebox.css('marginTop', '-' + (top + options.timeHeightInTimePicker) + 'px');
          } else if ($this.hasClass(options.prev) && top - options.timeHeightInTimePicker >= 0) {
            timebox.css('marginTop', '-' + (top - options.timeHeightInTimePicker) + 'px');
          }
          /**
           * Fixed bug:
           * When using css3 transition, it will cause a bug that you cannot scroll the timepicker list.
           * The reason is that the transition-duration time, if you set it to 0, all things fine, otherwise, this
           * would cause a bug when you use jquery.css method.
           * Let's say: * { transition: all .5s ease; }
           * jquery timebox.css('marginTop') will return the original value which is before you clicking the next/prev button,
           * meanwhile the timebox[0].style.marginTop will return the right value which is after you clicking the
           * next/prev button.
           *
           * What we should do:
           * Replace timebox.css('marginTop') with timebox[0].style.marginTop.
           */


          timeboxparent.trigger('scroll_element.xdsoft_scroller', [Math.abs(parseInt(timebox[0].style.marginTop, 10) / (height - pheight))]);
          period = period > 10 ? 10 : period - 10;

          if (!stop) {
            timer = setTimeout(arguments_callee4, v || period);
          }
        })(500);

        djQ([options.ownerDocument.body, options.contentWindow]).on('touchend mouseup.xdsoft', function arguments_callee5() {
          clearTimeout(timer);
          stop = true;
          djQ([options.ownerDocument.body, options.contentWindow]).off('touchend mouseup.xdsoft', arguments_callee5);
        });
      });
      xchangeTimer = 0; // base handler - generating a calendar and timepicker

      datetimepicker.on('xchange.xdsoft', function (event) {
        clearTimeout(xchangeTimer);
        xchangeTimer = setTimeout(function () {
          if (_xdsoft_datetime.currentTime === undefined || _xdsoft_datetime.currentTime === null) {
            _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
          }

          var table = '',
              start = new Date(_xdsoft_datetime.currentTime.getFullYear(), _xdsoft_datetime.currentTime.getMonth(), 1, 12, 0, 0),
              i = 0,
              j,
              today = _xdsoft_datetime.now(),
              maxDate = false,
              minDate = false,
              minDateTime = false,
              maxDateTime = false,
              hDate,
              day,
              d,
              y,
              m,
              w,
              classes = [],
              customDateSettings,
              newRow = true,
              time = '',
              h,
              line_time,
              description;

          while (start.getDay() !== options.dayOfWeekStart) {
            start.setDate(start.getDate() - 1);
          }

          table += '<table><thead><tr>';

          if (options.weeks) {
            table += '<th></th>';
          }

          for (j = 0; j < 7; j += 1) {
            table += '<th>' + options.i18n[globalLocale].dayOfWeekShort[(j + options.dayOfWeekStart) % 7] + '</th>';
          }

          table += '</tr></thead>';
          table += '<tbody>';

          if (options.maxDate !== false) {
            maxDate = _xdsoft_datetime.strToDate(options.maxDate);
            maxDate = new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate(), 23, 59, 59, 999);
          }

          if (options.minDate !== false) {
            minDate = _xdsoft_datetime.strToDate(options.minDate);
            minDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate());
          }

          if (options.minDateTime !== false) {
            minDateTime = _xdsoft_datetime.strToDate(options.minDateTime);
            minDateTime = new Date(minDateTime.getFullYear(), minDateTime.getMonth(), minDateTime.getDate(), minDateTime.getHours(), minDateTime.getMinutes(), minDateTime.getSeconds());
          }

          if (options.maxDateTime !== false) {
            maxDateTime = _xdsoft_datetime.strToDate(options.maxDateTime);
            maxDateTime = new Date(maxDateTime.getFullYear(), maxDateTime.getMonth(), maxDateTime.getDate(), maxDateTime.getHours(), maxDateTime.getMinutes(), maxDateTime.getSeconds());
          }

          var maxDateTimeDay;

          if (maxDateTime !== false) {
            maxDateTimeDay = (maxDateTime.getFullYear() * 12 + maxDateTime.getMonth()) * 31 + maxDateTime.getDate();
          }

          while (i < _xdsoft_datetime.currentTime.countDaysInMonth() || start.getDay() !== options.dayOfWeekStart || _xdsoft_datetime.currentTime.getMonth() === start.getMonth()) {
            classes = [];
            i += 1;
            day = start.getDay();
            d = start.getDate();
            y = start.getFullYear();
            m = start.getMonth();
            w = _xdsoft_datetime.getWeekOfYear(start);
            description = '';
            classes.push('xdsoft_date');

            if (options.beforeShowDay && djQ.isFunction(options.beforeShowDay.call)) {
              customDateSettings = options.beforeShowDay.call(datetimepicker, start);
            } else {
              customDateSettings = null;
            }

            if (options.allowDateRe && Object.prototype.toString.call(options.allowDateRe) === "[object RegExp]") {
              if (!options.allowDateRe.test(dateHelper.formatDate(start, options.formatDate))) {
                classes.push('xdsoft_disabled');
              }
            }

            if (options.allowDates && options.allowDates.length > 0) {
              if (options.allowDates.indexOf(dateHelper.formatDate(start, options.formatDate)) === -1) {
                classes.push('xdsoft_disabled');
              }
            }

            var currentDay = (start.getFullYear() * 12 + start.getMonth()) * 31 + start.getDate();

            if (maxDate !== false && start > maxDate || minDateTime !== false && start < minDateTime || minDate !== false && start < minDate || maxDateTime !== false && currentDay > maxDateTimeDay || customDateSettings && customDateSettings[0] === false) {
              classes.push('xdsoft_disabled');
            }

            if (options.disabledDates.indexOf(dateHelper.formatDate(start, options.formatDate)) !== -1) {
              classes.push('xdsoft_disabled');
            }

            if (options.disabledWeekDays.indexOf(day) !== -1) {
              classes.push('xdsoft_disabled');
            }

            if (input.is('[disabled]')) {
              classes.push('xdsoft_disabled');
            }

            if (customDateSettings && customDateSettings[1] !== "") {
              classes.push(customDateSettings[1]);
            }

            if (_xdsoft_datetime.currentTime.getMonth() !== m) {
              classes.push('xdsoft_other_month');
            }

            if ((options.defaultSelect || datetimepicker.data('changed')) && dateHelper.formatDate(_xdsoft_datetime.currentTime, options.formatDate) === dateHelper.formatDate(start, options.formatDate)) {
              classes.push('xdsoft_current');
            }

            if (dateHelper.formatDate(today, options.formatDate) === dateHelper.formatDate(start, options.formatDate)) {
              classes.push('xdsoft_today');
            }

            if (start.getDay() === 0 || start.getDay() === 6 || options.weekends.indexOf(dateHelper.formatDate(start, options.formatDate)) !== -1) {
              classes.push('xdsoft_weekend');
            }

            if (options.highlightedDates[dateHelper.formatDate(start, options.formatDate)] !== undefined) {
              hDate = options.highlightedDates[dateHelper.formatDate(start, options.formatDate)];
              classes.push(hDate.style === undefined ? 'xdsoft_highlighted_default' : hDate.style);
              description = hDate.desc === undefined ? '' : hDate.desc;
            }

            if (options.beforeShowDay && djQ.isFunction(options.beforeShowDay)) {
              classes.push(options.beforeShowDay(start));
            }

            if (newRow) {
              table += '<tr>';
              newRow = false;

              if (options.weeks) {
                table += '<th>' + w + '</th>';
              }
            }

            table += '<td data-date="' + d + '" data-month="' + m + '" data-year="' + y + '"' + ' class="xdsoft_date xdsoft_day_of_week' + start.getDay() + ' ' + classes.join(' ') + '" title="' + description + '">' + '<div>' + d + '</div>' + '</td>';

            if (start.getDay() === options.dayOfWeekStartPrev) {
              table += '</tr>';
              newRow = true;
            }

            start.setDate(d + 1);
          }

          table += '</tbody></table>';
          calendar.html(table);
          month_picker.find('.xdsoft_label span').eq(0).text(options.i18n[globalLocale].months[_xdsoft_datetime.currentTime.getMonth()]);
          month_picker.find('.xdsoft_label span').eq(1).text(_xdsoft_datetime.currentTime.getFullYear() + options.yearOffset); // generate timebox

          time = '';
          h = '';
          m = '';
          var minTimeMinutesOfDay = 0;

          if (options.minTime !== false) {
            var t = _xdsoft_datetime.strtotime(options.minTime);

            minTimeMinutesOfDay = 60 * t.getHours() + t.getMinutes();
          }

          var maxTimeMinutesOfDay = 24 * 60;

          if (options.maxTime !== false) {
            var t = _xdsoft_datetime.strtotime(options.maxTime);

            maxTimeMinutesOfDay = 60 * t.getHours() + t.getMinutes();
          }

          if (options.minDateTime !== false) {
            var t = _xdsoft_datetime.strToDateTime(options.minDateTime);

            var currentDayIsMinDateTimeDay = dateHelper.formatDate(_xdsoft_datetime.currentTime, options.formatDate) === dateHelper.formatDate(t, options.formatDate);

            if (currentDayIsMinDateTimeDay) {
              var m = 60 * t.getHours() + t.getMinutes();
              if (m > minTimeMinutesOfDay) minTimeMinutesOfDay = m;
            }
          }

          if (options.maxDateTime !== false) {
            var t = _xdsoft_datetime.strToDateTime(options.maxDateTime);

            var currentDayIsMaxDateTimeDay = dateHelper.formatDate(_xdsoft_datetime.currentTime, options.formatDate) === dateHelper.formatDate(t, options.formatDate);

            if (currentDayIsMaxDateTimeDay) {
              var m = 60 * t.getHours() + t.getMinutes();
              if (m < maxTimeMinutesOfDay) maxTimeMinutesOfDay = m;
            }
          }

          line_time = function line_time(h, m) {
            var now = _xdsoft_datetime.now(),
                current_time,
                isALlowTimesInit = options.allowTimes && djQ.isArray(options.allowTimes) && options.allowTimes.length;

            now.setHours(h);
            h = parseInt(now.getHours(), 10);
            now.setMinutes(m);
            m = parseInt(now.getMinutes(), 10);
            classes = [];
            var currentMinutesOfDay = 60 * h + m;

            if (input.is('[disabled]') || currentMinutesOfDay >= maxTimeMinutesOfDay || currentMinutesOfDay < minTimeMinutesOfDay) {
              classes.push('xdsoft_disabled');
            }

            current_time = new Date(_xdsoft_datetime.currentTime);
            current_time.setHours(parseInt(_xdsoft_datetime.currentTime.getHours(), 10));

            if (!isALlowTimesInit) {
              current_time.setMinutes(Math[options.roundTime](_xdsoft_datetime.currentTime.getMinutes() / options.step) * options.step);
            }

            if ((options.initTime || options.defaultSelect || datetimepicker.data('changed')) && current_time.getHours() === parseInt(h, 10) && (!isALlowTimesInit && options.step > 59 || current_time.getMinutes() === parseInt(m, 10))) {
              if (options.defaultSelect || datetimepicker.data('changed')) {
                classes.push('xdsoft_current');
              } else if (options.initTime) {
                classes.push('xdsoft_init_time');
              }
            }

            if (parseInt(today.getHours(), 10) === parseInt(h, 10) && parseInt(today.getMinutes(), 10) === parseInt(m, 10)) {
              classes.push('xdsoft_today');
            }

            time += '<div class="xdsoft_time ' + classes.join(' ') + '" data-hour="' + h + '" data-minute="' + m + '">' + dateHelper.formatDate(now, options.formatTime) + '</div>';
          };

          if (!options.allowTimes || !djQ.isArray(options.allowTimes) || !options.allowTimes.length) {
            for (i = 0, j = 0; i < (options.hours12 ? 12 : 24); i += 1) {
              for (j = 0; j < 60; j += options.step) {
                var currentMinutesOfDay = i * 60 + j;
                if (currentMinutesOfDay < minTimeMinutesOfDay) continue;
                if (currentMinutesOfDay >= maxTimeMinutesOfDay) continue;
                h = (i < 10 ? '0' : '') + i;
                m = (j < 10 ? '0' : '') + j;
                line_time(h, m);
              }
            }
          } else {
            for (i = 0; i < options.allowTimes.length; i += 1) {
              h = _xdsoft_datetime.strtotime(options.allowTimes[i]).getHours();
              m = _xdsoft_datetime.strtotime(options.allowTimes[i]).getMinutes();
              line_time(h, m);
            }
          }

          timebox.html(time);
          opt = '';

          for (i = parseInt(options.yearStart, 10); i <= parseInt(options.yearEnd, 10); i += 1) {
            opt += '<div class="xdsoft_option ' + (_xdsoft_datetime.currentTime.getFullYear() === i ? 'xdsoft_current' : '') + '" data-value="' + i + '">' + (i + options.yearOffset) + '</div>';
          }

          yearselect.children().eq(0).html(opt);

          for (i = parseInt(options.monthStart, 10), opt = ''; i <= parseInt(options.monthEnd, 10); i += 1) {
            opt += '<div class="xdsoft_option ' + (_xdsoft_datetime.currentTime.getMonth() === i ? 'xdsoft_current' : '') + '" data-value="' + i + '">' + options.i18n[globalLocale].months[i] + '</div>';
          }

          monthselect.children().eq(0).html(opt);
          djQ(datetimepicker).trigger('generate.xdsoft');
        }, 10);
        event.stopPropagation();
      }).on('afterOpen.xdsoft', function () {
        if (options.timepicker) {
          var classType, pheight, height, top;

          if (timebox.find('.xdsoft_current').length) {
            classType = '.xdsoft_current';
          } else if (timebox.find('.xdsoft_init_time').length) {
            classType = '.xdsoft_init_time';
          }

          if (classType) {
            pheight = timeboxparent[0].clientHeight;
            height = timebox[0].offsetHeight;
            top = timebox.find(classType).index() * options.timeHeightInTimePicker + 1;

            if (height - pheight < top) {
              top = height - pheight;
            }

            timeboxparent.trigger('scroll_element.xdsoft_scroller', [parseInt(top, 10) / (height - pheight)]);
          } else {
            timeboxparent.trigger('scroll_element.xdsoft_scroller', [0]);
          }
        }
      });
      timerclick = 0;
      calendar.on('touchend click.xdsoft', 'td', function (xdevent) {
        xdevent.stopPropagation(); // Prevents closing of Pop-ups, Modals and Flyouts in Bootstrap

        timerclick += 1;
        var $this = djQ(this),
            currentTime = _xdsoft_datetime.currentTime;

        if (currentTime === undefined || currentTime === null) {
          _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
          currentTime = _xdsoft_datetime.currentTime;
        }

        if ($this.hasClass('xdsoft_disabled')) {
          return false;
        }

        currentTime.setDate(1);
        currentTime.setFullYear($this.data('year'));
        currentTime.setMonth($this.data('month'));
        currentTime.setDate($this.data('date'));
        datetimepicker.trigger('select.xdsoft', [currentTime]);
        input.val(_xdsoft_datetime.str());

        if (options.onSelectDate && djQ.isFunction(options.onSelectDate)) {
          options.onSelectDate.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), xdevent);
        }

        datetimepicker.data('changed', true);
        datetimepicker.trigger('xchange.xdsoft');
        datetimepicker.trigger('changedatetime.xdsoft');

        if ((timerclick > 1 || options.closeOnDateSelect === true || options.closeOnDateSelect === false && !options.timepicker) && !options.inline) {
          datetimepicker.trigger('close.xdsoft');
        }

        setTimeout(function () {
          timerclick = 0;
        }, 200);
      });
      timebox.on('touchstart', 'div', function (xdevent) {
        this.touchMoved = false;
      }).on('touchmove', 'div', handleTouchMoved).on('touchend click.xdsoft', 'div', function (xdevent) {
        if (!this.touchMoved) {
          xdevent.stopPropagation();
          var $this = djQ(this),
              currentTime = _xdsoft_datetime.currentTime;

          if (currentTime === undefined || currentTime === null) {
            _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
            currentTime = _xdsoft_datetime.currentTime;
          }

          if ($this.hasClass('xdsoft_disabled')) {
            return false;
          }

          currentTime.setHours($this.data('hour'));
          currentTime.setMinutes($this.data('minute'));
          datetimepicker.trigger('select.xdsoft', [currentTime]);
          datetimepicker.data('input').val(_xdsoft_datetime.str());

          if (options.onSelectTime && djQ.isFunction(options.onSelectTime)) {
            options.onSelectTime.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), xdevent);
          }

          datetimepicker.data('changed', true);
          datetimepicker.trigger('xchange.xdsoft');
          datetimepicker.trigger('changedatetime.xdsoft');

          if (options.inline !== true && options.closeOnTimeSelect === true) {
            datetimepicker.trigger('close.xdsoft');
          }
        }
      });
      datepicker.on('mousewheel.xdsoft', function (event) {
        if (!options.scrollMonth) {
          return true;
        }

        if (event.deltaY < 0) {
          _xdsoft_datetime.nextMonth();
        } else {
          _xdsoft_datetime.prevMonth();
        }

        return false;
      });
      input.on('mousewheel.xdsoft', function (event) {
        if (!options.scrollInput) {
          return true;
        }

        if (!options.datepicker && options.timepicker) {
          current_time_index = timebox.find('.xdsoft_current').length ? timebox.find('.xdsoft_current').eq(0).index() : 0;

          if (current_time_index + event.deltaY >= 0 && current_time_index + event.deltaY < timebox.children().length) {
            current_time_index += event.deltaY;
          }

          if (timebox.children().eq(current_time_index).length) {
            timebox.children().eq(current_time_index).trigger('mousedown');
          }

          return false;
        }

        if (options.datepicker && !options.timepicker) {
          datepicker.trigger(event, [event.deltaY, event.deltaX, event.deltaY]);

          if (input.val) {
            input.val(_xdsoft_datetime.str());
          }

          datetimepicker.trigger('changedatetime.xdsoft');
          return false;
        }
      });
      datetimepicker.on('changedatetime.xdsoft', function (event) {
        if (options.onChangeDateTime && djQ.isFunction(options.onChangeDateTime)) {
          var $input = datetimepicker.data('input');
          options.onChangeDateTime.call(datetimepicker, _xdsoft_datetime.currentTime, $input, event);
          delete options.value;
          $input.trigger('change');
        }
      }).on('generate.xdsoft', function () {
        if (options.onGenerate && djQ.isFunction(options.onGenerate)) {
          options.onGenerate.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
        }

        if (triggerAfterOpen) {
          datetimepicker.trigger('afterOpen.xdsoft');
          triggerAfterOpen = false;
        }
      }).on('click.xdsoft', function (xdevent) {
        xdevent.stopPropagation();
      });
      current_time_index = 0;
      /**
       * Runs the callback for each of the specified node's ancestors.
       *
       * Return FALSE from the callback to stop ascending.
       *
       * @param {DOMNode} node
       * @param {Function} callback
       * @returns {undefined}
       */

      forEachAncestorOf = function forEachAncestorOf(node, callback) {
        do {
          node = node.parentNode;

          if (!node || callback(node) === false) {
            break;
          }
        } while (node.nodeName !== 'HTML');
      };
      /**
       * Sets the position of the picker.
       *
       * @returns {undefined}
       */


      setPos = function setPos() {
        var dateInputOffset, dateInputElem, verticalPosition, left, position, datetimepickerElem, dateInputHasFixedAncestor, $dateInput, windowWidth, verticalAnchorEdge, datetimepickerCss, windowHeight, windowScrollTop;
        $dateInput = datetimepicker.data('input');
        dateInputOffset = $dateInput.offset();
        dateInputElem = $dateInput[0];
        verticalAnchorEdge = 'top';
        verticalPosition = dateInputOffset.top + dateInputElem.offsetHeight - 1;
        left = dateInputOffset.left;
        position = "absolute";
        windowWidth = djQ(options.contentWindow).width();
        windowHeight = djQ(options.contentWindow).height();
        windowScrollTop = djQ(options.contentWindow).scrollTop();

        if (options.ownerDocument.documentElement.clientWidth - dateInputOffset.left < datepicker.parent().outerWidth(true)) {
          var diff = datepicker.parent().outerWidth(true) - dateInputElem.offsetWidth;
          left = left - diff;
        }

        if ($dateInput.parent().css('direction') === 'rtl') {
          left -= datetimepicker.outerWidth() - $dateInput.outerWidth();
        }

        if (options.fixed) {
          verticalPosition -= windowScrollTop;
          left -= djQ(options.contentWindow).scrollLeft();
          position = "fixed";
        } else {
          dateInputHasFixedAncestor = false;
          forEachAncestorOf(dateInputElem, function (ancestorNode) {
            if (ancestorNode === null) {
              return false;
            }

            if (options.contentWindow.getComputedStyle(ancestorNode).getPropertyValue('position') === 'fixed') {
              dateInputHasFixedAncestor = true;
              return false;
            }
          });

          if (dateInputHasFixedAncestor && !options.insideParent) {
            position = 'fixed'; //If the picker won't fit entirely within the viewport then display it above the date input.

            if (verticalPosition + datetimepicker.outerHeight() > windowHeight + windowScrollTop) {
              verticalAnchorEdge = 'bottom';
              verticalPosition = windowHeight + windowScrollTop - dateInputOffset.top;
            } else {
              verticalPosition -= windowScrollTop;
            }
          } else {
            if (verticalPosition + datetimepicker[0].offsetHeight > windowHeight + windowScrollTop) {
              verticalPosition = dateInputOffset.top - datetimepicker[0].offsetHeight + 1;
            }
          }

          if (verticalPosition < 0) {
            verticalPosition = 0;
          }

          if (left + dateInputElem.offsetWidth > windowWidth) {
            left = windowWidth - dateInputElem.offsetWidth;
          }
        }

        datetimepickerElem = datetimepicker[0];
        forEachAncestorOf(datetimepickerElem, function (ancestorNode) {
          var ancestorNodePosition;
          ancestorNodePosition = options.contentWindow.getComputedStyle(ancestorNode).getPropertyValue('position');

          if (ancestorNodePosition === 'relative' && windowWidth >= ancestorNode.offsetWidth) {
            left = left - (windowWidth - ancestorNode.offsetWidth) / 2;
            return false;
          }
        });
        datetimepickerCss = {
          position: position,
          left: options.insideParent ? dateInputElem.offsetLeft : left,
          top: '',
          //Initialize to prevent previous values interfering with new ones.
          bottom: '' //Initialize to prevent previous values interfering with new ones.

        };

        if (options.insideParent) {
          datetimepickerCss[verticalAnchorEdge] = dateInputElem.offsetTop + dateInputElem.offsetHeight;
        } else {
          datetimepickerCss[verticalAnchorEdge] = verticalPosition;
        }

        datetimepicker.css(datetimepickerCss);
      };

      datetimepicker.on('open.xdsoft', function (event) {
        var onShow = true;

        if (options.onShow && djQ.isFunction(options.onShow)) {
          onShow = options.onShow.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), event);
        }

        if (onShow !== false) {
          datetimepicker.show();
          setPos();
          djQ(options.contentWindow).off('resize.xdsoft', setPos).on('resize.xdsoft', setPos);

          if (options.closeOnWithoutClick) {
            djQ([options.ownerDocument.body, options.contentWindow]).on('touchstart mousedown.xdsoft', function arguments_callee6() {
              datetimepicker.trigger('close.xdsoft');
              djQ([options.ownerDocument.body, options.contentWindow]).off('touchstart mousedown.xdsoft', arguments_callee6);
            });
          }
        }
      }).on('close.xdsoft', function (event) {
        var onClose = true;
        month_picker.find('.xdsoft_month,.xdsoft_year').find('.xdsoft_select').hide();

        if (options.onClose && djQ.isFunction(options.onClose)) {
          onClose = options.onClose.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), event);
        }

        if (onClose !== false && !options.opened && !options.inline) {
          datetimepicker.hide();
        }

        event.stopPropagation();
      }).on('toggle.xdsoft', function () {
        if (datetimepicker.is(':visible')) {
          datetimepicker.trigger('close.xdsoft');
        } else {
          datetimepicker.trigger('open.xdsoft');
        }
      }).data('input', input);
      timer = 0;
      datetimepicker.data('xdsoft_datetime', _xdsoft_datetime);
      datetimepicker.setOptions(options);

      function getCurrentValue() {
        var ct = false,
            time;

        if (options.startDate) {
          ct = _xdsoft_datetime.strToDate(options.startDate);
        } else {
          ct = options.value || (input && input.val && input.val() ? input.val() : '');

          if (ct) {
            ct = _xdsoft_datetime.strToDateTime(ct);

            if (options.yearOffset) {
              ct = new Date(ct.getFullYear() - options.yearOffset, ct.getMonth(), ct.getDate(), ct.getHours(), ct.getMinutes(), ct.getSeconds(), ct.getMilliseconds());
            }
          } else if (options.defaultDate) {
            ct = _xdsoft_datetime.strToDateTime(options.defaultDate);

            if (options.defaultTime) {
              time = _xdsoft_datetime.strtotime(options.defaultTime);
              ct.setHours(time.getHours());
              ct.setMinutes(time.getMinutes());
            }
          }
        }

        if (ct && _xdsoft_datetime.isValidDate(ct)) {
          datetimepicker.data('changed', true);
        } else {
          ct = '';
        }

        return ct || 0;
      }

      function setMask(options) {
        var isValidValue = function isValidValue(mask, value) {
          var reg = mask.replace(/([\[\]\/\{\}\(\)\-\.\+]{1})/g, '\\$1').replace(/_/g, '{digit+}').replace(/([0-9]{1})/g, '{digit$1}').replace(/\{digit([0-9]{1})\}/g, '[0-$1_]{1}').replace(/\{digit[\+]\}/g, '[0-9_]{1}');
          return new RegExp(reg).test(value);
        },
            getCaretPos = function getCaretPos(input) {
          try {
            if (options.ownerDocument.selection && options.ownerDocument.selection.createRange) {
              var range = options.ownerDocument.selection.createRange();
              return range.getBookmark().charCodeAt(2) - 2;
            }

            if (input.setSelectionRange) {
              return input.selectionStart;
            }
          } catch (e) {
            return 0;
          }
        },
            setCaretPos = function setCaretPos(node, pos) {
          node = typeof node === "string" || node instanceof String ? options.ownerDocument.getElementById(node) : node;

          if (!node) {
            return false;
          }

          if (node.createTextRange) {
            var textRange = node.createTextRange();
            textRange.collapse(true);
            textRange.moveEnd('character', pos);
            textRange.moveStart('character', pos);
            textRange.select();
            return true;
          }

          if (node.setSelectionRange) {
            node.setSelectionRange(pos, pos);
            return true;
          }

          return false;
        };

        if (options.mask) {
          input.off('keydown.xdsoft');
        }

        if (options.mask === true) {
          if (dateHelper.formatMask) {
            options.mask = dateHelper.formatMask(options.format);
          } else {
            options.mask = options.format.replace(/Y/g, '9999').replace(/F/g, '9999').replace(/m/g, '19').replace(/d/g, '39').replace(/H/g, '29').replace(/i/g, '59').replace(/s/g, '59');
          }
        }

        if (djQ.type(options.mask) === 'string') {
          if (!isValidValue(options.mask, input.val())) {
            input.val(options.mask.replace(/[0-9]/g, '_'));
            setCaretPos(input[0], 0);
          }

          input.on('paste.xdsoft', function (event) {
            // couple options here
            // 1. return false - tell them they can't paste
            // 2. insert over current characters - minimal validation
            // 3. full fledged parsing and validation
            // let's go option 2 for now
            // fires multiple times for some reason
            // https://stackoverflow.com/a/30496488/1366033
            var clipboardData = event.clipboardData || event.originalEvent.clipboardData || window.clipboardData,
                pastedData = clipboardData.getData('text'),
                val = this.value,
                pos = this.selectionStart;
            var valueBeforeCursor = val.substr(0, pos);
            var valueAfterPaste = val.substr(pos + pastedData.length);
            val = valueBeforeCursor + pastedData + valueAfterPaste;
            pos += pastedData.length;

            if (isValidValue(options.mask, val)) {
              this.value = val;
              setCaretPos(this, pos);
            } else if (djQ.trim(val) === '') {
              this.value = options.mask.replace(/[0-9]/g, '_');
            } else {
              input.trigger('error_input.xdsoft');
            }

            event.preventDefault();
            return false;
          });
          input.on('keydown.xdsoft', function (event) {
            var val = this.value,
                key = event.which,
                pos = this.selectionStart,
                selEnd = this.selectionEnd,
                hasSel = pos !== selEnd,
                digit; // only alow these characters

            if (key >= KEY0 && key <= KEY9 || key >= _KEY0 && key <= _KEY9 || key === BACKSPACE || key === DEL) {
              // get char to insert which is new character or placeholder ('_')
              digit = key === BACKSPACE || key === DEL ? '_' : String.fromCharCode(_KEY0 <= key && key <= _KEY9 ? key - KEY0 : key); // we're deleting something, we're not at the start, and have normal cursor, move back one
              // if we have a selection length, cursor actually sits behind deletable char, not in front

              if (key === BACKSPACE && pos && !hasSel) {
                pos -= 1;
              } // don't stop on a separator, continue whatever direction you were going
              //   value char - keep incrementing position while on separator char and we still have room
              //   del char   - keep decrementing position while on separator char and we still have room


              while (true) {
                var maskValueAtCurPos = options.mask.substr(pos, 1);
                var posShorterThanMaskLength = pos < options.mask.length;
                var posGreaterThanZero = pos > 0;
                var notNumberOrPlaceholder = /[^0-9_]/;
                var curPosOnSep = notNumberOrPlaceholder.test(maskValueAtCurPos);
                var continueMovingPosition = curPosOnSep && posShorterThanMaskLength && posGreaterThanZero; // if we hit a real char, stay where we are

                if (!continueMovingPosition) break; // hitting backspace in a selection, you can possibly go back any further - go forward

                pos += key === BACKSPACE && !hasSel ? -1 : 1;
              }

              if (event.metaKey) {
                // cmd has been pressed
                pos = 0;
                hasSel = true;
              }

              if (hasSel) {
                // pos might have moved so re-calc length
                var selLength = selEnd - pos; // if we have a selection length we will wipe out entire selection and replace with default template for that range

                var defaultBlank = options.mask.replace(/[0-9]/g, '_');
                var defaultBlankSelectionReplacement = defaultBlank.substr(pos, selLength);
                var selReplacementRemainder = defaultBlankSelectionReplacement.substr(1); // might be empty

                var valueBeforeSel = val.substr(0, pos);
                var insertChars = digit + selReplacementRemainder;
                var charsAfterSelection = val.substr(pos + selLength);
                val = valueBeforeSel + insertChars + charsAfterSelection;
              } else {
                var valueBeforeCursor = val.substr(0, pos);
                var insertChar = digit;
                var valueAfterNextChar = val.substr(pos + 1);
                val = valueBeforeCursor + insertChar + valueAfterNextChar;
              }

              if (djQ.trim(val) === '') {
                // if empty, set to default
                val = defaultBlank;
              } else {
                // if at the last character don't need to do anything
                if (pos === options.mask.length) {
                  event.preventDefault();
                  return false;
                }
              } // resume cursor location


              pos += key === BACKSPACE ? 0 : 1; // don't stop on a separator, continue whatever direction you were going

              while (/[^0-9_]/.test(options.mask.substr(pos, 1)) && pos < options.mask.length && pos > 0) {
                pos += key === BACKSPACE ? 0 : 1;
              }

              if (isValidValue(options.mask, val)) {
                this.value = val;
                setCaretPos(this, pos);
              } else if (djQ.trim(val) === '') {
                this.value = options.mask.replace(/[0-9]/g, '_');
              } else {
                input.trigger('error_input.xdsoft');
              }
            } else {
              if ([AKEY, CKEY, VKEY, ZKEY, YKEY].indexOf(key) !== -1 && ctrlDown || [ESC, ARROWUP, ARROWDOWN, ARROWLEFT, ARROWRIGHT, F5, CTRLKEY, TAB, ENTER].indexOf(key) !== -1) {
                return true;
              }
            }

            event.preventDefault();
            return false;
          });
        }
      }

      _xdsoft_datetime.setCurrentTime(getCurrentValue());

      input.data('xdsoft_datetimepicker', datetimepicker).on('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', function () {
        if (input.is(':disabled') || input.data('xdsoft_datetimepicker').is(':visible') && options.closeOnInputClick) {
          return;
        }

        if (!options.openOnFocus) {
          return;
        }

        clearTimeout(timer);
        timer = setTimeout(function () {
          if (input.is(':disabled')) {
            return;
          }

          triggerAfterOpen = true;

          _xdsoft_datetime.setCurrentTime(getCurrentValue(), true);

          if (options.mask) {
            setMask(options);
          }

          datetimepicker.trigger('open.xdsoft');
        }, 100);
      }).on('keydown.xdsoft', function (event) {
        var elementSelector,
            key = event.which;

        if ([ENTER].indexOf(key) !== -1 && options.enterLikeTab) {
          elementSelector = djQ("input:visible,textarea:visible,button:visible,a:visible");
          datetimepicker.trigger('close.xdsoft');
          elementSelector.eq(elementSelector.index(this) + 1).focus();
          return false;
        }

        if ([TAB].indexOf(key) !== -1) {
          datetimepicker.trigger('close.xdsoft');
          return true;
        }
      }).on('blur.xdsoft', function () {
        datetimepicker.trigger('close.xdsoft');
      });
    };

    destroyDateTimePicker = function destroyDateTimePicker(input) {
      var datetimepicker = input.data('xdsoft_datetimepicker');

      if (datetimepicker) {
        datetimepicker.data('xdsoft_datetime', null);
        datetimepicker.remove();
        input.data('xdsoft_datetimepicker', null).off('.xdsoft');
        djQ(options.contentWindow).off('resize.xdsoft');
        djQ([options.contentWindow, options.ownerDocument.body]).off('mousedown.xdsoft touchstart');

        if (input.unmousewheel) {
          input.unmousewheel();
        }
      }
    };

    djQ(options.ownerDocument).off('keydown.xdsoftctrl keyup.xdsoftctrl').off('keydown.xdsoftcmd keyup.xdsoftcmd').on('keydown.xdsoftctrl', function (e) {
      if (e.keyCode === CTRLKEY) {
        ctrlDown = true;
      }
    }).on('keyup.xdsoftctrl', function (e) {
      if (e.keyCode === CTRLKEY) {
        ctrlDown = false;
      }
    }).on('keydown.xdsoftcmd', function (e) {
      if (e.keyCode === CMDKEY) {
        cmdDown = true;
      }
    }).on('keyup.xdsoftcmd', function (e) {
      if (e.keyCode === CMDKEY) {
        cmdDown = false;
      }
    });
    this.each(function () {
      var datetimepicker = djQ(this).data('xdsoft_datetimepicker'),
          $input;

      if (datetimepicker) {
        if (djQ.type(opt) === 'string') {
          switch (opt) {
            case 'show':
              djQ(this).select().focus();
              datetimepicker.trigger('open.xdsoft');
              break;

            case 'hide':
              datetimepicker.trigger('close.xdsoft');
              break;

            case 'toggle':
              datetimepicker.trigger('toggle.xdsoft');
              break;

            case 'destroy':
              destroyDateTimePicker(djQ(this));
              break;

            case 'reset':
              this.value = this.defaultValue;

              if (!this.value || !datetimepicker.data('xdsoft_datetime').isValidDate(dateHelper.parseDate(this.value, options.format))) {
                datetimepicker.data('changed', false);
              }

              datetimepicker.data('xdsoft_datetime').setCurrentTime(this.value);
              break;

            case 'validate':
              $input = datetimepicker.data('input');
              $input.trigger('blur.xdsoft');
              break;

            default:
              if (datetimepicker[opt] && djQ.isFunction(datetimepicker[opt])) {
                result = datetimepicker[opt](opt2);
              }

          }
        } else {
          datetimepicker.setOptions(opt);
        }

        return 0;
      }

      if (djQ.type(opt) !== 'string') {
        if (!options.lazyInit || options.open || options.inline) {
          createDateTimePicker(djQ(this));
        } else {
          lazyInit(djQ(this));
        }
      }
    });
    return result;
  };

  djQ.fn.datetimepicker.defaults = default_options;

  function HighlightedDate(date, desc, style) {
    "use strict";

    this.date = date;
    this.desc = desc;
    this.style = style;
  }
};

;

(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery', 'jquery-mousewheel'], factory);
  } else if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object') {
    // Node/CommonJS style for Browserify
    module.exports = factory(require('jquery'));
    ;
  } else {
    // Browser globals
    factory(djQ);
  }
})(datetimepickerFactory);
/*!
 * jQuery Mousewheel 3.1.13
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 */


(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object') {
    // Node/CommonJS style for Browserify
    module.exports = factory;
  } else {
    // Browser globals
    factory(djQ);
  }
})(function ($) {
  var toFix = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'],
      toBind = 'onwheel' in document || document.documentMode >= 9 ? ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'],
      slice = Array.prototype.slice,
      nullLowestDeltaTimeout,
      lowestDelta;

  if (djQ.event.fixHooks) {
    for (var i = toFix.length; i;) {
      djQ.event.fixHooks[toFix[--i]] = djQ.event.mouseHooks;
    }
  }

  var special = djQ.event.special.mousewheel = {
    version: '3.1.12',
    setup: function setup() {
      if (this.addEventListener) {
        for (var i = toBind.length; i;) {
          this.addEventListener(toBind[--i], handler, false);
        }
      } else {
        this.onmousewheel = handler;
      } // Store the line height and page height for this particular element


      djQ.data(this, 'mousewheel-line-height', special.getLineHeight(this));
      djQ.data(this, 'mousewheel-page-height', special.getPageHeight(this));
    },
    teardown: function teardown() {
      if (this.removeEventListener) {
        for (var i = toBind.length; i;) {
          this.removeEventListener(toBind[--i], handler, false);
        }
      } else {
        this.onmousewheel = null;
      } // Clean up the data we added to the element


      djQ.removeData(this, 'mousewheel-line-height');
      djQ.removeData(this, 'mousewheel-page-height');
    },
    getLineHeight: function getLineHeight(elem) {
      var $elem = djQ(elem),
          $parent = $elem['offsetParent' in djQ.fn ? 'offsetParent' : 'parent']();

      if (!$parent.length) {
        $parent = djQ('body');
      }

      return parseInt($parent.css('fontSize'), 10) || parseInt($elem.css('fontSize'), 10) || 16;
    },
    getPageHeight: function getPageHeight(elem) {
      return djQ(elem).height();
    },
    settings: {
      adjustOldDeltas: true,
      // see shouldAdjustOldDeltas() below
      normalizeOffset: true // calls getBoundingClientRect for each event

    }
  };
  djQ.fn.extend({
    mousewheel: function mousewheel(fn) {
      return fn ? this.bind('mousewheel', fn) : this.trigger('mousewheel');
    },
    unmousewheel: function unmousewheel(fn) {
      return this.unbind('mousewheel', fn);
    }
  });

  function handler(event) {
    var orgEvent = event || window.event,
        args = slice.call(arguments, 1),
        delta = 0,
        deltaX = 0,
        deltaY = 0,
        absDelta = 0,
        offsetX = 0,
        offsetY = 0;
    event = djQ.event.fix(orgEvent);
    event.type = 'mousewheel'; // Old school scrollwheel delta

    if ('detail' in orgEvent) {
      deltaY = orgEvent.detail * -1;
    }

    if ('wheelDelta' in orgEvent) {
      deltaY = orgEvent.wheelDelta;
    }

    if ('wheelDeltaY' in orgEvent) {
      deltaY = orgEvent.wheelDeltaY;
    }

    if ('wheelDeltaX' in orgEvent) {
      deltaX = orgEvent.wheelDeltaX * -1;
    } // Firefox < 17 horizontal scrolling related to DOMMouseScroll event


    if ('axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
      deltaX = deltaY * -1;
      deltaY = 0;
    } // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy


    delta = deltaY === 0 ? deltaX : deltaY; // New school wheel delta (wheel event)

    if ('deltaY' in orgEvent) {
      deltaY = orgEvent.deltaY * -1;
      delta = deltaY;
    }

    if ('deltaX' in orgEvent) {
      deltaX = orgEvent.deltaX;

      if (deltaY === 0) {
        delta = deltaX * -1;
      }
    } // No change actually happened, no reason to go any further


    if (deltaY === 0 && deltaX === 0) {
      return;
    } // Need to convert lines and pages to pixels if we aren't already in pixels
    // There are three delta modes:
    //   * deltaMode 0 is by pixels, nothing to do
    //   * deltaMode 1 is by lines
    //   * deltaMode 2 is by pages


    if (orgEvent.deltaMode === 1) {
      var lineHeight = djQ.data(this, 'mousewheel-line-height');
      delta *= lineHeight;
      deltaY *= lineHeight;
      deltaX *= lineHeight;
    } else if (orgEvent.deltaMode === 2) {
      var pageHeight = djQ.data(this, 'mousewheel-page-height');
      delta *= pageHeight;
      deltaY *= pageHeight;
      deltaX *= pageHeight;
    } // Store lowest absolute delta to normalize the delta values


    absDelta = Math.max(Math.abs(deltaY), Math.abs(deltaX));

    if (!lowestDelta || absDelta < lowestDelta) {
      lowestDelta = absDelta; // Adjust older deltas if necessary

      if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
        lowestDelta /= 40;
      }
    } // Adjust older deltas if necessary


    if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
      // Divide all the things by 40!
      delta /= 40;
      deltaX /= 40;
      deltaY /= 40;
    } // Get a whole, normalized value for the deltas


    delta = Math[delta >= 1 ? 'floor' : 'ceil'](delta / lowestDelta);
    deltaX = Math[deltaX >= 1 ? 'floor' : 'ceil'](deltaX / lowestDelta);
    deltaY = Math[deltaY >= 1 ? 'floor' : 'ceil'](deltaY / lowestDelta); // Normalise offsetX and offsetY properties

    if (special.settings.normalizeOffset && this.getBoundingClientRect) {
      var boundingRect = this.getBoundingClientRect();
      offsetX = event.clientX - boundingRect.left;
      offsetY = event.clientY - boundingRect.top;
    } // Add information to the event object


    event.deltaX = deltaX;
    event.deltaY = deltaY;
    event.deltaFactor = lowestDelta;
    event.offsetX = offsetX;
    event.offsetY = offsetY; // Go ahead and set deltaMode to 0 since we converted to pixels
    // Although this is a little odd since we overwrite the deltaX/Y
    // properties with normalized deltas.

    event.deltaMode = 0; // Add event and delta to the front of the arguments

    args.unshift(event, delta, deltaX, deltaY); // Clearout lowestDelta after sometime to better
    // handle multiple device types that give different
    // a different lowestDelta
    // Ex: trackpad = 3 and mouse wheel = 120

    if (nullLowestDeltaTimeout) {
      clearTimeout(nullLowestDeltaTimeout);
    }

    nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);
    return (djQ.event.dispatch || djQ.event.handle).apply(this, args);
  }

  function nullLowestDelta() {
    lowestDelta = null;
  }

  function shouldAdjustOldDeltas(orgEvent, absDelta) {
    // If this is an older event and the delta is divisable by 120,
    // then we are assuming that the browser is treating this as an
    // older mouse wheel event and that we should divide the deltas
    // by 40 to try and get a more usable deltaFactor.
    // Side note, this actually impacts the reported scroll distance
    // in older browsers and can cause scrolling to be slower than native.
    // Turn this off by setting djQ.event.special.mousewheel.settings.adjustOldDeltas to false.
    return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
  }
});
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function (global, factory) {
  (typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object' && typeof module !== 'undefined' ? factory(exports) : typeof define === 'function' && define.amd ? define(['exports'], factory) : factory(global.blobUtil = {});
})(this, function (exports) {
  'use strict'; // TODO: including these in blob-util.ts causes typedoc to generate docs for them,
  // even with --excludePrivate ¯\_(ツ)_/¯

  /** @private */

  function loadImage(src, crossOrigin) {
    return new Promise(function (resolve, reject) {
      var img = new Image();

      if (crossOrigin) {
        img.crossOrigin = crossOrigin;
      }

      img.onload = function () {
        resolve(img);
      };

      img.onerror = reject;
      img.src = src;
    });
  }
  /** @private */


  function imgToCanvas(img) {
    var canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height; // copy the image contents to the canvas

    var context = canvas.getContext('2d');
    context.drawImage(img, 0, 0, img.width, img.height, 0, 0, img.width, img.height);
    return canvas;
  }
  /* global Promise, Image, Blob, FileReader, atob, btoa,
     BlobBuilder, MSBlobBuilder, MozBlobBuilder, WebKitBlobBuilder, webkitURL */

  /**
   * Shim for
   * [`new Blob()`](https://developer.mozilla.org/en-US/docs/Web/API/Blob.Blob)
   * to support
   * [older browsers that use the deprecated `BlobBuilder` API](http://caniuse.com/blob).
   *
   * Example:
   *
   * ```js
   * var myBlob = blobUtil.createBlob(['hello world'], {type: 'text/plain'});
   * ```
   *
   * @param parts - content of the Blob
   * @param properties - usually `{type: myContentType}`,
   *                           you can also pass a string for the content type
   * @returns Blob
   */


  function createBlob(parts, properties) {
    parts = parts || [];
    properties = properties || {};

    if (typeof properties === 'string') {
      properties = {
        type: properties
      }; // infer content type
    }

    try {
      return new Blob(parts, properties);
    } catch (e) {
      if (e.name !== 'TypeError') {
        throw e;
      }

      var Builder = typeof BlobBuilder !== 'undefined' ? BlobBuilder : typeof MSBlobBuilder !== 'undefined' ? MSBlobBuilder : typeof MozBlobBuilder !== 'undefined' ? MozBlobBuilder : WebKitBlobBuilder;
      var builder = new Builder();

      for (var i = 0; i < parts.length; i += 1) {
        builder.append(parts[i]);
      }

      return builder.getBlob(properties.type);
    }
  }
  /**
   * Shim for
   * [`URL.createObjectURL()`](https://developer.mozilla.org/en-US/docs/Web/API/URL.createObjectURL)
   * to support browsers that only have the prefixed
   * `webkitURL` (e.g. Android <4.4).
   *
   * Example:
   *
   * ```js
   * var myUrl = blobUtil.createObjectURL(blob);
   * ```
   *
   * @param blob
   * @returns url
   */


  function createObjectURL(blob) {
    return (typeof URL !== 'undefined' ? URL : webkitURL).createObjectURL(blob);
  }
  /**
   * Shim for
   * [`URL.revokeObjectURL()`](https://developer.mozilla.org/en-US/docs/Web/API/URL.revokeObjectURL)
   * to support browsers that only have the prefixed
   * `webkitURL` (e.g. Android <4.4).
   *
   * Example:
   *
   * ```js
   * blobUtil.revokeObjectURL(myUrl);
   * ```
   *
   * @param url
   */


  function revokeObjectURL(url) {
    return (typeof URL !== 'undefined' ? URL : webkitURL).revokeObjectURL(url);
  }
  /**
   * Convert a `Blob` to a binary string.
   *
   * Example:
   *
   * ```js
   * blobUtil.blobToBinaryString(blob).then(function (binaryString) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param blob
   * @returns Promise that resolves with the binary string
   */


  function blobToBinaryString(blob) {
    return new Promise(function (resolve, reject) {
      var reader = new FileReader();
      var hasBinaryString = typeof reader.readAsBinaryString === 'function';

      reader.onloadend = function () {
        var result = reader.result || '';

        if (hasBinaryString) {
          return resolve(result);
        }

        resolve(arrayBufferToBinaryString(result));
      };

      reader.onerror = reject;

      if (hasBinaryString) {
        reader.readAsBinaryString(blob);
      } else {
        reader.readAsArrayBuffer(blob);
      }
    });
  }
  /**
   * Convert a base64-encoded string to a `Blob`.
   *
   * Example:
   *
   * ```js
   * var blob = blobUtil.base64StringToBlob(base64String);
   * ```
   * @param base64 - base64-encoded string
   * @param type - the content type (optional)
   * @returns Blob
   */


  function base64StringToBlob(base64, type) {
    var parts = [binaryStringToArrayBuffer(atob(base64))];
    return type ? createBlob(parts, {
      type: type
    }) : createBlob(parts);
  }
  /**
   * Convert a binary string to a `Blob`.
   *
   * Example:
   *
   * ```js
   * var blob = blobUtil.binaryStringToBlob(binaryString);
   * ```
   *
   * @param binary - binary string
   * @param type - the content type (optional)
   * @returns Blob
   */


  function binaryStringToBlob(binary, type) {
    return base64StringToBlob(btoa(binary), type);
  }
  /**
   * Convert a `Blob` to a binary string.
   *
   * Example:
   *
   * ```js
   * blobUtil.blobToBase64String(blob).then(function (base64String) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param blob
   * @returns Promise that resolves with the binary string
   */


  function blobToBase64String(blob) {
    return blobToBinaryString(blob).then(btoa);
  }
  /**
   * Convert a data URL string
   * (e.g. `'data:image/png;base64,iVBORw0KG...'`)
   * to a `Blob`.
   *
   * Example:
   *
   * ```js
   * var blob = blobUtil.dataURLToBlob(dataURL);
   * ```
   *
   * @param dataURL - dataURL-encoded string
   * @returns Blob
   */


  function dataURLToBlob(dataURL) {
    var type = dataURL.match(/data:([^;]+)/)[1];
    var base64 = dataURL.replace(/^[^,]+,/, '');
    var buff = binaryStringToArrayBuffer(atob(base64));
    return createBlob([buff], {
      type: type
    });
  }
  /**
   * Convert a `Blob` to a data URL string
   * (e.g. `'data:image/png;base64,iVBORw0KG...'`).
   *
   * Example:
   *
   * ```js
   * var dataURL = blobUtil.blobToDataURL(blob);
   * ```
   *
   * @param blob
   * @returns Promise that resolves with the data URL string
   */


  function blobToDataURL(blob) {
    return blobToBase64String(blob).then(function (base64String) {
      return 'data:' + blob.type + ';base64,' + base64String;
    });
  }
  /**
   * Convert an image's `src` URL to a data URL by loading the image and painting
   * it to a `canvas`.
   *
   * Note: this will coerce the image to the desired content type, and it
   * will only paint the first frame of an animated GIF.
   *
   * Examples:
   *
   * ```js
   * blobUtil.imgSrcToDataURL('http://mysite.com/img.png').then(function (dataURL) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * ```js
   * blobUtil.imgSrcToDataURL('http://some-other-site.com/img.jpg', 'image/jpeg',
   *                          'Anonymous', 1.0).then(function (dataURL) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param src - image src
   * @param type - the content type (optional, defaults to 'image/png')
   * @param crossOrigin - for CORS-enabled images, set this to
   *                                         'Anonymous' to avoid "tainted canvas" errors
   * @param quality - a number between 0 and 1 indicating image quality
   *                                     if the requested type is 'image/jpeg' or 'image/webp'
   * @returns Promise that resolves with the data URL string
   */


  function imgSrcToDataURL(src, type, crossOrigin, quality) {
    type = type || 'image/png';
    return loadImage(src, crossOrigin).then(imgToCanvas).then(function (canvas) {
      return canvas.toDataURL(type, quality);
    });
  }
  /**
   * Convert a `canvas` to a `Blob`.
   *
   * Examples:
   *
   * ```js
   * blobUtil.canvasToBlob(canvas).then(function (blob) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * Most browsers support converting a canvas to both `'image/png'` and `'image/jpeg'`. You may
   * also want to try `'image/webp'`, which will work in some browsers like Chrome (and in other browsers, will just fall back to `'image/png'`):
   *
   * ```js
   * blobUtil.canvasToBlob(canvas, 'image/webp').then(function (blob) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param canvas - HTMLCanvasElement
   * @param type - the content type (optional, defaults to 'image/png')
   * @param quality - a number between 0 and 1 indicating image quality
   *                                     if the requested type is 'image/jpeg' or 'image/webp'
   * @returns Promise that resolves with the `Blob`
   */


  function canvasToBlob(canvas, type, quality) {
    if (typeof canvas.toBlob === 'function') {
      return new Promise(function (resolve) {
        canvas.toBlob(resolve, type, quality);
      });
    }

    return Promise.resolve(dataURLToBlob(canvas.toDataURL(type, quality)));
  }
  /**
   * Convert an image's `src` URL to a `Blob` by loading the image and painting
   * it to a `canvas`.
   *
   * Note: this will coerce the image to the desired content type, and it
   * will only paint the first frame of an animated GIF.
   *
   * Examples:
   *
   * ```js
   * blobUtil.imgSrcToBlob('http://mysite.com/img.png').then(function (blob) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * ```js
   * blobUtil.imgSrcToBlob('http://some-other-site.com/img.jpg', 'image/jpeg',
   *                          'Anonymous', 1.0).then(function (blob) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param src - image src
   * @param type - the content type (optional, defaults to 'image/png')
   * @param crossOrigin - for CORS-enabled images, set this to
   *                                         'Anonymous' to avoid "tainted canvas" errors
   * @param quality - a number between 0 and 1 indicating image quality
   *                                     if the requested type is 'image/jpeg' or 'image/webp'
   * @returns Promise that resolves with the `Blob`
   */


  function imgSrcToBlob(src, type, crossOrigin, quality) {
    type = type || 'image/png';
    return loadImage(src, crossOrigin).then(imgToCanvas).then(function (canvas) {
      return canvasToBlob(canvas, type, quality);
    });
  }
  /**
   * Convert an `ArrayBuffer` to a `Blob`.
   *
   * Example:
   *
   * ```js
   * var blob = blobUtil.arrayBufferToBlob(arrayBuff, 'audio/mpeg');
   * ```
   *
   * @param buffer
   * @param type - the content type (optional)
   * @returns Blob
   */


  function arrayBufferToBlob(buffer, type) {
    return createBlob([buffer], type);
  }
  /**
   * Convert a `Blob` to an `ArrayBuffer`.
   *
   * Example:
   *
   * ```js
   * blobUtil.blobToArrayBuffer(blob).then(function (arrayBuff) {
   *   // success
   * }).catch(function (err) {
   *   // error
   * });
   * ```
   *
   * @param blob
   * @returns Promise that resolves with the `ArrayBuffer`
   */


  function blobToArrayBuffer(blob) {
    return new Promise(function (resolve, reject) {
      var reader = new FileReader();

      reader.onloadend = function () {
        var result = reader.result || new ArrayBuffer(0);
        resolve(result);
      };

      reader.onerror = reject;
      reader.readAsArrayBuffer(blob);
    });
  }
  /**
   * Convert an `ArrayBuffer` to a binary string.
   *
   * Example:
   *
   * ```js
   * var myString = blobUtil.arrayBufferToBinaryString(arrayBuff)
   * ```
   *
   * @param buffer - array buffer
   * @returns binary string
   */


  function arrayBufferToBinaryString(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var length = bytes.byteLength;
    var i = -1;

    while (++i < length) {
      binary += String.fromCharCode(bytes[i]);
    }

    return binary;
  }
  /**
   * Convert a binary string to an `ArrayBuffer`.
   *
   * ```js
   * var myBuffer = blobUtil.binaryStringToArrayBuffer(binaryString)
   * ```
   *
   * @param binary - binary string
   * @returns array buffer
   */


  function binaryStringToArrayBuffer(binary) {
    var length = binary.length;
    var buf = new ArrayBuffer(length);
    var arr = new Uint8Array(buf);
    var i = -1;

    while (++i < length) {
      arr[i] = binary.charCodeAt(i);
    }

    return buf;
  }

  exports.createBlob = createBlob;
  exports.createObjectURL = createObjectURL;
  exports.revokeObjectURL = revokeObjectURL;
  exports.blobToBinaryString = blobToBinaryString;
  exports.base64StringToBlob = base64StringToBlob;
  exports.binaryStringToBlob = binaryStringToBlob;
  exports.blobToBase64String = blobToBase64String;
  exports.dataURLToBlob = dataURLToBlob;
  exports.blobToDataURL = blobToDataURL;
  exports.imgSrcToDataURL = imgSrcToDataURL;
  exports.canvasToBlob = canvasToBlob;
  exports.imgSrcToBlob = imgSrcToBlob;
  exports.arrayBufferToBlob = arrayBufferToBlob;
  exports.blobToArrayBuffer = blobToArrayBuffer;
  exports.arrayBufferToBinaryString = arrayBufferToBinaryString;
  exports.binaryStringToArrayBuffer = binaryStringToArrayBuffer;
  Object.defineProperty(exports, '__esModule', {
    value: true
  });
});
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var DAVE_SETTINGS = {
  BASE_URL: "https://staging.iamdave.ai",
  //test.iamdave.ai
  SIGNUP_API_KEY: null,
  ENTERPRISE_ID: null,
  CONVERSATION_ID: null,
  SPEECH_SERVER: null,
  SPEECH_RECOGNIZER: 'google',
  SPEECH_MODEL_NAME: 'indian_english',
  USER_MODEL: 'person',
  USER_ID_ATTR: 'user_id',
  USER_EMAIL_ATTR: 'email',
  USER_PHONE_NUMBER_ATTR: 'mobile_number',
  LOGIN_ATTRS: null,
  DEFAULT_SYSTEM_RESPONSE: null,
  DEFAULT_USER_DATA: {
    "person_type": "visitor"
  },
  SESSION_MODEL: 'person_session',
  SESSION_ID: 'session_id',
  SESSION_USER_ID: 'user_id',
  DEFAULT_SESSION_DATA: {
    'location_dict': '{agent_info.ip}',
    'browser': '{agent_info.browser}',
    'os': '{agent_info.os}',
    'device_type': '{agent_info.device}'
  },
  LANGUAGE_CONVERSATION_MAP: {},
  INTERACTION_MODEL: 'interaction',
  INTERACTION_USER_ID: 'person_id',
  INTERACTION_SESSION_ID: 'session_id',
  INTERACTION_STAGE_ATTR: 'stage',
  DEFAULT_INTERACTION_DATA: {},
  LANGUAGE: null,
  VOICE_ID: null,
  MAX_SPEECH_DURATION: 10000,
  SESSION_ORIGIN: null,
  INTERACTION_ORIGIN: null,
  AVATAR_ID: null,
  AVATAR_MODEL: "avatar",
  LANGUAGE_AVATAR_MAP: {},
  GLB_ATTR: "glb_url",
  EVENT_MODEL: null,
  EVENT_SESSION_ID: 'session_id',
  EVENT_USER_ID: 'user_id',
  EVENT_DESCRIPTION: 'event',
  EVENT_WAIT_TIME: 'wait_time',
  EVENT_ORIGIN: 'origin',
  DEFAULT_EVENT_DATA: null,
  EVENT_TIMER: null,
  GOOGLE_MAP_KEY: null,
  RESPONSE_MODEL: null,
  RESPONSE_GLB_ATTR: "glb_url",
  RESPONSE_ATTR: "response",
  RESPONSE_PLAY_CREDITS_ATTR: "available_play_credits",
  RESPONSE_TOTAL_PLAYS_ATTR: "total_plays"
};

if (document.currentScript && isFinite(document.currentScript.src.split('/').slice(-2, -1)[0])) {
  DAVE_SETTINGS.asset_path = document.currentScript && document.currentScript.src.split('/').slice(0, -4).join('/') + '/';
} else {
  DAVE_SETTINGS.asset_path = document.currentScript && document.currentScript.src.split('/').slice(0, -3).join('/') + '/' || "https://chatbot-plugin.iamdave.ai/";
}

if (typeof daveAvatar !== 'undefined') {
  var DAVE_SCENE = new daveAvatar();
} else {
  var DAVE_SCENE = {};
}

var DAVE_HELP = null;

DAVE_SETTINGS.signup = function signup(data, callbackFunc, errorFunc, force) {
  if (DAVE_SETTINGS.getCookie("dave_started_signup") || DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("started_signup") || DAVE_SETTINGS.getCookie("authentication")) {
    console.warn("Another dave signup going on, so exiting");
    return;
  }

  if (!force && DAVE_SETTINGS.custom_signup && typeof DAVE_SETTINGS.custom_signup == 'function') {
    DAVE_SETTINGS.custom_signup(data, callbackFunc, errorFunc);
    return;
  }

  DAVE_SETTINGS.execute_custom_callback('before_customer_signup', [data]);
  DAVE_SETTINGS.clearCookies();
  DAVE_SETTINGS.setCookie("dave_started_signup", true);
  var signupurl = DAVE_SETTINGS.BASE_URL + "/customer-signup/" + DAVE_SETTINGS.USER_MODEL; //Password string generator

  var randomstring = Math.random().toString(36).slice(-8);
  data = data || {};

  if (!data.email) {
    data.email = "ananth+" + randomstring + "@iamdave.ai";
  }

  for (var k in DAVE_SETTINGS.DEFAULT_USER_DATA) {
    if (!data[k]) {
      data[k] = DAVE_SETTINGS.DEFAULT_USER_DATA[k];
    }
  }

  function on_success(data) {
    HEADERS = {
      "Content-Type": "application/json",
      "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
      "X-I2CE-USER-ID": data[DAVE_SETTINGS.USER_ID_ATTR],
      "X-I2CE-API-KEY": data.api_key
    };
    DAVE_SETTINGS.setCookie("dave_authentication", JSON.stringify(HEADERS), 24);
    DAVE_SETTINGS.setCookie("dave_user_id", data[DAVE_SETTINGS.USER_ID_ATTR]);
    DAVE_SETTINGS.setCookie("dave_conversation_id", DAVE_SETTINGS.CONVERSATION_ID);
    DAVE_SETTINGS.clearCookie("dave_started_signup");
    DAVE_SETTINGS.clearCookie("started_signup");
    DAVE_SETTINGS.clearCookie("authentication");
    DAVE_SETTINGS.clearCookie("user_id");
    DAVE_SETTINGS.clearCookie("conversation_id");
    DAVE_SETTINGS.clearCookie("engagement_id");
    DAVE_SETTINGS.clearCookie("dave_history");
    DAVE_SETTINGS.initialize_session();

    if (callbackFunc && typeof callbackFunc == 'function') {
      callbackFunc(data);
    }

    DAVE_SETTINGS.predict();
  }

  djQ.ajax({
    url: signupurl,
    method: "POST",
    dataType: "json",
    contentType: "json",
    withCredentials: true,
    headers: {
      "Content-Type": "application/json",
      "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
      "X-I2CE-SIGNUP-API-KEY": DAVE_SETTINGS.SIGNUP_API_KEY
    },
    data: JSON.stringify(data),
    success: function success(data1) {
      on_success(data1);
    },
    error: function error(r, e) {
      console.error(e);
      DAVE_SETTINGS.clearCookie("started_signup");
      DAVE_SETTINGS.clearCookie("dave_started_signup");

      if (errorFunc && typeof callbackFunc == 'function') {
        errorFunc(r, e);
      }
    }
  });
}; // Login function


DAVE_SETTINGS.login = function login(user_name, password, callbackFunc, errorFunc, unauthorizedFunc, attrs) {
  var params = {};
  params["user_id"] = user_name;
  params["password"] = password;
  params["roles"] = DAVE_SETTINGS.USER_MODEL;
  params["attrs"] = attrs || DAVE_SETTINGS.LOGIN_ATTRS || [DAVE_SETTINGS.USER_ID_ATTR, DAVE_SETTINGS.USER_EMAIL_ATTR, DAVE_SETTINGS.USER_PHONE_NUMBER_ATTR];
  params["enterprise_id"] = DAVE_SETTINGS.ENTERPRISE_ID;
  DAVE_SETTINGS.ajaxRequestWithData("/dave/oauth", "POST", JSON.stringify(params), function (data) {
    var HEADERS = {
      "Content-Type": "application/json",
      "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
      "X-I2CE-USER-ID": data.user_id,
      "X-I2CE-API-KEY": data.api_key
    };
    DAVE_SETTINGS.setCookie("dave_authentication", JSON.stringify(HEADERS), 24);
    DAVE_SETTINGS.setCookie("dave_user_id", data.user_id);
    DAVE_SETTINGS.setCookie("dave_conversation_id", DAVE_SETTINGS.CONVERSATION_ID);
    DAVE_SETTINGS.clearCookie("dave_started_signup");
    DAVE_SETTINGS.clearCookie("dave_engagement_id");
    DAVE_SETTINGS.setCookie("dave_logged_in", true);
    DAVE_SETTINGS.clearCookie("dave_history");
    DAVE_SETTINGS.initialize_session();

    if (callbackFunc && typeof callbackFunc == 'function') {
      callbackFunc(data);
    }

    DAVE_SETTINGS.predict();
    DAVE_SETTINGS.history(null, null, null, true);
  }, errorFunc, unauthorizedFunc);
}; // You can get the column names of any model with the following API
// DAVE_SETTINGS.ajaxRequestWithData("/attributes/<model_name>/name", "GET", 
//
//


DAVE_SETTINGS.patch_user = function patch_user(params, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.patch_user, [params, callbackFunc, errorFunc, repeats || 0], "dave_user_id")) {
    return;
  } // e.g. patch_user({"pincode": <pincode>, "name": <name of user>, "company_name": <company_name>}, function(data) {console.log(data)});
  // In the response you will get the warehouse_id


  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  params = params || {};

  if (params.person_name && params.email && params.mobile_number) {
    params["signed_up"] = true;
  }

  DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.USER_MODEL + "/" + user_id, "PATCH", JSON.stringify(params), callbackFunc, errorFunc);
};

DAVE_SETTINGS.create_session = function create_session(params, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.create_session, [params, callbackFunc, errorFunc, repeats || 0], "dave_user_id")) {
    return;
  }

  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  params[DAVE_SETTINGS.SESSION_USER_ID] = user_id;

  for (var k in DAVE_SETTINGS.DEFAULT_SESSION_DATA) {
    params[k] = DAVE_SETTINGS.DEFAULT_SESSION_DATA[k];
  }

  DAVE_SETTINGS.get_url_params(params);

  if (DAVE_SETTINGS.SESSION_ORIGIN) {
    params[DAVE_SETTINGS.SESSION_ORIGIN] = window.location.href.split(/[?#]/)[0];
  }

  DAVE_SETTINGS.execute_custom_callback("before_create_session", [params]);

  if ((typeof crypto === "undefined" ? "undefined" : _typeof(crypto)) !== undefined && !params[DAVE_SETTINGS.SESSION_ID]) {
    params[DAVE_SETTINGS.SESSION_ID] = crypto.randomUUID();
  }

  function on_success(data) {
    DAVE_SETTINGS.setCookie("dave_session_id", data[DAVE_SETTINGS.SESSION_ID]);
    DAVE_SETTINGS.execute_custom_callback("after_create_session", [data, params]);

    if (callbackFunc && _typeof(callbackFunc)) {
      callbackFunc(data);
    }
  }

  if (params[DAVE_SETTINGS.SESSION_ID]) {
    params['_async'] = true;
    on_success(params);
  }

  DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.SESSION_MODEL, "POST", JSON.stringify(params), function (data) {
    if (!params['_async']) {
      on_success(data);
    }
  }, errorFunc);
  DAVE_SETTINGS.create_event("created", {
    'event_type': 'auto'
  });
}; //
//
//

/* API of the Data response for chat
{
    "name": "sr_greeting",
    "title": "Sr Greeting",
    "whiteboard": null,
    "customer_state": "cs_hi",
    "placeholder_aliases": {},
    "state_options": {     // These are the quick access buttons
        "cs_xpress_cash_loan_repayment": "Method of repayment Personal Loan",
        "cs_car_loan_minimum_salary": "Car Loan minimum salaried individual",
        "cs_xpress_home_loan_tenure": "Home Loan tenure",
        "cs_car_loan_eligibility": "Car Loan eligibility",
        "cs_term_deposit_schemes": "Term deposit schemes",
        "cs_xpress_msme_loan_security": "Security for MSME Loan",
        "cs_xpress_home_loan_interest_rate": "Home Loan interest rate",
        "cs_personal_loan_features": "Personal Loan features"
    },
    "to_state_function": {
        "function": "_function_find_customer_state"
    },
    "placeholder": "Hello, hope you're having a good day!",     // This will go into the speech bubble, NOTE: This could be HTML, so it should be HTML safe
    "whiteboard_title": "Sr Greeting",
    "wait": 10000,    //// Time in millisecs to wait for the followup
    "data": {
         "_follow_ups": [  // nudges
            "nu_follow_up_state_1",
            "nu_follow_up_state_2"
         ],
         "response_type": "image",   // Other options are "video", "options", "thumbnails", "url", "form", "template", "to_timestamp",
         "template": <name of template (default is none)>
         "title"; <title to give the image, video or url or template>
         "sub_title"; <sub_title to give to the template, image or video>
         "description": "this description can be used in the template",
         "image": <url to the image>, //optional
         "video": <url to the video>,
         "video_start_timestamp": HH:MM:SS
         "video_stop_timestamp": HH:MM:SS
         "reply_to_id": <id of the response to which we want to provide the data>,
         "thumbnails": [
          {
             "image": <url to image>,
             "url": <url to redirect to if clicked>,
             "title": <title of the image>,
             "sub_title": <sub title of the image>,
             "description": <description of the image>,
             "target": <open in same window or another window (self or _blank)"
          }, ....
         ],
         "slideshow": [
          {
             "image": <url to image>,
             "title": <title of the image>,
             "sub_title": <sub title of the image>,
             "description": <description of the image>,
             "url": <url to redirect to if clicked>,
             "target": <open in same window or another window (self or _blank)"
          }, ....
         ],
         "url": <url to redirect to in case of clicking on the image or video>,
         "target": <target of the url, default "_blank" other option is self>,
         "form": [
             {
                "name": <name of field to send in json>,
                "title": <title of field, defaults to name>,
                "placeholder": <placeholder of field>,
                "ui_element": <text, textarea, number, switch, datetime-local, mobile_number, email, select, multiselect, tags, multiselect-tags>
                "error": <error message if required, can be null or not available>,
                "min": <min value in case of ui element to be number or date>,
                "max": <min value in case of ui element to be number or date>,
                "step": <resolution in seconds in case of datetime>,
                "required": true/ or false,
             }, ....
         ],    // The data collected from the form will be sent as json string in customer_response
         "customer_response": <custom title for the submit button>
         "options": {
             <customer_state>: <customer_response>,
             <customer_state>: <customer_response>
         },
         "options": [
            <customer_response option>,
            <customer_response option>
         ],
         "options": [
          {"customer_state": <customer_response>}
          {"customer_state": <customer_response>}
          {"customer_state": <customer_response>}
         ],
         "customer_state": <customer state to send in case there are options to select from>,
    },
    "options": null,
    "engagement_id": "YXNsa2RmamFsc2tqZGZkZXBsb3ltZW50IGtpb3NrIGthbm5hZGE_",
    "response_id": <response_id>
}*/
//
// params will contain one or both of
// "customer_state", and/or "customer_response"
// If user clicks on the Quick Access buttons, then both will be sent
// If there are options, then the selected option will be sent along with the customer_state sent in data
// If there is any text input then only customer_response will be sent.


DAVE_SETTINGS.on_having_response = function on_having_response(data, params, callbackFunc, unknownFunc, skip_history) {
  DAVE_SETTINGS.setCookie("dave_latest_response_id", data.response_id || DAVE_SETTINGS.generate_random_string(8));
  DAVE_SETTINGS.setCookie("dave_system_response", data.name);
  console.debug("Response Id: " + data.response_id);

  if (data.console) {
    var _iterator = _createForOfIteratorHelper(data.console.split('\n')),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var k = _step.value;

        if (k.indexOf('DEBUG') >= 0) {
          console.debug(k);
        } else if (k.indexOf('WARN') >= 0) {
          console.warn(k);
        } else if (k.indexOf('ERROR') >= 0) {
          console.error(k);
        } else {
          console.log(k);
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  }

  if (DAVE_SCENE && DAVE_SCENE.play && DAVE_SCENE.loaded && DAVE_SCENE.playable && DAVE_SCENE.opened && data.response_channels && data.response_channels.voice && data.response_channels.frames && data.response_channels.shapes) {
    DAVE_SCENE.snd = DAVE_SCENE.play(data.response_channels);
  }

  if (!skip_history) {
    var history = DAVE_SETTINGS.getCookie("dave_history") || {
      'history': []
    };
    var customer_state = params['customer_state'] || data['customer_state'] || "__init__";

    if (customer_state != '__init__' && !data.hide_in_customer_history) {
      history["history"].push({
        "direction": "user",
        "customer_response": params["customer_response"] || DAVE_SETTINGS.toTitleCase(params["customer_state"] || ''),
        "customer_state": customer_state,
        "user_id": DAVE_SETTINGS.getCookie("dave_user_id"),
        "user_model": DAVE_SETTINGS.USER_MODEL,
        "timestamp": data['start_timestamp'] || new Date(),
        "response_id": data["response_id"] || DAVE_SETTINGS.generate_random_string(8)
      });
    }

    if (data.show_in_history) {
      history["history"].push({
        "direction": "system",
        "name": data["name"],
        "placeholder": data['placeholder'],
        "data": data["data"] || {},
        "title": data["title"],
        "options": data["options"],
        "state_options": data["state_options"],
        "whiteboard": data["whiteboard"],
        "placeholder_aliases": data["placeholder_aliases"],
        "timestamp": data['timestamp'],
        "response_id": data["response_id"]
      });
    }

    DAVE_SETTINGS.setCookie("dave_history", history);
  }

  if (data.customer_state == 'unknown_customer_state' && unknownFunc && typeof unknownFunc == 'function') {
    unknownFunc(data);
    return;
  }

  if (callbackFunc && typeof callbackFunc == 'function') {
    callbackFunc(data);
  }
};

DAVE_SETTINGS.chat = function chat(params, callbackFunc, errorFunc, unknownFunc, discard_local, repeats) {
  repeats = repeats || 0;
  DAVE_SETTINGS.execute_custom_callback('on_chat_start', [params.customer_state || params.customer_response || "sent chat message", params]);

  if (DAVE_SETTINGS.DEFAULT_SYSTEM_RESPONSE && !params.customer_state && !params.customer_response && !discard_local) {
    DAVE_SETTINGS.DEFAULT_SYSTEM_RESPONSE.response_id = DAVE_SETTINGS.generate_random_string(8);
    DAVE_SETTINGS.on_having_response(DAVE_SETTINGS.DEFAULT_SYSTEM_RESPONSE, params, callbackFunc, unknownFunc, true);
    DAVE_SETTINGS.chat(params, null, null, null, true);
    return;
  }

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.chat, [params, callbackFunc, errorFunc, discard_local, repeats || 0], "dave_user_id")) {
    return;
  }

  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  var conversation_id = params['conversation_id'] || DAVE_SETTINGS.CONVERSATION_ID || DAVE_SETTINGS.getCookie('dave_conversation_id') || DAVE_SETTINGS.getCookie('conversation_id');
  delete params['conversation_id'];
  var engagement_id = params['engagement_id'] || DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id');

  if (engagement_id) {
    params["engagement_id"] = engagement_id;
  }

  var system_response = params['system_response'] || DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response');

  if (system_response && (params['customer_state'] || params['customer_response'])) {
    params["system_response"] = system_response;
  } else {
    delete params["system_response"];
  }

  if (DAVE_SETTINGS.VOICE_ID && !params['voice_id']) {
    params["voice_id"] = DAVE_SETTINGS.VOICE_ID;
  }

  if (DAVE_SETTINGS.LANGUAGE && !params['language']) {
    params["language"] = DAVE_SETTINGS.LANGUAGE;
  }

  if (DAVE_SETTINGS.AVATAR_ID || params['avatar_id']) {
    params["synthesize_now"] = true;
    params["csv_response"] = true;
  }

  params["origin"] = window.location.href;
  DAVE_SETTINGS.create_event("queried");

  if (params.customer_state || params.customer_response) {
    DAVE_SETTINGS.did_interact = true;
  }

  return DAVE_SETTINGS.ajaxRequestWithData("/conversation/" + conversation_id + "/" + user_id, "POST", JSON.stringify(params), function (data) {
    DAVE_SETTINGS.setCookie("dave_engagement_id", data.engagement_id);
    DAVE_SETTINGS.on_having_response(data, params, callbackFunc, unknownFunc);
    DAVE_SETTINGS.execute_custom_callback('on_response', [data.name || "received chat response", data.customer_state, data, params]);
  }, errorFunc);
};

DAVE_SETTINGS.set_conversation = function set_conversation(conversation_id, callbackFunc, errorFuc) {
  if (conversation_id == DAVE_SETTINGS.CONVERSATION_ID) {
    return;
  }

  DAVE_SETTINGS.CONVERSATION_ID = conversation_id;
  DAVE_SETTINGS.setCookie("dave_conversation_id", conversation_id);
  DAVE_SETTINGS.clearCookie('dave_keywords');
  DAVE_SETTINGS.clearCookie('dave_keyword_time');
  DAVE_SETTINGS.clearCookie('dave_engagement_id');
  DAVE_SETTINGS.chat({}, callbackFunc, errorFunc);
}; // get history function
// e.g. response data
//
// Check direction attribute to check if user or system
// Use placeholder if system and customer_response if user
//{
//    "history": [
//        {
//            "data": {},
//            "direction": "system",
//            "options": null,
//            "placeholder": "sr_init",
//            "placeholder_aliases": {},
//            "timestamp": "Sat, 09 Jan 2021 16:10:55 GMT",
//            "title": "Sr Agriculturists",
//            "whiteboard": null
//        },
//        {
//            "customer_response": "Response 1",
//            "customer_state": {
//                "unknown_customer_state": []
//            },
//            "direction": "user",
//            "timestamp": "Sat, 09 Jan 2021 16:11:57 GMT",
//            "user_id": "ananth+kabank@i2ce.in",
//            "user_model": "admin"
//        },
//        {
//            "data": {},
//            "direction": "system",
//            "options": null,
//            "placeholder": "I'm not sure what you mean. Can you rephrase that?",
//            "placeholder_aliases": {},
//            "timestamp": "Sat, 09 Jan 2021 16:11:57 GMT",
//            "title": "Sr Agriculturists",
//            "whiteboard": null
//        },
//        {
//            "customer_response": "Repsonse 2",
//            "customer_state": {
//                "unknown_customer_state": []
//            },
//            "direction": "user",
//            "timestamp": "Sat, 09 Jan 2021 16:13:40 GMT",
//            "user_id": "ananth+kabank@i2ce.in",
//            "user_model": "admin"
//        },
//        ....
//    ]
//}


DAVE_SETTINGS.history = function history(callbackFunc, errorFunc, reconcileFunction, discard_local, repeats) {
  repeats = repeats || 0;

  if (DAVE_SETTINGS.getCookie("dave_history") && !discard_local) {
    if (callbackFunc && typeof callbackFunc == "function") {
      callbackFunc(DAVE_SETTINGS.getCookie("dave_history"));
    }

    return DAVE_SETTINGS.history(null, null, reconcileFunction || callbackFunc, true);
  }

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.history, [callbackFunc, errorFunc, reconcileFunction, discard_local, repeats || 0], "dave_user_id")) {
    return;
  }

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.history, [callbackFunc, errorFunc, reconcileFunction, discard_local, repeats || 0], "dave_engagement_id")) {
    return;
  }

  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  var engagement_id = DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id');
  DAVE_SETTINGS.ajaxRequest("/conversation-history/" + DAVE_SETTINGS.CONVERSATION_ID + "/" + user_id + "/" + engagement_id, "GET", function (data) {
    var history = DAVE_SETTINGS.getCookie("dave_history") || {
      "history": []
    };
    nh = data['history'].filter(function (h) {
      return !history['history'].some(function (i) {
        if (h.response_id == i.response_id) {
          return true;
        } else {
          return false;
        }
      });
    });
    DAVE_SETTINGS.setCookie("dave_history", data);
    callbackFunc = callbackFunc || reconcileFunction;

    if (callbackFunc && typeof callbackFunc == "function") {
      callbackFunc({
        'history': nh
      });
    }
  }, errorFunc);
}; // DAVE_SETTINGS.feedback({"usefulness_rating": <rating>, "accuracy_rating": <rating>, "feedback": <feedback>}, null, function(){ console.log("Callback") }) to give accuracy rating for entire conversation
// DAVE_SETTINGS.feedback({"response_rating": <rating (true or false)>}, <response_id>, function(){ console.log("Callback") } ) to give rating for specific response
// DAVE_SETTINGS.feedback({"accuracy_rating": <rating (true or false)>}, <response_id>, function(){ console.log("Callback") } ) to give rating for recognition result


DAVE_SETTINGS.feedback = function send_feedback(rating, response_id, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.feedback, [rating, response_id, callbackFunc, errorFunc, repeats || 0], "dave_engagement_id")) {
    return;
  }

  var engagement_id = DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id');

  for (var k in rating) {
    if (typeof rating[k] == "boolean") {
      rating[k] = rating[k] ? 5 : 1;
    }
  }

  DAVE_SETTINGS.ajaxRequestWithData("/conversation-feedback/dave/" + engagement_id + (response_id ? "/" + response_id : ""), "PATCH", JSON.stringify(rating), function (data) {
    DAVE_SETTINGS.execute_custom_callback("after_submit_feedback", [rating, response_id]);

    if (callbackFunc && typeof callbackFunc == "function") {
      callbackFunc(data);
    }
  }, errorFunc);
}; // DAVE_SETTINGS.predict(<partial keywords>, function(response) { console.log(response)})
// response of type: [{sentence match: customer_state}, {sentence_match: customer_state}] 


DAVE_SETTINGS.predict = function predict(typed, callbackFunc, errorFunc, conversation_id, repeats) {
  typed = typed || "";
  repeats = repeats || 0;

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.predict, [typed, callbackFunc, errorFunc, conversation_id, repeats || 0], "dave_user_id")) {
    return;
  }

  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  conversation_id = conversation_id || DAVE_SETTINGS.CONVERSATION_ID || DAVE_SETTINGS.getCookie('dave_conversation_id') || DAVE_SETTINGS.getCookie('conversation_id');

  function do_set(data) {
    DAVE_SETTINGS.setCookie("dave_keywords", data);
    DAVE_SETTINGS.setCookie("dave_keyword_time", djQ.now());
  }

  typed = typed.split(" ").filter(function (el) {
    return el.length >= 3;
  });

  function do_keywords(data, noset) {
    if (!noset) {
      do_set(data);
    }

    response = [];
    resp = {};

    if (typed.length <= 0) {
      if (callbackFunc) {
        callbackFunc(response);
      }

      return response;
    }

    for (var k in data.keywords) {
      var d = data.keywords[k];

      if (!d[0]) {
        continue;
      }

      var d0 = d[0].split(" ");

      for (var v = 0; v < d0.length; v++) {
        var d1 = d0[v];

        for (var tv = 0; tv < typed.length; tv++) {
          var t = typed[tv];

          if (typeof d1 == 'string' && d1.toLowerCase() == t.toLowerCase()) {
            var r = {};
            r[d[1]] = d[2];

            if (!resp[d[2]]) {
              response.push(r);
              resp[d[2]] = d[1];
            }

            break;
          }
        }
      }
    }

    if (callbackFunc) {
      callbackFunc(response);
    }

    return response;
  }

  var keywords = DAVE_SETTINGS.getCookie("dave_keywords");
  var ret = do_keywords(keywords, true);

  if (DAVE_SETTINGS.getCookie("dave_keyword_time") > djQ.now() - 5 * 60 * 60 * 1000) {
    return ret;
  }

  DAVE_SETTINGS.ajaxRequest("/conversation-keywords/" + DAVE_SETTINGS.CONVERSATION_ID, "GET", do_set, errorFunc);
  return ret;
}; // Below this are all internal functions


DAVE_SETTINGS.iupdate_user = function iupdate_user(params, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.iupdate_user, [params, callbackFunc, errorFunc, repeats || 0])) {
    return;
  } // Used to update session duration and session number


  var user_id = DAVE_SETTINGS.getCookie('dave_user_id') || DAVE_SETTINGS.getCookie('user_id');
  params = params || {};
  params['_async'] = true;
  DAVE_SETTINGS.ajaxRequestWithData("/iupdate/" + DAVE_SETTINGS.USER_MODEL + "/" + user_id, "PATCH", JSON.stringify(params), callbackFunc, errorFunc);
};

DAVE_SETTINGS.iupdate_session = function iupdate_session(params, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.iupdate_session, [params, callbackFunc, errorFunc, repeats || 0])) {
    return;
  } // Used to update session duration and session number


  var session_id = DAVE_SETTINGS.getCookie('dave_session_id') || DAVE_SETTINGS.getCookie('session_id');
  params = params || {};
  params['_async'] = true;
  DAVE_SETTINGS.ajaxRequestWithData("/iupdate/" + DAVE_SETTINGS.SESSION_MODEL + "/" + session_id, "PATCH", JSON.stringify(params), callbackFunc, errorFunc);
};

DAVE_SETTINGS.create_interaction = function create_interaction(params, callbackFunc, errorFunc, repeats) {
  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.create_interaction, [params, callbackFunc, errorFunc, repeats || 0])) {
    return;
  } // Used to update session duration and session number


  var session_id = DAVE_SETTINGS.getCookie('dave_session_id') || DAVE_SETTINGS.getCookie('session_id');
  var user_id = DAVE_SETTINGS.getCookie("dave_user_id") || DAVE_SETTINGS.getCookie("user_id");
  params = params || {};
  params['_async'] = true;
  params[DAVE_SETTINGS.INTERACTION_SESSION_ID] = session_id;
  params[DAVE_SETTINGS.INTERACTION_USER_ID] = user_id;
  params["conversation_id"] = DAVE_SETTINGS.CONVERSATION_ID;

  if (DAVE_SETTINGS.INTERACTION_ORIGIN) {
    params[DAVE_SETTINGS.INTERACTION_ORIGIN] = window.location.href;
  }

  for (var k in DAVE_SETTINGS.DEFAULT_INTERACTION_DATA) {
    params[k] = DAVE_SETTINGS.DEFAULT_INTERACTION_DATA[k];
  }

  DAVE_SETTINGS.execute_custom_callback("before_create_interaction", [params]);
  DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.INTERACTION_MODEL, "POST", JSON.stringify(params), function (data) {
    DAVE_SETTINGS.execute_custom_callback("after_create_interaction", [data, params]);

    if (callbackFunc && _typeof(callbackFunc)) {
      callbackFunc(data);
    }
  }, errorFunc);
};

DAVE_SETTINGS.event_callbacks = {};

DAVE_SETTINGS.create_event = function create_event(description, params, callbackFunc, errorFunc, timestamp, repeats) {
  timestamp = timestamp || Math.floor(new Date().getTime() / 1000);

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.create_event, [description, params, callbackFunc, errorFunc, timestamp, repeats || 0], 'dave_session_id')) {
    return;
  }

  params = params || {};

  if (['opened', 'resumed', 'closed', 'minimized', 'queried', 'created'].indexOf(description) >= 0) {
    params[DAVE_SETTINGS.INTERACTION_STAGE_ATTR] = description;
    DAVE_SETTINGS.create_interaction(params);
  }

  if (DAVE_SETTINGS.cancel_events && DAVE_SETTINGS.cancel_events[description]) {
    for (var k in DAVE_SETTINGS.cancel_events[description]) {
      console.log("Cancelling timer for event " + description);
      var t = DAVE_SETTINGS.cancel_events[description][k];

      if (t) {
        clearTimeout(DAVE_SETTINGS.event_timers[t]);
        delete DAVE_SETTINGS.event_timers[t];
      }
    }

    DAVE_SETTINGS.cancel_events[description] = [];
  }

  if (!DAVE_SETTINGS.EVENT_MODEL || params["_skip_post"]) {
    return;
  }

  var session_id = DAVE_SETTINGS.getCookie('dave_session_id') || DAVE_SETTINGS.getCookie('session_id');
  var user_id = DAVE_SETTINGS.getCookie("dave_user_id") || DAVE_SETTINGS.getCookie("user_id");
  params['_async'] = true;
  params[DAVE_SETTINGS.EVENT_DESCRIPTION] = description;
  params[DAVE_SETTINGS.EVENT_SESSION_ID] = session_id;
  params[DAVE_SETTINGS.EVENT_USER_ID] = user_id;
  params["conversation_id"] = DAVE_SETTINGS.CONVERSATION_ID;
  params[DAVE_SETTINGS.EVENT_WAIT_TIME] = timestamp - DAVE_SETTINGS.EVENT_TIMER;
  DAVE_SETTINGS.EVENT_TIMER = timestamp;

  if (DAVE_SETTINGS.EVENT_ORIGIN) {
    params[DAVE_SETTINGS.EVENT_ORIGIN] = window.location.href;
  }

  var ed = DAVE_SETTINGS.DEFAULT_EVENT_DATA || DAVE_SETTINGS.DEFAULT_INTERACTION_DATA;

  for (var _k in ed) {
    params[_k] = ed[_k];
  }

  DAVE_SETTINGS.ajaxRequestWithData("/object/" + DAVE_SETTINGS.EVENT_MODEL, "POST", JSON.stringify(params), function (data) {
    if (DAVE_SETTINGS.event_callbacks[description]) {
      DAVE_SETTINGS.event_callbacks[description] = DAVE_SETTINGS.makeList(DAVE_SETTINGS.event_callbacks[description], []);

      var _iterator2 = _createForOfIteratorHelper(DAVE_SETTINGS.event_callbacks[description]),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var _k2 = _step2.value;

          _k2(description, params.event_type);
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
    }

    if (DAVE_SETTINGS.event_callbacks['__ALL__']) {
      DAVE_SETTINGS.event_callbacks['__ALL__'] = DAVE_SETTINGS.makeList(DAVE_SETTINGS.event_callbacks['__ALL__'], []);

      var _iterator3 = _createForOfIteratorHelper(DAVE_SETTINGS.event_callbacks['__ALL__']),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var _k3 = _step3.value;

          _k3(description, params.event_type);
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }
    }

    if (callbackFunc && typeof callbackFunc == "function") {
      callbackFunc(data);
    }
  }, errorFunc);
};

DAVE_SETTINGS.register_event = function register_event(identifier, description, event_type, params, beforeEventCallback, afterEventCallback) {
  event_type = event_type || 'click';
  djQ(identifier).on(event_type, function () {
    if (beforeEventCallback && typeof beforeEventCallback == 'function') {
      var d = beforeEventCallback(this, description, params, event_type);

      if (d) {
        description = d;
      }
    }

    params['event_type'] = event_type;
    DAVE_SETTINGS.create_event(description, params, afterEventCallback);
  });
};

DAVE_SETTINGS.play_again = function () {
  if (DAVE_SCENE.snd) {
    DAVE_SCENE.playAgain();
  }
};

DAVE_SETTINGS.execute_custom_callback = function execute_custom_callback(cb, args, base) {
  args = args || [];
  base = base || DAVE_SETTINGS;
  console.debug("Executing custom callback " + cb);
  base[cb] = DAVE_SETTINGS.makeList(base[cb], []);

  var _iterator4 = _createForOfIteratorHelper(base[cb]),
      _step4;

  try {
    for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
      var k = _step4.value;

      if (k && typeof k == 'function') {
        try {
          var r = k.apply({}, args);

          if (r === false) {
            return r;
          }
        } catch (err) {
          console.error(err);
        }
      }
    }
  } catch (err) {
    _iterator4.e(err);
  } finally {
    _iterator4.f();
  }

  return true;
};

DAVE_SETTINGS.register_custom_callback = function register_custom_callback(cb, f, base) {
  base = base || DAVE_SETTINGS;
  base[cb] = DAVE_SETTINGS.makeList(base[cb], []);
  base[cb].push(f);
};

DAVE_SETTINGS.on_maximize = function () {
  DAVE_SCENE.opened = true;

  if (DAVE_SCENE.opened_once) {
    DAVE_SETTINGS.create_event('resumed');
  } else {
    DAVE_SETTINGS.create_event('opened');
  }

  DAVE_SETTINGS.opened_time = new Date();
  DAVE_SCENE.unmute();
  DAVE_SCENE.opened_once = true;
  return DAVE_SETTINGS.execute_custom_callback('on_maximize_custom');
};

DAVE_SETTINGS.on_close = function () {
  DAVE_SCENE.opened = false;
  DAVE_SETTINGS.create_event('closed');

  if (DAVE_SETTINGS.opened_time instanceof Date) {
    DAVE_SETTINGS.iupdate_session({
      'session_duration': (new Date().getTime() - DAVE_SETTINGS.opened_time.getTime()) / 1000
    }, function () {
      DAVE_SETTINGS.opened_time = null;
    });
  }

  DAVE_SCENE.stop();
  return DAVE_SETTINGS.execute_custom_callback('on_close_custom');
};

DAVE_SETTINGS.register_event_callback = function (func, event_type) {
  event_type = event_type || '__ALL__';

  if (typeof func == "function") {
    if (!DAVE_SETTINGS.event_callbacks[event_type]) {
      DAVE_SETTINGS.event_callbacks[event_type] = [];
    }

    DAVE_SETTINGS.event_callbacks[event_type].push(func);
  }
};

DAVE_SETTINGS.on_minimize = function () {
  if (DAVE_SCENE.is_mobile) {
    DAVE_SETTINGS.on_close();
  }

  DAVE_SETTINGS.create_event('minimized');
  DAVE_SCENE.mute();
  return DAVE_SETTINGS.execute_custom_callback('on_minimize_custom');
};

DAVE_SETTINGS.getCookie = function (key, def) {
  var result = window.localStorage.getItem(key);
  return DAVE_SETTINGS.get_value(result, def);
};

DAVE_SETTINGS.get_value = function (value, def) {
  if (value === null || value === undefined) {
    return def === 'undefined' ? null : def;
  }

  try {
    return JSON.parse(value);
  } catch (err) {
    return value;
  }

  return value;
};

DAVE_SETTINGS.setCookie = function (key, value, hoursExpire) {
  if (hoursExpire === undefined) {
    hoursExpire = 24;
  }

  if (value === undefined) {
    return;
  }

  if (_typeof(value) == "object") {
    value = JSON.stringify(value);
  }

  if (hoursExpire < 0 || value === null) {
    window.localStorage.removeItem(key);
  } else {
    window.localStorage.setItem(key, value);
  }
};

DAVE_SETTINGS.clearCookie = function (key) {
  var r = DAVE_SETTINGS.getCookie(key, null);
  DAVE_SETTINGS.setCookie(key, null, -1);
  return r;
};

DAVE_SETTINGS.clearCookies = function () {
  // This is for older versions so that we can do migration, now it is not required
  //let cks = ["conversation_id", "authentication", "session_id", "keywords", "keyword_time", "user_id", "started_signup", "engagement_id", "history"];
  //for (let key in cks) {
  //    DAVE_SETTINGS.clearCookie(cks[key]);
  //}
  for (var key in window.localStorage) {
    if (key.startsWith('dave_')) {
      DAVE_SETTINGS.clearCookie(key);
    }
  }
};

DAVE_SETTINGS.Trim = function (strValue) {
  if (typeof strValue != 'string') {
    return strValue;
  }

  return strValue.replace(/^\s+|\s+$/g, '');
};

DAVE_SETTINGS.toJqId = function (strValue) {
  if (typeof strValue != 'string') {
    return strValue;
  }

  return strValue.replace(/\./g, '\\.').replace(/\//g, '\\/');
};

DAVE_SETTINGS.toTitleCase = function (str) {
  if (typeof str != 'string') {
    return str;
  }

  return str.replace(/_/g, ' ').replace(/(?:^|\s)\w/g, function (match) {
    return match.toUpperCase();
  });
};

DAVE_SETTINGS.makeList = function (inp, def) {
  if (inp == undefined || inp == null) {
    return def;
  }

  if (Array.isArray(inp)) {
    return inp;
  }

  return [inp];
};

DAVE_SETTINGS.makeSingle = function (inp, def) {
  if (inp == undefined || inp == null) {
    return def;
  }

  if (Array.isArray(inp)) {
    if (inp.length == 0) {
      return def;
    }

    return inp[0];
  }

  return inp;
};

DAVE_SETTINGS.generate_random_string = function (string_length) {
  var random_string = '';
  var random_ascii;

  for (var i = 0; i < string_length; i++) {
    random_ascii = Math.floor(Math.random() * 25 + 97);
    random_string += String.fromCharCode(random_ascii);
  }

  return random_string;
};

DAVE_SETTINGS.ajaxRequestSync = function (URL, METHOD, callbackFunc, errorFunc, async, unauthorized, HEADERS) {
  HEADERS = HEADERS || DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication");

  if (!unauthorized) {
    unauthorized = function unauthorized() {
      DAVE_SETTINGS.clearCookie('dave_user_id');
      DAVE_SETTINGS.clearCookie('dave_authentication');
      DAVE_SETTINGS.signup({}, function () {
        DAVE_SETTINGS.ajaxRequestSync(URL, METHOD, callbackFunc, errorFunc, async, function () {
          console.error("Could not signup do DaveAI chatbot! Please contact customer support!");

          if (typeof DAVE_SETTINGS.signup_failed == 'function') {
            DAVE_SETTINGS.signup_failed();
          }
        }, HEADERS);
      });
    };
  }

  djQ.ajax({
    url: DAVE_SETTINGS.BASE_URL + URL,
    method: (METHOD || "GET").toUpperCase(),
    dataType: "json",
    contentType: "json",
    async: async || false,
    headers: HEADERS,
    statusCode: {
      401: unauthorized,
      404: unauthorized
    }
  }).done(function (data) {
    result = data;

    if (callbackFunc) {
      callbackFunc(data);
    }
  }).fail(function (err) {
    if (errorFunc) {
      errorFunc(err);
    }
  });
};

DAVE_SETTINGS.ajaxRequest = function (URL, METHOD, callbackFunc, errorFunc, unauthorized, HEADERS) {
  return DAVE_SETTINGS.ajaxRequestSync(URL, METHOD, callbackFunc, errorFunc, true, unauthorized, HEADERS);
};

DAVE_SETTINGS.ajaxRequestWithData = function (URL, METHOD, DATA, callbackFunc, errorFunc, unauthorized, HEADERS, retries) {
  HEADERS = HEADERS || DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication");
  METHOD = (METHOD || "GET").toUpperCase();
  retries = retries || 0;
  var defaultData = "";

  if (DATA) {
    defaultData = DATA;
  }

  if (["POST", "PATCH", "DELETE"].indexOf(METHOD) >= 0 && typeof defaultData != 'string') {
    defaultData = JSON.stringify(defaultData);
  }

  if (!unauthorized) {
    unauthorized = function unauthorized() {
      if (retries <= 5) {
        setTimeout(function () {
          DAVE_SETTINGS.clearCookie('dave_user_id');
          DAVE_SETTINGS.clearCookie('dave_authentication');
          DAVE_SETTINGS.signup({}, DAVE_SETTINGS.ajaxRequestWithData(URL, METHOD, DATA, callbackFunc, errorFunc, function () {
            console.error("Could not signup do DaveAI chatbot! Please contact customer support!");

            if (typeof DAVE_SETTINGS.signup_failed == 'function') {
              DAVE_SETTINGS.signup_failed();
            }
          }, HEADERS, retries + 1));
        }, (retries + 1) * (retries + 1) * 5000);
      } else {
        console.error("Failed to authorize DaveAI plugin after 5 attempts");
      }
    };
  }

  return djQ.ajax({
    url: DAVE_SETTINGS.BASE_URL + URL,
    method: (METHOD || "GET").toUpperCase(),
    dataType: "json",
    contentType: "application/json",
    headers: HEADERS,
    data: defaultData,
    statusCode: {
      401: unauthorized,
      404: unauthorized
    }
  }).done(function (data) {
    if (callbackFunc) {
      callbackFunc(data);
    }
  }).fail(function (err) {
    if (errorFunc) {
      errorFunc(err);
    }
  });
};

DAVE_SETTINGS.initialize_session = function (force, callbackFunc, repeats) {
  if (!force && DAVE_SETTINGS.custom_session && typeof DAVE_SETTINGS.custom_session == 'function') {
    DAVE_SETTINGS.custom_session(callbackFunc);
    return;
  }

  if (!DAVE_SETTINGS.signup_guard(DAVE_SETTINGS.initialize_session, [force, callbackFunc, repeats || 0])) {
    return;
  } // If Page is refreshed, then this should not happen. Which is the scenario if
  // DAVE_SETTINGS.setCookie("system_response", null);


  DAVE_SETTINGS.create_session({}, function (data) {
    DAVE_SETTINGS.setCookie("dave_last_login", data.updated);
    DAVE_SETTINGS.execute_custom_callback('on_initialize_session');

    if (callbackFunc && typeof callbackFunc == 'function') {
      callbackFunc(data);
    }
  });

  if (DAVE_SETTINGS.AVATAR_ID && DAVE_SCENE.loadPreScene) {
    DAVE_SETTINGS.ajaxRequest("/object/" + DAVE_SETTINGS.AVATAR_MODEL + "/" + DAVE_SETTINGS.AVATAR_ID, "GET", function (data) {
      if (data[DAVE_SETTINGS.GLB_ATTR || 'glb_url']) {
        DAVE_SCENE.loadPreScene("dave-canvas", data[DAVE_SETTINGS.GLB_ATTR || 'glb_url'], function () {
          DAVE_SCENE.loaded = true;
          DAVE_SETTINGS.execute_custom_callback('onload', [], DAVE_SCENE);
        });
        DAVE_SETTINGS.LANGUAGE = data.language;
        DAVE_SETTINGS.VOICE_ID = data.voice_id;
      }
    });
  }
};

DAVE_SETTINGS.get_url_params = function (qd) {
  qd = qd || {};
  if (location.search) location.search.substr(1).split("&").forEach(function (item) {
    var s = item.split("="),
        k = s[0],
        v = s[1] && decodeURIComponent(s[1]); //  null-coalescing / short-circuit
    //(k in qd) ? qd[k].push(v) : qd[k] = [v]

    (qd[k] = qd[k] || []).push(v); // null-coalescing / short-circuit
  });
  return qd;
};

DAVE_SETTINGS.pretty_print = function (data, field_name_map, field_type_map, skip_fields, empty, separator) {
  separator = separator || '<br>';
  skip_fields = skip_fields || [];
  empty = empty || "";
  field_name_map = field_name_map || {};
  field_type_map = field_type_map || {};

  if (data == null || data == undefined) {
    return empty;
  }

  if (typeof data == "string") {
    try {
      data = JSON.parse(data);
    } catch (err) {
      return data;
    }
  }

  if (typeof data == "number") {
    return data;
  }

  if (typeof field_name_map == "string") {
    field_name_map = data[field_name_map] || {};
  }

  if (typeof field_type_map == "string") {
    field_type_map = field_type_map || data[field_type_map] || {};
  }

  var output_string = [];

  if (Array.isArray(data)) {
    for (var k in data) {
      var d = data[k];
      d = DAVE_SETTINGS.pretty_print(d, field_name_map, field_type_map, skip_fields, empty, separator);

      if (d) {
        output_string.push(d);
      }
    }
  } else if (djQ.type(data) == "object") {
    if (data._name_map && !djQ.isEmptyObject(data._name_map)) {
      field_name_map = data._name_map;
    }

    if (data._type_map && !djQ.isEmptyObject(data._type_map)) {
      field_type_map = data._type_map;
    }

    for (var _k4 in data) {
      if (skip_fields.indexOf(_k4) >= 0 || _k4.startsWith('_')) {
        continue;
      }

      var v = data[_k4];

      if (Array.isArray(v)) {
        v = DAVE_SETTINGS.pretty_print(v, ", ", field_name_map, field_type_map, skip_fields, empty);
      }

      if (djQ.type(v) == "object") {
        if (!djQ.isEmptyObject(v)) {
          v = '{ ' + DAVE_SETTINGS.pretty_print(v, ", ", field_name_map, field_type_map, skip_fields, empty) + " }";
        } else {
          v = empty;
        }
      }

      if (field_type_map[_k4]) {
        if (field_type_map[_k4] == 'datetime' || field_type_map[_k4] == 'datetime-local') {
          v = DAVE_SETTINGS.print_timestamp(v, v, true);
        } else if (field_type_map[_k4] == "file") {
          v = '\<Uploaded File of type' + v.split('.').pop() + '\>';
        } // Put here other field type cases

      } else {
        var v1 = new Date(v);

        if (v1.toLocaleString() != 'Invalid Date') {
          v = DAVE_SETTINGS.print_timestamp(v1, v1.toLocaleString(), true);
        }
      }

      if (v) {
        output_string.push('<b>' + (field_name_map[_k4] || DAVE_SETTINGS.toTitleCase(_k4)) + "</b> : " + v);
      }
    }

    return output_string.join(separator);
  } else {
    output_string.push(data);
  }

  if (output_string.length == 0) {
    if (empty) {
      output_string.push(empty);
    } else {
      return empty;
    }
  }

  return output_string.join(separator);
};

DAVE_SETTINGS.unauthorized = function (f, args) {
  return function () {
    DAVE_SETTINGS.clearCookie('dave_user_id');
    DAVE_SETTINGS.clearCookie('dave_authentication');

    var fu = function fu() {
      console.error("Could not signup do DaveAI chatbot! Please contact customer support!");

      if (typeof DAVE_SETTINGS.signup_failed == 'function') {
        DAVE_SETTINGS.signup_failed();
      }
    };

    args.push(fu);
    DAVE_SETTINGS.signup({}, function () {
      f.apply({}, args);
    }, fu, true);
  };
};

DAVE_SETTINGS.upload_file = function upload_file(blob_data, file_name, callbackFunc, errorFunc, unauthorized) {
  var formData = new FormData();
  formData.append('file', blob_data, file_name);
  var headers = DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication");

  if (!unauthorized) {
    unauthorized = DAVE_SETTINGS.unauthorized(DAVE_SETTINGS.upload_file, [blob_data, file_name, callbackFunc, errorFunc]);
  }

  djQ.ajax({
    url: DAVE_SETTINGS.BASE_URL + "/upload_file?large_file=true",
    type: "POST",
    dataType: "json",
    contentType: false,
    processData: false,
    headers: {
      "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
      "X-I2CE-USER-ID": headers["X-I2CE-USER-ID"],
      "X-I2CE-API-KEY": headers["X-I2CE-API-KEY"]
    },
    data: formData,
    statusCode: {
      401: unauthorized
    }
  }).done(function (data) {
    if (callbackFunc) {
      callbackFunc(data);
    }
  }).fail(function (err) {
    if (errorFunc) {
      errorFunc(err);
    }
  });
};

DAVE_SETTINGS.clean_input = function clean_input(inp) {
  var doc = new DOMParser().parseFromString(inp, 'text/html');
  return doc.body.textContent || "";
};

DAVE_SETTINGS.pad_string = function pad_string(n, width, z) {
  width = width || 2;
  z = z || '0';
  return (String(z).repeat(width) + String(n)).slice(String(n).length);
};

DAVE_SETTINGS.print_timestamp = function (timestamp, invalid_date, full_form) {
  invalid_date = invalid_date || 'N/A';
  td = new Date();
  timestamp = timestamp || td;
  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  if (typeof timestamp == "string") {
    if (timestamp.indexOf("/") >= 0) {
      // This is India date format
      var _timestamp$split = timestamp.split(/\D+/),
          _timestamp$split2 = _slicedToArray(_timestamp$split, 6),
          d = _timestamp$split2[0],
          m = _timestamp$split2[1],
          y = _timestamp$split2[2],
          h = _timestamp$split2[3],
          M = _timestamp$split2[4],
          s = _timestamp$split2[5];

      if (!s) {
        s = '00';
      }

      if (!M) {
        M = td.getMinutes();
      }

      if (!h) {
        h = td.getHours();
      }

      timestamp = new Date(y, m - 1, d, h, M, s);
    }
  }

  if (djQ.type(timestamp) != 'date') {
    timestamp = new Date(timestamp);
  }

  if (timestamp.toString() == 'Invalid Date') {
    return invalid_date;
  }

  if (full_form || timestamp.getYear() != td.getYear()) {
    return months[timestamp.getMonth()] + " " + DAVE_SETTINGS.pad_string(timestamp.getDate()) + ', ' + timestamp.getFullYear() + ', ' + DAVE_SETTINGS.pad_string(timestamp.getHours()) + ':' + DAVE_SETTINGS.pad_string(timestamp.getMinutes());
  }

  if (timestamp.getMonth() != td.getMonth() || timestamp.getDate() != td.getDate()) {
    return months[timestamp.getMonth()] + " " + DAVE_SETTINGS.pad_string(timestamp.getDate()) + ', ' + DAVE_SETTINGS.pad_string(timestamp.getHours()) + ':' + DAVE_SETTINGS.pad_string(timestamp.getMinutes());
  }

  return DAVE_SETTINGS.pad_string(timestamp.getHours()) + ':' + DAVE_SETTINGS.pad_string(timestamp.getMinutes());
};

DAVE_SETTINGS.signup_guard = function (func, args, cookie_name, redo_function) {
  cookie_name = cookie_name || "dave_authentication";

  if (!DAVE_SETTINGS.getCookie(cookie_name)) {
    console.warn(cookie_name + " headers are not set, but trying to run function " + func.name);
    var repeats = args.slice(-1);
    var args1 = args;

    if (repeats < 30) {
      if ((repeats == 10 || repeats == 20) && (DAVE_SETTINGS.getCookie('dave_started_signup') || DAVE_SETTINGS.getCookie('started_signup'))) {
        if (redo_function && typeof redo_function == 'function') {
          redo_function();
        } else {
          DAVE_SETTINGS.clearCookie('dave_started_signup');
          DAVE_SETTINGS.load_dave();
        }
      }

      setTimeout(function () {
        args1.push(args1.pop() + 1);
        func.apply({}, args1);
      }, 1000 * (repeats + 1));
    } else {
      console.error("Stopping trying to run function" + func.name + " after " + repeats + " attempts");

      if (typeof DAVE_SETTINGS.signup_failed == 'function') {
        DAVE_SETTINGS.signup_failed();
      }
    }

    return false;
  }

  return true;
};

DAVE_SETTINGS.load_settings = function load_settings(identifier, namespace) {
  if (djQ(identifier).length) {
    var ds = djQ(identifier);

    for (var s in namespace) {
      if (s.toUpperCase() != s) {
        continue;
      }

      var v = ds.attr("data-" + s.toLowerCase().replace(/_/g, '-')) || ds.attr("data-" + s.toLowerCase()) || ds.attr("data-" + s);

      if (v) {
        console.debug("Entered the condition " + s);
        var g = DAVE_SETTINGS.get_value(v);

        if (_typeof(g === 'object') && g !== null && _typeof(namespace[s]) === 'object' && namespace[s] !== null) {
          for (var k in g) {
            namespace[s][k] = g[k];
          }
        } else {
          namespace[s] = g;
        }
      } else {
        console.debug("Didn't get condition " + s);
      }
    }
  }
};

DAVE_SETTINGS.load_dave = function load_dave(callbackFunc) {
  DAVE_SETTINGS.EVENT_TIMER = Math.floor(new Date().getTime() / 1000);
  DAVE_SETTINGS.load_settings("#dave-settings", DAVE_SETTINGS);

  if (DAVE_SETTINGS.ENTERPRISE_ID && DAVE_SETTINGS.CONVERSATION_ID && DAVE_SETTINGS.SIGNUP_API_KEY) {
    console.debug("Got Enterprise ID, Conversation ID, SignUp API KEY");

    if (!(DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication")) || (DAVE_SETTINGS.getCookie("dave_conversation_id") || DAVE_SETTINGS.getCookie("conversation_id")) != DAVE_SETTINGS.CONVERSATION_ID) {
      DAVE_SETTINGS.clearCookies();
      DAVE_SETTINGS.signup({}, callbackFunc);
    } else if ((DAVE_SETTINGS.getCookie("dave_conversation_id") || DAVE_SETTINGS.getCookie("conversation_id")) != DAVE_SETTINGS.CONVERSATION_ID) {
      DAVE_SETTINGS.clearCookies();
      DAVE_SETTINGS.signup({}, callbackFunc);
    } else if ((DAVE_SETTINGS.getCookie("dave_authentication") || DAVE_SETTINGS.getCookie("authentication"))['X-I2CE-ENTERPRISE-ID'] != DAVE_SETTINGS.ENTERPRISE_ID) {
      DAVE_SETTINGS.clearCookies();
      DAVE_SETTINGS.signup({}, callbackFunc);
    } else {
      DAVE_SETTINGS.initialize_session(false, callbackFunc);
    }
  } else {
    console.warn("Did not get one of Enterprise ID, Conversation ID or SignUp API KEY");
  }

  djQ("#dave-settings").one("click", function () {
    DAVE_SCENE.audio_click();
  });

  if (DAVE_SETTINGS.SPEECH_SERVER) {
    var now_params = null;
    DAVE_SETTINGS.StreamingSpeech = {};
    DAVE_SETTINGS.streamer = new Streamer({
      wakeup_server: null,
      asr_server: DAVE_SETTINGS.SPEECH_SERVER,
      recognizer: DAVE_SETTINGS.SPEECH_RECOGNIZER,
      conversation_id: DAVE_SETTINGS.CONVERSATION_ID,
      enterprise_id: DAVE_SETTINGS.ENTERPRISE_ID,
      base_url: DAVE_SETTINGS.BASE_URL,
      auto_asr_detect_from_wakeup: false,
      vad: true,
      asr_only: true,
      audio_auto_stop: DAVE_SETTINGS.MAX_SPEECH_DURATION / 1000.0,
      asr_type: "full",
      //full-'flacking'||"chunks"-'asteam'||"direct"-'sending-flag',
      wakeup_type: "chunks",
      //chunks-'asteam'||"direct"-'sending-flag',
      video_stream_type: "chunks",
      //chunks-'asteam'||"direct"-'sending-flag'
      asr_additional_info: {},
      model_name: DAVE_SETTINGS.SPEECH_MODEL_NAME,
      event_callback: function event_callback(event, data) {
        if (event == 'wsconnect') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onSocketConnect == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onSocketConnect();
          }
        } else if (event == 'auto_stopped_recording') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording();
          }
        } else if (event == 'asr_intermediate_results') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable(data);
          }
        } else if (event == 'asr_results') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable(data);
          }
        } else if (event == 'conversation_response') {
          DAVE_SETTINGS.execute_custom_callback('on_response', [data.name || "received chat response", data.customer_state, data, now_params]);

          if (typeof DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable(data);
          }
        } else if (event == 'wsdisconnect') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect(data);
          }
        } else if (event == 'asr_error') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onError == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onError(data);
          }
        } else if (event == 'conv_error') {
          if (typeof DAVE_SETTINGS.StreamingSpeech.onError == 'function') {
            DAVE_SETTINGS.StreamingSpeech.onError(data);
          }
        }
      }
    });

    DAVE_SETTINGS.StreamingSpeech.onStartVoiceRecording = function (params) {
      params = params || {};
      params.engagement_id = DAVE_SETTINGS.getCookie("dave_engagement_id");
      params.customer_id = DAVE_SETTINGS.getCookie("dave_user_id");
      params.api_key = DAVE_SETTINGS.getCookie("dave_authentication")['X-I2CE-API-KEY'];
      params.system_response = DAVE_SETTINGS.getCookie("dave_system_response");
      DAVE_SETTINGS.execute_custom_callback("on_start_recording", [params]);
      DAVE_SETTINGS.execute_custom_callback("on_chat_start", [DAVE_SETTINGS.streamer.asr_additional_info]);
      DAVE_SETTINGS.streamer.startVoiceRecording(params);
      DAVE_SETTINGS.execute_custom_callback("after_start_recording", [params]);
      now_params = DAVE_SETTINGS.streamer.asr_additional_info;
    };

    DAVE_SETTINGS.StreamingSpeech.onStopVoiceRecording = function () {
      DAVE_SETTINGS.execute_custom_callback("on_stop_recording");
      DAVE_SETTINGS.streamer.stopVoiceRecording();
      DAVE_SETTINGS.execute_custom_callback("after_stop_recording");
    };

    DAVE_SETTINGS.execute_custom_callback('on_load_streamer');
  }
};

DAVE_SCENE.audio_click = function () {
  var aud = new Audio(DAVE_SETTINGS.asset_path + "assets/audio/empty.wav");
  aud.volume = 0.0;
  aud.play();
  DAVE_SCENE.playable = true;
}; // Pause temporaririly


DAVE_SCENE.pause = function () {
  if (DAVE_SCENE.snd) {
    DAVE_SCENE.snd.pause();
    DAVE_SCENE.stopMorphs();
    DAVE_SCENE.snd = null;
  }
}; // Pause permanently


DAVE_SCENE.stop = function () {
  if (DAVE_SCENE.snd) {
    DAVE_SCENE.snd.pause();
    DAVE_SCENE.pauseAnimsandMorphs();
    DAVE_SCENE.snd = null;
    DAVE_SCENE.queue = [];
  }
};

DAVE_SCENE.mute = function () {
  if (DAVE_SCENE.snd) {
    DAVE_SCENE.snd.volume = 0.0;
  }

  DAVE_SCENE.volume = 0.0;
};

DAVE_SCENE.unmute = function () {
  if (DAVE_SCENE.snd) {
    DAVE_SCENE.snd.volume = 1.0;
  }

  DAVE_SCENE.volume = 1.0;
};

djQ(document).one("click", function () {
  DAVE_SCENE.audio_click();
});
djQ(window).blur(function (e) {
  if (DAVE_SCENE.opened && DAVE_SETTINGS.opened_time instanceof Date) {
    DAVE_SETTINGS.iupdate_session({
      'session_duration': (new Date().getTime() - DAVE_SETTINGS.opened_time.getTime()) / 1000
    }, function () {
      DAVE_SETTINGS.opened_time = null;
    });
  }
});
djQ(window).focus(function (e) {
  if (DAVE_SCENE.opened && !DAVE_SETTINGS.opened_time) {
    DAVE_SETTINGS.opened_time = new Date();
  }
});
djQ(function () {
  DAVE_SETTINGS.load_dave();
});
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var DAVE_ASSET_PATH = "https://chatbot-plugin.iamdave.ai/";

if (document.currentScript && isFinite(document.currentScript.src.split('/').slice(-2, -1)[0])) {
  DAVE_ASSET_PATH = document.currentScript && document.currentScript.src.split('/').slice(0, -4).join('/') + '/';
} else {
  DAVE_ASSET_PATH = document.currentScript && document.currentScript.src.split('/').slice(0, -3).join('/') + '/' || "https://chatbot-plugin.iamdave.ai/";
}

function gm_authFailure() {
  document.getElementById("map").style.display = "none";
  console.log("MAP API KEY IS NOT WORKING ");
  handleMapError();
}

;

djQ.fn.ForcePhoneNumbersOnly = function () {
  return this.each(function () {
    djQ(this).keydown(function (e) {
      var key = e.charCode || e.keyCode || 0; // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
      // home, end, period, and numpad decimal

      return key == 8 || key == 9 || key == 13 || key == 46 || key == 110 || key == 190 || key >= 35 && key <= 40 || key >= 48 && key <= 57 || key >= 96 && key <= 105 || key == 61;
    });
  });
};

window.dave_load_chatbot = function () {
  DAVE_SETTINGS.execute_custom_callback("before_load_chatbot"); //All Dynamic Settings to change chatbot lookandfeel

  var dave_chatTitle = djQ('#dave-settings').attr('data-cbTitle') || djQ('#dave-settings').attr('data-dave-cbTitle') || "DAVE VIRTUAL ASSISTANT";
  var dave_bot_bubbleTitle = djQ('#dave-settings').attr('data-bot-bubbleTitle') || djQ('#dave-settings').attr('data-cbdTitle') || djQ('#dave-settings').attr('data-dave-cbTitle') || "Dave Virtual Assistant";
  var dave_user_bubbleTitle = djQ('#dave-settings').attr('data-user-bubbleTitle') || "You";
  var dave_optionTitle = djQ('#dave-settings').attr('data-optionTitle') || '';
  var data_feedback_redo_time = djQ("#dave-settings").attr("data-feedback-redo-time") || 3600000;
  var feedback_successMSG = djQ("#dave-settings").attr("data-feedback-successMsg") || "Thank you for your valuable feedback";
  var maxLockHeight = djQ("#dave-settings").attr("data-cb-minLockHeight") || 150;
  var dave_desktop_size = {
    dave_cb_height: djQ('#dave-settings').attr('data-cb-height-desktop') || '520px'
  };
  var dave_mobile_size = {
    dave_cb_height: djQ('#dave-settings').attr('data-cb-height-mobile') || '500px'
  }; //ALL IMAGE DATA-SETTINGS 

  var dave_bot_icon = djQ('#dave-settings').attr('data-botIcon') || djQ('#dave-settings').attr('data-dave-botIcon') || DAVE_ASSET_PATH + 'assets/img/dave-icon.png';
  var dave_user_icon = djQ('#dave-settings').attr('data-userIcon') || djQ('#dave-settings').attr('data-dave-userIcon') || DAVE_ASSET_PATH + 'assets/img/userchat-icon.png';
  var dave_chatbot_icon = djQ('#dave-settings').attr('data-cbIcon') || djQ('#dave-settings').attr('data-dave-cbIcon') || DAVE_ASSET_PATH + 'assets/img/dave-icon.png';
  var dave_title_icon = djQ('#dave-settings').attr('data-title-icon') || djQ('#dave-settings').attr('data-dave-title-icon') || dave_chatbot_icon;
  var cross_img = djQ("#dave-settings").attr("data-cross-img") || djQ("#dave-settings").attr("data-dave-cross-img") || DAVE_ASSET_PATH + 'assets/img/cross.png';
  var min_img = djQ("#dave-settings").attr("data-min-img") || djQ("#dave-settings").attr("data-dave-min-img") || DAVE_ASSET_PATH + 'assets/img/min.svg';
  var max_img = djQ("#dave-settings").attr("data-max-img") || djQ("#dave-settings").attr("data-dave-max-img") || DAVE_ASSET_PATH + 'assets/img/max.svg';
  var like_img = djQ("#dave-settings").attr("data-like-img") || DAVE_ASSET_PATH + 'assets/img/up.svg';
  var liked_img = djQ("#dave-settings").attr("data-liked-img") || DAVE_ASSET_PATH + 'assets/img/up-hover.svg';
  var dislike_img = djQ("#dave-settings").attr("data-dislike-img") || DAVE_ASSET_PATH + 'assets/img/down.svg';
  var disliked_img = djQ("#dave-settings").attr("data-disliked-img") || DAVE_ASSET_PATH + 'assets/img/down-hover.svg';
  var cb_sugg_next = djQ("#dave-settings").attr("data-cbSuggNext-img") || DAVE_ASSET_PATH + 'assets/img/next.png';
  var cb_sugg_prev = djQ("#dave-settings").attr("data-cbSuggPrev-img") || DAVE_ASSET_PATH + 'assets/img/pre.png';
  var typing_gif = djQ("#dave-settings").attr("data-typing-img") || DAVE_ASSET_PATH + 'assets/img/typing.gif';
  var mic_img = djQ("#dave-settings").attr("data-mic-img") || DAVE_ASSET_PATH + 'assets/img/mic.svg';
  var mic_hover_img = djQ("#dave-settings").attr("data-micHover-img") || DAVE_ASSET_PATH + 'assets/img/michover.svg';
  var send_img = djQ("#dave-settings").attr("data-send-img") || DAVE_ASSET_PATH + 'assets/img/send.png';
  var homeBtnImg = djQ("#dave-settings").attr("data-homeBtn-img");
  var WIA_RatingTitle = djQ("#dave-settings").attr("data-feedback-accurate-title") || djQ("#dave-settings").attr("data-dave-feedback-accurate-title") || "Was I accurate?";
  var WIH_RatingTitle = djQ("#dave-settings").attr("data-feedback-useful-title") || djQ("#dave-settings").attr("data-dave-feedback-useful-title") || "Was I helpful?";
  var writeFeedback = !(djQ("#dave-settings").attr("data-hide-feedback-text") === "true"); //All Dynamic Settings ENDS
  //global letiable declaration as flag and window width

  var originalScroll = djQ('body').css('overflow');
  console.log("Original Scroll is", originalScroll);
  var dave_window_width = djQ(window).width();
  var dave_window_height = djQ(window).height();
  console.log("Window height and width when page loads => " + dave_window_width + " " + dave_window_height);
  var dave_chat_heightchange = false;
  var pred_attach = false;
  var text_cleared = true;
  var chat_historyActivated = false;
  var map_enabled = undefined;
  var userPositon = {
    "lat": undefined,
    "lng": undefined
  };
  var dave_userchatfunc_exe = undefined;
  var dave_predication_send = undefined;
  var chat_loading = undefined;
  var dave_activityTimer = undefined;
  var nudge_activityInterval = undefined;
  var followupIndex = undefined;
  var followupList_array = new Array();
  var timerFlag = true;
  var close_count = 0;
  var chatbox_width = djQ(".dave-chatbox-cont").width() || "360px"; //GLOBAL VARIABLE VALUES SET AS PER SCREEN SIZING

  if (dave_window_width > 450) {
    //DESKTOP
    djQ(".dave-chatbox").css({
      "height": dave_desktop_size.dave_cb_height
    }); //chatbox_width = djQ('#dave-settings').attr('data-cb-width-desktop') || chatbox_width;
  } else {
    //MOBILE
    djQ(".dave-chatbox").css({
      "height": dave_mobile_size.dave_cb_height
    });
    DAVE_SCENE.is_mobile = true;
  } //AVATAR CODE BLOCK STARTS


  var data_avatar_initChk = djQ("#dave-settings").attr("data-avatar-id") || false;
  var dave_avatar_loadStatus = false;
  var chatboxOpenStatus = 'close';
  var nowRecording = false;

  function showAvatar(status) {
    if (status) {
      djQ(".dave-cb-avatar-contt").css("display", "block");
    } else {
      djQ(".dave-cb-avatar-contt").css("display", "none");
    }
  }

  DAVE_SCENE.set_avatar_position = function (x, y, h) {
    x = x || 0;
    y = y || DAVE_SCENE.is_mobile ? 0 : '100vh';
    h = h || 200;
    djQ(".dave-cb-avatar-contt").css({
      "left": x - parseFloat(h) / 2,
      "top": y
    });
    djQ("canvas#dave-canvas").height(h);
  };

  if (data_avatar_initChk) {
    DAVE_SETTINGS.register_custom_callback('onload', function () {
      dave_avatar_loadStatus = true;
      var avatar_xCord = djQ("#dave-settings").attr("data-avatar-position-x") || null;
      var avatar_yCord = djQ("#dave-settings").attr("data-avatar-position-y") || null;
      var avatarHeight = djQ("#dave-settings").attr("data-avatar-position-h") || null;
      /*let avatarWidth =  djQ("#dave-settings").attr("data-avatar-position-w") || null;*/

      DAVE_SCENE.set_avatar_position(avatar_xCord, avatar_yCord, avatarHeight);
      djQ(".dave-cb-tt").css("margin-left", "");

      if (dave_window_width > 450 && chatboxOpenStatus == "open") {
        var h = djQ(".dave-chatbox").height() + 50;
        dave_desktop_size.dave_cb_height = h;
      }

      if (chatboxOpenStatus == "open") {
        showAvatar(true);
      }

      set_maximized_height();
    }, DAVE_SCENE);
  } else {
    showAvatar(false);
  } //AVATAR CODE BLOCKS ENDS


  function set_inner_height() {
    if (dave_optionTitle) {
      djQ(".dave-cb-chatarea").height(-djQ(".dave-cb-tt-sec").first().offset().top - djQ(".dave-cb-tt-sec").first().height() + djQ(".dave-cb-stickBottom").first().offset().top - 27);
    } else {
      djQ(".dave-cb-chatarea").height(-djQ(".dave-cb-tt-sec").first().offset().top - djQ(".dave-cb-tt-sec").first().height() + djQ(".dave-cb-stickBottom").first().offset().top - 22);
    }
  }

  function set_maximized_height() {
    if (dave_window_width > 450) {
      djQ(".dave-chatbox").css({
        "height": dave_desktop_size.dave_cb_height
      });
    } else {
      djQ(".dave-chatbox").css({
        "height": dave_mobile_size.dave_cb_height
      });
    } //Just check if avatar is initialized then> is it mobile view or desk then> have a size of avtar detect that size and add to variable h


    var h = djQ(".dave-chatbox").height();
    var w = window.innerHeight;

    if (h > w) {
      //Just check if avatar is initialized then> is it mobile view or desk then> have a size of avtar detect that size and substract to variable w in height parameter passed
      djQ(".dave-chatbox").height(w);
    }

    set_inner_height();
  } // FUNCTION FOR PADDING LEADING ZEROS


  function pad(n, width, z) {
    width = width || 2;
    z = z || '0';
    return (String(z).repeat(width) + String(n)).slice(String(n).length);
  } //NUDGES CODE BLOCKS STARTS


  function dave_nudgeTrigger(index) {
    clearTimeout(dave_activityTimer);
    followupIndex++;

    if (followupIndex <= followupList_array.length) {
      if (DAVE_SCENE.opened == true) {
        n_openState = 'max';
      } else {
        n_openState = followupList_array[index][2];
      }

      if (DAVE_SCENE.opened) {
        botchat_data({
          'customer_state': followupList_array[index][0],
          "query_type": "auto"
        }, '', '', '', function (data) {
          dave_timerFunc();
        });
      } else {
        if (followupList_array[index][1]) {
          if (chatboxOpenStatus == 'close') {
            z = 'open';
          } else {
            z = '';
          }

          if (n_openState == 'max') {
            botchat_data({
              'customer_state': followupList_array[index][0],
              "query_type": "auto"
            }, '', z, 'max', function (data) {
              dave_timerFunc();
            });
          } else {
            botchat_data({
              'customer_state': followupList_array[index][0],
              "query_type": "auto"
            }, '', z, 'min', function (data) {
              dave_timerFunc();
            });
          }
        }
      }
    }
  }

  function dave_nudge(dave_activityInterval, followupList, forceStatus, openState) {
    openState = openState || false;
    var i = 0;
    followupIndex = 0;
    nudge_activityInterval = dave_activityInterval;

    if (dave_activityInterval && followupList) {
      followupList_array.length = 0;
      djQ.each(followupList, function (key, value) {
        followupList_array.push([value, forceStatus, openState]);
        i++;
      });

      if (followupList_array.length > 0) {
        timerFlag = true;
        dave_timerFunc();
        djQ("body").off("keydown click touchstart dblclick resize", window.dave_nudge_clear_func);
        djQ("body").on("keydown click touchstart dblclick resize", window.dave_nudge_clear_func);
      }
    }
  }

  function dave_timerFunc() {
    if (followupIndex <= followupList_array.length && timerFlag) {
      clearTimeout(dave_activityTimer);
      dave_activityTimer = setTimeout(function () {
        dave_nudgeTrigger(followupIndex);
      }, nudge_activityInterval);
    } else {
      djQ("body").off("keydown click touchstart dblclick resize", window.dave_nudge_clear_func);
    }
  } // THIS clears an nudges if they exist when you do any activities on the page


  window.dave_nudge_clear_func = function (e) {
    if (e.type == 'keydown' && (e.which < 30 || e.which > 90 && e.which < 96 || e.which > 105)) {
      return true;
    }

    console.debug("Reset nudge", e, e.which);
    clearTimeout(dave_activityTimer);
    dave_timerFunc();
  };

  window.stopTimmer = function () {
    timerFlag = false;
    clearTimeout(dave_activityTimer);
  };

  window.startTimmer = function () {
    timerFlag = true;

    if (typeof dave_timerFunc === 'function') {
      dave_timerFunc();
    }
  }; //NUDGES CODE BLOCKS END HERE
  //Like Dislike Function Block


  function like_dislike(currClickedElem, feedback_reaction, feedback_type) {
    if (feedback_reaction == 'like') {
      DAVE_SETTINGS.execute_custom_callback("on_click_like", [currClickedElem, feedback_type]);
      djQ(currClickedElem).html('');
      djQ(currClickedElem).html("<img src='".concat(liked_img, "'>"));
      djQ(currClickedElem).next().html('');
      djQ(currClickedElem).next().html("<img src='".concat(dislike_img, "'>"));
      l_d_react = true;
    } else {
      DAVE_SETTINGS.execute_custom_callback("on_click_dislike", [currClickedElem, feedback_type]);
      djQ(currClickedElem).html('');
      djQ(currClickedElem).html("<img src='".concat(disliked_img, "'>"));
      djQ(currClickedElem).prev().html();
      djQ(currClickedElem).prev().html("<img src='".concat(like_img, "'>"));

      if (feedback_type == "speech") {
        var disliked_voice_text = djQ(currClickedElem).closest('.dave-voiceBubble-feedback').prev('p').text();
        djQ("#dave-cb-textinput").val(disliked_voice_text);
      }

      l_d_react = false;
    }

    return l_d_react;
  } //printing Bubble Timing


  function dateTiming(part) {
    var dave_date = new Date();

    if (part) {
      return dave_date;
    } else {
      var printHrsMin = pad(dave_date.getHours()) + ":" + pad(dave_date.getMinutes());
      return printHrsMin;
    }
  } //auto scroll on every msg pop-in


  function scrollChatarea(speed) {
    speed = speed || 400;
    djQ(".dave-cb-chatarea").animate({
      scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight")
    }, speed);
  }

  DAVE_SETTINGS.scrollChatarea = scrollChatarea; //loader icon display function block

  window.dave_chatRes_loader = function dave_chatRes_loader(loaderType) {
    if (loaderType == 'bot') {
      if (DAVE_SETTINGS.execute_custom_callback('before_bot_loader') === false) {
        return djQ(".dave-cb-chatarea");
      }

      return djQ(".dave-cb-chatarea").append("<div class='dave-botchat dave-bottyping-loader'><div class='dave-bc-avatar'><img src='".concat(dave_bot_icon, "'></div><p class='dave-chattext dave-botchat-typing-p'><img class='dave-botchat-typing-img' src='").concat(typing_gif, "'></p></div>"));
    } else if (loaderType == 'user') {
      if (DAVE_SETTINGS.execute_custom_callback('before_user_loader') === false) {
        return djQ(".dave-cb-chatarea");
      }

      return djQ(".dave-cb-chatarea").append("<div class='dave-userchat dave-usertyping-loader'><div class='dave-uc-avatar'><img src='".concat(dave_user_icon, "'></div><p class='dave-userchat-p dave-user-typing-p'><img class='dave-user-typing-img' src='").concat(typing_gif, "'></p></div>"));
    }
  }; //remove loading gif 


  function dave_typingLoaderRemove(loaderType) {
    if (loaderType == 'user') {
      djQ('.dave-usertyping-loader').remove();
    } else if (loaderType == 'bot') {
      djQ('.dave-bottyping-loader').remove();
    }
  } //maximize chatbot


  function maximize_chatbox(minCustomSize) {
    //originalScroll = djQ('body').css('overflow');
    if (DAVE_SETTINGS.on_maximize() === false) {
      return;
    }

    chatboxOpenStatus = 'max';

    if (DAVE_SCENE.is_mobile) {
      djQ('.dave-cb-chatarea').css('display', 'block');
      djQ('.dave-cb-stickBottom').css('display', 'block');
    }

    djQ(".dave-cb-tt-minmax img").remove();
    djQ(".dave-cb-tt-minmax").append("<img src='".concat(min_img, "'>"));
    set_maximized_height();

    if (!minCustomSize) {
      djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
        minimize_chatbox();
      });
    } else {
      djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
        minimize_chatbox(minCustomSize);
      });
      djQ("textarea#dave-feedback-write").css("height", "");
      djQ(".rating-star-contt").css("bottom", "");
      djQ("#dave-userfull-rate, #dave-accuracy-rate").css('height', '');
    }

    scrollChatarea(500);
    DAVE_SETTINGS.execute_custom_callback("after_maximize_custom");
  } //minimize chatbot function


  function minimize_chatbox(customSize, chatType) {
    if (DAVE_SETTINGS.on_minimize() === false) {
      return;
    }

    chatboxOpenStatus = 'min';

    if (!DAVE_SCENE.is_mobile) {
      if (!customSize) {
        var type_sec_height = djQ(".dave-cb-type-sec").height();
        var chatsugg_height = djQ(".dave-cb-chatsugg").height();
        var tt_height = djQ(".dave-cb-tt-sec").height();
        var last_message_height = djQ(".dave-botchat").last().height();
        var ext_spc = 50;

        if (maxLockHeight) {
          maxLockHeight = parseInt(maxLockHeight);

          if (last_message_height > maxLockHeight) {
            djQ(".dave-chatbox").height(type_sec_height + chatsugg_height + tt_height + maxLockHeight + ext_spc);
          } else {
            djQ(".dave-chatbox").height(type_sec_height + chatsugg_height + tt_height + last_message_height + ext_spc);
          }
        } else {
          djQ(".dave-chatbox").height(type_sec_height + chatsugg_height + tt_height + last_message_height + ext_spc);
        }
      } else {
        if (!chatType) {
          djQ(".dave-chatbox").height(customSize);
        } else {
          djQ(".dave-chatbox").height(customSize); // if (chatType == 'feedback') {
          //     var writeFeedbackBox = (djQ("textarea#dave-feedback-write").height()) / 2;
          //     djQ("textarea#dave-feedback-write").css("height", writeFeedbackBox);
          //     djQ(".rating-star-contt").css("bottom", "-30%");
          //     djQ("#dave-userfull-rate, #dave-accuracy-rate").height('10%');
          // }
        }
      }

      set_inner_height();
      scrollChatarea(500);
    } else {
      var headerSecHeight = Math.max(djQ('.dave-cb-tt-sec').height(), '200');
      djQ('.dave-chatbox-open').height(headerSecHeight);
    }

    djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
      maximize_chatbox();
    });
    djQ('body').css("overflow", originalScroll);
    console.log("set back original scroll", originalScroll);
    djQ(".dave-cb-tt-minmax img").remove();
    djQ(".dave-cb-tt-minmax").append("<img src='".concat(max_img, "'>"));
    DAVE_SETTINGS.execute_custom_callback("after_minimize_custom");
  }

  DAVE_SETTINGS.maximize = maximize_chatbox;
  DAVE_SETTINGS.minimize = minimize_chatbox; //PREDICTION FOR USER TYPEING MESSAGE CODE BLOCK

  function dave_prediction(selectedPrediction) {
    if (selectedPrediction.value.length > 2) {
      DAVE_SETTINGS.predict(selectedPrediction.value, function (response) {
        DAVE_SETTINGS.execute_custom_callback("before_predicted_text", [selectedPrediction.value, response]);

        if (text_cleared) {
          console.warn("text was sent before we got prediction, so doing nothing!");
          return;
        }

        if (response.length > 0) {
          console.debug("We are getting response for prediction");

          if (!pred_attach) {
            djQ('.dave-pred-contt').append("<ol class='dave-pred-box'></ol>");

            if (response.length < 5) {
              djQ(".dave-pred-contt").css({
                "bottom": djQ(".dave-cb-type-sec").height() + 10 + "px",
                "height": "auto",
                "overflow": "auto",
                "max-height": "110px",
                "padding-bottom": "10px"
              });
            } else if (response.length >= 5) {
              djQ(".dave-pred-contt").css({
                "bottom": djQ(".dave-cb-type-sec").height() + 10 + "px",
                "height": "110px",
                "overflow-y": "scroll"
              });
            }

            pred_attach = true;
          }
        } else {
          console.warn("we are not getting any response for prediction");
          djQ(".dave-pred-contt").html('');
          djQ(".dave-pred-contt").css({
            "height": "0",
            "overflow": "hidden",
            "padding": "0"
          });
          pred_attach = false;
        }

        djQ('.dave-pred-box').html('');
        djQ.each(response, function (keys, values) {
          pred_custState = Object.keys(values);
          pred_title = Object.values(values);
          DAVE_SETTINGS.execute_custom_callback("before_add_predicted_text", [pred_title, pred_custState]);
          var prd = "\n                        <li class='dave-pred-txt' data-pred-custState='".concat(pred_custState, "'>").concat(pred_title, "</li>\n                    ");
          DAVE_SETTINGS.execute_custom_callback("on_add_predicted_text", [prd]);
          djQ(".dave-pred-box").append(prd);
        });

        if (pred_attach) {
          djQ(".dave-pred-txt").one("click", function () {
            var clicked_pred = djQ(this).text();
            clicked_pred_custState = djQ(this).attr('data-pred-custState');
            DAVE_SETTINGS.execute_custom_callback("before_click_predicted_option", [clicked_pred, clicked_pred_custState]);

            if (DAVE_SETTINGS.execute_custom_callback("before_click_predicted_option", [clicked_pred, clicked_pred_custState]) === false) {
              return;
            }

            djQ("#dave-cb-textinput").val(clicked_pred);
            djQ("#dave-cb-textinput").focus();
            djQ(".dave-pred-contt").html('');
            djQ(".dave-pred-contt").css({
              "height": "0",
              "overflow": "auto",
              "padding": "0"
            });
            pred_attach = false;
            dave_predication_send = true;
            set_inner_height();
            DAVE_SETTINGS.execute_custom_callback("after_click_predicted_option", [clicked_pred, clicked_pred_custState]);
          });
        }
      });
    } else {
      djQ(".dave-pred-contt").html('');
      djQ(".dave-pred-contt").css({
        "height": "0",
        "overflow": "hidden",
        "padding": "0"
      });
      pred_attach = false;
    }
  }

  djQ(document).on("click", ".dave-cb-chatarea, .dave-cb-chatsugg", function () {
    djQ(".dave-pred-contt").html('');
    djQ(".dave-pred-contt").css({
      "height": "0",
      "overflow": "auto",
      "padding": "0"
    });
    pred_attach = false;
  });

  function replyBubbleUserMsg(replyId) {
    if (typeof replyId == 'undefined') {
      return false;
    }

    ;
    var msg = djQ('.dave-cb-chatarea').find("#".concat(replyId)).find('.dave-userchat-p').text();
    djQ('.dave-cb-chatarea').find("#".concat(replyId)).nextAll().find("[data-scrollmsgid = '".concat(replyId, "']")).find('.replyBubbleMsg').html(msg); // /.find('.replyBubbleMsg').html(msg)
  } //ATTACH ONLY TEXT MESSAGE BUBBLE USER END


  function userTextMsgBubble(text, chatType, dateTime, bubbleId, msgId) {
    var utmb_date = dateTime || dateTiming();
    dave_typingLoaderRemove('user');
    djQ(".dave-cb-chatarea").append("\n                <div class='dave-userchat' id='".concat(bubbleId, "'>\n                    <div class='dave-uc-avatar'>\n                        <img src='").concat(dave_user_icon, "'>\n                    </div>\n                    <p class='dave-userBubble-header'>\n                        <span class='dave-userBubble-Title'> ").concat(dave_user_bubbleTitle, " &nbsp;&nbsp;</span>\n                        <span class='dave-userBubble-timestamp'>").concat(utmb_date, "</span>\n                    </p>\n                    <p class='dave-userchat-p'>").concat(text, "</p>\n                </div>\n            ")).ready(function () {
      if (chatType == "speech") {
        djQ('.dave-userchat').last().attr('data-voiceMsgId', msgId);
        dave_chatRes_loader('bot');
      }
    });
  } //Message Bubble Print Function Block


  function OCD_msgBubbles(userIcon, name, timestamp, bubbleId, responseTxt, reply_to) {
    djQ(".dave-cb-chatarea").append("\n                <div class='dave-botchat'>\n                    <div class='dave-bc-avatar'>\n                        <img src='".concat(userIcon, "'>\n                    </div>\n                    <p class='dave-botBubble-header'>\n                        <span class='dave-botBubble-Title'>").concat(name, "&nbsp;&nbsp;</span>\n                        <span class='dave-botBubble-timestamp'>").concat(timestamp, "</span>\n                    </p>\n                    ").concat(bubbleId && !reply_to ? "<div style='display: none;' class='scrollToMsg' data-scrollMsgId='".concat(bubbleId, "'></div>") : "".concat(bubbleId && reply_to ? "<div class='scrollToMsg' data-scrollMsgId='".concat(reply_to, "'><p class='replyBubbleTitle'>Query</p><p class='replyBubbleMsg'></p></div>") : ''), "\n                    <p class='dave-chattext'>").concat(responseTxt, "</p>\n                </div>\n            ")).ready(function () {
      djQ(".dave-cb-chatarea").animate({
        scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight")
      }, 500);

      if (reply_to) {
        replyBubbleUserMsg(reply_to);
      }
    });
  }

  function updateBubbleId(bubbleId) {
    var x = temp_bubbleId.pop();
    var z = perm_bubbleId.pop();
    djQ('.dave-cb-chatarea').find('#' + x).attr('id', z);
  }

  function msgBubbles(msgBy, chatType, dateTime, userMsg, bubbleId, replyPrintState, voiceMsgId, custState, callback) {
    DAVE_SETTINGS.execute_custom_callback('before_message_bubble', [msgBy, chatType, dateTime, userMsg, bubbleId, replyPrintState, voiceMsgId, custState]);
    userMsg = userMsg || undefined;
    voiceMsgId = voiceMsgId || undefined;
    custState = custState || undefined;

    if (msgBy == 'bot') {
      if (chatType == 'normal' || chatType == 'history' || chatType == 'speech' || chatType == 'error') {
        dave_typingLoaderRemove('bot');
        djQ(".dave-cb-chatarea").append("\n                <div class='dave-botchat'>\n                    <div class='dave-bc-avatar'>\n                        <img src='".concat(dave_bot_icon, "'>\n                    </div>\n                    <p class='dave-botBubble-header'>\n                        <span class='dave-botBubble-Title'>").concat(dave_bot_bubbleTitle, "&nbsp;&nbsp;</span>\n                        <span class='dave-botBubble-timestamp'>").concat(dateTime, "</span>\n                    </p>\n                    ").concat(bubbleId && !replyPrintState ? "<div style='display: none;' class='scrollToMsg' data-scrollMsgId='".concat(bubbleId, "'></div>") : "".concat(bubbleId && replyPrintState ? "<div class='scrollToMsg' data-scrollMsgId='".concat(bubbleId, "'><p class='replyBubbleTitle'>Query</p><p class='replyBubbleMsg'></p></div>") : ''), "\n\n                    <p class='dave-chattext'>").concat(userMsg, "</p>\n                </div>\n            ")).ready(function () {
          if (chatType == 'speech') {
            djQ('button#start-recording').prop('disabled', false);
            djQ(".dave-userchat[data-voiceMsgId='".concat(voiceMsgId, "']")).next('.dave-botchat').attr("data-voiceMsgId", voiceMsgId);
          }

          if (replyPrintState) {
            replyBubbleUserMsg(replyPrintState);
          }

          if (chatType == "normal") {
            updateBubbleId(bubbleId);
          }
        });
      } else {
        djQ(".dave-cb-chatarea").append("\n                <div class='dave-botchat'>\n                    <div class='dave-bc-avatar'>\n                        <img src='".concat(dave_bot_icon, "'>\n                    </div>\n                    <p class='dave-chattext'>We don't have a response to your request. Did you really say something ?</p>\n                </div>\n            "));
      }
    } else {
      if (chatType == 'normal' || chatType == 'history' || chatType == 'error' || chatType == 'speech') {
        if (!voiceMsgId) {
          userTextMsgBubble(userMsg, chatType, dateTime, bubbleId);
        } else {
          userTextMsgBubble(userMsg, chatType, dateTime, bubbleId, voiceMsgId);
        }

        if (chatType == 'history') {
          djQ('.dave-userchat').append("<p class='hidden' id='customer_response_history'>".concat(custState, "</p>"));
        } else if (chatType == 'speech') {
          djQ('button#start-recording').prop('disabled', true);
          djQ('.dave-userchat').append("\n                    <div class='dave-voiceBubble-feedback'>\n                        <input type='hidden' value='".concat(voiceMsgId, "' name='voice-responseStoredId'>\n                        <button class='voice-thumbsup' data-thumbtype='like'>\n                            <img src='").concat(like_img, "'>\n                        </button>\n                        <button class='voice-thumbsdown' data-thumbtype='dislike'>\n                            <img src='").concat(dislike_img, "'>\n                        </button>\n                    </div>\n                "));
        }
      }
    }

    DAVE_SETTINGS.execute_custom_callback('after_message_bubble', [msgBy, chatType, dateTime, userMsg, bubbleId, replyPrintState, voiceMsgId, custState]);

    if (callback) {
      callback();
    }
  }

  djQ(document).on('click', '.scrollToMsg', function () {
    var msgId = djQ(this).attr('data-scrollMsgId');
    var cbHeight = djQ('.dave-cb-chatarea').height();
    var msgBubblePos = djQ('#' + msgId).position().top;
    var chatScrollHeight = djQ('.dave-cb-chatarea').scrollTop();

    if (msgBubblePos < 0) {
      djQ('.dave-cb-chatarea').animate({
        scrollTop: chatScrollHeight - Math.round(cbHeight / 2.5)
      }, 200);
      djQ('#' + msgId).css("box-shadow", "-10px 0px 5px 5px #ecebeb");
      setTimeout(function () {
        djQ('#' + msgId).css("box-shadow", "");
      }, 1000);
    } else if (msgBubblePos > cbHeight) {
      djQ('.dave-cb-chatarea').animate({
        scrollTop: chatScrollHeight + msgBubblePos - Math.round(cbHeight / 2.5)
      }, 200);
      djQ('#' + msgId).css("box-shadow", "-10px 0px 5px 5px #ecebeb");
      setTimeout(function () {
        djQ('#' + msgId).css("box-shadow", "");
      }, 1000);
    } else {
      djQ('#' + msgId).css("box-shadow", "-10px 0px 5px 5px #ecebeb");
      setTimeout(function () {
        djQ('#' + msgId).css("box-shadow", "");
      }, 1000);
    }

    console.debug("== " + msgId + " == " + chatScrollHeight + " == " + cbHeight + " == " + Math.round(cbHeight / 2.5) + " == " + msgBubblePos);
  });

  window.other_chat_data = function (status, userIcon, name, timestamp, responseTxt, reply_to, data, bubbleId) {
    timestamp = DAVE_SETTINGS.print_timestamp(timestamp);

    if (status == 'typing') {
      djQ(".dave-cb-chatarea").append("<div class='dave-botchat .dave-otherpersonTyping-loader'><div class='dave-bc-avatar'><img src='".concat(userIcon, "'></div><p class='dave-chattext dave-botchat-typing-p'><img class='dave-botchat-typing-img' src='").concat(typing_gif, "'></p></div>"));
    } else {
      djQ(".dave-otherpersonTyping-loader").remove(); //window.response_print = function (data, bubbleId, data_length, new_date, chatType, msgId) {

      OCD_msgBubbles(userIcon, name, timestamp, bubbleId, responseTxt, reply_to);
      response_print(data, 1, timestamp, 'other_chat_data');
    }
  }; //On Error Display Msg Function Block


  function chatResponseError(chatType, eCode, params, bubbleId, behaviour, openState, lockHeight, prevTimeout) {
    prevTimeout = prevTimeout || 500;
    error_code = eCode['status'];
    dave_typingLoaderRemove('bot');
    msgBubbles('bot', 'error', dateTiming());

    if (error_code >= 400 & error_code < 500) {
      djQ(".dave-chattext").last().html("There seems to be some technical issue in the system. Please try a different query.");
      scrollChatarea();
    } else if (error_code >= 500) {
      djQ(".dave-chattext").last().html("There seems to be a delay in getting your response, hold on!");
      scrollChatarea();
      setTimeout(function () {
        if (chatType == "normal") {
          botchat_data(params, bubbleId, behaviour, lockHeight, openState);
        } else if (chatType == "history") {
          botchat_history();
        }
      }, prevTimeout * 2);
    } else {
      djQ(".dave-chattext").last().html("There seems to be problem in getting your response.  Can you please try again in some time?");
      scrollChatarea();
    }
  }

  window.handleMapError = function () {
    djQ(".dave-cb-form form").last().prev().remove();
    djQ(".dave-cb-form form").last().before("\n            <button class=\"getCurLoc\">Get My Location</button>\n        ");
  };

  djQ(document).on("click", ".getCurLoc", function () {
    getUserCurrentLocation(true);
    console.debug("TOOG");
  });

  function getUserCurrentLocation(directAccess) {
    if (directAccess) {
      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function (position) {
          userPositon['lat'] = position.coords.latitude;
          userPositon['lng'] = position.coords.longitude;
          console.debug(userPositon);
        }, function (error) {
          switch (error.code) {
            case error.PERMISSION_DENIED:
              console.warn("Permission denied by user.");
              break;

            case error.POSITION_UNAVAILABLE:
              console.warn("Location position unavailable.");
              break;

            case error.TIMEOUT:
              console.warn("Request timeout.");
              break;

            case error.UNKNOWN_ERROR:
              console.warn("Unknown error.");
              break;
          }
        }, {
          timeout: 1000,
          enableHighAccuracy: true
        });
      } else {
        console.log("Browser doesn't support geolocation!");
      }
    }
  }

  function loadDaveMap() {
    var googleMapKey = djQ(document).find("#dave-settings").attr("data-google-map-key");

    if (googleMapKey) {
      djQ.ajax({
        url: "https://maps.googleapis.com/maps/api/js?key=".concat(googleMapKey, "&libraries=geometry,places"),
        type: "GET",
        dataType: "script",
        cache: true,
        success: function success(data) {
          var map;
          var marker;
          var predefinedLocation = [{
            location: {
              "lat": 19.236370361630183,
              "lng": 73.13019900709297
            },
            title: "Title",
            address: "Adress"
          }, {
            location: {
              "lat": 19.193919679842637,
              "lng": 72.97556454923306
            },
            title: "Title",
            address: "Adress"
          }, {
            location: {
              "lat": 18.95095435428654,
              "lng": 72.83516893108772
            },
            title: "Title",
            address: "Adress"
          }]; // INITIALIZING THE GOOGLE MAP

          function initMap() {
            // SETTING UP THE MAP VARIABLE
            map = new google.maps.Map(djQ("#map")[0], {
              center: {
                lat: 20.5937,
                lng: 78.9629
              },
              zoom: 5
            }); // InfoWindow, MARKER, GEOCODER USED TO GENERATE PIN, AND LOCATION FULL DETAILS

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById("infowindow-content");
            var geocoder = new google.maps.Geocoder();
            var marker = new google.maps.Marker({
              map: map
            });
            infowindow.setContent(infowindowContent); // PREDEFINED LOCATION USED TO POINT SEVERAL LOCATION

            function pdl_InfoWindow(marker, map, title, address, url) {
              google.maps.event.addListener(marker, 'click', function () {
                var html = "\n                            <div id=\"infowindow-content\">\n                                <span id=\"place-name\" class=\"title\">".concat(title, "</span><br />\n                                <span id=\"place-address\">").concat(address, "</span>\n                            </div>\n                            ");
                pdl_infoWin = new google.maps.InfoWindow({
                  content: html,
                  maxWidth: 350
                });
                pdl_infoWin.open(map, marker);
              });
            } // LOOPING THROUGH PREDEFINED LOCATION DATA TO SET ALL LOCATIONS WITH MARKER ON MAP


            djQ.each(predefinedLocation, function (i, val) {
              geocoder.geocode({
                location: val['location']
              }).then(function (response, status) {
                if (response.results[0]) {
                  var predefinedMarker = new google.maps.Marker({
                    map: map,
                    icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                    position: val['location'],
                    animation: google.maps.Animation.DROP,
                    address: val['Address'] || response.results[0].formatted_address,
                    title: val['title'] || "Location" // url: url

                  });
                  pdl_InfoWindow(predefinedMarker, map, val['title'], response.results[0].formatted_address);
                } else {
                  window.alert("No results found");
                }
              }).catch(function (e) {
                return window.alert("Geocoder failed due to: " + e);
              });
            }); //INPUT BOX FOR USERS TO SEARCH PARTICULAR LOCATION

            djQ(document).on("focus", "#locationInput", function () {
              var input = document.getElementById('locationInput');
              var options = {
                fields: ["formatted_address", "geometry", "name"],
                strictBounds: false,
                types: ["establishment"]
              }; // SETTING UP THE Autocomplete VARIABLE TO TRIGGER AND GET AUTOSUGGESTIONS OF LOCATION

              var autocomplete = new google.maps.places.Autocomplete(input, options); //POINT NEW LOCATION USER SELECTED USING INPUT BOX

              autocomplete.addListener("place_changed", function () {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();

                if (!place.geometry || !place.geometry.location) {
                  window.alert("No details available for input: '" + place.name + "'");
                  return;
                }

                if (place.geometry.viewport) {
                  map.fitBounds(place.geometry.viewport);
                } else {
                  map.setCenter(place.geometry.location);
                  map.setZoom(17);
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                userPositon['lat'] = place.geometry.location.lat();
                userPositon['lng'] = place.geometry.location.lng();
                console.debug(userPositon);
                infowindowContent.children["place-name"].textContent = place.name;
                infowindowContent.children["place-address"].textContent = place.formatted_address;
                infowindow.open(map, marker);
              });
            }); //GET USERS CURRENT LOCATION

            if ("geolocation" in navigator) {
              navigator.geolocation.getCurrentPosition(function (position) {
                userPositon['lat'] = position.coords.latitude;
                userPositon['lng'] = position.coords.longitude;
                marker.setPosition(userPositon);
                marker.setVisible(true);
                geocoder.geocode({
                  location: userPositon
                }).then(function (response, status) {
                  if (response.results[0]) {
                    infowindowContent.children["place-name"].textContent = response.results[0].name;
                    infowindowContent.children["place-address"].textContent = response.results[0].formatted_address;
                    infowindow.open(map, marker);
                  } else {
                    window.alert("No results found");
                  }
                }).catch(function (e) {
                  return window.alert("Geocoder failed due to: " + e);
                });
                marker.setDraggable(true);
                map.setCenter(userPositon);
                map.setZoom(10);
                console.debug(userPositon);
                google.maps.event.addListener(marker, 'drag', function () {
                  userPositon['lat'] = marker.position.lat();
                  userPositon['lng'] = marker.position.lng();
                  console.debug(userPositon);
                });
              }, function (error) {
                switch (error.code) {
                  case error.PERMISSION_DENIED:
                    console.warn("Permission denied by user.");
                    break;

                  case error.POSITION_UNAVAILABLE:
                    console.warn("Location position unavailable.");
                    break;

                  case error.TIMEOUT:
                    console.warn("Request timeout.");
                    break;

                  case error.UNKNOWN_ERROR:
                    console.warn("Unknown error.");
                    break;
                }
              }, {
                timeout: 1000,
                enableHighAccuracy: true
              });
            } else {
              console.log("Browser doesn't support geolocation!");
            }
          }

          window.initializeMap = function () {
            initMap();
          };
        },
        error: function error(e) {
          console.error(e);
          console.warn("Something Went Wrong While Loading Google Map API");
          getUserCurrentLocation(true);
        }
      });
    } else {
      console.warn("NO KEY FOUND SO NO MAP LOAD");
    }
  }

  if (djQ(document).find("#dave-settings").attr("data-google-map-key")) {
    loadDaveMap();
  } //SERVER RESPONSE PRINTING FUNCTION BLOCK


  window.response_print = function (data, data_length, new_date, chatType, msgId) {
    if (close_count > 0) {
      return;
    }

    var permbubId = "RI" + data['response_id'];
    map_enabled = false;
    perm_bubbleId.push(permbubId);
    voice_MsgId = msgId || undefined;

    if (chatType == "speech") {
      dataLength_thresold = 1;
    } else {
      dataLength_thresold = 0;
      voice_MsgId = null;
    }

    if (data_length > dataLength_thresold) {
      dave_response_dataType = data['data']['response_type'];
      var rp_replyId = data['data']['reply_to'] || false;

      if (chatType == "normal") {
        dave_typingLoaderRemove('bot'); //print bot reply as message
      }

      if (chatType == "normal" || chatType == "speech") {
        msgBubbles('bot', chatType, dateTiming(), '', permbubId, rp_replyId, voice_MsgId, '', function () {
          if (chatType == 'speech') {
            djQ(".dave-userchat[data-voiceMsgId='".concat(voice_MsgId, "']")).find("input[name='voice-responseStoredId']").val(data['response_id']);
          }
        });
      }

      scrollChatarea(500);

      if (chatType == "normal" || chatType == "speech") {
        djQ(".dave-chattext").last().html(data["placeholder"] || ""); // if no state options are provided, then previous options are kept

        if (data['state_options'] && !djQ.isEmptyObject(data['state_options'])) {
          djQ('.dave-cb-chatsugg').html('');
          djQ(".dave-cb-chatsugg").css("overflow-x", "auto");
        }

        djQ(".dave-cb-chatsugg").append("<span class='chatsugg-next'><img src='".concat(cb_sugg_next, "'></span><span class='chatsugg-prev'><img src='").concat(cb_sugg_prev, "'></span>"));
      }

      response_elementPrint(dave_response_dataType, data, chatType);

      if (data['state_options'] && !djQ.isEmptyObject(data['state_options'])) {
        //print bot suggestion
        for (var _i = 0, _Object$entries = Object.entries(data['state_options']); _i < _Object$entries.length; _i++) {
          var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
              key = _Object$entries$_i[0],
              value = _Object$entries$_i[1];

          djQ('.dave-cb-chatsugg').append("<div class='dave-cb-chatsugg-list'>".concat(value, "<input type='hidden' value='").concat(key, "'></div>"));
        }
      } //Bot Reply Feedback Thumbsup and ThumbsDown


      if (chatType == "normal" || chatType == "speech") {
        if (data['show_feedback']) {
          djQ('p.dave-chattext').last().append("\n                    <div class='dave-botBubble-feedback'>\n                        <span class='hidden'>".concat(data['response_id'], "</span>\n                        <button class='dave-thumbsup' data-thumbType='like'><img src='").concat(like_img, "'></button>\n                        <button class='dave-thumbsdown' data-thumbType='dislike'><img src='").concat(dislike_img, "'></button>\n                    </div>\n                "));
        } //Nudges Starting


        if (typeof data['data']['_follow_ups'] !== 'undefined') {
          var forceOpen = data['data']['_force_open'] || null;
          var openState = data['data']['_open_state'] || 'min';
          dave_nudge(data['wait'], data['data']['_follow_ups'], forceOpen, openState);
        } //QUICKACCESS BUTTON NEXT & PREV ONLY VISIBLE ON SCROLL IS AVAILABLE


        var chatsugg_scrollWidth = djQ('.dave-cb-chatsugg')[0].scrollWidth;

        if (chatsugg_scrollWidth > chatbox_width - 20) {
          djQ('span.chatsugg-next').css('display', 'block');

          if (dave_optionTitle) {
            djQ('span.chatsugg-next').css('top', '12px');
            djQ('span.chatsugg-prev').css('top', '12px');
          } else {
            djQ('.dave-chatsuggTitle').hide();
          }
        }
      } //Chat box height Adjustment according to device


      set_inner_height();
      dave_userchatfunc_exe = false;
      chat_loading = true;
      userchat_data();
      djQ('button#start-recording').prop('disabled', false);
    } else {
      //if no data or reply recieved from bot
      dave_typingLoaderRemove('bot');
      msgBubbles('bot', 'error', dateTiming(), 'We don\'t have a response to your request. Did you really say something ?');
      scrollChatarea(500);
    }
  }; //Response Element Print


  function response_elementPrint(dave_response_dataType, data, chatType, count) {
    if (dave_response_dataType == 'thumbnails') {
      DAVE_SETTINGS.execute_custom_callback("before_thumbnails_response", [data['data'][dave_response_dataType], data['data'], data]);
      djQ('p.dave-chattext').last().append("\n                <div class=\"dave-chat-items-contt\">\n                    <ul class=\"dave-chat-items\"></ul>\n                </div>\n            ");
      djQ.each(data['data'][dave_response_dataType], function (key, value) {
        var s = djQ("\n                    <a href=\"".concat(encodeURI(value['url']) || '#', "\" data-customer-state='").concat(value['customer_state'], "' data-customer-response='").concat(value['customer_response'] || value['title'] || DAVE_SETTINGS.toTitleCase(value['customer_state']), "' target='").concat(value['target'] || '_blank', "'>\n                        <li>\n                            <div class='dave-chat-items-imgcontt'><img src='").concat(value['image'], "'></div>\n                            <p class='dave-chat-items-bottom-p'>").concat(value['title'], "</p>\n                        </li>\n                    </a>\n                "));
        djQ('.dave-chat-items').last().append(s);

        if (value['customer_state'] || value['customer_response']) {
          djQ(s).on('click', function () {
            userTextMsgBubble(djQ(this).attr('data-customer-response'), 'normal', dateTiming(), djQ.now());
            botchat_data({
              'query_type': 'click',
              'customer_state': djQ(this).attr('data-customer-state'),
              'customer_response': djQ(this).attr('data-customer-response')
            });
            return false;
          });
        }
      });
      DAVE_SETTINGS.execute_custom_callback("after_thumbnails_response", [data['data'][dave_response_dataType], data['data'], data]);
    } else if (dave_response_dataType == 'image') {
      DAVE_SETTINGS.execute_custom_callback("before_image_response", [data['data'][dave_response_dataType], data['data'], data]);
      var s = djQ("\n                <div class='dave-chat-items-contt' data-url='".concat(encodeURI(data.data.url), "' data-customer-state='").concat(data.data.customer_state, "' data-customer-response='").concat(data.data.customer_response || data.data.title || DAVE_SETTINGS.toTitleCase(data.data.customer_state), "'>\n                    <div class='dave-chat-imageResponse'><img class='dave-imgResponse-image' src='").concat(data['data']['image'], "'></div>\n                </div>\n            "));
      djQ('p.dave-chattext').last().append(s);

      if (data.data.customer_state || data.data.customer_response) {
        djQ(s).on('click', function () {
          userTextMsgBubble(djQ(this).attr('data-customer-response'), 'normal', dateTiming(), djQ.now());
          botchat_data({
            'query_type': 'click',
            'customer_state': djQ(this).attr('data-customer-state'),
            'customer_response': djQ(this).attr('data-customer-response')
          });
          return false;
        });
      }

      DAVE_SETTINGS.execute_custom_callback("after_image_response", [data['data'][dave_response_dataType], data['data'], data]);
    } else if (dave_response_dataType == "form") {
      DAVE_SETTINGS.execute_custom_callback("before_form_response", [data['data'][dave_response_dataType], data['data'], data]);
      form_error = '';
      var dbld = chatType == 'history' && count < dave_return_size ? 'disabled' : '';
      djQ('p.dave-chattext').last().append("\n                <div class=\"dave-cb-form\">\n                    <form class='dave-form' type='POST'></form>\n                </div>\n            ");
      djQ.each(data['data'][dave_response_dataType], function (key, value) {
        var datetime_converter = {
          'datetime': 'text',
          'date': 'text',
          'time': 'text'
        };
        datetime_converter['minDT'] = value.min_date || value.min_time ? (value.min_date || '1900-01-01') + "T" + (value.min_time || '00:00') : "";
        datetime_converter['maxDT'] = value.max_date || value.max_time ? (value.max_date || '2050-12-31') + "T" + (value.max_time || '23:59') : "";
        datetime_converter['step'] = value['step'] || '';
        datetime_converter['default_date'] = new Date(value['default_date']);
        var formats = {
          'datetime': 'd/m/Y H:i',
          'date': 'd/m/Y',
          'time': 'H:i'
        };

        if (value['format']) {
          for (var k in value['formats']) {
            formats[k] = value['formats'][k];
          }
        }

        value['title'] = value['title'] || DAVE_SETTINGS.toTitleCase(value['name']);

        if (value['ui_element'] == "textarea") {
          djQ('.dave-cb-form form').last().append("<textarea name='".concat(value['name'], "' placeholder='").concat(value['placeholder']).concat(value['required'] ? '*' : '', "' value='").concat(value['value'] || '', "' title='").concat(value['title'], "' ").concat(value['required'] ? 'required' : '', " ui_element='").concat(value['ui_element'], "' ").concat(dbld, "></textarea>"));
        } else if (value['ui_element'] == "select") {
          var _s2 = "<option value=\"\">".concat(value['placeholder']).concat(value['required'] ? '*' : '', "</option>");

          djQ.each(value['options'] || [], function (k, v) {
            _s2 += "<option value=".concat(typeof k == 'string' ? k : typeof v != 'string' ? v[0] : v, ">").concat(typeof v != 'string' ? v[1] : v, "</option>");
          });
          djQ('.dave-cb-form form').last().append("<select name='".concat(value['name'], "' title='").concat(value['title'], "' ").concat(value['required'] ? 'required' : '', " ui_element='").concat(value['ui_element'], "' ").concat(dbld, ">").concat(_s2, "</select>"));
        } else if (value['ui_element'] == "file") {
          var file_typeAllowed = value['file_type'];
          var max_uploadSize = value['max_file_size'];
          djQ('.dave-cb-form form').last().append("\n                        <label class='file_upload_label'>".concat(value['title'], "</label>\n                        <input type='file' name='").concat(value['name'], "' class='file_upload' accept=\"").concat(file_typeAllowed, "\" data-max='").concat(max_uploadSize, "' ").concat(value['required'] ? 'required' : '', " ui_element='").concat(value['ui_element'], "' title='").concat(value['title'], "' ").concat(dbld, ">\n                    "));
        } else if (value['ui_element'] == "geolocation") {
          map_enabled = true; // *********************************

          if (chatType !== "history") {
            djQ(".dave-cb-form form").last().before("\n                        <input type=\"text\" name=\"".concat(value["name"], "\" id=\"locationInput\" class=\"mapLocationInput\" placeholder=\"").concat(value['placeholder'], "\"").concat(value['required'] ? '*' : '', " title=\"").concat(value['title'], "\" error='").concat(value['error'] || '', "' ui_element='").concat(value['ui_element'], "'} >\n                        "));
            djQ('.dave-cb-form form').last().append("\n                            <div class=\"mapBox\">\n                                <div id=\"map\"></div>\n                                <div id=\"infowindow-content\">\n                                    <span id=\"place-name\" class=\"title\"></span><br />\n                                    <span id=\"place-address\"></span>\n                                </div>\n                            </div>\n                        ");
            initializeMap();
          } else if (chatType == 'history' && count >= dave_return_size) {
            djQ('.dave-cb-form form').last().append("\n                            <input type=\"text\" name=\"".concat(value["name"], "\" placeholder=\"").concat(value['placeholder']).concat(value['required'] ? '*' : '', "\" title=\"").concat(value['title'], "\" required=\"value\" error='").concat(value['error'] || '', "' ui_element='").concat(value['ui_element'], "' ").concat(value['required'] ? 'required' : '', " disabled>\n                        "));
          } // *********************************

        } else if (value['ui_element'] == "tel") {
          var _s3 = djQ("\n                            <input type='text' class='tel' name='".concat(value['name'], "' placeholder='").concat(value['placeholder']).concat(value['required'] ? '*' : '', "' value='").concat(value['value'] || '', "' title='").concat(value['title'], "' ").concat(value['required'] ? 'required' : '', " autocomplete=\"off\" error='").concat(value['error'] || 'Please enter a valid mobile number', "' ui_element='").concat(value['ui_element'], "' validate=\"[+]{0,1}[0]{0,2}[1-9][0-9]{9}\" ").concat(dbld, ">\n                    "));

          djQ('.dave-cb-form form').last().append(_s3);
          djQ(_s3).ForcePhoneNumbersOnly();
        } else if (value['ui_element'] == "email") {
          var _s4 = djQ("\n                            <input type='text' class='tel' name='".concat(value['name'], "' placeholder='").concat(value['placeholder']).concat(value['required'] ? '*' : '', "' value='").concat(value['value'] || '', "' title='").concat(value['title'], "' ").concat(value['required'] ? 'required' : '', " autocomplete=\"off\" error='").concat(value['error'] || 'Please enter a valid email', "' ui_element='").concat(value['ui_element'], "' validate=\"\\w+([\\.-_]?\\w+)*@\\w+([\\.-_]?\\w+)*(\\.\\w{2,5})+\" ").concat(dbld, ">\n                    "));

          djQ('.dave-cb-form form').last().append(_s4);
        } else {
          djQ('.dave-cb-form form').last().append("<input type='".concat(datetime_converter[value['ui_element'] || 'text'] || value['ui_element'] || 'text', "' class='").concat(value['ui_element'], "'name='").concat(value['name'], "' placeholder='").concat(value['placeholder']).concat(value['required'] ? '*' : '', "' value='").concat(value['value'] || '', "' title='").concat(value['title'], "' ").concat(value['required'] ? 'required' : '', " min='").concat(datetime_converter['minDT'], "' max='").concat(datetime_converter['maxDT'], "' step='").concat(datetime_converter['step'], "' autocomplete=\"off\" error='").concat(value['error'] || '', "' ui_element='").concat(value['ui_element'], "' ").concat(dbld, ">"));
        }

        if (['datetime', 'date', 'time'].indexOf(value['ui_element']) >= 0) {
          var t = {
            minDate: value.ui_element != 'time' ? value['min_date'] : false,
            maxDate: value.ui_element != 'time' ? value['max_date'] : false,
            minTime: value.ui_element != 'date' ? value['min_time'] : false,
            maxTime: value.ui_element != 'date' ? value['max_time'] : false,
            defaultTime: value['default_time'],
            defaultDate: datetime_converter.default_date,
            step: value['step'] || parseInt(value['time_resolution'] || "0") * 60 || 3600,
            mask: true,
            value: value.value ? new Date(value['value']) : '',
            format: formats[value.ui_element],
            timepicker: value['ui_element'] == 'date' ? false : true,
            datepicker: value['ui_element'] == 'time' ? false : true,
            allowBlank: !value['required'],
            lazyInit: value.value ? false : true,
            initTime: true
          };
          djQ('.dave-form input[name="' + value['name'] + '"]').datetimepicker(t);
        }

        if (chatType == "normal") {
          form_error += value['error'] + "<br /><br />";
        }
      });

      if (chatType == "history") {
        /////////you have to make a count variable here
        if (count == dave_return_size) {
          djQ('.dave-cb-form form').last().append("<input type='submit' value='".concat(data.data.submit_value || "submit", "'>"));
        } else {
          djQ('.dave-cb-form form').last().append("<input type='submit' value='".concat(data.data.submit_value || "submit", "' disabled>"));
        }
      } else if (chatType == "normal") {
        var _s5 = djQ("<input type='submit' value='".concat(data.data.submit_value || "submit", "'>"));

        djQ('.dave-cb-form form').last().append(_s5);
      }

      djQ('.dave-cb-form').append("<p class='hidden'>".concat(data['data']['customer_state'], "</p>"));
      DAVE_SETTINGS.execute_custom_callback("after_form_response", [data['data'][dave_response_dataType], data['data'], data]);
    } else if (dave_response_dataType == "url") {
      DAVE_SETTINGS.execute_custom_callback("before_url_response", [data['data'][dave_response_dataType], data['data'], data]);

      var _s6 = djQ("<a href=\"".concat(encodeURI(data['data']['url']), "\" target='").concat(data['data']['target'] || '_blank', "'>").concat(data['data']['title'] || data['data']['url'], "</a>"));

      djQ('p.dave-chattext').last().append(_s6);

      _s6.attr('href', encodeURI(data['data']['url']));

      djQ(_s6).on('click', function () {
        DAVE_SETTINGS.execute_custom_callback('on_url_click');
      });

      if (data.data['sub_title']) {
        djQ('p.dave-chattext').last().append("".concat(data.data['sub_title']));
      }

      DAVE_SETTINGS.execute_custom_callback("after_url_response", [data['data'][dave_response_dataType], data['data'], data]);
    } else if (dave_response_dataType == "video") {
      DAVE_SETTINGS.execute_custom_callback("before_video_response", [data['data'][dave_response_dataType], data['data'], data]);
      djQ('p.dave-chattext').last().append("\n            <a href='".concat(data['data']['video'], "' target='").concat(data['data']['target'], "'>\n                <div class='dave-videoplayer'>\n                    <video controls width='100%' height='200px' autoplay muted><source src='").concat(data['data']['video'], "'>Your Browser Does Not Support Video Player Try With Chrome</video>\n                </div>\n            </a>\n            "));
      DAVE_SETTINGS.execute_custom_callback("after_video_response", [data['data'][dave_response_dataType], data['data'], data]);
    } else if (dave_response_dataType == "custom") {
      var data_func_name = data['data']['custom_function'];
      DAVE_SETTINGS.execute_custom_callback("before_custom_response", [data['data'][data_func_name], data['data'], data]);
      var data_func = window[data_func_name];

      if (data_func && typeof data_func == "function") {
        data_func(djQ('p.dave-chattext').last(), data["data"][data_func_name], chatType);
      }

      DAVE_SETTINGS.execute_custom_callback("after_custom_response", [data['data']['option'], data['data'], data]);
    }

    if (dave_response_dataType == "options" || data['data']['options']) {
      DAVE_SETTINGS.execute_custom_callback("before_options_response", [data['data']['option'], data['data'], data]);
      djQ('p.dave-chattext').last().append("\n                </br><ul class='dave-option-list'></ul>\n            ");
      console.debug(data['data']['options']);
      djQ.each(data['data']['options'], function (key, value) {
        if (typeof key != "string") {
          if (typeof value != "string") {
            key = value[0];
            value = value[1];
          } else {
            key = data["customer_state"] || null;
          }
        }

        djQ('.dave-option-list').last().append("<li><input type='hidden' value='".concat(key, "'><button>").concat(value, "</button></li>"));
      });
      DAVE_SETTINGS.execute_custom_callback("after_options_response", [data['data']['option'], data['data'], data]);
    }
  } //FUNCTION TO TAKE USER MESSAGE AND PRINT IN CHATBOT AND PASS AS MESSAGE TO SYSTEM


  function userchat_data() {
    if ((DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response')) && !chat_historyActivated) {
      userInputBubble('normal');
    }
    /*IF HISTORY IS LOADED THEN THIS BLOCKS GET EXECUTED FOR ONCE*/
    else if ((DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id')) && chat_historyActivated) {
        userInputBubble('history');
      }
  } //history - CHECKING AND PRINTING USER CHAT HISTORY


  function on_load_history(data) {
    DAVE_SETTINGS.execute_custom_callback("before_load_history", [data]);
    var count = 0;
    chat_loading = true;
    djQ.each(data['history'], function (his_key, his_value) {
      count++;
      djQ('.dave-cb-chatsugg').html('');

      if (his_value['direction'] == 'system') {
        dave_response_dataType = his_value['data']['response_type'];
        his_value['timestamp'] = DAVE_SETTINGS.print_timestamp(his_value['timestamp']); //print bot reply as message

        c_bubbleId = his_value['response_id'];
        c_bubbleId = "RI" + c_bubbleId;

        if (count > 1) {
          if (typeof his_value['reply_to'] == "undefined") {
            msgBubbles('bot', 'history', his_value['timestamp'], '', c_bubbleId);
          } else {
            msgBubbles('bot', 'history', his_value['timestamp'], '', c_bubbleId, his_value['response_id']);
          }
        } else {
          msgBubbles('bot', 'history', his_value['timestamp'], '');
        }

        djQ(".dave-chattext").last().html(his_value["placeholder"]);

        if (his_value['state_options'] && !djQ.isEmptyObject(his_value['state_options'])) {
          djQ(".dave-cb-chatsugg").append("<span class='chatsugg-next'><img src='".concat(cb_sugg_next, "'></span><span class='chatsugg-prev'><img src='").concat(cb_sugg_prev, "'></span>"));
        }

        response_elementPrint(dave_response_dataType, his_value, 'history', count);

        if (his_value['state_options'] && !djQ.isEmptyObject(his_value['state_options'])) {
          //print bot suggestion
          for (var _i2 = 0, _Object$entries2 = Object.entries(his_value['state_options']); _i2 < _Object$entries2.length; _i2++) {
            var _Object$entries2$_i = _slicedToArray(_Object$entries2[_i2], 2),
                key = _Object$entries2$_i[0],
                value = _Object$entries2$_i[1];

            djQ('.dave-cb-chatsugg').append("<div class='dave-cb-chatsugg-list'>".concat(value, "<input type='hidden' value='").concat(key, "'></div>"));
          }
        } //QUICKACCESS BUTTON NEXT & PREV ONLY VISIBLE ON SCROLL IS AVAILABLE


        var chatsugg_scrollWidth = djQ('.dave-cb-chatsugg')[0].scrollWidth;

        if (chatsugg_scrollWidth > chatbox_width - 20) {
          djQ('span.chatsugg-next').css('display', 'block');

          if (dave_optionTitle) {
            djQ('span.chatsugg-next').css('top', '10px');
            djQ('span.chatsugg-prev').css('top', '10px');
          } else {
            djQ('.dave-chatsuggTitle').hide();
          }
        }
      } else if (his_value['direction'] == 'other') {
        c_bubbleId = his_value['response_id'];
        c_bubbleId = "RI" + c_bubbleId;
        replyTo = "RI" + his_value['reply_to'];
        other_chat_data(null, his_value['user_icon'], his_value['user_name'], his_value['timestamp'], his_value['placeholder'], replyTo, his_value, c_bubbleId);
      } else {
        //user chat printing
        his_value['timestamp'] = DAVE_SETTINGS.print_timestamp(his_value['timestamp']);
        c_bubbleId = his_value['response_id'];
        c_bubbleId = "RI" + c_bubbleId;
        msgBubbles('user', 'history', his_value['timestamp'], DAVE_SETTINGS.pretty_print(DAVE_SETTINGS.clean_input(his_value['customer_response']), '_name_map', '_type_map'), c_bubbleId, '', his_value['customer_state']);
      } //Chat box height Adjustment according to device


      set_inner_height();

      if (count == dave_return_size) {
        //
        djQ('button#start-recording').prop('disabled', false); //

        djQ(".dave-cb-chatarea").animate({
          scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight")
        }, 500);
        dave_typingLoaderRemove('bot');
      }
    });
    dave_userchatfunc_exe = false;
    chat_historyActivated = true;
    userchat_data();
    DAVE_SETTINGS.execute_custom_callback("after_load_history", [data]);
  }

  function botchat_history() {
    dave_chatRes_loader('bot');
    set_inner_height();
    DAVE_SETTINGS.history(function (data) {
      djQ('button#start-recording').prop('disabled', true);
      dave_return_size = Object.keys(data['history']).length;

      if (dave_return_size > 0) {
        on_load_history(data);
      } else {
        //If no history available
        chat_historyActivated = false;
        botchat_data({
          "query_type": "auto"
        });
      }
    }, function (e) {
      chatResponseError('history', e);
    }, function (data) {
      dave_return_size = Object.keys(data['history']).length;

      if (dave_return_size > 0) {
        on_load_history(data);
      }
    });
  } //USER INPUT MESSAGE SENDING TO SYSTEM ON REGULAR AND HISTORY CHAT


  var temp_bubbleId = new Array();
  var perm_bubbleId = new Array();

  function userInputBubble(chatType) {
    var dave_user_typeloader = 0;
    var clicked_pred_custState;
    djQ("#dave-cb-textinput").on("input", function () {
      text_cleared = false;
      djQ("#dave-cb-usersend").off('click').on('click', function () {
        c_bubbleId = djQ.now();
        temp_bubbleId.push(c_bubbleId);
        var dave_user_inputtext = DAVE_SETTINGS.clean_input(djQ("#dave-cb-textinput").val());

        if (dave_user_inputtext.length >= 1) {
          dave_typingLoaderRemove('user');
          userTextMsgBubble(dave_user_inputtext, 'normal', dateTiming(), c_bubbleId);
          scrollChatarea(500);
          djQ(".dave-pred-contt").html('');
          djQ(".dave-pred-contt").css({
            "height": "0",
            "overflow": "hidden",
            "padding": "0"
          });

          if (!dave_predication_send) {
            botchat_data({
              'customer_response': dave_user_inputtext,
              "query_type": "type"
            }, c_bubbleId);
          } else {
            botchat_data({
              'customer_response': dave_user_inputtext,
              'customer_state': clicked_pred_custState,
              "query_type": "click"
            }, c_bubbleId);
          } //Chat box height Adjustment according to device


          set_inner_height();
          djQ("#dave-cb-textinput").val('');
          text_cleared = true;
          dave_user_typeloader = 0;

          if (chatType == 'normal') {
            stopTimmer();
          } else if (chatType == 'history') {
            chat_historyActivated = false;
          }
        }
      });
      text_input = this.value.trim().replace(/\s/g, "");
      var dave_input_length = text_input.length;
      dave_predication_send = false;

      if (dave_input_length > 0 && dave_user_typeloader == 0) {
        dave_typingLoaderRemove('user');
        dave_user_typeloader = 1;

        if (dave_user_typeloader = 1) {
          //User typing gif
          dave_chatRes_loader('user');
          scrollChatarea(500);
        }
      } else if (dave_input_length == 0) {
        dave_user_typeloader = 0;
        dave_typingLoaderRemove('user');
        djQ('.dave-pred-contt').html('');
        pred_attach = false;
      } else if (dave_input_length >= 2 && !dave_userchatfunc_exe) {
        //User click on send icon
        //user all data print as message bubble
        //user all data then send to server
        dave_prediction(this);
      }
    });
  } //SERVER RESPONSE PRINTING FUNCTION BLOCK ENDS ENDS ENDS
  //THE CHATBOT FRONT-END ELEMENTS ASSEMBLING STARTS


  djQ('#dave-settings').append("\n        <div class='dave-cb-avatar-contt'>\n            <canvas id='dave-canvas'></canvas>\n        </div>\n        <div class='dave-chatbox-cont'>\n            <div class='dave-chatbox'>\n                <div class='dave-cb-tt-sec'>\n                    <div class='dave-tt-sec1'>\n                        <div class='dave-bottitleicon'>\n                            <img src='".concat(dave_title_icon, "'>\n                        </div>\n                        <div class='dave-cb-tt'> \n                            ").concat(dave_chatTitle, " \n                        </div>\n                    </div>\n                    <div class='close-min-max'>\n                        <div id='minchat' class='dave-cb-tt-minmax'>\n                            <img src=''>\n                        </div>\n                        <div class='dave-cb-tt-cross'>\n                            <img src=").concat(cross_img, " />\n                        </div>\n                    </div>\n                </div>\n                <div class='dave-cb-chatarea'></div>\n                <div class='dave-cb-stickBottom'>\n                    <p class=\"dave-chatsuggTitle\">").concat(dave_optionTitle, "</p>\n                    <div class='dave-cb-chatsugg'></div>\n                    <div class='dave-cb-type-sec'>\n                        <div class='dave-pred-contt'></div>\n                        <div class='dave-cb-input'>\n                        ").concat(homeBtnImg ? "<button class=\"dave-homeBtn\"><img src=\"".concat(homeBtnImg, "\" alt=\"Home\"></button>") : "", "\n                            <input type='text' name='chatbot-input-box' id='dave-cb-textinput' placeholder='Type here..' autocomplete=\"off\">\n                            <div class='dave-cb-send-icon'>\n                                <button id='dave-cb-usersend'>\n                                    <img src='").concat(send_img, "' />\n                                </button>\n                            </div>\n                        </div>\n                        <div class=\"dave-mic-contt\">\n                            <button class='dave-mic-button rec-start' id='start-recording' disabled><img src=\"").concat(mic_img, "\"></button>\n                            <button class='dave-mic-button rec-stop' id='stop-recording'><img src=\"").concat(mic_img, "\"></button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class='dave-cb-icon'>\n                <img src='").concat(dave_chatbot_icon, "'>\n            </div>\n        </div>\n        ")).ready(function () {
    djQ(window).resize(function () {
      if (djQ(window).width() != dave_window_width || djQ(window).height() != dave_window_height) {
        if (!djQ('input').is(':focus')) {
          set_maximized_height();
        }
      }

      set_inner_height();
    });
    /*HE CHATBOT FRONT-END ELEMENTS ASSEMBLING ENDS*/
    //CODE BLOCK TO AVOID OVERLAPPING CHATBOX ON OTHER PAGE ELEMENTS ON CLOSE

    chatbox_width = djQ(".dave-chatbox-cont").width();
    djQ(".dave-chatbox-cont").width(0); //SPEECH-REC CODE BLOCK STARTS

    if (DAVE_SETTINGS.SPEECH_SERVER) {
      var recButton_visibility = function recButton_visibility(startRecVis) {
        if (startRecVis) {
          nowRecording = true;
          djQ("button#start-recording").hide();
          djQ("button#stop-recording").show();
        } else {
          nowRecording = false;
          djQ("button#stop-recording").hide();
          djQ("button#start-recording").show();
        }
      };

      djQ(".dave-mic-contt button").show();
      var _voice_MsgId = undefined;
      var mic_status = undefined;
      navigator.permissions.query({
        name: 'microphone'
      }).then(function (r) {
        if (r.state === 'granted') {
          DAVE_SETTINGS.micAccess = true;
          recButton_visibility();
        } else {
          DAVE_SETTINGS.micAccess = false;
        }
      });
      djQ(document).on("click", "#start-recording", function (event) {
        djQ("#dave-cb-textinput").val('');
        window.stopTimmer();
        _voice_MsgId = DAVE_SETTINGS.generate_random_string(6);

        if (!DAVE_SETTINGS.micAccess) {
          navigator.mediaDevices.getUserMedia({
            audio: true
          }).then(function (auStream) {
            DAVE_SETTINGS.micAccess = true;
            console.log('Mic Access is provided');
          }).catch(function (e) {
            console.log('No mic access is provided');
          });
        }

        if (DAVE_SETTINGS.micAccess) {
          if (DAVE_SCENE.loaded) {
            DAVE_SCENE.stop();
          }

          recButton_visibility(true);
          DAVE_SETTINGS.StreamingSpeech.onStartVoiceRecording();
        } else {
          recButton_visibility();
          alert('Please allow permission to access microphone');
          djQ("button#start-recording").attr('title', 'You have dis-allowed the microphone. Please go to settings to allow microphone for this website');
        }

        event.stopPropagation();
      });
      djQ("#stop-recording").on("click", function () {
        DAVE_SETTINGS.StreamingSpeech.onStopVoiceRecording();
        recButton_visibility();
        djQ('button#start-recording').prop('disabled', true);
      });

      DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording = function () {
        recButton_visibility();
        djQ('button#start-recording').prop('disabled', true);
      };

      DAVE_SETTINGS.StreamingSpeech.onSocketConnect = function (o) {
        if (!nowRecording) {
          recButton_visibility();
        }

        djQ('button#start-recording').prop('disabled', false);
      };

      DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable = function (data) {
        stopTimmer();

        if (!data['final_text']) {
          var voice_rawData = "";

          if (data.hasOwnProperty('rec_text')) {
            voice_rawData = JSON.stringify(data['rec_text']);
          } else {
            voice_rawData = JSON.stringify(data['recognized_speech']);
          } // let voice_rawData = JSON.stringify(data['recognized_speech']) || JSON.stringify(data['rec_text'])


          voice_rawData = voice_rawData.slice(1, -1);
          djQ("#dave-cb-textinput").val(djQ("#dave-cb-textinput").val() + " " + voice_rawData);
        } else {
          dave_chatRes_loader('user');

          var _voice_rawData = JSON.stringify(data['final_text']);

          _voice_rawData = _voice_rawData.slice(1, -1);
          console.debug(_voice_rawData + ' =====> This is final speech text');
          djQ("#dave-cb-textinput").val('');
          msgBubbles('user', 'speech', dateTiming(), _voice_rawData, _voice_MsgId, null, _voice_MsgId);
          scrollChatarea(500);
        }
      };

      DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable = function (data) {
        if (!data['final_text']) {
          var voice_rawData = JSON.stringify(data['rec_text']);
          voice_rawData = voice_rawData.slice(1, -1);
          djQ("#dave-cb-textinput").val(djQ("#dave-cb-textinput").val() + " " + voice_rawData);
          console.debug(voice_rawData + " =====> this is in-time rec text");
        }
      };

      DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable = function (data) {
        if (data.hasOwnProperty('conv_resp')) {
          data = data['conv_resp'];
        } else {
          data = data['conversation_api_response'];
        }

        var v_returnSize = Object.keys(data).length;

        if (v_returnSize > 2) {
          djQ("input[value=\"".concat(_voice_MsgId, "\"]")).val(data['response_id']);
          _voice_MsgId = data['response_id'];
          response_print(data, v_returnSize, dateTiming(), "speech", _voice_MsgId);
        } else {
          c_bubbleId = djQ.now();
          userTextMsgBubble("---Silence--Noise---", 'normal', dateTiming(), c_bubbleId);
          msgBubbles('bot', 'normal', dateTiming(), 'Voice Recognition Failed! Please check your microphone settings and try again');
          scrollChatarea(500);
        }

        djQ('button#start-recording').prop('disabled', false);
      };

      DAVE_SETTINGS.StreamingSpeech.onError = function (data) {
        console.error("Speech onError is generated");
        msgBubbles('bot', 'normal', dateTiming(), 'Failed to get a response! Please try with a different query');
        scrollChatarea(500);

        if (typeof window.startTimmer == 'function') {
          window.startTimmer();
        }

        djQ('button#start-recording').prop('disabled', false);
      };

      DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect = function (o) {
        recButton_visibility();

        if (typeof window.startTimmer == 'function') {
          window.startTimmer();
        }

        djQ('button#start-recording').prop('disabled', false);
      };

      djQ(document).on("click", ".voice-thumbsup, .voice-thumbsdown", function () {
        var curr_rating = djQ(this).attr('data-thumbtype');
        var v_feedbackResId = djQ(this).siblings('input[type="hidden"]').val();
        like_dislike(this, curr_rating, "speech");

        if (v_feedbackResId) {
          DAVE_SETTINGS.feedback({
            "recognition_rating": l_d_react
          }, v_feedbackResId, function () {});
        }
      });
    } else {
      console.log("Speech Server not set.");
      djQ(".dave-mic-contt button").hide();
    }
    /*SPEECH CODE BLOCK ENDS*/
    //FUNTION TO PRINT ALL BOT RESPONSES IN CHAT-BOX PROVIDED BY DAVE_SETTINGS.chat


    window.botchat_data = function botchat_data(params, bubbleId, behaviour, nudgeopenState, lockHeight) {
      //bot chat typing gif
      if (close_count > 0) {
        return;
      }

      dave_chatRes_loader('bot');
      djQ('button#start-recording').prop('disabled', true); //pass user input to chat function

      params = params || {};
      behaviour = behaviour || false;
      nudgeopenState = nudgeopenState || false;
      lockHeight = lockHeight || maxLockHeight; //botchat_data({ 'customer_state': dave_clicked_sugg_value, 'customer_response': dave_clicked_sugg_message, "query_type": "click" }, c_bubbleId);

      DAVE_SETTINGS.chat(params, function (data) {
        var dave_return_size = Object.keys(data).length;
        response_print(data, dave_return_size, dateTiming('date'), "normal");
      }, function (e) {
        chatResponseError('normal', e, params, bubbleId, behaviour, nudgeopenState, lockHeight);
      });

      if (DAVE_SCENE.is_mobile == true) {
        nudgeopenState = 'max';
      }

      if (behaviour == 'open') {
        djQ('.dave-cb-icon').trigger('click');

        if (nudgeopenState == 'min') {
          if (chatboxOpenStatus == 'close' || chatboxOpenStatus == 'min') {
            minimize_chatbox(lockHeight);
          }
        } else if (nudgeopenState == 'max') {
          maximize_chatbox();
        }
      }
    }; //QUICK ACCESS BUTTON CODE BLOCK


    djQ(document).on("click", ".dave-cb-chatsugg-list", function () {
      stopTimmer();
      var dave_clicked_sugg_value = djQ(this).children().val();
      var dave_clicked_sugg_message = djQ(this).text();
      c_bubbleId = djQ.now();
      temp_bubbleId.push(c_bubbleId);
      userTextMsgBubble(dave_clicked_sugg_message, 'normal', dateTiming(), c_bubbleId);
      scrollChatarea(500);
      djQ("#dave-cb-textinput").val('');
      botchat_data({
        'customer_state': dave_clicked_sugg_value,
        'customer_response': dave_clicked_sugg_message,
        "query_type": "click"
      }, c_bubbleId);
    }); //OPITONS CODE BLOCK (IN MSG-BUBBLE)

    djQ(document).on("click", ".dave-option-list li button", function () {
      stopTimmer();
      var curr_clicked_option_key = djQ(this).prev().val();
      var curr_clicked_option_val = djQ(this).text();
      c_bubbleId = djQ.now();
      temp_bubbleId.push(c_bubbleId);
      userTextMsgBubble(curr_clicked_option_val, 'normal', dateTiming(), c_bubbleId);
      scrollChatarea(500);
      djQ("#dave-cb-textinput").val('');
      botchat_data({
        "customer_state": curr_clicked_option_key,
        "customer_response": curr_clicked_option_val,
        "query_type": "click"
      }, c_bubbleId);
    }); //If there is not cookie set for user and system then call botchat_data function

    if ((DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id')) && !(DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response'))) {
      botchat_history();
      DAVE_SETTINGS.dave_eng_id_status = true;
    } else if (!(DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id')) && !(DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response'))) {
      DAVE_SETTINGS.dave_eng_id_status = true;
      console.debug(DAVE_SETTINGS.getCookie('dave_engagement_id'));
      botchat_data({
        "query_type": "auto"
      });
    } else if (!DAVE_SETTINGS.dave_eng_id_status) {
      djQ('.dave-cb-stickBottom').css('display', 'block');
      botchat_history();
      DAVE_SETTINGS.dave_eng_id_status = true;
    } //Open Chat Box using Chatbox Icon


    djQ(".dave-cb-icon").on("click", function () {
      if (DAVE_SETTINGS.on_maximize() === false) {
        return;
      }

      djQ(".dave-cb-tt-cross").removeAttr('disabled');
      djQ(".dave-chatbox").addClass("dave-chatbox-open");
      djQ(".dave-cb-icon").addClass("dave-cb-icon-hide");
      djQ(".dave-chatbox-cont").width(chatbox_width);
      djQ(".dave-cb-tt-minmax img").remove();
      djQ(".dave-cb-tt-minmax").append("<img src='".concat(min_img, "'>"));
      chatboxOpenStatus = "open";
      set_maximized_height();
      djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
        minimize_chatbox();
      });
      djQ(".dave-cb-chatarea").animate({
        scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight")
      }, 500);
      dave_chat_heightchange = true;

      if (dave_window_width < 450) {
        djQ('body').css("overflow", "hidden");
      }

      if (DAVE_SCENE.loaded) {
        showAvatar(true);
      }
    }); //Send user data to chat box using Enter Button

    djQ('#dave-cb-textinput').off('keypress').on('keypress', function (e) {
      var daveInput_length = this.value.length;

      if (daveInput_length >= 1 && !dave_userchatfunc_exe) {
        if (e.which == 13) {
          djQ("#dave-cb-usersend").trigger('click');
          e.preventDefault();
        }
      }
    }); //Close chatbox using Cross "close" sign

    djQ(".dave-cb-tt-cross").click(function () {
      //feedback on close
      if (djQ(this).attr('disabled')) {
        return;
      }

      close_count++;
      now = new Date().getTime();
      previous_time = parseInt(DAVE_SETTINGS.getCookie('dave_previous_feedback_time')) || data_feedback_redo_time * 1000;
      time_diff = Math.max(now - previous_time, 0);

      if (DAVE_SCENE.loaded || data_avatar_initChk) {
        showAvatar(false);
      }

      if (time_diff >= data_feedback_redo_time * 1000 && chat_loading && close_count < 2 && DAVE_SETTINGS.did_interact) {
        stopTimmer();
        maximize_chatbox();
        djQ(".dave-cb-tt-minmax").unbind("click");
        var userfull_rate_value = 0;
        var accuracy_rate_value = 0;
        djQ('.dave-cb-chatarea').html('');
        djQ('.dave-cb-chatarea').css('padding', '0');
        djQ('.dave-cb-stickBottom').css('display', 'none');
        djQ('.dave-cb-chatarea').append("\n                            <div class=\"rating-star-bg\"></div>\n                            <div class='rating-star-contt'>\n                                ".concat(WIH_RatingTitle != "__NULL__" ? "\n                                            <p>".concat(WIH_RatingTitle || "Was I Helpful?", "</p>\n                                            <div class=\"dave-rate feedback-row-box\" id='dave-userfull-rate'>\n                                                <input type=\"radio\" id=\"dave-usefull-star5\" name=\"dave-usefull-rate\" value=\"5\" />\n                                                <label for=\"dave-usefull-star5\" title=\"dave-usefull-text\">5 stars</label>\n                                                <input type=\"radio\" id=\"dave-usefull-star4\" name=\"dave-usefull-rate\" value=\"4\" />\n                                                <label for=\"dave-usefull-star4\" title=\"dave-usefull-text\">4 stars</label>\n                                                <input type=\"radio\" id=\"dave-usefull-star3\" name=\"dave-usefull-rate\" value=\"3\" />\n                                                <label for=\"dave-usefull-star3\" title=\"dave-usefull-text\">3 stars</label>\n                                                <input type=\"radio\" id=\"dave-usefull-star2\" name=\"dave-usefull-rate\" value=\"2\" />\n                                                <label for=\"dave-usefull-star2\" title=\"dave-usefull-text\">2 stars</label>\n                                                <input type=\"radio\" id=\"dave-usefull-star1\" name=\"dave-usefull-rate\" value=\"1\" />\n                                                <label for=\"dave-usefull-star1\" title=\"dave-usefull-text\">1 star</label>\n                                            </div>\n                                        ") : "", "\n                                ").concat(WIA_RatingTitle != "__NULL__" ? "\n                                            <p>".concat(WIA_RatingTitle || "Was I Accurate?", "</p>\n                                            <div class=\"dave-rate feedback-row-box\" id='dave-accuracy-rate'>\n                                                <input type=\"radio\" id=\"dave-accuracy-star5\" name=\"dave-accuracy-rate\" value=\"5\" />\n                                                <label for=\"dave-accuracy-star5\" title=\"dave-accuracy-text\">5 stars</label>\n                                                <input type=\"radio\" id=\"dave-accuracy-star4\" name=\"dave-accuracy-rate\" value=\"4\" />\n                                                <label for=\"dave-accuracy-star4\" title=\"dave-accuracy-text\">4 stars</label>\n                                                <input type=\"radio\" id=\"dave-accuracy-star3\" name=\"dave-accuracy-rate\" value=\"3\" />\n                                                <label for=\"dave-accuracy-star3\" title=\"dave-accuracy-text\">3 stars</label>\n                                                <input type=\"radio\" id=\"dave-accuracy-star2\" name=\"dave-accuracy-rate\" value=\"2\" />\n                                                <label for=\"dave-accuracy-star2\" title=\"dave-accuracy-text\">2 stars</label>\n                                                <input type=\"radio\" id=\"dave-accuracy-star1\" name=\"dave-accuracy-rate\" value=\"1\" />\n                                                <label for=\"dave-accuracy-star1\" title=\"dave-accuracy-text\">1 star</label>\n                                            </div>\n                                        ") : "", "\n                                ").concat(writeFeedback ? "<div class='feedback-row-box'><textarea id='dave-feedback-write' placeholder='Write Your Feedback'></textarea></div>" : '', "\n                                <div class='feedback-row-box'><button id='dave-final-feedbackButton' tittle='Submit'>Submit</button></div>\n                            </div>\n                        "));

        if (WIH_RatingTitle != "__NULL__") {
          djQ('#dave-userfull-rate input').on('change', function () {
            if (djQ(this).is(':checked')) {
              userfull_rate_value = djQ(this).val();
            }
          });
        }

        if (WIA_RatingTitle != "__NULL__") {
          djQ('#dave-accuracy-rate input').on('change', function () {
            if (djQ(this).is(':checked')) {
              accuracy_rate_value = djQ(this).val();
            }
          });
        }

        djQ('#dave-final-feedbackButton').on("click", function () {
          djQ(this).prop("disabled", true);
          DAVE_SETTINGS.execute_custom_callback("before_submit_feedback");
          djQ(".dave-error").remove();
          var dave_feedback_write = djQ('#dave-feedback-write').val() || null;
          djQ('.rating-star-contt .dave-error .dave-valid').remove();

          if (WIA_RatingTitle == "__NULL__") {
            accuracy_rate_value = null;
          }

          if (WIH_RatingTitle == "__NULL__") {
            userfull_rate_value = null;
          }

          if ((WIA_RatingTitle == '__NULL__' || accuracy_rate_value > 0) && (WIH_RatingTitle == "__NULL__" || userfull_rate_value > 0)) {
            djQ(".dave-cb-tt-cross").attr('disabled', true);
            var fdbk = {
              "usefulness_rating": userfull_rate_value,
              "accuracy_rating": accuracy_rate_value,
              "feedback": dave_feedback_write
            };
            DAVE_SETTINGS.feedback(fdbk, null, function (data) {
              DAVE_SETTINGS.dave_eng_id_status = false;
              djQ('.dave-error').remove();
              djQ('.dave-valid').remove();
              djQ('.rating-star-contt').append("<p class='dave-valid'>".concat(feedback_successMSG, "</p>"));
              DAVE_SETTINGS.execute_custom_callback("after_submit_feedback", [fdbk]);
            });
            setTimeout(function () {
              if (DAVE_SETTINGS.on_close() === false) {
                return;
              }

              chatboxOpenStatus = 'close';
              stopTimmer();
              close_count = 0;
              djQ('.dave-cb-chatarea').html('');
              djQ(".dave-chatbox").removeClass("dave-chatbox-open");
              djQ(".dave-cb-icon").removeClass("dave-cb-icon-hide");
              djQ('body').css('overflow', originalScroll);
              console.log("set back original scroll", originalScroll);
              djQ(".dave-chatbox-cont").width(0);
              djQ('.dave-cb-chatarea').css('padding', "10px 15px 0px 15px");
              DAVE_SETTINGS.setCookie('dave_previous_feedback_time', now);
              djQ(this).prop("disabled", false);

              if (!DAVE_SETTINGS.dave_eng_id_status) {
                djQ('.dave-cb-stickBottom').css('display', 'block');
                botchat_history();
                DAVE_SETTINGS.dave_eng_id_status = true;
              }

              DAVE_SETTINGS.execute_custom_callback("after_submit_feedback_close");
              DAVE_SETTINGS.execute_custom_callback("after_close_custom");
            }, 2000);
          } else {
            djQ(this).prop("disabled", false);
            djQ('body').css('overflow', originalScroll);
            console.log("set back original scroll", originalScroll);
            djQ('.dave-error').remove();
            djQ('.dave-valid').remove();
            djQ('.rating-star-contt').append("<p class='dave-error'>Please Do Give Star Ratings!</p>");
          }
        });
      } else {
        chatboxOpenStatus = 'close';
        DAVE_SETTINGS.dave_eng_id_status = false;

        if (DAVE_SETTINGS.on_close() === false) {
          return;
        }

        ;

        if (typeof startTimmer === "function") {
          startTimmer();
        }

        djQ('body').css('overflow', originalScroll);
        console.log("set back original scroll", originalScroll);
        close_count = 0;
        djQ('.dave-cb-chatarea').html('');
        djQ('.dave-cb-chatarea').css('padding', '');
        djQ('.dave-cb-stickBottom').css('display', 'none');
        djQ(".dave-chatbox-cont").width(0);
        djQ(".dave-chatbox").removeClass("dave-chatbox-open");
        djQ(".dave-cb-icon").removeClass("dave-cb-icon-hide");

        if (!DAVE_SETTINGS.dave_eng_id_status) {
          djQ('.dave-cb-stickBottom').css('display', 'block');
          botchat_history();
          DAVE_SETTINGS.dave_eng_id_status = true;
        }

        DAVE_SETTINGS.execute_custom_callback("after_close_custom");
      } // djQ(".dave-cb-tt-minmax").one("click", function () {
      //     minimize_chatbox(maxLockHeight, 'feedback');
      // });

      /*if (dave_window_width > 450) { djQ(".dave-chatbox-cont").css("width", "5%"); } else { djQ(".dave-chatbox-cont").css("width", "25%"); }*/

    }); //Chat Suggestion Next and Prev Button
    //NEXT BUTTON 

    djQ(".dave-cb-chatsugg").on("click", ".chatsugg-next", function () {
      djQ(".dave-cb-chatsugg").animate({
        scrollLeft: "+=50px"
      }, 300);
      djQ(".dave-cb-chatsugg").on("scroll", function () {
        djQ("span.chatsugg-prev").css("display", "block");

        if (djQ(this).scrollLeft() + djQ(this).innerWidth() >= djQ(this)[0].scrollWidth) {
          djQ("span.chatsugg-next").css("display", "none");
        } else if (djQ(this).scrollLeft() == 0) {
          djQ("span.chatsugg-prev").css("display", "none");
        }
      });
    }); //PREV BUTTON

    djQ(".dave-cb-chatsugg").on("click", ".chatsugg-prev", function () {
      djQ(".dave-cb-chatsugg").animate({
        scrollLeft: "-=50px"
      }, 300);
      djQ(".dave-cb-chatsugg").on("scroll", function () {
        djQ("span.chatsugg-next").css("display", "block");

        if (djQ(this).scrollLeft() == 0) {
          djQ("span.chatsugg-prev").css("display", "none");
        } else if (djQ(this).scrollLeft() + djQ(this).innerWidth() >= djQ(this)[0].scrollWidth) {
          djQ("span.chatsugg-prev").css("display", "block");
        }
      });
    }); //THUMBS FEEDBACK FOR BUBBLE

    djQ(document).on('click', '.dave-thumbsup, .dave-thumbsdown', function () {
      eng_id = djQ(this).siblings('span').text();
      curr_rating = djQ(this).attr('data-thumbType');
      like_dislike(this, curr_rating, "normal");
      DAVE_SETTINGS.feedback({
        "response_rating": l_d_react
      }, eng_id, function (data) {});
      djQ(this).closest('.dave-botchat').append("<p class=\"dave-feedback-msg\">Thank you for your feedback</p>");
    }); // Form File Upload

    djQ(document).on("change", ".file_upload", function (data) {
      //add file uploading loader here
      djQ(this).before("<div class='file_upload_loader'><img src='".concat(typing_gif, "'></div>"));

      if (data.target.value.length > 0) {
        var curr_file = djQ(this);
        var maxUploadSize = djQ(this).attr('data-max') || 1024 * 1024 * 1024 * 0.5;
        var selectedFiles = data.target.files[0];
        var selectedFileName = data.target.files[0].name;
        var selectedFileType = data.target.files[0].type;
        var selectedFileSize = data.target.files[0].size;
        selectedFileSize = selectedFileSize / (1024 * 1024);
        var selectedFilePath = djQ(this).val();
        var fileReader = new FileReader();

        fileReader.onloadend = function (data) {
          var arrayBuffer = data.target.result;
          var blob = blobUtil.arrayBufferToBlob(arrayBuffer, selectedFileType);
          console.debug("File blob is:");
          console.debug(blob);

          if (selectedFileSize <= maxUploadSize) {
            if (selectedFileSize && selectedFilePath && selectedFileName) {
              DAVE_SETTINGS.upload_file(blob, selectedFileName, function (data) {
                console.debug("uploaded file response" + data); //remove loader

                var fileInputNameAttr = curr_file.attr('name');
                djQ(curr_file).data('fileUrl', data['path']);
                curr_file.prev(".file_upload_loader").remove();
                console.debug("upload_file function executed"); //file uploaded successfully

                curr_file.closest('form').find('.dave-error').remove();
                djQ(curr_file).data('fileUploadedStatus', 'true');
              }, function (e) {
                //remove loader and show error
                curr_file.prev().remove();
                curr_file.closest('form').find('.dave-error').remove();
                curr_file.closest('form').append("<p class='dave-error'>Something went wrong while upload file!</p>");
                scrollChatarea(500);
                djQ(curr_file).attr('fileUploadedStatus', 'false');
                console.error("The error while uploading file=> " + e);
              });
            }
          } else {
            //file size exceed error message
            console.error("File Size Exceed");
            curr_file.closest('form').find('.dave-error').remove();
            curr_file.closest('form').append("<p class='dave-error'>File size exceed the maximum allowed size of ".concat(djQ(this).attr('data-max'), " MB!</p>"));
            scrollChatarea(500);
          }
        };

        fileReader.readAsArrayBuffer(selectedFiles);
      } else {
        console.debug("No file selected now");
      }
    }); //FORM SUBMITTING CODE BLOCK

    djQ(document).on('submit', 'form.dave-form', function (e) {
      var formData = {};
      var printData = {};
      var nameMap = {};
      var typeMap = {};
      var dataValidated = true;
      var f_bubbleId = djQ(this).closest('.dave-chattext').prev('.scrollToMsg').attr('data-scrollmsgid');
      var c_form = djQ(this);
      var form_error = '';

      if (djQ(this).length <= 1 && map_enabled) {
        if (typeof userPositon['lat'] == "undefined" || typeof userPositon['lng'] == "undefined") {
          dataValidated = false;
          form_error += djQ(this).prev().attr('error') || 'Input for "' + djQ(this).prev().attr('title') + '" is required!' + '<br/><br/>';
        } else {
          formData['lat'] = userPositon['lat'].toString();
          formData['lng'] = userPositon['lng'].toString();
          printData['lat'] = userPositon['lat'].toString();
          printData['lng'] = userPositon['lng'].toString();
          printData = JSON.stringify(printData);
          console.debug(printData);
          console.warn(formData);
        }
      }

      djQ.each(djQ(this).serializeArray(), function (i, j) {
        var k = djQ(c_form).find("input[name='" + j.name + "']").first();

        if (!k.length) {
          k = djQ(c_form).find("textarea[name='" + j.name + "']").first();
        }

        if (!k.length) {
          k = djQ(c_form).find("select[name='" + j.name + "']").first();
        }

        if ((j.value == "" || j.value == null || j.value == undefined) && k.attr('required') && !map_enabled) {
          dataValidated = false;
          form_error += k.attr('error') || 'Input for "' + k.attr('title') + '" is required!' + '<br/><br/>';
        } else if (map_enabled) {
          if (j.value == "" || j.value == null || j.value == undefined || typeof userPositon['lat'] == "undefined" || typeof userPositon['lng'] == "undefined") {
            dataValidated = false;
            form_error += k.attr('error') || 'Input for "' + k.attr('title') || "form" + '" is required!' + '<br/><br/>';
          }
        } else if (k.attr('ui_element') == 'datetime' && j.value) {
          var a = new Date(k.datetimepicker('getValue'));
          var b = new Date(k.attr('min'));
          var c = new Date(k.attr('max'));

          if (b.toLocaleString() == 'Invalid Date') {
            b = new Date(-8640000000000000);
          }

          if (c.toLocaleString() == 'Invalid Date') {
            c = new Date(8640000000000000);
          }

          if (!(b <= a && c >= a)) {
            dataValidated = false;
            form_error += 'Input for "' + k.attr('title') + '" should be between ' + DAVE_SETTINGS.print_timestamp(b, null, true) + ' and ' + DAVE_SETTINGS.print_timestamp(c, null, true) + '!<br/><br/>';
          } else {
            // If print_timestamp can print the original string taken, then we are good
            j.print_value = a.toString();
          }
        } else if (k.attr('ui_element') == 'date' && j.value) {
          var _a = new Date(k.datetimepicker('getValue')); // If print_timestamp can print the original string taken, then we are good


          j.print_value = _a.toString();
        } else if (k.attr('ui_element') == 'select' && j.value) {
          j.print_value = djQ(k).find("option:selected").text();
        } else if (k.attr('type') == 'file') {
          if (k.data('fileUploadedStatus') == 'false') {
            dataValidated = false;
            form_error += k.attr('title') + ':-' + (k.attr('error') || 'Error in uploading file!') + '<br/><br/>';
          } else if (k.attr('required')) {
            dataValidated = false;
            form_error += k.attr('error') || 'Input for "' + k.attr('title') + '" is required!' + '<br/><br/>';
          } else {
            j.value = k.data('fileUrl');
          }
        }

        if (j.value && k.attr('validate')) {
          j.value = j.value.trim();
          var reg = new RegExp('\\b' + k.attr('validate') + '\\b');

          if (!reg.test(j.value)) {
            dataValidated = false;
            form_error += k.attr('title') + ':-' + (k.attr('error') || 'Value does not match expected pattern!') + '<br/><br/>';
          }
        }

        if (map_enabled) {
          formData['lat'] = userPositon['lat'];
          formData['lng'] = userPositon['lng'];
        }

        formData[j.name] = j.value;
        printData[j.name] = j.print_value || j.value;

        if (k.attr('title')) {
          nameMap[j.name] = k.attr('title');
        }

        if (k.attr('ui_element')) {
          typeMap[j.name] = k.attr('ui_element');
        }
      });
      djQ(this).find("input[type=file]").each(function () {
        var k = djQ(this);

        if (k.data('fileUploadedStatus') == 'false') {
          dataValidated = false;
          form_error += (k.attr('title') || k.attr('name')) + ":- " + k.attr('error') || 'Error in uploading file! <br/><br/>';
        } else if (k.data('fileUploadedStatus') == 'true') {
          formData[k.attr('name')] = k.data('fileUrl');

          if (k.attr('title')) {
            nameMap[k.attr('name')] = k.attr('title');
          }

          if (k.attr('ui_element')) {
            typeMap[k.attr('name')] = k.attr('ui_element');
          }
        } else if (k.attr('required')) {
          dataValidated = false;
          form_error += k.attr('error') || 'Input for "' + (k.attr('title') || k.attr('name')) + '" is required!' + '<br/><br/>';
        }
      });

      if (dataValidated) {
        formData['_name_map'] = nameMap;
        formData['_type_map'] = typeMap;
        c_bubbleId = djQ.now();
        temp_bubbleId.push(c_bubbleId);
        userTextMsgBubble(DAVE_SETTINGS.pretty_print(printData, nameMap, typeMap), 'normal', dateTiming(), c_bubbleId);
        djQ(this).find('.dave-error').remove();
        customerState = djQ(this).next().text();
        botchat_data({
          "customer_response": JSON.stringify(formData),
          "customer_state": customerState,
          "query_type": "click"
        }, c_bubbleId);
        DAVE_SETTINGS.execute_custom_callback('on_form_submit', [formData, customerState]);
        djQ(this).find('input[type="submit"]').prop('disabled', true);
        djQ(this).find('input').prop('disabled', true);
        djQ(this).find('textarea').prop('disabled', true);
        djQ(this).find('select').prop('disabled', true);
        scrollChatarea(500);
      } else {
        djQ(this).find('.dave-error').remove();
        djQ(this).append("<p class='dave-error'>".concat(form_error, "</p>"));
        scrollChatarea(500);
      }

      e.preventDefault();
    });
    dave_nudge(); //End of dave settings env
  });
  DAVE_SETTINGS.execute_custom_callback("after_load_chatbot");
};

djQ(document).ready(function () {
  DAVE_SETTINGS.botchat_data = window.botchat_data;

  if (djQ("#dave-settings").length) {
    window.dave_load_chatbot();
  }
});
/**
 * ##### DAVE_SETTINGS.register_nudge
 * Registering nudges based on activity on the webpage.
 * @param {jQuery} identifier   element to bind the action event to
 * @param {string} identifier (jquery selector) element to bind the action event to
 * @param {string} customer_state
 * @param {string} customer_response
 * @param {string} event_type   The the jQuery event type on which to trigger the nudge, defaults to 'click'
 * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
 * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
 * @param {object} data    Any extra data to be sent to chatbot 
 *
 * e.g. DAVE_SETTINGS.register_nudge("#MyId", "cs_nudge_state", "Nudge me here", "click", "max", null, {"my_new_data": "this data"})
 */

DAVE_SETTINGS.register_nudge = function register_nudge(identifier, customer_state, customer_response, event_type, nudgeopenState, lockHeight, data) {
  event_type = event_type || 'click';
  data = data || null;
  customer_response = customer_response || DAVE_SETTINGS.toTitleCase(customer_state);
  nudgeopenState = nudgeopenState || 'min';
  djQ(identifier).on(event_type, function () {
    DAVE_SETTINGS.create_event(customer_state);
    botchat_data({
      'customer_state': customer_state,
      "customer_response": customer_response,
      "query_type": "auto",
      "temporary_data": data
    }, null, 'open', nudgeopenState, lockHeight);
  });
};

djQ(document).ready(function () {
  if (DAVE_HELP) {
    /**
     * ##### DAVE_HELP.register_help_nudge
     * Registering help nudges based on activity on the webpage. The help text opens up. When we click on the help text, a nudge is sent
     * @param {jQuery} identifier   element to bind the action event to
     * @param {string} identifier (jquery selector) element to bind the action event to
     * @param {string} help_text  Text show in the help pop-up
     * @param {string} customer_state
     * @param {string} customer_response
     * @param {string} event_type   The the jQuery event type on which to trigger the nudge, defaults to 'click'
     * @param {string} help_sub_text  Sub-Text to show in the help pop-up, not required, defaults to "Click here for more help"
     * @param {jQuery} anchor_dom  element relative to which the help text should occur, defaults to 'dom' if evaluates to false
     * @param {string} anchor_dom (jquery selector) element relative to which the help text should occur, defaults to 'dom'
     * @param {object} pos  {position: right/left/above/below, length: distance from dom, xaway: x distance from dom, yaway: y distance form dom}
     * @param {string} pos  right/left/above/below
     * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
     * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
     * @param {object} data    Any extra data to be sent to chatbot 
     *
     * e.g. DAVE_HELP.register_help_nudge("#MyId", "About this topic", "cs_nudge_state", "Nudge me here", "click", "I need more info", "above", "#MyOtherElement", 5000, 'max', "450px", {"other_data": "this other data"})
     */
    DAVE_HELP.register_help_nudge = function register_help_nudge(identifier, help_text, customer_state, customer_response, event_type, help_sub_text, pos, anchor_dom, timeout, nudgeopenState, lockHeight, data) {
      customer_response = customer_response || DAVE_SETTINGS.toTitleCase(customer_state);
      nudgeopenState = nudgeopenState || 'max';
      DAVE_HELP.bind_help_for_element(identifier, help_text, help_sub_text || "Click here for more help", event_type, pos, anchor_dom, timeout, function (elem) {
        DAVE_SETTINGS.create_event(customer_state);
        botchat_data({
          'customer_state': customer_state,
          "customer_response": customer_response,
          "query_type": "auto",
          "temporary_data": data || null
        }, null, 'open', nudgeopenState, lockHeight);
      });
    };
  }
}); // Creates a passive nudge

/**
 * ##### DAVE_SETTINGS.register_passive_nudge
 * Registering nudge to open the chat when there is no activity by the user or the user is passive
 * @param {number} timout   time in milliseconds of user passivity which will trigger this nudge
 * @param {string} customer_state
 * @param {string} customer_response
 * @param {array} cancel_events     Any events we have registered, which will cancel this passive nudge.
 * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
 * @param {string} reset_events  JQuery sequence of events which will cause the timer to be reset, default to most of the common activity events
 * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
 * @param {object} data    Any extra data to be sent to chatbot 
 *
 * e.g. DAVE_SETTINGS.register_passive_nudge(20000, "cs_nudge_state", "Nudge me here", ["User's Next Event", "User's possible event"], 'min', 'touchstart click keydown scroll dblclick resize', "450px", {'extra_data': 'extra_data'} )
 */

DAVE_SETTINGS.register_passive_nudge = function register_passive_nudge(timeout, customer_state, customer_response, cancel_events, nudgeopenState, reset_events, lockHeight, random_string, data) {
  customer_response = customer_response || DAVE_SETTINGS.toTitleCase(customer_state);
  nudgeopenState = nudgeopenState || 'min';
  cancel_events = DAVE_SETTINGS.makeList(cancel_events);
  random_string = random_string || DAVE_SETTINGS.generate_random_string(6); //if ( cancel_events ) {
  //    reset_events = reset_events || '__NULL__';
  //} else {
  //    reset_events = reset_events || 'touchstart click keydown scroll dblclick resize';
  //}

  reset_events = reset_events || 'touchstart click keydown scroll dblclick resize';
  console.log("Starting timer for customer state " + customer_state + " - " + timeout);

  if (!DAVE_SETTINGS.event_timers) {
    DAVE_SETTINGS.event_timers = {};
  }

  var t = random_string + customer_state;

  if (DAVE_SETTINGS.event_timers[t]) {
    console.log("Same passive nudge found already, so clearing the timer");
    clearTimeout(DAVE_SETTINGS.event_timers[t]);
  }

  DAVE_SETTINGS.event_timers[t] = setTimeout(function () {
    console.log("Timeout done for customer state " + customer_state);
    delete DAVE_SETTINGS.event_timers[t];
    botchat_data({
      'customer_state': customer_state,
      "customer_response": customer_response,
      "query_type": "auto",
      "temporary_data": data || null
    }, null, 'open', nudgeopenState, lockHeight);
  }, timeout);

  if (cancel_events && DAVE_SETTINGS.EVENT_MODEL) {
    if (!DAVE_SETTINGS.cancel_events) {
      DAVE_SETTINGS.cancel_events = {};
    }

    var _iterator = _createForOfIteratorHelper(cancel_events),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var k = _step.value;

        if (!DAVE_SETTINGS.cancel_events[k]) {
          DAVE_SETTINGS.cancel_events[k] = [];
        }

        DAVE_SETTINGS.cancel_events[k].push(t);
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  }

  if (reset_events != "__NULL__") {
    djQ(document).one(reset_events, function (e) {
      if (e.type == 'keydown' && (e.which < 30 || e.which > 90 && e.which < 96 || e.which > 105)) {
        return true;
      }

      if (DAVE_SETTINGS.event_timers && DAVE_SETTINGS.event_timers[t]) {
        console.debug("Restarting timer for customer state " + customer_state + " - " + timeout);
        clearTimeout(DAVE_SETTINGS.event_timers[t]);
        delete DAVE_SETTINGS.event_timers[t];
        setTimeout(function () {
          DAVE_SETTINGS.register_passive_nudge(timeout, customer_state, customer_response, cancel_events, nudgeopenState, reset_events, lockHeight, random_string, data);
        }, timeout);
      }

      return true;
    });
  }
}; // Binds a passive nudge

/**
 * ##### DAVE_SETTINGS.bind_passive_nudge
 * Bind the registering of a passive nudge to a user event. After the event occurs, and no other activity occurs after this event, then the chat would open with the given customer_state
 * @param {jQuery} identifier   element to bind the action event to
 * @param {string} identifier (jquery selector) element to bind the action event to
 * @param {number} timout   time in milliseconds of user passivity which will trigger this nudge
 * @param {string} customer_state
 * @param {string} customer_response
 * @param {string} event_type   The the jQuery event type on which to trigger the registration of the nudge, defaults to 'click'
 * @param {array}  cancel_events     Any dave events we have registered, which will cancel this passive nudge.
 * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
 * @param {string} reset_events  JQuery sequence of events which will cause the timer to be reset, default to most of the common activity events
 * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
 * @param {object} data    Any extra data to be sent to chatbot 
 *
 * e.g. DAVE_SETTINGS.bind_passive_nudge("#MyId", 20000, "cs_nudge_state", "Nudge me here", "click", ["User's Next Event", "User's possible event"]], 'min', 'touchstart click keydown scroll dblclick resize', "450px", {'extra_data': 'extra_data'} )
 */


DAVE_SETTINGS.bind_passive_nudge = function initiate_passive_nudge(identifier, timeout, customer_state, customer_response, event_type, cancel_events, nudgeopenState, reset_events, lockHeight, data) {
  event_type = event_type || 'click';
  djQ(identifier).on(event_type, function () {
    DAVE_SETTINGS.register_passive_nudge(timeout, customer_state, customer_response, cancel_events, nudgeopenState, reset_events, lockHeight, identifier + event_type, data);
  });
};
