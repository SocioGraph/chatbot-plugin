let DAVE_ASSET_PATH = "https://chatbot-plugin.iamdave.ai/";
if (typeof (DAVE_PLUGIN_ASSET_PATH) != 'undefined') {
    DAVE_ASSET_PATH = DAVE_PLUGIN_ASSET_PATH;
} else if (document.currentScript && isFinite(document.currentScript.src.split('/').slice(-2, -1)[0])) {
    DAVE_ASSET_PATH = (document.currentScript && (document.currentScript.src.split('/').slice(0, -4).join('/') + '/'));
} else {
    DAVE_ASSET_PATH = (document.currentScript && (document.currentScript.src.split('/').slice(0, -3).join('/') + '/')) || "https://chatbot-plugin.iamdave.ai/";
}

function gm_authFailure() {
    document.getElementById("map").style.display = "none";
    console.warn("MAP API KEY IS NOT WORKING ");
    handleMapError();
};
djQ.fn.ForcePhoneNumbersOnly = function () {
    return this.each(function () {
        let that = this;
        djQ(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            if (e.shiftKey && key == 187 && that.value.length < 1) {
                return true;
            }
            if (e.shiftKey && key == 187 && that.value.length > 1) {
                return false;
            }
            if (e.shiftKey && key != 187) {
                return false;
            }
            if (!e.shiftKey && key == 187) {
                return false;
            }
            if (that.value[0] == '+' && that.value.length >= 13) {
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 190 ||
                    (key >= 35 && key <= 40)
                );
            }
            if (that.value[0] == '+' && that.value.length >= 13) {
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 190 ||
                    (key >= 35 && key <= 40)
                );
            }
            if (that.value[0] != '+' && that.value.length >= 10) {
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 190 ||
                    (key >= 35 && key <= 40)
                );
            }
            if (that.value[0] == '+' && that.value.length == 1 && key == 48) {
                return false
            }
            if (that.value.length == 0 && key == 48) {
                return false
            }
            return (
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105) ||
                key == 107
            );
        });
    });
};

window.dave_load_chatbot = function () {
    DAVE_SETTINGS.execute_custom_callback("before_load_chatbot");
    //All Dynamic Settings to change chatbot lookandfeel
    let dave_chatTitle = djQ('#dave-settings').attr('data-cbTitle') || djQ('#dave-settings').attr('data-dave-cbTitle') || "DAVE VIRTUAL ASSISTANT";
    let dave_bot_bubbleTitle = (djQ('#dave-settings').attr('data-bot-bubbleTitle') || djQ('#dave-settings').attr('data-cbdTitle') || djQ('#dave-settings').attr('data-dave-cbTitle') || "Dave Virtual Assistant");
    let dave_user_bubbleTitle = djQ('#dave-settings').attr('data-user-bubbleTitle') || "You";
    let dave_optionTitle = djQ('#dave-settings').attr('data-optionTitle') || '';
    let data_feedback_redo_time = djQ("#dave-settings").attr("data-feedback-redo-time") || 3600000;
    let feedback_successMSG = djQ("#dave-settings").attr("data-feedback-successMsg") || "Thank you for your valuable feedback";
    let maxLockHeight = djQ("#dave-settings").attr("data-cb-minLockHeight") || 150;

    let dave_desktop_size = {
        dave_cb_height: djQ('#dave-settings').attr('data-cb-height-desktop') || '520px',
    }
    let dave_mobile_size = {
        dave_cb_height: (djQ('#dave-settings').attr('data-cb-height-mobile') || '500px'),
    }

    //ALL IMAGE DATA-SETTINGS 
    let dave_bot_icon = djQ('#dave-settings').attr('data-botIcon') || djQ('#dave-settings').attr('data-dave-botIcon') || DAVE_ASSET_PATH + 'assets/img/dave-icon.png';
    let dave_user_icon = djQ('#dave-settings').attr('data-userIcon') || djQ('#dave-settings').attr('data-dave-userIcon') || DAVE_ASSET_PATH + 'assets/img/userchat-icon.png';
    let dave_chatbot_icon = djQ('#dave-settings').attr('data-cbIcon') || djQ('#dave-settings').attr('data-dave-cbIcon') || DAVE_ASSET_PATH + 'assets/img/dave-icon.png';
    let dave_title_icon = djQ('#dave-settings').attr('data-title-icon') || djQ('#dave-settings').attr('data-dave-title-icon') || dave_chatbot_icon;
    let cross_img = djQ("#dave-settings").attr("data-cross-img") || djQ("#dave-settings").attr("data-dave-cross-img") || DAVE_ASSET_PATH + 'assets/img/cross.png';
    let min_img = djQ("#dave-settings").attr("data-min-img") || djQ("#dave-settings").attr("data-dave-min-img") || DAVE_ASSET_PATH + 'assets/img/min.svg';
    let max_img = djQ("#dave-settings").attr("data-max-img") || djQ("#dave-settings").attr("data-dave-max-img") || DAVE_ASSET_PATH + 'assets/img/max.svg';
    let like_img = djQ("#dave-settings").attr("data-like-img") || DAVE_ASSET_PATH + 'assets/img/up.svg';
    let liked_img = djQ("#dave-settings").attr("data-liked-img") || DAVE_ASSET_PATH + 'assets/img/up-hover.svg';
    let dislike_img = djQ("#dave-settings").attr("data-dislike-img") || DAVE_ASSET_PATH + 'assets/img/down.svg';
    let disliked_img = djQ("#dave-settings").attr("data-disliked-img") || DAVE_ASSET_PATH + 'assets/img/down-hover.svg';
    let cb_sugg_next = djQ("#dave-settings").attr("data-cbSuggNext-img") || DAVE_ASSET_PATH + 'assets/img/next.png';
    let cb_sugg_prev = djQ("#dave-settings").attr("data-cbSuggPrev-img") || DAVE_ASSET_PATH + 'assets/img/pre.png';
    let typing_gif = djQ("#dave-settings").attr("data-typing-img") || DAVE_ASSET_PATH + 'assets/img/typing.gif';
    let mic_img = djQ("#dave-settings").attr("data-mic-img") || DAVE_ASSET_PATH + 'assets/img/mic.svg';
    let mic_hover_img = djQ("#dave-settings").attr("data-micHover-img") || DAVE_ASSET_PATH + 'assets/img/michover.svg';
    let send_img = djQ("#dave-settings").attr("data-send-img") || DAVE_ASSET_PATH + 'assets/img/send.png';
    let homeBtnImg = djQ("#dave-settings").attr("data-homeBtn-img");
    let WIA_RatingTitle = djQ("#dave-settings").attr("data-feedback-accurate-title") || djQ("#dave-settings").attr("data-dave-feedback-accurate-title") || "Was I accurate?";
    let WIH_RatingTitle = djQ("#dave-settings").attr("data-feedback-useful-title") || djQ("#dave-settings").attr("data-dave-feedback-useful-title") || "Was I helpful?";
    let writeFeedback = !(djQ("#dave-settings").attr("data-hide-feedback-text") === "true");
    let feedbackTextPlaceholder = djQ("#dave-settings").attr("data-feedback-text-placeholder") || djQ("#dave-settings").attr("data-dave-feedback-text-placeholder") || "Write your feedback";
    //All Dynamic Settings ENDS

    //global letiable declaration as flag and window width
    let originalScroll = djQ('body').css('overflow');
    console.debug("Original Scroll is", originalScroll);
    let dave_window_width = djQ(window).width();
    let dave_window_height = djQ(window).height();
    console.debug("Window height and width when page loads => " + dave_window_width + " " + dave_window_height);
    let dave_chat_heightchange = false;
    let pred_attach = false;
    let text_cleared = true;
    let chat_historyActivated = false;

    let map_enabled = undefined;
    let userPositon = {
        "lat": undefined,
        "lng": undefined,
    }

    let dave_userchatfunc_exe = undefined;
    let dave_predication_send = undefined;
    let chat_loading = undefined;
    let dave_activityTimer = undefined;
    let nudge_activityInterval = undefined;
    let followupIndex = undefined;
    let followupList_array = new Array();
    let timerFlag = true;
    let close_count = 0;


    let chatbox_width = djQ(".dave-chatbox-cont").width() || "360px";
    //GLOBAL VARIABLE VALUES SET AS PER SCREEN SIZING
    if (dave_window_width > 450) {
        //DESKTOP
        djQ(".dave-chatbox").css({ "height": dave_desktop_size.dave_cb_height });
        //chatbox_width = djQ('#dave-settings').attr('data-cb-width-desktop') || chatbox_width;
    } else {
        //MOBILE
        djQ(".dave-chatbox").css({ "height": dave_mobile_size.dave_cb_height });
        DAVE_SCENE.is_mobile = true;
    }
    //AVATAR CODE BLOCK STARTS
    let data_avatar_initChk = djQ("#dave-settings").attr("data-avatar-id") || false;
    let dave_avatar_loadStatus = false;
    let chatboxOpenStatus = 'close';
    let nowRecording = false;

    function showAvatar(status) {
        if (status) {
            djQ(".dave-cb-avatar-contt").css("display", "block");
        } else {
            djQ(".dave-cb-avatar-contt").css("display", "none");
        }
    }

    DAVE_SCENE.set_avatar_position = function (x, y, h) {
        h = h || 200;
        x = x || (0 - parseFloat(h) / 2)
        y = y || 0
        djQ(".dave-cb-avatar-contt").css({ "left": x, "top": y });
        djQ("canvas#dave-canvas").height(h);
    }
    if (data_avatar_initChk) {
        DAVE_SETTINGS.register_custom_callback('onload', function () {
            dave_avatar_loadStatus = true;
            let avatar_xCord = djQ("#dave-settings").attr("data-avatar-position-x") || null;
            let avatar_yCord = djQ("#dave-settings").attr("data-avatar-position-y") || null;
            let avatarHeight = djQ("#dave-settings").attr("data-avatar-position-h") || null;
            /*let avatarWidth =  djQ("#dave-settings").attr("data-avatar-position-w") || null;*/
            DAVE_SCENE.set_avatar_position(avatar_xCord, avatar_yCord, avatarHeight);
            djQ(".dave-cb-tt").css("margin-left", "");

            if (dave_window_width > 450 && chatboxOpenStatus == "open") {
                let h = djQ(".dave-chatbox").height() + 50;
                dave_desktop_size.dave_cb_height = h;
            }

            if (chatboxOpenStatus == "open") {
                showAvatar(true);
            }

            set_maximized_height();
        }, DAVE_SCENE);
    } else {
        showAvatar(false);
    }

    //AVATAR CODE BLOCKS ENDS
    function set_inner_height() {
        if (dave_optionTitle) {
            djQ(".dave-cb-chatarea").height(-djQ(".dave-cb-tt-sec").first().offset().top - djQ(".dave-cb-tt-sec").first().height() + djQ(".dave-cb-stickBottom").first().offset().top - 27);
        } else {
            djQ(".dave-cb-chatarea").height(-djQ(".dave-cb-tt-sec").first().offset().top - djQ(".dave-cb-tt-sec").first().height() + djQ(".dave-cb-stickBottom").first().offset().top - 22);
        }
    }

    function set_maximized_height() {
        if (dave_window_width > 450) {
            djQ(".dave-chatbox").css({ "height": dave_desktop_size.dave_cb_height });
        } else {
            djQ(".dave-chatbox").css({ "height": dave_mobile_size.dave_cb_height });
        }
        //Just check if avatar is initialized then> is it mobile view or desk then> have a size of avtar detect that size and add to variable h
        let h = djQ(".dave-chatbox").height();
        let w = window.innerHeight;
        if (h > w) {
            //Just check if avatar is initialized then> is it mobile view or desk then> have a size of avtar detect that size and substract to variable w in height parameter passed
            djQ(".dave-chatbox").height(w);
        }
        set_inner_height();
    }

    // FUNCTION FOR PADDING LEADING ZEROS
    function pad(n, width, z) {
        width = width || 2
        z = z || '0'
        return (String(z).repeat(width) + String(n)).slice(String(n).length)
    }

    //NUDGES CODE BLOCKS STARTS
    function dave_nudgeTrigger(index) {
        clearTimeout(dave_activityTimer);
        followupIndex++;
        if (followupIndex <= followupList_array.length) {

            if (DAVE_SCENE.opened == true) {
                n_openState = 'max'
            } else {
                n_openState = followupList_array[index][2];
            }

            if (DAVE_SCENE.opened) {
                botchat_data({ 'customer_state': followupList_array[index][0], "query_type": "auto" }, '', '', '', function (data) {
                    dave_timerFunc();
                });
            } else {
                if (followupList_array[index][1]) {
                    if (chatboxOpenStatus == 'close') {
                        z = 'open'
                    } else {
                        z = '';
                    }
                    if (n_openState == 'max') {
                        botchat_data({ 'customer_state': followupList_array[index][0], "query_type": "auto" }, '', z, 'max', function (data) {
                            dave_timerFunc();
                        });
                    } else {
                        botchat_data({ 'customer_state': followupList_array[index][0], "query_type": "auto" }, '', z, 'min', function (data) {
                            dave_timerFunc();
                        });
                    }
                }
            }
        }
    }

    function dave_nudge(dave_activityInterval, followupList, forceStatus, openState) {
        openState = openState || false;
        let i = 0;
        followupIndex = 0;
        nudge_activityInterval = dave_activityInterval;
        if (dave_activityInterval && followupList) {
            followupList_array.length = 0;
            djQ.each(followupList, function (key, value) {
                followupList_array.push([value, forceStatus, openState]);
                i++;
            });
            if (followupList_array.length > 0) {
                timerFlag = true;
                dave_timerFunc();
                djQ("body").off("keydown click touchstart dblclick resize", window.dave_nudge_clear_func);
                djQ("body").on("keydown click touchstart dblclick resize", window.dave_nudge_clear_func);
            }
        }
    }

    function dave_timerFunc() {
        if (followupIndex <= followupList_array.length && timerFlag) {
            clearTimeout(dave_activityTimer);
            dave_activityTimer = setTimeout(function () {
                dave_nudgeTrigger(followupIndex);
            }, nudge_activityInterval);
        } else {
            djQ("body").off("keydown click touchstart dblclick resize", window.dave_nudge_clear_func);
        }
    }
    // THIS clears an nudges if they exist when you do any activities on the page
    window.dave_nudge_clear_func = function (e) {
        if (e.type == 'keydown' && (e.which < 30 || (e.which > 90 && e.which < 96) || e.which > 105)) {
            return true;
        }
        console.debug("Reset nudge", e, e.which);
        clearTimeout(dave_activityTimer);
        dave_timerFunc();
    }
    window.stopTimmer = function () {
        timerFlag = false;
        clearTimeout(dave_activityTimer);
    }
    window.startTimmer = function () {
        timerFlag = true;
        if (typeof dave_timerFunc === 'function') {
            dave_timerFunc();
        }
    }
    //NUDGES CODE BLOCKS END HERE


    //Like Dislike Function Block
    function like_dislike(currClickedElem, feedback_reaction, feedback_type) {
        if (feedback_reaction == 'like') {
            DAVE_SETTINGS.execute_custom_callback("on_click_like", [currClickedElem, feedback_type]);
            djQ(currClickedElem).html('');
            djQ(currClickedElem).html(`<img src='${liked_img}'>`);

            djQ(currClickedElem).next().html('');
            djQ(currClickedElem).next().html(`<img src='${dislike_img}'>`)

            l_d_react = true;
        } else {
            DAVE_SETTINGS.execute_custom_callback("on_click_dislike", [currClickedElem, feedback_type]);
            djQ(currClickedElem).html('');
            djQ(currClickedElem).html(`<img src='${disliked_img}'>`)

            djQ(currClickedElem).prev().html();
            djQ(currClickedElem).prev().html(`<img src='${like_img}'>`)

            if (feedback_type == "speech") {
                let disliked_voice_text = djQ(currClickedElem).closest('.dave-voiceBubble-feedback').prev('p').text();
                djQ("#dave-cb-textinput").val(disliked_voice_text);
            }

            l_d_react = false;
        }
        return l_d_react;
    }

    //printing Bubble Timing
    function dateTiming(part) {
        let dave_date = new Date();
        if (part) {
            return dave_date;
        } else {
            const options={weekday:"short"}
            let printHrsMin = dave_date.toLocaleString('en-US',options) + ',' + pad(dave_date.getHours()) + ":" + pad(dave_date.getMinutes());
            return printHrsMin;
        }
    }

    //auto scroll on every msg pop-in
    function scrollChatarea(speed) {
        speed = speed || 400;
        djQ(".dave-cb-chatarea").animate({ scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight") }, speed);
    }
    DAVE_SETTINGS.scrollChatarea = scrollChatarea;
    //loader icon display function block
    window.dave_chatRes_loader = function dave_chatRes_loader(loaderType) {
        if (loaderType == 'bot') {
            if (DAVE_SETTINGS.execute_custom_callback('before_bot_loader') === false) {
                return djQ(".dave-cb-chatarea")
            }
            return djQ(".dave-cb-chatarea").append(`<div class='dave-botchat dave-bottyping-loader'><div class='dave-bc-avatar'><img src='${dave_bot_icon}'></div><p class='dave-chattext dave-botchat-typing-p'><img class='dave-botchat-typing-img' src='${typing_gif}'></p></div>`);
        } else if (loaderType == 'user') {
            if (DAVE_SETTINGS.execute_custom_callback('before_user_loader') === false) {
                return djQ(".dave-cb-chatarea")
            }
            return djQ(".dave-cb-chatarea").append(`<div class='dave-userchat dave-usertyping-loader'><div class='dave-uc-avatar'><img src='${dave_user_icon}'></div><p class='dave-userchat-p dave-user-typing-p'><img class='dave-user-typing-img' src='${typing_gif}'></p></div>`);
        }
    }

    //remove loading gif 
    function dave_typingLoaderRemove(loaderType) {
        if (loaderType == 'user') {
            djQ('.dave-usertyping-loader').remove();
        } else if (loaderType == 'bot') {
            djQ('.dave-bottyping-loader').remove();
        }
    }
    //maximize chatbot
    function maximize_chatbox(minCustomSize) {
        let oc = djQ('body').css('overflow');
        if (oc != 'hidden') {
            originalScroll = oc;
        }
        if (DAVE_SETTINGS.on_maximize() === false) {
            return
        }
        chatboxOpenStatus = 'max';

        if (DAVE_SCENE.is_mobile) {
            djQ('.dave-cb-chatarea').css('display', 'block');
            djQ('.dave-cb-stickBottom').css('display', 'block');
        }

        djQ(".dave-cb-tt-minmax img").remove();
        djQ(".dave-cb-tt-minmax").append(`<img src='${min_img}'>`);
        set_maximized_height();

        if (!minCustomSize) {
            djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
                minimize_chatbox();
            });
        } else {
            djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
                minimize_chatbox(minCustomSize);
            });
            djQ("textarea#dave-feedback-write").css("height", "");
            djQ(".rating-star-contt").css("bottom", "");
            djQ("#dave-userfull-rate, #dave-accuracy-rate").css('height', '');
        }
        scrollChatarea(500);
        DAVE_SETTINGS.execute_custom_callback("after_maximize_custom")
    }
    //minimize chatbot function
    function minimize_chatbox(customSize, chatType) {
        if (DAVE_SETTINGS.on_minimize() === false) {
            return;
        }
        chatboxOpenStatus = 'min';
        if (!DAVE_SCENE.is_mobile) {
            if (!customSize) {
                let type_sec_height = djQ(".dave-cb-type-sec").height();
                let chatsugg_height = djQ(".dave-cb-chatsugg").height();
                let tt_height = djQ(".dave-cb-tt-sec").height();
                let last_message_height = djQ(".dave-botchat").last().height();
                let ext_spc = 50;

                if (maxLockHeight) {
                    maxLockHeight = parseInt(maxLockHeight);
                    if (last_message_height > maxLockHeight) {
                        djQ(".dave-chatbox").height((type_sec_height + chatsugg_height + tt_height + maxLockHeight + ext_spc));
                    } else {
                        djQ(".dave-chatbox").height((type_sec_height + chatsugg_height + tt_height + last_message_height + ext_spc));
                    }
                } else {
                    djQ(".dave-chatbox").height((type_sec_height + chatsugg_height + tt_height + last_message_height + ext_spc));
                }
            } else {
                if (!chatType) {
                    djQ(".dave-chatbox").height(customSize);
                } else {
                    djQ(".dave-chatbox").height(customSize);
                    // if (chatType == 'feedback') {
                    //     var writeFeedbackBox = (djQ("textarea#dave-feedback-write").height()) / 2;
                    //     djQ("textarea#dave-feedback-write").css("height", writeFeedbackBox);
                    //     djQ(".rating-star-contt").css("bottom", "-30%");
                    //     djQ("#dave-userfull-rate, #dave-accuracy-rate").height('10%');
                    // }
                }
            }
            set_inner_height();
            scrollChatarea(500);
        } else {
            let headerSecHeight = Math.max(djQ('.dave-cb-tt-sec').height(), '200');
            djQ('.dave-chatbox-open').height(headerSecHeight);
        }
        djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
            maximize_chatbox();
        });
        if (originalScroll != 'hidden') {
            djQ('body').css('overflow', originalScroll);
        } else {
            djQ('body').css('overflow', '');
        }
        console.debug("set back original scroll", originalScroll);
        djQ(".dave-cb-tt-minmax img").remove();
        djQ(".dave-cb-tt-minmax").append(`<img src='${max_img}'>`);
        DAVE_SETTINGS.execute_custom_callback("after_minimize_custom")
    }
    DAVE_SETTINGS.maximize = maximize_chatbox;
    DAVE_SETTINGS.minimize = minimize_chatbox;

    //PREDICTION FOR USER TYPEING MESSAGE CODE BLOCK
    function dave_prediction(selectedPrediction) {
        if (selectedPrediction.value.length > 2) {
            DAVE_SETTINGS.predict(selectedPrediction.value, function (response) {
                DAVE_SETTINGS.execute_custom_callback("before_predicted_text", [selectedPrediction.value, response])
                if (text_cleared) {
                    console.warn("text was sent before we got prediction, so doing nothing!");
                    return;
                }
                if (response.length > 0) {
                    console.debug("We are getting response for prediction");
                    if (!pred_attach) {
                        djQ('.dave-pred-contt').append(`<ol class='dave-pred-box'></ol>`);
                        if (response.length < 5) {
                            djQ(".dave-pred-contt").css({ "bottom": (djQ(".dave-cb-type-sec").height() + 10) + "px", "height": "auto", "overflow": "auto", "max-height": "110px", "padding-bottom": "10px" });
                        } else if (response.length >= 5) {
                            djQ(".dave-pred-contt").css({ "bottom": (djQ(".dave-cb-type-sec").height() + 10) + "px", "height": "110px", "overflow-y": "scroll" });
                        }
                        pred_attach = true;
                    }
                } else {
                    console.warn("we are not getting any response for prediction")
                    djQ(".dave-pred-contt").html('');
                    djQ(".dave-pred-contt").css({ "height": "0", "overflow": "hidden", "padding": "0" });
                    pred_attach = false;
                }
                djQ('.dave-pred-box').html('');
                djQ.each(response, function (keys, values) {
                    pred_custState = Object.keys(values);
                    pred_title = Object.values(values);
                    DAVE_SETTINGS.execute_custom_callback("before_add_predicted_text", [pred_title, pred_custState])
                    let prd = `
                        <li class='dave-pred-txt' data-pred-custState='${pred_custState}'>${pred_title}</li>
                    `
                    DAVE_SETTINGS.execute_custom_callback("on_add_predicted_text", [prd])
                    djQ(".dave-pred-box").append(prd);
                });
                if (pred_attach) {
                    djQ(".dave-pred-txt").one("click", function () {
                        let clicked_pred = djQ(this).text();
                        clicked_pred_custState = djQ(this).attr('data-pred-custState');
                        DAVE_SETTINGS.execute_custom_callback("before_click_predicted_option", [clicked_pred, clicked_pred_custState]);
                        if (
                            DAVE_SETTINGS.execute_custom_callback(
                                "before_click_predicted_option", [clicked_pred, clicked_pred_custState]
                            ) === false
                        ) {
                            return
                        }
                        djQ("#dave-cb-textinput").val(clicked_pred);
                        djQ("#dave-cb-textinput").focus();
                        djQ(".dave-pred-contt").html('');
                        djQ(".dave-pred-contt").css({ "height": "0", "overflow": "auto", "padding": "0" });
                        pred_attach = false;
                        dave_predication_send = true;
                        set_inner_height();
                        DAVE_SETTINGS.execute_custom_callback("after_click_predicted_option", [clicked_pred, clicked_pred_custState]);
                    });
                }
            });
        } else {
            djQ(".dave-pred-contt").html('');
            djQ(".dave-pred-contt").css({ "height": "0", "overflow": "hidden", "padding": "0" });
            pred_attach = false;
        }
    }

    djQ(document).on("click", ".dave-cb-chatarea, .dave-cb-chatsugg", function () {
        djQ(".dave-pred-contt").html('');
        djQ(".dave-pred-contt").css({ "height": "0", "overflow": "auto", "padding": "0" });
        pred_attach = false;
    })

    function replyBubbleUserMsg(replyId) {
        if (typeof replyId == 'undefined') {
            return false;
        };
        let msg = djQ('.dave-cb-chatarea').find(`#${replyId}`).find('.dave-userchat-p').text();
        djQ('.dave-cb-chatarea').find(`#${replyId}`).nextAll().find(`[data-scrollmsgid = '${replyId}']`).find('.replyBubbleMsg').html(msg);
        // /.find('.replyBubbleMsg').html(msg)
    }
    //ATTACH ONLY TEXT MESSAGE BUBBLE USER END
    function userTextMsgBubble(text, chatType, dateTime, bubbleId, msgId, jquery_element, jquery_function) {
        jquery_element = jquery_element || djQ(".dave-cb-chatarea")
        jquery_function = jquery_function || "append"
        let d = { 'text': text }
        DAVE_SETTINGS.execute_custom_callback('before_user_text_bubble', [d]);
        text = d.text
        let utmb_date = dateTime || dateTiming();
        dave_typingLoaderRemove('user');
        jquery_element[jquery_function](`
                <div class='dave-userchat' id='${bubbleId}'>
                    <div class='dave-uc-avatar'>
                        <img src='${dave_user_icon}'>
                    </div>
                    <p class='dave-userBubble-header'>
                        <span class='dave-userBubble-Title'> ${dave_user_bubbleTitle} &nbsp;&nbsp;</span>
                        <span class='dave-userBubble-timestamp'>${utmb_date}</span>
                    </p>
                    <p class='dave-userchat-p'>${text}</p>
                </div>
            `).ready(function () {
            if (chatType == "speech") {
                djQ(this).find('.dave-userchat').last().attr('data-voiceMsgId', msgId);
                dave_chatRes_loader('bot');
            }
        });
    }
    //Message Bubble Print Function Block
    function OCD_msgBubbles(userIcon, name, timestamp, bubbleId, responseTxt, reply_to) {
        djQ(".dave-cb-chatarea").append(`
                <div class='dave-botchat'>
                    <div class='dave-bc-avatar'>
                        <img src='${userIcon}'>
                    </div>
                    <p class='dave-botBubble-header'>
                        <span class='dave-botBubble-Title'>${name}&nbsp;&nbsp;</span>
                        <span class='dave-botBubble-timestamp'>${timestamp}</span>
                    </p>
                    ${bubbleId && !reply_to ? `<div style='display: none;' class='scrollToMsg' data-scrollMsgId='${bubbleId}'></div>` : `${bubbleId && reply_to ? `<div class='scrollToMsg' data-scrollMsgId='${reply_to}'><p class='replyBubbleTitle'>Query</p><p class='replyBubbleMsg'></p></div>` : ''}`}
                    <p class='dave-chattext'>${responseTxt}</p>
                </div>
            `).ready(function () {
            djQ(".dave-cb-chatarea").animate({ scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight") }, 500);
            if (reply_to) {
                replyBubbleUserMsg(reply_to);
            }
        });
    }

    function updateBubbleId(bubbleId) {
        let x = temp_bubbleId.pop();
        let z = perm_bubbleId.pop();
        djQ('.dave-cb-chatarea').find('#' + x).attr('id', z);
    }
    function msgBubbles(msgBy, chatType, dateTime, userMsg, bubbleId, replyPrintState, voiceMsgId, custState, callback, jquery_element, jquery_function) {
        jquery_element = jquery_element || djQ(".dave-cb-chatarea")
        jquery_function = jquery_function || "append"
        DAVE_SETTINGS.execute_custom_callback('before_message_bubble', [msgBy, chatType, dateTime, userMsg, bubbleId, replyPrintState, voiceMsgId, custState, jquery_element, jquery_function])
        userMsg = userMsg || undefined;
        voiceMsgId = voiceMsgId || undefined;
        custState = custState || undefined;
        if (msgBy == 'bot') {
            if (chatType == 'normal' || chatType == 'history' || chatType == 'speech' || chatType == 'error') {
                dave_typingLoaderRemove('bot');
                jquery_element[jquery_function](`
                <div class='dave-botchat'>
                    <div class='dave-bc-avatar'>
                        <img src='${dave_bot_icon}'>
                    </div>
                    <p class='dave-botBubble-header'>
                        <span class='dave-botBubble-Title'>${dave_bot_bubbleTitle}&nbsp;&nbsp;</span>
                        <span class='dave-botBubble-timestamp'>${dateTime}</span>
                    </p>
                    ${bubbleId && !replyPrintState ? `<div style='display: none;' class='scrollToMsg' data-scrollMsgId='${bubbleId}'></div>` : `${bubbleId && replyPrintState ? `<div class='scrollToMsg' data-scrollMsgId='${bubbleId}'><p class='replyBubbleTitle'>Query</p><p class='replyBubbleMsg'></p></div>` : ''}`}

                    <p class='dave-chattext'>${userMsg}</p>
                </div>
                `).ready(function () {
                    if (chatType == 'speech') {
                        djQ('button#start-recording').prop('disabled', false);
                        djQ(`.dave-userchat[data-voiceMsgId='${voiceMsgId}']`).next('.dave-botchat').attr("data-voiceMsgId", voiceMsgId)
                    }
                    if (replyPrintState) {
                        replyBubbleUserMsg(replyPrintState);
                    }

                    if (chatType == "normal") {
                        updateBubbleId(bubbleId);
                    }

                });
            } else {
                jquery_element[jquery_function](`
                <div class='dave-botchat'>
                    <div class='dave-bc-avatar'>
                        <img src='${dave_bot_icon}'>
                    </div>
                    <p class='dave-chattext'>We don't have a response to your request. Did you really say something ?</p>
                </div>
            `);
            }
        } else {
            if (chatType == 'normal' || chatType == 'history' || chatType == 'error' || chatType == 'speech') {
                if (!voiceMsgId) {
                    userTextMsgBubble(userMsg, chatType, dateTime, bubbleId, null, jquery_element, jquery_function);
                } else {
                    userTextMsgBubble(userMsg, chatType, dateTime, bubbleId, voiceMsgId, jquery_element, jquery_function);
                }
                if (chatType == 'history') {
                    jquery_element.find('.dave-userchat').last().append(`<p class='hidden' id='customer_response_history'>${custState}</p>`);
                } else if (chatType == 'speech') {
                    djQ('button#start-recording').prop('disabled', true);
                    jquery_element.find('.dave-userchat').last().append(`
                        <div class='dave-voiceBubble-feedback'>
                            <input type='hidden' value='${voiceMsgId}' name='voice-responseStoredId'>
                            <button class='voice-thumbsup' data-thumbtype='like'>
                                <img src='${like_img}'>
                            </button>
                            <button class='voice-thumbsdown' data-thumbtype='dislike'>
                                <img src='${dislike_img}'>
                            </button>
                        </div>
                    `);
                }
            }
        }

        DAVE_SETTINGS.execute_custom_callback('after_message_bubble', [msgBy, chatType, dateTime, userMsg, bubbleId, replyPrintState, voiceMsgId, custState])
        if (callback) {
            callback();
        }
    }

    djQ(document).on('click', '.scrollToMsg', function () {
        let msgId = djQ(this).attr('data-scrollMsgId');
        let cbHeight = djQ('.dave-cb-chatarea').height();
        let msgBubblePos = djQ('#' + msgId).position().top;
        let chatScrollHeight = djQ('.dave-cb-chatarea').scrollTop();
        if (msgBubblePos < 0) {
            djQ('.dave-cb-chatarea').animate({ scrollTop: chatScrollHeight - (Math.round(cbHeight / 2.5)) }, 200);
            djQ('#' + msgId).css("box-shadow", "-10px 0px 5px 5px #ecebeb");
            setTimeout(function () {
                djQ('#' + msgId).css("box-shadow", "");
            }, 1000);
        } else if (msgBubblePos > cbHeight) {
            djQ('.dave-cb-chatarea').animate({ scrollTop: chatScrollHeight + msgBubblePos - (Math.round(cbHeight / 2.5)) }, 200);
            djQ('#' + msgId).css("box-shadow", "-10px 0px 5px 5px #ecebeb");
            setTimeout(function () {
                djQ('#' + msgId).css("box-shadow", "");
            }, 1000);
        } else {
            djQ('#' + msgId).css("box-shadow", "-10px 0px 5px 5px #ecebeb");
            setTimeout(function () {
                djQ('#' + msgId).css("box-shadow", "");
            }, 1000);
        }
        console.debug("== " + msgId + " == " + chatScrollHeight + " == " + cbHeight + " == " + Math.round(cbHeight / 2.5) + " == " + msgBubblePos)
    });
    window.other_chat_data = function (status, userIcon, name, timestamp, responseTxt, reply_to, data, bubbleId) {
        timestamp = DAVE_SETTINGS.print_timestamp(timestamp);
        if (status == 'typing') {
            djQ(".dave-cb-chatarea").append(`<div class='dave-botchat .dave-otherpersonTyping-loader'><div class='dave-bc-avatar'><img src='${userIcon}'></div><p class='dave-chattext dave-botchat-typing-p'><img class='dave-botchat-typing-img' src='${typing_gif}'></p></div>`);
        } else {
            djQ(".dave-otherpersonTyping-loader").remove();
            //window.response_print = function (data, bubbleId, data_length, new_date, chatType, msgId) {
            OCD_msgBubbles(userIcon, name, timestamp, bubbleId, responseTxt, reply_to);
            response_print(data, 1, timestamp, 'other_chat_data');
        }
    }
    //On Error Display Msg Function Block
    function chatResponseError(chatType, eCode, params, bubbleId, behaviour, openState, lockHeight, prevTimeout) {
        prevTimeout = prevTimeout || 500;
        error_code = eCode['status'];
        dave_typingLoaderRemove('bot');
        if (eCode.statusText == 'timeout') {
            scrollChatarea();
            return
        }
        msgBubbles('bot', 'error', dateTiming());
        if (error_code >= 400 & error_code < 500) {
            djQ(".dave-chattext").last().html("There seems to be some technical issue in the system. Please try a different query.");
            scrollChatarea();
        } else if (error_code >= 500) {
            djQ(".dave-chattext").last().html("There seems to be a delay in getting your response, hold on!");
            scrollChatarea();
            setTimeout(function () {
                if (chatType == "normal") { botchat_data(params, bubbleId, behaviour, lockHeight, openState) } else if (chatType == "history") { window.botchat_history(); }
            }, prevTimeout * 2)
        } else {
            djQ(".dave-chattext").last().html("There seems to be problem in getting your response.  Can you please try again in some time?");
            scrollChatarea();
        }
    }
    window.handleMapError = function () {
        djQ(".dave-cb-form form").last().prev().remove();
        djQ(".dave-cb-form form").last().before(`
            <button class="getCurLoc">Get My Location</button>
        `);
    }

    djQ(document).on("click", ".getCurLoc", function () {
        getUserCurrentLocation(true);
        console.debug("TOOG")
    });

    function getUserCurrentLocation(directAccess) {
        if (directAccess) {
            if ("geolocation" in navigator) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    userPositon['lat'] = position.coords.latitude;
                    userPositon['lng'] = position.coords.longitude;
                    console.debug(userPositon);
                }, function (error) {
                    switch (error.code) {
                        case error.PERMISSION_DENIED:
                            console.warn("Permission denied by user.");
                            break;
                        case error.POSITION_UNAVAILABLE:
                            console.warn("Location position unavailable.");
                            break;
                        case error.TIMEOUT:
                            console.warn("Request timeout.");
                            break;
                        case error.UNKNOWN_ERROR:
                            console.warn("Unknown error.");
                            break;
                    }
                }, { timeout: 1000, enableHighAccuracy: true });
            } else {
                console.warn("Browser doesn't support geolocation!");
            }
        }
    }
    window.loadDaveMap = function () {
        var googleMapKey = djQ(document).find("#dave-settings").attr("data-google-map-key") || DAVE_SETTINGS.GOOGLE_MAP_KEY;
        if (!googleMapKey) {
            console.warn("NO GOOGLE API KEY FOUND SO NO MAP LOAD")
            return
        }
        var map;
        var marker;

        var predefinedLocation = [
        ];
        function initMap() {
            // SETTING UP THE MAP VARIABLE
            map = new google.maps.Map(djQ("#map")[0], {
                center: { lat: 20.5937, lng: 78.9629 },
                zoom: 3,
            });
            // InfoWindow, MARKER, GEOCODER USED TO GENERATE PIN, AND LOCATION FULL DETAILS
            const infowindow = new google.maps.InfoWindow();
            const infowindowContent = document.getElementById("infowindow-content");

            const geocoder = new google.maps.Geocoder();
            const marker = new google.maps.Marker({ map: map });

            infowindow.setContent(infowindowContent);

            // PREDEFINED LOCATION USED TO POINT SEVERAL LOCATION
            function pdl_InfoWindow(marker, map, title, address, url) {
                google.maps.event.addListener(marker, 'click', function () {
                    var html = `
                <div id="infowindow-content">
                    <span id="place-name" class="title">${title}</span><br />
                    <span id="place-address">${address}</span>
                </div>
                `;
                    pdl_infoWin = new google.maps.InfoWindow({
                        content: html,
                        maxWidth: 350
                    });
                    pdl_infoWin.open(map, marker);
                });
            }

            // LOOPING THROUGH PREDEFINED LOCATION DATA TO SET ALL LOCATIONS WITH MARKER ON MAP
            djQ.each(predefinedLocation, function (i, val) {
                geocoder.geocode({ location: val['location'] })
                    .then(function (response, status) {
                        djQ("#mapError").remove();
                        if (response.results[0]) {
                            var predefinedMarker = new google.maps.Marker({
                                map: map,
                                icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                                position: val['location'],
                                animation: google.maps.Animation.DROP,
                                address: val['Address'] || response.results[0].formatted_address,
                                title: val['title'] || "Location",
                                // url: url
                            });
                            pdl_InfoWindow(predefinedMarker, map, val['title'], response.results[0].formatted_address);
                        } else {
                            djQ("#map").closest(".mapBox").after(`<p id="mapError" style="color:red;">Please enter a landmark near you and select from search box drop down or map</p>`);
                        }
                    })
                    .catch((e) => djQ("#map").closest(".mapBox").after(`<p id="mapError" style="color:red;">Geocoder failed due to: ${e}</p>`));
            });

            //INPUT BOX FOR USERS TO SEARCH PARTICULAR LOCATION
            djQ(document).on("focus", "#locationInput", function () {
                const input = document.getElementById('locationInput');
                const options = {
                    fields: ["address_components", "formatted_address", "geometry"],
                    strictBounds: false,
                    types: ["geocode"],
                };
                // SETTING UP THE Autocomplete VARIABLE TO TRIGGER AND GET AUTOSUGGESTIONS OF LOCATION
                const autocomplete = new google.maps.places.Autocomplete(input, options);
                google.maps.event.addDomListener(input, 'keydown', function (e) {
                    console.debug(e.triggered)
                    if (e.keyCode === 13 && !e.triggered) {
                        google.maps.event.trigger(this, 'keydown', { keyCode: 40 })
                        google.maps.event.trigger(this, 'keydown', { keyCode: 13, triggered: true })
                    }
                });
                //POINT NEW LOCATION USER SELECTED USING INPUT BOX
                autocomplete.addListener("place_changed", () => {
                    infowindow.close();
                    marker.setVisible(false);

                    const place = autocomplete.getPlace();

                    if (!place.geometry || !place.geometry.location) {
                        djQ("#mapError").remove();
                        djQ("#map").closest(".mapBox").after(`<p id="mapError" style="color:red;">Please enter a landmark near you and select from search box drop down or map</p>`);
                        return;
                    }

                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }

                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);
                    userPositon['lat'] = place.geometry.location.lat();
                    userPositon['lng'] = place.geometry.location.lng();
                    console.debug(userPositon);
                    infowindowContent.children["place-name"].textContent = place.name;
                    infowindowContent.children["place-address"].textContent = place.formatted_address;
                    infowindow.open(map, marker);
                });
            });

            function updateInfoWindow(userPositon) {
                geocoder.geocode({ location: userPositon })
                    .then(function (response, status) {
                        djQ("#mapError").remove();
                        if (response.results[0]) {
                            infowindowContent.children["place-name"].textContent = response.results[0].name;
                            infowindowContent.children["place-address"].textContent = response.results[0].formatted_address;
                            infowindow.open(map, marker);
                        } else {
                            djQ("#map").closest(".mapBox").after(`<p id="mapError" style="color:red;">Please enter a landmark near you and select from search box drop down or map</p>`);
                        }
                    })
                    .catch((e) => djQ("#map").closest(".mapBox").after(`<p id="mapError" style="color:red;">Geocoder failed due to: ${e}</p>`));

            }
            google.maps.event.addListener(
                marker,
                'dragend',
                function () {
                    userPositon['lat'] = marker.position.lat();
                    userPositon['lng'] = marker.position.lng();
                    updateInfoWindow(userPositon);
                    console.debug(userPositon);
                }
            );
            //GET USERS CURRENT LOCATION
            if ("geolocation" in navigator) {
                navigator.geolocation.getCurrentPosition(function (position) {

                    userPositon['lat'] = position.coords.latitude;
                    userPositon['lng'] = position.coords.longitude;
                    marker.setPosition(userPositon);
                    marker.setVisible(true);

                    updateInfoWindow(userPositon);

                    marker.setDraggable(true);
                    map.setCenter(userPositon);
                    map.setZoom(10);

                    console.debug(userPositon);


                }, function (error) {
                    switch (error.code) {
                        case error.PERMISSION_DENIED:
                            const perm_den_loc = {
                                lat: 20.5937,
                                lng: 78.9629,
                            }
                            marker.setPosition(perm_den_loc);
                            marker.setVisible(true);
                            marker.setDraggable(true);
                            map.setZoom(2);
                            console.warn("Permission denied by user.");
                            break;
                        case error.POSITION_UNAVAILABLE:
                            console.warn("Location position unavailable.");
                            break;
                        case error.TIMEOUT:
                            console.warn("Request timeout.");
                            break;
                        case error.UNKNOWN_ERROR:
                            console.warn("Unknown error.");
                            break;
                    }
                }, { timeout: 1000, enableHighAccuracy: true });
            } else {
                console.warn("Browser doesn't support geolocation!");
            }
        }

        // INITIALIZING THE GOOGLE MAP
        window.initializeMap = function () {
            initMap();
        }
        if (djQ('script[src*="maps.googleapis.com/maps"]').length > 0) {
            return;
        }
        djQ.ajax({
            url: `https://maps.googleapis.com/maps/api/js?key=${googleMapKey}&libraries=geometry,places&region=in`,
            type: "GET",
            dataType: "script",
            cache: true,
            error: function (e) {
                console.error(e);
                console.warn("Something Went Wrong While Loading Google Map API")
                getUserCurrentLocation(true);
            },
        });
    }
    //SERVER RESPONSE PRINTING FUNCTION BLOCK
    window.response_print = function (data, data_length, new_date, chatType, msgId) {
        if (!DAVE_SETTINGS.execute_custom_callback('before_response_print', [data, new_date, chatType, msgId])) {
            dave_typingLoaderRemove('bot');
            scrollChatarea(500);
        };
        if (close_count > 0) {
            return
        }
        let permbubId = "RI" + data['response_id'];
        map_enabled = false;
        perm_bubbleId.push(permbubId);
        voice_MsgId = msgId || undefined;
        if (chatType == "speech") {
            dataLength_thresold = 1;
        } else {
            dataLength_thresold = 0;
            voice_MsgId = null;
        }
        if (data_length > dataLength_thresold) {
            dave_response_dataType = data['data']['response_type'];
            let rp_replyId = data['data']['reply_to'] || false;
            if (chatType == "normal") {
                dave_typingLoaderRemove('bot');
                //print bot reply as message
            }
            if (chatType == "normal" || chatType == "speech") {
                msgBubbles('bot', chatType, dateTiming(), '', permbubId, rp_replyId, voice_MsgId, '', function () {
                    if (chatType == 'speech') {
                        djQ(`.dave-userchat[data-voiceMsgId='${voice_MsgId}']`).find("input[name='voice-responseStoredId']").val(data['response_id']);
                    }
                });
            }
            scrollChatarea(500);
            if (chatType == "normal" || chatType == "speech") {
                djQ(".dave-chattext").last().html(data["placeholder"] || "");
                // if no state options are provided, then previous options are kept
                if (data['state_options'] && !djQ.isEmptyObject(data['state_options'])) {
                    djQ('.dave-cb-chatsugg').html('');
                    djQ(".dave-cb-chatsugg").css("overflow-x", "auto");

                }
                djQ(".dave-cb-chatsugg").append(`<span class='chatsugg-next'><img src='${cb_sugg_next}'></span><span class='chatsugg-prev'><img src='${cb_sugg_prev}'></span>`);

            }
            response_elementPrint(dave_response_dataType, data, chatType);
            if (data['state_options'] && !djQ.isEmptyObject(data['state_options'])) {
                //print bot suggestion
                for (const [key, value] of Object.entries(data['state_options'])) {
                    djQ('.dave-cb-chatsugg').append(`<div class='dave-cb-chatsugg-list'>${value}<input type='hidden' value='${key}'></div>`);
                }
            }

            //Bot Reply Feedback Thumbsup and ThumbsDown
            if (chatType == "normal" || chatType == "speech") {
                if (data['show_feedback']) {
                    djQ('p.dave-chattext').last().append(`
                    <div class='dave-botBubble-feedback'>
                        <span class='hidden'>${data['response_id']}</span>
                        <button class='dave-thumbsup' data-thumbType='like'><img src='${like_img}'></button>
                        <button class='dave-thumbsdown' data-thumbType='dislike'><img src='${dislike_img}'></button>
                    </div>
                `);
                }

                //Nudges Starting
                if (typeof data['data']['_follow_ups'] !== 'undefined') {
                    let forceOpen = data['data']['_force_open'] || null;
                    let openState = data['data']['_open_state'] || 'min';
                    dave_nudge(data['wait'], data['data']['_follow_ups'], forceOpen, openState);
                }

                //QUICKACCESS BUTTON NEXT & PREV ONLY VISIBLE ON SCROLL IS AVAILABLE
                let chatsugg_scrollWidth = djQ('.dave-cb-chatsugg')[0].scrollWidth;

                if (chatsugg_scrollWidth > (chatbox_width - 20)) {
                    djQ('span.chatsugg-next').css('display', 'block');
                    if (dave_optionTitle) {
                        djQ('span.chatsugg-next').css('top', '12px');
                        djQ('span.chatsugg-prev').css('top', '12px');
                    } else {
                        djQ('.dave-chatsuggTitle').hide();
                    }
                }
            }
            //Chat box height Adjustment according to device
            set_inner_height();
            dave_userchatfunc_exe = false;
            chat_loading = true;
            userchat_data();
            djQ('button#start-recording').prop('disabled', false);
        } else {
            //if no data or reply recieved from bot
            dave_typingLoaderRemove('bot');
            msgBubbles('bot', 'error', dateTiming(), 'We don\'t have a response to your request. Did you really say something ?');
            scrollChatarea(500);
        }
    }
    //Response Element Print
    function response_elementPrint(dave_response_dataType, data, chatType, count, jquery_element, jquery_function) {
        jquery_element = jquery_element || djQ('p.dave-chattext').last()
        jquery_function = jquery_function || 'append'
        if (dave_response_dataType == 'thumbnails') {
            DAVE_SETTINGS.execute_custom_callback("before_thumbnails_response", [data['data'][dave_response_dataType], data['data'], data]);
            let ths = djQ(`
                <div class="dave-chat-items-contt">
                    <ul class="dave-chat-items"></ul>
                </div>
            `)
            jquery_element[jquery_function](ths);
            djQ.each(data['data'][dave_response_dataType], function (key, value) {
                let s = djQ(`
                    <a href="${encodeURI(value['url']) || '#'}" data-customer-state='${value['customer_state']}' data-customer-response='${value['customer_response'] || value['title'] || DAVE_SETTINGS.toTitleCase(value['customer_state'])}' target='${value['target'] || '_blank'}'>
                        <li>
                            <div class='dave-chat-items-imgcontt'><img src='${value['image']}'></div>
                            <p class='dave-chat-items-bottom-p'>${value['title']}</p>
                        </li>
                    </a>
                ` );
                ths.find('.dave-chat-items').last().append(s);
                if (value['customer_state'] || value['customer_response']) {
                    djQ(s).on('click', function () {
                        userTextMsgBubble(djQ(this).attr('data-customer-response'), 'normal', dateTiming(), djQ.now());
                        botchat_data({ 'query_type': 'click', 'customer_state': djQ(this).attr('data-customer-state'), 'customer_response': djQ(this).attr('data-customer-response') });
                        return false;
                    });
                }
                DAVE_SETTINGS.execute_custom_callback("after_thumbnail_response", [data['data'][dave_response_dataType], data['data'], data, s]);
            });
            DAVE_SETTINGS.execute_custom_callback("after_thumbnails_response", [data['data'][dave_response_dataType], data['data'], data]);
        } else if (dave_response_dataType == 'image') {
            DAVE_SETTINGS.execute_custom_callback("before_image_response", [data['data'][dave_response_dataType], data['data'], data]);
            let s = djQ(`
                <div class='dave-chat-items-contt' data-url='${encodeURI(data.data.url)}' data-customer-state='${data.data.customer_state}' data-customer-response='${data.data.customer_response || data.data.title || DAVE_SETTINGS.toTitleCase(data.data.customer_state)}'>
                    <div class='dave-chat-imageResponse'><img class='dave-imgResponse-image' src='${data['data']['image']}'></div>
                </div>
            `);
            jquery_element[jquery_function](s);
            if (data.data.customer_state || data.data.customer_response) {
                djQ(s).on('click', function () {
                    userTextMsgBubble(djQ(this).attr('data-customer-response'), 'normal', dateTiming(), djQ.now());
                    botchat_data({ 'query_type': 'click', 'customer_state': djQ(this).attr('data-customer-state'), 'customer_response': djQ(this).attr('data-customer-response') });
                    return false;
                });
            }
            DAVE_SETTINGS.execute_custom_callback("after_image_response", [data['data'][dave_response_dataType], data['data'], data, s]);
        } else if (dave_response_dataType == "form") {
            DAVE_SETTINGS.execute_custom_callback("before_form_response", [data['data'][dave_response_dataType], data['data'], data]);
            form_error = '';
            let dbld = (chatType == 'history' && count < dave_return_size) ? 'disabled' : ''
            let ths1 = djQ(`
                <div class="dave-cb-form">
                    <form class='dave-form' type='POST'></form>
                </div>
            `)
            jquery_element[jquery_function](ths1);
            djQ.each(data['data'][dave_response_dataType], function (key, value) {
                let datetime_converter = { 'datetime': 'text', 'date': 'text', 'time': 'text' }
                datetime_converter['minDT'] = (value.min_date || value.min_time) ? (value.min_date || '1900-01-01') + "T" + (value.min_time || '00:00') : "";
                datetime_converter['maxDT'] = (value.max_date || value.max_time) ? (value.max_date || '2050-12-31') + "T" + (value.max_time || '23:59') : "";
                datetime_converter['step'] = value['step'] || '';
                datetime_converter['default_date'] = new Date(value['default_date'] || null);
                scrollMonth: false;
                scrollTime: false;
                scrollInput: false;
                let formats = {
                    'datetime': 'd/m/Y H:i',
                    'date': 'd/m/Y',
                    'time': 'H:i'
                }
                if (value['format']) {
                    for (let k in value['formats']) {
                        formats[k] = value['formats'][k];
                    }
                }
                value['title'] = value['title'] || DAVE_SETTINGS.toTitleCase(value['name']);
                if (value['ui_element'] == "textarea") {
                    ths1.find('form').last().append(`<textarea name='${value['name']}' placeholder='${value['placeholder']}${value['required'] ? '*' : ''}' value='${value['value'] || ''}' title='${value['title']}' ${value['required'] ? 'required' : ''} ui_element='${value['ui_element']}' ${dbld}></textarea>`);
                } else if (value['ui_element'] == "select") {
                    let s = `<option value="">${value['placeholder']}${value['required'] ? '*' : ''}</option>`
                    djQ.each(value['options'] || [], function (k, v) {
                        s += `<option value=${typeof (k) == 'string' ? k : (typeof (v) != 'string' ? v[0] : v)}>${typeof (v) != 'string' ? v[1] : v}</option>`
                    })
                    let s1 = djQ(`<select name='${value['name']}' title='${value['title']}' ${value['required'] ? 'required' : ''} ui_element='${value['ui_element']}' ${dbld}>${s}</select>`);
                    ths1.find('form').last().append(s1);
                    if (value['url'] && value['name_attribute'] && djQ(s1).select2) {
                        let url = value['url'];
                        if (!value['url'].startsWith('http')) {
                            url = DAVE_SETTINGS.BASE_URL + value[url]
                        }
                        let st = value['search_term'] || '_keywords';
                        let mt = value['method'] || 'GET';
                        let name_field = value['name_attribute'];
                        let id_field = value['id_attribute'] || name_field;
                        djQ(s1).select2({
                            ajax: {
                                url: url,
                                data: function (params) {
                                    let r = {
                                        "_page_number": (params.page || 1),
                                        "_page_size": 10
                                    }
                                    if (st == '_keywords') {
                                        r[st] = params.term;
                                    } else {
                                        r[st] = '~' + params.term;
                                    }
                                    DAVE_SETTINGS.execute_custom_callback("on_select2_form", [djQ(s1), value, r]);
                                    return r
                                },
                                processResults: function (data) {
                                    return {
                                        results: data.data.map(function (obj) { return { id: obj[id_field], text: obj[name_field] } }),
                                        pagintion: {
                                            more: (!data.is_last)
                                        }
                                    }
                                },
                                delay: 500,
                                headers: DAVE_SETTINGS.getCookie("dave_authentication"),
                                contentType: 'application/json',
                                dataType: "json",
                            }
                        })
                    }
                    djQ(s1).on('change', function () {
                        DAVE_SETTINGS.execute_custom_callback("on_select_form_element", [djQ(s1), value]);
                    });
                } else if (value['ui_element'] == "file") {
                    let file_typeAllowed = value['file_type'];
                    let max_uploadSize = value['max_file_size'];
                    ths1.find('form').last().append(`
                        <label class='file_upload_label'>${value['title']}</label>
                        <input type='file' name='${value['name']}' class='file_upload' accept="${file_typeAllowed}" data-max='${max_uploadSize}' ${value['required'] ? 'required' : ''} ui_element='${value['ui_element']}' title='${value['title']}' ${dbld}>
                    `);
                } else if (value['ui_element'] == "geolocation") {
                    // *********************************
                    if (chatType !== "history") {
                        map_enabled = true;
                        djQ(".dave-cb-form form").last().before(`
                        <input type="text" name="${value["name"]}" id="locationInput" class="mapLocationInput" placeholder="${value['placeholder']}"${value['required'] ? '*' : ''} title="${value['title']}" error='${value['error'] || ''}' ui_element='${value['ui_element']}'} >
                        `);
                        ths1.find('form').last().append(`
                            <div class="mapBox">
                                <div id="map"></div>
                                <div id="infowindow-content">
                                    <span id="place-name" class="title"></span><br />
                                    <span id="place-address"></span>
                                </div>
                            </div>
                        `);
                        initializeMap();
                    } else if (chatType == 'history' && count >= dave_return_size) {
                        ths1.find('form').last().append(`
                            <input type="text" name="${value["name"]}" placeholder="${value['placeholder']}${value['required'] ? '*' : ''}" title="${value['title']}" required="value" error='${value['error'] || ''}' ui_element='${value['ui_element']}' ${value['required'] ? 'required' : ''} disabled>
                        `);
                    }
                    // *********************************
                } else if (value['ui_element'] == "tel") {
                    let s = djQ(`
                            <input type='text' class='tel' name='${value['name']}' placeholder='${value['placeholder']}${value['required'] ? '*' : ''}' value='${value['value'] || ''}' title='${value['title']}' ${value['required'] ? 'required' : ''} autocomplete="off" error='${value['error'] || 'Please enter a valid mobile number'}' ui_element='${value['ui_element']}' validate="[+]{0,1}[0]{0,2}[1-9][0-9]{9,12}" ${dbld}>
                    ` );
                    ths1.find('form').last().append(s);
                    djQ(s).ForcePhoneNumbersOnly();
                } else if (value['ui_element'] == "email") {
                    let s = djQ(`
                            <input type='text' class='tel' name='${value['name']}' placeholder='${value['placeholder']}${value['required'] ? '*' : ''}' value='${value['value'] || ''}' title='${value['title']}' ${value['required'] ? 'required' : ''} autocomplete="off" error='${value['error'] || 'Please enter a valid email'}' ui_element='${value['ui_element']}' validate="\\w+([\\.-_]?\\w+)*@\\w+([\\.-_]?\\w+)*(\\.\\w{2,5})+" ${dbld}>
                    ` );
                    ths1.find('form').last().append(s);
                }
                else if (value['ui_element'] == "checkbox") {
                    let s = djQ(`<div class='checkbox'>  
                    </div>
                    ` );
                    let input = djQ(`<input type='checkbox' class='checkbox' id='${value['value']}' name='${value['name']}' placeholder='${value['placeholder']}' value='${value['placeholder'] || ''}' title='${value['title']}' autocomplete="off" ui_element='${value['ui_element']}' ${dbld}>`)
                    let sc = djQ(`<label class='checkbox' for='${value['value']}' name="${value['name']} ">${value['placeholder']}</label>`)
                    s.append(input)
                    s.append(sc);
                    ths1.find('form').last().append(s);
                }
                else {
                    ths1.find('form').last().append(`<input type='${datetime_converter[value['ui_element'] || 'text'] || value['ui_element'] || 'text'}' class='${value['ui_element']}' name='${value['name']}' placeholder='${value['placeholder']}${value['required'] ? '*' : ''}' value='${value['value'] || ''}' title='${value['title']}' ${value['required'] ? 'required' : ''} min='${datetime_converter['minDT']}' max='${datetime_converter['maxDT']}' step='${datetime_converter['step']}' autocomplete="off" error='${value['error'] || ''}' ui_element='${value['ui_element']}' validate="${value['validate'] || ''}" ${dbld}>`);
                }
                if (['datetime', 'date', 'time'].indexOf(value['ui_element']) >= 0) {
                    let t = {
                        minDate: value.ui_element != 'time' ? value['min_date'] : false,
                        maxDate: value.ui_element != 'time' ? value['max_date'] : false,
                        minTime: value.ui_element != 'date' ? value['min_time'] : false,
                        maxTime: value.ui_element != 'date' ? value['max_time'] : false,
                        defaultTime: value['default_time'],
                        defaultDate: datetime_converter.default_date,
                        step: value['step'] || (parseInt(value['time_resolution'] || "0") * 60) || 3600,
                        mask: true,
                        scrollMonth: false,
                        scrollTime: false,
                        scrollInput: false,
                        value: value.value ? new Date(value['value']) : '',
                        format: formats[value.ui_element],
                        timepicker: value['ui_element'] == 'date' ? false : true,
                        datepicker: value['ui_element'] == 'time' ? false : true,
                        allowBlank: !value['required'],
                        lazyInit: value.value ? false : true,
                        initTime: true,
                    };
                    djQ('.dave-form input[name="' + value['name'] + '"]').datetimepicker(t);
                }
                if (chatType == "normal") {
                    form_error += value['error'] + `<br /><br />`;
                }

            });
            if (chatType == "history") {
                /////////you have to make a count variable here
                if (count == dave_return_size) {
                    ths1.find('form').last().append(`<input type='submit' value='${data.data.submit_value || "submit"}'>`);
                } else {
                    ths1.find('form').last().append(`<input type='submit' value='${data.data.submit_value || "submit"}' disabled>`);
                }
            } else if (chatType == "normal") {
                let s = djQ(`<input type='submit' value='${data.data.submit_value || "submit"}'>`);
                ths1.find('form').last().append(s);
            }
            ths1.append(`<p class='hidden'>${data['data']['customer_state']}</p>`);
            DAVE_SETTINGS.execute_custom_callback("after_form_response", [data['data'][dave_response_dataType], data['data'], data, ths1]);
        } else if (dave_response_dataType == "url") {
            DAVE_SETTINGS.execute_custom_callback("before_url_response", [data['data'][dave_response_dataType], data['data'], data]);
            let s = djQ(`<a href="${encodeURI(data['data']['url'])}" target='${data['data']['target'] || '_blank'}'>${data['data']['title'] || data['data']['url']}</a>`);
            jquery_element[jquery_function](s);
            s.attr('href', encodeURI(data['data']['url']))
            djQ(s).on('click', function () {
                DAVE_SETTINGS.execute_custom_callback('on_url_click');
            })
            if (data.data['sub_title']) {
                jquery_element[jquery_function](`${data.data['sub_title']}`);
            }
            DAVE_SETTINGS.execute_custom_callback("after_url_response", [data['data'][dave_response_dataType], data['data'], data, s]);
        } else if (dave_response_dataType == "multi_url") {
            DAVE_SETTINGS.execute_custom_callback("before_multi_url_response", [data['data'][dave_response_dataType], data['data'], data]);
            for (let u of data['data']['url']) {
                let s = djQ(`<a href="${encodeURI(u['url'])}" target='${u['target'] || '_blank'}'>${u['title'] || u['url']}</a>`);
                jquery_element[jquery_function](s);
                s.attr('href', encodeURI(u['url']))
                djQ(s).on('click', function () {
                    DAVE_SETTINGS.execute_custom_callback('on_url_click');
                })
                if (u['sub_title']) {
                    jquery_element[jquery_function](`${u['sub_title']}`);
                }
            }
            DAVE_SETTINGS.execute_custom_callback("after_multi_url_response", [data['data'][dave_response_dataType], data['data'], data, s]);
        } else if (dave_response_dataType == "html") {
            DAVE_SETTINGS.execute_custom_callback("before_html_response", [data['data'][dave_response_dataType], data['data'], data]);
            let s = djQ(data['data']['html']);
            jquery_element[jquery_function](s);
            DAVE_SETTINGS.execute_custom_callback("after_html_response", [data['data'][dave_response_dataType], data['data'], data, s]);
        } else if (dave_response_dataType == "video") {
            DAVE_SETTINGS.execute_custom_callback("before_video_response", [data['data'][dave_response_dataType], data['data'], data]);
            let s = djQ(`
            <a href='${data['data']['video']}' target='${data['data']['target']}'>
                <div class='dave-videoplayer'>
                    <video controls width='100%' height='200px' autoplay muted><source src='${data['data']['video']}'>Your Browser Does Not Support Video Player Try With Chrome</video>
                </div>
            </a>
            `);
            jquery_element[jquery_function](s);
            DAVE_SETTINGS.execute_custom_callback("after_video_response", [data['data'][dave_response_dataType], data['data'], data, s]);
        } else if (dave_response_dataType == "custom" || dave_response_dataType == "custom_function") {
            let data_func_name = data['data']['custom_function'];
            DAVE_SETTINGS.execute_custom_callback("before_custom_response", [data['data'][data_func_name], data['data'], data]);
            let data_func = window[data_func_name];
            let s;
            if (data_func && typeof data_func == "function") {
                s = data_func(jquery_element, data["data"][data_func_name], chatType);
            }
            DAVE_SETTINGS.execute_custom_callback("after_custom_response", [data['data']['option'], data['data'], data, s]);
        } else if (dave_response_dataType == "template") {
            DAVE_SETTINGS.execute_custom_callback("before_template_response", [data['data'][dave_response_dataType], data['data'], data]);
            let template_name = data['data']['template']
            // FUNCTION TO ADD HTML TEMPLATE
            function call_ajax(template_name, jquery_element, jquery_function, asset_paths, callback, errorCallback) {
                let s;
                let asset_path = asset_paths || [DAVE_ASSET_PATH];
                if (!asset_path.length) {
                    if (errorCallback && typeof (errorCallback) == 'function') {
                        errorCallback();
                    }
                }
                // asset_path = asset_path.shift()
                /* the response will be like this
                "response_type": "template",
                "template": "mytemplate",
                "mytemplate": {
                    ..data which is needed to execute the template...
                }*/
                djQ.ajax({
                    url: asset_paths + '/templates/' + template_name + '.html',
                    success: function (html_data) {
                        html = djQ(html_data)
                        jquery_element[jquery_function](html);
                        s = html.attr('data-dave-function')
                        let fnn = 'dave_template_' + s + '_function';
                        if (window[fnn] && typeof (window[fnn]) == 'function') {
                            window[fnn](html, data['data'][template_name], chatType, data['data'], data)
                        }
                        if (callback && typeof (callback) == 'function') {
                            callback();
                        }
                    },
                    404: function () {
                        call_ajax(template_name, jquery_element, jquery_function, asset_paths, callback, errorCallback);
                    },
                    error: errorCallback
                });
                let assets_paths = [DAVE_ENVIRONMENT.APP_PATH, DAVE_ASSET_PATH];
                if (!DAVE_ENVIRONMENT || !DAVE_ENVIRONMENT.APP_PATH) {
                    assets_paths.shift()
                }
                // call_ajax(template_name, assets_paths, function () {
                //     DAVE_SETTINGS.execute_custom_callback("after_template_response", [data['data'][dave_response_dataType], data['data'], data, s]);
                // }, function () {
                //     console.error("Error fetching template name", template_name)
                // })
            }
            call_ajax(template_name, jquery_element, jquery_function, DAVE_ENVIRONMENT.APP_PATH)
        }
        if (dave_response_dataType == "options" || (!djQ.isEmptyObject(data['data']['options']))) {
            DAVE_SETTINGS.execute_custom_callback("before_options_response", [data['data']['option'], data['data'], data]);
            let thl = djQ(`
                </br><ul class='dave-option-list'></ul>
            `)
            jquery_element[jquery_function](thl);
            console.debug(data['data']['options']);
            djQ.each(data['data']['options'], function (key, value) {
                if (typeof key != "string") {
                    if (typeof value != "string") {
                        key = value[0];
                        value = value[1];
                    } else {
                        key = data["customer_state"] || null;
                    }
                }
                thl.append(`<li><input type='hidden' value='${key}'><button>${value}</button></li>`);
            });
            DAVE_SETTINGS.execute_custom_callback("after_options_response", [data['data']['option'], data['data'], data, thl]);
        }
    }

    //FUNCTION TO TAKE USER MESSAGE AND PRINT IN CHATBOT AND PASS AS MESSAGE TO SYSTEM
    function userchat_data() {
        if ((DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response')) && !chat_historyActivated) {
            userInputBubble('normal')
        }
        /*IF HISTORY IS LOADED THEN THIS BLOCKS GET EXECUTED FOR ONCE*/
        else if ((DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id')) && chat_historyActivated) {
            userInputBubble('history');
        }
    }


    //history - CHECKING AND PRINTING USER CHAT HISTORY
    function on_load_history(data, back) {
        DAVE_SETTINGS.execute_custom_callback("before_load_history", [data]);
        let count = 0;
        chat_loading = true;
        djQ.each(data['history'], function (his_key, his_value) {
            count++;
            if (his_value['direction'] == 'system') {
                dave_response_dataType = his_value['data']['response_type'];
                his_value['timestamp'] = DAVE_SETTINGS.print_timestamp(his_value['timestamp']);
                //print bot reply as message
                c_bubbleId = his_value['response_id'];
                c_bubbleId = "RI" + c_bubbleId;
                if (!back) {
                    if (count > 1) {
                        if (typeof his_value['reply_to'] == "undefined") {
                            msgBubbles('bot', 'history', his_value['timestamp'], '', c_bubbleId);
                        } else {
                            msgBubbles('bot', 'history', his_value['timestamp'], '', c_bubbleId, his_value['response_id']);
                        }
                    } else {
                        msgBubbles('bot', 'history', his_value['timestamp'], '');
                    }
                    djQ(".dave-chattext").last().html(his_value["placeholder"]);

                    if (his_value['state_options'] && !djQ.isEmptyObject(his_value['state_options'])) {
                        djQ(".dave-cb-chatsugg").append(`<span class='chatsugg-next'><img src='${cb_sugg_next}'></span><span class='chatsugg-prev'><img src='${cb_sugg_prev}'></span>`)

                    }
                    response_elementPrint(dave_response_dataType, his_value, 'history', count);
                    if (his_value['state_options'] && !djQ.isEmptyObject(his_value['state_options'])) {
                        djQ('.dave-cb-chatsugg').html('');
                        //print bot suggestion
                        for (const [key, value] of Object.entries(his_value['state_options'])) {
                            djQ('.dave-cb-chatsugg').append(`<div class='dave-cb-chatsugg-list'>${value}<input type='hidden' value='${key}'></div>`);
                        }
                    }
                }

                //QUICKACCESS BUTTON NEXT & PREV ONLY VISIBLE ON SCROLL IS AVAILABLE
                let chatsugg_scrollWidth = djQ('.dave-cb-chatsugg')[0].scrollWidth;

                if (chatsugg_scrollWidth > (chatbox_width - 20)) {
                    djQ('span.chatsugg-next').css('display', 'block');

                    if (dave_optionTitle) {
                        djQ('span.chatsugg-next').css('top', '10px');
                        djQ('span.chatsugg-prev').css('top', '10px');
                    } else {
                        djQ('.dave-chatsuggTitle').hide();
                    }

                }
            } else if (his_value['direction'] == 'other') {
                c_bubbleId = his_value['response_id'];
                c_bubbleId = "RI" + c_bubbleId;
                replyTo = "RI" + his_value['reply_to']
                other_chat_data(null, his_value['user_icon'], his_value['user_name'], his_value['timestamp'], his_value['placeholder'], replyTo, his_value, c_bubbleId)
            } else {
                //user chat printing
                his_value['timestamp'] = DAVE_SETTINGS.print_timestamp(his_value['timestamp']);
                c_bubbleId = his_value['response_id'];
                c_bubbleId = "RI" + c_bubbleId;
                if (!back) {
                    msgBubbles('user', 'history', his_value['timestamp'], DAVE_SETTINGS.pretty_print(DAVE_SETTINGS.clean_input(his_value['customer_response']), '_name_map', '_type_map'), c_bubbleId, '', his_value['customer_state']);
                }
            }
            //Chat box height Adjustment according to device
            set_inner_height();
            if (count == dave_return_size) {
                //
                djQ('button#start-recording').prop('disabled', false);
                //
                djQ(".dave-cb-chatarea").animate({ scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight") }, 500);
                dave_typingLoaderRemove('bot');
            }
        });

        dave_userchatfunc_exe = false;
        chat_historyActivated = true;
        userchat_data();
        DAVE_SETTINGS.execute_custom_callback("after_load_history", [data]);
    }
    window.botchat_history = function (page_number) {
        page_number = page_number || DAVE_SETTINGS.history_page_number || 1
        set_inner_height();
        dave_chatRes_loader('bot');
        DAVE_SETTINGS.history(function (data) {
            djQ('button#start-recording').prop('disabled', true);
            dave_return_size = Object.keys(data['history']).length;
            if (dave_return_size > 0) {
                on_load_history(data);
            } else {
                //If no history available
                chat_historyActivated = false;
                botchat_data({ "query_type": "auto" });
            }
        }, function (e) {
            chatResponseError('history', e);
        }, function (data) {
            dave_return_size = Object.keys(data['history']).length;
            if (dave_return_size > 0) {
                on_load_history(data);
            }
        });
    }
    //USER INPUT MESSAGE SENDING TO SYSTEM ON REGULAR AND HISTORY CHAT
    let temp_bubbleId = new Array();
    let perm_bubbleId = new Array();
    function userInputBubble(chatType) {
        let dave_user_typeloader = 0;
        let clicked_pred_custState;
        djQ("#dave-cb-textinput").on("input", function () {
            text_cleared = false;
            djQ("#dave-cb-usersend").off('click').on('click', function () {
                c_bubbleId = djQ.now();
                temp_bubbleId.push(c_bubbleId);
                let dave_user_inputtext = DAVE_SETTINGS.clean_input(djQ("#dave-cb-textinput").val());
                if (dave_user_inputtext.length >= 1) {
                    dave_typingLoaderRemove('user');
                    userTextMsgBubble(dave_user_inputtext, 'normal', dateTiming(), c_bubbleId);
                    scrollChatarea(500);
                    djQ(".dave-pred-contt").html('');
                    djQ(".dave-pred-contt").css({ "height": "0", "overflow": "hidden", "padding": "0" });
                    if (!dave_predication_send) {
                        botchat_data({ 'customer_response': dave_user_inputtext, "query_type": "type" }, c_bubbleId);
                    } else {
                        botchat_data({ 'customer_response': dave_user_inputtext, 'customer_state': clicked_pred_custState, "query_type": "click" }, c_bubbleId);
                    }
                    //Chat box height Adjustment according to device
                    set_inner_height();
                    djQ("#dave-cb-textinput").val('');
                    text_cleared = true;
                    dave_user_typeloader = 0;

                    if (chatType == 'normal') {
                        stopTimmer();
                    } else if (chatType == 'history') {
                        chat_historyActivated = false;
                    }
                }
            });
            text_input = (this.value.trim()).replace(/\s/g, "");
            let dave_input_length1 = this.value.length
            let dave_input_length = text_input.length;
            let dave_input_last = this.value.charAt(dave_input_length1 - 1)
            let dave_input_words = this.value.trim().split(' ').length;
            dave_predication_send = false;
            if (dave_input_length > 0 && dave_user_typeloader == 0) {
                dave_typingLoaderRemove('user');
                dave_user_typeloader = 1;
                if (dave_user_typeloader = 1) {
                    //User typing gif
                    dave_chatRes_loader('user');
                    scrollChatarea(500);
                }
            } else if (dave_input_length == 0) {
                dave_user_typeloader = 0;
                dave_typingLoaderRemove('user');
                djQ('.dave-pred-contt').html('');
                djQ(".dave-pred-contt").css({ "height": "0", "overflow": "hidden", "padding": "0" });
                pred_attach = false;
            } else if (dave_input_length >= 2 && !dave_userchatfunc_exe) {
                //User click on send icon
                //user all data print as message bubble
                //user all data then send to server
                if (dave_input_length < 10 || (dave_input_length < 20 && (dave_input_length % 2 == 0))  || (dave_input_length < 30 && (dave_input_length % 3 == 0))|| (dave_input_length % 5 == 0)) {
                    //console.log(`Sending prediction for ${text_input} - ${dave_input_length} - ${dave_input_last} - ${dave_input_words}`)
                    let t = this;
                    setTimeout(function () {
                        dave_prediction(t);
                    }, 2
                    );
                } else {
                    //console.log(`Not sending prediction for ${text_input} - ${dave_input_length} - ${dave_input_last} - ${dave_input_words}`)
                }
            }
        });
    }
    //SERVER RESPONSE PRINTING FUNCTION BLOCK ENDS ENDS ENDS
    //THE CHATBOT FRONT-END ELEMENTS ASSEMBLING STARTS
    djQ('#dave-settings').append(`
        <div class='dave-cb-avatar-contt'>
            <canvas id='dave-canvas'></canvas>
        </div>
        <div class='dave-chatbox-cont'>
            <div class='dave-chatbox'>
                <div class='dave-cb-tt-sec'>
                        <div class='dave-tt-sec1'>
                            <div class='dave-bottitleicon'>
                                <img src='${dave_title_icon}'>
                            </div>
                            <div class='dave-cb-tt'> 
                                ${dave_chatTitle} 
                            </div>
                        </div>
                        <div class='close-min-max'>
                            <div id='minchat' class='dave-cb-tt-minmax'>
                                <img src=''>
                            </div>
                            <div class='dave-cb-tt-cross'>
                                <img src=${cross_img} />
                            </div>
                        </div>
                    </div>
                <div class='dave-cb-chatarea'></div>
                <div class='dave-cb-stickBottom'>
                    <p class="dave-chatsuggTitle">${dave_optionTitle}</p>
                    <div class='dave-cb-chatsugg'></div>
                    <div class='dave-cb-type-sec'>
                        <div class='dave-pred-contt'></div>
                        <div class='dave-cb-input'>
                        ${homeBtnImg ? `<button class="dave-homeBtn"><img src="${homeBtnImg}" alt="Home"></button>` : ""}
                            <input type='text' name='chatbot-input-box' id='dave-cb-textinput' placeholder='Type here..' autocomplete="off">
                            <div class='dave-cb-send-icon'>
                                <button id='dave-cb-usersend'>
                                    <img src='${send_img}' />
                                </button>
                            </div>
                        </div>
                        <div class="dave-mic-contt">
                            <button class='dave-mic-button rec-start' id='start-recording'><img src="${mic_img}"></button>
                            <button class='dave-mic-button rec-stop' id='stop-recording'><img src="${mic_img}"></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class='dave-cb-icon'>
                <img src='${dave_chatbot_icon}'>
            </div>
        </div>
        `)
    DAVE_SETTINGS.execute_custom_callback('on_add_chatbot_dom')
    djQ(window).resize(function () {
        if (djQ(window).width() != dave_window_width || djQ(window).height() != dave_window_height) {
            if (!djQ('input').is(':focus') && DAVE_SCENE.maximized) {
                set_maximized_height();
            }
        }
        set_inner_height();
    });
    /*HE CHATBOT FRONT-END ELEMENTS ASSEMBLING ENDS*/

    //CODE BLOCK TO AVOID OVERLAPPING CHATBOX ON OTHER PAGE ELEMENTS ON CLOSE
    chatbox_width = djQ(".dave-chatbox-cont").width();
    djQ(".dave-chatbox-cont").width(0);

    //FUNTION TO PRINT ALL BOT RESPONSES IN CHAT-BOX PROVIDED BY DAVE_SETTINGS.chat
    window.botchat_data = function botchat_data(params, bubbleId, behaviour, nudgeopenState, lockHeight) {
        //bot chat typing gif
        if (close_count > 0) {
            return
        }
        dave_chatRes_loader('bot');
        djQ('button#start-recording').prop('disabled', true);
        //pass user input to chat function
        params = params || {};
        behaviour = behaviour || false;
        nudgeopenState = nudgeopenState || false;
        lockHeight = lockHeight || maxLockHeight;
        //botchat_data({ 'customer_state': dave_clicked_sugg_value, 'customer_response': dave_clicked_sugg_message, "query_type": "click" }, c_bubbleId);
        DAVE_SETTINGS.chat(params, function (data) {
            let dave_return_size = Object.keys(data).length;
            response_print(data, dave_return_size, dateTiming('date'), "normal");
        }, function (e, f) {
            chatResponseError('normal', e, params, bubbleId, behaviour, nudgeopenState, lockHeight);
        });
        if (!DAVE_SCENE.playable && nudgeopenState == 'avatar') {
            nudgeopenState = 'min'
        }
        if (DAVE_SCENE.is_mobile == true) {
            if (nudgeopenState != 'avatar') {
                nudgeopenState = 'max'
            }
        }
        if (behaviour == 'open') {
            if (nudgeopenState == 'min') {
                if (!DAVE_SCENE.opened) {
                    djQ('.dave-cb-icon').trigger('click');
                    minimize_chatbox(lockHeight);
                } else if (!DAVE_SCENE.maximized) {
                    minimize_chatbox(lockHeight);
                }
            } else if (nudgeopenState == 'max') {
                if (!DAVE_SCENE.opened) {
                    djQ('.dave-cb-icon').trigger('click');
                }
                if (!DAVE_SCENE.maximized) {
                    maximize_chatbox();
                }
            } else if (nudgeopenState == 'avatar') {
                showAvatar(true);
                DAVE_SETTINGS.on_show_avatar()
            }
        }
    }


    //QUICK ACCESS BUTTON CODE BLOCK
    djQ(document).on("click", ".dave-cb-chatsugg-list", function () {
        stopTimmer();
        let dave_clicked_sugg_value = djQ(this).children().val();
        let dave_clicked_sugg_message = djQ(this).text();
        c_bubbleId = djQ.now();
        temp_bubbleId.push(c_bubbleId);
        userTextMsgBubble(dave_clicked_sugg_message, 'normal', dateTiming(), c_bubbleId);
        scrollChatarea(500);
        djQ("#dave-cb-textinput").val('');
        botchat_data({ 'customer_state': dave_clicked_sugg_value, 'customer_response': dave_clicked_sugg_message, "query_type": "click" }, c_bubbleId);
    });

    //OPITONS CODE BLOCK (IN MSG-BUBBLE)
    djQ(document).on("click", ".dave-option-list li button", function () {
        stopTimmer();
        let curr_clicked_option_key = djQ(this).prev().val();
        let curr_clicked_option_val = djQ(this).text();
        c_bubbleId = djQ.now();
        temp_bubbleId.push(c_bubbleId);
        userTextMsgBubble(curr_clicked_option_val, 'normal', dateTiming(), c_bubbleId)
        scrollChatarea(500);
        djQ("#dave-cb-textinput").val('');
        botchat_data({ "customer_state": curr_clicked_option_key, "customer_response": curr_clicked_option_val, "query_type": "click" }, c_bubbleId);
    });

    //Open Chat Box using Chatbox Icon
    djQ(".dave-cb-icon").on("click", function () {
        if (DAVE_SETTINGS.on_maximize() === false) {
            return;
        }
        djQ(".dave-cb-tt-cross").removeAttr('disabled');
        djQ(".dave-chatbox").addClass("dave-chatbox-open");
        djQ(".dave-cb-icon").addClass("dave-cb-icon-hide");
        djQ(".dave-chatbox-cont").width(chatbox_width);
        djQ(".dave-cb-tt-minmax img").remove();
        djQ(".dave-cb-tt-minmax").append(`<img src='${min_img}'>`);

        chatboxOpenStatus = "open";

        set_maximized_height();
        djQ(".dave-cb-tt-minmax").unbind("click").one("click", function () {
            minimize_chatbox();
        })
        djQ(".dave-cb-chatarea").animate({ scrollTop: djQ('.dave-cb-chatarea').prop("scrollHeight") }, 500);
        dave_chat_heightchange = true;

        if (dave_window_width < 450) {
            djQ('body').css("overflow", "hidden");
        }

        if (DAVE_SCENE.loaded) {
            showAvatar(true);
        }
    });

    //Send user data to chat box using Enter Button
    djQ('#dave-cb-textinput').off('keypress').on('keypress', function (e) {
        let daveInput_length = this.value.length;
        if (daveInput_length >= 1 && !dave_userchatfunc_exe) {
            if (e.which == 13) {
                djQ("#dave-cb-usersend").trigger('click');
                e.preventDefault();
            }
        }
    });

    //Close chatbox using Cross "close" sign
    djQ(".dave-cb-tt-cross").click(function () {
        //feedback on close
        if (djQ(this).attr('disabled')) {
            return;
        }
        close_count++;
        now = new Date().getTime();
        previous_time = parseInt(DAVE_SETTINGS.getCookie('dave_previous_feedback_time')) || data_feedback_redo_time * 1000;
        time_diff = Math.max((now - previous_time), 0);

        if (DAVE_SCENE.loaded || data_avatar_initChk) {
            showAvatar(false);
        }

        if (time_diff >= (data_feedback_redo_time * 1000) && chat_loading && close_count < 2 && DAVE_SETTINGS.did_interact) {
            stopTimmer();
            maximize_chatbox();
            djQ(".dave-cb-tt-minmax").unbind("click");
            let userfull_rate_value = 0;
            let accuracy_rate_value = 0;
            djQ('.dave-cb-chatarea').html('');
            djQ('.dave-cb-chatarea').css('padding', '0');
            djQ('.dave-cb-stickBottom').css('display', 'none');
            let feedbackTextDiv = `<div class='feedback-row-box'><textarea id='dave-feedback-write' placeholder='${feedbackTextPlaceholder}'></textarea></div>`
            djQ('.dave-cb-chatarea').append(`
                        <div class="rating-star-bg"></div>
                        <div class='rating-star-contt'>
                            ${(
                    WIH_RatingTitle != "__NULL__" ? `
                                        <p>${WIH_RatingTitle || "Was I Helpful?"}</p>
                                        <div class="dave-rate feedback-row-box" id='dave-userfull-rate'>
                                            <input type="radio" id="dave-usefull-star5" name="dave-usefull-rate" value="5" />
                                            <label for="dave-usefull-star5" title="dave-usefull-text">5 stars</label>
                                            <input type="radio" id="dave-usefull-star4" name="dave-usefull-rate" value="4" />
                                            <label for="dave-usefull-star4" title="dave-usefull-text">4 stars</label>
                                            <input type="radio" id="dave-usefull-star3" name="dave-usefull-rate" value="3" />
                                            <label for="dave-usefull-star3" title="dave-usefull-text">3 stars</label>
                                            <input type="radio" id="dave-usefull-star2" name="dave-usefull-rate" value="2" />
                                            <label for="dave-usefull-star2" title="dave-usefull-text">2 stars</label>
                                            <input type="radio" id="dave-usefull-star1" name="dave-usefull-rate" value="1" />
                                            <label for="dave-usefull-star1" title="dave-usefull-text">1 star</label>
                                        </div>
                                    ` : ""
                )
                }
                            ${(
                    WIA_RatingTitle != "__NULL__" ? `
                                        <p>${WIA_RatingTitle || "Was I Accurate?"}</p>
                                        <div class="dave-rate feedback-row-box" id='dave-accuracy-rate'>
                                            <input type="radio" id="dave-accuracy-star5" name="dave-accuracy-rate" value="5" />
                                            <label for="dave-accuracy-star5" title="dave-accuracy-text">5 stars</label>
                                            <input type="radio" id="dave-accuracy-star4" name="dave-accuracy-rate" value="4" />
                                            <label for="dave-accuracy-star4" title="dave-accuracy-text">4 stars</label>
                                            <input type="radio" id="dave-accuracy-star3" name="dave-accuracy-rate" value="3" />
                                            <label for="dave-accuracy-star3" title="dave-accuracy-text">3 stars</label>
                                            <input type="radio" id="dave-accuracy-star2" name="dave-accuracy-rate" value="2" />
                                            <label for="dave-accuracy-star2" title="dave-accuracy-text">2 stars</label>
                                            <input type="radio" id="dave-accuracy-star1" name="dave-accuracy-rate" value="1" />
                                            <label for="dave-accuracy-star1" title="dave-accuracy-text">1 star</label>
                                        </div>
                                    ` : ""
                )
                }
                            ${writeFeedback ? feedbackTextDiv : ''}
                            <div class='feedback-row-box'><button id='dave-final-feedbackButton' tittle='Submit'>Submit</button></div>
                        </div>
                    `);

            if (WIH_RatingTitle != "__NULL__") {
                djQ('#dave-userfull-rate input').on('change', function () {
                    if (djQ(this).is(':checked')) {
                        userfull_rate_value = djQ(this).val();
                    }
                });
            }
            if (WIA_RatingTitle != "__NULL__") {
                djQ('#dave-accuracy-rate input').on('change', function () {
                    if (djQ(this).is(':checked')) {
                        accuracy_rate_value = djQ(this).val();
                    }
                });
            }

            DAVE_SETTINGS.execute_custom_callback('on_feedback_form_show', ['#dave-userfull-rate', '#dave-accuracy-rate', '#dave-feedback-write'])
            djQ('#dave-final-feedbackButton').on("click", function () {
                djQ(this).prop("disabled", true);
                DAVE_SETTINGS.execute_custom_callback("before_submit_feedback");
                djQ(".dave-error").remove();
                let dave_feedback_write = djQ('#dave-feedback-write').val() || null;
                djQ('.rating-star-contt .dave-error .dave-valid').remove();
                if (WIA_RatingTitle == "__NULL__") { accuracy_rate_value = null; }
                if (WIH_RatingTitle == "__NULL__") { userfull_rate_value = null; }
                if (
                    (
                        WIA_RatingTitle == '__NULL__' || accuracy_rate_value > 0
                    ) && (
                        WIH_RatingTitle == "__NULL__" || userfull_rate_value > 0
                    )
                ) {
                    djQ(".dave-cb-tt-cross").attr('disabled', true);
                    let fdbk = {
                        "usefulness_rating": userfull_rate_value,
                        "accuracy_rating": accuracy_rate_value,
                        "feedback": dave_feedback_write
                    }
                    DAVE_SETTINGS.feedback(fdbk, null, function (data) {
                        DAVE_SETTINGS.dave_eng_id_status = false;
                        djQ('.dave-error').remove()
                        djQ('.dave-valid').remove();
                        djQ('.rating-star-contt').append(`<p class='dave-valid'>${feedback_successMSG}</p>`);
                        DAVE_SETTINGS.execute_custom_callback("after_submit_feedback", [fdbk]);
                    });
                    setTimeout(function () {
                        if (DAVE_SETTINGS.on_close() === false) {
                            return;
                        }
                        chatboxOpenStatus = 'close';
                        stopTimmer();
                        close_count = 0;
                        djQ('.dave-cb-chatarea').html('');
                        djQ(".dave-chatbox").removeClass("dave-chatbox-open");
                        djQ(".dave-cb-icon").removeClass("dave-cb-icon-hide");
                        if (originalScroll != 'hidden') {
                            djQ('body').css('overflow', originalScroll);
                        } else {
                            djQ('body').css('overflow', '');
                        }
                        console.debug("set back original scroll", originalScroll);
                        djQ(".dave-chatbox-cont").width(0);
                        djQ('.dave-cb-chatarea').css('padding', "10px 15px 0px 15px");
                        DAVE_SETTINGS.setCookie('dave_previous_feedback_time', now);
                        djQ(this).prop("disabled", false);
                        if (!DAVE_SETTINGS.dave_eng_id_status) {
                            djQ('.dave-cb-stickBottom').css('display', 'block');
                            window.botchat_history();
                            DAVE_SETTINGS.dave_eng_id_status = true;
                        }
                        DAVE_SETTINGS.execute_custom_callback("after_submit_feedback_close");
                        DAVE_SETTINGS.execute_custom_callback("after_close_custom");
                    }, 2000);
                } else {
                    djQ(this).prop("disabled", false);
                    if (originalScroll != 'hidden') {
                        djQ('body').css('overflow', originalScroll);
                    } else {
                        djQ('body').css('overflow', '');
                    }
                    console.debug("set back original scroll", originalScroll);
                    djQ('.dave-error').remove()
                    djQ('.dave-valid').remove();
                    djQ('.rating-star-contt').append(`<p class='dave-error'>Please Do Give Star Ratings!</p>`);
                }
            });
        } else {
            chatboxOpenStatus = 'close';
            DAVE_SETTINGS.dave_eng_id_status = false;
            if (DAVE_SETTINGS.on_close() === false) {
                return;
            };
            if (typeof startTimmer === "function") {
                startTimmer();
            }
            if (originalScroll != 'hidden') {
                djQ('body').css('overflow', originalScroll);
            } else {
                djQ('body').css('overflow', '');
            }
            console.debug("set back original scroll", originalScroll);
            close_count = 0;
            djQ('.dave-cb-chatarea').html('');
            djQ('.dave-cb-chatarea').css('padding', '');
            djQ('.dave-cb-stickBottom').css('display', 'none');
            djQ(".dave-chatbox-cont").width(0);
            djQ(".dave-chatbox").removeClass("dave-chatbox-open");
            djQ(".dave-cb-icon").removeClass("dave-cb-icon-hide");

            if (!DAVE_SETTINGS.dave_eng_id_status) {
                djQ('.dave-cb-stickBottom').css('display', 'block');
                window.botchat_history();
                DAVE_SETTINGS.dave_eng_id_status = true;
            }
            DAVE_SETTINGS.execute_custom_callback("after_close_custom");
        }
        // djQ(".dave-cb-tt-minmax").one("click", function () {
        //     minimize_chatbox(maxLockHeight, 'feedback');
        // });
        /*if (dave_window_width > 450) { djQ(".dave-chatbox-cont").css("width", "5%"); } else { djQ(".dave-chatbox-cont").css("width", "25%"); }*/
    });
    //Chat Suggestion Next and Prev Button
    //NEXT BUTTON 
    djQ(".dave-cb-chatsugg").on("click", ".chatsugg-next", function () {
        djQ(".dave-cb-chatsugg").animate({ scrollLeft: "+=50px" }, 300);
        djQ(".dave-cb-chatsugg").on("scroll", function () {
            djQ("span.chatsugg-prev").css("display", "block");
            if (djQ(this).scrollLeft() + djQ(this).innerWidth() >= djQ(this)[0].scrollWidth) {
                djQ("span.chatsugg-next").css("display", "none");
            } else if (djQ(this).scrollLeft() == 0) {
                djQ("span.chatsugg-prev").css("display", "none");
            }
        })
    });
    //PREV BUTTON
    djQ(".dave-cb-chatsugg").on("click", ".chatsugg-prev", function () {
        djQ(".dave-cb-chatsugg").animate({ scrollLeft: "-=50px" }, 300);
        djQ(".dave-cb-chatsugg").on("scroll", function () {
            djQ("span.chatsugg-next").css("display", "block");
            if (djQ(this).scrollLeft() == 0) {
                djQ("span.chatsugg-prev").css("display", "none");
            } else if (djQ(this).scrollLeft() + djQ(this).innerWidth() >= djQ(this)[0].scrollWidth) {
                djQ("span.chatsugg-prev").css("display", "block");
            }
        })
    });

    //THUMBS FEEDBACK FOR BUBBLE
    djQ(document).on('click', '.dave-thumbsup, .dave-thumbsdown', function () {

        eng_id = djQ(this).siblings('span').text();
        curr_rating = djQ(this).attr('data-thumbType');
        like_dislike(this, curr_rating, "normal");
        DAVE_SETTINGS.feedback({ "response_rating": l_d_react }, eng_id, function (data) { });
        djQ(this).closest('.dave-botchat').append(`<p class="dave-feedback-msg">Thank you for your feedback</p>`);
    });

    // Form File Upload
    djQ(document).on("change", ".file_upload", function (data) {
        //add file uploading loader here
        djQ(this).before(`<div class='file_upload_loader'><img src='${typing_gif}'></div>`);
        if (data.target.value.length > 0) {

            let curr_file = djQ(this);
            let maxUploadSize = djQ(this).attr('data-max') || 1024 * 1024 * 1024 * 0.5;
            let selectedFiles = data.target.files[0];
            let selectedFileName = data.target.files[0].name;
            let selectedFileType = data.target.files[0].type;
            let selectedFileSize = data.target.files[0].size;
            selectedFileSize = (selectedFileSize / (1024 * 1024));
            let selectedFilePath = djQ(this).val();

            var fileReader = new FileReader();
            fileReader.onloadend = function (data) {
                var arrayBuffer = data.target.result;
                var blob = blobUtil.arrayBufferToBlob(arrayBuffer, selectedFileType);
                console.debug("File blob is:")
                console.debug(blob);

                if (selectedFileSize <= maxUploadSize) {
                    if (selectedFileSize && selectedFilePath && selectedFileName) {
                        DAVE_SETTINGS.upload_file(blob, selectedFileName, function (data) {
                            console.debug("uploaded file response" + data);
                            //remove loader
                            let fileInputNameAttr = curr_file.attr('name');
                            djQ(curr_file).data('fileUrl', data['path']);
                            curr_file.prev(".file_upload_loader").remove();
                            console.debug("upload_file function executed")
                            //file uploaded successfully
                            curr_file.closest('form').find('.dave-error').remove();
                            djQ(curr_file).data('fileUploadedStatus', 'true');
                        }, function (e) {
                            //remove loader and show error
                            curr_file.prev().remove();
                            curr_file.closest('form').find('.dave-error').remove();
                            curr_file.closest('form').append(`<p class='dave-error'>Something went wrong while upload file!</p>`);
                            scrollChatarea(500);
                            djQ(curr_file).attr('fileUploadedStatus', 'false');
                            console.error("The error while uploading file=> " + e);
                        });
                    }
                } else {
                    //file size exceed error message
                    console.error("File Size Exceed")
                    curr_file.closest('form').find('.dave-error').remove();
                    curr_file.closest('form').append(`<p class='dave-error'>File size exceed the maximum allowed size of ${djQ(this).attr('data-max')} MB!</p>`);
                    scrollChatarea(500);
                }

            };
            fileReader.readAsArrayBuffer(selectedFiles);
        } else {
            console.debug("No file selected now");
        }

    });
    //FORM SUBMITTING CODE BLOCK
    djQ(document).on('submit', 'form.dave-form', function (e) {
        let formData = {};
        let printData = {};
        let nameMap = {};
        let typeMap = {};

        let dataValidated = true;
        let f_bubbleId = djQ(this).closest('.dave-chattext').prev('.scrollToMsg').attr('data-scrollmsgid');
        let c_form = djQ(this);
        let form_error = '';
        if (djQ(this).length <= 1 && map_enabled) {
            if (typeof userPositon['lat'] == "undefined" || typeof userPositon['lng'] == "undefined") {
                dataValidated = false;
                form_error += djQ(this).prev().attr('error') || 'Input for "' + djQ(this).prev().attr('title') + '" is required!' + '<br/><br/>';
            } else {
                formData['lat'] = userPositon['lat'].toString();
                formData['lng'] = userPositon['lng'].toString();
                printData['lat'] = userPositon['lat'].toString();
                printData['lng'] = userPositon['lng'].toString();
                printData = JSON.stringify(printData);
                console.debug(printData);
                console.warn(formData);
                djQ("#map, #locationInput").remove();
            }
        }
        djQ.each(djQ(this).serializeArray(), function (i, j) {
            let k = djQ(c_form).find("input[name='" + j.name + "']").first();
            if (!k.length) {
                k = djQ(c_form).find("textarea[name='" + j.name + "']").first();
            }
            if (!k.length) {
                k = djQ(c_form).find("select[name='" + j.name + "']").first();
            }
            if ((j.value == "" || j.value == null || j.value == undefined) && k.attr('required') && !map_enabled) {
                dataValidated = false;
                form_error += k.attr('error') || 'Input for "' + k.attr('title') + '" is required!' + '<br/><br/>';
            } else if (map_enabled) {
                if ((j.value == "" || j.value == null || j.value == undefined || typeof userPositon['lat'] == "undefined" || typeof userPositon['lng'] == "undefined")) {
                    dataValidated = false;
                    form_error += k.attr('error') || 'Input for "' + k.attr('title') || "form" + '" is required!' + '<br/><br/>';
                }
            } else if (k.attr('ui_element') == 'datetime' && j.value) {
                let a = new Date(k.datetimepicker('getValue'));
                let b = new Date(k.attr('min'));
                let c = new Date(k.attr('max'));
                if (b.toLocaleString() == 'Invalid Date') {
                    b = new Date(-8640000000000000);
                }
                if (c.toLocaleString() == 'Invalid Date') {
                    c = new Date(8640000000000000);
                }
                if (!(b <= a && c >= a)) {
                    dataValidated = false;
                    form_error += 'Input for "' + k.attr('title') + '" should be between ' + DAVE_SETTINGS.print_timestamp(b, null, true) + ' and ' + DAVE_SETTINGS.print_timestamp(c, null, true) + '!<br/><br/>';
                } else {
                    // If print_timestamp can print the original string taken, then we are good
                    j.print_value = a.toString();
                }
            } else if (k.attr('ui_element') == 'date' && j.value) {
                let a = new Date(k.datetimepicker('getValue'));
                // If print_timestamp can print the original string taken, then we are good
                j.print_value = a.toString();
            } else if (k.attr('ui_element') == 'select' && j.value) {
                j.print_value = djQ(k).find("option:selected").text();
            } else if (k.attr('type') == 'file') {
                if (k.data('fileUploadedStatus') == 'false') {
                    dataValidated = false;
                    form_error += k.attr('title') + ':-' + (k.attr('error') || 'Error in uploading file!') + '<br/><br/>';
                } else if (k.attr('required')) {
                    dataValidated = false;
                    form_error += k.attr('error') || 'Input for "' + k.attr('title') + '" is required!' + '<br/><br/>';
                } else {
                    j.value = k.data('fileUrl');
                }
            }
            else if (k.attr('ui_element') == 'checkbox') {
                k = djQ(c_form).find("input[name='" + j.name + "']")
                djQ(k).each(function () {
                    if (typeof j.value == 'string' || typeof j.value == 'undefined') {
                        j.value = []
                    }
                    if (djQ(this).is(':checked')) {
                        dataValidated = true
                        j.value.push(djQ(this).val())

                    }
                })
                if (j.value.length == []) {
                    dataValidated = false
                    form_error += k.attr('title') + ':-' + (k.attr('error') || 'Value does not match expected pattern!') + '<br/><br/>';
                }
                // j.value = djQ(k).is(":checked").val()
            }
            if (j.value && k.attr('validate')) {
                j.value = j.value.trim();
                let reg = new RegExp('\\b' + k.attr('validate') + '\\b');
                if (!reg.test(j.value)) {
                    dataValidated = false;
                    form_error += k.attr('title') + ':-' + (k.attr('error') || 'Value does not match expected pattern!') + '<br/><br/>';
                }

            }
            if (map_enabled) {
                formData['lat'] = userPositon['lat'];
                formData['lng'] = userPositon['lng'];
            }

            formData[j.name] = j.value;
            printData[j.name] = j.print_value || j.value;

            if (k.attr('title')) {
                nameMap[j.name] = k.attr('title');
            }
            if (k.attr('ui_element')) {
                typeMap[j.name] = k.attr('ui_element');
            }
        });
        djQ(this).find("input[type=file]").each(function () {
            let k = djQ(this);
            if (k.data('fileUploadedStatus') == 'false') {
                dataValidated = false;
                form_error += (k.attr('title') || k.attr('name')) + ":- " + k.attr('error') || 'Error in uploading file! <br/><br/>';
            } else if (k.data('fileUploadedStatus') == 'true') {
                formData[k.attr('name')] = k.data('fileUrl');
                if (k.attr('title')) {
                    nameMap[k.attr('name')] = k.attr('title');
                }
                if (k.attr('ui_element')) {
                    typeMap[k.attr('name')] = k.attr('ui_element');
                }
            } else if (k.attr('required')) {
                dataValidated = false;
                form_error += k.attr('error') || 'Input for "' + (k.attr('title') || k.attr('name')) + '" is required!' + '<br/><br/>';
            }
        });
        if (djQ(this).find('input[type=checkbox]').length != 0) {
            if (!djQ(this).find('input[type=checkbox]').is(':checked')) {
                dataValidated = false
                form_error = (djQ(this).find('input[type=checkbox]').attr('title') || djQ(this).find('input[type=checkbox]').attr('name')) + ":- " + 'Please Select the checkbox! <br/><br/>';
            }

        }
        if (dataValidated) {
            formData['_name_map'] = nameMap;
            formData['_type_map'] = typeMap;

            c_bubbleId = djQ.now();
            temp_bubbleId.push(c_bubbleId);

            userTextMsgBubble(DAVE_SETTINGS.pretty_print(printData, nameMap, typeMap), 'normal', dateTiming(), c_bubbleId);
            djQ(this).find('.dave-error').remove();
            customerState = djQ(this).next().text();
            botchat_data({ "customer_response": JSON.stringify(formData), "customer_state": customerState, "query_type": "click" }, c_bubbleId);
            DAVE_SETTINGS.execute_custom_callback('on_form_submit', [formData, customerState]);
            djQ(this).find('input[type="submit"]').prop('disabled', true);
            djQ(this).find('input').prop('disabled', true);
            djQ(this).find('textarea').prop('disabled', true);
            djQ(this).find('select').prop('disabled', true);
            scrollChatarea(500);
        } else {
            djQ(this).find('.dave-error').remove();
            djQ(this).append(`<p class='dave-error'>${form_error}</p>`);
            scrollChatarea(500);
        }
        e.preventDefault();

    });
    //SPEECH-REC CODE BLOCK STARTS
    function setup_dave_speech() {
        djQ(".dave-mic-contt button").show();
        let voice_MsgId = undefined;
        let mic_status = undefined
        function recButton_visibility(startRecVis) {
            if (startRecVis) {
                nowRecording = true;
                djQ("button#start-recording").hide();
                djQ("button#stop-recording").show();
            } else {
                nowRecording = false;
                djQ("button#stop-recording").hide();
                djQ("button#start-recording").show();
            }
        }
        DAVE_SETTINGS.register_custom_callback('on_mic_access_granted', recButton_visibility);
        if (DAVE_SCENE.opened && !DAVE_SETTINGS.micAccess) {
            DAVE_SETTINGS.ask_microphone();
        }
        djQ(document).on("click", "#start-recording", function (event) {
            djQ("#dave-cb-textinput").val('');
            window.stopTimmer();
            voice_MsgId = DAVE_SETTINGS.generate_random_string(6);
            if (!DAVE_SETTINGS.micAccess) {
                navigator.mediaDevices.getUserMedia({ audio: true })
                    .then(function (auStream) {
                        DAVE_SETTINGS.micAccess = true;
                        DAVE_SETTINGS.streamer.micAccess = true;
                        DAVE_SETTINGS.streamer.initSocket();
                        if (!DAVE_SETTINGS.PRODUCTION) {
                            console.log('Mic Access is provided');
                        }
                        recButton_visibility(true);
                        DAVE_SETTINGS.StreamingSpeech.onStartVoiceRecording();
                        djQ("button#start-recording").attr('title', 'Click here to start recording!');
                        djQ("button#stop-recording").attr('title', 'Click here to stop recording!');
                    })
                    .catch(function (e) {
                        console.warn('No mic access is provided')
                        djQ("button#start-recording").attr('title', 'You have dis-allowed the microphone. Please go to settings to allow microphone for this website');
                    });
            }
            if (DAVE_SETTINGS.micAccess) {
                if (DAVE_SCENE.loaded) {
                    DAVE_SCENE.stop();
                }
                recButton_visibility(true);
                DAVE_SETTINGS.StreamingSpeech.onStartVoiceRecording();
            } else {
                recButton_visibility();
                alert('Please allow permission to access microphone');
                djQ("button#start-recording").attr('title', 'You have dis-allowed the microphone. Please go to settings to allow microphone for this website');
            }
            event.stopPropagation();
        });

        djQ(document).on("click", "#stop-recording", function (ev) {
            DAVE_SETTINGS.StreamingSpeech.onStopVoiceRecording();
            recButton_visibility();
            djQ('button#start-recording').prop('disabled', true);
        });
        DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording = function () {
            recButton_visibility();
            djQ('button#start-recording').prop('disabled', true);
        }
        DAVE_SETTINGS.StreamingSpeech.onSocketConnect = function (o) {
            if (!nowRecording) {
                recButton_visibility();
            }
            djQ('button#start-recording').prop('disabled', false);
        }
        DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable = function (data) {
            stopTimmer();
            if (!data['final_text']) {
                let voice_rawData = "";
                if (data.hasOwnProperty('rec_text')) {
                    voice_rawData = JSON.stringify(data['rec_text']);
                } else {
                    voice_rawData = JSON.stringify(data['recognized_speech']);
                }
                // let voice_rawData = JSON.stringify(data['recognized_speech']) || JSON.stringify(data['rec_text'])
                voice_rawData = voice_rawData.slice(1, -1)
                djQ("#dave-cb-textinput").val(djQ("#dave-cb-textinput").val() + " " + voice_rawData);
            } else {
                dave_chatRes_loader('user');
                let voice_rawData = JSON.stringify(data['final_text'])
                voice_rawData = voice_rawData.slice(1, -1)
                console.debug(voice_rawData + ' =====> This is final speech text');
                djQ("#dave-cb-textinput").val('');
                msgBubbles('user', 'speech', dateTiming(), voice_rawData, voice_MsgId, null, voice_MsgId);
                scrollChatarea(500);
            }
        };
        DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable = function (data) {
            if (!data['final_text']) {
                let voice_rawData = JSON.stringify(data['rec_text'])
                voice_rawData = voice_rawData.slice(1, -1)
                djQ("#dave-cb-textinput").val(djQ("#dave-cb-textinput").val() + " " + voice_rawData);
                console.debug(voice_rawData + " =====> this is in-time rec text");
            }
        }
        DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable = function (data) {
            if (data.hasOwnProperty('conv_resp')) {
                data = data['conv_resp'];
            } else {
                data = data['conversation_api_response']
            }

            let v_returnSize = Object.keys(data).length;
            if (v_returnSize > 2) {
                djQ(`input[value="${voice_MsgId}"]`).val(data['response_id'])
                voice_MsgId = data['response_id']
                c_bubbleId = djQ.now();
                userTextMsgBubble(djQ("#dave-cb-textinput").val(), 'normal', dateTiming(), c_bubbleId)
                response_print(data, v_returnSize, dateTiming(), "speech", voice_MsgId);
                if (DAVE_SCENE && DAVE_SCENE.play && DAVE_SCENE.loaded && DAVE_SCENE.playable && DAVE_SCENE.opened && data.response_channels && data.response_channels.voice && data.response_channels.frames && data.response_channels.shapes) {
                    DAVE_SCENE.audio_text = data.placeholder;
                    DAVE_SCENE.play(data.response_channels, true);
                }
                scrollChatarea(500);
                djQ("#dave-cb-textinput").val('');
            } else {
                c_bubbleId = djQ.now();
                userTextMsgBubble("---Silence--Noise---", 'normal', dateTiming(), c_bubbleId)
                msgBubbles('bot', 'normal', dateTiming(), 'Voice Recognition Failed! Please check your microphone settings and try again');
                scrollChatarea(500);
            }
            djQ('button#start-recording').prop('disabled', false);
        }

        DAVE_SETTINGS.StreamingSpeech.onError = function (data) {
            console.error("Speech onError is generated");
            msgBubbles('bot', 'normal', dateTiming(), 'Failed to get a response! Please try with a different query');
            scrollChatarea(500);
            if (typeof window.startTimmer == 'function') {
                window.startTimmer();
            }
            djQ('button#start-recording').prop('disabled', false);
        };

        DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect = function (o) {
            recButton_visibility();
            if (typeof window.startTimmer == 'function') {
                window.startTimmer();
            }
            djQ('button#start-recording').prop('disabled', false);
        };

        djQ(document).on("click", ".voice-thumbsup, .voice-thumbsdown", function () {
            let curr_rating = djQ(this).attr('data-thumbtype');
            let v_feedbackResId = djQ(this).siblings('input[type="hidden"]').val();
            like_dislike(this, curr_rating, "speech");
            if (v_feedbackResId) {
                DAVE_SETTINGS.feedback({ "recognition_rating": l_d_react }, v_feedbackResId, function () { });
            }
        });
    }
    if (DAVE_SETTINGS.StreamingSpeech) {
        setup_dave_speech()
    } else {
        DAVE_SETTINGS.register_custom_callback('on_load_streamer', function () {
            setup_dave_speech()
        });
    }
    if (!DAVE_SETTINGS.SPEECH_SERVER && !djQ("#dave-settings").data('speech-server')) {
        if (!DAVE_SETTINGS.PRODUCTION) {
            console.log("Speech Server not set.");
        }
        djQ(".dave-mic-contt button").hide();
    }
    /*SPEECH CODE BLOCK ENDS*/
    dave_nudge();
    DAVE_SETTINGS.execute_custom_callback("after_load_chatbot");
}

djQ(document).ready(function () {
    if (!DAVE_SETTINGS.PRODUCTION) {
        console.log("Document is ready now");
    }
    if (djQ("#dave-settings").length) {
        window.dave_load_chatbot();
        if (djQ(document).find("#dave-settings").attr("data-google-map-key") || DAVE_SETTINGS.GOOGLE_MAP_KEY) {
            loadDaveMap();
            DAVE_SETTINGS.loaded_map = true
        }
    }
});
if (djQ().jquery.startsWith('1') || djQ().jquery.startsWith('2')) {
    djQ(window).load(function () {
        if (djQ("#dave-settings").length && !djQ(".dave-chatbox-cont").length) {
            window.dave_load_chatbot();
        }
        if (djQ(document).find("#dave-settings").attr("data-google-map-key") || DAVE_SETTINGS.GOOGLE_MAP_KEY && !DAVE_SETTINGS.loaded_map) {
            loadDaveMap();
        }
    });
}
window.do_on_load_chatbot = function () {
    if (!DAVE_SETTINGS.loaded_dave && window.botchat_data && window.botchat_history && DAVE_SETTINGS.ENTERPRISE_ID && DAVE_SETTINGS.CONVERSATION_ID == DAVE_SETTINGS.getCookie('dave_conversation_id')) {
        //If there is not cookie set for user and system then call botchat_data function
        if ((DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id')) && !(DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response'))) {
            window.botchat_history();
            DAVE_SETTINGS.dave_eng_id_status = true;
        } else if (!(DAVE_SETTINGS.getCookie('dave_engagement_id') || DAVE_SETTINGS.getCookie('engagement_id')) && !(DAVE_SETTINGS.getCookie('dave_system_response') || DAVE_SETTINGS.getCookie('system_response'))) {
            DAVE_SETTINGS.dave_eng_id_status = true;
            console.debug(DAVE_SETTINGS.getCookie('dave_engagement_id'));
            botchat_data({ "query_type": "auto" });
        } else if (!DAVE_SETTINGS.dave_eng_id_status) {
            djQ('.dave-cb-stickBottom').css('display', 'block');
            window.botchat_history();
            DAVE_SETTINGS.dave_eng_id_status = true;
        } else {
            if (!DAVE_SETTINGS.PRODUCTION) {
                console.log("Did not match any of the criteria!!! How!!")
            }
        }
        DAVE_SETTINGS.loaded_dave = true;
    }
}
DAVE_SETTINGS.register_custom_callback('on_load_dave', function () {
    if (!DAVE_SETTINGS.loaded_dave) {
        window.do_on_load_chatbot();
    }
    DAVE_SETTINGS.botchat_data = window.botchat_data;
});
DAVE_SETTINGS.register_custom_callback("after_load_chatbot", function () {
    if (!DAVE_SETTINGS.MANUAL_LOAD) {
        window.do_on_load_chatbot();
    }
    DAVE_SETTINGS.botchat_data = window.botchat_data;
});
/**
 * ##### DAVE_SETTINGS.register_nudge
 * Registering nudges based on activity on the webpage.
 * @param {jQuery} identifier   element to bind the action event to
 * @param {string} identifier (jquery selector) element to bind the action event to
 * @param {string} customer_state
 * @param {string} customer_response
 * @param {string} event_type   The the jQuery event type on which to trigger the nudge, defaults to 'click'
 * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
 * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
 * @param {object} data    Any extra data to be sent to chatbot 
 *
 * e.g. DAVE_SETTINGS.register_nudge("#MyId", "cs_nudge_state", "Nudge me here", "click", "max", null, {"my_new_data": "this data"})
 */
DAVE_SETTINGS.register_nudge = function register_nudge(
    identifier, customer_state,
    customer_response, event_type, nudgeopenState,
    lockHeight, data
) {
    event_type = event_type || 'click';
    data = data || null;
    customer_response = customer_response || DAVE_SETTINGS.toTitleCase(customer_state);
    nudgeopenState = nudgeopenState || 'min';
    djQ(identifier).on(event_type, function () {
        DAVE_SETTINGS.create_event(customer_state);
        botchat_data({ 'customer_state': customer_state, "customer_response": customer_response, "query_type": "auto", "temporary_data": data }, null,
            'open', nudgeopenState, lockHeight
        );
    });
}
djQ(document).ready(function () {
    if (DAVE_HELP) {
        /**
         * ##### DAVE_HELP.register_help_nudge
         * Registering help nudges based on activity on the webpage. The help text opens up. When we click on the help text, a nudge is sent
         * @param {jQuery} identifier   element to bind the action event to
         * @param {string} identifier (jquery selector) element to bind the action event to
         * @param {string} help_text  Text show in the help pop-up
         * @param {string} customer_state
         * @param {string} customer_response
         * @param {string} event_type   The the jQuery event type on which to trigger the nudge, defaults to 'click'
         * @param {string} help_sub_text  Sub-Text to show in the help pop-up, not required, defaults to "Click here for more help"
         * @param {jQuery} anchor_dom  element relative to which the help text should occur, defaults to 'dom' if evaluates to false
         * @param {string} anchor_dom (jquery selector) element relative to which the help text should occur, defaults to 'dom'
         * @param {object} pos  {position: right/left/above/below, length: distance from dom, xaway: x distance from dom, yaway: y distance form dom}
         * @param {string} pos  right/left/above/below
         * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
         * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
         * @param {object} data    Any extra data to be sent to chatbot 
         *
         * e.g. DAVE_HELP.register_help_nudge("#MyId", "About this topic", "cs_nudge_state", "Nudge me here", "click", "I need more info", "above", "#MyOtherElement", 5000, 'max', "450px", {"other_data": "this other data"})
         */
        DAVE_HELP.register_help_nudge = function register_help_nudge(
            identifier, help_text, customer_state,
            customer_response, event_type,
            help_sub_text, pos, anchor_dom, timeout,
            nudgeopenState, lockHeight, data
        ) {
            customer_response = customer_response || DAVE_SETTINGS.toTitleCase(customer_state);
            nudgeopenState = nudgeopenState || 'max';
            DAVE_HELP.bind_help_for_element(
                identifier,
                help_text,
                help_sub_text || "Click here for more help",
                event_type, pos, anchor_dom, timeout,
                function (elem) {
                    DAVE_SETTINGS.create_event(customer_state);
                    botchat_data({ 'customer_state': customer_state, "customer_response": customer_response, "query_type": "auto", "temporary_data": data || null },
                        null, 'open', nudgeopenState, lockHeight
                    );
                }
            );
        }
    }
});

// Creates a passive nudge
/**
 * ##### DAVE_SETTINGS.register_passive_nudge
 * Registering nudge to open the chat when there is no activity by the user or the user is passive
 * @param {number} timout   time in milliseconds of user passivity which will trigger this nudge
 * @param {string} customer_state
 * @param {string} customer_response
 * @param {array} cancel_events     Any events we have registered, which will cancel this passive nudge.
 * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
 * @param {string} reset_events  JQuery sequence of events which will cause the timer to be reset, default to most of the common activity events
 * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
 * @param {object} data    Any extra data to be sent to chatbot 
 *
 * e.g. DAVE_SETTINGS.register_passive_nudge(20000, "cs_nudge_state", "Nudge me here", ["User's Next Event", "User's possible event"], 'min', 'touchstart click keydown scroll dblclick resize', "450px", {'extra_data': 'extra_data'} )
 */
DAVE_SETTINGS.register_passive_nudge = function register_passive_nudge(
    timeout, customer_state, customer_response,
    cancel_events, nudgeopenState, reset_events, lockHeight, random_string, data
) {
    customer_response = customer_response || DAVE_SETTINGS.toTitleCase(customer_state);
    nudgeopenState = nudgeopenState || 'min';
    cancel_events = DAVE_SETTINGS.makeList(cancel_events);
    random_string = random_string || DAVE_SETTINGS.generate_random_string(6);
    //if ( cancel_events ) {
    //    reset_events = reset_events || '__NULL__';
    //} else {
    //    reset_events = reset_events || 'touchstart click keydown scroll dblclick resize';
    //}
    reset_events = reset_events || 'touchstart click keydown scroll dblclick resize';
    if (!DAVE_SETTINGS.PRODUCTION) {
        console.log("Starting timer for customer state " + customer_state + " - " + timeout)
    }
    if (!DAVE_SETTINGS.event_timers) {
        DAVE_SETTINGS.event_timers = {};
    }
    if (!DAVE_SETTINGS.reset_timers) {
        DAVE_SETTINGS.reset_timers = {};
    }
    let t = random_string + customer_state;
    if (DAVE_SETTINGS.event_timers[t]) {
        if (!DAVE_SETTINGS.PRODUCTION) {
            console.log("Same passive nudge found already, so clearing the timer");
        }
        clearTimeout(DAVE_SETTINGS.event_timers[t]);
    }
    DAVE_SETTINGS.event_timers[t] = setTimeout(function () {
        if (!DAVE_SETTINGS.PRODUCTION) {
            console.log("Timeout done for customer state " + customer_state)
        }
        delete DAVE_SETTINGS.event_timers[t];
        if (DAVE_SETTINGS.reset_timers[t]) {
            clearTimeout(DAVE_SETTINGS.reset_timers[t]);
            delete DAVE_SETTINGS.reset_timers[t];
        }
        botchat_data({ 'customer_state': customer_state, "customer_response": customer_response, "query_type": "auto", "temporary_data": data || null },
            null, 'open', nudgeopenState, lockHeight
        );
    }, timeout);
    if (cancel_events) {
        if (!DAVE_SETTINGS.cancel_events) {
            DAVE_SETTINGS.cancel_events = {};
        }
        for (let k of cancel_events) {
            if (!DAVE_SETTINGS.cancel_events[k]) {
                DAVE_SETTINGS.cancel_events[k] = [];
            }
            DAVE_SETTINGS.cancel_events[k].push(t);
        }
    }
    if (reset_events != "__NULL__") {
        djQ(document).one(reset_events, function (e) {
            if (e.type == 'keydown' && (e.which < 30 || (e.which > 90 && e.which < 96) || e.which > 105)) {
                return true;
            }
            if (DAVE_SETTINGS.event_timers && DAVE_SETTINGS.event_timers[t]) {
                console.debug("Restarting timer for customer state " + customer_state + " - " + timeout)
                clearTimeout(DAVE_SETTINGS.event_timers[t]);
                delete DAVE_SETTINGS.event_timers[t];
                DAVE_SETTINGS.reset_timers[t] = setTimeout(function () {
                    delete DAVE_SETTINGS.reset_timers[t];
                    DAVE_SETTINGS.register_passive_nudge(
                        timeout, customer_state, customer_response,
                        cancel_events, nudgeopenState, reset_events, lockHeight, random_string, data
                    );
                }, 1000);
            }
            return true;
        });
    }
}

// Binds a passive nudge
/**
 * ##### DAVE_SETTINGS.bind_passive_nudge
 * Bind the registering of a passive nudge to a user event. After the event occurs, and no other activity occurs after this event, then the chat would open with the given customer_state
 * @param {jQuery} identifier   element to bind the action event to
 * @param {string} identifier (jquery selector) element to bind the action event to
 * @param {number} timout   time in milliseconds of user passivity which will trigger this nudge
 * @param {string} customer_state
 * @param {string} customer_response
 * @param {string} event_type   The the jQuery event type on which to trigger the registration of the nudge, defaults to 'click'
 * @param {array}  cancel_events     Any dave events we have registered, which will cancel this passive nudge.
 * @param {string} nudgeopenState  The size of the chat-window to open the nudge in, 'min', 'max', if the size is larger than specified here at the time of the nudge, there is no effect
 * @param {string} reset_events  JQuery sequence of events which will cause the timer to be reset, default to most of the common activity events
 * @param {string} lockHeight  The the height of the chatbot to which to be locked. Don't use this unless you know what you are doing
 * @param {object} data    Any extra data to be sent to chatbot 
 *
 * e.g. DAVE_SETTINGS.bind_passive_nudge("#MyId", 20000, "cs_nudge_state", "Nudge me here", "click", ["User's Next Event", "User's possible event"]], 'min', 'touchstart click keydown scroll dblclick resize', "450px", {'extra_data': 'extra_data'} )
 */
DAVE_SETTINGS.bind_passive_nudge = function initiate_passive_nudge(
    identifier, timeout,
    customer_state, customer_response,
    event_type,
    cancel_events,
    nudgeopenState,
    reset_events,
    lockHeight,
    data
) {
    event_type = event_type || 'click';
    djQ(identifier).on(event_type, function () {
        DAVE_SETTINGS.register_passive_nudge(timeout, customer_state, customer_response, cancel_events, nudgeopenState, reset_events, lockHeight, identifier + event_type, data);
    });
};
