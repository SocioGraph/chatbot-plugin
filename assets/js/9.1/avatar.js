
function daveAvatar(aud, iOS, isSafari) {
    var scene = null;
    var talk = false;
    var lastTime = 0
    var shapesIndex = 0;
    var framesIndex = 0;
    var snd = aud || null;
    var IOSsnd = iOS || false;
    var shapes = null;
    var frames = null;
    var camera = null;
    var assetsManager = null;
    var headers = [];
    var volume = 1.0;
    var queue = [];
    let intervalId;
    let prevAni = []
    let isVisible = false;
    this.loadPreScene = function (option, model_path, sceneLoadedCallback, modelLoadedCallback) {
        var canvas = document.getElementById(option);
        var engine = new BABYLON.Engine(canvas, true, {
            timeStep: (1.0 / 10.0),
            deterministicLockstep: true,
            lockstepMaxSteps: 30,
            limitDeviceRatio: 3,
        }, isSafari ? false : true);
        var createScene = function () {
            scene = new BABYLON.Scene(engine);
            scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);
            camera = new BABYLON.FreeCamera('camera_dave', new BABYLON.
                Vector3(0, 1.5, 2), scene);
            camera.setTarget(new BABYLON.Vector3(0, 1.5, 0));
            camera.viewport = new BABYLON.Viewport(0, -1, 1, 2.75);
            //camera.attachControl(canvas, false);
            assetsManager = new BABYLON.AssetsManager(scene);
            return scene;
        }
        var onetime = 1;
        scene = createScene();
        this.scene = scene
        this.camera = camera
        this.canvas = canvas
        this.engine = engine
        this.loadScene(model_path, sceneLoadedCallback, modelLoadedCallback)
        var that = this;
        engine.runRenderLoop(function () {
            if (talk) {
                let shape = shapes[shapesIndex];
                let x = (Date.now() - lastTime) / 1000;
                if (!shape) {
                    talk = false;
                    shapesIndex = 0;
                    framesIndex = 0;
                    scene.render();
                    return;
                }
                while (x > shape["timestamp"]) {
                    console.debug("Went ahead");
                    shapesIndex += 1;
                    shape = shapes[shapesIndex]
                    if (!shape) {
                        talk = false;
                        shapesIndex = 0;
                        framesIndex = 0;
                        scene.render();
                        return;
                    }
                }
                if (x >= shape["timestamp"] - (1.0 / 25.0) && x <= shape["timestamp"]) {
                    that.setMorphTargets(shape);
                    shapesIndex += 1;
                } else if (x < shape["timestamp"] - (1.0 / 25.0)) {
                    console.debug("Lagging behind");
                }
                frame = frames[framesIndex];
                if (frame != null && (x >= frame["timestamp"])) {
                    that.playAnimation(frame);
                    framesIndex += 1;
                }
                if (shapesIndex >= shapes.length) {
                    talk = false;
                    shapesIndex = 0;
                    framesIndex = 0;
                }
            } else {
                that.idleShapes('idleBlink');
                that.idleShapes('idleMotion');
                that.idleShapes('idleHead');
                that.idleShapes('idleBlinkTwice');
            }
            scene.render();
        });
        this.idleBlinkArr = [
            {
                "shapes": {
                    "blink": 1
                },
                "period": 10
            }
        ]
        this.idleBlinkTwiceArr = [
            {
                "shapes": {
                    "blink": 0.5
                },
                "period": 10
            }
        ]
        this.idleMotionArr = [
            {
                "shapes": {
                    "smile": 0.6
                },
                "period": 250
            },
            {
                "shapes": {
                    "surprise": 0.2,
                    "smile": 0.6
                },
                "period": 250
            },
            {
                "shapes": {
                    "joy": 0.9,
                    "open": 0.3
                },
                "period": 150
            }
        ]
        this.idleHeadArr = [
            {
                "shapes": {
                    "look_right": 0.3,
                    "look_down": 0.2,
                },
                "period": 50
            },
            {
                "shapes": {
                    "look_right": 0.3,
                    "look_up": 0.2,
                },
                "period": 50
            },
            {
                "shapes": {
                    "look_left": 0.3,
                    "look_up": 0.1,
                },
                "period": 50
            },
            {
                "shapes": {
                    "look_left": 0.3,
                    "look_down": 0.1,
                },
                "period": 50
            },
            {
                "shapes": {
                    "look_right": 0.3,
                    "look_down": 0.1,
                },
                "period": 50
            },
            {
                "shapes": {

                    "head_right_up": 0.2,
                    "look_left": 0.3,
                    "look_up": 0.2,
                },
                "period": 80
            },
            {
                "shapes": {
                    "head_right_down": 0.2,
                    "look_left": 0.3,
                    "look_down": 0.2,
                },
                "period": 80
            },
            {
                "shapes": {
                    "head_left_up": 0.2,
                    "look_right": 0.3,
                    "look_up": 0.2,
                },
                "period": 80
            },
            {
                "shapes": {
                    "head_left_down": 0.2,
                    "look_right": 0.3,
                    "look_down": 0.2,
                },
                "period": 80
            },
            {
                "shapes": {
                    "head_right_down": 0.1
                },
                "period": 100
            },
            {
                "shapes": {
                    "head_right_up": 0.1
                },
                "period": 100
            },
            {
                "shapes": {
                    "head_left_up": 0.1
                },
                "period": 100
            },
            {
                "shapes": {
                    "head_left_down": 0.1
                },
                "period": 100
            }
        ]
        if(isVisible){
            this.playIdleAnimation();
        }
    }

    this.loadScene = function (model_path, sceneLoadedCallback, modelLoadedCallback) {
        BABYLON.SceneLoader.ImportMesh("", "", model_path, scene, function (meshes) {
            scene.createDefaultCameraOrLight(false, false, false);
            scene.animationGroups.forEach(function (animationGroup) {
                animationGroup.stop();
            })
            let loaded = meshes.map(function () { return false });
            meshes.forEach(function (mesh, i) {
                mesh.onMeshReadyObservable.add(function () {
                    loaded[i] = true;
                    if (loaded.filter(function (f) { return !f }).length <= 0) {
                        if (sceneLoadedCallback && typeof (sceneLoadedCallback) == 'function') {
                            sceneLoadedCallback()
                        }
                    }
                });
            });
            if (modelLoadedCallback && typeof (modelLoadedCallback) == 'function') {
                modelLoadedCallback()
            }
        });
    }

    this.createIdleAnimationsArray = function (ani, period, factor, offset, prev_shapes, shape_name) {
        let il = [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
        let p = period || 30;
        let fac = factor || 1;
        let off = offset || 0;
        let ret = prev_shapes || {
            "shapes": [],
            "time": 0,
            "index": 0
        }
        // prevAni.push(ani)
        let f = function (i, tp) {
            let val = Math.sin((Math.PI * i * fac) / 2) + off;
            let prev = ret.shapes.map((_) => { return _[0] }).indexOf(tp);
            if (prev >= 0) {
                ret.shapes[prev][1][ani] = val;
            } else {
                ret.shapes.push([tp, {}])
                ret.shapes[ret.shapes.length - 1][1][ani] = val;
            }
        }
        for (let t in il) {
            let i = il[t];
            let tp = t * p;
            f(i, tp);
        }
        il.reverse()
        for (let t in il) {
            let i = il[t];
            let tp = (2 * p * il.length) + (t * p);
            f(i, tp);
        }

        if (shape_name == "idleBlinkTwice" && ani == "blink") {
            il.reverse()

            for (let t in il) {
                let i = il[t];
                let tp = (4 * p * il.length) + (t * p);
                f(i, tp);
            }
            il.reverse()
            for (let t in il) {
                let i = il[t];
                let tp = (6 * p * il.length) + (t * p);
                f(i, tp);
            }

        }
        // console.log("ret.shapes.length---", ret.shapes.length, ani, prevAni)

        prevAni = [];
        return ret;
    }
    this.idleShapes = function (attr) {
        var that = this;
        if (!that[attr] || !that[attr].time) {
            return
        }
        let x = (Date.now() - that[attr].time);
        if (x < 0) {
            return;
        }
        let i = that[attr].index;
        for (let t = i; t < that[attr].shapes.length; t++) {
            if (that[attr].shapes[t][0] >= x) {
                that[attr].index = t;
                that.setMorphTargets(that[attr]["shapes"][t][1]);
                return;
            }
        }
        that.stopMorphs();
        if (that[attr]['callback'] && typeof (that[attr]['callback'] == 'function')) {
            that[attr]['callback'](that, attr);
        } else {
            that[attr] = null;
        }
    }
    // this.generateIdleAnimations = function (array, attr, min_period, max_period, force) {
    //     let that = this;
    //     max_period = max_period || 12000;
    //     min_period = min_period || 2000;
    //     let period = Math.max(max_period - min_period, 0);
    //     let f = function (list) {
    //         let force = false;
    //         if (!list) {
    //             force = true;
    //         }
    //         if (!list || list.length <= 0) {
    //             list = array.slice();
    //         }
    //         const [i, ani] = that.random_choice(list)
    //         that[attr] = {
    //             shapes: [],
    //             index: 0,
    //             time: 0
    //         }
    //         console.debug("Generated idle animation: ", Object.keys(ani.shapes), attr);
    //         for (let s in ani.shapes) {
    //             let o = 0;
    //             if (ani.offsets && ani.offsets[s]) {
    //                 o = ani.offsets[s]
    //             }
    //             that[attr] = that.createIdleAnimationsArray(s, ani.period, ani.shapes[s] * (Math.random() * 0.5 + 0.5), o, that[attr], attr)
    //             prevAni.push(s)
    //         }
    //         if (force) {
    //             that[attr]['time'] = 100 + Date.now();
    //         } else {
    //             that[attr]['time'] = (Math.random() * period) + Date.now();
    //         }
    //         // console.debug("Trigger idle animation after: ", that[attr]['time']);
    //         that[attr]['callback'] = function () { f(list) }
    //         if (attr !== 'idleBlinkTwice') {
    //             setTimeout(() => { f(list); }, that[attr]['time'] - Date.now());
    //         }
    //     }
    //     // Special scheduling for idleBlinkTwice
    //     if (attr === 'idleBlinkTwice') {
    //         let scheduleIdleBlinkTwice = function () {
    //             let randomDelay = Math.floor(Math.random() * (12000 - 3000 + 1)) + 3000; // Random interval between 3 to 12 seconds
    //             setTimeout(() => {
    //                 that.generateIdleAnimations(array, attr); // Call generateIdleAnimations for idleBlinkTwice
    //                 scheduleIdleBlinkTwice(); // Schedule the next call
    //             }, randomDelay);
    //         };
    //         scheduleIdleBlinkTwice(); // Start the scheduling for idleBlinkTwice
    //     }
    //     f();

    // }

    this.generateIdleAnimations = function (array, attr, min_period, max_period, force) {
        let that = this;
        max_period = max_period || 12000;
        min_period = min_period || 2000;
        let period = Math.max(max_period - min_period, 0);
        let f = function (list) {
            let force = false;
            if (!list) {
                force = true;
            }
            if (!list || list.length <= 0) {
                list = array.slice();
            }
            const [i, ani] = that.random_choice(list)
            that[attr] = {
                shapes: [],
                index: 0,
                time: 0,
            }
            console.debug("Generated idle animation: ", Object.keys(ani.shapes));
            for (let s in ani.shapes) {
                let o = 0;
                if (ani.offsets && ani.offsets[s]) {
                    o = ani.offsets[s]
                }
                that[attr] = that.createIdleAnimationsArray(s, ani.period, ani.shapes[s] * (Math.random() * 0.5 + 0.5), o, that[attr])
            }
            if (force) {
                that[attr]['time'] = 100 + Date.now();
            } else {
                that[attr]['time'] = (Math.random() * period) + Date.now();
            }
            console.debug("Trigger idle animation after: ", that[attr]['time']);
            that[attr]['callback'] = function () { f(list) }
            // if (attr !== 'idleBlinkTwice') {
            //     setTimeout(() => { f(list); }, that[attr]['time'] - Date.now());
            // }
        }
        // // Special scheduling for idleBlinkTwice
        // if (attr === 'idleBlinkTwice') {
        //     let scheduleIdleBlinkTwice = function () {
        //         let randomDelay = Math.floor(Math.random() * (12000 - 3000 + 1)) + 3000; // Random interval between 3 to 12 seconds
        //         setTimeout(() => {
        //             that.generateIdleAnimations(array, attr); // Call generateIdleAnimations for idleBlinkTwice
        //             scheduleIdleBlinkTwice(); // Schedule the next call
        //         }, randomDelay);
        //     };
        //     scheduleIdleBlinkTwice(); // Start the scheduling for idleBlinkTwice
        // }
        f();
    }
    this.random_choice = function (array) {
        let i = Math.floor(Math.random() * array.length);
        return [i, array[i]]
    }
    this.processData = function (allText) {
        var allTextLines = allText.split(/\r\n|\n/);
        headers = allTextLines[0].split(',');
        var lines = [];
        for (var i = 1; i < allTextLines.length; i++) {
            var data = allTextLines[i].split(',');
            if (data.length == headers.length) {

                var tarr = {};
                for (var j = 0; j < headers.length; j++) {
                    tarr[headers[j]] = parseFloat(data[j]);
                }
                lines.push(tarr);
            }
        }
        return lines;
    }

    this.processDataFrames = function (allText) {
        var allTextLines = allText.split(/\r\n|\n/);
        var headers = allTextLines[0].split(',');
        var lines = [];
        for (var i = 1; i < allTextLines.length; i++) {
            var data = allTextLines[i].split(',');
            if (data.length == headers.length) {

                var tarr = {};
                for (var j = 0; j < headers.length; j++) {
                    console.debug("got ani data = " + headers[j] + " = " + data[j])
                    if (headers[j] == 'timestamp') {
                        tarr[headers[j]] = parseFloat(data[j]);
                    } else {
                        tarr[headers[j]] = data[j];
                    }
                }
                lines.push(tarr);
            }
        }
        return lines;
    }

    this.setMorphTargets = function (shape) {
        for (var key in shape) {
            // if (key != "timestamp" && shape[key] != null) {
            //     s = scene.getMorphTargetByName(key)
            //     if (s !== null) {
            //         s.influence = shape[key];
            //     }
            // }
            this.setMorphTarget(key, shape[key])

        }
    }

    this.setMorphTarget = function (key, value) {
        if (key != "timestamp" && value != null && value != undefined) {
            for (let l of scene.morphTargetManagers) {
                let s = l._targets.filter((_) => { return _.name == key });
                if (s.length) {
                    s = s[0];
                    if (s) {
                        s.influence = value;
                    }
                }
            }
        }
    }

    this.playAnimation = function (animationName) {
        if (animationName["animation"] != null) {
            console.log("trying to play = " + animationName["animation"]);
            ani = scene.getAnimationGroupByName(animationName["animation"]);
            if (ani) {
                try {
                    ani.play(false);
                    ani.loopAnimation = false;
                    ani.isAdditive = true;
                } catch (error) {
                    console.error(error)
                }
            }
        }
    }
    this.get_random = function (list) {
        return list[Math.floor((Math.random() * list.length))];
    }
    // this.playIdleAnimation = function () {
    //     let animation_list = [];
    //     let ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/weight_shift/g) });
    //     animation_list.push(ani);
    //     if (ani.length <= 0) {
    //         ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/weight/g) });
    //         animation_list.push(ani);

    //     }
    //     if (ani.length <= 0) {
    //         ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/wieght/g) });
    //         animation_list.push(ani);

    //     }
    //     if (ani.length <= 0) {
    //         ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/talk_head_001/g) });
    //         animation_list.push(ani);

    //     }
    //     if (ani.length <= 0) {
    //         ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle/g) });
    //         animation_list.push(ani);

    //     }
    //     if (ani.length <= 0) {
    //         ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/body_idle_001/g) });
    //         animation_list.push(ani);

    //     }
    //     if (ani.length <= 0) {
    //         ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/body_idle_002/g) });
    //         animation_list.push(ani);

    //     }
    //     if (ani.length <= 0) {
    //         ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idel/g) });
    //         animation_list.push(ani);

    //     }
    //     if (ani.length <= 0) {
    //         ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/thinking_1/g) });
    //         animation_list.push(ani);

    //     }
    //     if (animation_list.length > 0) {
    //         console.log("animation_list---",animation_list)
    //         ani = that.get_random(animation_list)
    //         ani=ani[0];
    //         console.debug("trying to play idle animation", ani.name, animation_list.length);
    //         try {
    //             ani.play(true);
    //             ani.loopAnimation = false;
    //             if (!ani.onAnimationGroupEndObservable.hasObservers()) {
    //                 ani.onAnimationGroupEndObservable.add(() => {
    //                     console.log("Idle animation ended");

    //                     const randomTime = Math.floor(Math.random() * 8000) + 2000; // Random time between 2000ms (2s) and 10000ms (10s)
    //                     console.log(`Waiting for ${randomTime}ms to restart idle animation`);

    //                     setTimeout(() => {
    //                         this.playIdleAnimation(); // Restart the idle animation
    //                     }, randomTime);
    //                 });
    //             }

    //             this.generateIdleAnimations(this.idleBlinkArr, 'idleBlink');
    //             this.generateIdleAnimations(this.idleMotionArr, 'idleMotion');
    //             this.generateIdleAnimations(this.idleHeadArr, 'idleHead');
    //             this.generateIdleAnimations(this.idleBlinkTwiceArr, 'idleBlinkTwice');

    //             return true
    //         } catch (error) {
    //             console.log(error)
    //         }
    //     }

    //     return false;
    // }

    this.playIdleAnimation = function () {
        let body_idleAnimations=["body_idle_001","body_idle_002","body_idle_003","body_idle_004"];
        let hand_idleAnimations=["hand_idle_001"];
        let head_idleAnimations=["head_idle_001","head_idle_002"];
        
        if (body_idleAnimations.length > 0 && hand_idleAnimations.length > 0  && head_idleAnimations.length > 0 && that.scene.animationGroups.length > 0) {
            ani = that.get_random(body_idleAnimations)
            ani1=that.get_random(hand_idleAnimations)
            ani2=that.get_random(head_idleAnimations)
            console.log("animation_list---",body_idleAnimations,ani)
            ani=that.scene.animationGroups.find(group => group.name === ani);
            ani1=that.scene.animationGroups.find(group => group.name === ani1);
            ani2=that.scene.animationGroups.find(group => group.name === ani2);
            console.debug("trying to play idle animation", ani.name,ani1.name,ani2.name);
            try {
                ani.play(true);
                ani1.play(true);
                ani2.play(true);
                ani.loopAnimation = false;
                ani1.loopAnimation = false;
                ani2.loopAnimation = false;
                if (!ani.onAnimationGroupEndObservable.hasObservers()) {
                    ani.onAnimationGroupEndObservable.add(() => {
                        console.log("Idle animation ended");
                        const randomTime = Math.floor(Math.random() * 8000) + 2000; // Random time between 2000ms (2s) and 10000ms (10s)
                        console.log(`Waiting for ${randomTime}ms to restart idle animation`);
                        setTimeout(() => {
                            this.playIdleAnimation(); // Restart the idle animation
                        }, randomTime);
                    });
                }

                this.generateIdleAnimations(this.idleBlinkArr, 'idleBlink');
                this.generateIdleAnimations(this.idleMotionArr, 'idleMotion');
                this.generateIdleAnimations(this.idleHeadArr, 'idleHead');
                this.generateIdleAnimations(this.idleBlinkTwiceArr, 'idleBlinkTwice');

                return true
            } catch (error) {
                console.log(error)
            }
        }

        return false;
    }
    this.stopIdleAnimation = function () {
        console.log("stop Idle Animation.")
        var that = this;
        let ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/weight_shift/g) });
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/weight/g) });
        }
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/wieght/g) });
        }
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle_pose/g) });
        }
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle_animation/g) });
        }
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle/g) });
        }
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idel/g) });
        }
        if (ani.length <= 0) {
            ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/thinking_1/g) });
        }
        ani = ani[0];
        if (ani) {
            console.debug("trying to stop idle animation", ani);
            try {
                ani.stop();
                ani.loopAnimation = false;
                clearInterval(intervalId);
            } catch (error) {
                console.error(error);
            }
        }
    }

    this.getScene = function () {
        return scene;
    }
    this.pauseAnimsandMorphs = function () {
        talk = false
        that.stopMorphs()
        that.playIdleAnimation()
    }
    //send object here to get audio object back
    this.playAgain = function () {
        shapesIndex = 0;
        framesIndex = 0;
        talk = true;
        that.snd.currentTime = 0;
        lastTime = Date.now();
        that.snd.play();
    }
    let that = this;

    this.play = function (response, force) {
        if (IOSsnd && that.snd && that.snd.src && that.snd.src == IOSsnd) {
            force = true;
        }
        if (that.snd && that.snd.src && !force) {
            console.warn("Sound already playing");
            queue.push(response);
            return that.snd;
        }
        console.log("src-->", that.snd)

        shapes = that.processData(response["shapes"]);
        frames = that.processDataFrames(response["frames"]);
        that["idleBlink"] = null;
        that["idleMotion"] = null;
        that["idleHead"] = null;
        that["idleBlinkTwice"] = null;
        that.stopIdleAnimation();
        if (that.snd) {
            if (that.snd.src) {
                that.snd.removeEventListener('pause', that.on_pause);
                that.on_pause(true);
            }
            that.snd.removeAttribute('src');
            that.snd.src = response["voice"];
            setTimeout(function () {
                if (that.snd.paused) {
                    that.on_canplay()
                }
            }, 500);
        } else {
            that.snd = new Audio(response["voice"]);
        }
        that.snd.loop = false;
        that.snd.addEventListener("canplay", that.on_canplay);
        that.snd.addEventListener("pause", that.on_pause);
        that.snd.addEventListener("ended", that.on_ended)
        return that.snd;
    }
    this.on_ended = function () {
        that.on_pause(true);
        if (queue.length > 0) {
            that.play(queue.shift());
        }
    }
    this.on_canplay = function () {
        shapesIndex = 0;
        framesIndex = 0;
        // that.snd.currentTime = 0;
        that.snd.volume = that.volume;
        lastTime = Date.now();
        talk = true;
        try {
            that.snd.play();
            DAVE_SETTINGS.execute_custom_callback('on_canplay_audio', [that.snd])
        } catch (err) {
            console.error(err);
            that.snd.src = null;
            DAVE_SETTINGS.execute_custom_callback('on_play_audio_error', [that.snd])
        }
    }
    this.on_pause = function (do_pause) {
        if (do_pause) {
            that.snd.pause();
        }
        that.snd.removeEventListener('ended', that.on_ended);
        that.snd.removeEventListener('canplay', that.on_canplay);
        that.snd.removeEventListener('durationchange', that.on_canplay);
        that.snd.removeEventListener('pause', that.on_pause);
        that.pauseAnimsandMorphs();
        that.snd.removeAttribute('src');
        that.snd.volume = 0;
        // that.snd.currentTime = 0;
        that.snd.loop = false;
        if (IOSsnd) {
            that.snd.loop = true;
            that.snd.src = IOSsnd;
        }
        DAVE_SETTINGS.execute_custom_callback('on_pause_audio', [that.snd])
    }

    this.stopMorphs = function () {
        // if (headers != []) {
        for (i = 1; i < headers.length; i++) {
            // s = scene.getMorphTargetByName(headers[i])
            // if (s != null) {
            //     s.influence = 0.00000001;
            // }
            s = this.setMorphTarget(headers[i], 0.0000001)

        }
        // }
    }
}
