djQ(document).ready(function () {
    //ASSET LOADING
    const iv_loader = djQ("#dave-settings").attr("data-IV-loader") || "./assets/img/loading.gif";
    const iv_closeIcon = djQ("#dave-settings").attr("data-IV-closeIcon") || "./assets/img/interactive_video/close.svg";
    const iv_likeIcon = djQ("#dave-settings").attr("data-IV-likeIcon") || "./assets/img/interactive_video/Like.svg";
    const iv_likeActiveIcon = djQ("#dave-settings").attr("data-IV-likeActiveIcon") || "./assets/img/interactive_video/Like-active.svg";
    const iv_dislikeIcon = djQ("#dave-settings").attr("data-IV-dislikeIcon") || "./assets/img/interactive_video/Dislike.svg";
    const iv_dislikeActiveIcon = djQ("#dave-settings").attr("data-IV-dislikeActiveIcon") || "./assets/img/interactive_video/Dislike-active.svg";
    const iv_inputSendIcon = djQ("#dave-settings").attr("data-IV-inputSendIcon") || "./assets/img/interactive_video/send.png";
    const iv_forumMinIcon = djQ("#dave-settings").attr("data-IV-forumMinIcon") || "./assets/img/interactive_video/Minimize.svg";
    const iv_forumMaxIcon = djQ("#dave-settings").attr("data-IV-forumMaxIcon") || "./assets/img/interactive_video/Max.svg";

    //GLOBAL VAR SETTING
    video = undefined;
    videoActionAllowed = false;
    chatFuncExe = true;
    postQuestion = false;
    videoResumePoint = false;
    branchBtnClickClass = undefined;

    //OBJECT TO HOLD TEMPRORY DATA
    window.temp_holder = {
        'videoCurTime': undefined,
        'f_half_height': undefined,
        'quesToPost': undefined,
        'ansToPost': undefined,
        'chatType': 'normal',
        'quesPostTime': undefined,
        'quesStartTime': undefined,
    }

    function adjustForumHeight() {
        temp_holder['f_half_height'] = djQ(".f-half").height();
        djQ(".s-half").height(temp_holder['f_half_height']);
    }

    function getWindowWidth() {
        let windowWidth = djQ(window).width();
        return windowWidth;
    }

    function setPlayerWidth(forumStatus) {
        console.log("got Triggered")
        console.log(getWindowWidth());
        if (getWindowWidth() >= 1366) {
            if (forumStatus == "min") {
                djQ(".bottom-f-half").addClass("bottom-f-half-max");
                djQ(".video-container").addClass("video-container-max");
            } else {
                djQ(".bottom-f-half").removeClass("bottom-f-half-max");
                djQ(".video-container").removeClass("video-container-max");
            }
        }
    }
    //ANY ACTIVITY TO BE PERFORMED ON WINDOW RESIZING
    djQ(window).resize(function () {
        //SETTING HEIGHT FOR S-HALF IN ACCORDANCE TO F-HALF
        console.warn("window resize")
        djQ(document).on("fullscreenchange", video, function () {
            fullScreen_leftPanelAdjustment = undefined;
            if (video.isFullscreen()) {
                fullScreen_leftPanelAdjustment = true;
            } else {
                fullScreen_leftPanelAdjustment = false;
                adjustForumHeight();
            }
        });
        if (!video.isFullscreen()) {
            djQ(document).unbind("mousemove", function(){
                console.log("Mouse move detection stopped")
            })
            adjustForumHeight();
        }

    });
    // SCROLL THE FORUM AREA
    function scrollForumArea(speed) {
        speed = speed || 400;
        djQ(".qna-body-container").animate({ scrollTop: djQ('.qna-body-container').prop("scrollHeight") }, speed);
    }

    // FETCH VIDEO DATA ENDS
    let pp_buttonStatus = false;
    function pp_button(pp_status, action) {
        action = action || false;
        // if (pp_status) {
        //     djQ(".play-pause-img").attr("src", "./assets/img/interactive_video/Play.svg");
        //     if(action && videoActionAllowed){
        //         video.pause();
        //     }
        // } else {
        //     djQ(".play-pause-img").attr("src", "./assets/img/interactive_video/Pause.svg");
        //     if(action && videoActionAllowed){
        //         video.play();
        //     }
        // }
    }
    //PLAY PAUSE BUTTON ON CLICK
    // djQ('.play-pause-button').on("click", function () {
    //     pp_button(pp_buttonStatus, true);
    //     pp_buttonStatus = !pp_buttonStatus;
    // });

    //All work to perform after videoplayer is loaded completely
    window.secToHms = function (sec) {
        sec = parseInt(sec);
        let h = Math.floor(sec / 3600);
        let m = Math.floor(sec % 3600 / 60);
        let s = Math.floor(sec % 3600 % 60);

        let n_h = h > 0 ? (h < 10 ? "0" + h + ":" : h + ":") : '';
        let n_m = m > 0 ? (m < 10 ? "0" + m + ":" : m + ":") : "00:"
        let n_s = s > 0 ? (s < 10 ? "0" + s : s) : "00";

        return n_h + "" + n_m + "" + n_s;
    }

    window.hmsToSec = function (hms) {
        let hms_length = hms.length;
        let hts_temp = hms.split(":");
        let h = null, m = null, s = null, totalSec = null;
        switch (hms_length) {
            case 2:
                totalSec = parseInt(hts_temp[0]);
                break;
            case 5:
                totalSec = parseInt(hts_temp[0] * 60) + parseInt(hts_temp[1]);
                break;
            case 8:
                totalSec = parseInt(hts_temp[0] * 3600) + parseInt(hts_temp[1] * 60) + parseInt(hts_temp[2]);
                break;
        }
        return totalSec;
    }

    window.generateRandomNum = function () {
        return Math.floor((Math.random() * 9999) + 1);
    }


    //GET THE FIRST BRANCH THAT NEEDS TO BE SHOWN (REDUCE THE UNWANTED PROCESSING)
    function firstBranchToDisplay(data) {
        let temp = undefined;
        $.each(data, function (i, j) {
            let z = j['timestamp'];
            if (z < temp || typeof temp == "undefined") {
                temp = z;
            }
        });
        return temp;
    }

    // GET THE FIRST TAG THAT NEEDS TO BE SHOWN (REDUCE THE UNWANTED PROCESSING)
    function firstTagToDisplay(data) {
        let temp = undefined;
        $.each(data, function (i, j) {
            let startPoint = j[0]['start_time'];
            if (startPoint < temp || typeof temp == 'undefined') {
                temp = startPoint;
            }
        });

        return temp;
    }

    function showAvatar(status) {
        if (status) {
            djQ(".daveAvatarIVCont").css("display", "block");
        } else {
            djQ(".daveAvatarIVCont").css("display", "none");
        }
    }

    function daveSceneLoad(a_xCord, a_yCord, a_hCord) {
        console.log("DAVE SCENE IS CALLED")
        DAVE_SCENE.onload = function () {
            dave_avatar_loadStatus = true;
            /*let avatarWidth =  djQ("#dave-settings").attr("data-avatar-position-w") || null;*/
            DAVE_SCENE.set_avatar_position(a_xCord, a_yCord, a_hCord);
            console.log("Avatar is loaded")
        }
    }

    //CHAT RESPONSE ELEMENTS
    function chatResponseElem(responseText, showSatisfactionForm, errorBubbleClass) {
        showSatisfactionForm = showSatisfactionForm || false;
        errorBubbleClass = errorBubbleClass || "";

        chatFuncExe = true;
        showAvatar(true);

        djQ(".IV_chatBubble").remove();

        //APPEND THE CHAT BUBBLE BELLOW AVATAR
        djQ(".daveAvatarIVCont").append(`
                    <p class="IV_chatBubble ${errorBubbleClass}">${responseText}</p>
                `).ready(function () {
            adjustForumHeight();
        });

        //APPEND THE RESPONSE SATISFACTION REVIEW FORM 

        djQ(document).find(".review-answer").remove();
        if (showSatisfactionForm) {
            djQ("#dave-videoPlayer").append(`
                    <div class="review-answer" x-data="reviewAnswerFront();">
                        <div x-show="showReviewSec">
                            <p class="review-question" x-text="questionText"></p>
                            <template x-if="!reply">
                                <div class="review-button-holder">
                                    <button class="review-button r_yes" @click="reply = true, yes = true, no = false, thankyou = true, showReviewSec = false, setTimeout(() => thankyou = false, 5000), alp_resumeVideo()">Yes</button>
                                    <button class="review-button r_no"  @click="reply = true, yes = false, no = true, questionText='You can still get the answers'">No</button>
                                </div>
                            </template>
                            <template x-if="no">
                                <div class="review-button-holder">
                                    <button class="review-button a_expert" @click="showReviewSec = false;xPostForum();" id='askExpert'>Ask Expert</button>
                                    <button class="review-button a_community" @click="showReviewSec = false;xPostForum()" id='askCommunity'>Ask Community</button>
                                </div>
                            </template>
                        </div>
                        <p x-show="thankyou" class="review-thankyou">Thank You!</p>
                    </div>
            `);
        }

        videoResumePoint = true;
        video.pause();
    }

    //************************* TEST INTERACTIVE VIDEO PAGE HANDLING ******************************//
    let IV_leftBar = `
    <div class="s-half">
        <div class="maxForum"><img src="${iv_forumMaxIcon}" alt="Maximise Forum" /></div>
        <div class="qna-container">
            <div class="qna-title-container">
                <div class="qna-title"><strong>User Question & Answers</strong></div>
                <div class="title-icon dave-forum-minimize"><img src="${iv_forumMinIcon}" alt='Minimize'></div>
            </div>
            <div class="qna-body-container">
                <template x-for="(questions, q_index) in ques_array" :key="q_index">
                    <div class="qna-box">
                        <ol x-data="{showmore : false, ansButton: false}">
                            <li class="qna-video-timestamp" @click="qnaTimestamp($el.textContent)" x-text="questions.quesTimestamp"></li>
                            <li class="qna-question"><strong class="qna-ques-symbol">Q:</strong><strong class="qna-ques-frame" x-text="questions.ques"></strong></li>
                            <li class="qna-answerButton" @click="ansButton = !ansButton, alp_pauseVideo(ansButton)">Answer this quetion</li>
                            <li class="qna-answerInput-container" x-show="ansButton" x-transition.duration.150ms.scale.orign.top>
                                
                                <textarea minlength="2" type="text" name="qna-answerInput" class="qna-answerInput" placeholder="Please write your answer."></textarea>
                                <input type="submit" value="Submit" class="input-answerSubmit">
                            </li>
                            <li class="qna-answers-container">
                                
                            </li>
                        </ol>
                        <hr>
                    </div>
                </template>
            </div>
        </div>
    </div>
    `;
    let IV_bottomBar = `
    <div class="bottom-f-half">
        <div class="player-controls">
            <div class="video-title-container">
                <strong id="video-title"></strong>
                 <p id="video-author"></p>
            </div>
            
            <div class="video-cta" x-data='{subscribe: false, like: false, dislike: false}'>
                <button class="video-subscribe" @click="subscribe = !subscribe" :class="{'video-subscribed' : subscribe}" x-text="subscribe ? 'Subscribed' : 'Subscribe'"></button>
                <button class="video-like" @click="like = !like" :class="{'video-liked': like}">
                    <img :src="like ? '${iv_likeActiveIcon}' : '${iv_likeIcon}'" alt="" class="like-img" />
                    <span x-text="like ? ' Liked' : ' Like'"></span>
                </button>
                <button class="video-dislike" :class="{'video-disliked': dislike}" @click="dislike = !dislike"><img :src="dislike ? '${iv_dislikeActiveIcon}' : '${iv_dislikeIcon}' " alt="" class="dislike-img"><span x-text="dislike ? ' Disliked' : ' Dislike'"></span></button>
            </div>
        </div>
        <hr>
        <div class="qna-input-container" x-data="{mic_active: false, qna_submit_img: false}">
            <div class="qna-input-Button">
                <p class="postQuesErr err_txt" x-text="ques_err"></p>
                <input type="text" class="qna-input" id='communityQuesInput' placeholder="ask your questions here .." x-model="temp_ques" autofocus>
                <button class="qna-input-submit" @mouseover="qna_submit_img = true" @mouseleave="qna_submit_img = false" @click="xPostQues(); temp_ques='' ">
                    <template x-if="qna_submit_img != undefined">
                        <img :src="qna_submit_img ? './assets/img/interactive_video/sendhover.png' : '${iv_inputSendIcon}'" alt="Submit Your Question" class="qna-input-submitSvg">
                    </template>
                </button>
            </div>
            <button class="qna-mic-button" id="start-recording"><img src="./assets/img/interactive_video/Mic.svg" alt="" class="mic-img"></button>
            <button class="qna-mic-button qna-mic-button-active" id="stop-recording"><img src="./assets/img/interactive_video/Mic.svg" alt="" class="mic-img"></button>
        </div>
    </div>
    `;
    //Open Interactive Video as POPUP
    djQ(".video-anchor").on("click", function (event) {
        event.preventDefault();
        djQ("body").css({ "overflow": "hidden" })
        djQ("body").append(`
            <div class='dave-iv-popup'>
            <div class="videoPlayerLoader">
                <img src="${iv_loader}" alt="loading dave interactive video player">
            </div>
            <div class="dave-popup-close"><img src="${iv_closeIcon}" alt="close dave interactive video" /></div>
                <div class="IV_conatiner" x-data="ivBody()">
                    <div class="f-half">
                        <div class="video-container">
                            <video
                            id="dave-videoPlayer"
                            class="vjs-big-play-centered video-js"
                            data-dave-video-id="">
                            <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a
                            web browser that
                            <a href="https://videojs.com/html5-video-support/" target="_blank"
                                >supports HTML5 video</a>
                            </p>
                            </video>
                        </div>
                        ${IV_bottomBar}
                    </div>
                    ${IV_leftBar}
                </div>
            </div>
        </div>
        `).ready(function () {
            //AVATAR CODE BLOCK STARTS
            let data_avatar_initChk = djQ("#dave-settings").attr("data-avatar-id") || false;
            
            dave_avatar_loadStatus = false;
            DAVE_SCENE.set_avatar_position = function (x, y, h) {
                x = x || 0;
                y = y || 0;
                h = h || 200;
                djQ(".daveAvatarIVCont").css({ "left": x, "bottom": y });
                djQ("canvas#dave-canvas").height(h);
            }

            if (data_avatar_initChk) {
                console.log("THE AVATAR ID => "+data_avatar_initChk);
                djQ(document).find(".video-container").append(`
                <div class='daveAvatarIVCont'>
                    <canvas id='dave-canvas'>
                        <p>this is p</p>
                    </canvas>
                </div>
                `);

                let avatar_xCord = djQ("#dave-settings").attr("data-avatar-position-x") || null;
                let avatar_yCord = djQ("#dave-settings").attr("data-avatar-position-y") || null;
                let avatarHeight = djQ("#dave-settings").attr("data-avatar-position-h") || null;
                daveSceneLoad(avatar_xCord, avatar_yCord, avatarHeight);
            } else {
                //djQ(".daveAvatarIVCont").css("display", "none");
            }
            //AVATAR CODE BLOCKS ENDS
        })

        // FETCHING VIDEO DATA START

        DAVE_IV_SETTINGS.initialize_video_player("", function (data) {
            console.log(data);
            let videoData = data['video'];
            let video_tag = data['video_tag'];
            let video_branch = data['video_branch'];
            let forum_answer = data['forum_answer'];
            let forum_question = data['forum_question'];

            let videoTagMovment = {};
            let videoBranchtime = {};
            videoActionAllowed = true;

            // IF VIDEO PLAYER IS ALREADY INITIALIZED IT WILL DESTORY TO OLD INITIALIZATION SO YOU CAN INITIALIZE AGAIN FOR NEW VIDEO
            if (video) { video.dispose() }
            djQ(".videoPlayerLoader").remove();
            djQ(".IV_conatiner").addClass("show-IV_container");

            //VIDEO.JS PLAYER INITIALIZATION STARTING
            video = videojs("#dave-videoPlayer", {
                autoplay: false,
                controls: true,
                sources: [{
                    src: videoData['video_url'],
                    type: 'video/mp4',
                }],
                preload: 'auto',
                aspectRatio: '16:9',
                //poster: './assets/img/interactive_video/video-poster.jpg',
                loop: false,
                fluid: false,
                playbackRates: [0.25, 0.5, 1, 1.25, 1.5, 2],
                disablePictureInPicture: true,
            });


            //$(`<button>HEYNEYHEYEEYEHEYEHEY</button>`).appendTo($('#dave-videoPlayer')); 
            //VIDEO.JS PLAYER INITIALIZATION END


            //DISPLAYING ALL REQUIRED DATA ON PAGE W.R.T DATA RECIEVED
            djQ("#video-title").text(videoData['video_name']);
            djQ("video").attr("data-dave-video-id", videoData['video_id']);
            djQ(".player-controls").css("opacity", "1");
            adjustForumHeight();
            showAvatar(true);


            // **************************************************************************************************//
            // FILTER QUESTION IN ASCENDING ORDER OF TIME

            function compare(a, b) {
                if (a.timestamp < b.timestamp) {
                    return -1;
                }
                if (a.timestamp > b.timestamp) {
                    return 1;
                }
                return 0;
            }

            forum_question.sort(compare);
            // DISPLAYING ALL QUESTIONS IN QNA SECTION
            let forumQuestionShow = {}
            $.each(forum_question, function (i, j) {
                forumQuestionShow[secToHms(j['timestamp'])] = j;
            });

            // **************************************************************************************************//

            //CREATING THE VIDEO TAGS
            $.each(video_tag, function (index, item) {
                let video_tag_text = item['customer_response'];
                let video_tag_id = item['video_tag_id'];
                let video_tag_custState = item['customer_state'];

                videoTagMovment[video_tag_id] = item['position'];

                djQ("#dave-videoPlayer").append(`
                    <button class='video-tag-button video-tags hide-vTags' data-custState="${video_tag_custState}" data-video-tag-id="${video_tag_id}">
                        <span class="videoTagPlusIcon"><i class="fas fa-plus-circle"></i></span>
                        <span>${video_tag_text}</span>
                    </button>
                `);
            });

            // CREATING THE VIDEO BRANCHES
            $.each(video_branch, function (i, j) {
                let vb_question = j['question'];
                let vb_id = j['video_branch_id'];

                vb_timestamp = j['timestamp'];
                videoBranchtime[vb_id] = {};
                videoBranchtime[vb_id]['timestamp'] = vb_timestamp;
                videoBranchtime[vb_id]['responded'] = false;

                djQ("#dave-videoPlayer").append(`
                    <div class="video-branch-container hide-vBranch" data-vbID = "${vb_id}">
                        <p class="video-branch-question">${vb_question}</p>
                        <div class="video-branch-option-container"></div>
                    </div>
                `);

                $.each(j['options'], function (x, y) {
                    let cust_state = y['customer_state'];
                    let cust_response = y['customer_response'];
                    let v_redirectTimestamp = y['redirect'] || "";

                    djQ("#dave-videoPlayer").find(`[data-vbID = "${vb_id}"]`).find(".video-branch-option-container").append(`<button class="video-branch-option" data-custState="${cust_state}" data-redirectTime="${v_redirectTimestamp}">${cust_response}</button>`);
                });
            });

            branchDisplayStartTime = firstBranchToDisplay(videoBranchtime);
            tagDisplayStartTime = firstTagToDisplay(videoTagMovment);

            //TAGS ON CLICK
            djQ(document).one("click", ".video-tag-button", function () {
                let tag_custState = (djQ(this).text()).trim();
                let tag_custResp = djQ(this).attr("data-custState");
                let tag_id = djQ(this).attr("data-video-tag-id");

                console.log("you clicked on tags")
                djQ(this).remove();

                DAVE_SETTINGS.chat({ "customer_state": tag_custResp, "customer_response": tag_custState, "query_type": "click" }, function (data) {
                    console.log(data)
                    chatResponseElem(data['placeholder']);
                });
            })

            //BRANCH ON CLICK  
            djQ(document).on("click", ".video-branch-option", function () {
                djQ(".video-branch-option").unbind("click").bind("click", function (e) {
                    let branch_custState = djQ(this).attr("data-custState");
                    let branch_custResp = (djQ(this).text()).trim();
                    let branch_id = djQ(this).closest(".video-branch-container").attr('data-vbid');
                    let videoRedirectTime = djQ(this).attr("data-redirectTime");
                    branchBtnClickClass = "successChatBubble";
                    
                    if(videoRedirectTime){
                        video.currentTime(videoRedirectTime);
                        branchBtnClickClass = "errorChatBubble";
                        console.warn("REDIRECTED TO TIMESTAMP")
                    }

                    videoBranchtime[branch_id]['responded'] = true;

                    djQ(this).closest(".video-branch-container").removeClass("show-vBranch").addClass("hide-vBranch");

                    DAVE_SETTINGS.chat({ "customer_state": branch_custState, "customer_response": branch_custResp, "query_type": "click" }, function (data) {
                        console.log(data);
                        chatResponseElem(data['placeholder'], false, branchBtnClickClass);
                    });
                })
            })


            djQ(document).on("focus", "input", function () {

            })

            // *****************************************VIDEO FEATURES********************************************* //
            video.on('loadedmetadata', function () {
                const totalVideoLength = video.duration();
                // djQ(".total-videoPlayTime").text(secToHms(totalVideoLength));

                // All work to perform when video is PLAYED
                video.on("play", function () {
                    // pp_button(false);
                    console.log(temp_holder['quesStartTime'] + " <======================")
                    if (videoResumePoint) {
                        video.currentTime(temp_holder['quesStartTime']);
                        videoResumePoint = false;
                        console.log(temp_holder['quesStartTime']);
                        console.log("resume point is executed");
                    }
                    showAvatar(false);
                });

                //ALL WORKS TO PERFORM WHEN VIDEO IS PLAYING...
                window.videoPlaying = function () {
                    let temp = 0;
                    djQ('video').bind('timeupdate', function () {
                        djQ(".IV_chatBubble").remove();
                        let cur_time = video.currentTime();
                        let sec = Math.floor(cur_time % 3600 % 60);

                        // djQ(".current-play-time").text(sec);

                        //THE BELLOW BLOCK OF IF(SEC) WILL ONLY TRIGGER EVERY SEC NOT MILLISECONDS
                        if (Number.isInteger(sec) && sec != temp) {
                            temp = sec;
                            // HERE WE WILL TRIGGER TO DISPLAY THE FIRST VIDEO TAG
                            if (cur_time >= tagDisplayStartTime) {
                                $.each(videoTagMovment, function (index, val) {
                                    $.each(val, function (i, j) {
                                        if (cur_time >= j['start_time'] && cur_time <= j['end_time']) {
                                            djQ("#dave-videoPlayer").find(`[data-video-tag-id = ${index}]`).addClass('show-vTags').removeClass('hide-vTags');
                                            djQ("#dave-videoPlayer").find(`[data-video-tag-id = ${index}]`).css({ 'left': j['x'] + '%', 'bottom': j['y'] + '%' });
                                        } else {
                                            djQ("#dave-videoPlayer").find(`[data-video-tag-id = ${index}]`).addClass('hide-vTags').removeClass('show-vTags');
                                        }
                                    })
                                });
                            }

                            // HERE WE WILL TRIGGER TO DISPLAY FIRST VIDEO BRANCH
                            if (cur_time >= branchDisplayStartTime) {
                                $.each(videoBranchtime, function (i, j) {
                                    if (cur_time >= j['timestamp'] && !j['responded']) {
                                        djQ("#dave-videoPlayer").find(`[data-vbID = "${i}"]`).removeClass("hide-vBranch").animate({
                                            opacity: '1',
                                            height: '65%',
                                        })
                                        video.pause();
                                    }
                                });
                            }

                            //DISPLAYING FORUM QUESTION ON IT'S POSTED TIME WITH RESPECT TO VIDEO PLAY TIME
                            let key = secToHms(cur_time);
                            if (forumQuestionShow.hasOwnProperty(key)) {
                                let createdTime = forumQuestionShow[key]['created'];
                                let quesID = forumQuestionShow[key]['forum_question_id'];
                                let ques = forumQuestionShow[key]['question'];
                                let videoTimestamp = forumQuestionShow[key]['timestamp'];
                                $(".qna-body-container").append(`
                                        <div class="qna-box" data-quesID='${quesID}'>
                                            <ol x-data="{showmore : false, ansButton: false}">
                                                <li class="qna-video-timestamp" @click="qnaTimestamp($el.textContent)">${secToHms(videoTimestamp)}</li>
                                                <li class="qna-question"><strong class="qna-ques-symbol">Q:</strong><strong class="qna-ques-frame">${ques}</strong></li>
                                                <li class="qna-answerButton" @click="ansButton = !ansButton, alp_pauseVideo(ansButton)">Answer this quetion</li>
                                                <li class="qna-answerInput-container" x-show="ansButton" x-transition.duration.150ms.scale.orign.top>
                                                    <textarea minlength="2" type="text" name="qna-answerInput" class="qna-answerInput" placeholder="Please write your answer."></textarea>
                                                    <input type="submit" value="Submit" class="input-answerSubmit">
                                                </li>
                                                <li class="qna-answers-container">
                                                
                                                </li>
                                                <li class="qna-see-more">
                                                    <img src="./assets/img/interactive_video/Down.svg" alt="" class="see-more-icon" x-show="!showmore">
                                                    <a href="" @click.prevent="showmore = !showmore"><strong class="see-more-title" x-text="showmore ? 'See Less' : 'See More' "></strong></a>
                                                </li>
                                            </ol>
                                            <hr>
                                        </div>
                                    `).ready(function () {
                                    let ansCount = 0;
                                    let ansLength = forumQuestionShow[key]['answers'].length;
                                    if (ansLength < 1) {
                                        $(".qna-body-container").last(".qna-box").find('.qna-see-more').remove();
                                    }
                                    $.each(forumQuestionShow[key]['answers'], function (x, y) {
                                        let answer = y['answer'];
                                        let created = y['created'];
                                        let quesID = y['forum_question_id'];
                                        let ansID = y['forum_answer_id'];
                                        let authorName = y['person_name'];
                                        if (ansCount < 1 || ansLength == 1) {
                                            $(".qna-body-container").find(`[data-quesID='${quesID}']`).find(".qna-answers-container ").append(`
                                                    <div class="qna-answers-block" data-ansID="${ansID}" data-qID = "${quesID}">
                                                        <div class="qna-answers"><div class="ans-prefix">A:</div><div class="ans-content">${answer}</div></div>
                                                        <div class="qna-author-container">
                                                            <div class="qna-author">By&nbsp${authorName}</div>
                                                            <div class="qna-author-timestamp">${DAVE_SETTINGS.print_timestamp(created, '', true)}</div>
                                                        </div>
                                                    </div>
                                                `);
                                        } else {
                                            $(".qna-body-container").find(`[data-quesID='${quesID}']`).find(".qna-answers-container").append(`
                                                    <div class="qna-answers-block" data-ansID="${ansID}" data-qID = "${quesID}" x-show='showmore' x-transition.duration.200ms>
                                                        <div class="qna-answers"><div class="ans-prefix">A:</div><div class="ans-content">${answer}</div></div>
                                                        <div class="qna-author-container">
                                                            <div class="qna-author">By&nbsp${authorName}</div>
                                                            <div class="qna-author-timestamp">${DAVE_SETTINGS.print_timestamp(created, '', true)}</div>
                                                        </div>
                                                    </div>
                                                `);
                                        }
                                        if (ansLength == 1) {
                                            $(".qna-body-container").find(`[data-quesID='${quesID}']`).find('.qna-see-more').remove();
                                        }
                                        ansCount++;
                                    });
                                    scrollForumArea();
                                    delete forumQuestionShow[key];
                                });
                            }
                            sec = null;
                        }
                    });
                }
                videoPlaying();

                //All work to perform when video is PAUSED
                video.on("pause", function () {
                    // pp_button(true);
                    showAvatar(true);
                })
            });

            //SETTING UP MIC COMPLETE FUNCTIONALITY

            if (DAVE_SETTINGS) {
                if (DAVE_SETTINGS.SPEECH_SEVER) {
                    StreamingSpeech.startRecordingButton = djQ('button#start-recording')[0];
                    StreamingSpeech.stopRecordingButton = djQ('button#stop-recording')[0];
                    //CHECK WHETHER THE PERMISSION IS ALREADY PROVIDED FOR MICROPHONE
                    navigator.permissions.query({ name: 'microphone' }).then(function (r) {
                        if (r.state === 'granted') {
                            DAVE_SETTINGS.micAccess = true;
                            console.log("Already We Have MIC");
                        } else {
                            DAVE_SETTINGS.micAccess = false;
                            console.log("Already We Don't have mic");
                        }
                    });
                    //HANDLE RECORDING BUTTON VISIBILITY
                    function recButton_visibility(startRecVis, stopRecVis) {
                        if (startRecVis) {
                            djQ("button#start-recording").css('display', startRecVis);
                        }
                        if (stopRecVis) {
                            djQ("button#stop-recording").css('display', stopRecVis);
                        }
                    }

                    recButton_visibility('block');

                    // StreamingSpeech.onSocketConnect = function (o) {
                    //     if (!nowRecording) {
                    //     //     djQ(".qna-mic-button").css("opacity","1");
                    //     // }
                    // }

                    //MIC BUTTON ON CLICK
                    djQ(".qna-mic-button").on('click', function (event) {
                        video.pause();
                        djQ("#communityQuesInput").val("");
                        event.stopPropagation();

                        //IF MICROPHONE IS NOT PERMITTED THEN ON MIC CLICK ASK FOR PERMISSION
                        if (!DAVE_SETTINGS.micAccess) {
                            navigator.mediaDevices.getUserMedia({ audio: true })
                                .then(function (auStream) {
                                    DAVE_SETTINGS.micAccess = true;
                                    console.log('Mic Access is provided')
                                })
                                .catch(function (e) {
                                    console.log('No mic access is provided')
                                });
                        }

                        //EXECUTE ONLY WHEN WE HAVE MIC ACCESS
                        if (DAVE_SETTINGS.micAccess) {
                            temp_holder['quesStartTime'] = video.currentTime();
                            // if (DAVE_SCENE.loaded) {
                            //     DAVE_SCENE.stop();
                            // }
                            recButton_visibility('none', 'block');
                            //SPEECH DATA AFTER PROCCESSING
                            StreamingSpeech.onTranscriptionAvailable = function (data) {
                                //SPEECH WORDS POPING WITHOUT PROPER SENTENCE
                                if (!data['final_text']) {
                                    let voice_rawData = JSON.stringify(data['rec_text'])
                                    voice_rawData = voice_rawData.slice(1, -1)
                                    djQ("#communityQuesInput").val(djQ("#communityQuesInput").val() + " " + voice_rawData);
                                }
                                //SPEECH WORDS FORMING PROPER SENTENCE AFTER FINAL_TEXT OBTAINED
                                else {
                                    let voice_rawData = JSON.stringify(data['final_text']);
                                    voice_rawData = voice_rawData.slice(1, -1)
                                    djQ("#communityQuesInput").val(voice_rawData);
                                    console.log(voice_rawData);
                                    console.log("Voice Question Raised at : " + temp_holder['quesStartTime']);
                                    //PUSH THIS VOICE_RAWDATA OBTAINED DIRECTLY TO COMMUNITY POST

                                }
                            };
                            //SPEECH DATA FOR REAL-TIME WITHOUT PROCCESSING
                            StreamingSpeech.onStreamingResultsAvailable = function (data) {
                                if (!data['final_text']) {
                                    let voice_rawData = JSON.stringify(data['rec_text'])
                                    voice_rawData = voice_rawData.slice(1, -1)
                                    djQ("#communityQuesInput").val(djQ("#communityQuesInput").val() + " " + voice_rawData);
                                }
                            }

                            //SPEECH RECORDING THROW ERROR
                            StreamingSpeech.onError = function (data) {
                                console.log(data);
                                console.log("^^^Speech onError is generated");
                            };

                            //SOCKET DISCONNECT
                            StreamingSpeech.onSocketDisConnect = function (o) {
                                recButton_visibility('none', 'none');
                            };

                        } else {
                            recButton_visibility('block', 'none');
                            console.log('permission is denied');
                            djQ("button#start-recording").attr('title', 'You have dis-allowed the microphone. Please go to settings to allow microphone for this website');
                        }

                        //STOP RECORDING BUTTON FUNCTION
                        djQ("#stop-recording").on("click", function () {
                            recButton_visibility('block', 'none');
                        });

                        //INITIALIZING SOCKET FOR RECORDING ANY AUDIO
                        StreamingSpeech.initSocket();
                    });
                } else {
                    console.log("Speech Server not set.");
                    djQ(".qna-mic-button").css("display", "none");
                }
            }
            // SUBMITTING USERS ANSWERS //
            djQ(document).on("click", ".input-answerSubmit", function () {
                let d = new Date();
                let quesID = djQ(this).closest(".qna-box").attr("data-quesid");
                let qna_timestamp = DAVE_SETTINGS.print_timestamp(d, '', true);
                let qna_answers = djQ(this).prev().val();
                let qna_author = "Aman";

                djQ(this).prev().val('');

                if (qna_answers.length > 0) {
                    DAVE_IV_SETTINGS.post_forum_answer(quesID,qna_answers, videoData['video_id'], {}, function(data){
                        console.log(data);
                    }, function(e){
                        console.log(JSON.stringify(e));
                    });
                    djQ(".answerError").remove();
                    djQ(this).parent().next().prepend(`
                    <div class="qna-answers-block">
                        <div class="qna-answers">A:&nbsp;<span>${qna_answers}</span></div>
                        <div class="qna-author-container">
                            <div class="qna-author">By <span>${qna_author}</span></div>
                            <div class="qna-author-timestamp">${qna_timestamp}</div>
                        </div>
                    </div>
                    `);
                } else {
                    djQ(this).parent().prepend(`<p class="answerError err_txt">Please enter your answer</p>`);
                }
            });

            djQ(document).on("click", ".review-button.r_no", function(){
                chatFuncExe = false;
                console.log(chatFuncExe+" <= no")
            })

            djQ(document).on("click", "#askExpert", function () {
                chatFuncExe = true;
                if (djQ(this).attr("data-responseBtnType") == "unknownResponse") {
                    temp_holder['quesStartTime'] = secToHms(temp_holder['quesStartTime']);
                    djQ(".unknownReviewFormHolder").remove();
                    djQ(".qna-body-container").prepend(`
                        <div class="qna-box" x-data="{showmore : false, ansButton: false}">
                            <ol>
                                <li class="qna-video-timestamp">${temp_holder['quesStartTime']}</li>
                                <li class="qna-question"><strong class="qna-ques-symbol">Q:</strong><strong class="qna-ques-frame">${temp_holder['quesToPost']}</strong></li>
                                <li class="qna-answerButton" @click="ansButton = !ansButton, alp_pauseVideo(ansButton)">Answer this quetion</li>
                                <li class="qna-answerInput-container" x-show="ansButton" x-transition.duration.150ms.scale.orign.top>
                                    <textarea minlength="2" type="text" name="qna-answerInput" class="qna-answerInput" placeholder="Please write your answer."></textarea>
                                    <input type="submit" value="Submit" class="input-answerSubmit">
                                </li>
                                <li class="qna-answers-container">
                                    
                                </li>
                            </ol>
                            <hr>
                        </div>
                    `);
                }
                DAVE_IV_SETTINGS.post_forum_question(temp_holder['quesToPost'], videoData['video_id'], true, { "timestamp": Number(temp_holder['quesStartTime']) }, function (data) {
                    console.log(data);
                    adjustForumHeight();
                }, function (e) {
                    console.log(JSON.stringify(e));
                });

                video.play();
            });

            djQ(document).on("click", "#askCommunity", function () {
                chatFuncExe = true;
                if (djQ(this).attr("data-responseBtnType") == "unknownResponse") {
                    temp_holder['quesStartTime'] = secToHms(temp_holder['quesStartTime']);
                    djQ(".unknownReviewFormHolder").remove();
                    djQ(".qna-body-container").prepend(`
                        <div class="qna-box" x-data="{showmore : false, ansButton: false}">
                            <ol>
                                <li class="qna-video-timestamp">${temp_holder['quesStartTime']}</li>
                                <li class="qna-question"><strong class="qna-ques-symbol">Q:</strong><strong class="qna-ques-frame">${temp_holder['quesToPost']}</strong></li>
                                <li class="qna-answerButton" @click="ansButton = !ansButton, alp_pauseVideo(ansButton)">Answer this quetion</li>
                                <li class="qna-answerInput-container" x-show="ansButton" x-transition.duration.150ms.scale.orign.top>
                                    <textarea minlength="2" type="text" name="qna-answerInput" class="qna-answerInput" placeholder="Please write your answer."></textarea>
                                    <input type="submit" value="Submit" class="input-answerSubmit">
                                </li>
                                <li class="qna-answers-container">
                                    
                                </li>
                            </ol>
                            <hr>
                        </div>
                    `);
                }
                DAVE_IV_SETTINGS.post_forum_question(temp_holder['quesToPost'], videoData['video_id'], false, { "timestamp": Number(temp_holder['quesStartTime']) }, function (data) {
                    console.log(data);
                    adjustForumHeight();
                }, function (e) {
                    console.log(JSON.stringify(e));
                });

                video.play();
            })

            //INPUT VALUE --- INPUT BOX FUNCTION BLOCK
            djQ("input.qna-input").on("keyup", function (e) {
            });

            // USERS QUESTIONS POSTING WORKS //
            djQ(document).on("click", ".qna-input-submit", function () {
                if (postQuestion) {
                    //console.log("Question Typed: " + temp_holder['videoCurTime']);
                    //console.log("Question Posted Time" + video.currentTime());
                    temp_holder['quesPostTime'] = video.currentTime();
                    chatFuncExe = true;

                    adjustForumHeight();
                    //
                    if (temp_holder['chatType'] == 'normal') {
                        DAVE_SETTINGS.chat({ 'customer_response': temp_holder['quesToPost'], "query_type": "type" }, function (data) {
                            console.log(data)
                            chatResponseElem(data['placeholder'], true);
                        }, function (e) {
                            console.log(e);
                        }, function (unknownData) {
                            console.log(unknownData);
                            console.log("_____________UNKNOWN_________________");
                            chatResponseElem(unknownData['placeholder'], false);
                            djQ("#dave-videoPlayer").append(`
                                    <div class="unknownReviewFormHolder" x-data="reviewAnswerFront()">
                                        <p>Please Try Asking Our Experts or Community</p>
                                        <div class="review-button-holder">
                                            <button class="review-button a_expert" data-responseBtnType = "unknownResponse" id='askExpert' @click="xPostForum()">Ask Expert</button>
                                            <button class="review-button a_community" data-responseBtnType = "unknownResponse" id='askCommunity' @click="xPostForum()">Ask Community</button>
                                        </div>
                                    </div>
                                `);
                        });
                    } else {
                        showAvatar(false);
                        chatFuncExe = true;
                    }
                }
            });

            //GET VIDEO TIMESTAMP FOR ANY ACTIVITY

            //VIDEO TIMESTAMP FOR QUESTION POST
            djQ(document).on("keyup", "#communityQuesInput", function () {
                let inputVal = djQ(this).val();
                let inputLength = inputVal.length;
                djQ(document).find(".review-answer").remove();
                if (inputLength == 1) {
                    //The time when user started typing question
                    temp_holder['quesStartTime'] = video.currentTime();
                    console.log(temp_holder['quesStartTime']);
                }
                if (chatFuncExe) {
                    temp_holder['quesToPost'] = inputVal;
                    console.log("CHAT func exe")
                }
                if (inputLength > 0) {
                    postQuestion = true;
                } else {
                    postQuestion = false;
                }
            });

            //VIDEO PLAYER IN FULL SCREEN MODE ALL FUNCTIONS
            djQ(document).on("fullscreenchange", video, function () {
                let s_halfHidden = true, bottom_f_halfHidden = true;
                let s_half = djQ(document).find(".s-half");
                let bottom_f_half = djQ(document).find(".bottom-f-half");
                let setTime = undefined;
                if (video.isFullscreen()) {
                    s_half.appendTo("#dave-videoPlayer");
                    s_half.addClass("s-halfFullScreen");
                    s_half.css("height", "83vh");

                    bottom_f_half.appendTo("#dave-videoPlayer");
                    bottom_f_half.addClass("bottom-f-halfFullScreen");

                    //ACTION TO BE PERFORMED WHEN VIDEO IS PLAYING
                    function fullScreenVideoPlaying() {
                        djQ('video').bind("timeupdate", function () {
                            djQ(document).on("mousemove", "#dave-videoPlayer", function () {
                                console.log("mouse moving")
                                if (s_halfHidden && bottom_f_halfHidden) {
                                    s_half.addClass("s-halfFullScreenShow");
                                    bottom_f_half.addClass("bottom-f-halfFullScreenShow");

                                    s_halfHidden = false;
                                    bottom_f_halfHidden = false;

                                    setTime = setTimeout(function () {
                                        s_halfHidden = true;
                                        bottom_f_halfHidden = true;

                                        s_half.removeClass('s-halfFullScreenShow')
                                        bottom_f_half.removeClass("bottom-f-halfFullScreenShow");
                                    }, 3000);
                                }
                            });
                        });
                    };
                    fullScreenVideoPlaying();

                    //ACTION TO BE PERFOMED WHEN VIDEO IS CLICKED TO PAUSE
                    djQ('video').on("pause", function () {
                        clearTimeout(setTime);
                        djQ('video').unbind('timeupdate');
                        s_half.addClass("s-halfFullScreenShow");
                        bottom_f_half.addClass("bottom-f-halfFullScreenShow");
                    });

                    //ACTION TO BE PERFORMED WHEN VIDEO IS CLICKED TO PLAY
                    djQ('video').on("play", function () {
                        s_halfHidden = true;
                        fullScreenVideoPlaying();
                        s_half.removeClass("s-halfFullScreenShow");
                        bottom_f_half.removeClass("bottom-f-halfFullScreenShow");
                    });
                } else {
                    djQ("video").unbind("timeupdate");
                    djQ(document).off("mousemove", "#dave-videoPlayer");
                    videoPlaying();
                    s_half.appendTo(".IV_conatiner");
                    s_half.removeClass("s-halfFullScreen");

                    bottom_f_half.appendTo(".f-half");
                    bottom_f_half.removeClass("bottom-f-halfFullScreen");

                    adjustForumHeight();
                }
            });
        });
    });

    //CLOSE INTERACTIVE VIDEO POPUP
    djQ(document).on("click", ".dave-popup-close", function () {
        djQ(".dave-iv-popup").remove();
    });

    //Minimize the forum
    djQ(document).on("click", ".dave-forum-minimize", function () {
        djQ(".s-half").addClass("hide-shalf");
        djQ(".qna-container").addClass("hide-qna-container")
        djQ(".f-half").width("100%");
        djQ(".maxForum").addClass("showMaxForum");
        setPlayerWidth("min");
    });

    //Maximize the forum
    djQ(document).on("click", ".showMaxForum", function () {
        djQ(".s-half").removeClass("hide-shalf");
        djQ(".qna-container").removeClass("hide-qna-container")
        djQ(".f-half").width("75%");
        djQ(".maxForum").removeClass("showMaxForum");
        setPlayerWidth("max");
    });

    function addBar(appendElement, forumClass, bottomBarClass) {
        if (typeof webTagTimeOut == "undefined") {
            console.log("added");
            djQ(appendElement).append(`<div class="dave-iv-popup webTags-popup"></div>`);
            djQ(appendElement).append(IV_leftBar);
            djQ(".s-half").addClass(forumClass).attr("x-data", "ivBody()");
            djQ(appendElement).append(IV_bottomBar);
            djQ(".bottom-f-half").addClass(bottomBarClass).attr("x-data", "ivBody()");
        }
    }

    function removeBarElem() {
        djQ(".dave-iv-popup").remove();
        djQ(".s-half").remove();
        djQ(".bottom-f-half").remove();
        djQ(".daveWebTagAvatar").remove();
        webTagTimeOut = undefined;
    }

    function removeBar() {
        if (typeof webTagTimeOut == "undefined") {
            webTagTimeOut = setTimeout(function () {
                console.log("removed");
                removeBarElem();
            }, 5000)
        }
    }

    djQ(document).on("click", ".webTags-s-half, .webTags-bottom-f-half", function () {
        clearTimeout(webTagTimeOut);
    });
    djQ(document).on("click", ".webTags-popup", function () {
        removeBarElem();
    })

    //WEB TAGS ALL BLOCK OVER HERE
    djQ(document).on('click', `[data-daveWebTags]`, function () {
        //WE WILL SHOW RESPONSE PLACEHOLDER AND AVATAR BELLOW
        let webTagsAvatarPos = djQ(document).find(".bottom-f-half").height() + 40;
        console.log(webTagsAvatarPos)
        djQ(".dave-iv-popup").append(`
        <div class='daveAvatarIVCont daveWebTagAvatar' style="position: fixed;bottom: ${webTagsAvatarPos}px; display: block;">
            <canvas id='dave-canvas'>
                <p>this is p</p>
            </canvas>
        </div>
        `);

        daveSceneLoad("0px", "0px", '200px')
    });

    djQ(`[data-daveWebTags]`).hover(
        function () {
            addBar("body", "webTags-s-half", "webTags-bottom-f-half");
        },
        function () {
            removeBar();
        }
    );
});
// **********============================ ALPINE Functions ONLY ==================******************* //
function ivBody() {
    return {
        // POSTING USERS QUESTIONS
        temp_ques: '',
        ques_err: '',
        ques_array: [],

        xPostForum() {
            if (!chatFuncExe) {
                console.log("CHAT func NOT exe")
                this.ques_array.push({
                    ques: temp_holder['quesToPost'],
                    quesTimestamp: secToHms(temp_holder['quesStartTime']),
                });
            }
        },

        xPostQues() {
            if (this.temp_ques.length > 0) {
                this.ques_err = "";
            } else {
                this.ques_err = "please enter your question"
            }
        },
        qnaTimestamp(time) {
            video.currentTime(hmsToSec(time));
            video.play();
            console.log(time)
        },
        alp_resumeVideo() {
            video.currentTime(temp_holder['quesStartTime']);
            video.play();
        },
        alp_pauseVideo(status) {
            video.pause();
            if (status == false) {
                video.play();
            }
        }
    }
}

function reviewAnswerFront() {
    return {
        reply: false,
        yes: undefined,
        no: undefined,
        thankyou: false,
        after_reply: undefined,
        questionText: "Satisfied with the answer?",
        showReviewSec: true,
    }
}
	// **********============================ ALPINE Functions ONLY ==================******************* //