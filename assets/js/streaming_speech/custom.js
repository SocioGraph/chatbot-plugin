var BLOB = undefined;
var AUDIO = undefined;
var startRecording = undefined;
var stopRecording  = undefined;
var recordAudio = undefined;
var recognition_sid = undefined;
var recgIntervalMap = {};
var is_recording = false;
var selectedRecognizer = undefined;
var socketio = undefined;
var socket = undefined;
var resultpreview = undefined;
var startAudioRecording = undefined;
var stopAudioRecording = undefined;
var audioContext = undefined;

var onSocketConnect = undefined;
var onSocketDisConnect = undefined;
var onTranscriptionAvailable = undefined;
var onError = undefined;
var onStreamingResultsAvailable = undefined;
var onConversationResponseAvailable = undefined;

function makeid(length) {
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

var uid = makeid(6);

//RTC Recorder functions.

//Load recorded audio on browser.
function replaceAudio(src) { 
    var newAudio = document.getElementById("playback");
    newAudio.controls = true; 
    newAudio.autoplay = true; 
    if(src) { 
        newAudio.src = src;
    }
    AUDIO = newAudio; 
}


function setupRTC() {
    startAudioRecording = function() {
        if (!audioContext) {
            setupVAD();
        }
        startRecording.disabled = false;
        stopRecording.disabled = true;  

        is_recording = true;
        navigator.getUserMedia(
            {audio: true}, 
            function(stream) {
                recordAudio = RecordRTC(
                    stream, 
                    {
                        type: 'audio',
                        mimeType: 'audio/wav',
                        sampleRate: 44100,
                        timeSlice: 1000,
                        bufferSize : 1024,
                        recorderType: StereoAudioRecorder,
                        numberOfAudioChannels: 1,
                    
                        ondataavailable: function(blob) {
                            if (!recognition_sid) {
                                recognition_sid = makeid(8);
                                recgIntervalMap[recognition_sid] = createPolling(uid, recognition_sid);
                            }

                            // console.log(blob);

                            let audio_payload = {
                                "enterprise_id":DAVE_SETTINGS.getCookie("authentication")["X-I2CE-ENTERPRISE-ID"],
                                "conversation_id" : DAVE_SETTINGS.CONVERSATION_ID,
                                "customer_id" : DAVE_SETTINGS.getCookie("user_id"),
                                "engagement_id": DAVE_SETTINGS.ENGAGEMENT_ID,
                                "api_key" : DAVE_SETTINGS.getCookie("authentication")["X-I2CE-API-KEY"],
                                "system_response" : DAVE_SETTINGS.SYSTEM_RESPONSE,
                                "timestamp" : "timestamp",
                                "session":"1234",
                                "size":blob.size,
                                "blob":blob, 
                                "recognition_sid":recognition_sid, 
                                "is_recording":is_recording, 
                                "selectedRecognizer": DAVE_SETTINGS.RECOGNIZER,
                                "server" :DAVE_SETTINGS.BASE_URL
                            };


                            // let payload = '['+DAVE_SETTINGS.enterprise_id+']['+DAVE_SETTINGS.conversation_id+']['+DAVE_SETTINGS.customer_id+']['+DAVE_SETTINGS.engagement_id+']['+DAVE_SETTINGS.api_key+']['+DAVE_SETTINGS.system_response+'][timestamp]||||{"session":1234,"size":'+blob.size+',"blob":'+blob+',"recognition_sid":'+recognition_sid+',"is_recording":'+is_recording+',"selectedRecognizer":'+DAVE_SETTINGS.recognizer+'}||||""||||';
                            // console.log(payload);
                            socketio.emit('astream', audio_payload);
                        }
                    }
                );
                
                recordAudio.startRecording();
                stopRecording.disabled = false;
            }, 
            function(error) {console.error(JSON.stringify(error));}
            );
    };

    stopAudioRecording = function() {
        // recording stopped
        startRecording.disabled = false;
        stopRecording.disabled = true;
        is_recording = false;
        let payload = {"size":0, "blob":"", "recognition_sid":recognition_sid, "is_recording":is_recording};
        socketio.emit('astream', payload);
        //
        //// stop audio recorder
        recordAudio.stopRecording(function() {

        });
    };

}

//Polling functions.
//This function polls wesocket server for intermediate results every 2 seconds.
function createPolling(uid, recognition_sid, interval = 2000) {
    const intervalObject = setInterval(function() {
        // console.log("This messsage gets printed every 2 seconds.");
        socketio.emit("intermediateResults",{"uid":uid,"sid":recognition_sid});
    }, interval);
    
    return intervalObject;
}

function stopPolling(intervalObject) {
    clearInterval(intervalObject);
    recognition_sid = undefined;
}

//VAD
// Define function called by getUserMedia 
function startUserMedia(stream) {
	// Create MediaStreamAudioSourceNode
	var source = audioContext.createMediaStreamSource(stream);
	
	// Setup options
    	var options = {
		source: source,
		voice_stop: function() {
			// console.log('voice_stop');
            if (recordAudio) {
                stopAudioRecording();
                audioContext = undefined;
            }
		}, 
		voice_start: function() {
			// console.log('voice_start');
			//startAudioRecording();
		}
	}; 
    
    	// Create VAD
	var vad = new VAD(options);
}

function setupVAD() {
	// console.log("Setting up VAD.");
	window.AudioContext = window.AudioContext || window.webkitAudioContext;
	audioContext = new AudioContext();

	navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
	navigator.getUserMedia(
		{audio: true}, 
		startUserMedia, 
		function(e) {
			// console.log("No live audio input in this browser: " + e);
		});
}


function initSocket() {
	// console.log("Setting up Socketio.");
	var uid = makeid(6);
        socketio = io(DAVE_SETTINGS.SPEECH_SERVER, {query:"uid="+uid, transports: ['websocket']});

	//Socket Events
	socketio.on('connect', function(e) {
	    startRecording.disabled = false;
	    onSocketConnect();
	});
	
	socketio.on('disconnect', function () {
	    onSocketDisConnect();
	});
	
	socketio.on('results', function (data) {
	    // console.log(data);
	    onTranscriptionAvailable(data); 
	});
	
	socketio.on('intermediateResults', function(data) {
	    // console.log(data);
	    if (data["is_final"] == true) {
	        // console.log("Final data is received. Stopping polling.");
	        clearInterval(recgIntervalMap[recognition_sid]);
            recognition_sid = undefined;
	    }
	    
	    onStreamingResultsAvailable(data);
	});
	
	socketio.on('error', function(data){
	    // console.log("backend error");
	    onError(data);
	});
	
	socketio.on('recText', function(data){
	    // console.log("recText: "+data);
	});
	
	socketio.on('convResp', function(data){
	    // console.log("convResp: "+data);
	    onConversationResponseAvailable(data);
	});
	
	// End of socket events.


}
