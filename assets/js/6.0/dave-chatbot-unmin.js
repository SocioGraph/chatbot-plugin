!(function (e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports
        ? (module.exports = e.document
              ? t(e, !0)
              : function (e) {
                    if (!e.document) throw new Error("jQuery requires a window with a document");
                    return t(e);
                })
        : t(e);
})("undefined" != typeof window ? window : this, function (w, e) {
    "use strict";
    function g(e) {
        return null != e && e === e.window;
    }
    var t = [],
        n = Object.getPrototypeOf,
        s = t.slice,
        m = t.flat
            ? function (e) {
                  return t.flat.call(e);
              }
            : function (e) {
                  return t.concat.apply([], e);
              },
        u = t.push,
        i = t.indexOf,
        r = {},
        o = r.toString,
        v = r.hasOwnProperty,
        a = v.toString,
        l = a.call(Object),
        y = {},
        x = function (e) {
            return "function" == typeof e && "number" != typeof e.nodeType;
        },
        E = w.document,
        c = { type: !0, src: !0, nonce: !0, noModule: !0 };
    function b(e, t, n) {
        var r,
            i,
            o = (n = n || E).createElement("script");
        if (((o.text = e), t)) for (r in c) (i = t[r] || (t.getAttribute && t.getAttribute(r))) && o.setAttribute(r, i);
        n.head.appendChild(o).parentNode.removeChild(o);
    }
    function h(e) {
        return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? r[o.call(e)] || "object" : typeof e;
    }
    var f = "3.5.1",
        C = function (e, t) {
            return new C.fn.init(e, t);
        };
    function p(e) {
        var t = !!e && "length" in e && e.length,
            n = h(e);
        return !x(e) && !g(e) && ("array" === n || 0 === t || ("number" == typeof t && 0 < t && t - 1 in e));
    }
    (C.fn = C.prototype = {
        jquery: f,
        constructor: C,
        length: 0,
        toArray: function () {
            return s.call(this);
        },
        get: function (e) {
            return null == e ? s.call(this) : e < 0 ? this[e + this.length] : this[e];
        },
        pushStack: function (e) {
            e = C.merge(this.constructor(), e);
            return (e.prevObject = this), e;
        },
        each: function (e) {
            return C.each(this, e);
        },
        map: function (n) {
            return this.pushStack(
                C.map(this, function (e, t) {
                    return n.call(e, t, e);
                })
            );
        },
        slice: function () {
            return this.pushStack(s.apply(this, arguments));
        },
        first: function () {
            return this.eq(0);
        },
        last: function () {
            return this.eq(-1);
        },
        even: function () {
            return this.pushStack(
                C.grep(this, function (e, t) {
                    return (t + 1) % 2;
                })
            );
        },
        odd: function () {
            return this.pushStack(
                C.grep(this, function (e, t) {
                    return t % 2;
                })
            );
        },
        eq: function (e) {
            var t = this.length,
                e = +e + (e < 0 ? t : 0);
            return this.pushStack(0 <= e && e < t ? [this[e]] : []);
        },
        end: function () {
            return this.prevObject || this.constructor();
        },
        push: u,
        sort: t.sort,
        splice: t.splice,
    }),
        (C.extend = C.fn.extend = function () {
            var e,
                t,
                n,
                r,
                i,
                o = arguments[0] || {},
                a = 1,
                s = arguments.length,
                u = !1;
            for ("boolean" == typeof o && ((u = o), (o = arguments[a] || {}), a++), "object" == typeof o || x(o) || (o = {}), a === s && ((o = this), a--); a < s; a++)
                if (null != (e = arguments[a]))
                    for (t in e)
                        (n = e[t]),
                            "__proto__" !== t &&
                                o !== n &&
                                (u && n && (C.isPlainObject(n) || (r = Array.isArray(n)))
                                    ? ((i = o[t]), (i = r && !Array.isArray(i) ? [] : r || C.isPlainObject(i) ? i : {}), (r = !1), (o[t] = C.extend(u, i, n)))
                                    : void 0 !== n && (o[t] = n));
            return o;
        }),
        C.extend({
            expando: "djQ" + (f + Math.random()).replace(/\D/g, ""),
            isReady: !0,
            error: function (e) {
                throw new Error(e);
            },
            noop: function () {},
            isPlainObject: function (e) {
                return !(!e || "[object Object]" !== o.call(e)) && (!(e = n(e)) || ("function" == typeof (e = v.call(e, "constructor") && e.constructor) && a.call(e) === l));
            },
            isEmptyObject: function (e) {
                for (var t in e) return !1;
                return !0;
            },
            globalEval: function (e, t, n) {
                b(e, { nonce: t && t.nonce }, n);
            },
            each: function (e, t) {
                var n,
                    r = 0;
                if (p(e)) for (n = e.length; r < n && !1 !== t.call(e[r], r, e[r]); r++);
                else for (r in e) if (!1 === t.call(e[r], r, e[r])) break;
                return e;
            },
            makeArray: function (e, t) {
                t = t || [];
                return null != e && (p(Object(e)) ? C.merge(t, "string" == typeof e ? [e] : e) : u.call(t, e)), t;
            },
            inArray: function (e, t, n) {
                return null == t ? -1 : i.call(t, e, n);
            },
            merge: function (e, t) {
                for (var n = +t.length, r = 0, i = e.length; r < n; r++) e[i++] = t[r];
                return (e.length = i), e;
            },
            grep: function (e, t, n) {
                for (var r = [], i = 0, o = e.length, a = !n; i < o; i++) !t(e[i], i) != a && r.push(e[i]);
                return r;
            },
            map: function (e, t, n) {
                var r,
                    i,
                    o = 0,
                    a = [];
                if (p(e)) for (r = e.length; o < r; o++) null != (i = t(e[o], o, n)) && a.push(i);
                else for (o in e) null != (i = t(e[o], o, n)) && a.push(i);
                return m(a);
            },
            guid: 1,
            support: y,
        }),
        "function" == typeof Symbol && (C.fn[Symbol.iterator] = t[Symbol.iterator]),
        C.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
            r["[object " + t + "]"] = t.toLowerCase();
        });
    var d = (function (n) {
        function f(e, t) {
            return (e = "0x" + e.slice(1) - 65536), t || (e < 0 ? String.fromCharCode(65536 + e) : String.fromCharCode((e >> 10) | 55296, (1023 & e) | 56320));
        }
        function r() {
            w();
        }
        var e,
            d,
            b,
            o,
            i,
            h,
            p,
            g,
            T,
            u,
            l,
            w,
            E,
            a,
            C,
            m,
            s,
            c,
            v,
            S = "sizzle" + +new Date(),
            y = n.document,
            A = 0,
            x = 0,
            N = ue(),
            D = ue(),
            k = ue(),
            j = ue(),
            _ = function (e, t) {
                return e === t && (l = !0), 0;
            },
            q = {}.hasOwnProperty,
            t = [],
            L = t.pop,
            I = t.push,
            O = t.push,
            R = t.slice,
            H = function (e, t) {
                for (var n = 0, r = e.length; n < r; n++) if (e[n] === t) return n;
                return -1;
            },
            P = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            M = "[\\x20\\t\\r\\n\\f]",
            W = "(?:\\\\[\\da-fA-F]{1,6}" + M + "?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",
            F = "\\[" + M + "*(" + W + ")(?:" + M + "*([*^$|!~]?=)" + M + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + W + "))|)" + M + "*\\]",
            B = ":(" + W + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + F + ")*)|.*)\\)|)",
            $ = new RegExp(M + "+", "g"),
            U = new RegExp("^" + M + "+|((?:^|[^\\\\])(?:\\\\.)*)" + M + "+$", "g"),
            V = new RegExp("^" + M + "*," + M + "*"),
            G = new RegExp("^" + M + "*([>+~]|" + M + ")" + M + "*"),
            z = new RegExp(M + "|>"),
            X = new RegExp(B),
            J = new RegExp("^" + W + "$"),
            Q = {
                ID: new RegExp("^#(" + W + ")"),
                CLASS: new RegExp("^\\.(" + W + ")"),
                TAG: new RegExp("^(" + W + "|[*])"),
                ATTR: new RegExp("^" + F),
                PSEUDO: new RegExp("^" + B),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + M + "*(even|odd|(([+-]|)(\\d*)n|)" + M + "*(?:([+-]|)" + M + "*(\\d+)|))" + M + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + P + ")$", "i"),
                needsContext: new RegExp("^" + M + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + M + "*((?:-\\d)?\\d*)" + M + "*\\)|)(?=[^-]|$)", "i"),
            },
            Y = /HTML$/i,
            K = /^(?:input|select|textarea|button)$/i,
            Z = /^h\d$/i,
            ee = /^[^{]+\{\s*\[native \w/,
            te = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            ne = /[+~]/,
            re = new RegExp("\\\\[\\da-fA-F]{1,6}" + M + "?|\\\\([^\\r\\n\\f])", "g"),
            ie = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            oe = function (e, t) {
                return t ? ("\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " ") : "\\" + e;
            },
            ae = ye(
                function (e) {
                    return !0 === e.disabled && "fieldset" === e.nodeName.toLowerCase();
                },
                { dir: "parentNode", next: "legend" }
            );
        try {
            O.apply((t = R.call(y.childNodes)), y.childNodes), t[y.childNodes.length].nodeType;
        } catch (e) {
            O = {
                apply: t.length
                    ? function (e, t) {
                          I.apply(e, R.call(t));
                      }
                    : function (e, t) {
                          for (var n = e.length, r = 0; (e[n++] = t[r++]); );
                          e.length = n - 1;
                      },
            };
        }
        function se(t, e, n, r) {
            var i,
                o,
                a,
                s,
                u,
                l,
                c,
                f = e && e.ownerDocument,
                p = e ? e.nodeType : 9;
            if (((n = n || []), "string" != typeof t || !t || (1 !== p && 9 !== p && 11 !== p))) return n;
            if (!r && (w(e), (e = e || E), C)) {
                if (11 !== p && (u = te.exec(t)))
                    if ((i = u[1])) {
                        if (9 === p) {
                            if (!(a = e.getElementById(i))) return n;
                            if (a.id === i) return n.push(a), n;
                        } else if (f && (a = f.getElementById(i)) && v(e, a) && a.id === i) return n.push(a), n;
                    } else {
                        if (u[2]) return O.apply(n, e.getElementsByTagName(t)), n;
                        if ((i = u[3]) && d.getElementsByClassName && e.getElementsByClassName) return O.apply(n, e.getElementsByClassName(i)), n;
                    }
                if (d.qsa && !j[t + " "] && (!m || !m.test(t)) && (1 !== p || "object" !== e.nodeName.toLowerCase())) {
                    if (((c = t), (f = e), 1 === p && (z.test(t) || G.test(t)))) {
                        for (((f = (ne.test(t) && ge(e.parentNode)) || e) === e && d.scope) || ((s = e.getAttribute("id")) ? (s = s.replace(ie, oe)) : e.setAttribute("id", (s = S))), o = (l = h(t)).length; o--; )
                            l[o] = (s ? "#" + s : ":scope") + " " + ve(l[o]);
                        c = l.join(",");
                    }
                    try {
                        return O.apply(n, f.querySelectorAll(c)), n;
                    } catch (e) {
                        j(t, !0);
                    } finally {
                        s === S && e.removeAttribute("id");
                    }
                }
            }
            return g(t.replace(U, "$1"), e, n, r);
        }
        function ue() {
            var n = [];
            function r(e, t) {
                return n.push(e + " ") > b.cacheLength && delete r[n.shift()], (r[e + " "] = t);
            }
            return r;
        }
        function le(e) {
            return (e[S] = !0), e;
        }
        function ce(e) {
            var t = E.createElement("fieldset");
            try {
                return !!e(t);
            } catch (e) {
                return !1;
            } finally {
                t.parentNode && t.parentNode.removeChild(t), (t = null);
            }
        }
        function fe(e, t) {
            for (var n = e.split("|"), r = n.length; r--; ) b.attrHandle[n[r]] = t;
        }
        function pe(e, t) {
            var n = t && e,
                r = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (r) return r;
            if (n) for (; (n = n.nextSibling); ) if (n === t) return -1;
            return e ? 1 : -1;
        }
        function de(t) {
            return function (e) {
                return "form" in e
                    ? e.parentNode && !1 === e.disabled
                        ? "label" in e
                            ? "label" in e.parentNode
                                ? e.parentNode.disabled === t
                                : e.disabled === t
                            : e.isDisabled === t || (e.isDisabled !== !t && ae(e) === t)
                        : e.disabled === t
                    : "label" in e && e.disabled === t;
            };
        }
        function he(a) {
            return le(function (o) {
                return (
                    (o = +o),
                    le(function (e, t) {
                        for (var n, r = a([], e.length, o), i = r.length; i--; ) e[(n = r[i])] && (e[n] = !(t[n] = e[n]));
                    })
                );
            });
        }
        function ge(e) {
            return e && void 0 !== e.getElementsByTagName && e;
        }
        for (e in ((d = se.support = {}),
        (i = se.isXML = function (e) {
            var t = e.namespaceURI,
                e = (e.ownerDocument || e).documentElement;
            return !Y.test(t || (e && e.nodeName) || "HTML");
        }),
        (w = se.setDocument = function (e) {
            var t,
                e = e ? e.ownerDocument || e : y;
            return (
                e != E &&
                    9 === e.nodeType &&
                    e.documentElement &&
                    ((a = (E = e).documentElement),
                    (C = !i(E)),
                    y != E && (t = E.defaultView) && t.top !== t && (t.addEventListener ? t.addEventListener("unload", r, !1) : t.attachEvent && t.attachEvent("onunload", r)),
                    (d.scope = ce(function (e) {
                        return a.appendChild(e).appendChild(E.createElement("div")), void 0 !== e.querySelectorAll && !e.querySelectorAll(":scope fieldset div").length;
                    })),
                    (d.attributes = ce(function (e) {
                        return (e.className = "i"), !e.getAttribute("className");
                    })),
                    (d.getElementsByTagName = ce(function (e) {
                        return e.appendChild(E.createComment("")), !e.getElementsByTagName("*").length;
                    })),
                    (d.getElementsByClassName = ee.test(E.getElementsByClassName)),
                    (d.getById = ce(function (e) {
                        return (a.appendChild(e).id = S), !E.getElementsByName || !E.getElementsByName(S).length;
                    })),
                    d.getById
                        ? ((b.filter.ID = function (e) {
                              var t = e.replace(re, f);
                              return function (e) {
                                  return e.getAttribute("id") === t;
                              };
                          }),
                          (b.find.ID = function (e, t) {
                              if (void 0 !== t.getElementById && C) {
                                  e = t.getElementById(e);
                                  return e ? [e] : [];
                              }
                          }))
                        : ((b.filter.ID = function (e) {
                              var t = e.replace(re, f);
                              return function (e) {
                                  e = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                                  return e && e.value === t;
                              };
                          }),
                          (b.find.ID = function (e, t) {
                              if (void 0 !== t.getElementById && C) {
                                  var n,
                                      r,
                                      i,
                                      o = t.getElementById(e);
                                  if (o) {
                                      if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
                                      for (i = t.getElementsByName(e), r = 0; (o = i[r++]); ) if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
                                  }
                                  return [];
                              }
                          })),
                    (b.find.TAG = d.getElementsByTagName
                        ? function (e, t) {
                              return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : d.qsa ? t.querySelectorAll(e) : void 0;
                          }
                        : function (e, t) {
                              var n,
                                  r = [],
                                  i = 0,
                                  o = t.getElementsByTagName(e);
                              if ("*" !== e) return o;
                              for (; (n = o[i++]); ) 1 === n.nodeType && r.push(n);
                              return r;
                          }),
                    (b.find.CLASS =
                        d.getElementsByClassName &&
                        function (e, t) {
                            if (void 0 !== t.getElementsByClassName && C) return t.getElementsByClassName(e);
                        }),
                    (s = []),
                    (m = []),
                    (d.qsa = ee.test(E.querySelectorAll)) &&
                        (ce(function (e) {
                            var t;
                            (a.appendChild(e).innerHTML = "<a id='" + S + "'></a><select id='" + S + "-\r\\' msallowcapture=''><option selected=''></option></select>"),
                                e.querySelectorAll("[msallowcapture^='']").length && m.push("[*^$]=" + M + "*(?:''|\"\")"),
                                e.querySelectorAll("[selected]").length || m.push("\\[" + M + "*(?:value|" + P + ")"),
                                e.querySelectorAll("[id~=" + S + "-]").length || m.push("~="),
                                (t = E.createElement("input")).setAttribute("name", ""),
                                e.appendChild(t),
                                e.querySelectorAll("[name='']").length || m.push("\\[" + M + "*name" + M + "*=" + M + "*(?:''|\"\")"),
                                e.querySelectorAll(":checked").length || m.push(":checked"),
                                e.querySelectorAll("a#" + S + "+*").length || m.push(".#.+[+~]"),
                                e.querySelectorAll("\\\f"),
                                m.push("[\\r\\n\\f]");
                        }),
                        ce(function (e) {
                            e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                            var t = E.createElement("input");
                            t.setAttribute("type", "hidden"),
                                e.appendChild(t).setAttribute("name", "D"),
                                e.querySelectorAll("[name=d]").length && m.push("name" + M + "*[*^$|!~]?="),
                                2 !== e.querySelectorAll(":enabled").length && m.push(":enabled", ":disabled"),
                                (a.appendChild(e).disabled = !0),
                                2 !== e.querySelectorAll(":disabled").length && m.push(":enabled", ":disabled"),
                                e.querySelectorAll("*,:x"),
                                m.push(",.*:");
                        })),
                    (d.matchesSelector = ee.test((c = a.matches || a.webkitMatchesSelector || a.mozMatchesSelector || a.oMatchesSelector || a.msMatchesSelector))) &&
                        ce(function (e) {
                            (d.disconnectedMatch = c.call(e, "*")), c.call(e, "[s!='']:x"), s.push("!=", B);
                        }),
                    (m = m.length && new RegExp(m.join("|"))),
                    (s = s.length && new RegExp(s.join("|"))),
                    (t = ee.test(a.compareDocumentPosition)),
                    (v =
                        t || ee.test(a.contains)
                            ? function (e, t) {
                                  var n = 9 === e.nodeType ? e.documentElement : e,
                                      t = t && t.parentNode;
                                  return e === t || !(!t || 1 !== t.nodeType || !(n.contains ? n.contains(t) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(t)));
                              }
                            : function (e, t) {
                                  if (t) for (; (t = t.parentNode); ) if (t === e) return !0;
                                  return !1;
                              }),
                    (_ = t
                        ? function (e, t) {
                              if (e === t) return (l = !0), 0;
                              var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                              return (
                                  n ||
                                  (1 & (n = (e.ownerDocument || e) == (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || (!d.sortDetached && t.compareDocumentPosition(e) === n)
                                      ? e == E || (e.ownerDocument == y && v(y, e))
                                          ? -1
                                          : t == E || (t.ownerDocument == y && v(y, t))
                                          ? 1
                                          : u
                                          ? H(u, e) - H(u, t)
                                          : 0
                                      : 4 & n
                                      ? -1
                                      : 1)
                              );
                          }
                        : function (e, t) {
                              if (e === t) return (l = !0), 0;
                              var n,
                                  r = 0,
                                  i = e.parentNode,
                                  o = t.parentNode,
                                  a = [e],
                                  s = [t];
                              if (!i || !o) return e == E ? -1 : t == E ? 1 : i ? -1 : o ? 1 : u ? H(u, e) - H(u, t) : 0;
                              if (i === o) return pe(e, t);
                              for (n = e; (n = n.parentNode); ) a.unshift(n);
                              for (n = t; (n = n.parentNode); ) s.unshift(n);
                              for (; a[r] === s[r]; ) r++;
                              return r ? pe(a[r], s[r]) : a[r] == y ? -1 : s[r] == y ? 1 : 0;
                          })),
                E
            );
        }),
        (se.matches = function (e, t) {
            return se(e, null, null, t);
        }),
        (se.matchesSelector = function (e, t) {
            if ((w(e), d.matchesSelector && C && !j[t + " "] && (!s || !s.test(t)) && (!m || !m.test(t))))
                try {
                    var n = c.call(e, t);
                    if (n || d.disconnectedMatch || (e.document && 11 !== e.document.nodeType)) return n;
                } catch (e) {
                    j(t, !0);
                }
            return 0 < se(t, E, null, [e]).length;
        }),
        (se.contains = function (e, t) {
            return (e.ownerDocument || e) != E && w(e), v(e, t);
        }),
        (se.attr = function (e, t) {
            (e.ownerDocument || e) != E && w(e);
            var n = b.attrHandle[t.toLowerCase()],
                n = n && q.call(b.attrHandle, t.toLowerCase()) ? n(e, t, !C) : void 0;
            return void 0 !== n ? n : d.attributes || !C ? e.getAttribute(t) : (n = e.getAttributeNode(t)) && n.specified ? n.value : null;
        }),
        (se.escape = function (e) {
            return (e + "").replace(ie, oe);
        }),
        (se.error = function (e) {
            throw new Error("Syntax error, unrecognized expression: " + e);
        }),
        (se.uniqueSort = function (e) {
            var t,
                n = [],
                r = 0,
                i = 0;
            if (((l = !d.detectDuplicates), (u = !d.sortStable && e.slice(0)), e.sort(_), l)) {
                for (; (t = e[i++]); ) t === e[i] && (r = n.push(i));
                for (; r--; ) e.splice(n[r], 1);
            }
            return (u = null), e;
        }),
        (o = se.getText = function (e) {
            var t,
                n = "",
                r = 0,
                i = e.nodeType;
            if (i) {
                if (1 === i || 9 === i || 11 === i) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += o(e);
                } else if (3 === i || 4 === i) return e.nodeValue;
            } else for (; (t = e[r++]); ) n += o(t);
            return n;
        }),
        ((b = se.selectors = {
            cacheLength: 50,
            createPseudo: le,
            match: Q,
            attrHandle: {},
            find: {},
            relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } },
            preFilter: {
                ATTR: function (e) {
                    return (e[1] = e[1].replace(re, f)), (e[3] = (e[3] || e[4] || e[5] || "").replace(re, f)), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4);
                },
                CHILD: function (e) {
                    return (
                        (e[1] = e[1].toLowerCase()),
                        "nth" === e[1].slice(0, 3) ? (e[3] || se.error(e[0]), (e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3]))), (e[5] = +(e[7] + e[8] || "odd" === e[3]))) : e[3] && se.error(e[0]),
                        e
                    );
                },
                PSEUDO: function (e) {
                    var t,
                        n = !e[6] && e[2];
                    return Q.CHILD.test(e[0])
                        ? null
                        : (e[3] ? (e[2] = e[4] || e[5] || "") : n && X.test(n) && (t = h(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && ((e[0] = e[0].slice(0, t)), (e[2] = n.slice(0, t))), e.slice(0, 3));
                },
            },
            filter: {
                TAG: function (e) {
                    var t = e.replace(re, f).toLowerCase();
                    return "*" === e
                        ? function () {
                              return !0;
                          }
                        : function (e) {
                              return e.nodeName && e.nodeName.toLowerCase() === t;
                          };
                },
                CLASS: function (e) {
                    var t = N[e + " "];
                    return (
                        t ||
                        ((t = new RegExp("(^|" + M + ")" + e + "(" + M + "|$)")) &&
                            N(e, function (e) {
                                return t.test(("string" == typeof e.className && e.className) || (void 0 !== e.getAttribute && e.getAttribute("class")) || "");
                            }))
                    );
                },
                ATTR: function (t, n, r) {
                    return function (e) {
                        e = se.attr(e, t);
                        return null == e
                            ? "!=" === n
                            : !n ||
                                  ((e += ""),
                                  "=" === n
                                      ? e === r
                                      : "!=" === n
                                      ? e !== r
                                      : "^=" === n
                                      ? r && 0 === e.indexOf(r)
                                      : "*=" === n
                                      ? r && -1 < e.indexOf(r)
                                      : "$=" === n
                                      ? r && e.slice(-r.length) === r
                                      : "~=" === n
                                      ? -1 < (" " + e.replace($, " ") + " ").indexOf(r)
                                      : "|=" === n && (e === r || e.slice(0, r.length + 1) === r + "-"));
                    };
                },
                CHILD: function (h, e, t, g, m) {
                    var v = "nth" !== h.slice(0, 3),
                        y = "last" !== h.slice(-4),
                        x = "of-type" === e;
                    return 1 === g && 0 === m
                        ? function (e) {
                              return !!e.parentNode;
                          }
                        : function (e, t, n) {
                              var r,
                                  i,
                                  o,
                                  a,
                                  s,
                                  u,
                                  l = v != y ? "nextSibling" : "previousSibling",
                                  c = e.parentNode,
                                  f = x && e.nodeName.toLowerCase(),
                                  p = !n && !x,
                                  d = !1;
                              if (c) {
                                  if (v) {
                                      for (; l; ) {
                                          for (a = e; (a = a[l]); ) if (x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) return !1;
                                          u = l = "only" === h && !u && "nextSibling";
                                      }
                                      return !0;
                                  }
                                  if (((u = [y ? c.firstChild : c.lastChild]), y && p)) {
                                      for (
                                          d = (s = (r = (i = (o = (a = c)[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === A && r[1]) && r[2], a = s && c.childNodes[s];
                                          (a = (++s && a && a[l]) || (d = s = 0) || u.pop());

                                      )
                                          if (1 === a.nodeType && ++d && a === e) {
                                              i[h] = [A, s, d];
                                              break;
                                          }
                                  } else if (!1 === (d = p ? (s = (r = (i = (o = (a = e)[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === A && r[1]) : d))
                                      for (
                                          ;
                                          (a = (++s && a && a[l]) || (d = s = 0) || u.pop()) &&
                                          ((x ? a.nodeName.toLowerCase() !== f : 1 !== a.nodeType) || !++d || (p && ((i = (o = a[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] = [A, d]), a !== e));

                                      );
                                  return (d -= m) === g || (d % g == 0 && 0 <= d / g);
                              }
                          };
                },
                PSEUDO: function (e, o) {
                    var t,
                        a = b.pseudos[e] || b.setFilters[e.toLowerCase()] || se.error("unsupported pseudo: " + e);
                    return a[S]
                        ? a(o)
                        : 1 < a.length
                        ? ((t = [e, e, "", o]),
                          b.setFilters.hasOwnProperty(e.toLowerCase())
                              ? le(function (e, t) {
                                    for (var n, r = a(e, o), i = r.length; i--; ) e[(n = H(e, r[i]))] = !(t[n] = r[i]);
                                })
                              : function (e) {
                                    return a(e, 0, t);
                                })
                        : a;
                },
            },
            pseudos: {
                not: le(function (e) {
                    var r = [],
                        i = [],
                        s = p(e.replace(U, "$1"));
                    return s[S]
                        ? le(function (e, t, n, r) {
                              for (var i, o = s(e, null, r, []), a = e.length; a--; ) (i = o[a]) && (e[a] = !(t[a] = i));
                          })
                        : function (e, t, n) {
                              return (r[0] = e), s(r, null, n, i), (r[0] = null), !i.pop();
                          };
                }),
                has: le(function (t) {
                    return function (e) {
                        return 0 < se(t, e).length;
                    };
                }),
                contains: le(function (t) {
                    return (
                        (t = t.replace(re, f)),
                        function (e) {
                            return -1 < (e.textContent || o(e)).indexOf(t);
                        }
                    );
                }),
                lang: le(function (n) {
                    return (
                        J.test(n || "") || se.error("unsupported lang: " + n),
                        (n = n.replace(re, f).toLowerCase()),
                        function (e) {
                            var t;
                            do {
                                if ((t = C ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang"))) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-");
                            } while ((e = e.parentNode) && 1 === e.nodeType);
                            return !1;
                        }
                    );
                }),
                target: function (e) {
                    var t = n.location && n.location.hash;
                    return t && t.slice(1) === e.id;
                },
                root: function (e) {
                    return e === a;
                },
                focus: function (e) {
                    return e === E.activeElement && (!E.hasFocus || E.hasFocus()) && !!(e.type || e.href || ~e.tabIndex);
                },
                enabled: de(!1),
                disabled: de(!0),
                checked: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return ("input" === t && !!e.checked) || ("option" === t && !!e.selected);
                },
                selected: function (e) {
                    return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected;
                },
                empty: function (e) {
                    for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
                    return !0;
                },
                parent: function (e) {
                    return !b.pseudos.empty(e);
                },
                header: function (e) {
                    return Z.test(e.nodeName);
                },
                input: function (e) {
                    return K.test(e.nodeName);
                },
                button: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return ("input" === t && "button" === e.type) || "button" === t;
                },
                text: function (e) {
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (e = e.getAttribute("type")) || "text" === e.toLowerCase());
                },
                first: he(function () {
                    return [0];
                }),
                last: he(function (e, t) {
                    return [t - 1];
                }),
                eq: he(function (e, t, n) {
                    return [n < 0 ? n + t : n];
                }),
                even: he(function (e, t) {
                    for (var n = 0; n < t; n += 2) e.push(n);
                    return e;
                }),
                odd: he(function (e, t) {
                    for (var n = 1; n < t; n += 2) e.push(n);
                    return e;
                }),
                lt: he(function (e, t, n) {
                    for (var r = n < 0 ? n + t : t < n ? t : n; 0 <= --r; ) e.push(r);
                    return e;
                }),
                gt: he(function (e, t, n) {
                    for (var r = n < 0 ? n + t : n; ++r < t; ) e.push(r);
                    return e;
                }),
            },
        }).pseudos.nth = b.pseudos.eq),
        { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }))
            b.pseudos[e] = (function (t) {
                return function (e) {
                    return "input" === e.nodeName.toLowerCase() && e.type === t;
                };
            })(e);
        for (e in { submit: !0, reset: !0 })
            b.pseudos[e] = (function (n) {
                return function (e) {
                    var t = e.nodeName.toLowerCase();
                    return ("input" === t || "button" === t) && e.type === n;
                };
            })(e);
        function me() {}
        function ve(e) {
            for (var t = 0, n = e.length, r = ""; t < n; t++) r += e[t].value;
            return r;
        }
        function ye(a, e, t) {
            var s = e.dir,
                u = e.next,
                l = u || s,
                c = t && "parentNode" === l,
                f = x++;
            return e.first
                ? function (e, t, n) {
                      for (; (e = e[s]); ) if (1 === e.nodeType || c) return a(e, t, n);
                      return !1;
                  }
                : function (e, t, n) {
                      var r,
                          i,
                          o = [A, f];
                      if (n) {
                          for (; (e = e[s]); ) if ((1 === e.nodeType || c) && a(e, t, n)) return !0;
                      } else
                          for (; (e = e[s]); )
                              if (1 === e.nodeType || c)
                                  if (((r = (i = e[S] || (e[S] = {}))[e.uniqueID] || (i[e.uniqueID] = {})), u && u === e.nodeName.toLowerCase())) e = e[s] || e;
                                  else {
                                      if ((i = r[l]) && i[0] === A && i[1] === f) return (o[2] = i[2]);
                                      if (((r[l] = o)[2] = a(e, t, n))) return !0;
                                  }
                      return !1;
                  };
        }
        function xe(i) {
            return 1 < i.length
                ? function (e, t, n) {
                      for (var r = i.length; r--; ) if (!i[r](e, t, n)) return !1;
                      return !0;
                  }
                : i[0];
        }
        function be(e, t, n, r, i) {
            for (var o, a = [], s = 0, u = e.length, l = null != t; s < u; s++) (o = e[s]) && ((n && !n(o, r, i)) || (a.push(o), l && t.push(s)));
            return a;
        }
        function Te(d, h, g, m, v, e) {
            return (
                m && !m[S] && (m = Te(m)),
                v && !v[S] && (v = Te(v, e)),
                le(function (e, t, n, r) {
                    var i,
                        o,
                        a,
                        s = [],
                        u = [],
                        l = t.length,
                        c =
                            e ||
                            (function (e, t, n) {
                                for (var r = 0, i = t.length; r < i; r++) se(e, t[r], n);
                                return n;
                            })(h || "*", n.nodeType ? [n] : n, []),
                        f = !d || (!e && h) ? c : be(c, s, d, n, r),
                        p = g ? (v || (e ? d : l || m) ? [] : t) : f;
                    if ((g && g(f, p, n, r), m)) for (i = be(p, u), m(i, [], n, r), o = i.length; o--; ) (a = i[o]) && (p[u[o]] = !(f[u[o]] = a));
                    if (e) {
                        if (v || d) {
                            if (v) {
                                for (i = [], o = p.length; o--; ) (a = p[o]) && i.push((f[o] = a));
                                v(null, (p = []), i, r);
                            }
                            for (o = p.length; o--; ) (a = p[o]) && -1 < (i = v ? H(e, a) : s[o]) && (e[i] = !(t[i] = a));
                        }
                    } else (p = be(p === t ? p.splice(l, p.length) : p)), v ? v(null, t, p, r) : O.apply(t, p);
                })
            );
        }
        function we(m, v) {
            function e(e, t, n, r, i) {
                var o,
                    a,
                    s,
                    u = 0,
                    l = "0",
                    c = e && [],
                    f = [],
                    p = T,
                    d = e || (x && b.find.TAG("*", i)),
                    h = (A += null == p ? 1 : Math.random() || 0.1),
                    g = d.length;
                for (i && (T = t == E || t || i); l !== g && null != (o = d[l]); l++) {
                    if (x && o) {
                        for (a = 0, t || o.ownerDocument == E || (w(o), (n = !C)); (s = m[a++]); )
                            if (s(o, t || E, n)) {
                                r.push(o);
                                break;
                            }
                        i && (A = h);
                    }
                    y && ((o = !s && o) && u--, e && c.push(o));
                }
                if (((u += l), y && l !== u)) {
                    for (a = 0; (s = v[a++]); ) s(c, f, t, n);
                    if (e) {
                        if (0 < u) for (; l--; ) c[l] || f[l] || (f[l] = L.call(r));
                        f = be(f);
                    }
                    O.apply(r, f), i && !e && 0 < f.length && 1 < u + v.length && se.uniqueSort(r);
                }
                return i && ((A = h), (T = p)), c;
            }
            var y = 0 < v.length,
                x = 0 < m.length;
            return y ? le(e) : e;
        }
        return (
            (me.prototype = b.filters = b.pseudos),
            (b.setFilters = new me()),
            (h = se.tokenize = function (e, t) {
                var n,
                    r,
                    i,
                    o,
                    a,
                    s,
                    u,
                    l = D[e + " "];
                if (l) return t ? 0 : l.slice(0);
                for (a = e, s = [], u = b.preFilter; a; ) {
                    for (o in ((n && !(r = V.exec(a))) || (r && (a = a.slice(r[0].length) || a), s.push((i = []))),
                    (n = !1),
                    (r = G.exec(a)) && ((n = r.shift()), i.push({ value: n, type: r[0].replace(U, " ") }), (a = a.slice(n.length))),
                    b.filter))
                        !(r = Q[o].exec(a)) || (u[o] && !(r = u[o](r))) || ((n = r.shift()), i.push({ value: n, type: o, matches: r }), (a = a.slice(n.length)));
                    if (!n) break;
                }
                return t ? a.length : a ? se.error(e) : D(e, s).slice(0);
            }),
            (p = se.compile = function (e, t) {
                var n,
                    r = [],
                    i = [],
                    o = k[e + " "];
                if (!o) {
                    for (n = (t = t || h(e)).length; n--; )
                        ((o = (function e(t) {
                            for (
                                var r,
                                    n,
                                    i,
                                    o = t.length,
                                    a = b.relative[t[0].type],
                                    s = a || b.relative[" "],
                                    u = a ? 1 : 0,
                                    l = ye(
                                        function (e) {
                                            return e === r;
                                        },
                                        s,
                                        !0
                                    ),
                                    c = ye(
                                        function (e) {
                                            return -1 < H(r, e);
                                        },
                                        s,
                                        !0
                                    ),
                                    f = [
                                        function (e, t, n) {
                                            return (n = (!a && (n || t !== T)) || ((r = t).nodeType ? l : c)(e, t, n)), (r = null), n;
                                        },
                                    ];
                                u < o;
                                u++
                            )
                                if ((n = b.relative[t[u].type])) f = [ye(xe(f), n)];
                                else {
                                    if ((n = b.filter[t[u].type].apply(null, t[u].matches))[S]) {
                                        for (i = ++u; i < o && !b.relative[t[i].type]; i++);
                                        return Te(
                                            1 < u && xe(f),
                                            1 < u && ve(t.slice(0, u - 1).concat({ value: " " === t[u - 2].type ? "*" : "" })).replace(U, "$1"),
                                            n,
                                            u < i && e(t.slice(u, i)),
                                            i < o && e((t = t.slice(i))),
                                            i < o && ve(t)
                                        );
                                    }
                                    f.push(n);
                                }
                            return xe(f);
                        })(t[n]))[S]
                            ? r
                            : i
                        ).push(o);
                    (o = k(e, we(i, r))).selector = e;
                }
                return o;
            }),
            (g = se.select = function (e, t, n, r) {
                var i,
                    o,
                    a,
                    s,
                    u,
                    l = "function" == typeof e && e,
                    c = !r && h((e = l.selector || e));
                if (((n = n || []), 1 === c.length)) {
                    if (2 < (o = c[0] = c[0].slice(0)).length && "ID" === (a = o[0]).type && 9 === t.nodeType && C && b.relative[o[1].type]) {
                        if (!(t = (b.find.ID(a.matches[0].replace(re, f), t) || [])[0])) return n;
                        l && (t = t.parentNode), (e = e.slice(o.shift().value.length));
                    }
                    for (i = Q.needsContext.test(e) ? 0 : o.length; i-- && ((a = o[i]), !b.relative[(s = a.type)]); )
                        if ((u = b.find[s]) && (r = u(a.matches[0].replace(re, f), (ne.test(o[0].type) && ge(t.parentNode)) || t))) {
                            if ((o.splice(i, 1), !(e = r.length && ve(o)))) return O.apply(n, r), n;
                            break;
                        }
                }
                return (l || p(e, c))(r, t, !C, n, !t || (ne.test(e) && ge(t.parentNode)) || t), n;
            }),
            (d.sortStable = S.split("").sort(_).join("") === S),
            (d.detectDuplicates = !!l),
            w(),
            (d.sortDetached = ce(function (e) {
                return 1 & e.compareDocumentPosition(E.createElement("fieldset"));
            })),
            ce(function (e) {
                return (e.innerHTML = "<a href='#'></a>"), "#" === e.firstChild.getAttribute("href");
            }) ||
                fe("type|href|height|width", function (e, t, n) {
                    if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2);
                }),
            (d.attributes &&
                ce(function (e) {
                    return (e.innerHTML = "<input/>"), e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value");
                })) ||
                fe("value", function (e, t, n) {
                    if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue;
                }),
            ce(function (e) {
                return null == e.getAttribute("disabled");
            }) ||
                fe(P, function (e, t, n) {
                    if (!n) return !0 === e[t] ? t.toLowerCase() : (t = e.getAttributeNode(t)) && t.specified ? t.value : null;
                }),
            se
        );
    })(w);
    (C.find = d), (C.expr = d.selectors), (C.expr[":"] = C.expr.pseudos), (C.uniqueSort = C.unique = d.uniqueSort), (C.text = d.getText), (C.isXMLDoc = d.isXML), (C.contains = d.contains), (C.escapeSelector = d.escape);
    function T(e, t, n) {
        for (var r = [], i = void 0 !== n; (e = e[t]) && 9 !== e.nodeType; )
            if (1 === e.nodeType) {
                if (i && C(e).is(n)) break;
                r.push(e);
            }
        return r;
    }
    function S(e, t) {
        for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
        return n;
    }
    var A = C.expr.match.needsContext;
    function N(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
    }
    var D = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;
    function k(e, n, r) {
        return x(n)
            ? C.grep(e, function (e, t) {
                  return !!n.call(e, t, e) !== r;
              })
            : n.nodeType
            ? C.grep(e, function (e) {
                  return (e === n) !== r;
              })
            : "string" != typeof n
            ? C.grep(e, function (e) {
                  return -1 < i.call(n, e) !== r;
              })
            : C.filter(n, e, r);
    }
    (C.filter = function (e, t, n) {
        var r = t[0];
        return (
            n && (e = ":not(" + e + ")"),
            1 === t.length && 1 === r.nodeType
                ? C.find.matchesSelector(r, e)
                    ? [r]
                    : []
                : C.find.matches(
                      e,
                      C.grep(t, function (e) {
                          return 1 === e.nodeType;
                      })
                  )
        );
    }),
        C.fn.extend({
            find: function (e) {
                var t,
                    n,
                    r = this.length,
                    i = this;
                if ("string" != typeof e)
                    return this.pushStack(
                        C(e).filter(function () {
                            for (t = 0; t < r; t++) if (C.contains(i[t], this)) return !0;
                        })
                    );
                for (n = this.pushStack([]), t = 0; t < r; t++) C.find(e, i[t], n);
                return 1 < r ? C.uniqueSort(n) : n;
            },
            filter: function (e) {
                return this.pushStack(k(this, e || [], !1));
            },
            not: function (e) {
                return this.pushStack(k(this, e || [], !0));
            },
            is: function (e) {
                return !!k(this, "string" == typeof e && A.test(e) ? C(e) : e || [], !1).length;
            },
        });
    var j = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (C.fn.init = function (e, t, n) {
        if (!e) return this;
        if (((n = n || _), "string" != typeof e)) return e.nodeType ? ((this[0] = e), (this.length = 1), this) : x(e) ? (void 0 !== n.ready ? n.ready(e) : e(C)) : C.makeArray(e, this);
        if (!(r = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : j.exec(e)) || (!r[1] && t)) return (!t || t.jquery ? t || n : this.constructor(t)).find(e);
        if (r[1]) {
            if (((t = t instanceof C ? t[0] : t), C.merge(this, C.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : E, !0)), D.test(r[1]) && C.isPlainObject(t))) for (var r in t) x(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
            return this;
        }
        return (e = E.getElementById(r[2])) && ((this[0] = e), (this.length = 1)), this;
    }).prototype = C.fn;
    var _ = C(E),
        q = /^(?:parents|prev(?:Until|All))/,
        L = { children: !0, contents: !0, next: !0, prev: !0 };
    function I(e, t) {
        for (; (e = e[t]) && 1 !== e.nodeType; );
        return e;
    }
    C.fn.extend({
        has: function (e) {
            var t = C(e, this),
                n = t.length;
            return this.filter(function () {
                for (var e = 0; e < n; e++) if (C.contains(this, t[e])) return !0;
            });
        },
        closest: function (e, t) {
            var n,
                r = 0,
                i = this.length,
                o = [],
                a = "string" != typeof e && C(e);
            if (!A.test(e))
                for (; r < i; r++)
                    for (n = this[r]; n && n !== t; n = n.parentNode)
                        if (n.nodeType < 11 && (a ? -1 < a.index(n) : 1 === n.nodeType && C.find.matchesSelector(n, e))) {
                            o.push(n);
                            break;
                        }
            return this.pushStack(1 < o.length ? C.uniqueSort(o) : o);
        },
        index: function (e) {
            return e ? ("string" == typeof e ? i.call(C(e), this[0]) : i.call(this, e.jquery ? e[0] : e)) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
        },
        add: function (e, t) {
            return this.pushStack(C.uniqueSort(C.merge(this.get(), C(e, t))));
        },
        addBack: function (e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e));
        },
    }),
        C.each(
            {
                parent: function (e) {
                    e = e.parentNode;
                    return e && 11 !== e.nodeType ? e : null;
                },
                parents: function (e) {
                    return T(e, "parentNode");
                },
                parentsUntil: function (e, t, n) {
                    return T(e, "parentNode", n);
                },
                next: function (e) {
                    return I(e, "nextSibling");
                },
                prev: function (e) {
                    return I(e, "previousSibling");
                },
                nextAll: function (e) {
                    return T(e, "nextSibling");
                },
                prevAll: function (e) {
                    return T(e, "previousSibling");
                },
                nextUntil: function (e, t, n) {
                    return T(e, "nextSibling", n);
                },
                prevUntil: function (e, t, n) {
                    return T(e, "previousSibling", n);
                },
                siblings: function (e) {
                    return S((e.parentNode || {}).firstChild, e);
                },
                children: function (e) {
                    return S(e.firstChild);
                },
                contents: function (e) {
                    return null != e.contentDocument && n(e.contentDocument) ? e.contentDocument : (N(e, "template") && (e = e.content || e), C.merge([], e.childNodes));
                },
            },
            function (r, i) {
                C.fn[r] = function (e, t) {
                    var n = C.map(this, i, e);
                    return (t = "Until" !== r.slice(-5) ? e : t) && "string" == typeof t && (n = C.filter(t, n)), 1 < this.length && (L[r] || C.uniqueSort(n), q.test(r) && n.reverse()), this.pushStack(n);
                };
            }
        );
    var O = /[^\x20\t\r\n\f]+/g;
    function R(e) {
        return e;
    }
    function H(e) {
        throw e;
    }
    function P(e, t, n, r) {
        var i;
        try {
            e && x((i = e.promise)) ? i.call(e).done(t).fail(n) : e && x((i = e.then)) ? i.call(e, t, n) : t.apply(void 0, [e].slice(r));
        } catch (e) {
            n.apply(void 0, [e]);
        }
    }
    (C.Callbacks = function (r) {
        var e, n;
        r =
            "string" == typeof r
                ? ((e = r),
                  (n = {}),
                  C.each(e.match(O) || [], function (e, t) {
                      n[t] = !0;
                  }),
                  n)
                : C.extend({}, r);
        function i() {
            for (s = s || r.once, a = o = !0; l.length; c = -1) for (t = l.shift(); ++c < u.length; ) !1 === u[c].apply(t[0], t[1]) && r.stopOnFalse && ((c = u.length), (t = !1));
            r.memory || (t = !1), (o = !1), s && (u = t ? [] : "");
        }
        var o,
            t,
            a,
            s,
            u = [],
            l = [],
            c = -1,
            f = {
                add: function () {
                    return (
                        u &&
                            (t && !o && ((c = u.length - 1), l.push(t)),
                            (function n(e) {
                                C.each(e, function (e, t) {
                                    x(t) ? (r.unique && f.has(t)) || u.push(t) : t && t.length && "string" !== h(t) && n(t);
                                });
                            })(arguments),
                            t && !o && i()),
                        this
                    );
                },
                remove: function () {
                    return (
                        C.each(arguments, function (e, t) {
                            for (var n; -1 < (n = C.inArray(t, u, n)); ) u.splice(n, 1), n <= c && c--;
                        }),
                        this
                    );
                },
                has: function (e) {
                    return e ? -1 < C.inArray(e, u) : 0 < u.length;
                },
                empty: function () {
                    return (u = u && []), this;
                },
                disable: function () {
                    return (s = l = []), (u = t = ""), this;
                },
                disabled: function () {
                    return !u;
                },
                lock: function () {
                    return (s = l = []), t || o || (u = t = ""), this;
                },
                locked: function () {
                    return !!s;
                },
                fireWith: function (e, t) {
                    return s || ((t = [e, (t = t || []).slice ? t.slice() : t]), l.push(t), o || i()), this;
                },
                fire: function () {
                    return f.fireWith(this, arguments), this;
                },
                fired: function () {
                    return !!a;
                },
            };
        return f;
    }),
        C.extend({
            Deferred: function (e) {
                var o = [
                        ["notify", "progress", C.Callbacks("memory"), C.Callbacks("memory"), 2],
                        ["resolve", "done", C.Callbacks("once memory"), C.Callbacks("once memory"), 0, "resolved"],
                        ["reject", "fail", C.Callbacks("once memory"), C.Callbacks("once memory"), 1, "rejected"],
                    ],
                    i = "pending",
                    a = {
                        state: function () {
                            return i;
                        },
                        always: function () {
                            return s.done(arguments).fail(arguments), this;
                        },
                        catch: function (e) {
                            return a.then(null, e);
                        },
                        pipe: function () {
                            var i = arguments;
                            return C.Deferred(function (r) {
                                C.each(o, function (e, t) {
                                    var n = x(i[t[4]]) && i[t[4]];
                                    s[t[1]](function () {
                                        var e = n && n.apply(this, arguments);
                                        e && x(e.promise) ? e.promise().progress(r.notify).done(r.resolve).fail(r.reject) : r[t[0] + "With"](this, n ? [e] : arguments);
                                    });
                                }),
                                    (i = null);
                            }).promise();
                        },
                        then: function (t, n, r) {
                            var u = 0;
                            function l(i, o, a, s) {
                                return function () {
                                    function e() {
                                        var e, t;
                                        if (!(i < u)) {
                                            if ((e = a.apply(n, r)) === o.promise()) throw new TypeError("Thenable self-resolution");
                                            (t = e && ("object" == typeof e || "function" == typeof e) && e.then),
                                                x(t)
                                                    ? s
                                                        ? t.call(e, l(u, o, R, s), l(u, o, H, s))
                                                        : (u++, t.call(e, l(u, o, R, s), l(u, o, H, s), l(u, o, R, o.notifyWith)))
                                                    : (a !== R && ((n = void 0), (r = [e])), (s || o.resolveWith)(n, r));
                                        }
                                    }
                                    var n = this,
                                        r = arguments,
                                        t = s
                                            ? e
                                            : function () {
                                                  try {
                                                      e();
                                                  } catch (e) {
                                                      C.Deferred.exceptionHook && C.Deferred.exceptionHook(e, t.stackTrace), u <= i + 1 && (a !== H && ((n = void 0), (r = [e])), o.rejectWith(n, r));
                                                  }
                                              };
                                    i ? t() : (C.Deferred.getStackHook && (t.stackTrace = C.Deferred.getStackHook()), w.setTimeout(t));
                                };
                            }
                            return C.Deferred(function (e) {
                                o[0][3].add(l(0, e, x(r) ? r : R, e.notifyWith)), o[1][3].add(l(0, e, x(t) ? t : R)), o[2][3].add(l(0, e, x(n) ? n : H));
                            }).promise();
                        },
                        promise: function (e) {
                            return null != e ? C.extend(e, a) : a;
                        },
                    },
                    s = {};
                return (
                    C.each(o, function (e, t) {
                        var n = t[2],
                            r = t[5];
                        (a[t[1]] = n.add),
                            r &&
                                n.add(
                                    function () {
                                        i = r;
                                    },
                                    o[3 - e][2].disable,
                                    o[3 - e][3].disable,
                                    o[0][2].lock,
                                    o[0][3].lock
                                ),
                            n.add(t[3].fire),
                            (s[t[0]] = function () {
                                return s[t[0] + "With"](this === s ? void 0 : this, arguments), this;
                            }),
                            (s[t[0] + "With"] = n.fireWith);
                    }),
                    a.promise(s),
                    e && e.call(s, s),
                    s
                );
            },
            when: function (e) {
                function t(t) {
                    return function (e) {
                        (i[t] = this), (o[t] = 1 < arguments.length ? s.call(arguments) : e), --n || a.resolveWith(i, o);
                    };
                }
                var n = arguments.length,
                    r = n,
                    i = Array(r),
                    o = s.call(arguments),
                    a = C.Deferred();
                if (n <= 1 && (P(e, a.done(t(r)).resolve, a.reject, !n), "pending" === a.state() || x(o[r] && o[r].then))) return a.then();
                for (; r--; ) P(o[r], t(r), a.reject);
                return a.promise();
            },
        });
    var M = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    (C.Deferred.exceptionHook = function (e, t) {
        w.console && w.console.warn && e && M.test(e.name) && w.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t);
    }),
        (C.readyException = function (e) {
            w.setTimeout(function () {
                throw e;
            });
        });
    var W = C.Deferred();
    function F() {
        E.removeEventListener("DOMContentLoaded", F), w.removeEventListener("load", F), C.ready();
    }
    (C.fn.ready = function (e) {
        return (
            W.then(e).catch(function (e) {
                C.readyException(e);
            }),
            this
        );
    }),
        C.extend({
            isReady: !1,
            readyWait: 1,
            ready: function (e) {
                (!0 === e ? --C.readyWait : C.isReady) || ((C.isReady = !0) !== e && 0 < --C.readyWait) || W.resolveWith(E, [C]);
            },
        }),
        (C.ready.then = W.then),
        "complete" === E.readyState || ("loading" !== E.readyState && !E.documentElement.doScroll) ? w.setTimeout(C.ready) : (E.addEventListener("DOMContentLoaded", F), w.addEventListener("load", F));
    var B = function (e, t, n, r, i, o, a) {
            var s = 0,
                u = e.length,
                l = null == n;
            if ("object" === h(n)) for (s in ((i = !0), n)) B(e, t, s, n[s], !0, o, a);
            else if (
                void 0 !== r &&
                ((i = !0),
                x(r) || (a = !0),
                (t = l
                    ? a
                        ? (t.call(e, r), null)
                        : ((l = t),
                          function (e, t, n) {
                              return l.call(C(e), n);
                          })
                    : t))
            )
                for (; s < u; s++) t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
            return i ? e : l ? t.call(e) : u ? t(e[0], n) : o;
        },
        $ = /^-ms-/,
        U = /-([a-z])/g;
    function V(e, t) {
        return t.toUpperCase();
    }
    function G(e) {
        return e.replace($, "ms-").replace(U, V);
    }
    function z(e) {
        return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType;
    }
    function X() {
        this.expando = C.expando + X.uid++;
    }
    (X.uid = 1),
        (X.prototype = {
            cache: function (e) {
                var t = e[this.expando];
                return t || ((t = {}), z(e) && (e.nodeType ? (e[this.expando] = t) : Object.defineProperty(e, this.expando, { value: t, configurable: !0 }))), t;
            },
            set: function (e, t, n) {
                var r,
                    i = this.cache(e);
                if ("string" == typeof t) i[G(t)] = n;
                else for (r in t) i[G(r)] = t[r];
                return i;
            },
            get: function (e, t) {
                return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][G(t)];
            },
            access: function (e, t, n) {
                return void 0 === t || (t && "string" == typeof t && void 0 === n) ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t);
            },
            remove: function (e, t) {
                var n,
                    r = e[this.expando];
                if (void 0 !== r) {
                    if (void 0 !== t) {
                        n = (t = Array.isArray(t) ? t.map(G) : (t = G(t)) in r ? [t] : t.match(O) || []).length;
                        for (; n--; ) delete r[t[n]];
                    }
                    (void 0 !== t && !C.isEmptyObject(r)) || (e.nodeType ? (e[this.expando] = void 0) : delete e[this.expando]);
                }
            },
            hasData: function (e) {
                e = e[this.expando];
                return void 0 !== e && !C.isEmptyObject(e);
            },
        });
    var J = new X(),
        Q = new X(),
        Y = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        K = /[A-Z]/g;
    function Z(e, t, n) {
        var r, i;
        if (void 0 === n && 1 === e.nodeType)
            if (((r = "data-" + t.replace(K, "-$&").toLowerCase()), "string" == typeof (n = e.getAttribute(r)))) {
                try {
                    n = "true" === (i = n) || ("false" !== i && ("null" === i ? null : i === +i + "" ? +i : Y.test(i) ? JSON.parse(i) : i));
                } catch (e) {}
                Q.set(e, t, n);
            } else n = void 0;
        return n;
    }
    C.extend({
        hasData: function (e) {
            return Q.hasData(e) || J.hasData(e);
        },
        data: function (e, t, n) {
            return Q.access(e, t, n);
        },
        removeData: function (e, t) {
            Q.remove(e, t);
        },
        _data: function (e, t, n) {
            return J.access(e, t, n);
        },
        _removeData: function (e, t) {
            J.remove(e, t);
        },
    }),
        C.fn.extend({
            data: function (n, e) {
                var t,
                    r,
                    i,
                    o = this[0],
                    a = o && o.attributes;
                if (void 0 !== n)
                    return "object" == typeof n
                        ? this.each(function () {
                              Q.set(this, n);
                          })
                        : B(
                              this,
                              function (e) {
                                  var t;
                                  return o && void 0 === e
                                      ? void 0 !== (t = Q.get(o, n)) || void 0 !== (t = Z(o, n))
                                          ? t
                                          : void 0
                                      : void this.each(function () {
                                            Q.set(this, n, e);
                                        });
                              },
                              null,
                              e,
                              1 < arguments.length,
                              null,
                              !0
                          );
                if (this.length && ((i = Q.get(o)), 1 === o.nodeType && !J.get(o, "hasDataAttrs"))) {
                    for (t = a.length; t--; ) a[t] && 0 === (r = a[t].name).indexOf("data-") && ((r = G(r.slice(5))), Z(o, r, i[r]));
                    J.set(o, "hasDataAttrs", !0);
                }
                return i;
            },
            removeData: function (e) {
                return this.each(function () {
                    Q.remove(this, e);
                });
            },
        }),
        C.extend({
            queue: function (e, t, n) {
                var r;
                if (e) return (t = (t || "fx") + "queue"), (r = J.get(e, t)), n && (!r || Array.isArray(n) ? (r = J.access(e, t, C.makeArray(n))) : r.push(n)), r || [];
            },
            dequeue: function (e, t) {
                t = t || "fx";
                var n = C.queue(e, t),
                    r = n.length,
                    i = n.shift(),
                    o = C._queueHooks(e, t);
                "inprogress" === i && ((i = n.shift()), r--),
                    i &&
                        ("fx" === t && n.unshift("inprogress"),
                        delete o.stop,
                        i.call(
                            e,
                            function () {
                                C.dequeue(e, t);
                            },
                            o
                        )),
                    !r && o && o.empty.fire();
            },
            _queueHooks: function (e, t) {
                var n = t + "queueHooks";
                return (
                    J.get(e, n) ||
                    J.access(e, n, {
                        empty: C.Callbacks("once memory").add(function () {
                            J.remove(e, [t + "queue", n]);
                        }),
                    })
                );
            },
        }),
        C.fn.extend({
            queue: function (t, n) {
                var e = 2;
                return (
                    "string" != typeof t && ((n = t), (t = "fx"), e--),
                    arguments.length < e
                        ? C.queue(this[0], t)
                        : void 0 === n
                        ? this
                        : this.each(function () {
                              var e = C.queue(this, t, n);
                              C._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && C.dequeue(this, t);
                          })
                );
            },
            dequeue: function (e) {
                return this.each(function () {
                    C.dequeue(this, e);
                });
            },
            clearQueue: function (e) {
                return this.queue(e || "fx", []);
            },
            promise: function (e, t) {
                function n() {
                    --i || o.resolveWith(a, [a]);
                }
                var r,
                    i = 1,
                    o = C.Deferred(),
                    a = this,
                    s = this.length;
                for ("string" != typeof e && ((t = e), (e = void 0)), e = e || "fx"; s--; ) (r = J.get(a[s], e + "queueHooks")) && r.empty && (i++, r.empty.add(n));
                return n(), o.promise(t);
            },
        });
    var ee = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        te = new RegExp("^(?:([+-])=|)(" + ee + ")([a-z%]*)$", "i"),
        ne = ["Top", "Right", "Bottom", "Left"],
        re = E.documentElement,
        ie = function (e) {
            return C.contains(e.ownerDocument, e);
        },
        oe = { composed: !0 };
    re.getRootNode &&
        (ie = function (e) {
            return C.contains(e.ownerDocument, e) || e.getRootNode(oe) === e.ownerDocument;
        });
    var ae = function (e, t) {
        return "none" === (e = t || e).style.display || ("" === e.style.display && ie(e) && "none" === C.css(e, "display"));
    };
    function se(e, t, n, r) {
        var i,
            o,
            a = 20,
            s = r
                ? function () {
                      return r.cur();
                  }
                : function () {
                      return C.css(e, t, "");
                  },
            u = s(),
            l = (n && n[3]) || (C.cssNumber[t] ? "" : "px"),
            c = e.nodeType && (C.cssNumber[t] || ("px" !== l && +u)) && te.exec(C.css(e, t));
        if (c && c[3] !== l) {
            for (u /= 2, l = l || c[3], c = +u || 1; a--; ) C.style(e, t, c + l), (1 - o) * (1 - (o = s() / u || 0.5)) <= 0 && (a = 0), (c /= o);
            (c *= 2), C.style(e, t, c + l), (n = n || []);
        }
        return n && ((c = +c || +u || 0), (i = n[1] ? c + (n[1] + 1) * n[2] : +n[2]), r && ((r.unit = l), (r.start = c), (r.end = i))), i;
    }
    var ue = {};
    function le(e, t) {
        for (var n, r, i, o, a, s = [], u = 0, l = e.length; u < l; u++)
            (r = e[u]).style &&
                ((n = r.style.display),
                t
                    ? ("none" === n && ((s[u] = J.get(r, "display") || null), s[u] || (r.style.display = "")),
                      "" === r.style.display &&
                          ae(r) &&
                          (s[u] =
                              ((a = o = void 0),
                              (o = (i = r).ownerDocument),
                              (a = i.nodeName),
                              (i = ue[a]) || ((o = o.body.appendChild(o.createElement(a))), (i = C.css(o, "display")), o.parentNode.removeChild(o), "none" === i && (i = "block"), (ue[a] = i)))))
                    : "none" !== n && ((s[u] = "none"), J.set(r, "display", n)));
        for (u = 0; u < l; u++) null != s[u] && (e[u].style.display = s[u]);
        return e;
    }
    C.fn.extend({
        show: function () {
            return le(this, !0);
        },
        hide: function () {
            return le(this);
        },
        toggle: function (e) {
            return "boolean" == typeof e
                ? e
                    ? this.show()
                    : this.hide()
                : this.each(function () {
                      ae(this) ? C(this).show() : C(this).hide();
                  });
        },
    });
    var ce = /^(?:checkbox|radio)$/i,
        fe = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i,
        pe = /^$|^module$|\/(?:java|ecma)script/i;
    (f = E.createDocumentFragment().appendChild(E.createElement("div"))),
        (d = E.createElement("input")).setAttribute("type", "radio"),
        d.setAttribute("checked", "checked"),
        d.setAttribute("name", "t"),
        f.appendChild(d),
        (y.checkClone = f.cloneNode(!0).cloneNode(!0).lastChild.checked),
        (f.innerHTML = "<textarea>x</textarea>"),
        (y.noCloneChecked = !!f.cloneNode(!0).lastChild.defaultValue),
        (f.innerHTML = "<option></option>"),
        (y.option = !!f.lastChild);
    var de = { thead: [1, "<table>", "</table>"], col: [2, "<table><colgroup>", "</colgroup></table>"], tr: [2, "<table><tbody>", "</tbody></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], _default: [0, "", ""] };
    function he(e, t) {
        var n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [];
        return void 0 === t || (t && N(e, t)) ? C.merge([e], n) : n;
    }
    function ge(e, t) {
        for (var n = 0, r = e.length; n < r; n++) J.set(e[n], "globalEval", !t || J.get(t[n], "globalEval"));
    }
    (de.tbody = de.tfoot = de.colgroup = de.caption = de.thead), (de.th = de.td), y.option || (de.optgroup = de.option = [1, "<select multiple='multiple'>", "</select>"]);
    var me = /<|&#?\w+;/;
    function ve(e, t, n, r, i) {
        for (var o, a, s, u, l, c = t.createDocumentFragment(), f = [], p = 0, d = e.length; p < d; p++)
            if ((o = e[p]) || 0 === o)
                if ("object" === h(o)) C.merge(f, o.nodeType ? [o] : o);
                else if (me.test(o)) {
                    for (a = a || c.appendChild(t.createElement("div")), s = (fe.exec(o) || ["", ""])[1].toLowerCase(), s = de[s] || de._default, a.innerHTML = s[1] + C.htmlPrefilter(o) + s[2], l = s[0]; l--; ) a = a.lastChild;
                    C.merge(f, a.childNodes), ((a = c.firstChild).textContent = "");
                } else f.push(t.createTextNode(o));
        for (c.textContent = "", p = 0; (o = f[p++]); )
            if (r && -1 < C.inArray(o, r)) i && i.push(o);
            else if (((u = ie(o)), (a = he(c.appendChild(o), "script")), u && ge(a), n)) for (l = 0; (o = a[l++]); ) pe.test(o.type || "") && n.push(o);
        return c;
    }
    var ye = /^key/,
        xe = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        be = /^([^.]*)(?:\.(.+)|)/;
    function Te() {
        return !0;
    }
    function we() {
        return !1;
    }
    function Ee(e, t) {
        return (
            (e ===
                (function () {
                    try {
                        return E.activeElement;
                    } catch (e) {}
                })()) ==
            ("focus" === t)
        );
    }
    function Ce(e, t, n, r, i, o) {
        var a, s;
        if ("object" == typeof t) {
            for (s in ("string" != typeof n && ((r = r || n), (n = void 0)), t)) Ce(e, s, n, r, t[s], o);
            return e;
        }
        if ((null == r && null == i ? ((i = n), (r = n = void 0)) : null == i && ("string" == typeof n ? ((i = r), (r = void 0)) : ((i = r), (r = n), (n = void 0))), !1 === i)) i = we;
        else if (!i) return e;
        return (
            1 === o &&
                ((a = i),
                ((i = function (e) {
                    return C().off(e), a.apply(this, arguments);
                }).guid = a.guid || (a.guid = C.guid++))),
            e.each(function () {
                C.event.add(this, t, i, r, n);
            })
        );
    }
    function Se(e, i, o) {
        o
            ? (J.set(e, i, !1),
              C.event.add(e, i, {
                  namespace: !1,
                  handler: function (e) {
                      var t,
                          n,
                          r = J.get(this, i);
                      if (1 & e.isTrigger && this[i]) {
                          if (r.length) (C.event.special[i] || {}).delegateType && e.stopPropagation();
                          else if (((r = s.call(arguments)), J.set(this, i, r), (t = o(this, i)), this[i](), r !== (n = J.get(this, i)) || t ? J.set(this, i, !1) : (n = {}), r !== n))
                              return e.stopImmediatePropagation(), e.preventDefault(), n.value;
                      } else r.length && (J.set(this, i, { value: C.event.trigger(C.extend(r[0], C.Event.prototype), r.slice(1), this) }), e.stopImmediatePropagation());
                  },
              }))
            : void 0 === J.get(e, i) && C.event.add(e, i, Te);
    }
    (C.event = {
        global: {},
        add: function (t, e, n, r, i) {
            var o,
                a,
                s,
                u,
                l,
                c,
                f,
                p,
                d,
                h = J.get(t);
            if (z(t))
                for (
                    n.handler && ((n = (o = n).handler), (i = o.selector)),
                        i && C.find.matchesSelector(re, i),
                        n.guid || (n.guid = C.guid++),
                        (s = h.events) || (s = h.events = Object.create(null)),
                        (a = h.handle) ||
                            (a = h.handle = function (e) {
                                return void 0 !== C && C.event.triggered !== e.type ? C.event.dispatch.apply(t, arguments) : void 0;
                            }),
                        u = (e = (e || "").match(O) || [""]).length;
                    u--;

                )
                    (f = d = (l = be.exec(e[u]) || [])[1]),
                        (p = (l[2] || "").split(".").sort()),
                        f &&
                            ((c = C.event.special[f] || {}),
                            (f = (i ? c.delegateType : c.bindType) || f),
                            (c = C.event.special[f] || {}),
                            (l = C.extend({ type: f, origType: d, data: r, handler: n, guid: n.guid, selector: i, needsContext: i && C.expr.match.needsContext.test(i), namespace: p.join(".") }, o)),
                            (d = s[f]) || (((d = s[f] = []).delegateCount = 0), (c.setup && !1 !== c.setup.call(t, r, p, a)) || (t.addEventListener && t.addEventListener(f, a))),
                            c.add && (c.add.call(t, l), l.handler.guid || (l.handler.guid = n.guid)),
                            i ? d.splice(d.delegateCount++, 0, l) : d.push(l),
                            (C.event.global[f] = !0));
        },
        remove: function (e, t, n, r, i) {
            var o,
                a,
                s,
                u,
                l,
                c,
                f,
                p,
                d,
                h,
                g,
                m = J.hasData(e) && J.get(e);
            if (m && (u = m.events)) {
                for (l = (t = (t || "").match(O) || [""]).length; l--; )
                    if (((d = g = (s = be.exec(t[l]) || [])[1]), (h = (s[2] || "").split(".").sort()), d)) {
                        for (f = C.event.special[d] || {}, p = u[(d = (r ? f.delegateType : f.bindType) || d)] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = o = p.length; o--; )
                            (c = p[o]),
                                (!i && g !== c.origType) ||
                                    (n && n.guid !== c.guid) ||
                                    (s && !s.test(c.namespace)) ||
                                    (r && r !== c.selector && ("**" !== r || !c.selector)) ||
                                    (p.splice(o, 1), c.selector && p.delegateCount--, f.remove && f.remove.call(e, c));
                        a && !p.length && ((f.teardown && !1 !== f.teardown.call(e, h, m.handle)) || C.removeEvent(e, d, m.handle), delete u[d]);
                    } else for (d in u) C.event.remove(e, d + t[l], n, r, !0);
                C.isEmptyObject(u) && J.remove(e, "handle events");
            }
        },
        dispatch: function (e) {
            var t,
                n,
                r,
                i,
                o,
                a = new Array(arguments.length),
                s = C.event.fix(e),
                u = (J.get(this, "events") || Object.create(null))[s.type] || [],
                e = C.event.special[s.type] || {};
            for (a[0] = s, t = 1; t < arguments.length; t++) a[t] = arguments[t];
            if (((s.delegateTarget = this), !e.preDispatch || !1 !== e.preDispatch.call(this, s))) {
                for (o = C.event.handlers.call(this, s, u), t = 0; (r = o[t++]) && !s.isPropagationStopped(); )
                    for (s.currentTarget = r.elem, n = 0; (i = r.handlers[n++]) && !s.isImmediatePropagationStopped(); )
                        (s.rnamespace && !1 !== i.namespace && !s.rnamespace.test(i.namespace)) ||
                            ((s.handleObj = i), (s.data = i.data), void 0 !== (i = ((C.event.special[i.origType] || {}).handle || i.handler).apply(r.elem, a)) && !1 === (s.result = i) && (s.preventDefault(), s.stopPropagation()));
                return e.postDispatch && e.postDispatch.call(this, s), s.result;
            }
        },
        handlers: function (e, t) {
            var n,
                r,
                i,
                o,
                a,
                s = [],
                u = t.delegateCount,
                l = e.target;
            if (u && l.nodeType && !("click" === e.type && 1 <= e.button))
                for (; l !== this; l = l.parentNode || this)
                    if (1 === l.nodeType && ("click" !== e.type || !0 !== l.disabled)) {
                        for (o = [], a = {}, n = 0; n < u; n++) void 0 === a[(i = (r = t[n]).selector + " ")] && (a[i] = r.needsContext ? -1 < C(i, this).index(l) : C.find(i, this, null, [l]).length), a[i] && o.push(r);
                        o.length && s.push({ elem: l, handlers: o });
                    }
            return (l = this), u < t.length && s.push({ elem: l, handlers: t.slice(u) }), s;
        },
        addProp: function (t, e) {
            Object.defineProperty(C.Event.prototype, t, {
                enumerable: !0,
                configurable: !0,
                get: x(e)
                    ? function () {
                          if (this.originalEvent) return e(this.originalEvent);
                      }
                    : function () {
                          if (this.originalEvent) return this.originalEvent[t];
                      },
                set: function (e) {
                    Object.defineProperty(this, t, { enumerable: !0, configurable: !0, writable: !0, value: e });
                },
            });
        },
        fix: function (e) {
            return e[C.expando] ? e : new C.Event(e);
        },
        special: {
            load: { noBubble: !0 },
            click: {
                setup: function (e) {
                    e = this || e;
                    return ce.test(e.type) && e.click && N(e, "input") && Se(e, "click", Te), !1;
                },
                trigger: function (e) {
                    e = this || e;
                    return ce.test(e.type) && e.click && N(e, "input") && Se(e, "click"), !0;
                },
                _default: function (e) {
                    e = e.target;
                    return (ce.test(e.type) && e.click && N(e, "input") && J.get(e, "click")) || N(e, "a");
                },
            },
            beforeunload: {
                postDispatch: function (e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result);
                },
            },
        },
    }),
        (C.removeEvent = function (e, t, n) {
            e.removeEventListener && e.removeEventListener(t, n);
        }),
        (C.Event = function (e, t) {
            if (!(this instanceof C.Event)) return new C.Event(e, t);
            e && e.type
                ? ((this.originalEvent = e),
                  (this.type = e.type),
                  (this.isDefaultPrevented = e.defaultPrevented || (void 0 === e.defaultPrevented && !1 === e.returnValue) ? Te : we),
                  (this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target),
                  (this.currentTarget = e.currentTarget),
                  (this.relatedTarget = e.relatedTarget))
                : (this.type = e),
                t && C.extend(this, t),
                (this.timeStamp = (e && e.timeStamp) || Date.now()),
                (this[C.expando] = !0);
        }),
        (C.Event.prototype = {
            constructor: C.Event,
            isDefaultPrevented: we,
            isPropagationStopped: we,
            isImmediatePropagationStopped: we,
            isSimulated: !1,
            preventDefault: function () {
                var e = this.originalEvent;
                (this.isDefaultPrevented = Te), e && !this.isSimulated && e.preventDefault();
            },
            stopPropagation: function () {
                var e = this.originalEvent;
                (this.isPropagationStopped = Te), e && !this.isSimulated && e.stopPropagation();
            },
            stopImmediatePropagation: function () {
                var e = this.originalEvent;
                (this.isImmediatePropagationStopped = Te), e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation();
            },
        }),
        C.each(
            {
                altKey: !0,
                bubbles: !0,
                cancelable: !0,
                changedTouches: !0,
                ctrlKey: !0,
                detail: !0,
                eventPhase: !0,
                metaKey: !0,
                pageX: !0,
                pageY: !0,
                shiftKey: !0,
                view: !0,
                char: !0,
                code: !0,
                charCode: !0,
                key: !0,
                keyCode: !0,
                button: !0,
                buttons: !0,
                clientX: !0,
                clientY: !0,
                offsetX: !0,
                offsetY: !0,
                pointerId: !0,
                pointerType: !0,
                screenX: !0,
                screenY: !0,
                targetTouches: !0,
                toElement: !0,
                touches: !0,
                which: function (e) {
                    var t = e.button;
                    return null == e.which && ye.test(e.type) ? (null != e.charCode ? e.charCode : e.keyCode) : !e.which && void 0 !== t && xe.test(e.type) ? (1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0) : e.which;
                },
            },
            C.event.addProp
        ),
        C.each({ focus: "focusin", blur: "focusout" }, function (e, t) {
            C.event.special[e] = {
                setup: function () {
                    return Se(this, e, Ee), !1;
                },
                trigger: function () {
                    return Se(this, e), !0;
                },
                delegateType: t,
            };
        }),
        C.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, function (e, i) {
            C.event.special[e] = {
                delegateType: i,
                bindType: i,
                handle: function (e) {
                    var t,
                        n = e.relatedTarget,
                        r = e.handleObj;
                    return (n && (n === this || C.contains(this, n))) || ((e.type = r.origType), (t = r.handler.apply(this, arguments)), (e.type = i)), t;
                },
            };
        }),
        C.fn.extend({
            on: function (e, t, n, r) {
                return Ce(this, e, t, n, r);
            },
            one: function (e, t, n, r) {
                return Ce(this, e, t, n, r, 1);
            },
            off: function (e, t, n) {
                var r, i;
                if (e && e.preventDefault && e.handleObj) return (r = e.handleObj), C(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;
                if ("object" != typeof e)
                    return (
                        (!1 !== t && "function" != typeof t) || ((n = t), (t = void 0)),
                        !1 === n && (n = we),
                        this.each(function () {
                            C.event.remove(this, e, n, t);
                        })
                    );
                for (i in e) this.off(i, t, e[i]);
                return this;
            },
        });
    var Ae = /<script|<style|<link/i,
        Ne = /checked\s*(?:[^=]|=\s*.checked.)/i,
        De = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    function ke(e, t) {
        return (N(e, "table") && N(11 !== t.nodeType ? t : t.firstChild, "tr") && C(e).children("tbody")[0]) || e;
    }
    function je(e) {
        return (e.type = (null !== e.getAttribute("type")) + "/" + e.type), e;
    }
    function _e(e) {
        return "true/" === (e.type || "").slice(0, 5) ? (e.type = e.type.slice(5)) : e.removeAttribute("type"), e;
    }
    function qe(e, t) {
        var n, r, i, o;
        if (1 === t.nodeType) {
            if (J.hasData(e) && (o = J.get(e).events)) for (i in (J.remove(t, "handle events"), o)) for (n = 0, r = o[i].length; n < r; n++) C.event.add(t, i, o[i][n]);
            Q.hasData(e) && ((e = Q.access(e)), (e = C.extend({}, e)), Q.set(t, e));
        }
    }
    function Le(n, r, i, o) {
        r = m(r);
        var e,
            t,
            a,
            s,
            u,
            l,
            c = 0,
            f = n.length,
            p = f - 1,
            d = r[0],
            h = x(d);
        if (h || (1 < f && "string" == typeof d && !y.checkClone && Ne.test(d)))
            return n.each(function (e) {
                var t = n.eq(e);
                h && (r[0] = d.call(this, e, t.html())), Le(t, r, i, o);
            });
        if (f && ((t = (e = ve(r, n[0].ownerDocument, !1, n, o)).firstChild), 1 === e.childNodes.length && (e = t), t || o)) {
            for (s = (a = C.map(he(e, "script"), je)).length; c < f; c++) (u = e), c !== p && ((u = C.clone(u, !0, !0)), s && C.merge(a, he(u, "script"))), i.call(n[c], u, c);
            if (s)
                for (l = a[a.length - 1].ownerDocument, C.map(a, _e), c = 0; c < s; c++)
                    (u = a[c]),
                        pe.test(u.type || "") &&
                            !J.access(u, "globalEval") &&
                            C.contains(l, u) &&
                            (u.src && "module" !== (u.type || "").toLowerCase() ? C._evalUrl && !u.noModule && C._evalUrl(u.src, { nonce: u.nonce || u.getAttribute("nonce") }, l) : b(u.textContent.replace(De, ""), u, l));
        }
        return n;
    }
    function Ie(e, t, n) {
        for (var r, i = t ? C.filter(t, e) : e, o = 0; null != (r = i[o]); o++) n || 1 !== r.nodeType || C.cleanData(he(r)), r.parentNode && (n && ie(r) && ge(he(r, "script")), r.parentNode.removeChild(r));
        return e;
    }
    C.extend({
        htmlPrefilter: function (e) {
            return e;
        },
        clone: function (e, t, n) {
            var r,
                i,
                o,
                a,
                s,
                u,
                l,
                c = e.cloneNode(!0),
                f = ie(e);
            if (!(y.noCloneChecked || (1 !== e.nodeType && 11 !== e.nodeType) || C.isXMLDoc(e)))
                for (a = he(c), r = 0, i = (o = he(e)).length; r < i; r++)
                    (s = o[r]), (u = a[r]), (l = void 0), "input" === (l = u.nodeName.toLowerCase()) && ce.test(s.type) ? (u.checked = s.checked) : ("input" !== l && "textarea" !== l) || (u.defaultValue = s.defaultValue);
            if (t)
                if (n) for (o = o || he(e), a = a || he(c), r = 0, i = o.length; r < i; r++) qe(o[r], a[r]);
                else qe(e, c);
            return 0 < (a = he(c, "script")).length && ge(a, !f && he(e, "script")), c;
        },
        cleanData: function (e) {
            for (var t, n, r, i = C.event.special, o = 0; void 0 !== (n = e[o]); o++)
                if (z(n)) {
                    if ((t = n[J.expando])) {
                        if (t.events) for (r in t.events) i[r] ? C.event.remove(n, r) : C.removeEvent(n, r, t.handle);
                        n[J.expando] = void 0;
                    }
                    n[Q.expando] && (n[Q.expando] = void 0);
                }
        },
    }),
        C.fn.extend({
            detach: function (e) {
                return Ie(this, e, !0);
            },
            remove: function (e) {
                return Ie(this, e);
            },
            text: function (e) {
                return B(
                    this,
                    function (e) {
                        return void 0 === e
                            ? C.text(this)
                            : this.empty().each(function () {
                                  (1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) || (this.textContent = e);
                              });
                    },
                    null,
                    e,
                    arguments.length
                );
            },
            append: function () {
                return Le(this, arguments, function (e) {
                    (1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) || ke(this, e).appendChild(e);
                });
            },
            prepend: function () {
                return Le(this, arguments, function (e) {
                    var t;
                    (1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) || (t = ke(this, e)).insertBefore(e, t.firstChild);
                });
            },
            before: function () {
                return Le(this, arguments, function (e) {
                    this.parentNode && this.parentNode.insertBefore(e, this);
                });
            },
            after: function () {
                return Le(this, arguments, function (e) {
                    this.parentNode && this.parentNode.insertBefore(e, this.nextSibling);
                });
            },
            empty: function () {
                for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (C.cleanData(he(e, !1)), (e.textContent = ""));
                return this;
            },
            clone: function (e, t) {
                return (
                    (e = null != e && e),
                    (t = null == t ? e : t),
                    this.map(function () {
                        return C.clone(this, e, t);
                    })
                );
            },
            html: function (e) {
                return B(
                    this,
                    function (e) {
                        var t = this[0] || {},
                            n = 0,
                            r = this.length;
                        if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                        if ("string" == typeof e && !Ae.test(e) && !de[(fe.exec(e) || ["", ""])[1].toLowerCase()]) {
                            e = C.htmlPrefilter(e);
                            try {
                                for (; n < r; n++) 1 === (t = this[n] || {}).nodeType && (C.cleanData(he(t, !1)), (t.innerHTML = e));
                                t = 0;
                            } catch (e) {}
                        }
                        t && this.empty().append(e);
                    },
                    null,
                    e,
                    arguments.length
                );
            },
            replaceWith: function () {
                var n = [];
                return Le(
                    this,
                    arguments,
                    function (e) {
                        var t = this.parentNode;
                        C.inArray(this, n) < 0 && (C.cleanData(he(this)), t && t.replaceChild(e, this));
                    },
                    n
                );
            },
        }),
        C.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function (e, a) {
            C.fn[e] = function (e) {
                for (var t, n = [], r = C(e), i = r.length - 1, o = 0; o <= i; o++) (t = o === i ? this : this.clone(!0)), C(r[o])[a](t), u.apply(n, t.get());
                return this.pushStack(n);
            };
        });
    function Oe(e, t, n) {
        var r,
            i = {};
        for (r in t) (i[r] = e.style[r]), (e.style[r] = t[r]);
        for (r in ((n = n.call(e)), t)) e.style[r] = i[r];
        return n;
    }
    var Re,
        He,
        Pe,
        Me,
        We,
        Fe,
        Be,
        $e,
        Ue = new RegExp("^(" + ee + ")(?!px)[a-z%]+$", "i"),
        Ve = function (e) {
            var t = e.ownerDocument.defaultView;
            return (t = !t || !t.opener ? w : t).getComputedStyle(e);
        },
        Ge = new RegExp(ne.join("|"), "i");
    function ze() {
        var e;
        $e &&
            ((Be.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0"),
            ($e.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%"),
            re.appendChild(Be).appendChild($e),
            (e = w.getComputedStyle($e)),
            (Re = "1%" !== e.top),
            (Fe = 12 === Xe(e.marginLeft)),
            ($e.style.right = "60%"),
            (Me = 36 === Xe(e.right)),
            (He = 36 === Xe(e.width)),
            ($e.style.position = "absolute"),
            (Pe = 12 === Xe($e.offsetWidth / 3)),
            re.removeChild(Be),
            ($e = null));
    }
    function Xe(e) {
        return Math.round(parseFloat(e));
    }
    function Je(e, t, n) {
        var r,
            i,
            o = e.style;
        return (
            (n = n || Ve(e)) &&
                ("" !== (i = n.getPropertyValue(t) || n[t]) || ie(e) || (i = C.style(e, t)),
                !y.pixelBoxStyles() && Ue.test(i) && Ge.test(t) && ((r = o.width), (e = o.minWidth), (t = o.maxWidth), (o.minWidth = o.maxWidth = o.width = i), (i = n.width), (o.width = r), (o.minWidth = e), (o.maxWidth = t))),
            void 0 !== i ? i + "" : i
        );
    }
    function Qe(e, t) {
        return {
            get: function () {
                if (!e()) return (this.get = t).apply(this, arguments);
                delete this.get;
            },
        };
    }
    (Be = E.createElement("div")),
        ($e = E.createElement("div")).style &&
            (($e.style.backgroundClip = "content-box"),
            ($e.cloneNode(!0).style.backgroundClip = ""),
            (y.clearCloneStyle = "content-box" === $e.style.backgroundClip),
            C.extend(y, {
                boxSizingReliable: function () {
                    return ze(), He;
                },
                pixelBoxStyles: function () {
                    return ze(), Me;
                },
                pixelPosition: function () {
                    return ze(), Re;
                },
                reliableMarginLeft: function () {
                    return ze(), Fe;
                },
                scrollboxSize: function () {
                    return ze(), Pe;
                },
                reliableTrDimensions: function () {
                    var e, t, n;
                    return (
                        null == We &&
                            ((e = E.createElement("table")),
                            (n = E.createElement("tr")),
                            (t = E.createElement("div")),
                            (e.style.cssText = "position:absolute;left:-11111px"),
                            (n.style.height = "1px"),
                            (t.style.height = "9px"),
                            re.appendChild(e).appendChild(n).appendChild(t),
                            (n = w.getComputedStyle(n)),
                            (We = 3 < parseInt(n.height)),
                            re.removeChild(e)),
                        We
                    );
                },
            }));
    var Ye = ["Webkit", "Moz", "ms"],
        Ke = E.createElement("div").style,
        Ze = {};
    function et(e) {
        var t = C.cssProps[e] || Ze[e];
        return (
            t ||
            (e in Ke
                ? e
                : (Ze[e] =
                      (function (e) {
                          for (var t = e[0].toUpperCase() + e.slice(1), n = Ye.length; n--; ) if ((e = Ye[n] + t) in Ke) return e;
                      })(e) || e))
        );
    }
    var tt = /^(none|table(?!-c[ea]).+)/,
        nt = /^--/,
        rt = { position: "absolute", visibility: "hidden", display: "block" },
        it = { letterSpacing: "0", fontWeight: "400" };
    function ot(e, t, n) {
        var r = te.exec(t);
        return r ? Math.max(0, r[2] - (n || 0)) + (r[3] || "px") : t;
    }
    function at(e, t, n, r, i, o) {
        var a = "width" === t ? 1 : 0,
            s = 0,
            u = 0;
        if (n === (r ? "border" : "content")) return 0;
        for (; a < 4; a += 2)
            "margin" === n && (u += C.css(e, n + ne[a], !0, i)),
                r
                    ? ("content" === n && (u -= C.css(e, "padding" + ne[a], !0, i)), "margin" !== n && (u -= C.css(e, "border" + ne[a] + "Width", !0, i)))
                    : ((u += C.css(e, "padding" + ne[a], !0, i)), "padding" !== n ? (u += C.css(e, "border" + ne[a] + "Width", !0, i)) : (s += C.css(e, "border" + ne[a] + "Width", !0, i)));
        return !r && 0 <= o && (u += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - o - u - s - 0.5)) || 0), u;
    }
    function st(e, t, n) {
        var r = Ve(e),
            i = (!y.boxSizingReliable() || n) && "border-box" === C.css(e, "boxSizing", !1, r),
            o = i,
            a = Je(e, t, r),
            s = "offset" + t[0].toUpperCase() + t.slice(1);
        if (Ue.test(a)) {
            if (!n) return a;
            a = "auto";
        }
        return (
            ((!y.boxSizingReliable() && i) || (!y.reliableTrDimensions() && N(e, "tr")) || "auto" === a || (!parseFloat(a) && "inline" === C.css(e, "display", !1, r))) &&
                e.getClientRects().length &&
                ((i = "border-box" === C.css(e, "boxSizing", !1, r)), (o = s in e) && (a = e[s])),
            (a = parseFloat(a) || 0) + at(e, t, n || (i ? "border" : "content"), o, r, a) + "px"
        );
    }
    function ut(e, t, n, r, i) {
        return new ut.prototype.init(e, t, n, r, i);
    }
    C.extend({
        cssHooks: {
            opacity: {
                get: function (e, t) {
                    if (t) {
                        e = Je(e, "opacity");
                        return "" === e ? "1" : e;
                    }
                },
            },
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            gridArea: !0,
            gridColumn: !0,
            gridColumnEnd: !0,
            gridColumnStart: !0,
            gridRow: !0,
            gridRowEnd: !0,
            gridRowStart: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0,
        },
        cssProps: {},
        style: function (e, t, n, r) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var i,
                    o,
                    a,
                    s = G(t),
                    u = nt.test(t),
                    l = e.style;
                if ((u || (t = et(s)), (a = C.cssHooks[t] || C.cssHooks[s]), void 0 === n)) return a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t];
                "string" === (o = typeof n) && (i = te.exec(n)) && i[1] && ((n = se(e, t, i)), (o = "number")),
                    null != n &&
                        n == n &&
                        ("number" !== o || u || (n += (i && i[3]) || (C.cssNumber[s] ? "" : "px")),
                        y.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"),
                        (a && "set" in a && void 0 === (n = a.set(e, n, r))) || (u ? l.setProperty(t, n) : (l[t] = n)));
            }
        },
        css: function (e, t, n, r) {
            var i,
                o = G(t);
            return (
                nt.test(t) || (t = et(o)),
                "normal" === (i = void 0 === (i = (o = C.cssHooks[t] || C.cssHooks[o]) && "get" in o ? o.get(e, !0, n) : i) ? Je(e, t, r) : i) && t in it && (i = it[t]),
                "" === n || n ? ((t = parseFloat(i)), !0 === n || isFinite(t) ? t || 0 : i) : i
            );
        },
    }),
        C.each(["height", "width"], function (e, s) {
            C.cssHooks[s] = {
                get: function (e, t, n) {
                    if (t)
                        return !tt.test(C.css(e, "display")) || (e.getClientRects().length && e.getBoundingClientRect().width)
                            ? st(e, s, n)
                            : Oe(e, rt, function () {
                                  return st(e, s, n);
                              });
                },
                set: function (e, t, n) {
                    var r,
                        i = Ve(e),
                        o = !y.scrollboxSize() && "absolute" === i.position,
                        a = (o || n) && "border-box" === C.css(e, "boxSizing", !1, i),
                        n = n ? at(e, s, n, a, i) : 0;
                    return (
                        a && o && (n -= Math.ceil(e["offset" + s[0].toUpperCase() + s.slice(1)] - parseFloat(i[s]) - at(e, s, "border", !1, i) - 0.5)),
                        n && (r = te.exec(t)) && "px" !== (r[3] || "px") && ((e.style[s] = t), (t = C.css(e, s))),
                        ot(0, t, n)
                    );
                },
            };
        }),
        (C.cssHooks.marginLeft = Qe(y.reliableMarginLeft, function (e, t) {
            if (t)
                return (
                    (parseFloat(Je(e, "marginLeft")) ||
                        e.getBoundingClientRect().left -
                            Oe(e, { marginLeft: 0 }, function () {
                                return e.getBoundingClientRect().left;
                            })) + "px"
                );
        })),
        C.each({ margin: "", padding: "", border: "Width" }, function (i, o) {
            (C.cssHooks[i + o] = {
                expand: function (e) {
                    for (var t = 0, n = {}, r = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) n[i + ne[t] + o] = r[t] || r[t - 2] || r[0];
                    return n;
                },
            }),
                "margin" !== i && (C.cssHooks[i + o].set = ot);
        }),
        C.fn.extend({
            css: function (e, t) {
                return B(
                    this,
                    function (e, t, n) {
                        var r,
                            i,
                            o = {},
                            a = 0;
                        if (Array.isArray(t)) {
                            for (r = Ve(e), i = t.length; a < i; a++) o[t[a]] = C.css(e, t[a], !1, r);
                            return o;
                        }
                        return void 0 !== n ? C.style(e, t, n) : C.css(e, t);
                    },
                    e,
                    t,
                    1 < arguments.length
                );
            },
        }),
        ((C.Tween = ut).prototype = {
            constructor: ut,
            init: function (e, t, n, r, i, o) {
                (this.elem = e), (this.prop = n), (this.easing = i || C.easing._default), (this.options = t), (this.start = this.now = this.cur()), (this.end = r), (this.unit = o || (C.cssNumber[n] ? "" : "px"));
            },
            cur: function () {
                var e = ut.propHooks[this.prop];
                return (e && e.get ? e : ut.propHooks._default).get(this);
            },
            run: function (e) {
                var t,
                    n = ut.propHooks[this.prop];
                return (
                    this.options.duration ? (this.pos = t = C.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration)) : (this.pos = t = e),
                    (this.now = (this.end - this.start) * t + this.start),
                    this.options.step && this.options.step.call(this.elem, this.now, this),
                    (n && n.set ? n : ut.propHooks._default).set(this),
                    this
                );
            },
        }),
        (ut.prototype.init.prototype = ut.prototype),
        (ut.propHooks = {
            _default: {
                get: function (e) {
                    return 1 !== e.elem.nodeType || (null != e.elem[e.prop] && null == e.elem.style[e.prop]) ? e.elem[e.prop] : (e = C.css(e.elem, e.prop, "")) && "auto" !== e ? e : 0;
                },
                set: function (e) {
                    C.fx.step[e.prop] ? C.fx.step[e.prop](e) : 1 !== e.elem.nodeType || (!C.cssHooks[e.prop] && null == e.elem.style[et(e.prop)]) ? (e.elem[e.prop] = e.now) : C.style(e.elem, e.prop, e.now + e.unit);
                },
            },
        }),
        (ut.propHooks.scrollTop = ut.propHooks.scrollLeft = {
            set: function (e) {
                e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
            },
        }),
        (C.easing = {
            linear: function (e) {
                return e;
            },
            swing: function (e) {
                return 0.5 - Math.cos(e * Math.PI) / 2;
            },
            _default: "swing",
        }),
        (C.fx = ut.prototype.init),
        (C.fx.step = {});
    var lt,
        ct,
        ft = /^(?:toggle|show|hide)$/,
        pt = /queueHooks$/;
    function dt() {
        ct && (!1 === E.hidden && w.requestAnimationFrame ? w.requestAnimationFrame(dt) : w.setTimeout(dt, C.fx.interval), C.fx.tick());
    }
    function ht() {
        return (
            w.setTimeout(function () {
                lt = void 0;
            }),
            (lt = Date.now())
        );
    }
    function gt(e, t) {
        var n,
            r = 0,
            i = { height: e };
        for (t = t ? 1 : 0; r < 4; r += 2 - t) i["margin" + (n = ne[r])] = i["padding" + n] = e;
        return t && (i.opacity = i.width = e), i;
    }
    function mt(e, t, n) {
        for (var r, i = (vt.tweeners[t] || []).concat(vt.tweeners["*"]), o = 0, a = i.length; o < a; o++) if ((r = i[o].call(n, t, e))) return r;
    }
    function vt(i, e, t) {
        var n,
            o,
            r = 0,
            a = vt.prefilters.length,
            s = C.Deferred().always(function () {
                delete u.elem;
            }),
            u = function () {
                if (o) return !1;
                for (var e = lt || ht(), e = Math.max(0, l.startTime + l.duration - e), t = 1 - (e / l.duration || 0), n = 0, r = l.tweens.length; n < r; n++) l.tweens[n].run(t);
                return s.notifyWith(i, [l, t, e]), t < 1 && r ? e : (r || s.notifyWith(i, [l, 1, 0]), s.resolveWith(i, [l]), !1);
            },
            l = s.promise({
                elem: i,
                props: C.extend({}, e),
                opts: C.extend(!0, { specialEasing: {}, easing: C.easing._default }, t),
                originalProperties: e,
                originalOptions: t,
                startTime: lt || ht(),
                duration: t.duration,
                tweens: [],
                createTween: function (e, t) {
                    e = C.Tween(i, l.opts, e, t, l.opts.specialEasing[e] || l.opts.easing);
                    return l.tweens.push(e), e;
                },
                stop: function (e) {
                    var t = 0,
                        n = e ? l.tweens.length : 0;
                    if (o) return this;
                    for (o = !0; t < n; t++) l.tweens[t].run(1);
                    return e ? (s.notifyWith(i, [l, 1, 0]), s.resolveWith(i, [l, e])) : s.rejectWith(i, [l, e]), this;
                },
            }),
            c = l.props;
        for (
            !(function (e, t) {
                var n, r, i, o, a;
                for (n in e)
                    if (((i = t[(r = G(n))]), (o = e[n]), Array.isArray(o) && ((i = o[1]), (o = e[n] = o[0])), n !== r && ((e[r] = o), delete e[n]), (a = C.cssHooks[r]) && ("expand" in a)))
                        for (n in ((o = a.expand(o)), delete e[r], o)) (n in e) || ((e[n] = o[n]), (t[n] = i));
                    else t[r] = i;
            })(c, l.opts.specialEasing);
            r < a;
            r++
        )
            if ((n = vt.prefilters[r].call(l, i, c, l.opts))) return x(n.stop) && (C._queueHooks(l.elem, l.opts.queue).stop = n.stop.bind(n)), n;
        return (
            C.map(c, mt, l),
            x(l.opts.start) && l.opts.start.call(i, l),
            l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always),
            C.fx.timer(C.extend(u, { elem: i, anim: l, queue: l.opts.queue })),
            l
        );
    }
    (C.Animation = C.extend(vt, {
        tweeners: {
            "*": [
                function (e, t) {
                    var n = this.createTween(e, t);
                    return se(n.elem, e, te.exec(t), n), n;
                },
            ],
        },
        tweener: function (e, t) {
            for (var n, r = 0, i = (e = x(e) ? ((t = e), ["*"]) : e.match(O)).length; r < i; r++) (n = e[r]), (vt.tweeners[n] = vt.tweeners[n] || []), vt.tweeners[n].unshift(t);
        },
        prefilters: [
            function (e, t, n) {
                var r,
                    i,
                    o,
                    a,
                    s,
                    u,
                    l,
                    c = "width" in t || "height" in t,
                    f = this,
                    p = {},
                    d = e.style,
                    h = e.nodeType && ae(e),
                    g = J.get(e, "fxshow");
                for (r in (n.queue ||
                    (null == (a = C._queueHooks(e, "fx")).unqueued &&
                        ((a.unqueued = 0),
                        (s = a.empty.fire),
                        (a.empty.fire = function () {
                            a.unqueued || s();
                        })),
                    a.unqueued++,
                    f.always(function () {
                        f.always(function () {
                            a.unqueued--, C.queue(e, "fx").length || a.empty.fire();
                        });
                    })),
                t))
                    if (((i = t[r]), ft.test(i))) {
                        if ((delete t[r], (o = o || "toggle" === i), i === (h ? "hide" : "show"))) {
                            if ("show" !== i || !g || void 0 === g[r]) continue;
                            h = !0;
                        }
                        p[r] = (g && g[r]) || C.style(e, r);
                    }
                if ((u = !C.isEmptyObject(t)) || !C.isEmptyObject(p))
                    for (r in (c &&
                        1 === e.nodeType &&
                        ((n.overflow = [d.overflow, d.overflowX, d.overflowY]),
                        null == (l = g && g.display) && (l = J.get(e, "display")),
                        "none" === (c = C.css(e, "display")) && (l ? (c = l) : (le([e], !0), (l = e.style.display || l), (c = C.css(e, "display")), le([e]))),
                        ("inline" === c || ("inline-block" === c && null != l)) &&
                            "none" === C.css(e, "float") &&
                            (u ||
                                (f.done(function () {
                                    d.display = l;
                                }),
                                null == l && ((c = d.display), (l = "none" === c ? "" : c))),
                            (d.display = "inline-block"))),
                    n.overflow &&
                        ((d.overflow = "hidden"),
                        f.always(function () {
                            (d.overflow = n.overflow[0]), (d.overflowX = n.overflow[1]), (d.overflowY = n.overflow[2]);
                        })),
                    (u = !1),
                    p))
                        u ||
                            (g ? "hidden" in g && (h = g.hidden) : (g = J.access(e, "fxshow", { display: l })),
                            o && (g.hidden = !h),
                            h && le([e], !0),
                            f.done(function () {
                                for (r in (h || le([e]), J.remove(e, "fxshow"), p)) C.style(e, r, p[r]);
                            })),
                            (u = mt(h ? g[r] : 0, r, f)),
                            r in g || ((g[r] = u.start), h && ((u.end = u.start), (u.start = 0)));
            },
        ],
        prefilter: function (e, t) {
            t ? vt.prefilters.unshift(e) : vt.prefilters.push(e);
        },
    })),
        (C.speed = function (e, t, n) {
            var r = e && "object" == typeof e ? C.extend({}, e) : { complete: n || (!n && t) || (x(e) && e), duration: e, easing: (n && t) || (t && !x(t) && t) };
            return (
                C.fx.off ? (r.duration = 0) : "number" != typeof r.duration && (r.duration in C.fx.speeds ? (r.duration = C.fx.speeds[r.duration]) : (r.duration = C.fx.speeds._default)),
                (null != r.queue && !0 !== r.queue) || (r.queue = "fx"),
                (r.old = r.complete),
                (r.complete = function () {
                    x(r.old) && r.old.call(this), r.queue && C.dequeue(this, r.queue);
                }),
                r
            );
        }),
        C.fn.extend({
            fadeTo: function (e, t, n, r) {
                return this.filter(ae).css("opacity", 0).show().end().animate({ opacity: t }, e, n, r);
            },
            animate: function (t, e, n, r) {
                var i = C.isEmptyObject(t),
                    o = C.speed(e, n, r),
                    r = function () {
                        var e = vt(this, C.extend({}, t), o);
                        (i || J.get(this, "finish")) && e.stop(!0);
                    };
                return (r.finish = r), i || !1 === o.queue ? this.each(r) : this.queue(o.queue, r);
            },
            stop: function (i, e, o) {
                function a(e) {
                    var t = e.stop;
                    delete e.stop, t(o);
                }
                return (
                    "string" != typeof i && ((o = e), (e = i), (i = void 0)),
                    e && this.queue(i || "fx", []),
                    this.each(function () {
                        var e = !0,
                            t = null != i && i + "queueHooks",
                            n = C.timers,
                            r = J.get(this);
                        if (t) r[t] && r[t].stop && a(r[t]);
                        else for (t in r) r[t] && r[t].stop && pt.test(t) && a(r[t]);
                        for (t = n.length; t--; ) n[t].elem !== this || (null != i && n[t].queue !== i) || (n[t].anim.stop(o), (e = !1), n.splice(t, 1));
                        (!e && o) || C.dequeue(this, i);
                    })
                );
            },
            finish: function (a) {
                return (
                    !1 !== a && (a = a || "fx"),
                    this.each(function () {
                        var e,
                            t = J.get(this),
                            n = t[a + "queue"],
                            r = t[a + "queueHooks"],
                            i = C.timers,
                            o = n ? n.length : 0;
                        for (t.finish = !0, C.queue(this, a, []), r && r.stop && r.stop.call(this, !0), e = i.length; e--; ) i[e].elem === this && i[e].queue === a && (i[e].anim.stop(!0), i.splice(e, 1));
                        for (e = 0; e < o; e++) n[e] && n[e].finish && n[e].finish.call(this);
                        delete t.finish;
                    })
                );
            },
        }),
        C.each(["toggle", "show", "hide"], function (e, r) {
            var i = C.fn[r];
            C.fn[r] = function (e, t, n) {
                return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(gt(r, !0), e, t, n);
            };
        }),
        C.each({ slideDown: gt("show"), slideUp: gt("hide"), slideToggle: gt("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, function (e, r) {
            C.fn[e] = function (e, t, n) {
                return this.animate(r, e, t, n);
            };
        }),
        (C.timers = []),
        (C.fx.tick = function () {
            var e,
                t = 0,
                n = C.timers;
            for (lt = Date.now(); t < n.length; t++) (e = n[t])() || n[t] !== e || n.splice(t--, 1);
            n.length || C.fx.stop(), (lt = void 0);
        }),
        (C.fx.timer = function (e) {
            C.timers.push(e), C.fx.start();
        }),
        (C.fx.interval = 13),
        (C.fx.start = function () {
            ct || ((ct = !0), dt());
        }),
        (C.fx.stop = function () {
            ct = null;
        }),
        (C.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
        (C.fn.delay = function (r, e) {
            return (
                (r = (C.fx && C.fx.speeds[r]) || r),
                (e = e || "fx"),
                this.queue(e, function (e, t) {
                    var n = w.setTimeout(e, r);
                    t.stop = function () {
                        w.clearTimeout(n);
                    };
                })
            );
        }),
        (f = E.createElement("input")),
        (ee = E.createElement("select").appendChild(E.createElement("option"))),
        (f.type = "checkbox"),
        (y.checkOn = "" !== f.value),
        (y.optSelected = ee.selected),
        ((f = E.createElement("input")).value = "t"),
        (f.type = "radio"),
        (y.radioValue = "t" === f.value);
    var yt,
        xt = C.expr.attrHandle;
    C.fn.extend({
        attr: function (e, t) {
            return B(this, C.attr, e, t, 1 < arguments.length);
        },
        removeAttr: function (e) {
            return this.each(function () {
                C.removeAttr(this, e);
            });
        },
    }),
        C.extend({
            attr: function (e, t, n) {
                var r,
                    i,
                    o = e.nodeType;
                if (3 !== o && 8 !== o && 2 !== o)
                    return void 0 === e.getAttribute
                        ? C.prop(e, t, n)
                        : ((1 === o && C.isXMLDoc(e)) || (i = C.attrHooks[t.toLowerCase()] || (C.expr.match.bool.test(t) ? yt : void 0)),
                          void 0 !== n
                              ? null === n
                                  ? void C.removeAttr(e, t)
                                  : i && "set" in i && void 0 !== (r = i.set(e, n, t))
                                  ? r
                                  : (e.setAttribute(t, n + ""), n)
                              : !(i && "get" in i && null !== (r = i.get(e, t))) && null == (r = C.find.attr(e, t))
                              ? void 0
                              : r);
            },
            attrHooks: {
                type: {
                    set: function (e, t) {
                        if (!y.radioValue && "radio" === t && N(e, "input")) {
                            var n = e.value;
                            return e.setAttribute("type", t), n && (e.value = n), t;
                        }
                    },
                },
            },
            removeAttr: function (e, t) {
                var n,
                    r = 0,
                    i = t && t.match(O);
                if (i && 1 === e.nodeType) for (; (n = i[r++]); ) e.removeAttribute(n);
            },
        }),
        (yt = {
            set: function (e, t, n) {
                return !1 === t ? C.removeAttr(e, n) : e.setAttribute(n, n), n;
            },
        }),
        C.each(C.expr.match.bool.source.match(/\w+/g), function (e, t) {
            var a = xt[t] || C.find.attr;
            xt[t] = function (e, t, n) {
                var r,
                    i,
                    o = t.toLowerCase();
                return n || ((i = xt[o]), (xt[o] = r), (r = null != a(e, t, n) ? o : null), (xt[o] = i)), r;
            };
        });
    var bt = /^(?:input|select|textarea|button)$/i,
        Tt = /^(?:a|area)$/i;
    function wt(e) {
        return (e.match(O) || []).join(" ");
    }
    function Et(e) {
        return (e.getAttribute && e.getAttribute("class")) || "";
    }
    function Ct(e) {
        return Array.isArray(e) ? e : ("string" == typeof e && e.match(O)) || [];
    }
    C.fn.extend({
        prop: function (e, t) {
            return B(this, C.prop, e, t, 1 < arguments.length);
        },
        removeProp: function (e) {
            return this.each(function () {
                delete this[C.propFix[e] || e];
            });
        },
    }),
        C.extend({
            prop: function (e, t, n) {
                var r,
                    i,
                    o = e.nodeType;
                if (3 !== o && 8 !== o && 2 !== o)
                    return (
                        (1 === o && C.isXMLDoc(e)) || ((t = C.propFix[t] || t), (i = C.propHooks[t])),
                        void 0 !== n ? (i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : (e[t] = n)) : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t]
                    );
            },
            propHooks: {
                tabIndex: {
                    get: function (e) {
                        var t = C.find.attr(e, "tabindex");
                        return t ? parseInt(t, 10) : bt.test(e.nodeName) || (Tt.test(e.nodeName) && e.href) ? 0 : -1;
                    },
                },
            },
            propFix: { for: "htmlFor", class: "className" },
        }),
        y.optSelected ||
            (C.propHooks.selected = {
                get: function (e) {
                    e = e.parentNode;
                    return e && e.parentNode && e.parentNode.selectedIndex, null;
                },
                set: function (e) {
                    e = e.parentNode;
                    e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex);
                },
            }),
        C.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
            C.propFix[this.toLowerCase()] = this;
        }),
        C.fn.extend({
            addClass: function (t) {
                var e,
                    n,
                    r,
                    i,
                    o,
                    a,
                    s = 0;
                if (x(t))
                    return this.each(function (e) {
                        C(this).addClass(t.call(this, e, Et(this)));
                    });
                if ((e = Ct(t)).length)
                    for (; (n = this[s++]); )
                        if (((a = Et(n)), (r = 1 === n.nodeType && " " + wt(a) + " "))) {
                            for (o = 0; (i = e[o++]); ) r.indexOf(" " + i + " ") < 0 && (r += i + " ");
                            a !== (a = wt(r)) && n.setAttribute("class", a);
                        }
                return this;
            },
            removeClass: function (t) {
                var e,
                    n,
                    r,
                    i,
                    o,
                    a,
                    s = 0;
                if (x(t))
                    return this.each(function (e) {
                        C(this).removeClass(t.call(this, e, Et(this)));
                    });
                if (!arguments.length) return this.attr("class", "");
                if ((e = Ct(t)).length)
                    for (; (n = this[s++]); )
                        if (((a = Et(n)), (r = 1 === n.nodeType && " " + wt(a) + " "))) {
                            for (o = 0; (i = e[o++]); ) for (; -1 < r.indexOf(" " + i + " "); ) r = r.replace(" " + i + " ", " ");
                            a !== (a = wt(r)) && n.setAttribute("class", a);
                        }
                return this;
            },
            toggleClass: function (i, t) {
                var o = typeof i,
                    a = "string" == o || Array.isArray(i);
                return "boolean" == typeof t && a
                    ? t
                        ? this.addClass(i)
                        : this.removeClass(i)
                    : x(i)
                    ? this.each(function (e) {
                          C(this).toggleClass(i.call(this, e, Et(this), t), t);
                      })
                    : this.each(function () {
                          var e, t, n, r;
                          if (a) for (t = 0, n = C(this), r = Ct(i); (e = r[t++]); ) n.hasClass(e) ? n.removeClass(e) : n.addClass(e);
                          else (void 0 !== i && "boolean" != o) || ((e = Et(this)) && J.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", (!e && !1 !== i && J.get(this, "__className__")) || ""));
                      });
            },
            hasClass: function (e) {
                for (var t, n = 0, r = " " + e + " "; (t = this[n++]); ) if (1 === t.nodeType && -1 < (" " + wt(Et(t)) + " ").indexOf(r)) return !0;
                return !1;
            },
        });
    var St = /\r/g;
    C.fn.extend({
        val: function (t) {
            var n,
                e,
                r,
                i = this[0];
            return arguments.length
                ? ((r = x(t)),
                  this.each(function (e) {
                      1 === this.nodeType &&
                          (null == (e = r ? t.call(this, e, C(this).val()) : t)
                              ? (e = "")
                              : "number" == typeof e
                              ? (e += "")
                              : Array.isArray(e) &&
                                (e = C.map(e, function (e) {
                                    return null == e ? "" : e + "";
                                })),
                          ((n = C.valHooks[this.type] || C.valHooks[this.nodeName.toLowerCase()]) && "set" in n && void 0 !== n.set(this, e, "value")) || (this.value = e));
                  }))
                : i
                ? (n = C.valHooks[i.type] || C.valHooks[i.nodeName.toLowerCase()]) && "get" in n && void 0 !== (e = n.get(i, "value"))
                    ? e
                    : "string" == typeof (e = i.value)
                    ? e.replace(St, "")
                    : null == e
                    ? ""
                    : e
                : void 0;
        },
    }),
        C.extend({
            valHooks: {
                option: {
                    get: function (e) {
                        var t = C.find.attr(e, "value");
                        return null != t ? t : wt(C.text(e));
                    },
                },
                select: {
                    get: function (e) {
                        for (var t, n = e.options, r = e.selectedIndex, i = "select-one" === e.type, o = i ? null : [], a = i ? r + 1 : n.length, s = r < 0 ? a : i ? r : 0; s < a; s++)
                            if (((t = n[s]).selected || s === r) && !t.disabled && (!t.parentNode.disabled || !N(t.parentNode, "optgroup"))) {
                                if (((t = C(t).val()), i)) return t;
                                o.push(t);
                            }
                        return o;
                    },
                    set: function (e, t) {
                        for (var n, r, i = e.options, o = C.makeArray(t), a = i.length; a--; ) ((r = i[a]).selected = -1 < C.inArray(C.valHooks.option.get(r), o)) && (n = !0);
                        return n || (e.selectedIndex = -1), o;
                    },
                },
            },
        }),
        C.each(["radio", "checkbox"], function () {
            (C.valHooks[this] = {
                set: function (e, t) {
                    if (Array.isArray(t)) return (e.checked = -1 < C.inArray(C(e).val(), t));
                },
            }),
                y.checkOn ||
                    (C.valHooks[this].get = function (e) {
                        return null === e.getAttribute("value") ? "on" : e.value;
                    });
        }),
        (y.focusin = "onfocusin" in w);
    function At(e) {
        e.stopPropagation();
    }
    var Nt = /^(?:focusinfocus|focusoutblur)$/;
    C.extend(C.event, {
        trigger: function (e, t, n, r) {
            var i,
                o,
                a,
                s,
                u,
                l,
                c,
                f = [n || E],
                p = v.call(e, "type") ? e.type : e,
                d = v.call(e, "namespace") ? e.namespace.split(".") : [],
                h = (c = o = n = n || E);
            if (
                3 !== n.nodeType &&
                8 !== n.nodeType &&
                !Nt.test(p + C.event.triggered) &&
                (-1 < p.indexOf(".") && ((p = (d = p.split(".")).shift()), d.sort()),
                (s = p.indexOf(":") < 0 && "on" + p),
                ((e = e[C.expando] ? e : new C.Event(p, "object" == typeof e && e)).isTrigger = r ? 2 : 3),
                (e.namespace = d.join(".")),
                (e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + d.join("\\.(?:.*\\.|)") + "(\\.|$)") : null),
                (e.result = void 0),
                e.target || (e.target = n),
                (t = null == t ? [e] : C.makeArray(t, [e])),
                (l = C.event.special[p] || {}),
                r || !l.trigger || !1 !== l.trigger.apply(n, t))
            ) {
                if (!r && !l.noBubble && !g(n)) {
                    for (a = l.delegateType || p, Nt.test(a + p) || (h = h.parentNode); h; h = h.parentNode) f.push(h), (o = h);
                    o === (n.ownerDocument || E) && f.push(o.defaultView || o.parentWindow || w);
                }
                for (i = 0; (h = f[i++]) && !e.isPropagationStopped(); )
                    (c = h),
                        (e.type = 1 < i ? a : l.bindType || p),
                        (u = (J.get(h, "events") || Object.create(null))[e.type] && J.get(h, "handle")) && u.apply(h, t),
                        (u = s && h[s]) && u.apply && z(h) && ((e.result = u.apply(h, t)), !1 === e.result && e.preventDefault());
                return (
                    (e.type = p),
                    r ||
                        e.isDefaultPrevented() ||
                        (l._default && !1 !== l._default.apply(f.pop(), t)) ||
                        !z(n) ||
                        (s &&
                            x(n[p]) &&
                            !g(n) &&
                            ((o = n[s]) && (n[s] = null),
                            (C.event.triggered = p),
                            e.isPropagationStopped() && c.addEventListener(p, At),
                            n[p](),
                            e.isPropagationStopped() && c.removeEventListener(p, At),
                            (C.event.triggered = void 0),
                            o && (n[s] = o))),
                    e.result
                );
            }
        },
        simulate: function (e, t, n) {
            e = C.extend(new C.Event(), n, { type: e, isSimulated: !0 });
            C.event.trigger(e, null, t);
        },
    }),
        C.fn.extend({
            trigger: function (e, t) {
                return this.each(function () {
                    C.event.trigger(e, t, this);
                });
            },
            triggerHandler: function (e, t) {
                var n = this[0];
                if (n) return C.event.trigger(e, t, n, !0);
            },
        }),
        y.focusin ||
            C.each({ focus: "focusin", blur: "focusout" }, function (n, r) {
                function i(e) {
                    C.event.simulate(r, e.target, C.event.fix(e));
                }
                C.event.special[r] = {
                    setup: function () {
                        var e = this.ownerDocument || this.document || this,
                            t = J.access(e, r);
                        t || e.addEventListener(n, i, !0), J.access(e, r, (t || 0) + 1);
                    },
                    teardown: function () {
                        var e = this.ownerDocument || this.document || this,
                            t = J.access(e, r) - 1;
                        t ? J.access(e, r, t) : (e.removeEventListener(n, i, !0), J.remove(e, r));
                    },
                };
            });
    var Dt = w.location,
        kt = { guid: Date.now() },
        jt = /\?/;
    C.parseXML = function (e) {
        var t;
        if (!e || "string" != typeof e) return null;
        try {
            t = new w.DOMParser().parseFromString(e, "text/xml");
        } catch (e) {
            t = void 0;
        }
        return (t && !t.getElementsByTagName("parsererror").length) || C.error("Invalid XML: " + e), t;
    };
    var _t = /\[\]$/,
        qt = /\r?\n/g,
        Lt = /^(?:submit|button|image|reset|file)$/i,
        It = /^(?:input|select|textarea|keygen)/i;
    (C.param = function (e, t) {
        function n(e, t) {
            (t = x(t) ? t() : t), (i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == t ? "" : t));
        }
        var r,
            i = [];
        if (null == e) return "";
        if (Array.isArray(e) || (e.jquery && !C.isPlainObject(e)))
            C.each(e, function () {
                n(this.name, this.value);
            });
        else
            for (r in e)
                !(function n(r, e, i, o) {
                    if (Array.isArray(e))
                        C.each(e, function (e, t) {
                            i || _t.test(r) ? o(r, t) : n(r + "[" + ("object" == typeof t && null != t ? e : "") + "]", t, i, o);
                        });
                    else if (i || "object" !== h(e)) o(r, e);
                    else for (var t in e) n(r + "[" + t + "]", e[t], i, o);
                })(r, e[r], t, n);
        return i.join("&");
    }),
        C.fn.extend({
            serialize: function () {
                return C.param(this.serializeArray());
            },
            serializeArray: function () {
                return this.map(function () {
                    var e = C.prop(this, "elements");
                    return e ? C.makeArray(e) : this;
                })
                    .filter(function () {
                        var e = this.type;
                        return this.name && !C(this).is(":disabled") && It.test(this.nodeName) && !Lt.test(e) && (this.checked || !ce.test(e));
                    })
                    .map(function (e, t) {
                        var n = C(this).val();
                        return null == n
                            ? null
                            : Array.isArray(n)
                            ? C.map(n, function (e) {
                                  return { name: t.name, value: e.replace(qt, "\r\n") };
                              })
                            : { name: t.name, value: n.replace(qt, "\r\n") };
                    })
                    .get();
            },
        });
    var Ot = /%20/g,
        Rt = /#.*$/,
        Ht = /([?&])_=[^&]*/,
        Pt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Mt = /^(?:GET|HEAD)$/,
        Wt = /^\/\//,
        Ft = {},
        Bt = {},
        $t = "*/".concat("*"),
        Ut = E.createElement("a");
    function Vt(o) {
        return function (e, t) {
            "string" != typeof e && ((t = e), (e = "*"));
            var n,
                r = 0,
                i = e.toLowerCase().match(O) || [];
            if (x(t)) for (; (n = i[r++]); ) "+" === n[0] ? ((n = n.slice(1) || "*"), (o[n] = o[n] || []).unshift(t)) : (o[n] = o[n] || []).push(t);
        };
    }
    function Gt(t, r, i, o) {
        var a = {},
            s = t === Bt;
        function u(e) {
            var n;
            return (
                (a[e] = !0),
                C.each(t[e] || [], function (e, t) {
                    t = t(r, i, o);
                    return "string" != typeof t || s || a[t] ? (s ? !(n = t) : void 0) : (r.dataTypes.unshift(t), u(t), !1);
                }),
                n
            );
        }
        return u(r.dataTypes[0]) || (!a["*"] && u("*"));
    }
    function zt(e, t) {
        var n,
            r,
            i = C.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((i[n] ? e : (r = r || {}))[n] = t[n]);
        return r && C.extend(!0, e, r), e;
    }
    (Ut.href = Dt.href),
        C.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: Dt.href,
                type: "GET",
                isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Dt.protocol),
                global: !0,
                processData: !0,
                async: !0,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: { "*": $t, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript" },
                contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
                responseFields: { xml: "responseXML", text: "responseText", json: "responseJSON" },
                converters: { "* text": String, "text html": !0, "text json": JSON.parse, "text xml": C.parseXML },
                flatOptions: { url: !0, context: !0 },
            },
            ajaxSetup: function (e, t) {
                return t ? zt(zt(e, C.ajaxSettings), t) : zt(C.ajaxSettings, e);
            },
            ajaxPrefilter: Vt(Ft),
            ajaxTransport: Vt(Bt),
            ajax: function (e, t) {
                "object" == typeof e && ((t = e), (e = void 0)), (t = t || {});
                var u,
                    l,
                    c,
                    n,
                    f,
                    r,
                    p,
                    d,
                    i,
                    h = C.ajaxSetup({}, t),
                    g = h.context || h,
                    m = h.context && (g.nodeType || g.jquery) ? C(g) : C.event,
                    v = C.Deferred(),
                    y = C.Callbacks("once memory"),
                    x = h.statusCode || {},
                    o = {},
                    a = {},
                    s = "canceled",
                    b = {
                        readyState: 0,
                        getResponseHeader: function (e) {
                            var t;
                            if (p) {
                                if (!n) for (n = {}; (t = Pt.exec(c)); ) n[t[1].toLowerCase() + " "] = (n[t[1].toLowerCase() + " "] || []).concat(t[2]);
                                t = n[e.toLowerCase() + " "];
                            }
                            return null == t ? null : t.join(", ");
                        },
                        getAllResponseHeaders: function () {
                            return p ? c : null;
                        },
                        setRequestHeader: function (e, t) {
                            return null == p && ((e = a[e.toLowerCase()] = a[e.toLowerCase()] || e), (o[e] = t)), this;
                        },
                        overrideMimeType: function (e) {
                            return null == p && (h.mimeType = e), this;
                        },
                        statusCode: function (e) {
                            if (e)
                                if (p) b.always(e[b.status]);
                                else for (var t in e) x[t] = [x[t], e[t]];
                            return this;
                        },
                        abort: function (e) {
                            e = e || s;
                            return u && u.abort(e), T(0, e), this;
                        },
                    };
                if (
                    (v.promise(b),
                    (h.url = ((e || h.url || Dt.href) + "").replace(Wt, Dt.protocol + "//")),
                    (h.type = t.method || t.type || h.method || h.type),
                    (h.dataTypes = (h.dataType || "*").toLowerCase().match(O) || [""]),
                    null == h.crossDomain)
                ) {
                    r = E.createElement("a");
                    try {
                        (r.href = h.url), (r.href = r.href), (h.crossDomain = Ut.protocol + "//" + Ut.host != r.protocol + "//" + r.host);
                    } catch (e) {
                        h.crossDomain = !0;
                    }
                }
                if ((h.data && h.processData && "string" != typeof h.data && (h.data = C.param(h.data, h.traditional)), Gt(Ft, h, t, b), p)) return b;
                for (i in ((d = C.event && h.global) && 0 == C.active++ && C.event.trigger("ajaxStart"),
                (h.type = h.type.toUpperCase()),
                (h.hasContent = !Mt.test(h.type)),
                (l = h.url.replace(Rt, "")),
                h.hasContent
                    ? h.data && h.processData && 0 === (h.contentType || "").indexOf("application/x-www-form-urlencoded") && (h.data = h.data.replace(Ot, "+"))
                    : ((e = h.url.slice(l.length)),
                      h.data && (h.processData || "string" == typeof h.data) && ((l += (jt.test(l) ? "&" : "?") + h.data), delete h.data),
                      !1 === h.cache && ((l = l.replace(Ht, "$1")), (e = (jt.test(l) ? "&" : "?") + "_=" + kt.guid++ + e)),
                      (h.url = l + e)),
                h.ifModified && (C.lastModified[l] && b.setRequestHeader("If-Modified-Since", C.lastModified[l]), C.etag[l] && b.setRequestHeader("If-None-Match", C.etag[l])),
                ((h.data && h.hasContent && !1 !== h.contentType) || t.contentType) && b.setRequestHeader("Content-Type", h.contentType),
                b.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + $t + "; q=0.01" : "") : h.accepts["*"]),
                h.headers))
                    b.setRequestHeader(i, h.headers[i]);
                if (h.beforeSend && (!1 === h.beforeSend.call(g, b, h) || p)) return b.abort();
                if (((s = "abort"), y.add(h.complete), b.done(h.success), b.fail(h.error), (u = Gt(Bt, h, t, b)))) {
                    if (((b.readyState = 1), d && m.trigger("ajaxSend", [b, h]), p)) return b;
                    h.async &&
                        0 < h.timeout &&
                        (f = w.setTimeout(function () {
                            b.abort("timeout");
                        }, h.timeout));
                    try {
                        (p = !1), u.send(o, T);
                    } catch (e) {
                        if (p) throw e;
                        T(-1, e);
                    }
                } else T(-1, "No Transport");
                function T(e, t, n, r) {
                    var i,
                        o,
                        a,
                        s = t;
                    p ||
                        ((p = !0),
                        f && w.clearTimeout(f),
                        (u = void 0),
                        (c = r || ""),
                        (b.readyState = 0 < e ? 4 : 0),
                        (r = (200 <= e && e < 300) || 304 === e),
                        n &&
                            (a = (function (e, t, n) {
                                for (var r, i, o, a, s = e.contents, u = e.dataTypes; "*" === u[0]; ) u.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
                                if (r)
                                    for (i in s)
                                        if (s[i] && s[i].test(r)) {
                                            u.unshift(i);
                                            break;
                                        }
                                if (u[0] in n) o = u[0];
                                else {
                                    for (i in n) {
                                        if (!u[0] || e.converters[i + " " + u[0]]) {
                                            o = i;
                                            break;
                                        }
                                        a = a || i;
                                    }
                                    o = o || a;
                                }
                                if (o) return o !== u[0] && u.unshift(o), n[o];
                            })(h, b, n)),
                        !r && -1 < C.inArray("script", h.dataTypes) && (h.converters["text script"] = function () {}),
                        (a = (function (e, t, n, r) {
                            var i,
                                o,
                                a,
                                s,
                                u,
                                l = {},
                                c = e.dataTypes.slice();
                            if (c[1]) for (a in e.converters) l[a.toLowerCase()] = e.converters[a];
                            for (o = c.shift(); o; )
                                if ((e.responseFields[o] && (n[e.responseFields[o]] = t), !u && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), (u = o), (o = c.shift())))
                                    if ("*" === o) o = u;
                                    else if ("*" !== u && u !== o) {
                                        if (!(a = l[u + " " + o] || l["* " + o]))
                                            for (i in l)
                                                if (((s = i.split(" ")), s[1] === o && (a = l[u + " " + s[0]] || l["* " + s[0]]))) {
                                                    !0 === a ? (a = l[i]) : !0 !== l[i] && ((o = s[0]), c.unshift(s[1]));
                                                    break;
                                                }
                                        if (!0 !== a)
                                            if (a && e.throws) t = a(t);
                                            else
                                                try {
                                                    t = a(t);
                                                } catch (e) {
                                                    return { state: "parsererror", error: a ? e : "No conversion from " + u + " to " + o };
                                                }
                                    }
                            return { state: "success", data: t };
                        })(h, a, b, r)),
                        r
                            ? (h.ifModified && ((n = b.getResponseHeader("Last-Modified")) && (C.lastModified[l] = n), (n = b.getResponseHeader("etag")) && (C.etag[l] = n)),
                              204 === e || "HEAD" === h.type ? (s = "nocontent") : 304 === e ? (s = "notmodified") : ((s = a.state), (i = a.data), (r = !(o = a.error))))
                            : ((o = s), (!e && s) || ((s = "error"), e < 0 && (e = 0))),
                        (b.status = e),
                        (b.statusText = (t || s) + ""),
                        r ? v.resolveWith(g, [i, s, b]) : v.rejectWith(g, [b, s, o]),
                        b.statusCode(x),
                        (x = void 0),
                        d && m.trigger(r ? "ajaxSuccess" : "ajaxError", [b, h, r ? i : o]),
                        y.fireWith(g, [b, s]),
                        d && (m.trigger("ajaxComplete", [b, h]), --C.active || C.event.trigger("ajaxStop")));
                }
                return b;
            },
            getJSON: function (e, t, n) {
                return C.get(e, t, n, "json");
            },
            getScript: function (e, t) {
                return C.get(e, void 0, t, "script");
            },
        }),
        C.each(["get", "post"], function (e, i) {
            C[i] = function (e, t, n, r) {
                return x(t) && ((r = r || n), (n = t), (t = void 0)), C.ajax(C.extend({ url: e, type: i, dataType: r, data: t, success: n }, C.isPlainObject(e) && e));
            };
        }),
        C.ajaxPrefilter(function (e) {
            for (var t in e.headers) "content-type" === t.toLowerCase() && (e.contentType = e.headers[t] || "");
        }),
        (C._evalUrl = function (e, t, n) {
            return C.ajax({
                url: e,
                type: "GET",
                dataType: "script",
                cache: !0,
                async: !1,
                global: !1,
                converters: { "text script": function () {} },
                dataFilter: function (e) {
                    C.globalEval(e, t, n);
                },
            });
        }),
        C.fn.extend({
            wrapAll: function (e) {
                return (
                    this[0] &&
                        (x(e) && (e = e.call(this[0])),
                        (e = C(e, this[0].ownerDocument).eq(0).clone(!0)),
                        this[0].parentNode && e.insertBefore(this[0]),
                        e
                            .map(function () {
                                for (var e = this; e.firstElementChild; ) e = e.firstElementChild;
                                return e;
                            })
                            .append(this)),
                    this
                );
            },
            wrapInner: function (n) {
                return x(n)
                    ? this.each(function (e) {
                          C(this).wrapInner(n.call(this, e));
                      })
                    : this.each(function () {
                          var e = C(this),
                              t = e.contents();
                          t.length ? t.wrapAll(n) : e.append(n);
                      });
            },
            wrap: function (t) {
                var n = x(t);
                return this.each(function (e) {
                    C(this).wrapAll(n ? t.call(this, e) : t);
                });
            },
            unwrap: function (e) {
                return (
                    this.parent(e)
                        .not("body")
                        .each(function () {
                            C(this).replaceWith(this.childNodes);
                        }),
                    this
                );
            },
        }),
        (C.expr.pseudos.hidden = function (e) {
            return !C.expr.pseudos.visible(e);
        }),
        (C.expr.pseudos.visible = function (e) {
            return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length);
        }),
        (C.ajaxSettings.xhr = function () {
            try {
                return new w.XMLHttpRequest();
            } catch (e) {}
        });
    var Xt = { 0: 200, 1223: 204 },
        Jt = C.ajaxSettings.xhr();
    (y.cors = !!Jt && "withCredentials" in Jt),
        (y.ajax = Jt = !!Jt),
        C.ajaxTransport(function (i) {
            var o, a;
            if (y.cors || (Jt && !i.crossDomain))
                return {
                    send: function (e, t) {
                        var n,
                            r = i.xhr();
                        if ((r.open(i.type, i.url, i.async, i.username, i.password), i.xhrFields)) for (n in i.xhrFields) r[n] = i.xhrFields[n];
                        for (n in (i.mimeType && r.overrideMimeType && r.overrideMimeType(i.mimeType), i.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e)) r.setRequestHeader(n, e[n]);
                        (o = function (e) {
                            return function () {
                                o &&
                                    ((o = a = r.onload = r.onerror = r.onabort = r.ontimeout = r.onreadystatechange = null),
                                    "abort" === e
                                        ? r.abort()
                                        : "error" === e
                                        ? "number" != typeof r.status
                                            ? t(0, "error")
                                            : t(r.status, r.statusText)
                                        : t(Xt[r.status] || r.status, r.statusText, "text" !== (r.responseType || "text") || "string" != typeof r.responseText ? { binary: r.response } : { text: r.responseText }, r.getAllResponseHeaders()));
                            };
                        }),
                            (r.onload = o()),
                            (a = r.onerror = r.ontimeout = o("error")),
                            void 0 !== r.onabort
                                ? (r.onabort = a)
                                : (r.onreadystatechange = function () {
                                      4 === r.readyState &&
                                          w.setTimeout(function () {
                                              o && a();
                                          });
                                  }),
                            (o = o("abort"));
                        try {
                            r.send((i.hasContent && i.data) || null);
                        } catch (e) {
                            if (o) throw e;
                        }
                    },
                    abort: function () {
                        o && o();
                    },
                };
        }),
        C.ajaxPrefilter(function (e) {
            e.crossDomain && (e.contents.script = !1);
        }),
        C.ajaxSetup({
            accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" },
            contents: { script: /\b(?:java|ecma)script\b/ },
            converters: {
                "text script": function (e) {
                    return C.globalEval(e), e;
                },
            },
        }),
        C.ajaxPrefilter("script", function (e) {
            void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET");
        }),
        C.ajaxTransport("script", function (n) {
            var r, i;
            if (n.crossDomain || n.scriptAttrs)
                return {
                    send: function (e, t) {
                        (r = C("<script>")
                            .attr(n.scriptAttrs || {})
                            .prop({ charset: n.scriptCharset, src: n.url })
                            .on(
                                "load error",
                                (i = function (e) {
                                    r.remove(), (i = null), e && t("error" === e.type ? 404 : 200, e.type);
                                })
                            )),
                            E.head.appendChild(r[0]);
                    },
                    abort: function () {
                        i && i();
                    },
                };
        });
    var Qt = [],
        Yt = /(=)\?(?=&|$)|\?\?/;
    C.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function () {
            var e = Qt.pop() || C.expando + "_" + kt.guid++;
            return (this[e] = !0), e;
        },
    }),
        C.ajaxPrefilter("json jsonp", function (e, t, n) {
            var r,
                i,
                o,
                a = !1 !== e.jsonp && (Yt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Yt.test(e.data) && "data");
            if (a || "jsonp" === e.dataTypes[0])
                return (
                    (r = e.jsonpCallback = x(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback),
                    a ? (e[a] = e[a].replace(Yt, "$1" + r)) : !1 !== e.jsonp && (e.url += (jt.test(e.url) ? "&" : "?") + e.jsonp + "=" + r),
                    (e.converters["script json"] = function () {
                        return o || C.error(r + " was not called"), o[0];
                    }),
                    (e.dataTypes[0] = "json"),
                    (i = w[r]),
                    (w[r] = function () {
                        o = arguments;
                    }),
                    n.always(function () {
                        void 0 === i ? C(w).removeProp(r) : (w[r] = i), e[r] && ((e.jsonpCallback = t.jsonpCallback), Qt.push(r)), o && x(i) && i(o[0]), (o = i = void 0);
                    }),
                    "script"
                );
        }),
        (y.createHTMLDocument = (((f = E.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>"), 2 === f.childNodes.length)),
        (C.parseHTML = function (e, t, n) {
            return "string" != typeof e
                ? []
                : ("boolean" == typeof t && ((n = t), (t = !1)),
                  t || (y.createHTMLDocument ? (((r = (t = E.implementation.createHTMLDocument("")).createElement("base")).href = E.location.href), t.head.appendChild(r)) : (t = E)),
                  (r = !n && []),
                  (n = D.exec(e)) ? [t.createElement(n[1])] : ((n = ve([e], t, r)), r && r.length && C(r).remove(), C.merge([], n.childNodes)));
            var r;
        }),
        (C.fn.load = function (e, t, n) {
            var r,
                i,
                o,
                a = this,
                s = e.indexOf(" ");
            return (
                -1 < s && ((r = wt(e.slice(s))), (e = e.slice(0, s))),
                x(t) ? ((n = t), (t = void 0)) : t && "object" == typeof t && (i = "POST"),
                0 < a.length &&
                    C.ajax({ url: e, type: i || "GET", dataType: "html", data: t })
                        .done(function (e) {
                            (o = arguments), a.html(r ? C("<div>").append(C.parseHTML(e)).find(r) : e);
                        })
                        .always(
                            n &&
                                function (e, t) {
                                    a.each(function () {
                                        n.apply(this, o || [e.responseText, t, e]);
                                    });
                                }
                        ),
                this
            );
        }),
        (C.expr.pseudos.animated = function (t) {
            return C.grep(C.timers, function (e) {
                return t === e.elem;
            }).length;
        }),
        (C.offset = {
            setOffset: function (e, t, n) {
                var r,
                    i,
                    o,
                    a,
                    s = C.css(e, "position"),
                    u = C(e),
                    l = {};
                "static" === s && (e.style.position = "relative"),
                    (o = u.offset()),
                    (r = C.css(e, "top")),
                    (a = C.css(e, "left")),
                    (a = ("absolute" === s || "fixed" === s) && -1 < (r + a).indexOf("auto") ? ((i = (s = u.position()).top), s.left) : ((i = parseFloat(r) || 0), parseFloat(a) || 0)),
                    null != (t = x(t) ? t.call(e, n, C.extend({}, o)) : t).top && (l.top = t.top - o.top + i),
                    null != t.left && (l.left = t.left - o.left + a),
                    "using" in t ? t.using.call(e, l) : ("number" == typeof l.top && (l.top += "px"), "number" == typeof l.left && (l.left += "px"), u.css(l));
            },
        }),
        C.fn.extend({
            offset: function (t) {
                if (arguments.length)
                    return void 0 === t
                        ? this
                        : this.each(function (e) {
                              C.offset.setOffset(this, t, e);
                          });
                var e,
                    n = this[0];
                return n ? (n.getClientRects().length ? ((e = n.getBoundingClientRect()), (n = n.ownerDocument.defaultView), { top: e.top + n.pageYOffset, left: e.left + n.pageXOffset }) : { top: 0, left: 0 }) : void 0;
            },
            position: function () {
                if (this[0]) {
                    var e,
                        t,
                        n,
                        r = this[0],
                        i = { top: 0, left: 0 };
                    if ("fixed" === C.css(r, "position")) t = r.getBoundingClientRect();
                    else {
                        for (t = this.offset(), n = r.ownerDocument, e = r.offsetParent || n.documentElement; e && (e === n.body || e === n.documentElement) && "static" === C.css(e, "position"); ) e = e.parentNode;
                        e && e !== r && 1 === e.nodeType && (((i = C(e).offset()).top += C.css(e, "borderTopWidth", !0)), (i.left += C.css(e, "borderLeftWidth", !0)));
                    }
                    return { top: t.top - i.top - C.css(r, "marginTop", !0), left: t.left - i.left - C.css(r, "marginLeft", !0) };
                }
            },
            offsetParent: function () {
                return this.map(function () {
                    for (var e = this.offsetParent; e && "static" === C.css(e, "position"); ) e = e.offsetParent;
                    return e || re;
                });
            },
        }),
        C.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function (t, i) {
            var o = "pageYOffset" === i;
            C.fn[t] = function (e) {
                return B(
                    this,
                    function (e, t, n) {
                        var r;
                        return g(e) ? (r = e) : 9 === e.nodeType && (r = e.defaultView), void 0 === n ? (r ? r[i] : e[t]) : void (r ? r.scrollTo(o ? r.pageXOffset : n, o ? n : r.pageYOffset) : (e[t] = n));
                    },
                    t,
                    e,
                    arguments.length
                );
            };
        }),
        C.each(["top", "left"], function (e, n) {
            C.cssHooks[n] = Qe(y.pixelPosition, function (e, t) {
                if (t) return (t = Je(e, n)), Ue.test(t) ? C(e).position()[n] + "px" : t;
            });
        }),
        C.each({ Height: "height", Width: "width" }, function (a, s) {
            C.each({ padding: "inner" + a, content: s, "": "outer" + a }, function (r, o) {
                C.fn[o] = function (e, t) {
                    var n = arguments.length && (r || "boolean" != typeof e),
                        i = r || (!0 === e || !0 === t ? "margin" : "border");
                    return B(
                        this,
                        function (e, t, n) {
                            var r;
                            return g(e)
                                ? 0 === o.indexOf("outer")
                                    ? e["inner" + a]
                                    : e.document.documentElement["client" + a]
                                : 9 === e.nodeType
                                ? ((r = e.documentElement), Math.max(e.body["scroll" + a], r["scroll" + a], e.body["offset" + a], r["offset" + a], r["client" + a]))
                                : void 0 === n
                                ? C.css(e, t, i)
                                : C.style(e, t, n, i);
                        },
                        s,
                        n ? e : void 0,
                        n
                    );
                };
            });
        }),
        C.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
            C.fn[t] = function (e) {
                return this.on(t, e);
            };
        }),
        C.fn.extend({
            bind: function (e, t, n) {
                return this.on(e, null, t, n);
            },
            unbind: function (e, t) {
                return this.off(e, null, t);
            },
            delegate: function (e, t, n, r) {
                return this.on(t, e, n, r);
            },
            undelegate: function (e, t, n) {
                return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n);
            },
            hover: function (e, t) {
                return this.mouseenter(e).mouseleave(t || e);
            },
        }),
        C.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (e, n) {
            C.fn[n] = function (e, t) {
                return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n);
            };
        });
    var Kt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    (C.proxy = function (e, t) {
        var n, r;
        if (("string" == typeof t && ((r = e[t]), (t = e), (e = r)), x(e)))
            return (
                (n = s.call(arguments, 2)),
                ((r = function () {
                    return e.apply(t || this, n.concat(s.call(arguments)));
                }).guid = e.guid = e.guid || C.guid++),
                r
            );
    }),
        (C.holdReady = function (e) {
            e ? C.readyWait++ : C.ready(!0);
        }),
        (C.isArray = Array.isArray),
        (C.parseJSON = JSON.parse),
        (C.nodeName = N),
        (C.isFunction = x),
        (C.isWindow = g),
        (C.camelCase = G),
        (C.type = h),
        (C.now = Date.now),
        (C.isNumeric = function (e) {
            var t = C.type(e);
            return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e));
        }),
        (C.trim = function (e) {
            return null == e ? "" : (e + "").replace(Kt, "");
        }),
        "function" == typeof define &&
            define.amd &&
            define("jquery", [], function () {
                return C;
            });
    var Zt = w.djQ,
        en = w.$;
    return (
        (C.noConflict = function (e) {
            return w.$ === C && (w.$ = en), e && w.djQ === C && (w.djQ = Zt), C;
        }),
        void 0 === e && (w.djQ = w.$ = C),
        C
    );
});
var DAVE_SETTINGS = {
    BASE_URL: "https://test.iamdave.ai",
    SIGNUP_API_KEY: "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__",
    ENTERPRISE_ID: "dave_expo",
    CONVERSATION_ID: "exhibit_aldo",
    SESSION_MODEL: "person_session",
    USER_MODEL: "person",
    DEFAULT_USER_DATA: { person_type: "visitor" },
};
function signup(e, t, n) {
    var r,
        i = DAVE_SETTINGS.BASE_URL + "/customer-signup",
        o = Math.random().toString(36).slice(-8);
    for (r in ((e = e || {}).email || (e.email = "ananth+" + o + "@iamdave.ai"), DAVE_SETTINGS.DEFAULT_USER_DATA)) e[r] || (e[r] = DAVE_SETTINGS.DEFAULT_USER_DATA[r]);
    djQ.ajax({
        url: i,
        method: "POST",
        dataType: "json",
        contentType: "json",
        withCredentials: !0,
        headers: { "Content-Type": "application/json", "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID, "X-I2CE-SIGNUP-API-KEY": DAVE_SETTINGS.SIGNUP_API_KEY },
        data: JSON.stringify(e),
        success: function (e) {
            (HEADERS = { "Content-Type": "application/json", "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID, "X-I2CE-USER-ID": e.user_id, "X-I2CE-API-KEY": e.api_key }),
                setCookie("authentication", JSON.stringify(HEADERS), 24),
                setCookie("user_id", e.user_id),
                initialize_session(),
                t && t(e);
        },
        error: function (e, t) {
            console.log(t), n && n(e, t);
        },
    });
}
function patch_user(e, t, n) {
    var r = getCookie("user_id");
    (e = e || {}).person_name && e.email && e.mobile_number && (e.signed_up = !0), ajaxRequestWithData("/object/" + DAVE_SETTINGS.USER_MODEL + "/" + r, "PATCH", JSON.stringify(e), t, n);
}
function create_session(e, t, n) {
    var r = getCookie("user_id");
    (e.user_id = r),
        ajaxRequestWithData(
            "/object/" + DAVE_SETTINGS.SESSION_MODEL,
            "POST",
            JSON.stringify(e),
            function (e) {
                setCookie("session_id", e.session_id), t && t(e);
            },
            n
        );
}
function chat(e, t, n) {
    var r = getCookie("user_id");
    r ||
        setTimeout(function () {
            chat(customer_state, t, n);
        }, 5e3);
    var i = getCookie("engagement_id");
    i && (e.engagement_id = i);
    i = getCookie("system_response");
    i && (e.system_response = i),
        ajaxRequestWithData(
            "/conversation/" + DAVE_SETTINGS.CONVERSATION_ID + "/" + r,
            "POST",
            JSON.stringify(e),
            function (e) {
                setCookie("engagement_id", e.engagement_id), setCookie("system_response", e.name), t && t(e);
            },
            n
        );
}
function iupdate_user(e, t, n) {
    var r = getCookie("user_id");
    ((e = e || {})._async = !0), ajaxRequestWithData("/iupdate/" + DAVE_SETTINGS.USER_MODEL + "/" + r, "PATCH", JSON.stringify(e), t, n);
}
function iupdate_session(e, t, n) {
    var r = getCookie("session_id");
    ((e = e || {})._async = !0), ajaxRequestWithData("/iupdate/" + DAVE_SETTINGS.SESSION_MODEL + "/" + r, "PATCH", JSON.stringify(e), t, n);
}
function getCookie(e) {
    var t = window.localStorage.getItem(e);
    if (!t) return null;
    try {
        r = JSON.parse(t);
    } catch (e) {
        t && (r = t);
    }
    return r;
}
function setCookie(e, t, n) {
    void 0 === n && (n = 24), null != t && ("object" == typeof t && (t = JSON.stringify(t)), n < 0 ? window.localStorage.removeItem(e) : window.localStorage.setItem(e, t));
}
function Trim(e) {
    return e.replace(/^\s+|\s+$/g, "");
}
function toTitleCase(e) {
    return e.replace(/_/g, " ").replace(/(?:^|\s)\w/g, function (e) {
        return e.toUpperCase();
    });
}
function generate_random_string(t) {
    let n = "",
        r;
    for (let e = 0; e < t; e++) (r = Math.floor(25 * Math.random() + 97)), (n += String.fromCharCode(r));
    return n;
}
function ajaxRequestSync(e, t, n, r, i, o, a) {
    (a = a || getCookie("authentication")),
        (o =
            o ||
            function () {
                signup(
                    {},
                    ajaxRequestSync(
                        e,
                        t,
                        n,
                        r,
                        i,
                        function () {
                            console.log("Failed after signup"), alert("Could not signup! Please contact customer support!");
                        },
                        a
                    )
                );
            }),
        djQ
            .ajax({ url: DAVE_SETTINGS.BASE_URL + e, method: (t || "GET").toUpperCase(), dataType: "json", contentType: "json", async: i || !1, headers: a, statusCode: { 401: o, 404: o } })
            .done(function (e) {
                (result = e), n && n(e);
            })
            .fail(function (e) {
                r && r(e);
            });
}
function ajaxRequest(e, t, n, r, i, o) {
    return ajaxRequestSync(e, t, n, r, !0, i, o);
}
function ajaxRequestWithData(e, t, n, r, i, o, a) {
    a = a || getCookie("authentication");
    var s = n ? n : "";
    (o =
        o ||
        function () {
            signup(
                {},
                ajaxRequestWithData(
                    e,
                    t,
                    n,
                    r,
                    i,
                    function () {
                        console.log("Failed after signup"), alert("Could not signup! Please contact customer support!");
                    },
                    a
                )
            );
        }),
        djQ
            .ajax({ url: DAVE_SETTINGS.BASE_URL + e, method: (t || "GET").toUpperCase(), dataType: "json", contentType: "application/json", headers: a, data: s, statusCode: { 401: o, 404: o } })
            .done(function (e) {
                r && r(e);
            })
            .fail(function (e) {
                i && i(e);
            });
}
function initialize_session() {
    let t = [];
    create_session({ location_dict: "{agent_info.ip}", browser: "{agent_info.browser}", os: "{agent_info.os}", device_type: "{agent_info.device}" }, function (e) {
        setCookie("last_login", e.updated);
        e = setTimeout(function () {
            iupdate_session({ session_duration: 1 });
            var e = setTimeout(function () {
                iupdate_session({ session_duration: 5 });
                var e = setTimeout(function () {
                    iupdate_session({ session_duration: 10 });
                    var e = setTimeout(function () {
                        iupdate_session({ session_duration: 20 });
                        var e = setTimeout(function () {
                            iupdate_session({ session_duration: 30 });
                            var e = setInterval(function () {
                                iupdate_session({ session_duration: 60 });
                            }, 6e4);
                            t.push(e);
                        }, 3e4);
                        t.push(e);
                    }, 2e4);
                    t.push(e);
                }, 1e4);
                t.push(e);
            }, 5e3);
            t.push(e);
        }, 1e3);
        t.push(e);
    });
}
djQ(document).ready(function () {
    if (djQ("#dave-settings").length) {
        let e = djQ("#dave-settings");
        for (var t in DAVE_SETTINGS) e.attr(t) && (DAVE_SETTINGS[t] = e.attr(t));
    }
    (getCookie("authentication") ? initialize_session : signup)();
});
